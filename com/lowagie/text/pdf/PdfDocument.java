/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.Anchor;
import com.lowagie.text.Annotation;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocListener;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ElementListener;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Graphic;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.List;
import com.lowagie.text.ListItem;
import com.lowagie.text.Meta;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Section;
import com.lowagie.text.SimpleTable;
import com.lowagie.text.StringCompare;
import com.lowagie.text.Table;
import com.lowagie.text.Watermark;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.MultiColumnText;
import com.lowagie.text.pdf.PageResources;
import com.lowagie.text.pdf.PdfAcroForm;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfAnnotation;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfCell;
import com.lowagie.text.pdf.PdfChunk;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfContents;
import com.lowagie.text.pdf.PdfDate;
import com.lowagie.text.pdf.PdfDestination;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfException;
import com.lowagie.text.pdf.PdfFileSpecification;
import com.lowagie.text.pdf.PdfFont;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfIndirectObject;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfLine;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfOutline;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPage;
import com.lowagie.text.pdf.PdfPageEvent;
import com.lowagie.text.pdf.PdfPageLabels;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfRectangle;
import com.lowagie.text.pdf.PdfString;
import com.lowagie.text.pdf.PdfTable;
import com.lowagie.text.pdf.PdfTextArray;
import com.lowagie.text.pdf.PdfTransition;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.PdfXConformanceException;
import java.awt.Color;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

class PdfDocument
extends Document
implements DocListener {
    private PdfIndirectReference thumb;
    static final String hangingPunctuation = ".,;:'";
    private PdfWriter writer;
    private PdfInfo info = new PdfInfo();
    private boolean firstPageEvent = true;
    private boolean isParagraph = true;
    private PdfLine line = null;
    private float indentLeft = 0.0f;
    private float indentRight = 0.0f;
    private float listIndentLeft = 0.0f;
    private int alignment = 0;
    private PdfContentByte text;
    private PdfContentByte graphics;
    private ArrayList lines = new ArrayList();
    private float leading = 0.0f;
    private float currentHeight = 0.0f;
    private float indentTop = 0.0f;
    private float indentBottom = 0.0f;
    private boolean pageEmpty = true;
    private int textEmptySize;
    protected Rectangle nextPageSize = null;
    protected HashMap thisBoxSize = new HashMap();
    protected HashMap boxSize = new HashMap();
    protected PageResources pageResources;
    private Image imageWait = null;
    private float imageEnd = -1.0f;
    private float imageIndentLeft = 0.0f;
    private float imageIndentRight = 0.0f;
    private ArrayList annotations;
    private ArrayList delayedAnnotations = new ArrayList();
    PdfAcroForm acroForm;
    private PdfOutline rootOutline;
    private PdfOutline currentOutline;
    private PdfAction currentAction = null;
    private TreeMap localDestinations = new TreeMap(new StringCompare());
    private ArrayList documentJavaScript = new ArrayList();
    private int viewerPreferences = 0;
    private String openActionName;
    private PdfAction openActionAction;
    private PdfDictionary additionalActions;
    private PdfPageLabels pageLabels;
    private boolean isNewpage = false;
    private float paraIndent = 0.0f;
    protected float nextMarginLeft;
    protected float nextMarginRight;
    protected float nextMarginTop;
    protected float nextMarginBottom;
    protected int duration = -1;
    protected PdfTransition transition = null;
    protected PdfDictionary pageAA = null;
    private boolean strictImageSequence = false;
    private int lastElementType = -1;

    public PdfDocument() throws DocumentException {
        this.addProducer();
        this.addCreationDate();
    }

    public void addWriter(PdfWriter writer) throws DocumentException {
        if (this.writer == null) {
            this.writer = writer;
            this.acroForm = new PdfAcroForm(writer);
            return;
        }
        throw new DocumentException("You can only add a writer to a PdfDocument once.");
    }

    public boolean setPageSize(Rectangle pageSize) {
        if (this.writer != null && this.writer.isPaused()) {
            return false;
        }
        this.nextPageSize = new Rectangle(pageSize);
        return true;
    }

    public void setHeader(HeaderFooter header) {
        if (this.writer != null && this.writer.isPaused()) {
            return;
        }
        super.setHeader(header);
    }

    public void resetHeader() {
        if (this.writer != null && this.writer.isPaused()) {
            return;
        }
        super.resetHeader();
    }

    public void setFooter(HeaderFooter footer) {
        if (this.writer != null && this.writer.isPaused()) {
            return;
        }
        super.setFooter(footer);
    }

    public void resetFooter() {
        if (this.writer != null && this.writer.isPaused()) {
            return;
        }
        super.resetFooter();
    }

    public void resetPageCount() {
        if (this.writer != null && this.writer.isPaused()) {
            return;
        }
        super.resetPageCount();
    }

    public void setPageCount(int pageN) {
        if (this.writer != null && this.writer.isPaused()) {
            return;
        }
        super.setPageCount(pageN);
    }

    public boolean add(Watermark watermark) {
        if (this.writer != null && this.writer.isPaused()) {
            return false;
        }
        this.watermark = watermark;
        return true;
    }

    public void removeWatermark() {
        if (this.writer != null && this.writer.isPaused()) {
            return;
        }
        this.watermark = null;
    }

    public boolean setMargins(float marginLeft, float marginRight, float marginTop, float marginBottom) {
        if (this.writer != null && this.writer.isPaused()) {
            return false;
        }
        this.nextMarginLeft = marginLeft;
        this.nextMarginRight = marginRight;
        this.nextMarginTop = marginTop;
        this.nextMarginBottom = marginBottom;
        return true;
    }

    protected PdfArray rotateAnnotations() {
        PdfArray array = new PdfArray();
        int rotation = this.pageSize.getRotation() % 360;
        int currentPage = this.writer.getCurrentPageNumber();
        for (int k = 0; k < this.annotations.size(); ++k) {
            PdfAnnotation dic = (PdfAnnotation)this.annotations.get(k);
            int page = dic.getPlaceInPage();
            if (page > currentPage) {
                this.delayedAnnotations.add(dic);
                continue;
            }
            if (dic.isForm()) {
                HashMap templates;
                PdfFormField field;
                if (!(dic.isUsed() || (templates = dic.getTemplates()) == null)) {
                    this.acroForm.addFieldTemplates(templates);
                }
                if ((field = (PdfFormField)dic).getParent() == null) {
                    this.acroForm.addDocumentField(field.getIndirectReference());
                }
            }
            if (dic.isAnnotation()) {
                array.add(dic.getIndirectReference());
                if (!dic.isUsed()) {
                    PdfRectangle rect = (PdfRectangle)dic.get(PdfName.RECT);
                    switch (rotation) {
                        case 90: {
                            dic.put(PdfName.RECT, new PdfRectangle(this.pageSize.top() - rect.bottom(), rect.left(), this.pageSize.top() - rect.top(), rect.right()));
                            break;
                        }
                        case 180: {
                            dic.put(PdfName.RECT, new PdfRectangle(this.pageSize.right() - rect.left(), this.pageSize.top() - rect.bottom(), this.pageSize.right() - rect.right(), this.pageSize.top() - rect.top()));
                            break;
                        }
                        case 270: {
                            dic.put(PdfName.RECT, new PdfRectangle(rect.bottom(), this.pageSize.right() - rect.left(), rect.top(), this.pageSize.right() - rect.right()));
                        }
                    }
                }
            }
            if (dic.isUsed()) continue;
            dic.setUsed();
            try {
                this.writer.addToBody((PdfObject)dic, dic.getIndirectReference());
                continue;
            }
            catch (IOException e) {
                throw new ExceptionConverter(e);
            }
        }
        return array;
    }

    public boolean newPage() throws DocumentException {
        PdfArray array;
        this.lastElementType = -1;
        this.isNewpage = true;
        if (this.writer.getDirectContent().size() == 0 && this.writer.getDirectContentUnder().size() == 0 && (this.pageEmpty || this.writer != null && this.writer.isPaused())) {
            return false;
        }
        PdfPageEvent pageEvent = this.writer.getPageEvent();
        if (pageEvent != null) {
            pageEvent.onEndPage(this.writer, this);
        }
        super.newPage();
        this.imageIndentLeft = 0.0f;
        this.imageIndentRight = 0.0f;
        this.flushLines();
        this.pageResources.addDefaultColorDiff(this.writer.getDefaultColorspace());
        PdfDictionary resources = this.pageResources.getResources();
        if (this.writer.getPDFXConformance() != 0) {
            if (this.thisBoxSize.containsKey("art") && this.thisBoxSize.containsKey("trim")) {
                throw new PdfXConformanceException("Only one of ArtBox or TrimBox can exist in the page.");
            }
            if (!(this.thisBoxSize.containsKey("art") || this.thisBoxSize.containsKey("trim"))) {
                if (this.thisBoxSize.containsKey("crop")) {
                    this.thisBoxSize.put("trim", this.thisBoxSize.get("crop"));
                } else {
                    this.thisBoxSize.put("trim", new PdfRectangle(this.pageSize, this.pageSize.getRotation()));
                }
            }
        }
        int rotation = this.pageSize.getRotation();
        PdfPage page = new PdfPage(new PdfRectangle(this.pageSize, rotation), this.thisBoxSize, resources, rotation);
        if (this.transition != null) {
            page.put(PdfName.TRANS, this.transition.getTransitionDictionary());
            this.transition = null;
        }
        if (this.duration > 0) {
            page.put(PdfName.DUR, new PdfNumber(this.duration));
            this.duration = 0;
        }
        if (this.pageAA != null) {
            try {
                page.put(PdfName.AA, this.writer.addToBody(this.pageAA).getIndirectReference());
            }
            catch (IOException ioe) {
                throw new ExceptionConverter(ioe);
            }
            this.pageAA = null;
        }
        if (this.annotations.size() > 0 && (array = this.rotateAnnotations()).size() != 0) {
            page.put(PdfName.ANNOTS, array);
        }
        if (this.thumb != null) {
            page.put(PdfName.THUMB, this.thumb);
            this.thumb = null;
        }
        if (!this.open || this.close) {
            throw new PdfException("The document isn't open.");
        }
        if (this.text.size() > this.textEmptySize) {
            this.text.endText();
        } else {
            this.text = null;
        }
        PdfIndirectReference pageReference = this.writer.add(page, new PdfContents(this.writer.getDirectContentUnder(), this.graphics, this.text, this.writer.getDirectContent(), this.pageSize));
        this.initPage();
        this.isNewpage = false;
        return true;
    }

    public void open() {
        if (!this.open) {
            super.open();
            this.writer.open();
            this.currentOutline = this.rootOutline = new PdfOutline(this.writer);
        }
        try {
            this.initPage();
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }

    void outlineTree(PdfOutline outline) throws IOException {
        int k;
        outline.setIndirectReference(this.writer.getPdfIndirectReference());
        if (outline.parent() != null) {
            outline.put(PdfName.PARENT, outline.parent().indirectReference());
        }
        ArrayList kids = outline.getKids();
        int size = kids.size();
        for (k = 0; k < size; ++k) {
            this.outlineTree((PdfOutline)kids.get(k));
        }
        for (k = 0; k < size; ++k) {
            if (k > 0) {
                ((PdfOutline)kids.get(k)).put(PdfName.PREV, ((PdfOutline)kids.get(k - 1)).indirectReference());
            }
            if (k >= size - 1) continue;
            ((PdfOutline)kids.get(k)).put(PdfName.NEXT, ((PdfOutline)kids.get(k + 1)).indirectReference());
        }
        if (size > 0) {
            outline.put(PdfName.FIRST, ((PdfOutline)kids.get(0)).indirectReference());
            outline.put(PdfName.LAST, ((PdfOutline)kids.get(size - 1)).indirectReference());
        }
        for (k = 0; k < size; ++k) {
            PdfOutline kid = (PdfOutline)kids.get(k);
            this.writer.addToBody((PdfObject)kid, kid.indirectReference());
        }
    }

    void writeOutlines() throws IOException {
        if (this.rootOutline.getKids().size() == 0) {
            return;
        }
        this.outlineTree(this.rootOutline);
        this.writer.addToBody((PdfObject)this.rootOutline, this.rootOutline.indirectReference());
    }

    void traverseOutlineCount(PdfOutline outline) {
        ArrayList kids = outline.getKids();
        PdfOutline parent = outline.parent();
        if (kids.size() == 0) {
            if (parent != null) {
                parent.setCount(parent.getCount() + 1);
            }
        } else {
            for (int k = 0; k < kids.size(); ++k) {
                this.traverseOutlineCount((PdfOutline)kids.get(k));
            }
            if (parent != null) {
                if (outline.isOpen()) {
                    parent.setCount(outline.getCount() + parent.getCount() + 1);
                } else {
                    parent.setCount(parent.getCount() + 1);
                    outline.setCount(- outline.getCount());
                }
            }
        }
    }

    void calculateOutlineCount() {
        if (this.rootOutline.getKids().size() == 0) {
            return;
        }
        this.traverseOutlineCount(this.rootOutline);
    }

    public void close() {
        if (this.close) {
            return;
        }
        try {
            this.newPage();
            if (this.imageWait != null) {
                this.newPage();
            }
            if (this.annotations.size() > 0) {
                throw new RuntimeException(String.valueOf(this.annotations.size()) + " annotations had invalid placement pages.");
            }
            PdfPageEvent pageEvent = this.writer.getPageEvent();
            if (pageEvent != null) {
                pageEvent.onCloseDocument(this.writer, this);
            }
            super.close();
            this.writer.addLocalDestinations(this.localDestinations);
            this.calculateOutlineCount();
            this.writeOutlines();
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
        this.writer.close();
    }

    PageResources getPageResources() {
        return this.pageResources;
    }

    void addPTable(PdfPTable ptable) throws DocumentException {
        ColumnText ct = new ColumnText(this.writer.getDirectContent());
        if (this.currentHeight > 0.0f) {
            Paragraph p = new Paragraph();
            p.setLeading(0.0f);
            ct.addElement(p);
        }
        ct.addElement(ptable);
        boolean he = ptable.isHeadersInEvent();
        ptable.setHeadersInEvent(true);
        int loop = 0;
        do {
            ct.setSimpleColumn(this.indentLeft(), this.indentBottom(), this.indentRight(), this.indentTop() - this.currentHeight);
            int status = ct.go();
            if ((status & 1) != 0) {
                this.text.moveText(0.0f, ct.getYLine() - this.indentTop() + this.currentHeight);
                this.currentHeight = this.indentTop() - ct.getYLine();
                break;
            }
            loop = this.indentTop() - this.currentHeight == ct.getYLine() ? ++loop : 0;
            if (loop == 3) {
                this.add(new Paragraph("ERROR: Infinite table loop"));
                break;
            }
            this.newPage();
        } while (true);
        ptable.setHeadersInEvent(he);
    }

    PdfTable getPdfTable(Table table, boolean supportRowAdditions) {
        return new PdfTable(table, this.indentLeft(), this.indentRight(), this.indentTop() - this.currentHeight, supportRowAdditions);
    }

    boolean breakTableIfDoesntFit(PdfTable table) throws DocumentException {
        table.updateRowAdditions();
        if (!(table.hasToFitPageTable() || table.bottom() > this.indentBottom)) {
            this.add(table, true);
            return true;
        }
        return false;
    }

    private void add(PdfTable table, boolean onlyFirstPage) throws DocumentException {
        boolean tableHasToFit;
        this.flushLines();
        float pagetop = this.indentTop();
        float oldHeight = this.currentHeight;
        PdfContentByte cellGraphics = new PdfContentByte(this.writer);
        boolean bl = table.hasToFitPageTable() ? table.bottom() < this.indentBottom() : (tableHasToFit = false);
        if (this.pageEmpty) {
            tableHasToFit = false;
        }
        boolean cellsHaveToFit = table.hasToFitPageCells();
        ArrayList cells = table.getCells();
        ArrayList headercells = table.getHeaderCells();
        if (headercells.size() > 0 && (cells.size() == 0 || cells.get(0) != headercells.get(0))) {
            ArrayList allCells = new ArrayList(cells.size() + headercells.size());
            allCells.addAll(headercells);
            allCells.addAll(cells);
            cells = allCells;
        }
        while (!cells.isEmpty()) {
            int i;
            int i2;
            float cellDisplacement;
            PdfCell cell;
            float lostTableBottom = 0.0f;
            boolean cellsShown = false;
            int currentGroupNumber = 0;
            boolean headerChecked = false;
            ListIterator iterator = cells.listIterator();
            while (iterator.hasNext() && !tableHasToFit) {
                cell = (PdfCell)iterator.next();
                boolean atLeastOneFits = false;
                if (cellsHaveToFit) {
                    if (!cell.isHeader()) {
                        if (cell.getGroupNumber() != currentGroupNumber) {
                            boolean cellsFit = true;
                            currentGroupNumber = cell.getGroupNumber();
                            cellsHaveToFit = table.hasToFitPageCells();
                            int cellCount = 0;
                            while (cell.getGroupNumber() == currentGroupNumber && cellsFit && iterator.hasNext()) {
                                if (cell.bottom() < this.indentBottom()) {
                                    cellsFit = false;
                                } else {
                                    atLeastOneFits|=true;
                                }
                                cell = (PdfCell)iterator.next();
                                ++cellCount;
                            }
                            if (!atLeastOneFits) {
                                cellsHaveToFit = false;
                            }
                            if (!cellsFit) break;
                            for (int i3 = cellCount; i3 >= 0; --i3) {
                                cell = (PdfCell)iterator.previous();
                            }
                        }
                    } else if (!headerChecked) {
                        headerChecked = true;
                        boolean cellsFit = true;
                        int cellCount = 0;
                        float firstTop = cell.top();
                        while (cell.isHeader() && cellsFit && iterator.hasNext()) {
                            if (firstTop - cell.bottom(0.0f) > this.indentTop() - this.currentHeight - this.indentBottom()) {
                                cellsFit = false;
                            }
                            cell = (PdfCell)iterator.next();
                            ++cellCount;
                        }
                        currentGroupNumber = cell.getGroupNumber();
                        while (cell.getGroupNumber() == currentGroupNumber && cellsFit && iterator.hasNext()) {
                            if ((double)(firstTop - cell.bottom(0.0f)) > (double)(this.indentTop() - this.currentHeight - this.indentBottom()) - 10.0) {
                                cellsFit = false;
                            }
                            cell = (PdfCell)iterator.next();
                            ++cellCount;
                        }
                        for (i2 = cellCount; i2 >= 0; --i2) {
                            cell = (PdfCell)iterator.previous();
                        }
                        if (!cellsFit) {
                            while (cell.isHeader()) {
                                iterator.remove();
                                cell = (PdfCell)iterator.next();
                            }
                            break;
                        }
                    }
                }
                this.lines = cell.getLines(pagetop, this.indentBottom());
                if (this.lines != null && this.lines.size() > 0) {
                    cellsShown = true;
                    cellGraphics.rectangle(cell.rectangle(pagetop, this.indentBottom()));
                    lostTableBottom = Math.max(cell.bottom(), this.indentBottom());
                    float cellTop = cell.top(pagetop - oldHeight);
                    this.text.moveText(0.0f, cellTop);
                    cellDisplacement = this.flushLines() - cellTop;
                    this.text.moveText(0.0f, cellDisplacement);
                    if (oldHeight + cellDisplacement > this.currentHeight) {
                        this.currentHeight = oldHeight + cellDisplacement;
                    }
                }
                ArrayList images = cell.getImages(pagetop, this.indentBottom());
                Iterator i4 = images.iterator();
                while (i4.hasNext()) {
                    cellsShown = true;
                    Image image = (Image)i4.next();
                    this.addImage(this.graphics, image, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
                }
                if (!cell.mayBeRemoved()) continue;
                iterator.remove();
            }
            tableHasToFit = false;
            if (cellsShown) {
                Rectangle tablerec = new Rectangle(table);
                tablerec.setBorder(table.border());
                tablerec.setBorderWidth(table.borderWidth());
                tablerec.setBorderColor(table.borderColor());
                tablerec.setBackgroundColor(table.backgroundColor());
                tablerec.setGrayFill(table.grayFill());
                PdfContentByte under = this.writer.getDirectContentUnder();
                under.rectangle(tablerec.rectangle(this.top(), this.indentBottom()));
                under.add(cellGraphics);
                tablerec.setGrayFill(0.0f);
                tablerec.setBackgroundColor(null);
                under.rectangle(tablerec.rectangle(this.top(), this.indentBottom()));
            }
            cellGraphics = new PdfContentByte(null);
            if (cells.isEmpty()) continue;
            this.graphics.setLineWidth(table.borderWidth());
            if (cellsShown && (table.border() & 2) == 2) {
                Color tColor = table.borderColor();
                if (tColor != null) {
                    this.graphics.setColorStroke(tColor);
                }
                this.graphics.moveTo(table.left(), Math.max(table.bottom(), this.indentBottom()));
                this.graphics.lineTo(table.right(), Math.max(table.bottom(), this.indentBottom()));
                this.graphics.stroke();
                if (tColor != null) {
                    this.graphics.resetRGBColorStroke();
                }
            }
            this.pageEmpty = false;
            float difference = lostTableBottom;
            this.newPage();
            float heightCorrection = 0.0f;
            boolean somethingAdded = false;
            if (this.currentHeight > 0.0f) {
                heightCorrection = 6.0f;
                this.currentHeight+=heightCorrection;
                somethingAdded = true;
                this.newLine();
                this.flushLines();
                this.indentTop = this.currentHeight - this.leading;
                this.currentHeight = 0.0f;
            } else {
                this.flushLines();
            }
            int size = headercells.size();
            if (size > 0) {
                cell = (PdfCell)headercells.get(0);
                float oldTop = cell.top(0.0f);
                for (i2 = 0; i2 < size; ++i2) {
                    cell = (PdfCell)headercells.get(i2);
                    cell.setTop(this.indentTop() - oldTop + cell.top(0.0f));
                    cell.setBottom(this.indentTop() - oldTop + cell.bottom(0.0f));
                    pagetop = cell.bottom();
                    cellGraphics.rectangle(cell.rectangle(this.indentTop(), this.indentBottom()));
                    ArrayList images = cell.getImages(this.indentTop(), this.indentBottom());
                    Iterator im = images.iterator();
                    while (im.hasNext()) {
                        cellsShown = true;
                        Image image = (Image)im.next();
                        this.addImage(this.graphics, image, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
                    }
                    this.lines = cell.getLines(this.indentTop(), this.indentBottom());
                    float cellTop = cell.top(this.indentTop());
                    this.text.moveText(0.0f, cellTop - heightCorrection);
                    cellDisplacement = this.flushLines() - cellTop + heightCorrection;
                    this.text.moveText(0.0f, cellDisplacement);
                }
                this.currentHeight = this.indentTop() - pagetop + table.cellspacing();
                this.text.moveText(0.0f, pagetop - this.indentTop() - this.currentHeight);
            } else if (somethingAdded) {
                pagetop = this.indentTop();
                this.text.moveText(0.0f, - table.cellspacing());
            }
            oldHeight = this.currentHeight - heightCorrection;
            size = Math.min(cells.size(), table.columns());
            for (i = 0; i < size; ++i) {
                float newBottom;
                float neededHeight;
                cell = (PdfCell)cells.get(i);
                if (cell.top(- table.cellspacing()) <= lostTableBottom || (newBottom = pagetop - difference + cell.bottom()) <= pagetop - (neededHeight = cell.remainingHeight())) continue;
                difference+=newBottom - (pagetop - neededHeight);
            }
            size = cells.size();
            table.setTop(this.indentTop());
            table.setBottom(pagetop - difference + table.bottom(table.cellspacing()));
            for (i = 0; i < size; ++i) {
                cell = (PdfCell)cells.get(i);
                float newBottom = pagetop - difference + cell.bottom();
                float newTop = pagetop - difference + cell.top(- table.cellspacing());
                if (newTop > this.indentTop() - this.currentHeight) {
                    newTop = this.indentTop() - this.currentHeight;
                }
                cell.setTop(newTop);
                cell.setBottom(newBottom);
            }
            if (onlyFirstPage) break;
        }
        float tableHeight = table.top() - table.bottom();
        this.currentHeight = oldHeight + tableHeight;
        this.text.moveText(0.0f, - tableHeight);
        this.pageEmpty = false;
    }

    public boolean add(Element element) throws DocumentException {
        if (this.writer != null && this.writer.isPaused()) {
            return false;
        }
        try {
            switch (element.type()) {
                case 0: {
                    this.info.addkey(((Meta)element).name(), ((Meta)element).content());
                    break;
                }
                case 1: {
                    this.info.addTitle(((Meta)element).content());
                    break;
                }
                case 2: {
                    this.info.addSubject(((Meta)element).content());
                    break;
                }
                case 3: {
                    this.info.addKeywords(((Meta)element).content());
                    break;
                }
                case 4: {
                    this.info.addAuthor(((Meta)element).content());
                    break;
                }
                case 7: {
                    this.info.addCreator(((Meta)element).content());
                    break;
                }
                case 5: {
                    this.info.addProducer();
                    break;
                }
                case 6: {
                    this.info.addCreationDate();
                    break;
                }
                case 10: {
                    PdfChunk overflow;
                    if (this.line == null) {
                        this.carriageReturn();
                    }
                    PdfChunk chunk = new PdfChunk((Chunk)element, this.currentAction);
                    while ((overflow = this.line.add(chunk)) != null) {
                        this.carriageReturn();
                        chunk = overflow;
                    }
                    this.pageEmpty = false;
                    if (!chunk.isAttribute("NEWPAGE")) break;
                    this.newPage();
                    break;
                }
                case 17: {
                    Anchor anchor = (Anchor)element;
                    String url = anchor.reference();
                    this.leading = anchor.leading();
                    if (url != null) {
                        this.currentAction = new PdfAction(url);
                    }
                    element.process(this);
                    this.currentAction = null;
                    break;
                }
                case 29: {
                    if (this.line == null) {
                        this.carriageReturn();
                    }
                    Annotation annot = (Annotation)element;
                    switch (annot.annotationType()) {
                        case 1: {
                            this.annotations.add(new PdfAnnotation(this.writer, annot.llx(), annot.lly(), annot.urx(), annot.ury(), new PdfAction((URL)annot.attributes().get(Annotation.URL))));
                            break;
                        }
                        case 2: {
                            this.annotations.add(new PdfAnnotation(this.writer, annot.llx(), annot.lly(), annot.urx(), annot.ury(), new PdfAction((String)annot.attributes().get(Annotation.FILE))));
                            break;
                        }
                        case 3: {
                            this.annotations.add(new PdfAnnotation(this.writer, annot.llx(), annot.lly(), annot.urx(), annot.ury(), new PdfAction((String)annot.attributes().get(Annotation.FILE), (String)annot.attributes().get(Annotation.DESTINATION))));
                            break;
                        }
                        case 7: {
                            boolean[] sparams = (boolean[])annot.attributes().get(Annotation.PARAMETERS);
                            String fname = (String)annot.attributes().get(Annotation.FILE);
                            String mimetype = (String)annot.attributes().get(Annotation.MIMETYPE);
                            PdfFileSpecification fs = sparams[0] ? PdfFileSpecification.fileEmbedded(this.writer, fname, fname, null) : PdfFileSpecification.fileExtern(this.writer, fname);
                            PdfAnnotation ann = PdfAnnotation.createScreen(this.writer, new Rectangle(annot.llx(), annot.lly(), annot.urx(), annot.ury()), fname, fs, mimetype, sparams[1]);
                            this.annotations.add(ann);
                            break;
                        }
                        case 4: {
                            this.annotations.add(new PdfAnnotation(this.writer, annot.llx(), annot.lly(), annot.urx(), annot.ury(), new PdfAction((String)annot.attributes().get(Annotation.FILE), (Integer)annot.attributes().get(Annotation.PAGE))));
                            break;
                        }
                        case 5: {
                            this.annotations.add(new PdfAnnotation(this.writer, annot.llx(), annot.lly(), annot.urx(), annot.ury(), new PdfAction((Integer)annot.attributes().get(Annotation.NAMED))));
                            break;
                        }
                        case 6: {
                            this.annotations.add(new PdfAnnotation(this.writer, annot.llx(), annot.lly(), annot.urx(), annot.ury(), new PdfAction((String)annot.attributes().get(Annotation.APPLICATION), (String)annot.attributes().get(Annotation.PARAMETERS), (String)annot.attributes().get(Annotation.OPERATION), (String)annot.attributes().get(Annotation.DEFAULTDIR))));
                            break;
                        }
                        default: {
                            PdfAnnotation an = new PdfAnnotation(this.writer, annot.llx(this.indentRight() - this.line.widthLeft()), annot.lly(this.indentTop() - this.currentHeight), annot.urx(this.indentRight() - this.line.widthLeft() + 20.0f), annot.ury(this.indentTop() - this.currentHeight - 20.0f), new PdfString(annot.title()), new PdfString(annot.content()));
                            this.annotations.add(an);
                        }
                    }
                    this.pageEmpty = false;
                    break;
                }
                case 11: {
                    this.leading = ((Phrase)element).leading();
                    element.process(this);
                    break;
                }
                case 12: {
                    Paragraph paragraph = (Paragraph)element;
                    float spacingBefore = paragraph.spacingBefore();
                    if (spacingBefore != 0.0f) {
                        this.leading = spacingBefore;
                        this.carriageReturn();
                        if (!this.pageEmpty) {
                            Chunk space = new Chunk(" ");
                            space.process(this);
                            this.carriageReturn();
                        }
                    }
                    this.alignment = paragraph.alignment();
                    this.leading = paragraph.leading();
                    this.carriageReturn();
                    if (this.currentHeight + this.line.height() + this.leading > this.indentTop() - this.indentBottom()) {
                        this.newPage();
                    }
                    this.indentLeft+=paragraph.indentationLeft();
                    this.indentRight+=paragraph.indentationRight();
                    this.carriageReturn();
                    this.paraIndent+=paragraph.indentationLeft();
                    PdfPageEvent pageEvent = this.writer.getPageEvent();
                    if (pageEvent != null && this.isParagraph) {
                        pageEvent.onParagraph(this.writer, this, this.indentTop() - this.currentHeight);
                    }
                    if (paragraph.getKeepTogether()) {
                        Table table = new Table(1, 1);
                        table.setOffset(0.0f);
                        table.setBorder(0);
                        table.setWidth(100.0f);
                        table.setTableFitsPage(true);
                        Cell cell = new Cell(paragraph);
                        cell.setBorder(0);
                        cell.setHorizontalAlignment(paragraph.alignment());
                        table.addCell(cell);
                        this.add(table);
                        break;
                    }
                    element.process(this);
                    this.paraIndent-=paragraph.indentationLeft();
                    float spacingAfter = paragraph.spacingAfter();
                    if (spacingAfter != 0.0f) {
                        this.leading = spacingAfter;
                        this.carriageReturn();
                        if (this.currentHeight + this.line.height() + this.leading < this.indentTop() - this.indentBottom()) {
                            Chunk space = new Chunk(" ");
                            space.process(this);
                            this.carriageReturn();
                        }
                        this.leading = paragraph.leading();
                    }
                    if (pageEvent != null && this.isParagraph) {
                        pageEvent.onParagraphEnd(this.writer, this, this.indentTop() - this.currentHeight);
                    }
                    this.alignment = 0;
                    this.indentLeft-=paragraph.indentationLeft();
                    this.indentRight-=paragraph.indentationRight();
                    this.carriageReturn();
                    break;
                }
                case 13: 
                case 16: {
                    boolean hasTitle;
                    Section section = (Section)element;
                    boolean bl = hasTitle = section.title() != null;
                    if (section.isChapter()) {
                        this.newPage();
                    } else {
                        this.newLine();
                    }
                    if (hasTitle) {
                        PdfOutline outline;
                        float fith = this.indentTop() - this.currentHeight;
                        int rotation = this.pageSize.getRotation();
                        if (rotation == 90 || rotation == 180) {
                            fith = this.pageSize.height() - fith;
                        }
                        PdfDestination destination = new PdfDestination(2, fith);
                        while (this.currentOutline.level() >= section.depth()) {
                            this.currentOutline = this.currentOutline.parent();
                        }
                        this.currentOutline = outline = new PdfOutline(this.currentOutline, destination, section.getBookmarkTitle(), section.isBookmarkOpen());
                    }
                    this.carriageReturn();
                    this.indentLeft+=section.indentationLeft();
                    this.indentRight+=section.indentationRight();
                    PdfPageEvent pageEvent = this.writer.getPageEvent();
                    if (pageEvent != null) {
                        if (element.type() == 16) {
                            pageEvent.onChapter(this.writer, this, this.indentTop() - this.currentHeight, section.title());
                        } else {
                            pageEvent.onSection(this.writer, this, this.indentTop() - this.currentHeight, section.depth(), section.title());
                        }
                    }
                    if (hasTitle) {
                        this.isParagraph = false;
                        this.add(section.title());
                        this.isParagraph = true;
                    }
                    this.indentLeft+=section.indentation();
                    element.process(this);
                    this.indentLeft-=section.indentationLeft() + section.indentation();
                    this.indentRight-=section.indentationRight();
                    if (pageEvent == null) break;
                    if (element.type() == 16) {
                        pageEvent.onChapterEnd(this.writer, this, this.indentTop() - this.currentHeight);
                        break;
                    }
                    pageEvent.onSectionEnd(this.writer, this, this.indentTop() - this.currentHeight);
                    break;
                }
                case 14: {
                    List list = (List)element;
                    this.listIndentLeft+=list.indentationLeft();
                    this.indentRight+=list.indentationRight();
                    element.process(this);
                    this.listIndentLeft-=list.indentationLeft();
                    this.indentRight-=list.indentationRight();
                    break;
                }
                case 15: {
                    ListItem listItem = (ListItem)element;
                    float spacingBefore = listItem.spacingBefore();
                    if (spacingBefore != 0.0f) {
                        this.leading = spacingBefore;
                        this.carriageReturn();
                        if (!this.pageEmpty) {
                            Chunk space = new Chunk(" ");
                            space.process(this);
                            this.carriageReturn();
                        }
                    }
                    this.alignment = listItem.alignment();
                    this.listIndentLeft+=listItem.indentationLeft();
                    this.indentRight+=listItem.indentationRight();
                    this.leading = listItem.leading();
                    this.carriageReturn();
                    this.line.setListItem(listItem);
                    element.process(this);
                    float spacingAfter = listItem.spacingAfter();
                    if (spacingAfter != 0.0f) {
                        this.leading = spacingAfter;
                        this.carriageReturn();
                        if (this.currentHeight + this.line.height() + this.leading < this.indentTop() - this.indentBottom()) {
                            Chunk space = new Chunk(" ");
                            space.process(this);
                            this.carriageReturn();
                        }
                        this.leading = listItem.leading();
                    }
                    this.carriageReturn();
                    this.listIndentLeft-=listItem.indentationLeft();
                    this.indentRight-=listItem.indentationRight();
                    break;
                }
                case 30: {
                    Rectangle rectangle = (Rectangle)element;
                    this.graphics.rectangle(rectangle);
                    this.pageEmpty = false;
                    break;
                }
                case 23: {
                    PdfPTable ptable = (PdfPTable)element;
                    if (ptable.size() <= ptable.getHeaderRows()) break;
                    this.ensureNewLine();
                    this.flushLines();
                    this.addPTable(ptable);
                    this.pageEmpty = false;
                    break;
                }
                case 40: {
                    this.ensureNewLine();
                    this.flushLines();
                    MultiColumnText multiText = (MultiColumnText)element;
                    float height = multiText.write(this.writer.getDirectContent(), this, this.indentTop() - this.currentHeight);
                    this.currentHeight+=height;
                    this.text.moveText(0.0f, -1.0f * height);
                    this.pageEmpty = false;
                    break;
                }
                case 22: {
                    PdfTable table;
                    if (element instanceof PdfTable) {
                        table = (PdfTable)element;
                        table.updateRowAdditions();
                    } else {
                        if (element instanceof SimpleTable) {
                            PdfPTable ptable = ((SimpleTable)element).createPdfPTable();
                            if (ptable.size() <= ptable.getHeaderRows()) break;
                            this.ensureNewLine();
                            this.flushLines();
                            this.addPTable(ptable);
                            this.pageEmpty = false;
                            break;
                        }
                        if (element instanceof Table) {
                            try {
                                PdfPTable ptable = ((Table)element).createPdfPTable();
                                if (ptable.size() <= ptable.getHeaderRows()) break;
                                this.ensureNewLine();
                                this.flushLines();
                                this.addPTable(ptable);
                                this.pageEmpty = false;
                                break;
                            }
                            catch (BadElementException bee) {
                                float offset = ((Table)element).getOffset();
                                if (Float.isNaN(offset)) {
                                    offset = this.leading;
                                }
                                this.carriageReturn();
                                this.lines.add(new PdfLine(this.indentLeft(), this.indentRight(), this.alignment, offset));
                                this.currentHeight+=offset;
                                table = this.getPdfTable((Table)element, false);
                            }
                        } else {
                            return false;
                        }
                    }
                    this.add(table, false);
                    break;
                }
                case 32: 
                case 34: 
                case 35: {
                    this.add((Image)element);
                    break;
                }
                case 39: {
                    Graphic graphic = (Graphic)element;
                    graphic.processAttributes(this.indentLeft(), this.indentBottom(), this.indentRight(), this.indentTop(), this.indentTop() - this.currentHeight);
                    this.graphics.add(graphic);
                    this.pageEmpty = false;
                    break;
                }
                default: {
                    return false;
                }
            }
            this.lastElementType = element.type();
            return true;
        }
        catch (Exception e) {
            throw new DocumentException(e);
        }
    }

    private void addImage(PdfContentByte graphics, Image image, float a, float b, float c, float d, float e, float f) throws DocumentException {
        Annotation annotation = image.annotation();
        if (image.hasAbsolutePosition()) {
            graphics.addImage(image);
            if (annotation != null) {
                annotation.setDimensions(image.absoluteX(), image.absoluteY(), image.absoluteX() + image.scaledWidth(), image.absoluteY() + image.scaledHeight());
                this.add(annotation);
            }
        } else {
            graphics.addImage(image, a, b, c, d, e, f);
            if (annotation != null) {
                annotation.setDimensions(e, f, e + image.scaledWidth(), f + image.scaledHeight());
                this.add(annotation);
            }
        }
    }

    private void add(Image image) throws PdfException, DocumentException {
        if (image.hasAbsolutePosition()) {
            this.addImage(this.graphics, image, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
            this.pageEmpty = false;
            return;
        }
        if (this.currentHeight != 0.0f && this.indentTop() - this.currentHeight - image.scaledHeight() < this.indentBottom()) {
            if (!(this.strictImageSequence || this.imageWait != null)) {
                this.imageWait = image;
                return;
            }
            this.newPage();
            if (this.currentHeight != 0.0f && this.indentTop() - this.currentHeight - image.scaledHeight() < this.indentBottom()) {
                this.imageWait = image;
                return;
            }
        }
        this.pageEmpty = false;
        if (image == this.imageWait) {
            this.imageWait = null;
        }
        boolean textwrap = (image.alignment() & 4) == 4 && (image.alignment() & 1) != 1;
        boolean underlying = (image.alignment() & 8) == 8;
        float diff = this.leading / 2.0f;
        if (textwrap) {
            diff+=this.leading;
        }
        float lowerleft = this.indentTop() - this.currentHeight - image.scaledHeight() - diff;
        float[] mt = image.matrix();
        float startPosition = this.indentLeft() - mt[4];
        if ((image.alignment() & 2) == 2) {
            startPosition = this.indentRight() - image.scaledWidth() - mt[4];
        }
        if ((image.alignment() & 1) == 1) {
            startPosition = this.indentLeft() + (this.indentRight() - this.indentLeft() - image.scaledWidth()) / 2.0f - mt[4];
        }
        if (image.hasAbsoluteX()) {
            startPosition = image.absoluteX();
        }
        this.addImage(this.graphics, image, mt[0], mt[1], mt[2], mt[3], startPosition, lowerleft - mt[5]);
        if (textwrap) {
            if (this.imageEnd < 0.0f || this.imageEnd < this.currentHeight + image.scaledHeight() + diff) {
                this.imageEnd = this.currentHeight + image.scaledHeight() + diff;
            }
            if ((image.alignment() & 2) == 2) {
                this.imageIndentRight+=image.scaledWidth() + image.indentationLeft();
            } else {
                this.imageIndentLeft+=image.scaledWidth() + image.indentationRight();
            }
        }
        if (!(textwrap || underlying)) {
            this.currentHeight+=image.scaledHeight() + diff;
            this.flushLines();
            this.text.moveText(0.0f, - image.scaledHeight() + diff);
            this.newLine();
        }
    }

    private void initPage() throws DocumentException {
        float tmpImageIndentRight;
        float tmpListIndentLeft;
        float tmpIndentRight;
        float tmpImageIndentLeft;
        this.annotations = this.delayedAnnotations;
        this.delayedAnnotations = new ArrayList();
        this.pageResources = new PageResources();
        this.writer.resetContent();
        ++this.pageN;
        float oldleading = this.leading;
        int oldAlignment = this.alignment;
        if (this.marginMirroring && (this.getPageNumber() & 1) == 0) {
            this.marginRight = this.nextMarginLeft;
            this.marginLeft = this.nextMarginRight;
        } else {
            this.marginLeft = this.nextMarginLeft;
            this.marginRight = this.nextMarginRight;
        }
        this.marginTop = this.nextMarginTop;
        this.marginBottom = this.nextMarginBottom;
        this.imageEnd = -1.0f;
        this.imageIndentRight = 0.0f;
        this.imageIndentLeft = 0.0f;
        this.graphics = new PdfContentByte(this.writer);
        this.text = new PdfContentByte(this.writer);
        this.text.beginText();
        this.text.moveText(this.left(), this.top());
        this.textEmptySize = this.text.size();
        this.text.reset();
        this.text.beginText();
        this.leading = 16.0f;
        this.indentBottom = 0.0f;
        this.indentTop = 0.0f;
        this.currentHeight = 0.0f;
        this.pageSize = this.nextPageSize;
        this.thisBoxSize = new HashMap(this.boxSize);
        if (this.pageSize.backgroundColor() != null || this.pageSize.hasBorders() || this.pageSize.borderColor() != null || this.pageSize.grayFill() > 0.0f) {
            this.add(this.pageSize);
        }
        if (this.watermark != null) {
            float[] mt = this.watermark.matrix();
            this.addImage(this.graphics, this.watermark, mt[0], mt[1], mt[2], mt[3], this.watermark.offsetX() - mt[4], this.watermark.offsetY() - mt[5]);
        }
        if (this.footer != null) {
            float tmpIndentLeft = this.indentLeft;
            tmpIndentRight = this.indentRight;
            tmpListIndentLeft = this.listIndentLeft;
            tmpImageIndentLeft = this.imageIndentLeft;
            tmpImageIndentRight = this.imageIndentRight;
            this.indentRight = 0.0f;
            this.indentLeft = 0.0f;
            this.listIndentLeft = 0.0f;
            this.imageIndentLeft = 0.0f;
            this.imageIndentRight = 0.0f;
            this.footer.setPageNumber(this.pageN);
            this.leading = this.footer.paragraph().leading();
            this.add(this.footer.paragraph());
            this.indentBottom = this.currentHeight;
            this.text.moveText(this.left(), this.indentBottom());
            this.flushLines();
            this.text.moveText(- this.left(), - this.bottom());
            this.footer.setTop(this.bottom(this.currentHeight));
            this.footer.setBottom(this.bottom() - 0.75f * this.leading);
            this.footer.setLeft(this.left());
            this.footer.setRight(this.right());
            this.graphics.rectangle(this.footer);
            this.indentBottom = this.currentHeight + this.leading * 2.0f;
            this.currentHeight = 0.0f;
            this.indentLeft = tmpIndentLeft;
            this.indentRight = tmpIndentRight;
            this.listIndentLeft = tmpListIndentLeft;
            this.imageIndentLeft = tmpImageIndentLeft;
            this.imageIndentRight = tmpImageIndentRight;
        }
        this.text.moveText(this.left(), this.top());
        if (this.header != null) {
            float tmpIndentLeft = this.indentLeft;
            tmpIndentRight = this.indentRight;
            tmpListIndentLeft = this.listIndentLeft;
            tmpImageIndentLeft = this.imageIndentLeft;
            tmpImageIndentRight = this.imageIndentRight;
            this.indentRight = 0.0f;
            this.indentLeft = 0.0f;
            this.listIndentLeft = 0.0f;
            this.imageIndentLeft = 0.0f;
            this.imageIndentRight = 0.0f;
            this.header.setPageNumber(this.pageN);
            this.leading = this.header.paragraph().leading();
            this.text.moveText(0.0f, this.leading);
            this.add(this.header.paragraph());
            this.newLine();
            this.indentTop = this.currentHeight - this.leading;
            this.header.setTop(this.top() + this.leading);
            this.header.setBottom(this.indentTop() + this.leading * 2.0f / 3.0f);
            this.header.setLeft(this.left());
            this.header.setRight(this.right());
            this.graphics.rectangle(this.header);
            this.flushLines();
            this.currentHeight = 0.0f;
            this.indentLeft = tmpIndentLeft;
            this.indentRight = tmpIndentRight;
            this.listIndentLeft = tmpListIndentLeft;
            this.imageIndentLeft = tmpImageIndentLeft;
            this.imageIndentRight = tmpImageIndentRight;
        }
        this.pageEmpty = true;
        try {
            if (this.imageWait != null) {
                this.add(this.imageWait);
                this.imageWait = null;
            }
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
        this.leading = oldleading;
        this.alignment = oldAlignment;
        this.carriageReturn();
        PdfPageEvent pageEvent = this.writer.getPageEvent();
        if (pageEvent != null) {
            if (this.firstPageEvent) {
                pageEvent.onOpenDocument(this.writer, this);
            }
            pageEvent.onStartPage(this.writer, this);
        }
        this.firstPageEvent = false;
    }

    private void carriageReturn() throws DocumentException {
        if (this.lines == null) {
            this.lines = new ArrayList();
        }
        if (this.line != null) {
            if (this.currentHeight + this.line.height() + this.leading < this.indentTop() - this.indentBottom()) {
                if (this.line.size() > 0) {
                    this.currentHeight+=this.line.height();
                    this.lines.add(this.line);
                    this.pageEmpty = false;
                }
            } else {
                this.newPage();
            }
        }
        if (this.imageEnd > -1.0f && this.currentHeight > this.imageEnd) {
            this.imageEnd = -1.0f;
            this.imageIndentRight = 0.0f;
            this.imageIndentLeft = 0.0f;
        }
        this.line = new PdfLine(this.indentLeft(), this.indentRight(), this.alignment, this.leading);
    }

    private void newLine() throws DocumentException {
        this.lastElementType = -1;
        this.carriageReturn();
        if (this.lines != null && this.lines.size() > 0) {
            this.lines.add(this.line);
            this.currentHeight+=this.line.height();
        }
        this.line = new PdfLine(this.indentLeft(), this.indentRight(), this.alignment, this.leading);
    }

    private float flushLines() throws DocumentException {
        Float lastBaseFactor;
        if (this.lines == null) {
            return 0.0f;
        }
        boolean newline = false;
        if (this.line != null && this.line.size() > 0) {
            this.lines.add(this.line);
            this.line = new PdfLine(this.indentLeft(), this.indentRight(), this.alignment, this.leading);
            newline = true;
        }
        if (this.lines.size() == 0) {
            return 0.0f;
        }
        Object[] currentValues = new Object[2];
        PdfFont currentFont = null;
        float displacement = 0.0f;
        currentValues[1] = lastBaseFactor = new Float(0.0f);
        Iterator i = this.lines.iterator();
        while (i.hasNext()) {
            PdfLine l = (PdfLine)i.next();
            if (this.isNewpage && newline) {
                newline = false;
                this.text.moveText(l.indentLeft() - this.indentLeft() + this.listIndentLeft + this.paraIndent, - l.height());
            } else {
                this.text.moveText(l.indentLeft() - this.indentLeft() + this.listIndentLeft, - l.height());
            }
            if (l.listSymbol() != null) {
                PdfChunk chunk = l.listSymbol();
                this.text.moveText(- l.listIndent(), 0.0f);
                if (chunk.font().compareTo(currentFont) != 0) {
                    currentFont = chunk.font();
                    this.text.setFontAndSize(currentFont.getFont(), currentFont.size());
                }
                if (chunk.color() != null) {
                    Color color = chunk.color();
                    this.text.setColorFill(color);
                    this.text.showText(chunk.toString());
                    this.text.resetRGBColorFill();
                } else if (chunk.isImage()) {
                    Image image = chunk.getImage();
                    float[] matrix = image.matrix();
                    float xMarker = this.text.getXTLM();
                    float yMarker = this.text.getYTLM();
                    matrix[4] = xMarker + chunk.getImageOffsetX() - matrix[4];
                    matrix[5] = yMarker + chunk.getImageOffsetY() - matrix[5];
                    this.addImage(this.graphics, image, matrix[0], matrix[1], matrix[2], matrix[3], matrix[4], matrix[5]);
                } else {
                    this.text.showText(chunk.toString());
                }
                this.text.moveText(l.listIndent(), 0.0f);
            }
            currentValues[0] = currentFont;
            this.writeLineToContent(l, this.text, this.graphics, currentValues, this.writer.getSpaceCharRatio());
            currentFont = (PdfFont)currentValues[0];
            displacement+=l.height();
            if (this.indentLeft() - this.listIndentLeft == l.indentLeft()) continue;
            this.text.moveText(this.indentLeft() - l.indentLeft() - this.listIndentLeft, 0.0f);
        }
        this.lines = new ArrayList();
        return displacement;
    }

    PdfInfo getInfo() {
        return this.info;
    }

    PdfCatalog getCatalog(PdfIndirectReference pages) {
        PdfCatalog catalog = this.rootOutline.getKids().size() > 0 ? new PdfCatalog(pages, this.rootOutline.indirectReference(), this.writer) : new PdfCatalog(pages, this.writer);
        if (this.openActionName != null) {
            PdfAction action = this.getLocalGotoAction(this.openActionName);
            catalog.setOpenAction(action);
        } else if (this.openActionAction != null) {
            catalog.setOpenAction(this.openActionAction);
        }
        if (this.additionalActions != null) {
            catalog.setAdditionalActions(this.additionalActions);
        }
        if (this.pageLabels != null) {
            catalog.setPageLabels(this.pageLabels);
        }
        catalog.addNames(this.localDestinations, this.documentJavaScript, this.writer);
        catalog.setViewerPreferences(this.viewerPreferences);
        if (this.acroForm.isValid()) {
            try {
                catalog.setAcroForm(this.writer.addToBody(this.acroForm).getIndirectReference());
            }
            catch (IOException e) {
                throw new ExceptionConverter(e);
            }
        }
        return catalog;
    }

    float bottom(Table table) {
        float h = this.currentHeight > 0.0f ? this.indentTop() - this.currentHeight - 2.0f * this.leading : this.indentTop();
        PdfTable tmp = this.getPdfTable(table, false);
        return tmp.bottom();
    }

    boolean fitsPage(PdfPTable table, float margin) {
        if (!table.isLockedWidth()) {
            float totalWidth = (this.indentRight() - this.indentLeft()) * table.getWidthPercentage() / 100.0f;
            table.setTotalWidth(totalWidth);
        }
        this.ensureNewLine();
        if (table.getTotalHeight() <= this.indentTop() - this.currentHeight - this.indentBottom() - margin) {
            return true;
        }
        return false;
    }

    public float getVerticalPosition(boolean ensureNewLine) {
        if (ensureNewLine) {
            this.ensureNewLine();
        }
        return this.top() - this.currentHeight - this.indentTop;
    }

    private void ensureNewLine() {
        try {
            if (this.lastElementType == 11 || this.lastElementType == 10) {
                this.newLine();
                this.flushLines();
            }
        }
        catch (DocumentException ex) {
            throw new ExceptionConverter(ex);
        }
    }

    private float indentLeft() {
        return this.left(this.indentLeft + this.listIndentLeft + this.imageIndentLeft);
    }

    private float indentRight() {
        return this.right(this.indentRight + this.imageIndentRight);
    }

    private float indentTop() {
        return this.top(this.indentTop);
    }

    float indentBottom() {
        return this.bottom(this.indentBottom);
    }

    void addOutline(PdfOutline outline, String name) {
        this.localDestination(name, outline.getPdfDestination());
    }

    public PdfAcroForm getAcroForm() {
        return this.acroForm;
    }

    public PdfOutline getRootOutline() {
        return this.rootOutline;
    }

    void writeLineToContent(PdfLine line, PdfContentByte text, PdfContentByte graphics, Object[] currentValues, float ratio) throws DocumentException {
        boolean isJustified;
        float xMarker;
        PdfFont currentFont = (PdfFont)currentValues[0];
        float lastBaseFactor = ((Float)currentValues[1]).floatValue();
        float hangingCorrection = 0.0f;
        float hScale = 1.0f;
        float lastHScale = NaNf;
        float baseWordSpacing = 0.0f;
        float baseCharacterSpacing = 0.0f;
        int numberOfSpaces = line.numberOfSpaces();
        int lineLen = line.toString().length();
        boolean bl = isJustified = line.hasToBeJustified() && (numberOfSpaces != 0 || lineLen > 1);
        if (isJustified) {
            if (line.isNewlineSplit() && line.widthLeft() >= lastBaseFactor * (ratio * (float)numberOfSpaces + (float)lineLen - 1.0f)) {
                if (line.isRTL()) {
                    text.moveText(line.widthLeft() - lastBaseFactor * (ratio * (float)numberOfSpaces + (float)lineLen - 1.0f), 0.0f);
                }
                baseWordSpacing = ratio * lastBaseFactor;
                baseCharacterSpacing = lastBaseFactor;
            } else {
                String s;
                char c;
                float width = line.widthLeft();
                PdfChunk last = line.getChunk(line.size() - 1);
                if (last != null && (s = last.toString()).length() > 0 && ".,;:'".indexOf(c = s.charAt(s.length() - 1)) >= 0) {
                    float oldWidth = width;
                    hangingCorrection = (width+=last.font().width(c) * 0.4f) - oldWidth;
                }
                float baseFactor = width / (ratio * (float)numberOfSpaces + (float)lineLen - 1.0f);
                baseWordSpacing = ratio * baseFactor;
                baseCharacterSpacing = baseFactor;
                lastBaseFactor = baseFactor;
            }
        }
        int lastChunkStroke = line.getLastStrokeChunk();
        int chunkStrokeIdx = 0;
        float baseXMarker = xMarker = text.getXTLM();
        float yMarker = text.getYTLM();
        boolean adjustMatrix = false;
        Iterator j = line.iterator();
        while (j.hasNext()) {
            PdfChunk chunk = (PdfChunk)j.next();
            Color color = chunk.color();
            hScale = 1.0f;
            if (chunkStrokeIdx <= lastChunkStroke) {
                float width = isJustified ? chunk.getWidthCorrected(baseCharacterSpacing, baseWordSpacing) : chunk.width();
                if (chunk.isStroked()) {
                    float descender;
                    float ascender;
                    float subtract;
                    PdfChunk nextChunk = line.getChunk(chunkStrokeIdx + 1);
                    if (chunk.isAttribute("BACKGROUND")) {
                        subtract = lastBaseFactor;
                        if (nextChunk != null && nextChunk.isAttribute("BACKGROUND")) {
                            subtract = 0.0f;
                        }
                        if (nextChunk == null) {
                            subtract+=hangingCorrection;
                        }
                        float fontSize = chunk.font().size();
                        ascender = chunk.font().getFont().getFontDescriptor(1, fontSize);
                        descender = chunk.font().getFont().getFontDescriptor(3, fontSize);
                        Object[] bgr = (Object[])chunk.getAttribute("BACKGROUND");
                        graphics.setColorFill((Color)bgr[0]);
                        float[] extra = (float[])bgr[1];
                        graphics.rectangle(xMarker - extra[0], yMarker + descender - extra[1] + chunk.getTextRise(), width - subtract + extra[0] + extra[2], ascender - descender + extra[1] + extra[3]);
                        graphics.fill();
                        graphics.setGrayFill(0.0f);
                    }
                    if (chunk.isAttribute("UNDERLINE")) {
                        subtract = lastBaseFactor;
                        if (nextChunk != null && nextChunk.isAttribute("UNDERLINE")) {
                            subtract = 0.0f;
                        }
                        if (nextChunk == null) {
                            subtract+=hangingCorrection;
                        }
                        Object[][] unders = (Object[][])chunk.getAttribute("UNDERLINE");
                        Color scolor = null;
                        boolean cap = false;
                        for (int k = 0; k < unders.length; ++k) {
                            Object[] obj = unders[k];
                            scolor = (Color)obj[0];
                            float[] ps = (float[])obj[1];
                            if (scolor == null) {
                                scolor = color;
                            }
                            if (scolor != null) {
                                graphics.setColorStroke(scolor);
                            }
                            float fsize = chunk.font().size();
                            graphics.setLineWidth(ps[0] + fsize * ps[1]);
                            float shift = ps[2] + fsize * ps[3];
                            int cap2 = (int)ps[4];
                            if (cap2 != 0) {
                                graphics.setLineCap(cap2);
                            }
                            graphics.moveTo(xMarker, yMarker + shift);
                            graphics.lineTo(xMarker + width - subtract, yMarker + shift);
                            graphics.stroke();
                            if (scolor != null) {
                                graphics.resetGrayStroke();
                            }
                            if (cap2 == 0) continue;
                            graphics.setLineCap(0);
                        }
                        graphics.setLineWidth(1.0f);
                    }
                    if (chunk.isAttribute("ACTION")) {
                        subtract = lastBaseFactor;
                        if (nextChunk != null && nextChunk.isAttribute("ACTION")) {
                            subtract = 0.0f;
                        }
                        if (nextChunk == null) {
                            subtract+=hangingCorrection;
                        }
                        text.addAnnotation(new PdfAnnotation(this.writer, xMarker, yMarker, xMarker + width - subtract, yMarker + chunk.font().size(), (PdfAction)chunk.getAttribute("ACTION")));
                    }
                    if (chunk.isAttribute("REMOTEGOTO")) {
                        subtract = lastBaseFactor;
                        if (nextChunk != null && nextChunk.isAttribute("REMOTEGOTO")) {
                            subtract = 0.0f;
                        }
                        if (nextChunk == null) {
                            subtract+=hangingCorrection;
                        }
                        Object[] obj = (Object[])chunk.getAttribute("REMOTEGOTO");
                        String filename = (String)obj[0];
                        if (obj[1] instanceof String) {
                            this.remoteGoto(filename, (String)obj[1], xMarker, yMarker, xMarker + width - subtract, yMarker + chunk.font().size());
                        } else {
                            this.remoteGoto(filename, (Integer)obj[1], xMarker, yMarker, xMarker + width - subtract, yMarker + chunk.font().size());
                        }
                    }
                    if (chunk.isAttribute("LOCALGOTO")) {
                        subtract = lastBaseFactor;
                        if (nextChunk != null && nextChunk.isAttribute("LOCALGOTO")) {
                            subtract = 0.0f;
                        }
                        if (nextChunk == null) {
                            subtract+=hangingCorrection;
                        }
                        this.localGoto((String)chunk.getAttribute("LOCALGOTO"), xMarker, yMarker, xMarker + width - subtract, yMarker + chunk.font().size());
                    }
                    if (chunk.isAttribute("LOCALDESTINATION")) {
                        subtract = lastBaseFactor;
                        if (nextChunk != null && nextChunk.isAttribute("LOCALDESTINATION")) {
                            subtract = 0.0f;
                        }
                        if (nextChunk == null) {
                            subtract+=hangingCorrection;
                        }
                        this.localDestination((String)chunk.getAttribute("LOCALDESTINATION"), new PdfDestination(0, xMarker, yMarker + chunk.font().size(), 0.0f));
                    }
                    if (chunk.isAttribute("GENERICTAG")) {
                        subtract = lastBaseFactor;
                        if (nextChunk != null && nextChunk.isAttribute("GENERICTAG")) {
                            subtract = 0.0f;
                        }
                        if (nextChunk == null) {
                            subtract+=hangingCorrection;
                        }
                        Rectangle rect = new Rectangle(xMarker, yMarker, xMarker + width - subtract, yMarker + chunk.font().size());
                        PdfPageEvent pev = this.writer.getPageEvent();
                        if (pev != null) {
                            pev.onGenericTag(this.writer, this, rect, (String)chunk.getAttribute("GENERICTAG"));
                        }
                    }
                    if (chunk.isAttribute("PDFANNOTATION")) {
                        subtract = lastBaseFactor;
                        if (nextChunk != null && nextChunk.isAttribute("PDFANNOTATION")) {
                            subtract = 0.0f;
                        }
                        if (nextChunk == null) {
                            subtract+=hangingCorrection;
                        }
                        float fontSize = chunk.font().size();
                        ascender = chunk.font().getFont().getFontDescriptor(1, fontSize);
                        descender = chunk.font().getFont().getFontDescriptor(3, fontSize);
                        PdfAnnotation annot = PdfFormField.shallowDuplicate((PdfAnnotation)chunk.getAttribute("PDFANNOTATION"));
                        annot.put(PdfName.RECT, new PdfRectangle(xMarker, yMarker + descender, xMarker + width - subtract, yMarker + ascender));
                        text.addAnnotation(annot);
                    }
                    float[] params = (float[])chunk.getAttribute("SKEW");
                    Float hs = (Float)chunk.getAttribute("HSCALE");
                    if (params != null || hs != null) {
                        float a = 1.0f;
                        float b = 0.0f;
                        float c = 0.0f;
                        if (params != null) {
                            b = params[0];
                            c = params[1];
                        }
                        if (hs != null) {
                            hScale = hs.floatValue();
                        }
                        text.setTextMatrix(hScale, b, c, 1.0f, xMarker, yMarker);
                    }
                    if (chunk.isImage()) {
                        Image image = chunk.getImage();
                        float[] matrix = image.matrix();
                        matrix[4] = xMarker + chunk.getImageOffsetX() - matrix[4];
                        matrix[5] = yMarker + chunk.getImageOffsetY() - matrix[5];
                        this.addImage(graphics, image, matrix[0], matrix[1], matrix[2], matrix[3], matrix[4], matrix[5]);
                        text.moveText(xMarker + lastBaseFactor + image.scaledWidth() - text.getXTLM(), 0.0f);
                    }
                }
                xMarker+=width;
                ++chunkStrokeIdx;
            }
            if (chunk.font().compareTo(currentFont) != 0) {
                currentFont = chunk.font();
                text.setFontAndSize(currentFont.getFont(), currentFont.size());
            }
            float rise = 0.0f;
            Object[] textRender = (Object[])chunk.getAttribute("TEXTRENDERMODE");
            int tr = 0;
            float strokeWidth = 1.0f;
            Color strokeColor = null;
            Float fr = (Float)chunk.getAttribute("SUBSUPSCRIPT");
            if (textRender != null) {
                tr = (Integer)textRender[0] & 3;
                if (tr != 0) {
                    text.setTextRenderingMode(tr);
                }
                if (tr == 1 || tr == 2) {
                    strokeWidth = ((Float)textRender[1]).floatValue();
                    if (strokeWidth != 1.0f) {
                        text.setLineWidth(strokeWidth);
                    }
                    if ((strokeColor = (Color)textRender[2]) == null) {
                        strokeColor = color;
                    }
                    if (strokeColor != null) {
                        text.setColorStroke(strokeColor);
                    }
                }
            }
            if (fr != null) {
                rise = fr.floatValue();
            }
            if (color != null) {
                text.setColorFill(color);
            }
            if (rise != 0.0f) {
                text.setTextRise(rise);
            }
            if (chunk.isImage()) {
                adjustMatrix = true;
            } else if (isJustified && numberOfSpaces > 0 && chunk.isSpecialEncoding()) {
                String s;
                int idx;
                if (hScale != lastHScale) {
                    lastHScale = hScale;
                    text.setWordSpacing(baseWordSpacing / hScale);
                    text.setCharacterSpacing(baseCharacterSpacing / hScale);
                }
                if ((idx = (s = chunk.toString()).indexOf(32)) < 0) {
                    text.showText(chunk.toString());
                } else {
                    float spaceCorrection = (- baseWordSpacing) * 1000.0f / chunk.font.size() / hScale;
                    PdfTextArray textArray = new PdfTextArray(s.substring(0, idx));
                    int lastIdx = idx;
                    while ((idx = s.indexOf(32, lastIdx + 1)) >= 0) {
                        textArray.add(spaceCorrection);
                        textArray.add(s.substring(lastIdx, idx));
                        lastIdx = idx;
                    }
                    textArray.add(spaceCorrection);
                    textArray.add(s.substring(lastIdx));
                    text.showText(textArray);
                }
            } else {
                if (isJustified && hScale != lastHScale) {
                    lastHScale = hScale;
                    text.setWordSpacing(baseWordSpacing / hScale);
                    text.setCharacterSpacing(baseCharacterSpacing / hScale);
                }
                text.showText(chunk.toString());
            }
            if (rise != 0.0f) {
                text.setTextRise(0.0f);
            }
            if (color != null) {
                text.resetRGBColorFill();
            }
            if (tr != 0) {
                text.setTextRenderingMode(0);
            }
            if (strokeColor != null) {
                text.resetRGBColorStroke();
            }
            if (strokeWidth != 1.0f) {
                text.setLineWidth(1.0f);
            }
            if (!chunk.isAttribute("SKEW") && !chunk.isAttribute("HSCALE")) continue;
            adjustMatrix = true;
            text.setTextMatrix(xMarker, yMarker);
        }
        if (isJustified) {
            text.setWordSpacing(0.0f);
            text.setCharacterSpacing(0.0f);
            if (line.isNewlineSplit()) {
                lastBaseFactor = 0.0f;
            }
        }
        if (adjustMatrix) {
            text.moveText(baseXMarker - text.getXTLM(), 0.0f);
        }
        currentValues[0] = currentFont;
        currentValues[1] = new Float(lastBaseFactor);
    }

    void localGoto(String name, float llx, float lly, float urx, float ury) {
        PdfAction action = this.getLocalGotoAction(name);
        this.annotations.add(new PdfAnnotation(this.writer, llx, lly, urx, ury, action));
    }

    PdfAction getLocalGotoAction(String name) {
        PdfAction action;
        Object[] obj = (Object[])this.localDestinations.get(name);
        if (obj == null) {
            obj = new Object[3];
        }
        if (obj[0] == null) {
            if (obj[1] == null) {
                obj[1] = this.writer.getPdfIndirectReference();
            }
            obj[0] = action = new PdfAction((PdfIndirectReference)obj[1]);
            this.localDestinations.put(name, obj);
        } else {
            action = (PdfAction)obj[0];
        }
        return action;
    }

    boolean localDestination(String name, PdfDestination destination) {
        Object[] obj = (Object[])this.localDestinations.get(name);
        if (obj == null) {
            obj = new Object[3];
        }
        if (obj[2] != null) {
            return false;
        }
        obj[2] = destination;
        this.localDestinations.put(name, obj);
        destination.addPage(this.writer.getCurrentPage());
        return true;
    }

    void remoteGoto(String filename, String name, float llx, float lly, float urx, float ury) {
        this.annotations.add(new PdfAnnotation(this.writer, llx, lly, urx, ury, new PdfAction(filename, name)));
    }

    void remoteGoto(String filename, int page, float llx, float lly, float urx, float ury) {
        this.writer.addAnnotation(new PdfAnnotation(this.writer, llx, lly, urx, ury, new PdfAction(filename, page)));
    }

    public void setViewerPreferences(int preferences) {
        this.viewerPreferences|=preferences;
    }

    void setAction(PdfAction action, float llx, float lly, float urx, float ury) {
        this.writer.addAnnotation(new PdfAnnotation(this.writer, llx, lly, urx, ury, action));
    }

    void setOpenAction(String name) {
        this.openActionName = name;
        this.openActionAction = null;
    }

    void setOpenAction(PdfAction action) {
        this.openActionAction = action;
        this.openActionName = null;
    }

    void addAdditionalAction(PdfName actionType, PdfAction action) {
        if (this.additionalActions == null) {
            this.additionalActions = new PdfDictionary();
        }
        if (action == null) {
            this.additionalActions.remove(actionType);
        } else {
            this.additionalActions.put(actionType, action);
        }
        if (this.additionalActions.size() == 0) {
            this.additionalActions = null;
        }
    }

    void setPageLabels(PdfPageLabels pageLabels) {
        this.pageLabels = pageLabels;
    }

    void addJavaScript(PdfAction js) {
        if (js.get(PdfName.JS) == null) {
            throw new RuntimeException("Only JavaScript actions are allowed.");
        }
        try {
            this.documentJavaScript.add(this.writer.addToBody(js).getIndirectReference());
        }
        catch (IOException e) {
            throw new ExceptionConverter(e);
        }
    }

    void setCropBoxSize(Rectangle crop) {
        this.setBoxSize("crop", crop);
    }

    void setBoxSize(String boxName, Rectangle size) {
        if (size == null) {
            this.boxSize.remove(boxName);
        } else {
            this.boxSize.put(boxName, new PdfRectangle(size));
        }
    }

    void addCalculationOrder(PdfFormField formField) {
        this.acroForm.addCalculationOrder(formField);
    }

    void setSigFlags(int f) {
        this.acroForm.setSigFlags(f);
    }

    void addFormFieldRaw(PdfFormField field) {
        this.annotations.add(field);
        ArrayList kids = field.getKids();
        if (kids != null) {
            for (int k = 0; k < kids.size(); ++k) {
                this.addFormFieldRaw((PdfFormField)kids.get(k));
            }
        }
    }

    void addAnnotation(PdfAnnotation annot) {
        this.pageEmpty = false;
        if (annot.isForm()) {
            PdfFormField field = (PdfFormField)annot;
            if (field.getParent() == null) {
                this.addFormFieldRaw(field);
            }
        } else {
            this.annotations.add(annot);
        }
    }

    void setDuration(int seconds) {
        this.duration = seconds > 0 ? seconds : -1;
    }

    void setTransition(PdfTransition transition) {
        this.transition = transition;
    }

    void setPageAction(PdfName actionType, PdfAction action) {
        if (this.pageAA == null) {
            this.pageAA = new PdfDictionary();
        }
        this.pageAA.put(actionType, action);
    }

    boolean isStrictImageSequence() {
        return this.strictImageSequence;
    }

    void setStrictImageSequence(boolean strictImageSequence) {
        this.strictImageSequence = strictImageSequence;
    }

    void setPageEmpty(boolean pageEmpty) {
        this.pageEmpty = pageEmpty;
    }

    public void clearTextWrap() throws DocumentException {
        super.clearTextWrap();
        float tmpHeight = this.imageEnd - this.currentHeight;
        if (this.line != null) {
            tmpHeight+=this.line.height();
        }
        if (this.imageEnd > -1.0f && tmpHeight > 0.0f) {
            this.carriageReturn();
            this.currentHeight+=tmpHeight;
        }
    }

    ArrayList getDocumentJavaScript() {
        return this.documentJavaScript;
    }

    public boolean setMarginMirroring(boolean MarginMirroring) {
        if (this.writer != null && this.writer.isPaused()) {
            return false;
        }
        return super.setMarginMirroring(MarginMirroring);
    }

    void setThumbnail(Image image) throws PdfException, DocumentException {
        this.thumb = this.writer.getImageReference(this.writer.addDirectImageSimple(image));
    }

    static class PdfCatalog
    extends PdfDictionary {
        PdfWriter writer;

        PdfCatalog(PdfIndirectReference pages, PdfWriter writer) {
            super(PdfDictionary.CATALOG);
            this.writer = writer;
            this.put(PdfName.PAGES, pages);
        }

        PdfCatalog(PdfIndirectReference pages, PdfIndirectReference outlines, PdfWriter writer) {
            super(PdfDictionary.CATALOG);
            this.writer = writer;
            this.put(PdfName.PAGES, pages);
            this.put(PdfName.PAGEMODE, PdfName.USEOUTLINES);
            this.put(PdfName.OUTLINES, outlines);
        }

        void addNames(TreeMap localDestinations, ArrayList documentJavaScript, PdfWriter writer) {
            if (localDestinations.size() == 0 && documentJavaScript.size() == 0) {
                return;
            }
            try {
                PdfDictionary names = new PdfDictionary();
                if (localDestinations.size() > 0) {
                    PdfArray ar = new PdfArray();
                    Iterator i = localDestinations.keySet().iterator();
                    while (i.hasNext()) {
                        String name = (String)i.next();
                        Object[] obj = (Object[])localDestinations.get(name);
                        PdfIndirectReference ref = (PdfIndirectReference)obj[1];
                        ar.add(new PdfString(name));
                        ar.add(ref);
                    }
                    PdfDictionary dests = new PdfDictionary();
                    dests.put(PdfName.NAMES, ar);
                    names.put(PdfName.DESTS, writer.addToBody(dests).getIndirectReference());
                }
                if (documentJavaScript.size() > 0) {
                    String[] s = new String[documentJavaScript.size()];
                    for (int k = 0; k < s.length; ++k) {
                        s[k] = Integer.toHexString(k);
                    }
                    Arrays.sort(s, new StringCompare());
                    PdfArray ar = new PdfArray();
                    for (int k2 = 0; k2 < s.length; ++k2) {
                        ar.add(new PdfString(s[k2]));
                        ar.add((PdfIndirectReference)documentJavaScript.get(k2));
                    }
                    PdfDictionary js = new PdfDictionary();
                    js.put(PdfName.NAMES, ar);
                    names.put(PdfName.JAVASCRIPT, writer.addToBody(js).getIndirectReference());
                }
                this.put(PdfName.NAMES, writer.addToBody(names).getIndirectReference());
            }
            catch (IOException e) {
                throw new ExceptionConverter(e);
            }
        }

        void setViewerPreferences(int preferences) {
            PdfReader.setViewerPreferences(preferences, this);
        }

        void setOpenAction(PdfAction action) {
            this.put(PdfName.OPENACTION, action);
        }

        void setAdditionalActions(PdfDictionary actions) {
            try {
                this.put(PdfName.AA, this.writer.addToBody(actions).getIndirectReference());
            }
            catch (Exception e) {
                new com.lowagie.text.ExceptionConverter(e);
            }
        }

        void setPageLabels(PdfPageLabels pageLabels) {
            this.put(PdfName.PAGELABELS, pageLabels.getDictionary());
        }

        void setAcroForm(PdfObject fields) {
            this.put(PdfName.ACROFORM, fields);
        }
    }

    public static class PdfInfo
    extends PdfDictionary {
        PdfInfo() {
            this.addProducer();
            this.addCreationDate();
        }

        PdfInfo(String author, String title, String subject) {
            this();
            this.addTitle(title);
            this.addSubject(subject);
            this.addAuthor(author);
        }

        void addTitle(String title) {
            this.put(PdfName.TITLE, new PdfString(title, "UnicodeBig"));
        }

        void addSubject(String subject) {
            this.put(PdfName.SUBJECT, new PdfString(subject, "UnicodeBig"));
        }

        void addKeywords(String keywords) {
            this.put(PdfName.KEYWORDS, new PdfString(keywords, "UnicodeBig"));
        }

        void addAuthor(String author) {
            this.put(PdfName.AUTHOR, new PdfString(author, "UnicodeBig"));
        }

        void addCreator(String creator) {
            this.put(PdfName.CREATOR, new PdfString(creator, "UnicodeBig"));
        }

        void addProducer() {
            this.put(PdfName.PRODUCER, new PdfString(Document.getVersion()));
        }

        void addCreationDate() {
            PdfDate date = new PdfDate();
            this.put(PdfName.CREATIONDATE, date);
            this.put(PdfName.MODDATE, date);
        }

        void addkey(String key, String value) {
            if (key.equals("Producer") || key.equals("CreationDate")) {
                return;
            }
            this.put(new PdfName(key), new PdfString(value, "UnicodeBig"));
        }
    }

}

