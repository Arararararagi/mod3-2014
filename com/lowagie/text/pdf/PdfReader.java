/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocWriter;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.StringCompare;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.IntHashtable;
import com.lowagie.text.pdf.LZWDecoder;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PRIndirectReference;
import com.lowagie.text.pdf.PRStream;
import com.lowagie.text.pdf.PRTokeniser;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfBoolean;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfEncodings;
import com.lowagie.text.pdf.PdfEncryption;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfLiteral;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNameTree;
import com.lowagie.text.pdf.PdfNull;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfReaderInstance;
import com.lowagie.text.pdf.PdfString;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.SequenceList;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.zip.InflaterInputStream;

public class PdfReader {
    static final PdfName[] pageInhCandidates = new PdfName[]{PdfName.MEDIABOX, PdfName.ROTATE, PdfName.RESOURCES, PdfName.CROPBOX};
    static final PdfName[] vpnames = new PdfName[]{PdfName.HIDETOOLBAR, PdfName.HIDEMENUBAR, PdfName.HIDEWINDOWUI, PdfName.FITWINDOW, PdfName.CENTERWINDOW, PdfName.DISPLAYDOCTITLE};
    static final int[] vpints = new int[]{256, 512, 1024, 2048, 4096, 262144};
    static final byte[] endstream = PdfEncodings.convertToBytes("endstream", null);
    static final byte[] endobj = PdfEncodings.convertToBytes("endobj", null);
    protected PRTokeniser tokens;
    protected int[] xref;
    protected HashMap objStmMark;
    protected IntHashtable objStmToOffset;
    protected boolean newXrefType;
    private ArrayList xrefObj;
    PdfDictionary rootPages;
    protected PdfDictionary trailer;
    protected PdfDictionary catalog;
    protected PageRefs pageRefs;
    protected PRAcroForm acroForm = null;
    protected boolean acroFormParsed = false;
    protected ArrayList pageInh;
    protected boolean encrypted = false;
    protected boolean rebuilt = false;
    protected int freeXref;
    protected boolean tampered = false;
    protected int lastXref;
    protected int eofPos;
    protected char pdfVersion;
    protected PdfEncryption decrypt;
    protected byte[] password = null;
    protected ArrayList strings = new ArrayList();
    protected boolean sharedStreams = true;
    protected boolean consolidateNamedDestinations = false;
    protected int rValue;
    protected int pValue;
    private int objNum;
    private int objGen;
    private boolean[] visited;
    private IntHashtable newHits;
    private int fileLength;
    private boolean hybridXref;
    private int lastXrefPartial = -1;
    private boolean partial;
    private boolean appendable;

    protected PdfReader() {
    }

    public PdfReader(String filename) throws IOException {
        this(filename, null);
    }

    public PdfReader(String filename, byte[] ownerPassword) throws IOException {
        this.password = ownerPassword;
        this.tokens = new PRTokeniser(filename);
        this.readPdf();
    }

    public PdfReader(byte[] pdfIn) throws IOException {
        this(pdfIn, null);
    }

    public PdfReader(byte[] pdfIn, byte[] ownerPassword) throws IOException {
        this.password = ownerPassword;
        this.tokens = new PRTokeniser(pdfIn);
        this.readPdf();
    }

    public PdfReader(URL url) throws IOException {
        this(url, null);
    }

    public PdfReader(URL url, byte[] ownerPassword) throws IOException {
        this.password = ownerPassword;
        this.tokens = new PRTokeniser(new RandomAccessFileOrArray(url));
        this.readPdf();
    }

    public PdfReader(InputStream is, byte[] ownerPassword) throws IOException {
        this.password = ownerPassword;
        this.tokens = new PRTokeniser(new RandomAccessFileOrArray(is));
        this.readPdf();
    }

    public PdfReader(InputStream is) throws IOException {
        this(is, null);
    }

    public PdfReader(RandomAccessFileOrArray raf, byte[] ownerPassword) throws IOException {
        this.password = ownerPassword;
        this.partial = true;
        this.tokens = new PRTokeniser(raf);
        this.readPdfPartial();
    }

    public PdfReader(PdfReader reader) {
        this.appendable = reader.appendable;
        this.consolidateNamedDestinations = reader.consolidateNamedDestinations;
        this.encrypted = reader.encrypted;
        this.rebuilt = reader.rebuilt;
        this.sharedStreams = reader.sharedStreams;
        this.tampered = reader.tampered;
        this.password = reader.password;
        this.pdfVersion = reader.pdfVersion;
        this.eofPos = reader.eofPos;
        this.freeXref = reader.freeXref;
        this.lastXref = reader.lastXref;
        this.tokens = new PRTokeniser(reader.tokens.getSafeFile());
        if (reader.decrypt != null) {
            this.decrypt = new PdfEncryption(reader.decrypt);
        }
        this.pValue = reader.pValue;
        this.rValue = reader.rValue;
        this.xrefObj = new ArrayList(reader.xrefObj);
        for (int k = 0; k < reader.xrefObj.size(); ++k) {
            this.xrefObj.set(k, PdfReader.duplicatePdfObject((PdfObject)reader.xrefObj.get(k), this));
        }
        this.pageRefs = new PageRefs(reader.pageRefs, this);
        this.trailer = (PdfDictionary)PdfReader.duplicatePdfObject(reader.trailer, this);
        this.catalog = (PdfDictionary)PdfReader.getPdfObject(this.trailer.get(PdfName.ROOT));
        this.rootPages = (PdfDictionary)PdfReader.getPdfObject(this.catalog.get(PdfName.PAGES));
        this.fileLength = reader.fileLength;
        this.partial = reader.partial;
        this.hybridXref = reader.hybridXref;
        this.objStmToOffset = reader.objStmToOffset;
        this.xref = reader.xref;
    }

    public RandomAccessFileOrArray getSafeFile() {
        return this.tokens.getSafeFile();
    }

    protected PdfReaderInstance getPdfReaderInstance(PdfWriter writer) {
        return new PdfReaderInstance(this, writer);
    }

    public int getNumberOfPages() {
        return this.pageRefs.size();
    }

    public PdfDictionary getCatalog() {
        return this.catalog;
    }

    public PRAcroForm getAcroForm() {
        if (!this.acroFormParsed) {
            this.acroFormParsed = true;
            PdfObject form = this.catalog.get(PdfName.ACROFORM);
            if (form != null) {
                try {
                    this.acroForm = new PRAcroForm(this);
                    this.acroForm.readAcroForm((PdfDictionary)PdfReader.getPdfObject(form));
                }
                catch (Exception e) {
                    this.acroForm = null;
                }
            }
        }
        return this.acroForm;
    }

    public int getPageRotation(int index) {
        return this.getPageRotation(this.pageRefs.getPageNRelease(index));
    }

    int getPageRotation(PdfDictionary page) {
        PdfNumber rotate = (PdfNumber)PdfReader.getPdfObject(page.get(PdfName.ROTATE));
        if (rotate == null) {
            return 0;
        }
        int n = rotate.intValue();
        return (n%=360) < 0 ? n + 360 : n;
    }

    public Rectangle getPageSizeWithRotation(int index) {
        return this.getPageSizeWithRotation(this.pageRefs.getPageNRelease(index));
    }

    public Rectangle getPageSizeWithRotation(PdfDictionary page) {
        Rectangle rect = this.getPageSize(page);
        for (int rotation = this.getPageRotation((PdfDictionary)page); rotation > 0; rotation-=90) {
            rect = rect.rotate();
        }
        return rect;
    }

    public Rectangle getPageSize(int index) {
        return this.getPageSize(this.pageRefs.getPageNRelease(index));
    }

    public Rectangle getPageSize(PdfDictionary page) {
        PdfArray mediaBox = (PdfArray)PdfReader.getPdfObject(page.get(PdfName.MEDIABOX));
        return PdfReader.getNormalizedRectangle(mediaBox);
    }

    public Rectangle getCropBox(int index) {
        PdfDictionary page = this.pageRefs.getPageNRelease(index);
        PdfArray cropBox = (PdfArray)PdfReader.getPdfObjectRelease(page.get(PdfName.CROPBOX));
        if (cropBox == null) {
            return this.getPageSize(page);
        }
        return PdfReader.getNormalizedRectangle(cropBox);
    }

    public Rectangle getBoxSize(int index, String boxName) {
        PdfDictionary page = this.pageRefs.getPageNRelease(index);
        PdfArray box = null;
        if (boxName.equals("trim")) {
            box = (PdfArray)PdfReader.getPdfObjectRelease(page.get(PdfName.TRIMBOX));
        } else if (boxName.equals("art")) {
            box = (PdfArray)PdfReader.getPdfObjectRelease(page.get(PdfName.ARTBOX));
        } else if (boxName.equals("bleed")) {
            box = (PdfArray)PdfReader.getPdfObjectRelease(page.get(PdfName.BLEEDBOX));
        } else if (boxName.equals("crop")) {
            box = (PdfArray)PdfReader.getPdfObjectRelease(page.get(PdfName.CROPBOX));
        } else if (boxName.equals("media")) {
            box = (PdfArray)PdfReader.getPdfObjectRelease(page.get(PdfName.MEDIABOX));
        }
        if (box == null) {
            return null;
        }
        return PdfReader.getNormalizedRectangle(box);
    }

    public HashMap getInfo() {
        HashMap<String, String> map = new HashMap<String, String>();
        PdfDictionary info = (PdfDictionary)PdfReader.getPdfObject(this.trailer.get(PdfName.INFO));
        if (info == null) {
            return map;
        }
        Iterator it = info.getKeys().iterator();
        while (it.hasNext()) {
            PdfName key = (PdfName)it.next();
            PdfObject obj = PdfReader.getPdfObject(info.get(key));
            if (obj == null) continue;
            String value = obj.toString();
            switch (obj.type()) {
                case 3: {
                    value = ((PdfString)obj).toUnicodeString();
                    break;
                }
                case 4: {
                    value = PdfName.decodeName(value);
                }
            }
            map.put(PdfName.decodeName(key.toString()), value);
        }
        return map;
    }

    public static Rectangle getNormalizedRectangle(PdfArray box) {
        ArrayList rect = box.getArrayList();
        float llx = ((PdfNumber)rect.get(0)).floatValue();
        float lly = ((PdfNumber)rect.get(1)).floatValue();
        float urx = ((PdfNumber)rect.get(2)).floatValue();
        float ury = ((PdfNumber)rect.get(3)).floatValue();
        return new Rectangle(Math.min(llx, urx), Math.min(lly, ury), Math.max(llx, urx), Math.max(lly, ury));
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     */
    protected void readPdf() throws IOException {
        try {
            this.fileLength = this.tokens.getFile().length();
            this.pdfVersion = this.tokens.checkPdfHeader();
            try {
                this.readXref();
            }
            catch (Exception e) {
                try {
                    this.rebuilt = true;
                    this.rebuildXref();
                    this.lastXref = -1;
                }
                catch (Exception ne) {
                    throw new IOException("Rebuild failed: " + ne.getMessage() + "; Original message: " + e.getMessage());
                }
            }
            try {
                this.readDocObj();
            }
            catch (IOException ne) {
                if (this.rebuilt) {
                    throw ne;
                }
                this.rebuilt = true;
                this.encrypted = false;
                this.rebuildXref();
                this.lastXref = -1;
                this.readDocObj();
            }
            this.strings.clear();
            this.readPages();
            this.eliminateSharedStreams();
            this.removeUnusedObjects();
            Object var3_6 = null;
        }
        catch (Throwable var4_4) {
            Object var3_5 = null;
            try {
                this.tokens.close();
                throw var4_4;
            }
            catch (Exception e) {
                // empty catch block
            }
            throw var4_4;
        }
        try {}
        catch (Exception e) {
            return;
        }
        this.tokens.close();
    }

    protected void readPdfPartial() throws IOException {
        try {
            this.fileLength = this.tokens.getFile().length();
            this.pdfVersion = this.tokens.checkPdfHeader();
            try {
                this.readXref();
            }
            catch (Exception e) {
                try {
                    this.rebuilt = true;
                    this.rebuildXref();
                    this.lastXref = -1;
                }
                catch (Exception ne) {
                    throw new IOException("Rebuild failed: " + ne.getMessage() + "; Original message: " + e.getMessage());
                }
            }
            this.readDocObjPartial();
            this.readPages();
        }
        catch (IOException e) {
            try {
                this.tokens.close();
            }
            catch (Exception ne) {
                // empty catch block
            }
            throw e;
        }
    }

    private boolean equalsArray(byte[] ar1, byte[] ar2, int size) {
        for (int k = 0; k < size; ++k) {
            if (ar1[k] == ar2[k]) continue;
            return false;
        }
        return true;
    }

    private void readDecryptedDocObj() throws IOException {
        PdfObject o;
        String s;
        if (this.encrypted) {
            return;
        }
        PdfObject encDic = this.trailer.get(PdfName.ENCRYPT);
        if (encDic == null || encDic.toString().equals("null")) {
            return;
        }
        this.encrypted = true;
        PdfDictionary enc = (PdfDictionary)PdfReader.getPdfObject(encDic);
        PdfArray documentIDs = (PdfArray)PdfReader.getPdfObject(this.trailer.get(PdfName.ID));
        byte[] documentID = null;
        if (documentIDs != null) {
            o = (PdfObject)documentIDs.getArrayList().get(0);
            s = o.toString();
            documentID = DocWriter.getISOBytes(s);
        }
        s = enc.get(PdfName.U).toString();
        byte[] uValue = DocWriter.getISOBytes(s);
        s = enc.get(PdfName.O).toString();
        byte[] oValue = DocWriter.getISOBytes(s);
        o = enc.get(PdfName.R);
        if (!o.isNumber()) {
            throw new IOException("Illegal R value.");
        }
        this.rValue = ((PdfNumber)o).intValue();
        if (this.rValue != 2 && this.rValue != 3) {
            throw new IOException("Unknown encryption type (" + this.rValue + ")");
        }
        o = enc.get(PdfName.P);
        if (!o.isNumber()) {
            throw new IOException("Illegal P value.");
        }
        this.pValue = ((PdfNumber)o).intValue();
        this.decrypt = new PdfEncryption();
        this.decrypt.setupByUserPassword(documentID, this.password, oValue, this.pValue, this.rValue == 3);
        if (!this.equalsArray(uValue, this.decrypt.userKey, this.rValue == 3 ? 16 : 32)) {
            this.decrypt.setupByOwnerPassword(documentID, this.password, uValue, oValue, this.pValue, this.rValue == 3);
            if (!this.equalsArray(uValue, this.decrypt.userKey, this.rValue == 3 ? 16 : 32)) {
                throw new IOException("Bad user password");
            }
        }
        for (int k = 0; k < this.strings.size(); ++k) {
            PdfString str = (PdfString)this.strings.get(k);
            str.decrypt(this);
        }
        if (encDic.isIndirect()) {
            this.xrefObj.set(((PRIndirectReference)encDic).getNumber(), null);
        }
    }

    public static PdfObject getPdfObjectRelease(PdfObject obj) {
        PdfObject obj2 = PdfReader.getPdfObject(obj);
        PdfReader.releaseLastXrefPartial(obj);
        return obj2;
    }

    public static PdfObject getPdfObject(PdfObject obj) {
        if (obj == null) {
            return null;
        }
        if (!obj.isIndirect()) {
            return obj;
        }
        try {
            PRIndirectReference ref = (PRIndirectReference)obj;
            int idx = ref.getNumber();
            boolean appendable = ref.getReader().appendable;
            obj = ref.getReader().getPdfObject(idx);
            if (obj == null) {
                if (appendable) {
                    obj = new PdfNull();
                    obj.setIndRef(ref);
                    return obj;
                }
                return PdfNull.PDFNULL;
            }
            if (appendable) {
                switch (obj.type()) {
                    case 8: {
                        obj = new PdfNull();
                        break;
                    }
                    case 1: {
                        obj = new PdfBoolean(((PdfBoolean)obj).booleanValue());
                        break;
                    }
                    case 4: {
                        obj = new PdfName(obj.getBytes());
                    }
                }
                obj.setIndRef(ref);
            }
            return obj;
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    public static PdfObject getPdfObjectRelease(PdfObject obj, PdfObject parent) {
        PdfObject obj2 = PdfReader.getPdfObject(obj, parent);
        PdfReader.releaseLastXrefPartial(obj);
        return obj2;
    }

    public static PdfObject getPdfObject(PdfObject obj, PdfObject parent) {
        if (obj == null) {
            return null;
        }
        if (!obj.isIndirect()) {
            PRIndirectReference ref = null;
            if (parent != null && (ref = parent.getIndRef()) != null && ref.getReader().isAppendable()) {
                switch (obj.type()) {
                    case 8: {
                        obj = new PdfNull();
                        break;
                    }
                    case 1: {
                        obj = new PdfBoolean(((PdfBoolean)obj).booleanValue());
                        break;
                    }
                    case 4: {
                        obj = new PdfName(obj.getBytes());
                    }
                }
                obj.setIndRef(ref);
            }
            return obj;
        }
        return PdfReader.getPdfObject(obj);
    }

    public PdfObject getPdfObjectRelease(int idx) {
        PdfObject obj = this.getPdfObject(idx);
        this.releaseLastXrefPartial();
        return obj;
    }

    public PdfObject getPdfObject(int idx) {
        try {
            this.lastXrefPartial = -1;
            if (idx < 0 || idx >= this.xrefObj.size()) {
                return null;
            }
            PdfObject obj = (PdfObject)this.xrefObj.get(idx);
            if (!(this.partial && obj == null)) {
                return obj;
            }
            if (idx * 2 >= this.xref.length) {
                return null;
            }
            obj = this.readSingleObject(idx);
            this.lastXrefPartial = -1;
            if (obj != null) {
                this.lastXrefPartial = idx;
            }
            return obj;
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    public void resetLastXrefPartial() {
        this.lastXrefPartial = -1;
    }

    public void releaseLastXrefPartial() {
        if (this.partial && this.lastXrefPartial != -1) {
            this.xrefObj.set(this.lastXrefPartial, null);
            this.lastXrefPartial = -1;
        }
    }

    public static void releaseLastXrefPartial(PdfObject obj) {
        if (obj == null) {
            return;
        }
        if (!obj.isIndirect()) {
            return;
        }
        PRIndirectReference ref = (PRIndirectReference)obj;
        PdfReader reader = ref.getReader();
        if (reader.partial && reader.lastXrefPartial != -1 && reader.lastXrefPartial == ref.getNumber()) {
            reader.xrefObj.set(reader.lastXrefPartial, null);
        }
        reader.lastXrefPartial = -1;
    }

    private void setXrefPartialObject(int idx, PdfObject obj) {
        if (!(this.partial && idx >= 0)) {
            return;
        }
        this.xrefObj.set(idx, obj);
    }

    public PRIndirectReference addPdfObject(PdfObject obj) {
        this.xrefObj.add(obj);
        return new PRIndirectReference(this, this.xrefObj.size() - 1);
    }

    protected void readPages() throws IOException {
        this.pageInh = new ArrayList();
        this.catalog = (PdfDictionary)PdfReader.getPdfObject(this.trailer.get(PdfName.ROOT));
        this.rootPages = (PdfDictionary)PdfReader.getPdfObject(this.catalog.get(PdfName.PAGES));
        this.pageRefs = new PageRefs(this, null);
    }

    protected PRIndirectReference getSinglePage(int n) throws IOException {
        PdfDictionary acc = new PdfDictionary();
        PdfDictionary top = this.rootPages;
        boolean base = false;
        return null;
    }

    protected void PRSimpleRecursive(PdfObject obj) throws IOException {
        switch (obj.type()) {
            case 6: 
            case 7: {
                PdfDictionary dic = (PdfDictionary)obj;
                Iterator it = dic.getKeys().iterator();
                while (it.hasNext()) {
                    PdfName key = (PdfName)it.next();
                    this.PRSimpleRecursive(dic.get(key));
                }
                break;
            }
            case 5: {
                ArrayList list = ((PdfArray)obj).getArrayList();
                for (int k = 0; k < list.size(); ++k) {
                    this.PRSimpleRecursive((PdfObject)list.get(k));
                }
                break;
            }
            case 10: {
                PRIndirectReference ref = (PRIndirectReference)obj;
                int num = ref.getNumber();
                if (this.visited[num]) break;
                this.visited[num] = true;
                this.newHits.put(num, 1);
            }
        }
    }

    protected void readDocObjPartial() throws IOException {
        this.xrefObj = new ArrayList(this.xref.length / 2);
        this.xrefObj.addAll(Collections.nCopies(this.xref.length / 2, null));
        this.readDecryptedDocObj();
        if (this.objStmToOffset != null) {
            int[] keys = this.objStmToOffset.getKeys();
            for (int k = 0; k < keys.length; ++k) {
                int n = keys[k];
                this.objStmToOffset.put(n, this.xref[n * 2]);
                this.xref[n * 2] = -1;
            }
        }
    }

    protected PdfObject readSingleObject(int k) throws IOException {
        PdfObject obj;
        this.strings.clear();
        int k2 = k * 2;
        int pos = this.xref[k2];
        if (pos < 0) {
            return null;
        }
        if (this.xref[k2 + 1] > 0) {
            pos = this.objStmToOffset.get(this.xref[k2 + 1]);
        }
        this.tokens.seek(pos);
        this.tokens.nextValidToken();
        if (this.tokens.getTokenType() != 1) {
            this.tokens.throwError("Invalid object number.");
        }
        this.objNum = this.tokens.intValue();
        this.tokens.nextValidToken();
        if (this.tokens.getTokenType() != 1) {
            this.tokens.throwError("Invalid generation number.");
        }
        this.objGen = this.tokens.intValue();
        this.tokens.nextValidToken();
        if (!this.tokens.getStringValue().equals("obj")) {
            this.tokens.throwError("Token 'obj' expected.");
        }
        try {
            obj = this.readPRObject();
            for (int j = 0; j < this.strings.size(); ++j) {
                PdfString str = (PdfString)this.strings.get(j);
                str.decrypt(this);
            }
            if (obj.isStream()) {
                this.checkPRStreamLength((PRStream)obj);
            }
        }
        catch (Exception e) {
            obj = null;
        }
        if (this.xref[k2 + 1] > 0) {
            obj = this.readOneObjStm((PRStream)obj, this.xref[k2]);
        }
        this.xrefObj.set(k, obj);
        return obj;
    }

    protected PdfObject readOneObjStm(PRStream stream, int idx) throws IOException {
        int first = ((PdfNumber)PdfReader.getPdfObject(stream.get(PdfName.FIRST))).intValue();
        int n = ((PdfNumber)PdfReader.getPdfObject(stream.get(PdfName.N))).intValue();
        byte[] b = PdfReader.getStreamBytes(stream, this.tokens.getFile());
        PRTokeniser saveTokens = this.tokens;
        this.tokens = new PRTokeniser(b);
        try {
            int address = 0;
            int objNumber = 0;
            boolean ok = true;
            for (int k = 0; k < ++idx; ++k) {
                ok = this.tokens.nextToken();
                if (!ok) break;
                if (this.tokens.getTokenType() != 1) {
                    ok = false;
                    break;
                }
                objNumber = this.tokens.intValue();
                ok = this.tokens.nextToken();
                if (!ok) break;
                if (this.tokens.getTokenType() != 1) {
                    ok = false;
                    break;
                }
                address = this.tokens.intValue() + first;
            }
            if (!ok) {
                throw new IOException("Error reading ObjStm");
            }
            this.tokens.seek(address);
            PdfObject pdfObject = this.readPRObject();
            Object var11_12 = null;
            this.tokens = saveTokens;
            return pdfObject;
        }
        catch (Throwable var12_14) {
            Object var11_13 = null;
            this.tokens = saveTokens;
            throw var12_14;
        }
    }

    public double dumpPerc() {
        int total = 0;
        for (int k = 0; k < this.xrefObj.size(); ++k) {
            if (this.xrefObj.get(k) == null) continue;
            ++total;
        }
        return (double)total * 100.0 / (double)this.xrefObj.size();
    }

    protected void readDocObj() throws IOException {
        ArrayList<PdfObject> streams = new ArrayList<PdfObject>();
        this.xrefObj = new ArrayList(this.xref.length / 2);
        this.xrefObj.addAll(Collections.nCopies(this.xref.length / 2, null));
        for (int k = 2; k < this.xref.length; k+=2) {
            PdfObject obj;
            int pos = this.xref[k];
            if (pos <= 0) continue;
            if (this.xref[k + 1] > 0) continue;
            this.tokens.seek(pos);
            this.tokens.nextValidToken();
            if (this.tokens.getTokenType() != 1) {
                this.tokens.throwError("Invalid object number.");
            }
            this.objNum = this.tokens.intValue();
            this.tokens.nextValidToken();
            if (this.tokens.getTokenType() != 1) {
                this.tokens.throwError("Invalid generation number.");
            }
            this.objGen = this.tokens.intValue();
            this.tokens.nextValidToken();
            if (!this.tokens.getStringValue().equals("obj")) {
                this.tokens.throwError("Token 'obj' expected.");
            }
            try {
                obj = this.readPRObject();
                if (obj.isStream()) {
                    streams.add(obj);
                }
            }
            catch (Exception e) {
                obj = null;
            }
            this.xrefObj.set(k / 2, obj);
        }
        int fileLength = this.tokens.length();
        byte[] tline = new byte[16];
        for (int k2 = 0; k2 < streams.size(); ++k2) {
            this.checkPRStreamLength((PRStream)streams.get(k2));
        }
        this.readDecryptedDocObj();
        if (this.objStmMark != null) {
            Iterator i = this.objStmMark.entrySet().iterator();
            while (i.hasNext()) {
                Map.Entry entry = i.next();
                int n = (Integer)entry.getKey();
                IntHashtable h = (IntHashtable)entry.getValue();
                this.readObjStm((PRStream)this.xrefObj.get(n), h);
                this.xrefObj.set(n, null);
            }
            this.objStmMark = null;
        }
        this.xref = null;
    }

    private void checkPRStreamLength(PRStream stream) throws IOException {
        int streamLength;
        block9 : {
            int fileLength = this.tokens.length();
            int start = stream.getOffset();
            boolean calc = false;
            streamLength = 0;
            PdfObject obj = PdfReader.getPdfObjectRelease(stream.get(PdfName.LENGTH));
            if (obj != null && obj.type() == 2) {
                streamLength = ((PdfNumber)obj).intValue();
                if (streamLength + start > fileLength - 20) {
                    calc = true;
                } else {
                    this.tokens.seek(start + streamLength);
                    String line = this.tokens.readString(20);
                    if (!(line.startsWith("\nendstream") || line.startsWith("\r\nendstream") || line.startsWith("\rendstream") || line.startsWith("endstream"))) {
                        calc = true;
                    }
                }
            } else {
                calc = true;
            }
            if (calc) {
                int pos;
                byte[] tline = new byte[16];
                this.tokens.seek(start);
                do {
                    pos = this.tokens.getFilePointer();
                    if (!this.tokens.readLineSegment(tline)) break block9;
                    if (!PdfReader.equalsn(tline, endstream)) continue;
                    streamLength = pos - start;
                    break block9;
                } while (!PdfReader.equalsn(tline, endobj));
                this.tokens.seek(pos - 16);
                String s = this.tokens.readString(16);
                int index = s.indexOf("endstream");
                if (index >= 0) {
                    pos = pos - 16 + index;
                }
                streamLength = pos - start;
            }
        }
        stream.setLength(streamLength);
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     */
    protected void readObjStm(PRStream stream, IntHashtable map) throws IOException {
        int first = ((PdfNumber)PdfReader.getPdfObject(stream.get(PdfName.FIRST))).intValue();
        int n = ((PdfNumber)PdfReader.getPdfObject(stream.get(PdfName.N))).intValue();
        byte[] b = PdfReader.getStreamBytes(stream, this.tokens.getFile());
        PRTokeniser saveTokens = this.tokens;
        this.tokens = new PRTokeniser(b);
        try {
            int k;
            int[] address = new int[n];
            int[] objNumber = new int[n];
            boolean ok = true;
            for (k = 0; k < n; ++k) {
                ok = this.tokens.nextToken();
                if (!ok) break;
                if (this.tokens.getTokenType() != 1) {
                    ok = false;
                    break;
                }
                objNumber[k] = this.tokens.intValue();
                ok = this.tokens.nextToken();
                if (!ok) break;
                if (this.tokens.getTokenType() != 1) {
                    ok = false;
                    break;
                }
                address[k] = this.tokens.intValue() + first;
            }
            if (!ok) {
                throw new IOException("Error reading ObjStm");
            }
            for (k = 0; k < n; ++k) {
                if (!map.containsKey(k)) continue;
                this.tokens.seek(address[k]);
                PdfObject obj = this.readPRObject();
                this.xrefObj.set(objNumber[k], obj);
            }
            Object var12_14 = null;
            this.tokens = saveTokens;
            return;
        }
        catch (Throwable var13_12) {
            Object var12_13 = null;
            this.tokens = saveTokens;
            throw var13_12;
        }
    }

    static PdfObject killIndirect(PdfObject obj) {
        if (obj == null || obj.isNull()) {
            return null;
        }
        PdfObject ret = PdfReader.getPdfObjectRelease(obj);
        if (obj.isIndirect()) {
            PRIndirectReference ref = (PRIndirectReference)obj;
            PdfReader reader = ref.getReader();
            int n = ref.getNumber();
            reader.xrefObj.set(n, null);
            if (reader.partial) {
                reader.xref[n * 2] = -1;
            }
        }
        return ret;
    }

    private void ensureXrefSize(int size) {
        if (size == 0) {
            return;
        }
        if (this.xref == null) {
            this.xref = new int[size];
        } else if (this.xref.length < size) {
            int[] xref2 = new int[size];
            System.arraycopy(this.xref, 0, xref2, 0, this.xref.length);
            this.xref = xref2;
        }
    }

    protected void readXref() throws IOException {
        int startxref;
        PdfNumber prev;
        this.hybridXref = false;
        this.newXrefType = false;
        this.tokens.seek(this.tokens.getStartxref());
        this.tokens.nextToken();
        if (!this.tokens.getStringValue().equals("startxref")) {
            throw new IOException("startxref not found.");
        }
        this.tokens.nextToken();
        if (this.tokens.getTokenType() != 1) {
            throw new IOException("startxref is not followed by a number.");
        }
        this.lastXref = startxref = this.tokens.intValue();
        this.eofPos = this.tokens.getFilePointer();
        try {
            if (this.readXRefStream(startxref)) {
                this.newXrefType = true;
                return;
            }
        }
        catch (Exception var2_2) {
            // empty catch block
        }
        this.xref = null;
        this.tokens.seek(startxref);
        PdfDictionary trailer2 = this.trailer = this.readXrefSection();
        while ((prev = (PdfNumber)trailer2.get(PdfName.PREV)) != null) {
            this.tokens.seek(prev.intValue());
            trailer2 = this.readXrefSection();
        }
    }

    protected PdfDictionary readXrefSection() throws IOException {
        this.tokens.nextValidToken();
        if (!this.tokens.getStringValue().equals("xref")) {
            this.tokens.throwError("xref subsection not found");
        }
        int start = 0;
        int end = 0;
        int pos = 0;
        int gen = 0;
        block2 : do {
            this.tokens.nextValidToken();
            if (this.tokens.getStringValue().equals("trailer")) break;
            if (this.tokens.getTokenType() != 1) {
                this.tokens.throwError("Object number of the first object in this xref subsection not found");
            }
            start = this.tokens.intValue();
            this.tokens.nextValidToken();
            if (this.tokens.getTokenType() != 1) {
                this.tokens.throwError("Number of entries in this xref subsection not found");
            }
            end = this.tokens.intValue() + start;
            if (start == 1) {
                int back = this.tokens.getFilePointer();
                this.tokens.nextValidToken();
                pos = this.tokens.intValue();
                this.tokens.nextValidToken();
                gen = this.tokens.intValue();
                if (pos == 0 && gen == 65535) {
                    --start;
                    --end;
                }
                this.tokens.seek(back);
            }
            this.ensureXrefSize(end * 2);
            int k = start;
            do {
                if (k >= end) continue block2;
                this.tokens.nextValidToken();
                pos = this.tokens.intValue();
                this.tokens.nextValidToken();
                gen = this.tokens.intValue();
                this.tokens.nextValidToken();
                int p = k * 2;
                if (this.tokens.getStringValue().equals("n")) {
                    if (this.xref[p] == 0 && this.xref[p + 1] == 0) {
                        this.xref[p] = pos;
                    }
                } else if (this.tokens.getStringValue().equals("f")) {
                    if (this.xref[p] == 0 && this.xref[p + 1] == 0) {
                        this.xref[p] = -1;
                    }
                } else {
                    this.tokens.throwError("Invalid cross-reference entry in this xref subsection");
                }
                ++k;
            } while (true);
            break;
        } while (true);
        PdfDictionary trailer = (PdfDictionary)this.readPRObject();
        PdfNumber xrefSize = (PdfNumber)trailer.get(PdfName.SIZE);
        this.ensureXrefSize(xrefSize.intValue() * 2);
        PdfObject xrs = trailer.get(PdfName.XREFSTM);
        if (xrs != null && xrs.isNumber()) {
            int loc = ((PdfNumber)xrs).intValue();
            try {
                this.readXRefStream(loc);
                this.newXrefType = true;
                this.hybridXref = true;
            }
            catch (IOException e) {
                this.xref = null;
                throw e;
            }
        }
        return trailer;
    }

    protected boolean readXRefStream(int ptr) throws IOException {
        PdfArray index;
        this.tokens.seek(ptr);
        int thisStream = 0;
        if (!this.tokens.nextToken()) {
            return false;
        }
        if (this.tokens.getTokenType() != 1) {
            return false;
        }
        thisStream = this.tokens.intValue();
        if (!(this.tokens.nextToken() && this.tokens.getTokenType() == 1)) {
            return false;
        }
        if (!(this.tokens.nextToken() && this.tokens.getStringValue().equals("obj"))) {
            return false;
        }
        PdfObject object = this.readPRObject();
        PRStream stm = null;
        if (object.isStream() && !PdfName.XREF.equals((stm = (PRStream)object).get(PdfName.TYPE))) {
            return false;
        }
        if (this.trailer == null) {
            this.trailer = new PdfDictionary();
            this.trailer.putAll(stm);
        }
        stm.setLength(((PdfNumber)stm.get(PdfName.LENGTH)).intValue());
        int size = ((PdfNumber)stm.get(PdfName.SIZE)).intValue();
        PdfObject obj = stm.get(PdfName.INDEX);
        if (obj == null) {
            index = new PdfArray();
            int[] arrn = new int[2];
            arrn[1] = size;
            index.add(arrn);
        } else {
            index = (PdfArray)obj;
        }
        PdfArray w = (PdfArray)stm.get(PdfName.W);
        int prev = -1;
        obj = stm.get(PdfName.PREV);
        if (obj != null) {
            prev = ((PdfNumber)obj).intValue();
        }
        this.ensureXrefSize(size * 2);
        if (!(this.objStmMark != null || this.partial)) {
            this.objStmMark = new HashMap();
        }
        if (this.objStmToOffset == null && this.partial) {
            this.objStmToOffset = new IntHashtable();
        }
        byte[] b = PdfReader.getStreamBytes(stm, this.tokens.getFile());
        int bptr = 0;
        ArrayList wa = w.getArrayList();
        int[] wc = new int[3];
        for (int k = 0; k < 3; ++k) {
            wc[k] = ((PdfNumber)wa.get(k)).intValue();
        }
        ArrayList sections = index.getArrayList();
        for (int idx = 0; idx < sections.size(); idx+=2) {
            int start = ((PdfNumber)sections.get(idx)).intValue();
            int length = ((PdfNumber)sections.get(idx + 1)).intValue();
            this.ensureXrefSize((start + length) * 2);
            while (length-- > 0) {
                boolean total = false;
                int type = 1;
                if (wc[0] > 0) {
                    type = 0;
                    for (int k2 = 0; k2 < wc[0]; ++k2) {
                        type = (type << 8) + (b[bptr++] & 255);
                    }
                }
                int field2 = 0;
                for (int k3 = 0; k3 < wc[1]; ++k3) {
                    field2 = (field2 << 8) + (b[bptr++] & 255);
                }
                int field3 = 0;
                for (int k4 = 0; k4 < wc[2]; ++k4) {
                    field3 = (field3 << 8) + (b[bptr++] & 255);
                }
                int base = start * 2;
                if (this.xref[base] == 0 && this.xref[base + 1] == 0) {
                    switch (type) {
                        case 0: {
                            this.xref[base] = -1;
                            break;
                        }
                        case 1: {
                            this.xref[base] = field2;
                            break;
                        }
                        case 2: {
                            this.xref[base] = field3;
                            this.xref[base + 1] = field2;
                            if (this.partial) {
                                this.objStmToOffset.put(field2, 0);
                                break;
                            }
                            Integer on = new Integer(field2);
                            IntHashtable seq = (IntHashtable)this.objStmMark.get(on);
                            if (seq == null) {
                                seq = new IntHashtable();
                                seq.put(field3, 1);
                                this.objStmMark.put(on, seq);
                                break;
                            }
                            seq.put(field3, 1);
                        }
                    }
                }
                ++start;
            }
        }
        if ((thisStream*=2) < this.xref.length) {
            this.xref[thisStream] = -1;
        }
        if (prev == -1) {
            return true;
        }
        return this.readXRefStream(prev);
    }

    protected void rebuildXref() throws IOException {
        int[] obj;
        this.hybridXref = false;
        this.newXrefType = false;
        this.tokens.seek(0);
        int[][] xr = new int[1024][];
        int top = 0;
        this.trailer = null;
        byte[] line = new byte[64];
        do {
            int pos = this.tokens.getFilePointer();
            if (!this.tokens.readLineSegment(line)) break;
            if (line[0] == 116) {
                if (!PdfEncodings.convertToString(line, null).startsWith("trailer")) continue;
                pos = this.tokens.getFilePointer();
                try {
                    PdfDictionary dic = (PdfDictionary)this.readPRObject();
                    if (dic.get(PdfName.ROOT) != null) {
                        this.trailer = dic;
                        continue;
                    }
                    this.tokens.seek(pos);
                }
                catch (Exception e) {
                    this.tokens.seek(pos);
                }
                continue;
            }
            if (line[0] < 48 || line[0] > 57) continue;
            obj = PRTokeniser.checkObjectStart(line);
            if (obj == null) continue;
            int num = obj[0];
            int gen = obj[1];
            if (num >= xr.length) {
                int newLength = num * 2;
                int[][] xr2 = new int[newLength][];
                System.arraycopy(xr, 0, xr2, 0, top);
                xr = xr2;
            }
            if (num >= top) {
                top = num + 1;
            }
            if (xr[num] != null && gen < xr[num][1]) continue;
            obj[0] = pos;
            xr[num] = obj;
        } while (true);
        if (this.trailer == null) {
            throw new IOException("trailer not found.");
        }
        this.xref = new int[top * 2];
        for (int k = 0; k < top; ++k) {
            obj = xr[k];
            if (obj == null) continue;
            this.xref[k * 2] = obj[0];
        }
    }

    protected PdfDictionary readDictionary() throws IOException {
        PdfDictionary dic = new PdfDictionary();
        do {
            this.tokens.nextValidToken();
            if (this.tokens.getTokenType() == 8) break;
            if (this.tokens.getTokenType() != 3) {
                this.tokens.throwError("Dictionary key is not a name.");
            }
            PdfName name = new PdfName(this.tokens.getStringValue());
            PdfObject obj = this.readPRObject();
            int type = obj.type();
            if (- type == 8) {
                this.tokens.throwError("Unexpected '>>'");
            }
            if (- type == 6) {
                this.tokens.throwError("Unexpected ']'");
            }
            dic.put(name, obj);
        } while (true);
        return dic;
    }

    protected PdfArray readArray() throws IOException {
        PdfObject obj;
        int type;
        PdfArray array = new PdfArray();
        while (- (type = (obj = this.readPRObject()).type()) != 6) {
            if (- type == 8) {
                this.tokens.throwError("Unexpected '>>'");
            }
            array.add(obj);
        }
        return array;
    }

    protected PdfObject readPRObject() throws IOException {
        this.tokens.nextValidToken();
        int type = this.tokens.getTokenType();
        switch (type) {
            case 7: {
                PdfDictionary dic = this.readDictionary();
                int pos = this.tokens.getFilePointer();
                if (this.tokens.nextToken() && this.tokens.getStringValue().equals("stream")) {
                    int ch = this.tokens.read();
                    if (ch != 10) {
                        ch = this.tokens.read();
                    }
                    if (ch != 10) {
                        this.tokens.backOnePosition(ch);
                    }
                    PRStream stream = new PRStream(this, this.tokens.getFilePointer());
                    stream.putAll(dic);
                    stream.setObjNum(this.objNum, this.objGen);
                    return stream;
                }
                this.tokens.seek(pos);
                return dic;
            }
            case 5: {
                return this.readArray();
            }
            case 1: {
                return new PdfNumber(this.tokens.getStringValue());
            }
            case 2: {
                PdfString str = new PdfString(this.tokens.getStringValue(), null).setHexWriting(this.tokens.isHexString());
                str.setObjNum(this.objNum, this.objGen);
                if (this.strings != null) {
                    this.strings.add(str);
                }
                return str;
            }
            case 3: {
                return new PdfName(this.tokens.getStringValue());
            }
            case 9: {
                int num = this.tokens.getReference();
                PRIndirectReference ref = new PRIndirectReference(this, num, this.tokens.getGeneration());
                if (!(this.visited == null || this.visited[num])) {
                    this.visited[num] = true;
                    this.newHits.put(num, 1);
                }
                return ref;
            }
        }
        return new PdfLiteral(- type, this.tokens.getStringValue());
    }

    public static byte[] FlateDecode(byte[] in) {
        byte[] b = PdfReader.FlateDecode(in, true);
        if (b == null) {
            return PdfReader.FlateDecode(in, false);
        }
        return b;
    }

    public static byte[] decodePredictor(byte[] in, PdfObject dicPar) {
        if (!(dicPar != null && dicPar.isDictionary())) {
            return in;
        }
        PdfDictionary dic = (PdfDictionary)dicPar;
        PdfObject obj = PdfReader.getPdfObject(dic.get(PdfName.PREDICTOR));
        if (!(obj != null && obj.isNumber())) {
            return in;
        }
        int predictor = ((PdfNumber)obj).intValue();
        if (predictor < 10) {
            return in;
        }
        int width = 1;
        obj = PdfReader.getPdfObject(dic.get(PdfName.COLUMNS));
        if (obj != null && obj.isNumber()) {
            width = ((PdfNumber)obj).intValue();
        }
        int colors = 1;
        obj = PdfReader.getPdfObject(dic.get(PdfName.COLORS));
        if (obj != null && obj.isNumber()) {
            colors = ((PdfNumber)obj).intValue();
        }
        int bpc = 8;
        obj = PdfReader.getPdfObject(dic.get(PdfName.BITSPERCOMPONENT));
        if (obj != null && obj.isNumber()) {
            bpc = ((PdfNumber)obj).intValue();
        }
        DataInputStream dataStream = new DataInputStream(new ByteArrayInputStream(in));
        ByteArrayOutputStream fout = new ByteArrayOutputStream(in.length);
        int bytesPerPixel = colors * bpc / 8;
        int bytesPerRow = (colors * width * bpc + 7) / 8;
        byte[] curr = new byte[bytesPerRow];
        byte[] prior = new byte[bytesPerRow];
        do {
            int filter = 0;
            try {
                filter = dataStream.read();
                if (filter < 0) {
                    return fout.toByteArray();
                }
                dataStream.readFully(curr, 0, bytesPerRow);
            }
            catch (Exception e) {
                return fout.toByteArray();
            }
            switch (filter) {
                case 0: {
                    break;
                }
                case 1: {
                    for (int i = bytesPerPixel; i < bytesPerRow; ++i) {
                        byte[] arrby = curr;
                        int n = i;
                        arrby[n] = (byte)(arrby[n] + curr[i - bytesPerPixel]);
                    }
                    break;
                }
                case 2: {
                    for (int i = 0; i < bytesPerRow; ++i) {
                        byte[] arrby = curr;
                        int n = i;
                        arrby[n] = (byte)(arrby[n] + prior[i]);
                    }
                    break;
                }
                case 3: {
                    int i;
                    for (i = 0; i < bytesPerPixel; ++i) {
                        byte[] arrby = curr;
                        int n = i;
                        arrby[n] = (byte)(arrby[n] + prior[i] / 2);
                    }
                    for (i = bytesPerPixel; i < bytesPerRow; ++i) {
                        byte[] arrby = curr;
                        int n = i;
                        arrby[n] = (byte)(arrby[n] + ((curr[i - bytesPerPixel] & 255) + (prior[i] & 255)) / 2);
                    }
                    break;
                }
                case 4: {
                    int i;
                    for (i = 0; i < bytesPerPixel; ++i) {
                        byte[] arrby = curr;
                        int n = i;
                        arrby[n] = (byte)(arrby[n] + prior[i]);
                    }
                    i = bytesPerPixel;
                    while (i < bytesPerRow) {
                        int a = curr[i - bytesPerPixel] & 255;
                        int b = prior[i] & 255;
                        int c = prior[i - bytesPerPixel] & 255;
                        int p = a + b - c;
                        int pa = Math.abs(p - a);
                        int pb = Math.abs(p - b);
                        int pc = Math.abs(p - c);
                        int ret = pa <= pb && pa <= pc ? a : (pb <= pc ? b : c);
                        byte[] arrby = curr;
                        int n = i++;
                        arrby[n] = (byte)(arrby[n] + (byte)ret);
                    }
                    break;
                }
                default: {
                    throw new RuntimeException("PNG filter unknown.");
                }
            }
            try {
                fout.write(curr);
            }
            catch (IOException i) {
                // empty catch block
            }
            byte[] tmp = prior;
            prior = curr;
            curr = tmp;
        } while (true);
    }

    public static byte[] FlateDecode(byte[] in, boolean strict) {
        ByteArrayInputStream stream = new ByteArrayInputStream(in);
        InflaterInputStream zip = new InflaterInputStream(stream);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] b = new byte[strict ? 4092 : 1];
        try {
            int n;
            while ((n = zip.read(b)) >= 0) {
                out.write(b, 0, n);
            }
            zip.close();
            out.close();
            return out.toByteArray();
        }
        catch (Exception e) {
            if (strict) {
                return null;
            }
            return out.toByteArray();
        }
    }

    public static byte[] ASCIIHexDecode(byte[] in) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        boolean first = true;
        int n1 = 0;
        for (int k = 0; k < in.length; ++k) {
            int ch = in[k] & 255;
            if (ch == 62) break;
            if (PRTokeniser.isWhitespace(ch)) continue;
            int n = PRTokeniser.getHex(ch);
            if (n == -1) {
                throw new RuntimeException("Illegal character in ASCIIHexDecode.");
            }
            if (first) {
                n1 = n;
            } else {
                out.write((byte)((n1 << 4) + n));
            }
            first = !first;
        }
        if (!first) {
            out.write((byte)(n1 << 4));
        }
        return out.toByteArray();
    }

    public static byte[] ASCII85Decode(byte[] in) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int state = 0;
        int[] chn = new int[5];
        for (int k = 0; k < in.length; ++k) {
            int ch = in[k] & 255;
            if (ch == 126) break;
            if (PRTokeniser.isWhitespace(ch)) continue;
            if (ch == 122 && state == 0) {
                out.write(0);
                out.write(0);
                out.write(0);
                out.write(0);
                continue;
            }
            if (ch < 33 || ch > 117) {
                throw new RuntimeException("Illegal character in ASCII85Decode.");
            }
            chn[state] = ch - 33;
            if (++state != 5) continue;
            state = 0;
            int r = 0;
            for (int j = 0; j < 5; ++j) {
                r = r * 85 + chn[j];
            }
            out.write((byte)(r >> 24));
            out.write((byte)(r >> 16));
            out.write((byte)(r >> 8));
            out.write((byte)r);
        }
        int r = 0;
        if (state == 1) {
            throw new RuntimeException("Illegal length in ASCII85Decode.");
        }
        if (state == 2) {
            r = chn[0] * 85 * 85 * 85 * 85 + chn[1] * 85 * 85 * 85;
            out.write((byte)(r >> 24));
        } else if (state == 3) {
            r = chn[0] * 85 * 85 * 85 * 85 + chn[1] * 85 * 85 * 85 + chn[2] * 85 * 85;
            out.write((byte)(r >> 24));
            out.write((byte)(r >> 16));
        } else if (state == 4) {
            r = chn[0] * 85 * 85 * 85 * 85 + chn[1] * 85 * 85 * 85 + chn[2] * 85 * 85 + chn[3] * 85;
            out.write((byte)(r >> 24));
            out.write((byte)(r >> 16));
            out.write((byte)(r >> 8));
        }
        return out.toByteArray();
    }

    public static byte[] LZWDecode(byte[] in) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        LZWDecoder lzw = new LZWDecoder();
        lzw.decode(in, out);
        return out.toByteArray();
    }

    public boolean isRebuilt() {
        return this.rebuilt;
    }

    public PdfDictionary getPageN(int pageNum) {
        PdfDictionary dic = this.pageRefs.getPageN(pageNum);
        if (dic == null) {
            return null;
        }
        if (this.appendable) {
            dic.setIndRef(this.pageRefs.getPageOrigRef(pageNum));
        }
        return dic;
    }

    public PdfDictionary getPageNRelease(int pageNum) {
        PdfDictionary dic = this.getPageN(pageNum);
        this.pageRefs.releasePage(pageNum);
        return dic;
    }

    public void releasePage(int pageNum) {
        this.pageRefs.releasePage(pageNum);
    }

    public void resetReleasePage() {
        this.pageRefs.resetReleasePage();
    }

    public PRIndirectReference getPageOrigRef(int pageNum) {
        return this.pageRefs.getPageOrigRef(pageNum);
    }

    public byte[] getPageContent(int pageNum, RandomAccessFileOrArray file) throws IOException {
        PdfDictionary page = this.getPageNRelease(pageNum);
        if (page == null) {
            return null;
        }
        PdfObject contents = PdfReader.getPdfObjectRelease(page.get(PdfName.CONTENTS));
        if (contents == null) {
            return new byte[0];
        }
        ByteArrayOutputStream bout = null;
        if (contents.isStream()) {
            return PdfReader.getStreamBytes((PRStream)contents, file);
        }
        if (contents.isArray()) {
            PdfArray array = (PdfArray)contents;
            ArrayList list = array.getArrayList();
            bout = new ByteArrayOutputStream();
            for (int k = 0; k < list.size(); ++k) {
                PdfObject item = PdfReader.getPdfObjectRelease((PdfObject)list.get(k));
                if (item == null) continue;
                if (!item.isStream()) continue;
                byte[] b = PdfReader.getStreamBytes((PRStream)item, file);
                bout.write(b);
                if (k == list.size() - 1) continue;
                bout.write(10);
            }
            return bout.toByteArray();
        }
        return new byte[0];
    }

    public byte[] getPageContent(int pageNum) throws IOException {
        byte[] arrby;
        RandomAccessFileOrArray rf = this.getSafeFile();
        try {
            rf.reOpen();
            arrby = this.getPageContent(pageNum, rf);
            Object var3_4 = null;
        }
        catch (Throwable var4_8) {
            Object var3_5 = null;
            try {
                rf.close();
            }
            catch (Exception e) {
                // empty catch block
            }
            throw var4_8;
        }
        try {
            rf.close();
        }
        catch (Exception e) {
            // empty catch block
        }
        return arrby;
    }

    protected void killXref(PdfObject obj) {
        if (obj == null) {
            return;
        }
        if (obj instanceof PdfIndirectReference && !obj.isIndirect()) {
            return;
        }
        switch (obj.type()) {
            case 10: {
                int xr = ((PRIndirectReference)obj).getNumber();
                obj = (PdfObject)this.xrefObj.get(xr);
                this.xrefObj.set(xr, null);
                this.freeXref = xr;
                this.killXref(obj);
                break;
            }
            case 5: {
                ArrayList t = ((PdfArray)obj).getArrayList();
                for (int i = 0; i < t.size(); ++i) {
                    this.killXref((PdfObject)t.get(i));
                }
                break;
            }
            case 6: 
            case 7: {
                PdfDictionary dic = (PdfDictionary)obj;
                Iterator i = dic.getKeys().iterator();
                while (i.hasNext()) {
                    this.killXref(dic.get((PdfName)i.next()));
                }
                break block0;
            }
        }
    }

    public void setPageContent(int pageNum, byte[] content) throws IOException {
        PdfDictionary page = this.getPageN(pageNum);
        if (page == null) {
            return;
        }
        PdfObject contents = page.get(PdfName.CONTENTS);
        this.freeXref = -1;
        this.killXref(contents);
        if (this.freeXref == -1) {
            this.xrefObj.add(null);
            this.freeXref = this.xrefObj.size() - 1;
        }
        page.put(PdfName.CONTENTS, new PRIndirectReference(this, this.freeXref));
        this.xrefObj.set(this.freeXref, new PRStream(this, content));
    }

    public static byte[] getStreamBytes(PRStream stream, RandomAccessFileOrArray file) throws IOException {
        byte[] b;
        PdfReader reader = stream.getReader();
        PdfObject filter = PdfReader.getPdfObjectRelease(stream.get(PdfName.FILTER));
        if (stream.getOffset() < 0) {
            b = stream.getBytes();
        } else {
            b = new byte[stream.getLength()];
            file.seek(stream.getOffset());
            file.readFully(b);
            PdfEncryption decrypt = reader.getDecrypt();
            if (decrypt != null) {
                decrypt.setHashKey(stream.getObjNum(), stream.getObjGen());
                decrypt.prepareKey();
                decrypt.encryptRC4(b);
            }
        }
        ArrayList filters = new ArrayList();
        if (filter != null) {
            if (filter.isName()) {
                filters.add((PdfObject)filter);
            } else if (filter.isArray()) {
                filters = ((PdfArray)filter).getArrayList();
            }
        }
        ArrayList dp = new ArrayList();
        PdfObject dpo = PdfReader.getPdfObjectRelease(stream.get(PdfName.DECODEPARMS));
        if (!(dpo != null && (dpo.isDictionary() || dpo.isArray()))) {
            dpo = PdfReader.getPdfObjectRelease(stream.get(PdfName.DP));
        }
        if (dpo != null) {
            if (dpo.isDictionary()) {
                dp.add((PdfObject)dpo);
            } else if (dpo.isArray()) {
                dp = ((PdfArray)dpo).getArrayList();
            }
        }
        for (int j = 0; j < filters.size(); ++j) {
            PdfObject dicParam;
            String name = ((PdfName)PdfReader.getPdfObjectRelease((PdfObject)filters.get(j))).toString();
            if (name.equals("/FlateDecode") || name.equals("/Fl")) {
                b = PdfReader.FlateDecode(b);
                dicParam = null;
                if (j >= dp.size()) continue;
                dicParam = (PdfObject)dp.get(j);
                b = PdfReader.decodePredictor(b, dicParam);
                continue;
            }
            if (name.equals("/ASCIIHexDecode") || name.equals("/AHx")) {
                b = PdfReader.ASCIIHexDecode(b);
                continue;
            }
            if (name.equals("/ASCII85Decode") || name.equals("/A85")) {
                b = PdfReader.ASCII85Decode(b);
                continue;
            }
            if (name.equals("/LZWDecode")) {
                b = PdfReader.LZWDecode(b);
                dicParam = null;
                if (j >= dp.size()) continue;
                dicParam = (PdfObject)dp.get(j);
                b = PdfReader.decodePredictor(b, dicParam);
                continue;
            }
            throw new IOException("The filter " + name + " is not supported.");
        }
        return b;
    }

    public static byte[] getStreamBytes(PRStream stream) throws IOException {
        byte[] arrby;
        RandomAccessFileOrArray rf = stream.getReader().getSafeFile();
        try {
            rf.reOpen();
            arrby = PdfReader.getStreamBytes(stream, rf);
            Object var2_3 = null;
        }
        catch (Throwable var3_7) {
            Object var2_4 = null;
            try {
                rf.close();
            }
            catch (Exception e) {
                // empty catch block
            }
            throw var3_7;
        }
        try {
            rf.close();
        }
        catch (Exception e) {
            // empty catch block
        }
        return arrby;
    }

    public void eliminateSharedStreams() {
        int k;
        if (!this.sharedStreams) {
            return;
        }
        this.sharedStreams = false;
        if (this.pageRefs.size() == 1) {
            return;
        }
        ArrayList<PRIndirectReference> newRefs = new ArrayList<PRIndirectReference>();
        ArrayList<PRStream> newStreams = new ArrayList<PRStream>();
        IntHashtable visited = new IntHashtable();
        for (k = 1; k <= this.pageRefs.size(); ++k) {
            PdfObject contents;
            PdfDictionary page = this.pageRefs.getPageN(k);
            if (page == null || (contents = PdfReader.getPdfObject(page.get(PdfName.CONTENTS))) == null) continue;
            if (contents.isStream()) {
                PRIndirectReference ref = (PRIndirectReference)page.get(PdfName.CONTENTS);
                if (visited.containsKey(ref.getNumber())) {
                    newRefs.add(ref);
                    newStreams.add(new PRStream((PRStream)contents, null));
                    continue;
                }
                visited.put(ref.getNumber(), 1);
                continue;
            }
            PdfArray array = (PdfArray)contents;
            ArrayList list = array.getArrayList();
            for (int j = 0; j < list.size(); ++j) {
                PRIndirectReference ref = (PRIndirectReference)list.get(j);
                if (visited.containsKey(ref.getNumber())) {
                    newRefs.add(ref);
                    newStreams.add(new PRStream((PRStream)PdfReader.getPdfObject(ref), null));
                    continue;
                }
                visited.put(ref.getNumber(), 1);
            }
        }
        if (newStreams.size() == 0) {
            return;
        }
        for (k = 0; k < newStreams.size(); ++k) {
            this.xrefObj.add(newStreams.get(k));
            PRIndirectReference ref = (PRIndirectReference)newRefs.get(k);
            ref.setNumber(this.xrefObj.size() - 1, 0);
        }
    }

    public boolean isTampered() {
        return this.tampered;
    }

    public void setTampered(boolean tampered) {
        this.tampered = tampered;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    public byte[] getMetadata() throws IOException {
        obj = PdfReader.getPdfObject(this.catalog.get(PdfName.METADATA));
        if (!(obj instanceof PRStream)) {
            return null;
        }
        rf = this.getSafeFile();
        b = null;
        try {
            rf.reOpen();
            b = PdfReader.getStreamBytes((PRStream)obj, rf);
            var4_6 = null;
        }
        catch (Throwable var5_4) {
            var4_5 = null;
            try {
                rf.close();
                throw var5_4;
            }
            catch (Exception e) {
                // empty catch block
            }
            throw var5_4;
        }
        ** try [egrp 1[TRYBLOCK] [2 : 59->66)] { 
lbl22: // 1 sources:
        rf.close();
        return b;
lbl24: // 1 sources:
        catch (Exception e) {
            // empty catch block
        }
        return b;
    }

    public int getLastXref() {
        return this.lastXref;
    }

    public int getXrefSize() {
        return this.xrefObj.size();
    }

    public int getEofPos() {
        return this.eofPos;
    }

    public char getPdfVersion() {
        return this.pdfVersion;
    }

    public boolean isEncrypted() {
        return this.encrypted;
    }

    public int getPermissions() {
        return this.pValue;
    }

    public boolean is128Key() {
        if (this.rValue == 3) {
            return true;
        }
        return false;
    }

    public PdfDictionary getTrailer() {
        return this.trailer;
    }

    PdfEncryption getDecrypt() {
        return this.decrypt;
    }

    static boolean equalsn(byte[] a1, byte[] a2) {
        int length = a2.length;
        for (int k = 0; k < length; ++k) {
            if (a1[k] == a2[k]) continue;
            return false;
        }
        return true;
    }

    static boolean existsName(PdfDictionary dic, PdfName key, PdfName value) {
        PdfObject type = PdfReader.getPdfObjectRelease(dic.get(key));
        if (!(type != null && type.isName())) {
            return false;
        }
        PdfName name = (PdfName)type;
        return name.equals(value);
    }

    static String getFontName(PdfDictionary dic) {
        PdfObject type = PdfReader.getPdfObjectRelease(dic.get(PdfName.BASEFONT));
        if (!(type != null && type.isName())) {
            return null;
        }
        return PdfName.decodeName(type.toString());
    }

    static String getSubsetPrefix(PdfDictionary dic) {
        String s = PdfReader.getFontName(dic);
        if (s == null) {
            return null;
        }
        if (s.length() < 8 || s.charAt(6) != '+') {
            return null;
        }
        for (int k = 0; k < 6; ++k) {
            char c = s.charAt(k);
            if (c >= 'A' && c <= 'Z') continue;
            return null;
        }
        return s;
    }

    public int shuffleSubsetNames() {
        int total = 0;
        for (int k = 1; k < this.xrefObj.size(); ++k) {
            String s;
            ArrayList list;
            PdfDictionary dic;
            PdfObject obj = this.getPdfObjectRelease(k);
            if (obj == null || !obj.isDictionary() || !PdfReader.existsName(dic = (PdfDictionary)obj, PdfName.TYPE, PdfName.FONT)) continue;
            if (PdfReader.existsName(dic, PdfName.SUBTYPE, PdfName.TYPE1) || PdfReader.existsName(dic, PdfName.SUBTYPE, PdfName.MMTYPE1) || PdfReader.existsName(dic, PdfName.SUBTYPE, PdfName.TRUETYPE)) {
                s = PdfReader.getSubsetPrefix(dic);
                if (s == null) continue;
                String ns = String.valueOf(BaseFont.createSubsetPrefix()) + s.substring(7);
                PdfName newName = new PdfName(ns);
                dic.put(PdfName.BASEFONT, newName);
                this.setXrefPartialObject(k, dic);
                ++total;
                PdfDictionary fd = (PdfDictionary)PdfReader.getPdfObject(dic.get(PdfName.FONTDESCRIPTOR));
                if (fd == null) continue;
                fd.put(PdfName.FONTNAME, newName);
                continue;
            }
            if (!PdfReader.existsName(dic, PdfName.SUBTYPE, PdfName.TYPE0)) continue;
            s = PdfReader.getSubsetPrefix(dic);
            PdfArray arr = (PdfArray)PdfReader.getPdfObject(dic.get(PdfName.DESCENDANTFONTS));
            if (arr == null || (list = arr.getArrayList()).size() == 0) continue;
            PdfDictionary desc = (PdfDictionary)PdfReader.getPdfObject((PdfObject)list.get(0));
            String sde = PdfReader.getSubsetPrefix(desc);
            if (sde == null) continue;
            String ns = BaseFont.createSubsetPrefix();
            if (s != null) {
                dic.put(PdfName.BASEFONT, new PdfName(String.valueOf(ns) + s.substring(7)));
            }
            this.setXrefPartialObject(k, dic);
            PdfName newName = new PdfName(String.valueOf(ns) + sde.substring(7));
            desc.put(PdfName.BASEFONT, newName);
            ++total;
            PdfDictionary fd = (PdfDictionary)PdfReader.getPdfObject(desc.get(PdfName.FONTDESCRIPTOR));
            if (fd == null) continue;
            fd.put(PdfName.FONTNAME, newName);
        }
        return total;
    }

    public int createFakeFontSubsets() {
        int total = 0;
        for (int k = 1; k < this.xrefObj.size(); ++k) {
            String s;
            PdfDictionary dic;
            PdfObject obj = this.getPdfObjectRelease(k);
            if (obj == null || !obj.isDictionary() || !PdfReader.existsName(dic = (PdfDictionary)obj, PdfName.TYPE, PdfName.FONT) || !PdfReader.existsName(dic, PdfName.SUBTYPE, PdfName.TYPE1) && !PdfReader.existsName(dic, PdfName.SUBTYPE, PdfName.MMTYPE1) && !PdfReader.existsName(dic, PdfName.SUBTYPE, PdfName.TRUETYPE) || (s = PdfReader.getSubsetPrefix(dic)) != null) continue;
            s = PdfReader.getFontName(dic);
            if (s == null) continue;
            String ns = String.valueOf(BaseFont.createSubsetPrefix()) + s;
            PdfDictionary fd = (PdfDictionary)PdfReader.getPdfObjectRelease(dic.get(PdfName.FONTDESCRIPTOR));
            if (fd == null) continue;
            if (fd.get(PdfName.FONTFILE) == null && fd.get(PdfName.FONTFILE2) == null && fd.get(PdfName.FONTFILE3) == null) continue;
            fd = (PdfDictionary)PdfReader.getPdfObject(dic.get(PdfName.FONTDESCRIPTOR));
            PdfName newName = new PdfName(ns);
            dic.put(PdfName.BASEFONT, newName);
            fd.put(PdfName.FONTNAME, newName);
            this.setXrefPartialObject(k, dic);
            ++total;
        }
        return total;
    }

    private static PdfArray getNameArray(PdfObject obj) {
        PdfObject arr2;
        if (obj == null) {
            return null;
        }
        if ((obj = PdfReader.getPdfObjectRelease(obj)).isArray()) {
            return (PdfArray)obj;
        }
        if (obj.isDictionary() && (arr2 = PdfReader.getPdfObjectRelease(((PdfDictionary)obj).get(PdfName.D))) != null && arr2.isArray()) {
            return (PdfArray)arr2;
        }
        return null;
    }

    public HashMap getNamedDestination() {
        HashMap names = this.getNamedDestinationFromNames();
        names.putAll(this.getNamedDestinationFromStrings());
        return names;
    }

    public HashMap getNamedDestinationFromNames() {
        HashMap<String, PdfArray> names = new HashMap<String, PdfArray>();
        if (this.catalog.get(PdfName.DESTS) != null) {
            PdfDictionary dic = (PdfDictionary)PdfReader.getPdfObjectRelease(this.catalog.get(PdfName.DESTS));
            Set keys = dic.getKeys();
            Iterator it = keys.iterator();
            while (it.hasNext()) {
                PdfName key = (PdfName)it.next();
                String name = PdfName.decodeName(key.toString());
                PdfArray arr = PdfReader.getNameArray(dic.get(key));
                if (arr == null) continue;
                names.put(name, arr);
            }
        }
        return names;
    }

    public HashMap getNamedDestinationFromStrings() {
        if (this.catalog.get(PdfName.NAMES) != null) {
            PdfDictionary dic = (PdfDictionary)PdfReader.getPdfObjectRelease(this.catalog.get(PdfName.NAMES));
            if ((dic = (PdfDictionary)PdfReader.getPdfObjectRelease(dic.get(PdfName.DESTS))) != null) {
                HashMap names = PdfNameTree.readTree(dic);
                Iterator it = names.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry entry = it.next();
                    PdfArray arr = PdfReader.getNameArray((PdfObject)entry.getValue());
                    if (arr != null) {
                        entry.setValue(arr);
                        continue;
                    }
                    it.remove();
                }
                return names;
            }
        }
        return new HashMap();
    }

    private boolean replaceNamedDestination(PdfObject obj, HashMap names) {
        obj = PdfReader.getPdfObject(obj);
        int objIdx = this.lastXrefPartial;
        this.releaseLastXrefPartial();
        if (obj != null && obj.isDictionary()) {
            PdfObject ob2 = PdfReader.getPdfObjectRelease(((PdfDictionary)obj).get(PdfName.DEST));
            String name = null;
            if (ob2 != null) {
                if (ob2.isName()) {
                    name = PdfName.decodeName(ob2.toString());
                } else if (ob2.isString()) {
                    name = ob2.toString();
                }
                PdfArray dest = (PdfArray)names.get(name);
                if (dest != null) {
                    ((PdfDictionary)obj).put(PdfName.DEST, dest);
                    this.setXrefPartialObject(objIdx, obj);
                    return true;
                }
            } else {
                ob2 = PdfReader.getPdfObject(((PdfDictionary)obj).get(PdfName.A));
                if (ob2 != null) {
                    int obj2Idx = this.lastXrefPartial;
                    this.releaseLastXrefPartial();
                    PdfDictionary dic = (PdfDictionary)ob2;
                    PdfName type = (PdfName)PdfReader.getPdfObjectRelease(dic.get(PdfName.S));
                    if (PdfName.GOTO.equals(type)) {
                        PdfObject ob3 = PdfReader.getPdfObjectRelease(dic.get(PdfName.D));
                        if (ob3.isName()) {
                            name = PdfName.decodeName(ob3.toString());
                        } else if (ob3.isString()) {
                            name = ob3.toString();
                        }
                        PdfArray dest = (PdfArray)names.get(name);
                        if (dest != null) {
                            dic.put(PdfName.D, dest);
                            this.setXrefPartialObject(obj2Idx, ob2);
                            this.setXrefPartialObject(objIdx, obj);
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public void removeFields() {
        this.pageRefs.resetReleasePage();
        for (int k = 1; k <= this.pageRefs.size(); ++k) {
            PdfDictionary page = this.pageRefs.getPageN(k);
            PdfArray annots = (PdfArray)PdfReader.getPdfObject(page.get(PdfName.ANNOTS));
            if (annots == null) {
                this.pageRefs.releasePage(k);
                continue;
            }
            ArrayList arr = annots.getArrayList();
            int startSize = arr.size();
            for (int j = 0; j < arr.size(); ++j) {
                PdfDictionary annot = (PdfDictionary)PdfReader.getPdfObjectRelease((PdfObject)arr.get(j));
                if (!PdfName.WIDGET.equals(annot.get(PdfName.SUBTYPE))) continue;
                arr.remove(j--);
            }
            if (arr.isEmpty()) {
                page.remove(PdfName.ANNOTS);
                continue;
            }
            this.pageRefs.releasePage(k);
        }
        this.catalog.remove(PdfName.ACROFORM);
        this.pageRefs.resetReleasePage();
    }

    public void removeAnnotations() {
        this.pageRefs.resetReleasePage();
        for (int k = 1; k <= this.pageRefs.size(); ++k) {
            PdfDictionary page = this.pageRefs.getPageN(k);
            if (page.get(PdfName.ANNOTS) == null) {
                this.pageRefs.releasePage(k);
                continue;
            }
            page.remove(PdfName.ANNOTS);
        }
        this.catalog.remove(PdfName.ACROFORM);
        this.pageRefs.resetReleasePage();
    }

    private void iterateBookmarks(PdfObject outlineRef, HashMap names) {
        while (outlineRef != null) {
            this.replaceNamedDestination(outlineRef, names);
            PdfDictionary outline = (PdfDictionary)PdfReader.getPdfObjectRelease(outlineRef);
            PdfObject first = outline.get(PdfName.FIRST);
            if (first != null) {
                this.iterateBookmarks(first, names);
            }
            outlineRef = outline.get(PdfName.NEXT);
        }
    }

    public void consolidateNamedDestinations() {
        if (this.consolidateNamedDestinations) {
            return;
        }
        this.consolidateNamedDestinations = true;
        HashMap names = this.getNamedDestination();
        if (names.size() == 0) {
            return;
        }
        for (int k = 1; k <= this.pageRefs.size(); ++k) {
            PdfDictionary page = this.pageRefs.getPageN(k);
            PdfObject annotsRef = page.get(PdfName.ANNOTS);
            PdfArray annots = (PdfArray)PdfReader.getPdfObject(annotsRef);
            int annotIdx = this.lastXrefPartial;
            this.releaseLastXrefPartial();
            if (annots == null) {
                this.pageRefs.releasePage(k);
                continue;
            }
            ArrayList list = annots.getArrayList();
            boolean commitAnnots = false;
            for (int an = 0; an < list.size(); ++an) {
                PdfObject objRef = (PdfObject)list.get(an);
                if (!this.replaceNamedDestination(objRef, names) || objRef.isIndirect()) continue;
                commitAnnots = true;
            }
            if (commitAnnots) {
                this.setXrefPartialObject(annotIdx, annots);
            }
            if (commitAnnots && !annotsRef.isIndirect()) continue;
            this.pageRefs.releasePage(k);
        }
        PdfDictionary outlines = (PdfDictionary)PdfReader.getPdfObjectRelease(this.catalog.get(PdfName.OUTLINES));
        if (outlines == null) {
            return;
        }
        this.iterateBookmarks(outlines.get(PdfName.FIRST), names);
    }

    protected static PdfDictionary duplicatePdfDictionary(PdfDictionary original, PdfDictionary copy, PdfReader newReader) {
        if (copy == null) {
            copy = new PdfDictionary();
        }
        Iterator it = original.getKeys().iterator();
        while (it.hasNext()) {
            PdfName key = (PdfName)it.next();
            copy.put(key, PdfReader.duplicatePdfObject(original.get(key), newReader));
        }
        return copy;
    }

    protected static PdfObject duplicatePdfObject(PdfObject original, PdfReader newReader) {
        if (original == null) {
            return null;
        }
        switch (original.type()) {
            case 6: {
                return PdfReader.duplicatePdfDictionary((PdfDictionary)original, null, newReader);
            }
            case 7: {
                PRStream org = (PRStream)original;
                PRStream stream = new PRStream(org, null, newReader);
                PdfReader.duplicatePdfDictionary(org, stream, newReader);
                return stream;
            }
            case 5: {
                ArrayList list = ((PdfArray)original).getArrayList();
                PdfArray arr = new PdfArray();
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    arr.add(PdfReader.duplicatePdfObject((PdfObject)it.next(), newReader));
                }
                return arr;
            }
            case 10: {
                PRIndirectReference org = (PRIndirectReference)original;
                return new PRIndirectReference(newReader, org.getNumber(), org.getGeneration());
            }
        }
        return original;
    }

    public void close() {
        if (!this.partial) {
            return;
        }
        try {
            this.tokens.close();
        }
        catch (IOException e) {
            throw new ExceptionConverter(e);
        }
    }

    protected void removeUnusedNode(PdfObject obj, boolean[] hits) {
        if (obj == null) {
            return;
        }
        switch (obj.type()) {
            case 6: 
            case 7: {
                PdfDictionary dic = (PdfDictionary)obj;
                Iterator it = dic.getKeys().iterator();
                while (it.hasNext()) {
                    int num;
                    PdfName key = (PdfName)it.next();
                    PdfObject v = dic.get(key);
                    if (v.isIndirect() && ((num = ((PRIndirectReference)v).getNumber()) >= this.xrefObj.size() || this.xrefObj.get(num) == null)) {
                        dic.put(key, PdfNull.PDFNULL);
                        continue;
                    }
                    this.removeUnusedNode(v, hits);
                }
                break;
            }
            case 5: {
                ArrayList list = ((PdfArray)obj).getArrayList();
                for (int k = 0; k < list.size(); ++k) {
                    int num;
                    PdfObject v = (PdfObject)list.get(k);
                    if (v.isIndirect() && this.xrefObj.get(num = ((PRIndirectReference)v).getNumber()) == null) {
                        list.set(k, PdfNull.PDFNULL);
                        continue;
                    }
                    this.removeUnusedNode(v, hits);
                }
                break;
            }
            case 10: {
                PRIndirectReference ref = (PRIndirectReference)obj;
                int num = ref.getNumber();
                if (hits[num]) break;
                hits[num] = true;
                this.removeUnusedNode(PdfReader.getPdfObjectRelease(ref), hits);
            }
        }
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public int removeUnusedObjects() {
        boolean[] hits = new boolean[this.xrefObj.size()];
        this.removeUnusedNode(this.trailer, hits);
        int total = 0;
        if (this.partial) {
            for (int k = 1; k < hits.length; ++k) {
                if (hits[k]) continue;
                this.xref[k * 2] = -1;
                this.xref[k * 2 + 1] = 0;
                this.xrefObj.set(k, null);
                ++total;
            }
            return total;
        } else {
            for (int k = 1; k < hits.length; ++k) {
                if (hits[k]) continue;
                this.xrefObj.set(k, null);
                ++total;
            }
        }
        return total;
    }

    public AcroFields getAcroFields() {
        return new AcroFields(this, null);
    }

    public String getJavaScript(RandomAccessFileOrArray file) throws IOException {
        PdfDictionary names = (PdfDictionary)PdfReader.getPdfObjectRelease(this.catalog.get(PdfName.NAMES));
        if (names == null) {
            return null;
        }
        PdfDictionary js = (PdfDictionary)PdfReader.getPdfObjectRelease(names.get(PdfName.JAVASCRIPT));
        if (js == null) {
            return null;
        }
        HashMap jscript = PdfNameTree.readTree(js);
        String[] sortedNames = new String[jscript.size()];
        sortedNames = jscript.keySet().toArray(sortedNames);
        Arrays.sort(sortedNames, new StringCompare());
        StringBuffer buf = new StringBuffer();
        for (int k = 0; k < sortedNames.length; ++k) {
            PdfDictionary j = (PdfDictionary)PdfReader.getPdfObjectRelease((PdfIndirectReference)jscript.get(sortedNames[k]));
            if (j == null) continue;
            PdfObject obj = PdfReader.getPdfObjectRelease(j.get(PdfName.JS));
            if (obj.isString()) {
                buf.append(((PdfString)obj).toUnicodeString()).append('\n');
                continue;
            }
            if (!obj.isStream()) continue;
            byte[] bytes = PdfReader.getStreamBytes((PRStream)obj, file);
            if (bytes.length >= 2 && bytes[0] == -2 && bytes[1] == -1) {
                buf.append(PdfEncodings.convertToString(bytes, "UnicodeBig"));
            } else {
                buf.append(PdfEncodings.convertToString(bytes, "PDF"));
            }
            buf.append('\n');
        }
        return buf.toString();
    }

    public String getJavaScript() throws IOException {
        String string;
        RandomAccessFileOrArray rf = this.getSafeFile();
        try {
            rf.reOpen();
            string = this.getJavaScript(rf);
            Object var2_3 = null;
        }
        catch (Throwable var3_7) {
            Object var2_4 = null;
            try {
                rf.close();
            }
            catch (Exception e) {
                // empty catch block
            }
            throw var3_7;
        }
        try {
            rf.close();
        }
        catch (Exception e) {
            // empty catch block
        }
        return string;
    }

    public void selectPages(String ranges) {
        this.selectPages(SequenceList.expand(ranges, this.getNumberOfPages()));
    }

    public void selectPages(List pagesToKeep) {
        throw new UnsupportedOperationException("Later.");
    }

    public static void setViewerPreferences(int preferences, PdfDictionary catalog) {
        catalog.remove(PdfName.PAGELAYOUT);
        catalog.remove(PdfName.PAGEMODE);
        catalog.remove(PdfName.VIEWERPREFERENCES);
        if ((preferences & 1) != 0) {
            catalog.put(PdfName.PAGELAYOUT, PdfName.SINGLEPAGE);
        } else if ((preferences & 2) != 0) {
            catalog.put(PdfName.PAGELAYOUT, PdfName.ONECOLUMN);
        } else if ((preferences & 4) != 0) {
            catalog.put(PdfName.PAGELAYOUT, PdfName.TWOCOLUMNLEFT);
        } else if ((preferences & 8) != 0) {
            catalog.put(PdfName.PAGELAYOUT, PdfName.TWOCOLUMNRIGHT);
        }
        if ((preferences & 16) != 0) {
            catalog.put(PdfName.PAGEMODE, PdfName.USENONE);
        } else if ((preferences & 32) != 0) {
            catalog.put(PdfName.PAGEMODE, PdfName.USEOUTLINES);
        } else if ((preferences & 64) != 0) {
            catalog.put(PdfName.PAGEMODE, PdfName.USETHUMBS);
        } else if ((preferences & 128) != 0) {
            catalog.put(PdfName.PAGEMODE, PdfName.FULLSCREEN);
        } else if ((preferences & 1048576) != 0) {
            catalog.put(PdfName.PAGEMODE, PdfName.USEOC);
        }
        if ((preferences & 16776960) == 0) {
            return;
        }
        PdfDictionary vp = new PdfDictionary();
        if ((preferences & 256) != 0) {
            vp.put(PdfName.HIDETOOLBAR, PdfBoolean.PDFTRUE);
        }
        if ((preferences & 512) != 0) {
            vp.put(PdfName.HIDEMENUBAR, PdfBoolean.PDFTRUE);
        }
        if ((preferences & 1024) != 0) {
            vp.put(PdfName.HIDEWINDOWUI, PdfBoolean.PDFTRUE);
        }
        if ((preferences & 2048) != 0) {
            vp.put(PdfName.FITWINDOW, PdfBoolean.PDFTRUE);
        }
        if ((preferences & 4096) != 0) {
            vp.put(PdfName.CENTERWINDOW, PdfBoolean.PDFTRUE);
        }
        if ((preferences & 262144) != 0) {
            vp.put(PdfName.DISPLAYDOCTITLE, PdfBoolean.PDFTRUE);
        }
        if ((preferences & 8192) != 0) {
            vp.put(PdfName.NONFULLSCREENPAGEMODE, PdfName.USENONE);
        } else if ((preferences & 16384) != 0) {
            vp.put(PdfName.NONFULLSCREENPAGEMODE, PdfName.USEOUTLINES);
        } else if ((preferences & 32768) != 0) {
            vp.put(PdfName.NONFULLSCREENPAGEMODE, PdfName.USETHUMBS);
        } else if ((preferences & 524288) != 0) {
            vp.put(PdfName.NONFULLSCREENPAGEMODE, PdfName.USEOC);
        }
        if ((preferences & 65536) != 0) {
            vp.put(PdfName.DIRECTION, PdfName.L2R);
        } else if ((preferences & 131072) != 0) {
            vp.put(PdfName.DIRECTION, PdfName.R2L);
        }
        if ((preferences & 1048576) != 0) {
            vp.put(PdfName.PRINTSCALING, PdfName.NONE);
        }
        catalog.put(PdfName.VIEWERPREFERENCES, vp);
    }

    public void setViewerPreferences(int preferences) {
        PdfReader.setViewerPreferences(preferences, this.catalog);
    }

    public int getViewerPreferences() {
        int prefs = 0;
        PdfName name = null;
        PdfObject obj = PdfReader.getPdfObjectRelease(this.catalog.get(PdfName.PAGELAYOUT));
        if (obj != null && obj.isName()) {
            name = (PdfName)obj;
            if (name.equals(PdfName.SINGLEPAGE)) {
                prefs|=true;
            } else if (name.equals(PdfName.ONECOLUMN)) {
                prefs|=2;
            } else if (name.equals(PdfName.TWOCOLUMNLEFT)) {
                prefs|=4;
            } else if (name.equals(PdfName.TWOCOLUMNRIGHT)) {
                prefs|=8;
            }
        }
        if ((obj = PdfReader.getPdfObjectRelease(this.catalog.get(PdfName.PAGEMODE))) != null && obj.isName()) {
            name = (PdfName)obj;
            if (name.equals(PdfName.USENONE)) {
                prefs|=16;
            } else if (name.equals(PdfName.USEOUTLINES)) {
                prefs|=32;
            } else if (name.equals(PdfName.USETHUMBS)) {
                prefs|=64;
            } else if (name.equals(PdfName.USEOC)) {
                prefs|=1048576;
            }
        }
        if (!((obj = PdfReader.getPdfObjectRelease(this.catalog.get(PdfName.VIEWERPREFERENCES))) != null && obj.isDictionary())) {
            return prefs;
        }
        PdfDictionary vp = (PdfDictionary)obj;
        for (int k = 0; k < vpnames.length; ++k) {
            obj = PdfReader.getPdfObject(vp.get(vpnames[k]));
            if (obj == null || !"true".equals(obj.toString())) continue;
            prefs|=vpints[k];
        }
        obj = PdfReader.getPdfObjectRelease(vp.get(PdfName.PRINTSCALING));
        if (PdfName.NONE.equals(obj)) {
            prefs|=1048576;
        }
        if ((obj = PdfReader.getPdfObjectRelease(vp.get(PdfName.NONFULLSCREENPAGEMODE))) != null && obj.isName()) {
            name = (PdfName)obj;
            if (name.equals(PdfName.USENONE)) {
                prefs|=8192;
            } else if (name.equals(PdfName.USEOUTLINES)) {
                prefs|=16384;
            } else if (name.equals(PdfName.USETHUMBS)) {
                prefs|=32768;
            } else if (name.equals(PdfName.USEOC)) {
                prefs|=524288;
            }
        }
        if ((obj = PdfReader.getPdfObjectRelease(vp.get(PdfName.DIRECTION))) != null && obj.isName()) {
            name = (PdfName)obj;
            if (name.equals(PdfName.L2R)) {
                prefs|=65536;
            } else if (name.equals(PdfName.R2L)) {
                prefs|=131072;
            }
        }
        return prefs;
    }

    public boolean isAppendable() {
        return this.appendable;
    }

    public void setAppendable(boolean appendable) {
        this.appendable = appendable;
        if (appendable) {
            PdfReader.getPdfObject(this.trailer.get(PdfName.ROOT));
        }
    }

    public boolean isNewXrefType() {
        return this.newXrefType;
    }

    public int getFileLength() {
        return this.fileLength;
    }

    public boolean isHybridXref() {
        return this.hybridXref;
    }

    static /* synthetic */ void access$2(PdfReader pdfReader, int n) {
        pdfReader.lastXrefPartial = n;
    }

    static class PageRefs {
        private PdfReader reader;
        private IntHashtable refsp;
        private ArrayList refsn;
        private ArrayList pageInh;
        private int lastPageRead;
        private int sizep;

        private PageRefs(PdfReader reader) throws IOException {
            this.lastPageRead = -1;
            this.reader = reader;
            if (reader.partial) {
                this.refsp = new IntHashtable();
                PdfNumber npages = (PdfNumber)PdfReader.getPdfObjectRelease(reader.rootPages.get(PdfName.COUNT));
                this.sizep = npages.intValue();
            } else {
                this.readPages();
            }
        }

        PageRefs(PageRefs other, PdfReader reader) {
            this.lastPageRead = -1;
            this.reader = reader;
            this.sizep = other.sizep;
            if (this.refsn != null) {
                this.refsn = new ArrayList(other.refsn);
                for (int k = 0; k < this.refsn.size(); ++k) {
                    this.refsn.set(k, PdfReader.duplicatePdfObject((PdfObject)this.refsn.get(k), reader));
                }
            } else {
                this.refsp = (IntHashtable)other.refsp.clone();
            }
        }

        int size() {
            if (this.refsn != null) {
                return this.refsn.size();
            }
            return this.sizep;
        }

        void readPages() throws IOException {
            if (this.refsn != null) {
                return;
            }
            this.refsp = null;
            this.refsn = new ArrayList();
            this.pageInh = new ArrayList();
            this.iteratePages((PRIndirectReference)this.reader.catalog.get(PdfName.PAGES));
            this.pageInh = null;
            this.reader.rootPages.put(PdfName.COUNT, new PdfNumber(this.refsn.size()));
        }

        void reReadPages() throws IOException {
            this.refsn = null;
            this.readPages();
        }

        public PdfDictionary getPageN(int pageNum) {
            PRIndirectReference ref = this.getPageOrigRef(pageNum);
            return (PdfDictionary)PdfReader.getPdfObject(ref);
        }

        public PdfDictionary getPageNRelease(int pageNum) {
            PdfDictionary page = this.getPageN(pageNum);
            this.releasePage(pageNum);
            return page;
        }

        public PRIndirectReference getPageOrigRefRelease(int pageNum) {
            PRIndirectReference ref = this.getPageOrigRef(pageNum);
            this.releasePage(pageNum);
            return ref;
        }

        public PRIndirectReference getPageOrigRef(int pageNum) {
            try {
                if (--pageNum < 0 || pageNum >= this.size()) {
                    return null;
                }
                if (this.refsn != null) {
                    return (PRIndirectReference)this.refsn.get(pageNum);
                }
                int n = this.refsp.get(pageNum);
                if (n == 0) {
                    PRIndirectReference ref = this.getSinglePage(pageNum);
                    this.lastPageRead = this.reader.lastXrefPartial == -1 ? -1 : pageNum;
                    PdfReader.access$2(this.reader, -1);
                    this.refsp.put(pageNum, ref.getNumber());
                    return ref;
                }
                if (this.lastPageRead != pageNum) {
                    this.lastPageRead = -1;
                }
                return new PRIndirectReference(this.reader, n);
            }
            catch (Exception e) {
                throw new ExceptionConverter(e);
            }
        }

        public void releasePage(int pageNum) {
            if (this.refsp == null) {
                return;
            }
            if (--pageNum < 0 || pageNum >= this.size()) {
                return;
            }
            if (pageNum != this.lastPageRead) {
                return;
            }
            this.lastPageRead = -1;
            PdfReader.access$2(this.reader, this.refsp.get(pageNum));
            this.reader.releaseLastXrefPartial();
            this.refsp.remove(pageNum);
        }

        public void resetReleasePage() {
            if (this.refsp == null) {
                return;
            }
            this.lastPageRead = -1;
        }

        void insertPage(int pageNum, PRIndirectReference ref) {
            --pageNum;
            if (this.refsn != null) {
                if (pageNum >= this.refsn.size()) {
                    this.refsn.add(ref);
                } else {
                    this.refsn.add(pageNum, ref);
                }
            } else {
                ++this.sizep;
                this.lastPageRead = -1;
                if (pageNum >= this.size()) {
                    this.refsp.put(this.size(), ref.getNumber());
                } else {
                    IntHashtable refs2 = new IntHashtable((this.refsp.size() + 1) * 2);
                    Iterator it = this.refsp.getEntryIterator();
                    while (it.hasNext()) {
                        IntHashtable.IntHashtableEntry entry = (IntHashtable.IntHashtableEntry)it.next();
                        int p = entry.getKey();
                        refs2.put(p >= pageNum ? p + 1 : p, entry.getValue());
                    }
                    refs2.put(pageNum, ref.getNumber());
                    this.refsp = refs2;
                }
            }
        }

        private void pushPageAttributes(PdfDictionary nodePages) {
            PdfDictionary dic = new PdfDictionary();
            if (this.pageInh.size() != 0) {
                dic.putAll((PdfDictionary)this.pageInh.get(this.pageInh.size() - 1));
            }
            for (int k = 0; k < PdfReader.pageInhCandidates.length; ++k) {
                PdfObject obj = nodePages.get(PdfReader.pageInhCandidates[k]);
                if (obj == null) continue;
                dic.put(PdfReader.pageInhCandidates[k], obj);
            }
            this.pageInh.add(dic);
        }

        private void popPageAttributes() {
            this.pageInh.remove(this.pageInh.size() - 1);
        }

        private void iteratePages(PRIndirectReference rpage) throws IOException {
            PdfDictionary page = (PdfDictionary)PdfReader.getPdfObject(rpage);
            PdfArray kidsPR = (PdfArray)PdfReader.getPdfObject(page.get(PdfName.KIDS));
            if (kidsPR == null) {
                page.put(PdfName.TYPE, PdfName.PAGE);
                PdfDictionary dic = (PdfDictionary)this.pageInh.get(this.pageInh.size() - 1);
                Iterator i = dic.getKeys().iterator();
                while (i.hasNext()) {
                    PdfName key = (PdfName)i.next();
                    if (page.get(key) != null) continue;
                    page.put(key, dic.get(key));
                }
                if (page.get(PdfName.MEDIABOX) == null) {
                    PdfArray arr = new PdfArray(new float[]{0.0f, 0.0f, PageSize.LETTER.right(), PageSize.LETTER.top()});
                    page.put(PdfName.MEDIABOX, arr);
                }
                this.refsn.add(rpage);
            } else {
                page.put(PdfName.TYPE, PdfName.PAGES);
                this.pushPageAttributes(page);
                ArrayList kids = kidsPR.getArrayList();
                for (int k = 0; k < kids.size(); ++k) {
                    this.iteratePages((PRIndirectReference)kids.get(k));
                }
                this.popPageAttributes();
            }
        }

        /*
         * Unable to fully structure code
         * Enabled aggressive block sorting
         * Lifted jumps to return sites
         */
        protected PRIndirectReference getSinglePage(int n) throws IOException {
            acc = new PdfDictionary();
            top = this.reader.rootPages;
            base = 0;
            do lbl-1000: // 5 sources:
            {
                for (k = 0; k < PdfReader.pageInhCandidates.length; ++k) {
                    obj = top.get(PdfReader.pageInhCandidates[k]);
                    if (obj == null) continue;
                    acc.put(PdfReader.pageInhCandidates[k], obj);
                }
                kids = (PdfArray)PdfReader.getPdfObjectRelease(top.get(PdfName.KIDS));
                it = kids.listIterator();
                do {
                    if (!it.hasNext()) ** continue;
                    ref = (PRIndirectReference)it.next();
                    dic = (PdfDictionary)PdfReader.getPdfObject(ref);
                    last = PdfReader.access$1(this.reader);
                    count = PdfReader.getPdfObjectRelease(dic.get(PdfName.COUNT));
                    PdfReader.access$2(this.reader, last);
                    acn = 1;
                    if (count != null && count.type() == 2) {
                        acn = ((PdfNumber)count).intValue();
                    }
                    if (n < base + acn) {
                        if (count == null) {
                            dic.mergeDifferent(acc);
                            return ref;
                        }
                        this.reader.releaseLastXrefPartial();
                        top = dic;
                        ** continue;
                    }
                    this.reader.releaseLastXrefPartial();
                    base+=acn;
                } while (true);
                break;
            } while (true);
        }

        /* synthetic */ PageRefs(PdfReader pdfReader, PageRefs pageRefs) {
            PageRefs pageRefs2;
            pageRefs2(pdfReader);
        }
    }

}

