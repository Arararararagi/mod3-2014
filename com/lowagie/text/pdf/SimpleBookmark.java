/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.ByteBuffer;
import com.lowagie.text.pdf.IntHashtable;
import com.lowagie.text.pdf.PRIndirectReference;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfBoolean;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfEncodings;
import com.lowagie.text.pdf.PdfIndirectObject;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNull;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfString;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.SimpleNamedDestination;
import com.lowagie.text.pdf.SimpleXMLDocHandler;
import com.lowagie.text.pdf.SimpleXMLParser;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.StringTokenizer;

public class SimpleBookmark
implements SimpleXMLDocHandler {
    private ArrayList topList;
    private Stack attr = new Stack();

    private SimpleBookmark() {
    }

    private static List bookmarkDepth(PdfReader reader, PdfDictionary outline, IntHashtable pages) {
        ArrayList list = new ArrayList();
        while (outline != null) {
            PdfNumber style;
            PdfNumber count;
            HashMap<String, Object> map = new HashMap<String, Object>();
            PdfString title = (PdfString)PdfReader.getPdfObjectRelease(outline.get(PdfName.TITLE));
            map.put("Title", title.toUnicodeString());
            PdfArray color = (PdfArray)PdfReader.getPdfObjectRelease(outline.get(PdfName.C));
            if (color != null && color.getArrayList().size() == 3) {
                ByteBuffer out = new ByteBuffer();
                ArrayList arr = color.getArrayList();
                out.append(((PdfNumber)arr.get(0)).floatValue()).append(' ');
                out.append(((PdfNumber)arr.get(1)).floatValue()).append(' ');
                out.append(((PdfNumber)arr.get(2)).floatValue());
                map.put("Color", PdfEncodings.convertToString(out.toByteArray(), null));
            }
            if ((style = (PdfNumber)PdfReader.getPdfObjectRelease(outline.get(PdfName.F))) != null) {
                int f = style.intValue();
                String s = "";
                if ((f & 1) != 0) {
                    s = String.valueOf(s) + "italic ";
                }
                if ((f & 2) != 0) {
                    s = String.valueOf(s) + "bold ";
                }
                if ((s = s.trim()).length() != 0) {
                    map.put("Style", s);
                }
            }
            if ((count = (PdfNumber)PdfReader.getPdfObjectRelease(outline.get(PdfName.COUNT))) != null && count.intValue() < 0) {
                map.put("Open", "false");
            }
            try {
                PdfObject dest = PdfReader.getPdfObjectRelease(outline.get(PdfName.DEST));
                if (dest != null) {
                    SimpleBookmark.mapGotoBookmark(map, dest, pages);
                } else {
                    PdfDictionary action = (PdfDictionary)PdfReader.getPdfObjectRelease(outline.get(PdfName.A));
                    if (action != null) {
                        PdfObject file;
                        if (PdfName.GOTO.equals(PdfReader.getPdfObjectRelease(action.get(PdfName.S)))) {
                            dest = PdfReader.getPdfObjectRelease(action.get(PdfName.D));
                            if (dest != null) {
                                SimpleBookmark.mapGotoBookmark(map, dest, pages);
                            }
                        } else if (PdfName.URI.equals(PdfReader.getPdfObjectRelease(action.get(PdfName.S)))) {
                            map.put("Action", "URI");
                            map.put("URI", ((PdfString)PdfReader.getPdfObjectRelease(action.get(PdfName.URI))).toUnicodeString());
                        } else if (PdfName.GOTOR.equals(PdfReader.getPdfObjectRelease(action.get(PdfName.S)))) {
                            PdfObject newWindow;
                            dest = PdfReader.getPdfObjectRelease(action.get(PdfName.D));
                            if (dest != null) {
                                if (dest.isString()) {
                                    map.put("Named", dest.toString());
                                } else if (dest.isName()) {
                                    map.put("NamedN", PdfName.decodeName(dest.toString()));
                                } else if (dest.isArray()) {
                                    ArrayList arr = ((PdfArray)dest).getArrayList();
                                    StringBuffer s = new StringBuffer();
                                    s.append(arr.get(0).toString());
                                    s.append(' ').append(arr.get(1).toString());
                                    for (int k = 2; k < arr.size(); ++k) {
                                        s.append(' ').append(arr.get(k).toString());
                                    }
                                    map.put("Page", s.toString());
                                }
                            }
                            map.put("Action", "GoToR");
                            file = PdfReader.getPdfObjectRelease(action.get(PdfName.F));
                            if (file != null) {
                                if (file.isString()) {
                                    map.put("File", ((PdfString)file).toUnicodeString());
                                } else if (file.isDictionary() && (file = PdfReader.getPdfObject(((PdfDictionary)file).get(PdfName.F))).isString()) {
                                    map.put("File", ((PdfString)file).toUnicodeString());
                                }
                            }
                            if ((newWindow = PdfReader.getPdfObjectRelease(action.get(PdfName.NEWWINDOW))) != null) {
                                map.put("NewWindow", newWindow.toString());
                            }
                        } else if (PdfName.LAUNCH.equals(PdfReader.getPdfObjectRelease(action.get(PdfName.S)))) {
                            map.put("Action", "Launch");
                            file = PdfReader.getPdfObjectRelease(action.get(PdfName.F));
                            if (file == null) {
                                file = PdfReader.getPdfObjectRelease(action.get(PdfName.WIN));
                            }
                            if (file != null) {
                                if (file.isString()) {
                                    map.put("File", ((PdfString)file).toUnicodeString());
                                } else if (file.isDictionary() && (file = PdfReader.getPdfObjectRelease(((PdfDictionary)file).get(PdfName.F))).isString()) {
                                    map.put("File", ((PdfString)file).toUnicodeString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception dest) {
                // empty catch block
            }
            PdfDictionary first = (PdfDictionary)PdfReader.getPdfObjectRelease(outline.get(PdfName.FIRST));
            if (first != null) {
                map.put("Kids", SimpleBookmark.bookmarkDepth(reader, first, pages));
            }
            list.add(map);
            outline = (PdfDictionary)PdfReader.getPdfObjectRelease(outline.get(PdfName.NEXT));
        }
        return list;
    }

    private static void mapGotoBookmark(HashMap map, PdfObject dest, IntHashtable pages) {
        if (dest.isString()) {
            map.put("Named", dest.toString());
        } else if (dest.isName()) {
            map.put("Named", PdfName.decodeName(dest.toString()));
        } else if (dest.isArray()) {
            map.put("Page", SimpleBookmark.makeBookmarkParam((PdfArray)dest, pages));
        }
        map.put("Action", "GoTo");
    }

    private static String makeBookmarkParam(PdfArray dest, IntHashtable pages) {
        ArrayList arr = dest.getArrayList();
        StringBuffer s = new StringBuffer();
        s.append(pages.get(SimpleBookmark.getNumber((PdfIndirectReference)arr.get(0))));
        s.append(' ').append(arr.get(1).toString().substring(1));
        for (int k = 2; k < arr.size(); ++k) {
            s.append(' ').append(arr.get(k).toString());
        }
        return s.toString();
    }

    private static int getNumber(PdfIndirectReference indirect) {
        PdfDictionary pdfObj = (PdfDictionary)PdfReader.getPdfObjectRelease(indirect);
        if (pdfObj.contains(PdfName.TYPE) && pdfObj.get(PdfName.TYPE).equals(PdfName.PAGES) && pdfObj.contains(PdfName.KIDS)) {
            PdfArray kids = (PdfArray)pdfObj.get(PdfName.KIDS);
            indirect = (PdfIndirectReference)kids.arrayList.get(0);
        }
        return indirect.getNumber();
    }

    public static List getBookmark(PdfReader reader) {
        PdfDictionary catalog = reader.getCatalog();
        PdfDictionary outlines = (PdfDictionary)PdfReader.getPdfObjectRelease(catalog.get(PdfName.OUTLINES));
        if (outlines == null) {
            return null;
        }
        IntHashtable pages = new IntHashtable();
        int numPages = reader.getNumberOfPages();
        for (int k = 1; k <= numPages; ++k) {
            pages.put(reader.getPageOrigRef(k).getNumber(), k);
            reader.releasePage(k);
        }
        return SimpleBookmark.bookmarkDepth(reader, (PdfDictionary)PdfReader.getPdfObjectRelease(outlines.get(PdfName.FIRST)), pages);
    }

    public static void eliminatePages(List list, int[] pageRange) {
        if (list == null) {
            return;
        }
        ListIterator it = list.listIterator();
        while (it.hasNext()) {
            List kids;
            String page;
            HashMap map = (HashMap)it.next();
            boolean hit = false;
            if ("GoTo".equals(map.get("Action")) && (page = (String)map.get("Page")) != null) {
                int idx = (page = page.trim()).indexOf(32);
                int pageNum = idx < 0 ? Integer.parseInt(page) : Integer.parseInt(page.substring(0, idx));
                int len = pageRange.length & -2;
                for (int k = 0; k < len; k+=2) {
                    if (pageNum < pageRange[k] || pageNum > pageRange[k + 1]) continue;
                    hit = true;
                    break;
                }
            }
            if ((kids = (List)map.get("Kids")) != null) {
                SimpleBookmark.eliminatePages(kids, pageRange);
                if (kids.size() == 0) {
                    map.remove("Kids");
                    kids = null;
                }
            }
            if (!hit) continue;
            if (kids == null) {
                it.remove();
                continue;
            }
            map.remove("Action");
            map.remove("Page");
            map.remove("Named");
        }
    }

    public static void shiftPageNumbers(List list, int pageShift, int[] pageRange) {
        if (list == null) {
            return;
        }
        ListIterator it = list.listIterator();
        while (it.hasNext()) {
            List kids;
            String page;
            HashMap map = (HashMap)it.next();
            if ("GoTo".equals(map.get("Action")) && (page = (String)map.get("Page")) != null) {
                int idx = (page = page.trim()).indexOf(32);
                int pageNum = idx < 0 ? Integer.parseInt(page) : Integer.parseInt(page.substring(0, idx));
                boolean hit = false;
                if (pageRange == null) {
                    hit = true;
                } else {
                    int len = pageRange.length & -2;
                    for (int k = 0; k < len; k+=2) {
                        if (pageNum < pageRange[k] || pageNum > pageRange[k + 1]) continue;
                        hit = true;
                        break;
                    }
                }
                if (hit) {
                    page = idx < 0 ? String.valueOf(pageNum + pageShift) : String.valueOf(pageNum + pageShift) + page.substring(idx);
                }
                map.put("Page", page);
            }
            if ((kids = (List)map.get("Kids")) == null) continue;
            SimpleBookmark.shiftPageNumbers(kids, pageShift, pageRange);
        }
    }

    static void createOutlineAction(PdfDictionary outline, HashMap map, PdfWriter writer, boolean namedAsNames) throws IOException {
        try {
            String file;
            String action = (String)map.get("Action");
            if ("GoTo".equals(action)) {
                String p = (String)map.get("Named");
                if (p != null) {
                    if (namedAsNames) {
                        outline.put(PdfName.DEST, new PdfName(p));
                    } else {
                        outline.put(PdfName.DEST, new PdfString(p, null));
                    }
                } else {
                    p = (String)map.get("Page");
                    if (p != null) {
                        PdfArray ar = new PdfArray();
                        StringTokenizer tk = new StringTokenizer(p);
                        int n = Integer.parseInt(tk.nextToken());
                        ar.add(writer.getPageReference(n));
                        if (!tk.hasMoreTokens()) {
                            ar.add(PdfName.XYZ);
                            ar.add(new float[]{0.0f, 10000.0f, 0.0f});
                        } else {
                            String fn = tk.nextToken();
                            if (fn.startsWith("/")) {
                                fn = fn.substring(1);
                            }
                            ar.add(new PdfName(fn));
                            for (int k = 0; k < 4 && tk.hasMoreTokens(); ++k) {
                                fn = tk.nextToken();
                                if (fn.equals("null")) {
                                    ar.add(PdfNull.PDFNULL);
                                    continue;
                                }
                                ar.add(new PdfNumber(fn));
                            }
                        }
                        outline.put(PdfName.DEST, ar);
                    }
                }
            } else if ("GoToR".equals(action)) {
                PdfDictionary dic = new PdfDictionary();
                String p = (String)map.get("Named");
                if (p != null) {
                    dic.put(PdfName.D, new PdfString(p, null));
                } else {
                    p = (String)map.get("NamedN");
                    if (p != null) {
                        dic.put(PdfName.D, new PdfName(p));
                    } else {
                        p = (String)map.get("Page");
                        if (p != null) {
                            PdfArray ar = new PdfArray();
                            StringTokenizer tk = new StringTokenizer(p);
                            ar.add(new PdfNumber(tk.nextToken()));
                            if (!tk.hasMoreTokens()) {
                                ar.add(PdfName.XYZ);
                                ar.add(new float[]{0.0f, 10000.0f, 0.0f});
                            } else {
                                String fn = tk.nextToken();
                                if (fn.startsWith("/")) {
                                    fn = fn.substring(1);
                                }
                                ar.add(new PdfName(fn));
                                for (int k = 0; k < 4 && tk.hasMoreTokens(); ++k) {
                                    fn = tk.nextToken();
                                    if (fn.equals("null")) {
                                        ar.add(PdfNull.PDFNULL);
                                        continue;
                                    }
                                    ar.add(new PdfNumber(fn));
                                }
                            }
                            dic.put(PdfName.D, ar);
                        }
                    }
                }
                String file2 = (String)map.get("File");
                if (dic.size() > 0 && file2 != null) {
                    dic.put(PdfName.S, PdfName.GOTOR);
                    dic.put(PdfName.F, new PdfString(file2));
                    String nw = (String)map.get("NewWindow");
                    if (nw != null) {
                        if (nw.equals("true")) {
                            dic.put(PdfName.NEWWINDOW, PdfBoolean.PDFTRUE);
                        } else if (nw.equals("false")) {
                            dic.put(PdfName.NEWWINDOW, PdfBoolean.PDFFALSE);
                        }
                    }
                    outline.put(PdfName.A, dic);
                }
            } else if ("URI".equals(action)) {
                String uri = (String)map.get("URI");
                if (uri != null) {
                    PdfDictionary dic = new PdfDictionary();
                    dic.put(PdfName.S, PdfName.URI);
                    dic.put(PdfName.URI, new PdfString(uri));
                    outline.put(PdfName.A, dic);
                }
            } else if ("Launch".equals(action) && (file = (String)map.get("File")) != null) {
                PdfDictionary dic = new PdfDictionary();
                dic.put(PdfName.S, PdfName.LAUNCH);
                dic.put(PdfName.F, new PdfString(file));
                outline.put(PdfName.A, dic);
            }
        }
        catch (Exception action) {
            // empty catch block
        }
    }

    static Object[] iterateOutlines(PdfWriter writer, PdfIndirectReference parent, List kids, boolean namedAsNames) throws IOException {
        PdfIndirectReference[] refs = new PdfIndirectReference[kids.size()];
        for (int k = 0; k < refs.length; ++k) {
            refs[k] = writer.getPdfIndirectReference();
        }
        int ptr = 0;
        int count = 0;
        ListIterator it = kids.listIterator();
        while (it.hasNext()) {
            String style;
            HashMap map = (HashMap)it.next();
            Object[] lower = null;
            List subKid = (List)map.get("Kids");
            if (subKid != null && subKid.size() > 0) {
                lower = SimpleBookmark.iterateOutlines(writer, refs[ptr], subKid, namedAsNames);
            }
            PdfDictionary outline = new PdfDictionary();
            ++count;
            if (lower != null) {
                outline.put(PdfName.FIRST, (PdfIndirectReference)lower[0]);
                outline.put(PdfName.LAST, (PdfIndirectReference)lower[1]);
                int n = (Integer)lower[2];
                if ("false".equals(map.get("Open"))) {
                    outline.put(PdfName.COUNT, new PdfNumber(- n));
                } else {
                    outline.put(PdfName.COUNT, new PdfNumber(n));
                    count+=n;
                }
            }
            outline.put(PdfName.PARENT, parent);
            if (ptr > 0) {
                outline.put(PdfName.PREV, refs[ptr - 1]);
            }
            if (ptr < refs.length - 1) {
                outline.put(PdfName.NEXT, refs[ptr + 1]);
            }
            outline.put(PdfName.TITLE, new PdfString((String)map.get("Title"), "UnicodeBig"));
            String color = (String)map.get("Color");
            if (color != null) {
                try {
                    PdfArray arr = new PdfArray();
                    StringTokenizer tk = new StringTokenizer(color);
                    for (int k2 = 0; k2 < 3; ++k2) {
                        float f = Float.valueOf(tk.nextToken()).intValue();
                        if (f < 0.0f) {
                            f = 0.0f;
                        }
                        if (f > 1.0f) {
                            f = 1.0f;
                        }
                        arr.add(new PdfNumber(f));
                    }
                    outline.put(PdfName.C, arr);
                }
                catch (Exception arr) {
                    // empty catch block
                }
            }
            if ((style = (String)map.get("Style")) != null) {
                style = style.toLowerCase();
                int bits = 0;
                if (style.indexOf("italic") >= 0) {
                    bits|=true;
                }
                if (style.indexOf("bold") >= 0) {
                    bits|=2;
                }
                if (bits != 0) {
                    outline.put(PdfName.F, new PdfNumber(bits));
                }
            }
            SimpleBookmark.createOutlineAction(outline, map, writer, namedAsNames);
            writer.addToBody((PdfObject)outline, refs[ptr]);
            ++ptr;
        }
        return new Object[]{refs[0], refs[refs.length - 1], new Integer(count)};
    }

    public static void exportToXMLNode(List list, Writer out, int indent, boolean onlyASCII) throws IOException {
        String dep = "";
        for (int k = 0; k < indent; ++k) {
            dep = String.valueOf(dep) + "  ";
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            HashMap map = (HashMap)it.next();
            String title = null;
            out.write(dep);
            out.write("<Title ");
            List kids = null;
            Iterator e = map.keySet().iterator();
            while (e.hasNext()) {
                String key = (String)e.next();
                if (key.equals("Title")) {
                    title = (String)map.get(key);
                    continue;
                }
                if (key.equals("Kids")) {
                    kids = (List)map.get(key);
                    continue;
                }
                out.write(key);
                out.write("=\"");
                String value = (String)map.get(key);
                if (key.equals("Named") || key.equals("NamedN")) {
                    value = SimpleNamedDestination.escapeBinaryString(value);
                }
                out.write(SimpleXMLParser.escapeXML(value, onlyASCII));
                out.write("\" ");
            }
            out.write(">");
            if (title == null) {
                title = "";
            }
            out.write(SimpleXMLParser.escapeXML(title, onlyASCII));
            if (kids != null) {
                out.write("\n");
                SimpleBookmark.exportToXMLNode(kids, out, indent + 1, onlyASCII);
                out.write(dep);
            }
            out.write("</Title>\n");
        }
    }

    public static void exportToXML(List list, OutputStream out, String encoding, boolean onlyASCII) throws IOException {
        String jenc = SimpleXMLParser.getJavaEncoding(encoding);
        BufferedWriter wrt = new BufferedWriter(new OutputStreamWriter(out, jenc));
        SimpleBookmark.exportToXML(list, wrt, encoding, onlyASCII);
    }

    public static void exportToXML(List list, Writer wrt, String encoding, boolean onlyASCII) throws IOException {
        wrt.write("<?xml version=\"1.0\" encoding=\"");
        wrt.write(SimpleXMLParser.escapeXML(encoding, onlyASCII));
        wrt.write("\"?>\n<Bookmark>\n");
        SimpleBookmark.exportToXMLNode(list, wrt, 1, onlyASCII);
        wrt.write("</Bookmark>\n");
        wrt.flush();
    }

    public static List importFromXML(InputStream in) throws IOException {
        SimpleBookmark book = new SimpleBookmark();
        SimpleXMLParser.parse((SimpleXMLDocHandler)book, in);
        return book.topList;
    }

    public static List importFromXML(Reader in) throws IOException {
        SimpleBookmark book = new SimpleBookmark();
        SimpleXMLParser.parse((SimpleXMLDocHandler)book, in);
        return book.topList;
    }

    public void endDocument() {
    }

    public void endElement(String tag) {
        if (tag.equals("Bookmark")) {
            if (this.attr.isEmpty()) {
                return;
            }
            throw new RuntimeException("Bookmark end tag out of place.");
        }
        if (!tag.equals("Title")) {
            throw new RuntimeException("Invalid end tag - " + tag);
        }
        HashMap attributes = (HashMap)this.attr.pop();
        String title = (String)attributes.get("Title");
        attributes.put("Title", title.trim());
        String named = (String)attributes.get("Named");
        if (named != null) {
            attributes.put("Named", SimpleNamedDestination.unEscapeBinaryString(named));
        }
        if ((named = (String)attributes.get("NamedN")) != null) {
            attributes.put("NamedN", SimpleNamedDestination.unEscapeBinaryString(named));
        }
        if (this.attr.isEmpty()) {
            this.topList.add(attributes);
        } else {
            HashMap parent = (HashMap)this.attr.peek();
            ArrayList<HashMap> kids = (ArrayList<HashMap>)parent.get("Kids");
            if (kids == null) {
                kids = new ArrayList<HashMap>();
                parent.put("Kids", kids);
            }
            kids.add(attributes);
        }
    }

    public void startDocument() {
    }

    public void startElement(String tag, HashMap h) {
        if (this.topList == null) {
            if (tag.equals("Bookmark")) {
                this.topList = new ArrayList();
                return;
            }
            throw new RuntimeException("Root element is not Bookmark.");
        }
        if (!tag.equals("Title")) {
            throw new RuntimeException("Tag " + tag + " not allowed.");
        }
        HashMap<String, String> attributes = new HashMap<String, String>(h);
        attributes.put("Title", "");
        attributes.remove("Kids");
        this.attr.push(attributes);
    }

    public void text(String str) {
        if (this.attr.isEmpty()) {
            return;
        }
        HashMap attributes = (HashMap)this.attr.peek();
        String title = (String)attributes.get("Title");
        title = String.valueOf(title) + str;
        attributes.put("Title", title);
    }
}

