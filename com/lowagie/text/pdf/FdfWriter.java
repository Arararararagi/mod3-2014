/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocWriter;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.FdfReader;
import com.lowagie.text.pdf.OutputStreamCounter;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfDocument;
import com.lowagie.text.pdf.PdfIndirectObject;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfString;
import com.lowagie.text.pdf.PdfWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;

public class FdfWriter {
    static byte[] HEADER_FDF = DocWriter.getISOBytes("%FDF-1.2\n%\u00e2\u00e3\u00cf\u00d3\n");
    HashMap fields = new HashMap();
    private String file;

    public void writeTo(OutputStream os) throws DocumentException, IOException {
        Wrt wrt = new Wrt(os, this);
        wrt.writeTo();
    }

    boolean setField(String field, PdfObject value) {
        HashMap map;
        String s;
        Object obj;
        block4 : {
            map = this.fields;
            StringTokenizer tk = new StringTokenizer(field, ".");
            if (!tk.hasMoreTokens()) {
                return false;
            }
            do {
                s = tk.nextToken();
                obj = map.get(s);
                if (!tk.hasMoreTokens()) break block4;
                if (obj == null) {
                    obj = new HashMap();
                    map.put(s, obj);
                    map = (HashMap)obj;
                    continue;
                }
                if (!(obj instanceof HashMap)) break;
                map = (HashMap)obj;
            } while (true);
            return false;
        }
        if (!(obj != null && obj instanceof HashMap)) {
            map.put(s, value);
            return true;
        }
        return false;
    }

    void iterateFields(HashMap values, HashMap map, String name) {
        Iterator it = map.keySet().iterator();
        while (it.hasNext()) {
            String s = (String)it.next();
            Object obj = map.get(s);
            if (obj instanceof HashMap) {
                this.iterateFields(values, (HashMap)obj, String.valueOf(name) + "." + s);
                continue;
            }
            values.put((String.valueOf(name) + "." + s).substring(1), obj);
        }
    }

    public boolean removeField(String field) {
        HashMap map;
        ArrayList<Object> hist;
        Object obj;
        block5 : {
            map = this.fields;
            StringTokenizer tk = new StringTokenizer(field, ".");
            if (!tk.hasMoreTokens()) {
                return false;
            }
            hist = new ArrayList<Object>();
            do {
                String s;
                if ((obj = map.get(s = tk.nextToken())) == null) {
                    return false;
                }
                hist.add(map);
                hist.add(s);
                if (!tk.hasMoreTokens()) break block5;
                if (!(obj instanceof HashMap)) break;
                map = (HashMap)obj;
            } while (true);
            return false;
        }
        if (obj instanceof HashMap) {
            return false;
        }
        for (int k = hist.size() - 2; k >= 0; k-=2) {
            map = (HashMap)hist.get(k);
            String s = (String)hist.get(k + 1);
            map.remove(s);
            if (map.size() > 0) break;
        }
        return true;
    }

    public HashMap getFields() {
        HashMap values = new HashMap();
        this.iterateFields(values, this.fields, "");
        return values;
    }

    public String getField(String field) {
        Object obj;
        block5 : {
            HashMap map = this.fields;
            StringTokenizer tk = new StringTokenizer(field, ".");
            if (!tk.hasMoreTokens()) {
                return null;
            }
            do {
                String s;
                if ((obj = map.get(s = tk.nextToken())) == null) {
                    return null;
                }
                if (!tk.hasMoreTokens()) break block5;
                if (!(obj instanceof HashMap)) break;
                map = (HashMap)obj;
            } while (true);
            return null;
        }
        if (obj instanceof HashMap) {
            return null;
        }
        if (((PdfObject)obj).isString()) {
            return ((PdfString)obj).toUnicodeString();
        }
        return PdfName.decodeName(obj.toString());
    }

    public boolean setFieldAsName(String field, String value) {
        return this.setField(field, new PdfName(value));
    }

    public boolean setFieldAsString(String field, String value) {
        return this.setField(field, new PdfString(value, "UnicodeBig"));
    }

    public void setFields(FdfReader fdf) {
        HashMap map = fdf.getFields();
        Iterator it = map.keySet().iterator();
        while (it.hasNext()) {
            String key = (String)it.next();
            PdfDictionary dic = (PdfDictionary)map.get(key);
            PdfObject v = dic.get(PdfName.V);
            if (v == null) continue;
            this.setField(key, v);
        }
    }

    public void setFields(PdfReader pdf) {
        PRAcroForm acro = pdf.getAcroForm();
        ArrayList f = acro.getFields();
        for (int k = 0; k < f.size(); ++k) {
            PRAcroForm.FieldInformation inf = (PRAcroForm.FieldInformation)f.get(k);
            PdfObject obj = inf.getInfo().get(PdfName.V);
            if (obj == null) continue;
            this.setField(inf.name, obj);
        }
    }

    public void setFields(AcroFields acro) {
        HashMap map = acro.getFields();
        Iterator it = map.keySet().iterator();
        while (it.hasNext()) {
            String key = (String)it.next();
            AcroFields.Item item = (AcroFields.Item)map.get(key);
            PdfObject obj = ((PdfDictionary)item.merged.get(0)).get(PdfName.V);
            if (obj == null) continue;
            this.setField(key, obj);
        }
    }

    public String getFile() {
        return this.file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    static class Wrt
    extends PdfWriter {
        private FdfWriter fdf;

        Wrt(OutputStream os, FdfWriter fdf) throws DocumentException, IOException {
            super(new PdfDocument(), os);
            this.fdf = fdf;
            this.os.write(FdfWriter.HEADER_FDF);
            this.body = new PdfWriter.PdfBody(this);
        }

        void writeTo() throws DocumentException, IOException {
            PdfDictionary dic = new PdfDictionary();
            dic.put(PdfName.FIELDS, this.calculate(this.fdf.fields));
            if (this.fdf.file != null) {
                dic.put(PdfName.F, new PdfString(this.fdf.file, "UnicodeBig"));
            }
            PdfDictionary fd = new PdfDictionary();
            fd.put(PdfName.FDF, dic);
            PdfIndirectReference ref = this.addToBody(fd).getIndirectReference();
            this.os.write(DocWriter.getISOBytes("trailer\n"));
            PdfDictionary trailer = new PdfDictionary();
            trailer.put(PdfName.ROOT, ref);
            trailer.toPdf(null, this.os);
            this.os.write(DocWriter.getISOBytes("\n%%EOF\n"));
            this.os.close();
        }

        PdfArray calculate(HashMap map) throws IOException {
            PdfArray ar = new PdfArray();
            Iterator it = map.keySet().iterator();
            while (it.hasNext()) {
                String key = (String)it.next();
                Object v = map.get(key);
                PdfDictionary dic = new PdfDictionary();
                dic.put(PdfName.T, new PdfString(key, "UnicodeBig"));
                if (v instanceof HashMap) {
                    dic.put(PdfName.KIDS, this.calculate((HashMap)v));
                } else {
                    dic.put(PdfName.V, (PdfObject)v);
                }
                ar.add(dic);
            }
            return ar;
        }
    }

}

