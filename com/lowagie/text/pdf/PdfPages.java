/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocumentException;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfIndirectObject;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PdfPages {
    private ArrayList pages = new ArrayList();
    private ArrayList parents = new ArrayList();
    private int leafSize = 10;
    private PdfWriter writer;
    private PdfIndirectReference topParent;

    PdfPages(PdfWriter writer) {
        this.writer = writer;
    }

    void addPage(PdfDictionary page) {
        try {
            if (this.pages.size() % this.leafSize == 0) {
                this.parents.add(this.writer.getPdfIndirectReference());
            }
            PdfIndirectReference parent = (PdfIndirectReference)this.parents.get(this.parents.size() - 1);
            page.put(PdfName.PARENT, parent);
            PdfIndirectReference current = this.writer.getCurrentPage();
            this.writer.addToBody((PdfObject)page, current);
            this.pages.add(current);
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    PdfIndirectReference addPageRef(PdfIndirectReference pageRef) {
        try {
            if (this.pages.size() % this.leafSize == 0) {
                this.parents.add(this.writer.getPdfIndirectReference());
            }
            this.pages.add(pageRef);
            return (PdfIndirectReference)this.parents.get(this.parents.size() - 1);
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    PdfIndirectReference writePageTree() throws IOException {
        if (this.pages.size() == 0) {
            throw new IOException("The document has no pages.");
        }
        int leaf = 1;
        ArrayList<PdfIndirectReference> tParents = this.parents;
        ArrayList tPages = this.pages;
        ArrayList<PdfIndirectReference> nextParents = new ArrayList<PdfIndirectReference>();
        do {
            leaf*=this.leafSize;
            int stdCount = this.leafSize;
            int rightCount = tPages.size() % this.leafSize;
            if (rightCount == 0) {
                rightCount = this.leafSize;
            }
            for (int p = 0; p < tParents.size(); ++p) {
                int count;
                int thisLeaf = leaf;
                if (p == tParents.size() - 1) {
                    count = rightCount;
                    thisLeaf = this.pages.size() % leaf;
                    if (thisLeaf == 0) {
                        thisLeaf = leaf;
                    }
                } else {
                    count = stdCount;
                }
                PdfDictionary top = new PdfDictionary(PdfName.PAGES);
                top.put(PdfName.COUNT, new PdfNumber(thisLeaf));
                PdfArray kids = new PdfArray();
                ArrayList internal = kids.getArrayList();
                internal.addAll(tPages.subList(p * stdCount, p * stdCount + count));
                top.put(PdfName.KIDS, kids);
                if (tParents.size() > 1) {
                    if (p % this.leafSize == 0) {
                        nextParents.add(this.writer.getPdfIndirectReference());
                    }
                    top.put(PdfName.PARENT, (PdfIndirectReference)nextParents.get(p / this.leafSize));
                }
                this.writer.addToBody((PdfObject)top, (PdfIndirectReference)tParents.get(p));
            }
            if (tParents.size() == 1) {
                this.topParent = (PdfIndirectReference)tParents.get(0);
                return this.topParent;
            }
            tPages = tParents;
            tParents = nextParents;
            nextParents = new ArrayList();
        } while (true);
    }

    PdfIndirectReference getTopParent() {
        return this.topParent;
    }

    void setLinearMode(PdfIndirectReference topParent) {
        if (this.parents.size() > 1) {
            throw new RuntimeException("Linear page mode can only be called with a single parent.");
        }
        if (topParent != null) {
            this.topParent = topParent;
            this.parents.clear();
            this.parents.add(topParent);
        }
        this.leafSize = 10000000;
    }

    void addPage(PdfIndirectReference page) {
        this.pages.add(page);
    }

    int reorderPages(int[] order) throws DocumentException {
        if (order == null) {
            return this.pages.size();
        }
        if (this.parents.size() > 1) {
            throw new DocumentException("Page reordering requires a single parent in the page tree. Call PdfWriter.setLinearMode() after open.");
        }
        if (order.length != this.pages.size()) {
            throw new DocumentException("Page reordering requires an array with the same size as the number of pages.");
        }
        int max = this.pages.size();
        boolean[] temp = new boolean[max];
        for (int k = 0; k < max; ++k) {
            int p = order[k];
            if (p < 1 || p > max) {
                throw new DocumentException("Page reordering requires pages between 1 and " + max + ". Found " + p + ".");
            }
            if (temp[p - 1]) {
                throw new DocumentException("Page reordering requires no page repetition. Page " + p + " is repeated.");
            }
            temp[p - 1] = true;
        }
        Object[] copy = this.pages.toArray();
        for (int k2 = 0; k2 < max; ++k2) {
            this.pages.set(k2, copy[order[k2] - 1]);
        }
        return max;
    }
}

