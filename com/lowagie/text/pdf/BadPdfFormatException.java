/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.PdfException;

public class BadPdfFormatException
extends PdfException {
    BadPdfFormatException() {
    }

    BadPdfFormatException(String message) {
        super(message);
    }
}

