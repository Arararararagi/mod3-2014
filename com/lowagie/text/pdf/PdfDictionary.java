/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class PdfDictionary
extends PdfObject {
    public static final PdfName FONT = PdfName.FONT;
    public static final PdfName OUTLINES = PdfName.OUTLINES;
    public static final PdfName PAGE = PdfName.PAGE;
    public static final PdfName PAGES = PdfName.PAGES;
    public static final PdfName CATALOG = PdfName.CATALOG;
    private PdfName dictionaryType = null;
    protected HashMap hashMap = new HashMap();

    public PdfDictionary() {
        super(6);
    }

    public PdfDictionary(PdfName type) {
        this();
        this.dictionaryType = type;
        this.put(PdfName.TYPE, this.dictionaryType);
    }

    public void toPdf(PdfWriter writer, OutputStream os) throws IOException {
        os.write(60);
        os.write(60);
        int type = 0;
        Iterator i = this.hashMap.keySet().iterator();
        while (i.hasNext()) {
            PdfName key = (PdfName)i.next();
            PdfObject value = (PdfObject)this.hashMap.get(key);
            key.toPdf(writer, os);
            type = value.type();
            if (type != 5 && type != 6 && type != 4 && type != 3) {
                os.write(32);
            }
            value.toPdf(writer, os);
        }
        os.write(62);
        os.write(62);
    }

    public PdfObject put(PdfName key, PdfObject value) {
        return this.hashMap.put(key, value);
    }

    public PdfObject putEx(PdfName key, PdfObject value) {
        if (value == null) {
            return null;
        }
        return this.hashMap.put(key, value);
    }

    public PdfObject putDel(PdfName key, PdfObject value) {
        if (value == null) {
            return (PdfObject)this.hashMap.remove(key);
        }
        return this.hashMap.put(key, value);
    }

    public PdfObject remove(PdfName key) {
        return (PdfObject)this.hashMap.remove(key);
    }

    public PdfObject get(PdfName key) {
        return (PdfObject)this.hashMap.get(key);
    }

    public boolean isDictionaryType(PdfName type) {
        if (this.dictionaryType.compareTo(type) == 0) {
            return true;
        }
        return false;
    }

    public boolean isFont() {
        if (this.dictionaryType.compareTo(FONT) == 0) {
            return true;
        }
        return false;
    }

    public boolean isPage() {
        if (this.dictionaryType.compareTo(PAGE) == 0) {
            return true;
        }
        return false;
    }

    public boolean isPages() {
        if (this.dictionaryType.compareTo(PAGES) == 0) {
            return true;
        }
        return false;
    }

    public boolean isCatalog() {
        if (this.dictionaryType.compareTo(CATALOG) == 0) {
            return true;
        }
        return false;
    }

    public boolean isOutlineTree() {
        if (this.dictionaryType.compareTo(OUTLINES) == 0) {
            return true;
        }
        return false;
    }

    public void merge(PdfDictionary other) {
        this.hashMap.putAll(other.hashMap);
    }

    public void mergeDifferent(PdfDictionary other) {
        Iterator i = other.hashMap.keySet().iterator();
        while (i.hasNext()) {
            Object key = i.next();
            if (this.hashMap.containsKey(key)) continue;
            this.hashMap.put(key, other.hashMap.get(key));
        }
    }

    public Set getKeys() {
        return this.hashMap.keySet();
    }

    public void putAll(PdfDictionary dic) {
        this.hashMap.putAll(dic.hashMap);
    }

    public int size() {
        return this.hashMap.size();
    }

    public boolean contains(PdfName key) {
        return this.hashMap.containsKey(key);
    }
}

