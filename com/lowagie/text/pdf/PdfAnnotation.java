/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.ByteBuffer;
import com.lowagie.text.pdf.CMYKColor;
import com.lowagie.text.pdf.ExtendedColor;
import com.lowagie.text.pdf.GrayColor;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfBoolean;
import com.lowagie.text.pdf.PdfBorderArray;
import com.lowagie.text.pdf.PdfBorderDictionary;
import com.lowagie.text.pdf.PdfColor;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfDestination;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfFileSpecification;
import com.lowagie.text.pdf.PdfIndirectObject;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfOCG;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfRectangle;
import com.lowagie.text.pdf.PdfString;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.IOException;
import java.util.HashMap;

public class PdfAnnotation
extends PdfDictionary {
    public static final PdfName HIGHLIGHT_NONE = PdfName.N;
    public static final PdfName HIGHLIGHT_INVERT = PdfName.I;
    public static final PdfName HIGHLIGHT_OUTLINE = PdfName.O;
    public static final PdfName HIGHLIGHT_PUSH = PdfName.P;
    public static final PdfName HIGHLIGHT_TOGGLE = PdfName.T;
    public static final int FLAGS_INVISIBLE = 1;
    public static final int FLAGS_HIDDEN = 2;
    public static final int FLAGS_PRINT = 4;
    public static final int FLAGS_NOZOOM = 8;
    public static final int FLAGS_NOROTATE = 16;
    public static final int FLAGS_NOVIEW = 32;
    public static final int FLAGS_READONLY = 64;
    public static final int FLAGS_LOCKED = 128;
    public static final int FLAGS_TOGGLENOVIEW = 256;
    public static final PdfName APPEARANCE_NORMAL = PdfName.N;
    public static final PdfName APPEARANCE_ROLLOVER = PdfName.R;
    public static final PdfName APPEARANCE_DOWN = PdfName.D;
    public static final PdfName AA_ENTER = PdfName.E;
    public static final PdfName AA_EXIT = PdfName.X;
    public static final PdfName AA_DOWN = PdfName.D;
    public static final PdfName AA_UP = PdfName.U;
    public static final PdfName AA_FOCUS = PdfName.FO;
    public static final PdfName AA_BLUR = PdfName.BL;
    public static final PdfName AA_JS_KEY = PdfName.K;
    public static final PdfName AA_JS_FORMAT = PdfName.F;
    public static final PdfName AA_JS_CHANGE = PdfName.V;
    public static final PdfName AA_JS_OTHER_CHANGE = PdfName.C;
    public static final int MARKUP_HIGHLIGHT = 0;
    public static final int MARKUP_UNDERLINE = 1;
    public static final int MARKUP_STRIKEOUT = 2;
    protected PdfWriter writer;
    protected PdfIndirectReference reference;
    protected HashMap templates;
    protected boolean form = false;
    protected boolean annotation = true;
    protected boolean used = false;
    private int placeInPage = -1;

    protected PdfAnnotation(PdfWriter writer, Rectangle rect) {
        this.writer = writer;
        if (rect != null) {
            this.put(PdfName.RECT, new PdfRectangle(rect));
        }
    }

    PdfAnnotation(PdfWriter writer, float llx, float lly, float urx, float ury, PdfString title, PdfString content) {
        this.writer = writer;
        this.put(PdfName.SUBTYPE, PdfName.TEXT);
        this.put(PdfName.T, title);
        this.put(PdfName.RECT, new PdfRectangle(llx, lly, urx, ury));
        this.put(PdfName.CONTENTS, content);
    }

    public PdfAnnotation(PdfWriter writer, float llx, float lly, float urx, float ury, PdfAction action) {
        this.writer = writer;
        this.put(PdfName.SUBTYPE, PdfName.LINK);
        this.put(PdfName.RECT, new PdfRectangle(llx, lly, urx, ury));
        this.put(PdfName.A, action);
        this.put(PdfName.BORDER, new PdfBorderArray(0.0f, 0.0f, 0.0f));
        this.put(PdfName.C, new PdfColor(0, 0, 255));
    }

    public static PdfAnnotation createScreen(PdfWriter writer, Rectangle rect, String clipTitle, PdfFileSpecification fs, String mimeType, boolean playOnDisplay) throws IOException {
        PdfAnnotation ann = new PdfAnnotation(writer, rect);
        ann.put(PdfName.SUBTYPE, PdfName.SCREEN);
        ann.put(PdfName.F, new PdfNumber(4));
        ann.put(PdfName.TYPE, PdfName.ANNOT);
        ann.setPage();
        PdfIndirectReference ref = ann.getIndirectReference();
        PdfAction action = PdfAction.rendition(clipTitle, fs, mimeType, ref);
        PdfIndirectReference actionRef = writer.addToBody(action).getIndirectReference();
        if (playOnDisplay) {
            PdfDictionary aa = new PdfDictionary();
            aa.put(new PdfName("PV"), actionRef);
            ann.put(PdfName.AA, aa);
        }
        ann.put(PdfName.A, actionRef);
        return ann;
    }

    PdfIndirectReference getIndirectReference() {
        if (this.reference == null) {
            this.reference = this.writer.getPdfIndirectReference();
        }
        return this.reference;
    }

    public static PdfAnnotation createText(PdfWriter writer, Rectangle rect, String title, String contents, boolean open, String icon) {
        PdfAnnotation annot = new PdfAnnotation(writer, rect);
        annot.put(PdfName.SUBTYPE, PdfName.TEXT);
        if (title != null) {
            annot.put(PdfName.T, new PdfString(title, "UnicodeBig"));
        }
        if (contents != null) {
            annot.put(PdfName.CONTENTS, new PdfString(contents, "UnicodeBig"));
        }
        if (open) {
            annot.put(PdfName.OPEN, PdfBoolean.PDFTRUE);
        }
        if (icon != null) {
            annot.put(PdfName.NAME, new PdfName(icon));
        }
        return annot;
    }

    protected static PdfAnnotation createLink(PdfWriter writer, Rectangle rect, PdfName highlight) {
        PdfAnnotation annot = new PdfAnnotation(writer, rect);
        annot.put(PdfName.SUBTYPE, PdfName.LINK);
        if (!highlight.equals(HIGHLIGHT_INVERT)) {
            annot.put(PdfName.H, highlight);
        }
        return annot;
    }

    public static PdfAnnotation createLink(PdfWriter writer, Rectangle rect, PdfName highlight, PdfAction action) {
        PdfAnnotation annot = PdfAnnotation.createLink(writer, rect, highlight);
        annot.putEx(PdfName.A, action);
        return annot;
    }

    public static PdfAnnotation createLink(PdfWriter writer, Rectangle rect, PdfName highlight, String namedDestination) {
        PdfAnnotation annot = PdfAnnotation.createLink(writer, rect, highlight);
        annot.put(PdfName.DEST, new PdfString(namedDestination));
        return annot;
    }

    public static PdfAnnotation createLink(PdfWriter writer, Rectangle rect, PdfName highlight, int page, PdfDestination dest) {
        PdfAnnotation annot = PdfAnnotation.createLink(writer, rect, highlight);
        PdfIndirectReference ref = writer.getPageReference(page);
        dest.addPage(ref);
        annot.put(PdfName.DEST, dest);
        return annot;
    }

    public static PdfAnnotation createFreeText(PdfWriter writer, Rectangle rect, String contents, PdfContentByte defaultAppearance) {
        PdfAnnotation annot = new PdfAnnotation(writer, rect);
        annot.put(PdfName.SUBTYPE, PdfName.FREETEXT);
        annot.put(PdfName.CONTENTS, new PdfString(contents, "UnicodeBig"));
        annot.setDefaultAppearanceString(defaultAppearance);
        return annot;
    }

    public static PdfAnnotation createLine(PdfWriter writer, Rectangle rect, String contents, float x1, float y1, float x2, float y2) {
        PdfAnnotation annot = new PdfAnnotation(writer, rect);
        annot.put(PdfName.SUBTYPE, PdfName.LINE);
        annot.put(PdfName.CONTENTS, new PdfString(contents, "UnicodeBig"));
        PdfArray array = new PdfArray(new PdfNumber(x1));
        array.add(new PdfNumber(y1));
        array.add(new PdfNumber(x2));
        array.add(new PdfNumber(y2));
        annot.put(PdfName.L, array);
        return annot;
    }

    public static PdfAnnotation createSquareCircle(PdfWriter writer, Rectangle rect, String contents, boolean square) {
        PdfAnnotation annot = new PdfAnnotation(writer, rect);
        if (square) {
            annot.put(PdfName.SUBTYPE, PdfName.SQUARE);
        } else {
            annot.put(PdfName.SUBTYPE, PdfName.CIRCLE);
        }
        annot.put(PdfName.CONTENTS, new PdfString(contents, "UnicodeBig"));
        return annot;
    }

    public static PdfAnnotation createMarkup(PdfWriter writer, Rectangle rect, String contents, int type, float[] quadPoints) {
        PdfAnnotation annot = new PdfAnnotation(writer, rect);
        PdfName name = PdfName.HIGHLIGHT;
        switch (type) {
            case 1: {
                name = PdfName.UNDERLINE;
                break;
            }
            case 2: {
                name = PdfName.STRIKEOUT;
            }
        }
        annot.put(PdfName.SUBTYPE, name);
        annot.put(PdfName.CONTENTS, new PdfString(contents, "UnicodeBig"));
        PdfArray array = new PdfArray();
        for (int k = 0; k < quadPoints.length; ++k) {
            array.add(new PdfNumber(quadPoints[k]));
        }
        annot.put(PdfName.QUADPOINTS, array);
        return annot;
    }

    public static PdfAnnotation createStamp(PdfWriter writer, Rectangle rect, String contents, String name) {
        PdfAnnotation annot = new PdfAnnotation(writer, rect);
        annot.put(PdfName.SUBTYPE, PdfName.STAMP);
        annot.put(PdfName.CONTENTS, new PdfString(contents, "UnicodeBig"));
        annot.put(PdfName.NAME, new PdfName(name));
        return annot;
    }

    public static PdfAnnotation createInk(PdfWriter writer, Rectangle rect, String contents, float[][] inkList) {
        PdfAnnotation annot = new PdfAnnotation(writer, rect);
        annot.put(PdfName.SUBTYPE, PdfName.INK);
        annot.put(PdfName.CONTENTS, new PdfString(contents, "UnicodeBig"));
        PdfArray outer = new PdfArray();
        for (int k = 0; k < inkList.length; ++k) {
            PdfArray inner = new PdfArray();
            float[] deep = inkList[k];
            for (int j = 0; j < deep.length; ++j) {
                inner.add(new PdfNumber(deep[j]));
            }
            outer.add(inner);
        }
        annot.put(PdfName.INKLIST, outer);
        return annot;
    }

    public static PdfAnnotation createFileAttachment(PdfWriter writer, Rectangle rect, String contents, byte[] fileStore, String file, String fileDisplay) throws IOException {
        return PdfAnnotation.createFileAttachment(writer, rect, contents, PdfFileSpecification.fileEmbedded(writer, file, fileDisplay, fileStore));
    }

    public static PdfAnnotation createFileAttachment(PdfWriter writer, Rectangle rect, String contents, PdfFileSpecification fs) throws IOException {
        PdfAnnotation annot = new PdfAnnotation(writer, rect);
        annot.put(PdfName.SUBTYPE, PdfName.FILEATTACHMENT);
        annot.put(PdfName.CONTENTS, new PdfString(contents, "UnicodeBig"));
        annot.put(PdfName.FS, fs.getReference());
        return annot;
    }

    public static PdfAnnotation createPopup(PdfWriter writer, Rectangle rect, String contents, boolean open) {
        PdfAnnotation annot = new PdfAnnotation(writer, rect);
        annot.put(PdfName.SUBTYPE, PdfName.POPUP);
        if (contents != null) {
            annot.put(PdfName.CONTENTS, new PdfString(contents, "UnicodeBig"));
        }
        if (open) {
            annot.put(PdfName.OPEN, PdfBoolean.PDFTRUE);
        }
        return annot;
    }

    public void setDefaultAppearanceString(PdfContentByte cb) {
        byte[] b = cb.getInternalBuffer().toByteArray();
        int len = b.length;
        for (int k = 0; k < len; ++k) {
            if (b[k] != 10) continue;
            b[k] = 32;
        }
        this.put(PdfName.DA, new PdfString(b));
    }

    public void setFlags(int flags) {
        if (flags == 0) {
            this.remove(PdfName.F);
        } else {
            this.put(PdfName.F, new PdfNumber(flags));
        }
    }

    public void setBorder(PdfBorderArray border) {
        this.putDel(PdfName.BORDER, border);
    }

    public void setBorderStyle(PdfBorderDictionary border) {
        this.putDel(PdfName.BS, border);
    }

    public void setHighlighting(PdfName highlight) {
        if (highlight.equals(HIGHLIGHT_INVERT)) {
            this.remove(PdfName.H);
        } else {
            this.put(PdfName.H, highlight);
        }
    }

    public void setAppearance(PdfName ap, PdfTemplate template) {
        PdfDictionary dic = (PdfDictionary)this.get(PdfName.AP);
        if (dic == null) {
            dic = new PdfDictionary();
        }
        dic.put(ap, template.getIndirectReference());
        this.put(PdfName.AP, dic);
        if (!this.form) {
            return;
        }
        if (this.templates == null) {
            this.templates = new HashMap();
        }
        this.templates.put(template, null);
    }

    public void setAppearance(PdfName ap, String state, PdfTemplate template) {
        PdfObject obj;
        PdfDictionary dicAp = (PdfDictionary)this.get(PdfName.AP);
        if (dicAp == null) {
            dicAp = new PdfDictionary();
        }
        PdfDictionary dic = (obj = dicAp.get(ap)) != null && obj.isDictionary() ? (PdfDictionary)obj : new PdfDictionary();
        dic.put(new PdfName(state), template.getIndirectReference());
        dicAp.put(ap, dic);
        this.put(PdfName.AP, dicAp);
        if (!this.form) {
            return;
        }
        if (this.templates == null) {
            this.templates = new HashMap();
        }
        this.templates.put(template, null);
    }

    public void setAppearanceState(String state) {
        if (state == null) {
            this.remove(PdfName.AS);
            return;
        }
        this.put(PdfName.AS, new PdfName(state));
    }

    public void setColor(Color color) {
        this.putDel(PdfName.C, new PdfColor(color));
    }

    public void setTitle(String title) {
        if (title == null) {
            this.remove(PdfName.T);
            return;
        }
        this.put(PdfName.T, new PdfString(title, "UnicodeBig"));
    }

    public void setPopup(PdfAnnotation popup) {
        this.put(PdfName.POPUP, popup.getIndirectReference());
        popup.put(PdfName.PARENT, this.getIndirectReference());
    }

    public void setAction(PdfAction action) {
        this.putDel(PdfName.A, action);
    }

    public void setAdditionalActions(PdfName key, PdfAction action) {
        PdfObject obj = this.get(PdfName.AA);
        PdfDictionary dic = obj != null && obj.isDictionary() ? (PdfDictionary)obj : new PdfDictionary();
        dic.put(key, action);
        this.put(PdfName.AA, dic);
    }

    public boolean isUsed() {
        return this.used;
    }

    void setUsed() {
        this.used = true;
    }

    HashMap getTemplates() {
        return this.templates;
    }

    public boolean isForm() {
        return this.form;
    }

    public boolean isAnnotation() {
        return this.annotation;
    }

    public void setPage(int page) {
        this.put(PdfName.P, this.writer.getPageReference(page));
    }

    public void setPage() {
        this.put(PdfName.P, this.writer.getCurrentPage());
    }

    public int getPlaceInPage() {
        return this.placeInPage;
    }

    public void setPlaceInPage(int placeInPage) {
        this.placeInPage = placeInPage;
    }

    public void setRotate(int v) {
        this.put(PdfName.ROTATE, new PdfNumber(v));
    }

    PdfDictionary getMK() {
        PdfDictionary mk = (PdfDictionary)this.get(PdfName.MK);
        if (mk == null) {
            mk = new PdfDictionary();
            this.put(PdfName.MK, mk);
        }
        return mk;
    }

    public void setMKRotation(int rotation) {
        this.getMK().put(PdfName.R, new PdfNumber(rotation));
    }

    public static PdfArray getMKColor(Color color) {
        PdfArray array = new PdfArray();
        int type = ExtendedColor.getType(color);
        switch (type) {
            case 1: {
                array.add(new PdfNumber(((GrayColor)color).getGray()));
                break;
            }
            case 2: {
                CMYKColor cmyk = (CMYKColor)color;
                array.add(new PdfNumber(cmyk.getCyan()));
                array.add(new PdfNumber(cmyk.getMagenta()));
                array.add(new PdfNumber(cmyk.getYellow()));
                array.add(new PdfNumber(cmyk.getBlack()));
                break;
            }
            case 3: 
            case 4: 
            case 5: {
                throw new RuntimeException("Separations, patterns and shadings are not allowed in MK dictionary.");
            }
            default: {
                array.add(new PdfNumber((float)color.getRed() / 255.0f));
                array.add(new PdfNumber((float)color.getGreen() / 255.0f));
                array.add(new PdfNumber((float)color.getBlue() / 255.0f));
            }
        }
        return array;
    }

    public void setMKBorderColor(Color color) {
        if (color == null) {
            this.getMK().remove(PdfName.BC);
        } else {
            this.getMK().put(PdfName.BC, PdfAnnotation.getMKColor(color));
        }
    }

    public void setMKBackgroundColor(Color color) {
        if (color == null) {
            this.getMK().remove(PdfName.BG);
        } else {
            this.getMK().put(PdfName.BG, PdfAnnotation.getMKColor(color));
        }
    }

    public void setMKNormalCaption(String caption) {
        this.getMK().put(PdfName.CA, new PdfString(caption, "UnicodeBig"));
    }

    public void setMKRolloverCaption(String caption) {
        this.getMK().put(PdfName.RC, new PdfString(caption, "UnicodeBig"));
    }

    public void setMKAlternateCaption(String caption) {
        this.getMK().put(PdfName.AC, new PdfString(caption, "UnicodeBig"));
    }

    public void setMKNormalIcon(PdfTemplate template) {
        this.getMK().put(PdfName.I, template.getIndirectReference());
    }

    public void setMKRolloverIcon(PdfTemplate template) {
        this.getMK().put(PdfName.RI, template.getIndirectReference());
    }

    public void setMKAlternateIcon(PdfTemplate template) {
        this.getMK().put(PdfName.IX, template.getIndirectReference());
    }

    public void setMKIconFit(PdfName scale, PdfName scalingType, float leftoverLeft, float leftoverBottom, boolean fitInBounds) {
        PdfDictionary dic = new PdfDictionary();
        if (!scale.equals(PdfName.A)) {
            dic.put(PdfName.SW, scale);
        }
        if (!scalingType.equals(PdfName.P)) {
            dic.put(PdfName.S, scalingType);
        }
        if (leftoverLeft != 0.5f || leftoverBottom != 0.5f) {
            PdfArray array = new PdfArray(new PdfNumber(leftoverLeft));
            array.add(new PdfNumber(leftoverBottom));
            dic.put(PdfName.A, array);
        }
        if (fitInBounds) {
            dic.put(PdfName.FB, PdfBoolean.PDFTRUE);
        }
        this.getMK().put(PdfName.IF, dic);
    }

    public void setMKTextPosition(int tp) {
        this.getMK().put(PdfName.TP, new PdfNumber(tp));
    }

    public void setLayer(PdfOCG layer) {
        this.put(PdfName.OC, layer.getRef());
    }
}

