/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.bc.asn1.ASN1EncodableVector;
import com.lowagie.bc.asn1.ASN1InputStream;
import com.lowagie.bc.asn1.ASN1OutputStream;
import com.lowagie.bc.asn1.ASN1Sequence;
import com.lowagie.bc.asn1.ASN1Set;
import com.lowagie.bc.asn1.ASN1TaggedObject;
import com.lowagie.bc.asn1.DERConstructedSet;
import com.lowagie.bc.asn1.DEREncodable;
import com.lowagie.bc.asn1.DEREncodableVector;
import com.lowagie.bc.asn1.DERInteger;
import com.lowagie.bc.asn1.DERNull;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DERObjectIdentifier;
import com.lowagie.bc.asn1.DEROctetString;
import com.lowagie.bc.asn1.DERSequence;
import com.lowagie.bc.asn1.DERSet;
import com.lowagie.bc.asn1.DERString;
import com.lowagie.bc.asn1.DERTaggedObject;
import com.lowagie.bc.asn1.DERUTCTime;
import com.lowagie.text.ExceptionConverter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class PdfPKCS7 {
    private byte[] sigAttr;
    private byte[] digestAttr;
    private int version;
    private int signerversion;
    private Set digestalgos;
    private Collection certs;
    private Collection crls;
    private X509Certificate signCert;
    private byte[] digest;
    private MessageDigest messageDigest;
    private String digestAlgorithm;
    private String digestEncryptionAlgorithm;
    private Signature sig;
    private transient PrivateKey privKey;
    private byte[] RSAdata;
    private boolean verified;
    private boolean verifyResult;
    private byte[] externalDigest;
    private byte[] externalRSAdata;
    private static final String ID_PKCS7_DATA = "1.2.840.113549.1.7.1";
    private static final String ID_PKCS7_SIGNED_DATA = "1.2.840.113549.1.7.2";
    private static final String ID_MD5 = "1.2.840.113549.2.5";
    private static final String ID_MD2 = "1.2.840.113549.2.2";
    private static final String ID_SHA1 = "1.3.14.3.2.26";
    private static final String ID_RSA = "1.2.840.113549.1.1.1";
    private static final String ID_DSA = "1.2.840.10040.4.1";
    private static final String ID_CONTENT_TYPE = "1.2.840.113549.1.9.3";
    private static final String ID_MESSAGE_DIGEST = "1.2.840.113549.1.9.4";
    private static final String ID_SIGNING_TIME = "1.2.840.113549.1.9.5";
    private static final String ID_MD2RSA = "1.2.840.113549.1.1.2";
    private static final String ID_MD5RSA = "1.2.840.113549.1.1.4";
    private static final String ID_SHA1RSA = "1.2.840.113549.1.1.5";
    private String reason;
    private String location;
    private Calendar signDate;
    private String signName;

    public PdfPKCS7(byte[] contentsKey, byte[] certsKey, String provider) throws SecurityException, CRLException, InvalidKeyException, CertificateException, NoSuchProviderException, NoSuchAlgorithmException, IOException {
        CertificateFactory cf = provider == null ? CertificateFactory.getInstance("X.509") : CertificateFactory.getInstance("X.509", provider);
        if (provider == null) {
            this.certs = cf.generateCertificates(new ByteArrayInputStream(certsKey));
        }
        this.signCert = (X509Certificate)this.certs.iterator().next();
        this.crls = new ArrayList();
        ASN1InputStream in = new ASN1InputStream(new ByteArrayInputStream(contentsKey));
        this.digest = ((DEROctetString)in.readObject()).getOctets();
        this.sig = provider == null ? Signature.getInstance("SHA1withRSA") : Signature.getInstance("SHA1withRSA", provider);
        this.sig.initVerify(this.signCert.getPublicKey());
    }

    public PdfPKCS7(byte[] contentsKey, String provider) throws SecurityException, CRLException, InvalidKeyException, CertificateException, NoSuchProviderException, NoSuchAlgorithmException {
        DERObject pkcs;
        ASN1InputStream din = new ASN1InputStream(new ByteArrayInputStream(contentsKey));
        try {
            pkcs = din.readObject();
        }
        catch (IOException e) {
            throw new SecurityException("can't decode PKCS7SignedData object");
        }
        if (!(pkcs instanceof ASN1Sequence)) {
            throw new SecurityException("Not a valid PKCS#7 object - not a sequence");
        }
        ASN1Sequence signedData = (ASN1Sequence)pkcs;
        DERObjectIdentifier objId = (DERObjectIdentifier)signedData.getObjectAt(0);
        if (!objId.getId().equals("1.2.840.113549.1.7.2")) {
            throw new SecurityException("Not a valid PKCS#7 object - not signed data");
        }
        ASN1Sequence content = (ASN1Sequence)((DERTaggedObject)signedData.getObjectAt(1)).getObject();
        this.version = ((DERInteger)content.getObjectAt(0)).getValue().intValue();
        this.digestalgos = new HashSet();
        Enumeration e = ((ASN1Set)content.getObjectAt(1)).getObjects();
        while (e.hasMoreElements()) {
            ASN1Sequence s = (ASN1Sequence)e.nextElement();
            DERObjectIdentifier o = (DERObjectIdentifier)s.getObjectAt(0);
            this.digestalgos.add(o.getId());
        }
        CertificateFactory cf = provider == null ? CertificateFactory.getInstance("X.509") : CertificateFactory.getInstance("X.509", provider);
        this.certs = cf.generateCertificates(new ByteArrayInputStream(contentsKey));
        this.crls = cf.generateCRLs(new ByteArrayInputStream(contentsKey));
        ASN1Sequence rsaData = (ASN1Sequence)content.getObjectAt(2);
        if (rsaData.size() > 1) {
            DEROctetString rsaDataContent = (DEROctetString)((DERTaggedObject)rsaData.getObjectAt(1)).getObject();
            this.RSAdata = rsaDataContent.getOctets();
        }
        int next = 3;
        while (content.getObjectAt(next) instanceof DERTaggedObject) {
            ++next;
        }
        ASN1Set signerInfos = (ASN1Set)content.getObjectAt(next);
        if (signerInfos.size() != 1) {
            throw new SecurityException("This PKCS#7 object has multiple SignerInfos - only one is supported at this time");
        }
        ASN1Sequence signerInfo = (ASN1Sequence)signerInfos.getObjectAt(0);
        this.signerversion = ((DERInteger)signerInfo.getObjectAt(0)).getValue().intValue();
        ASN1Sequence issuerAndSerialNumber = (ASN1Sequence)signerInfo.getObjectAt(1);
        BigInteger serialNumber = ((DERInteger)issuerAndSerialNumber.getObjectAt(1)).getValue();
        Iterator i = this.certs.iterator();
        while (i.hasNext()) {
            X509Certificate cert = (X509Certificate)i.next();
            if (!serialNumber.equals(cert.getSerialNumber())) continue;
            this.signCert = cert;
            break;
        }
        if (this.signCert == null) {
            throw new SecurityException("Can't find signing certificate with serial " + serialNumber.toString(16));
        }
        this.digestAlgorithm = ((DERObjectIdentifier)((ASN1Sequence)signerInfo.getObjectAt(2)).getObjectAt(0)).getId();
        next = 3;
        if (signerInfo.getObjectAt(next) instanceof ASN1TaggedObject) {
            ASN1TaggedObject tagsig = (ASN1TaggedObject)signerInfo.getObjectAt(next);
            ASN1Sequence sseq = (ASN1Sequence)tagsig.getObject();
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            ASN1OutputStream dout = new ASN1OutputStream(bOut);
            try {
                ASN1EncodableVector attribute = new ASN1EncodableVector();
                for (int k = 0; k < sseq.size(); ++k) {
                    attribute.add(sseq.getObjectAt(k));
                }
                dout.writeObject(new DERSet(attribute));
                dout.close();
            }
            catch (IOException attribute) {
                // empty catch block
            }
            this.sigAttr = bOut.toByteArray();
            for (int k = 0; k < sseq.size(); ++k) {
                ASN1Sequence seq2 = (ASN1Sequence)sseq.getObjectAt(k);
                if (!((DERObjectIdentifier)seq2.getObjectAt(0)).getId().equals("1.2.840.113549.1.9.4")) continue;
                ASN1Set set = (ASN1Set)seq2.getObjectAt(1);
                this.digestAttr = ((DEROctetString)set.getObjectAt(0)).getOctets();
                break;
            }
            if (this.digestAttr == null) {
                throw new SecurityException("Authenticated attribute is missing the digest.");
            }
            ++next;
        }
        this.digestEncryptionAlgorithm = ((DERObjectIdentifier)((ASN1Sequence)signerInfo.getObjectAt(next++)).getObjectAt(0)).getId();
        this.digest = ((DEROctetString)signerInfo.getObjectAt(next)).getOctets();
        if (this.RSAdata != null || this.digestAttr != null) {
            this.messageDigest = provider == null || provider.startsWith("SunPKCS11") ? MessageDigest.getInstance(this.getHashAlgorithm()) : MessageDigest.getInstance(this.getHashAlgorithm(), provider);
        }
        this.sig = provider == null ? Signature.getInstance(this.getDigestAlgorithm()) : Signature.getInstance(this.getDigestAlgorithm(), provider);
        this.sig.initVerify(this.signCert.getPublicKey());
    }

    public PdfPKCS7(PrivateKey privKey, Certificate[] certChain, CRL[] crlList, String hashAlgorithm, String provider, boolean hasRSAdata) throws SecurityException, InvalidKeyException, NoSuchProviderException, NoSuchAlgorithmException {
        int i;
        this.privKey = privKey;
        if (hashAlgorithm.equals("MD5")) {
            this.digestAlgorithm = "1.2.840.113549.2.5";
        } else if (hashAlgorithm.equals("MD2")) {
            this.digestAlgorithm = "1.2.840.113549.2.2";
        } else if (hashAlgorithm.equals("SHA")) {
            this.digestAlgorithm = "1.3.14.3.2.26";
        } else if (hashAlgorithm.equals("SHA1")) {
            this.digestAlgorithm = "1.3.14.3.2.26";
        } else {
            throw new NoSuchAlgorithmException("Unknown Hash Algorithm " + hashAlgorithm);
        }
        this.signerversion = 1;
        this.version = 1;
        this.certs = new ArrayList();
        this.crls = new ArrayList();
        this.digestalgos = new HashSet();
        this.digestalgos.add(this.digestAlgorithm);
        this.signCert = (X509Certificate)certChain[0];
        for (i = 0; i < certChain.length; ++i) {
            this.certs.add(certChain[i]);
        }
        if (crlList != null) {
            for (i = 0; i < crlList.length; ++i) {
                this.crls.add(crlList[i]);
            }
        }
        if (privKey != null) {
            this.digestEncryptionAlgorithm = privKey.getAlgorithm();
            if (this.digestEncryptionAlgorithm.equals("RSA")) {
                this.digestEncryptionAlgorithm = "1.2.840.113549.1.1.1";
            } else if (this.digestEncryptionAlgorithm.equals("DSA")) {
                this.digestEncryptionAlgorithm = "1.2.840.10040.4.1";
            } else {
                throw new NoSuchAlgorithmException("Unknown Key Algorithm " + this.digestEncryptionAlgorithm);
            }
        }
        if (hasRSAdata) {
            this.RSAdata = new byte[0];
            this.messageDigest = provider == null || provider.startsWith("SunPKCS11") ? MessageDigest.getInstance(this.getHashAlgorithm()) : MessageDigest.getInstance(this.getHashAlgorithm(), provider);
        }
        if (privKey != null) {
            this.sig = provider == null ? Signature.getInstance(this.getDigestAlgorithm()) : Signature.getInstance(this.getDigestAlgorithm(), provider);
            this.sig.initSign(privKey);
        }
    }

    public void update(byte[] buf, int off, int len) throws SignatureException {
        if (this.RSAdata != null || this.digestAttr != null) {
            this.messageDigest.update(buf, off, len);
        } else {
            this.sig.update(buf, off, len);
        }
    }

    public boolean verify() throws SignatureException {
        if (this.verified) {
            return this.verifyResult;
        }
        if (this.sigAttr != null) {
            this.sig.update(this.sigAttr);
            if (this.RSAdata != null) {
                byte[] msd = this.messageDigest.digest();
                this.messageDigest.update(msd);
            }
            this.verifyResult = Arrays.equals(this.messageDigest.digest(), this.digestAttr) && this.sig.verify(this.digest);
        } else {
            if (this.RSAdata != null) {
                this.sig.update(this.messageDigest.digest());
            }
            this.verifyResult = this.sig.verify(this.digest);
        }
        this.verified = true;
        return this.verifyResult;
    }

    public Certificate[] getCertificates() {
        return this.certs.toArray(new X509Certificate[0]);
    }

    public Collection getCRLs() {
        return this.crls;
    }

    public X509Certificate getSigningCertificate() {
        return this.signCert;
    }

    public int getVersion() {
        return this.version;
    }

    public int getSigningInfoVersion() {
        return this.signerversion;
    }

    public String getDigestAlgorithm() {
        String dea = this.digestEncryptionAlgorithm;
        if (this.digestEncryptionAlgorithm.equals("1.2.840.113549.1.1.1")) {
            dea = "RSA";
        } else if (this.digestEncryptionAlgorithm.equals("1.2.840.10040.4.1")) {
            dea = "DSA";
        }
        return String.valueOf(this.getHashAlgorithm()) + "with" + dea;
    }

    public String getHashAlgorithm() {
        String da = this.digestAlgorithm;
        if (this.digestAlgorithm.equals("1.2.840.113549.2.5") || this.digestAlgorithm.equals("1.2.840.113549.1.1.4")) {
            da = "MD5";
        } else if (this.digestAlgorithm.equals("1.2.840.113549.2.2") || this.digestAlgorithm.equals("1.2.840.113549.1.1.2")) {
            da = "MD2";
        } else if (this.digestAlgorithm.equals("1.3.14.3.2.26") || this.digestAlgorithm.equals("1.2.840.113549.1.1.5")) {
            da = "SHA1";
        }
        return da;
    }

    public static KeyStore loadCacertsKeyStore() {
        return PdfPKCS7.loadCacertsKeyStore(null);
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    public static KeyStore loadCacertsKeyStore(String provider) {
        file = new File(System.getProperty("java.home"), "lib");
        file = new File(file, "security");
        file = new File(file, "cacerts");
        fin = null;
        try {
            try {
                fin = new FileInputStream(file);
                k = provider == null ? KeyStore.getInstance("JKS") : KeyStore.getInstance("JKS", provider);
                k.load(fin, null);
                keyStore = k;
                var4_6 = null;
                try {
                    fin.close();
                    return keyStore;
                }
                catch (Exception ex) {
                    // empty catch block
                }
                return keyStore;
            }
            catch (Exception e) {
                throw new ExceptionConverter(e);
            }
        }
        catch (Throwable throwable) {
            var4_7 = null;
            ** try [egrp 2[TRYBLOCK] [3 : 109->116)] { 
lbl25: // 1 sources:
            fin.close();
            throw throwable;
lbl27: // 1 sources:
            catch (Exception ex) {
                // empty catch block
            }
            throw throwable;
        }
    }

    public static String verifyCertificate(X509Certificate cert, Collection crls, Calendar calendar) {
        if (calendar == null) {
            calendar = new GregorianCalendar();
        }
        if (cert.hasUnsupportedCriticalExtension()) {
            return "Has unsupported critical extension";
        }
        try {
            cert.checkValidity(calendar.getTime());
        }
        catch (Exception e) {
            return e.getMessage();
        }
        if (crls != null) {
            Iterator it = crls.iterator();
            while (it.hasNext()) {
                if (!((CRL)it.next()).isRevoked(cert)) continue;
                return "Certificate revoked";
            }
        }
        return null;
    }

    public static Object[] verifyCertificates(Certificate[] certs, KeyStore keystore, Collection crls, Calendar calendar) {
        if (calendar == null) {
            calendar = new GregorianCalendar();
        }
        ArrayList fails = new ArrayList();
        for (int k = 0; k < certs.length; ++k) {
            int j;
            X509Certificate cert = (X509Certificate)certs[k];
            String err = PdfPKCS7.verifyCertificate(cert, crls, calendar);
            if (err != null) {
                return new Object[]{cert, err};
            }
            try {
                Enumeration<String> aliases = keystore.aliases();
                while (aliases.hasMoreElements()) {
                    try {
                        String alias = aliases.nextElement();
                        if (!keystore.isCertificateEntry(alias)) continue;
                        X509Certificate certStoreX509 = (X509Certificate)keystore.getCertificate(alias);
                        if (PdfPKCS7.verifyCertificate(certStoreX509, crls, calendar) != null) continue;
                        try {
                            cert.verify(certStoreX509.getPublicKey());
                            return null;
                        }
                        catch (Exception e) {
                            continue;
                        }
                    }
                    catch (Exception alias) {
                        // empty catch block
                    }
                }
            }
            catch (Exception aliases) {
                // empty catch block
            }
            for (j = 0; j < certs.length; ++j) {
                if (j == k) continue;
                X509Certificate certNext = (X509Certificate)certs[j];
                try {
                    cert.verify(certNext.getPublicKey());
                    break;
                }
                catch (Exception certStoreX509) {
                    // empty catch block
                }
            }
            if (j != certs.length) continue;
            return new Object[]{cert, "Cannot be verified against the KeyStore or the certificate chain"};
        }
        Object[] arrobject = new Object[2];
        arrobject[1] = "Invalid state. Possible circular certificate chain";
        return arrobject;
    }

    private static DERObject getIssuer(byte[] enc) {
        try {
            ASN1InputStream in = new ASN1InputStream(new ByteArrayInputStream(enc));
            ASN1Sequence seq = (ASN1Sequence)in.readObject();
            return (DERObject)seq.getObjectAt(seq.getObjectAt(0) instanceof DERTaggedObject ? 3 : 2);
        }
        catch (IOException e) {
            throw new ExceptionConverter(e);
        }
    }

    private static DERObject getSubject(byte[] enc) {
        try {
            ASN1InputStream in = new ASN1InputStream(new ByteArrayInputStream(enc));
            ASN1Sequence seq = (ASN1Sequence)in.readObject();
            return (DERObject)seq.getObjectAt(seq.getObjectAt(0) instanceof DERTaggedObject ? 5 : 4);
        }
        catch (IOException e) {
            throw new ExceptionConverter(e);
        }
    }

    public static X509Name getIssuerFields(X509Certificate cert) {
        try {
            return new X509Name((ASN1Sequence)PdfPKCS7.getIssuer(cert.getTBSCertificate()));
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    public static X509Name getSubjectFields(X509Certificate cert) {
        try {
            return new X509Name((ASN1Sequence)PdfPKCS7.getSubject(cert.getTBSCertificate()));
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    public byte[] getEncodedPKCS1() {
        try {
            this.digest = this.externalDigest != null ? this.externalDigest : this.sig.sign();
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            ASN1OutputStream dout = new ASN1OutputStream(bOut);
            dout.writeObject(new DEROctetString(this.digest));
            dout.close();
            return bOut.toByteArray();
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    public void setExternalDigest(byte[] digest, byte[] RSAdata, String digestEncryptionAlgorithm) {
        this.externalDigest = digest;
        this.externalRSAdata = RSAdata;
        if (digestEncryptionAlgorithm != null) {
            if (digestEncryptionAlgorithm.equals("RSA")) {
                this.digestEncryptionAlgorithm = "1.2.840.113549.1.1.1";
            } else if (digestEncryptionAlgorithm.equals("DSA")) {
                this.digestEncryptionAlgorithm = "1.2.840.10040.4.1";
            } else {
                throw new ExceptionConverter(new NoSuchAlgorithmException("Unknown Key Algorithm " + digestEncryptionAlgorithm));
            }
        }
    }

    public byte[] getEncodedPKCS7() {
        return this.getEncodedPKCS7(null, null);
    }

    public byte[] getEncodedPKCS7(byte[] secondDigest, Calendar signingTime) {
        try {
            if (this.externalDigest != null) {
                this.digest = this.externalDigest;
                if (this.RSAdata != null) {
                    this.RSAdata = this.externalRSAdata;
                }
            } else if (this.externalRSAdata != null && this.RSAdata != null) {
                this.RSAdata = this.externalRSAdata;
                this.sig.update(this.RSAdata);
                this.digest = this.sig.sign();
            } else {
                if (this.RSAdata != null) {
                    this.RSAdata = this.messageDigest.digest();
                    this.sig.update(this.RSAdata);
                }
                this.digest = this.sig.sign();
            }
            DERConstructedSet digestAlgorithms = new DERConstructedSet();
            Iterator it = this.digestalgos.iterator();
            while (it.hasNext()) {
                ASN1EncodableVector algos = new ASN1EncodableVector();
                algos.add(new DERObjectIdentifier((String)it.next()));
                algos.add(new DERNull());
                digestAlgorithms.addObject(new DERSequence(algos));
            }
            ASN1EncodableVector v = new ASN1EncodableVector();
            v.add(new DERObjectIdentifier("1.2.840.113549.1.7.1"));
            if (this.RSAdata != null) {
                v.add(new DERTaggedObject(0, new DEROctetString(this.RSAdata)));
            }
            DERSequence contentinfo = new DERSequence(v);
            v = new ASN1EncodableVector();
            Iterator i = this.certs.iterator();
            while (i.hasNext()) {
                ASN1InputStream tempstream = new ASN1InputStream(new ByteArrayInputStream(((X509Certificate)i.next()).getEncoded()));
                v.add(tempstream.readObject());
            }
            DERSet dercertificates = new DERSet(v);
            ASN1EncodableVector signerinfo = new ASN1EncodableVector();
            signerinfo.add(new DERInteger(this.signerversion));
            v = new ASN1EncodableVector();
            v.add(PdfPKCS7.getIssuer(this.signCert.getTBSCertificate()));
            v.add(new DERInteger(this.signCert.getSerialNumber()));
            signerinfo.add(new DERSequence(v));
            v = new ASN1EncodableVector();
            v.add(new DERObjectIdentifier(this.digestAlgorithm));
            v.add(new DERNull());
            signerinfo.add(new DERSequence(v));
            if (secondDigest != null && signingTime != null) {
                ASN1EncodableVector attribute = new ASN1EncodableVector();
                v = new ASN1EncodableVector();
                v.add(new DERObjectIdentifier("1.2.840.113549.1.9.3"));
                v.add(new DERSet(new DERObjectIdentifier("1.2.840.113549.1.7.1")));
                attribute.add(new DERSequence(v));
                v = new ASN1EncodableVector();
                v.add(new DERObjectIdentifier("1.2.840.113549.1.9.5"));
                v.add(new DERSet(new DERUTCTime(signingTime.getTime())));
                attribute.add(new DERSequence(v));
                v = new ASN1EncodableVector();
                v.add(new DERObjectIdentifier("1.2.840.113549.1.9.4"));
                v.add(new DERSet(new DEROctetString(secondDigest)));
                attribute.add(new DERSequence(v));
                signerinfo.add(new DERTaggedObject(false, 0, new DERSet(attribute)));
            }
            v = new ASN1EncodableVector();
            v.add(new DERObjectIdentifier(this.digestEncryptionAlgorithm));
            v.add(new DERNull());
            signerinfo.add(new DERSequence(v));
            signerinfo.add(new DEROctetString(this.digest));
            ASN1EncodableVector body = new ASN1EncodableVector();
            body.add(new DERInteger(this.version));
            body.add(digestAlgorithms);
            body.add(contentinfo);
            body.add(new DERTaggedObject(false, 0, dercertificates));
            if (this.crls.size() > 0) {
                v = new ASN1EncodableVector();
                Iterator i2 = this.crls.iterator();
                while (i2.hasNext()) {
                    ASN1InputStream t = new ASN1InputStream(new ByteArrayInputStream(((X509CRL)i2.next()).getEncoded()));
                    v.add(t.readObject());
                }
                DERSet dercrls = new DERSet(v);
                body.add(new DERTaggedObject(false, 1, dercrls));
            }
            body.add(new DERSet(new DERSequence(signerinfo)));
            ASN1EncodableVector whole = new ASN1EncodableVector();
            whole.add(new DERObjectIdentifier("1.2.840.113549.1.7.2"));
            whole.add(new DERTaggedObject(0, new DERSequence(body)));
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            ASN1OutputStream dout = new ASN1OutputStream(bOut);
            dout.writeObject(new DERSequence(whole));
            dout.close();
            return bOut.toByteArray();
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    public byte[] getAuthenticatedAttributeBytes(byte[] secondDigest, Calendar signingTime) {
        try {
            ASN1EncodableVector attribute = new ASN1EncodableVector();
            ASN1EncodableVector v = new ASN1EncodableVector();
            v.add(new DERObjectIdentifier("1.2.840.113549.1.9.3"));
            v.add(new DERSet(new DERObjectIdentifier("1.2.840.113549.1.7.1")));
            attribute.add(new DERSequence(v));
            v = new ASN1EncodableVector();
            v.add(new DERObjectIdentifier("1.2.840.113549.1.9.5"));
            v.add(new DERSet(new DERUTCTime(signingTime.getTime())));
            attribute.add(new DERSequence(v));
            v = new ASN1EncodableVector();
            v.add(new DERObjectIdentifier("1.2.840.113549.1.9.4"));
            v.add(new DERSet(new DEROctetString(secondDigest)));
            attribute.add(new DERSequence(v));
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            ASN1OutputStream dout = new ASN1OutputStream(bOut);
            dout.writeObject(new DERSet(attribute));
            dout.close();
            return bOut.toByteArray();
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Calendar getSignDate() {
        return this.signDate;
    }

    public void setSignDate(Calendar signDate) {
        this.signDate = signDate;
    }

    public String getSignName() {
        return this.signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public static class X509Name {
        public static final DERObjectIdentifier C = new DERObjectIdentifier("2.5.4.6");
        public static final DERObjectIdentifier O = new DERObjectIdentifier("2.5.4.10");
        public static final DERObjectIdentifier OU = new DERObjectIdentifier("2.5.4.11");
        public static final DERObjectIdentifier T = new DERObjectIdentifier("2.5.4.12");
        public static final DERObjectIdentifier CN = new DERObjectIdentifier("2.5.4.3");
        public static final DERObjectIdentifier SN = new DERObjectIdentifier("2.5.4.5");
        public static final DERObjectIdentifier L = new DERObjectIdentifier("2.5.4.7");
        public static final DERObjectIdentifier ST = new DERObjectIdentifier("2.5.4.8");
        public static final DERObjectIdentifier SURNAME = new DERObjectIdentifier("2.5.4.4");
        public static final DERObjectIdentifier GIVENNAME = new DERObjectIdentifier("2.5.4.42");
        public static final DERObjectIdentifier INITIALS = new DERObjectIdentifier("2.5.4.43");
        public static final DERObjectIdentifier GENERATION = new DERObjectIdentifier("2.5.4.44");
        public static final DERObjectIdentifier UNIQUE_IDENTIFIER = new DERObjectIdentifier("2.5.4.45");
        public static final DERObjectIdentifier EmailAddress;
        public static final DERObjectIdentifier E;
        public static final DERObjectIdentifier DC;
        public static final DERObjectIdentifier UID;
        public static HashMap DefaultSymbols;
        public HashMap values = new HashMap();

        static {
            E = X509Name.EmailAddress = new DERObjectIdentifier("1.2.840.113549.1.9.1");
            DC = new DERObjectIdentifier("0.9.2342.19200300.100.1.25");
            UID = new DERObjectIdentifier("0.9.2342.19200300.100.1.1");
            DefaultSymbols = new HashMap();
            DefaultSymbols.put(C, "C");
            DefaultSymbols.put(O, "O");
            DefaultSymbols.put(T, "T");
            DefaultSymbols.put(OU, "OU");
            DefaultSymbols.put(CN, "CN");
            DefaultSymbols.put(L, "L");
            DefaultSymbols.put(ST, "ST");
            DefaultSymbols.put(SN, "SN");
            DefaultSymbols.put(EmailAddress, "E");
            DefaultSymbols.put(DC, "DC");
            DefaultSymbols.put(UID, "UID");
            DefaultSymbols.put(SURNAME, "SURNAME");
            DefaultSymbols.put(GIVENNAME, "GIVENNAME");
            DefaultSymbols.put(INITIALS, "INITIALS");
            DefaultSymbols.put(GENERATION, "GENERATION");
        }

        public X509Name(ASN1Sequence seq) {
            Enumeration e = seq.getObjects();
            while (e.hasMoreElements()) {
                ASN1Set set = (ASN1Set)e.nextElement();
                for (int i = 0; i < set.size(); ++i) {
                    ASN1Sequence s = (ASN1Sequence)set.getObjectAt(i);
                    String id = (String)DefaultSymbols.get(s.getObjectAt(0));
                    if (id == null) continue;
                    ArrayList<String> vs = (ArrayList<String>)this.values.get(id);
                    if (vs == null) {
                        vs = new ArrayList<String>();
                        this.values.put(id, vs);
                    }
                    vs.add(((DERString)s.getObjectAt(1)).getString());
                }
            }
        }

        public X509Name(String dirName) {
            X509NameTokenizer nTok = new X509NameTokenizer(dirName);
            while (nTok.hasMoreTokens()) {
                String token = nTok.nextToken();
                int index = token.indexOf(61);
                if (index == -1) {
                    throw new IllegalArgumentException("badly formated directory string");
                }
                String id = token.substring(0, index).toUpperCase();
                String value = token.substring(index + 1);
                ArrayList<String> vs = (ArrayList<String>)this.values.get(id);
                if (vs == null) {
                    vs = new ArrayList<String>();
                    this.values.put(id, vs);
                }
                vs.add(value);
            }
        }

        public String getField(String name) {
            ArrayList vs = (ArrayList)this.values.get(name);
            return vs == null ? null : (String)vs.get(0);
        }

        public ArrayList getFieldArray(String name) {
            ArrayList vs = (ArrayList)this.values.get(name);
            return vs == null ? null : vs;
        }

        public HashMap getFields() {
            return this.values;
        }

        public String toString() {
            return this.values.toString();
        }
    }

    public static class X509NameTokenizer {
        private String oid;
        private int index;
        private StringBuffer buf = new StringBuffer();

        public X509NameTokenizer(String oid) {
            this.oid = oid;
            this.index = -1;
        }

        public boolean hasMoreTokens() {
            if (this.index != this.oid.length()) {
                return true;
            }
            return false;
        }

        public String nextToken() {
            int end;
            if (this.index == this.oid.length()) {
                return null;
            }
            boolean quoted = false;
            boolean escaped = false;
            this.buf.setLength(0);
            for (end = this.index + 1; end != this.oid.length(); ++end) {
                char c = this.oid.charAt(end);
                if (c == '\"') {
                    if (!escaped) {
                        quoted = !quoted;
                    } else {
                        this.buf.append(c);
                    }
                    escaped = false;
                    continue;
                }
                if (escaped || quoted) {
                    this.buf.append(c);
                    escaped = false;
                    continue;
                }
                if (c == '\\') {
                    escaped = true;
                    continue;
                }
                if (c == ',') break;
                this.buf.append(c);
            }
            this.index = end;
            return this.buf.toString().trim();
        }
    }

}

