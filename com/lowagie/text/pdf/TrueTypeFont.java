/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocumentException;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.IntHashtable;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfEncodings;
import com.lowagie.text.pdf.PdfIndirectObject;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfRectangle;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.TrueTypeFontSubSet;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

class TrueTypeFont
extends BaseFont {
    static final String[] codePages;
    protected boolean justNames = false;
    protected HashMap tables;
    protected RandomAccessFileOrArray rf;
    protected String fileName;
    protected boolean cff = false;
    protected int cffOffset;
    protected int cffLength;
    protected int directoryOffset;
    protected String ttcIndex;
    protected String style = "";
    protected FontHeader head = new FontHeader();
    protected HorizontalHeader hhea = new HorizontalHeader();
    protected WindowsMetrics os_2 = new WindowsMetrics();
    protected int[] GlyphWidths;
    protected int[][] bboxes;
    protected HashMap cmap10;
    protected HashMap cmap31;
    protected IntHashtable kerning = new IntHashtable();
    protected String fontName;
    protected String[][] fullName;
    protected String[][] familyName;
    protected double italicAngle;
    protected boolean isFixedPitch = false;

    static {
        String[] arrstring = new String[64];
        arrstring[0] = "1252 Latin 1";
        arrstring[1] = "1250 Latin 2: Eastern Europe";
        arrstring[2] = "1251 Cyrillic";
        arrstring[3] = "1253 Greek";
        arrstring[4] = "1254 Turkish";
        arrstring[5] = "1255 Hebrew";
        arrstring[6] = "1256 Arabic";
        arrstring[7] = "1257 Windows Baltic";
        arrstring[8] = "1258 Vietnamese";
        arrstring[16] = "874 Thai";
        arrstring[17] = "932 JIS/Japan";
        arrstring[18] = "936 Chinese: Simplified chars--PRC and Singapore";
        arrstring[19] = "949 Korean Wansung";
        arrstring[20] = "950 Chinese: Traditional chars--Taiwan and Hong Kong";
        arrstring[21] = "1361 Korean Johab";
        arrstring[29] = "Macintosh Character Set (US Roman)";
        arrstring[30] = "OEM Character Set";
        arrstring[31] = "Symbol Character Set";
        arrstring[48] = "869 IBM Greek";
        arrstring[49] = "866 MS-DOS Russian";
        arrstring[50] = "865 MS-DOS Nordic";
        arrstring[51] = "864 Arabic";
        arrstring[52] = "863 MS-DOS Canadian French";
        arrstring[53] = "862 Hebrew";
        arrstring[54] = "861 MS-DOS Icelandic";
        arrstring[55] = "860 MS-DOS Portuguese";
        arrstring[56] = "857 IBM Turkish";
        arrstring[57] = "855 IBM Cyrillic; primarily Russian";
        arrstring[58] = "852 Latin 2";
        arrstring[59] = "775 MS-DOS Baltic";
        arrstring[60] = "737 Greek; former 437 G";
        arrstring[61] = "708 Arabic; ASMO 708";
        arrstring[62] = "850 WE/Latin 1";
        arrstring[63] = "437 US";
        codePages = arrstring;
    }

    protected TrueTypeFont() {
    }

    TrueTypeFont(String ttFile, String enc, boolean emb, byte[] ttfAfm) throws DocumentException, IOException {
        this(ttFile, enc, emb, ttfAfm, false);
    }

    TrueTypeFont(String ttFile, String enc, boolean emb, byte[] ttfAfm, boolean justNames) throws DocumentException, IOException {
        this.justNames = justNames;
        String nameBase = BaseFont.getBaseName(ttFile);
        String ttcName = TrueTypeFont.getTTCName(nameBase);
        if (nameBase.length() < ttFile.length()) {
            this.style = ttFile.substring(nameBase.length());
        }
        this.encoding = enc;
        this.embedded = emb;
        this.fileName = ttcName;
        this.fontType = 1;
        this.ttcIndex = "";
        if (ttcName.length() < nameBase.length()) {
            this.ttcIndex = nameBase.substring(ttcName.length() + 1);
        }
        if (this.fileName.toLowerCase().endsWith(".ttf") || this.fileName.toLowerCase().endsWith(".otf") || this.fileName.toLowerCase().endsWith(".ttc")) {
            this.process(ttfAfm);
            if (!justNames && this.embedded && this.os_2.fsType == 2) {
                throw new DocumentException(String.valueOf(this.fileName) + this.style + " cannot be embedded due to licensing restrictions.");
            }
        } else {
            throw new DocumentException(String.valueOf(this.fileName) + this.style + " is not a TTF, OTF or TTC font file.");
        }
        PdfEncodings.convertToBytes(" ", enc);
        this.createEncoding();
    }

    protected static String getTTCName(String name) {
        int idx = name.toLowerCase().indexOf(".ttc,");
        if (idx < 0) {
            return name;
        }
        return name.substring(0, idx + 4);
    }

    void fillTables() throws DocumentException, IOException {
        int[] table_location = (int[])this.tables.get("head");
        if (table_location == null) {
            throw new DocumentException("Table 'head' does not exist in " + this.fileName + this.style);
        }
        this.rf.seek(table_location[0] + 16);
        this.head.flags = this.rf.readUnsignedShort();
        this.head.unitsPerEm = this.rf.readUnsignedShort();
        this.rf.skipBytes(16);
        this.head.xMin = this.rf.readShort();
        this.head.yMin = this.rf.readShort();
        this.head.xMax = this.rf.readShort();
        this.head.yMax = this.rf.readShort();
        this.head.macStyle = this.rf.readUnsignedShort();
        table_location = (int[])this.tables.get("hhea");
        if (table_location == null) {
            throw new DocumentException("Table 'hhea' does not exist " + this.fileName + this.style);
        }
        this.rf.seek(table_location[0] + 4);
        this.hhea.Ascender = this.rf.readShort();
        this.hhea.Descender = this.rf.readShort();
        this.hhea.LineGap = this.rf.readShort();
        this.hhea.advanceWidthMax = this.rf.readUnsignedShort();
        this.hhea.minLeftSideBearing = this.rf.readShort();
        this.hhea.minRightSideBearing = this.rf.readShort();
        this.hhea.xMaxExtent = this.rf.readShort();
        this.hhea.caretSlopeRise = this.rf.readShort();
        this.hhea.caretSlopeRun = this.rf.readShort();
        this.rf.skipBytes(12);
        this.hhea.numberOfHMetrics = this.rf.readUnsignedShort();
        table_location = (int[])this.tables.get("OS/2");
        if (table_location == null) {
            throw new DocumentException("Table 'OS/2' does not exist in " + this.fileName + this.style);
        }
        this.rf.seek(table_location[0]);
        int version = this.rf.readUnsignedShort();
        this.os_2.xAvgCharWidth = this.rf.readShort();
        this.os_2.usWeightClass = this.rf.readUnsignedShort();
        this.os_2.usWidthClass = this.rf.readUnsignedShort();
        this.os_2.fsType = this.rf.readShort();
        this.os_2.ySubscriptXSize = this.rf.readShort();
        this.os_2.ySubscriptYSize = this.rf.readShort();
        this.os_2.ySubscriptXOffset = this.rf.readShort();
        this.os_2.ySubscriptYOffset = this.rf.readShort();
        this.os_2.ySuperscriptXSize = this.rf.readShort();
        this.os_2.ySuperscriptYSize = this.rf.readShort();
        this.os_2.ySuperscriptXOffset = this.rf.readShort();
        this.os_2.ySuperscriptYOffset = this.rf.readShort();
        this.os_2.yStrikeoutSize = this.rf.readShort();
        this.os_2.yStrikeoutPosition = this.rf.readShort();
        this.os_2.sFamilyClass = this.rf.readShort();
        this.rf.readFully(this.os_2.panose);
        this.rf.skipBytes(16);
        this.rf.readFully(this.os_2.achVendID);
        this.os_2.fsSelection = this.rf.readUnsignedShort();
        this.os_2.usFirstCharIndex = this.rf.readUnsignedShort();
        this.os_2.usLastCharIndex = this.rf.readUnsignedShort();
        this.os_2.sTypoAscender = this.rf.readShort();
        this.os_2.sTypoDescender = this.rf.readShort();
        if (this.os_2.sTypoDescender > 0) {
            this.os_2.sTypoDescender = - this.os_2.sTypoDescender;
        }
        this.os_2.sTypoLineGap = this.rf.readShort();
        this.os_2.usWinAscent = this.rf.readUnsignedShort();
        this.os_2.usWinDescent = this.rf.readUnsignedShort();
        this.os_2.ulCodePageRange1 = 0;
        this.os_2.ulCodePageRange2 = 0;
        if (version > 0) {
            this.os_2.ulCodePageRange1 = this.rf.readInt();
            this.os_2.ulCodePageRange2 = this.rf.readInt();
        }
        if (version > 1) {
            this.rf.skipBytes(2);
            this.os_2.sCapHeight = this.rf.readShort();
        } else {
            this.os_2.sCapHeight = (int)(0.7 * (double)this.head.unitsPerEm);
        }
        table_location = (int[])this.tables.get("post");
        if (table_location == null) {
            this.italicAngle = (- Math.atan2(this.hhea.caretSlopeRun, this.hhea.caretSlopeRise)) * 180.0 / 3.141592653589793;
            return;
        }
        this.rf.seek(table_location[0] + 4);
        short mantissa = this.rf.readShort();
        int fraction = this.rf.readUnsignedShort();
        this.italicAngle = (double)mantissa + (double)fraction / 16384.0;
        this.rf.skipBytes(4);
        this.isFixedPitch = this.rf.readInt() != 0;
    }

    String getBaseFont() throws DocumentException, IOException {
        int[] table_location = (int[])this.tables.get("name");
        if (table_location == null) {
            throw new DocumentException("Table 'name' does not exist in " + this.fileName + this.style);
        }
        this.rf.seek(table_location[0] + 2);
        int numRecords = this.rf.readUnsignedShort();
        int startOfStorage = this.rf.readUnsignedShort();
        for (int k = 0; k < numRecords; ++k) {
            int platformID = this.rf.readUnsignedShort();
            int platformEncodingID = this.rf.readUnsignedShort();
            int languageID = this.rf.readUnsignedShort();
            int nameID = this.rf.readUnsignedShort();
            int length = this.rf.readUnsignedShort();
            int offset = this.rf.readUnsignedShort();
            if (nameID != 6) continue;
            this.rf.seek(table_location[0] + startOfStorage + offset);
            if (platformID == 0 || platformID == 3) {
                return this.readUnicodeString(length);
            }
            return this.readStandardString(length);
        }
        File file = new File(this.fileName);
        return file.getName().replace(' ', '-');
    }

    String[][] getNames(int id) throws DocumentException, IOException {
        int[] table_location = (int[])this.tables.get("name");
        if (table_location == null) {
            throw new DocumentException("Table 'name' does not exist in " + this.fileName + this.style);
        }
        this.rf.seek(table_location[0] + 2);
        int numRecords = this.rf.readUnsignedShort();
        int startOfStorage = this.rf.readUnsignedShort();
        ArrayList<String[]> names = new ArrayList<String[]>();
        for (int k = 0; k < numRecords; ++k) {
            int platformID = this.rf.readUnsignedShort();
            int platformEncodingID = this.rf.readUnsignedShort();
            int languageID = this.rf.readUnsignedShort();
            int nameID = this.rf.readUnsignedShort();
            int length = this.rf.readUnsignedShort();
            int offset = this.rf.readUnsignedShort();
            if (nameID != id) continue;
            int pos = this.rf.getFilePointer();
            this.rf.seek(table_location[0] + startOfStorage + offset);
            String name = platformID == 0 || platformID == 3 || platformID == 2 && platformEncodingID == 1 ? this.readUnicodeString(length) : this.readStandardString(length);
            names.add(new String[]{String.valueOf(platformID), String.valueOf(platformEncodingID), String.valueOf(languageID), name});
            this.rf.seek(pos);
        }
        String[][] thisName = new String[names.size()][];
        for (int k2 = 0; k2 < names.size(); ++k2) {
            thisName[k2] = (String[])names.get(k2);
        }
        return thisName;
    }

    void checkCff() throws DocumentException, IOException {
        int[] table_location = (int[])this.tables.get("CFF ");
        if (table_location != null) {
            this.cff = true;
            this.cffOffset = table_location[0];
            this.cffLength = table_location[1];
        }
    }

    /*
     * Exception decompiling
     */
    void process(byte[] ttfAfm) throws DocumentException, IOException {
        // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
        // java.lang.IllegalStateException: Backjump on non jumping statement [] lbl55 : TryStatement: try { 1[TRYBLOCK]

        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.Cleaner$1.call(Cleaner.java:44)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.Cleaner$1.call(Cleaner.java:22)
        // org.benf.cfr.reader.util.graph.GraphVisitorDFS.process(GraphVisitorDFS.java:68)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.Cleaner.removeUnreachableCode(Cleaner.java:54)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.RemoveDeterministicJumps.apply(RemoveDeterministicJumps.java:34)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:501)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:214)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:159)
        // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:91)
        // org.benf.cfr.reader.entities.Method.analyse(Method.java:353)
        // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:731)
        // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:663)
        // org.benf.cfr.reader.Main.doJar(Main.java:126)
        // org.benf.cfr.reader.Main.main(Main.java:178)
        throw new IllegalStateException("Decompilation failed");
    }

    protected String readStandardString(int length) throws IOException {
        byte[] buf = new byte[length];
        this.rf.readFully(buf);
        try {
            return new String(buf, "Cp1252");
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    protected String readUnicodeString(int length) throws IOException {
        StringBuffer buf = new StringBuffer();
        for (int k = 0; k < (length/=2); ++k) {
            buf.append(this.rf.readChar());
        }
        return buf.toString();
    }

    protected void readGlyphWidths() throws DocumentException, IOException {
        int[] table_location = (int[])this.tables.get("hmtx");
        if (table_location == null) {
            throw new DocumentException("Table 'hmtx' does not exist in " + this.fileName + this.style);
        }
        this.rf.seek(table_location[0]);
        this.GlyphWidths = new int[this.hhea.numberOfHMetrics];
        for (int k = 0; k < this.hhea.numberOfHMetrics; ++k) {
            this.GlyphWidths[k] = this.rf.readUnsignedShort() * 1000 / this.head.unitsPerEm;
            this.rf.readUnsignedShort();
        }
    }

    protected int getGlyphWidth(int glyph) {
        if (glyph >= this.GlyphWidths.length) {
            glyph = this.GlyphWidths.length - 1;
        }
        return this.GlyphWidths[glyph];
    }

    private void readBbox() throws DocumentException, IOException {
        int[] locaTable;
        int entries;
        int k;
        int[] tableLocation = (int[])this.tables.get("head");
        if (tableLocation == null) {
            throw new DocumentException("Table 'head' does not exist in " + this.fileName + this.style);
        }
        this.rf.seek(tableLocation[0] + 51);
        boolean locaShortTable = this.rf.readUnsignedShort() == 0;
        tableLocation = (int[])this.tables.get("loca");
        if (tableLocation == null) {
            return;
        }
        this.rf.seek(tableLocation[0]);
        if (locaShortTable) {
            entries = tableLocation[1] / 2;
            locaTable = new int[entries];
            for (k = 0; k < entries; ++k) {
                locaTable[k] = this.rf.readUnsignedShort() * 2;
            }
        } else {
            entries = tableLocation[1] / 4;
            locaTable = new int[entries];
            for (k = 0; k < entries; ++k) {
                locaTable[k] = this.rf.readInt();
            }
        }
        tableLocation = (int[])this.tables.get("glyf");
        if (tableLocation == null) {
            throw new DocumentException("Table 'glyf' does not exist in " + this.fileName + this.style);
        }
        int tableGlyphOffset = tableLocation[0];
        this.bboxes = new int[locaTable.length - 1][];
        for (int glyph = 0; glyph < locaTable.length - 1; ++glyph) {
            int start = locaTable[glyph];
            if (start == locaTable[glyph + 1]) continue;
            this.rf.seek(tableGlyphOffset + start + 2);
            this.bboxes[glyph] = new int[]{this.rf.readShort() * 1000 / this.head.unitsPerEm, this.rf.readShort() * 1000 / this.head.unitsPerEm, this.rf.readShort() * 1000 / this.head.unitsPerEm, this.rf.readShort() * 1000 / this.head.unitsPerEm};
        }
    }

    void readCMaps() throws DocumentException, IOException {
        int format;
        int[] table_location = (int[])this.tables.get("cmap");
        if (table_location == null) {
            throw new DocumentException("Table 'cmap' does not exist in " + this.fileName + this.style);
        }
        this.rf.seek(table_location[0]);
        this.rf.skipBytes(2);
        int num_tables = this.rf.readUnsignedShort();
        this.fontSpecific = false;
        int map10 = 0;
        int map31 = 0;
        int map30 = 0;
        for (int k = 0; k < num_tables; ++k) {
            int platId = this.rf.readUnsignedShort();
            int platSpecId = this.rf.readUnsignedShort();
            int offset = this.rf.readInt();
            if (platId == 3 && platSpecId == 0) {
                this.fontSpecific = true;
                map30 = offset;
            } else if (platId == 3 && platSpecId == 1) {
                map31 = offset;
            }
            if (platId != 1 || platSpecId != 0) continue;
            map10 = offset;
        }
        if (map10 > 0) {
            this.rf.seek(table_location[0] + map10);
            format = this.rf.readUnsignedShort();
            switch (format) {
                case 0: {
                    this.cmap10 = this.readFormat0();
                    break;
                }
                case 4: {
                    this.cmap10 = this.readFormat4();
                    break;
                }
                case 6: {
                    this.cmap10 = this.readFormat6();
                }
            }
        }
        if (map31 > 0) {
            this.rf.seek(table_location[0] + map31);
            format = this.rf.readUnsignedShort();
            if (format == 4) {
                this.cmap31 = this.readFormat4();
            }
        }
        if (map30 > 0) {
            this.rf.seek(table_location[0] + map30);
            format = this.rf.readUnsignedShort();
            if (format == 4) {
                this.cmap10 = this.readFormat4();
            }
        }
    }

    HashMap readFormat0() throws IOException {
        HashMap<Integer, int[]> h = new HashMap<Integer, int[]>();
        this.rf.skipBytes(4);
        for (int k = 0; k < 256; ++k) {
            int[] r = new int[]{this.rf.readUnsignedByte(), this.getGlyphWidth(r[0])};
            h.put(new Integer(k), r);
        }
        return h;
    }

    HashMap readFormat4() throws IOException {
        int k;
        HashMap<Integer, int[]> h = new HashMap<Integer, int[]>();
        int table_lenght = this.rf.readUnsignedShort();
        this.rf.skipBytes(2);
        int segCount = this.rf.readUnsignedShort() / 2;
        this.rf.skipBytes(6);
        int[] endCount = new int[segCount];
        for (int k2 = 0; k2 < segCount; ++k2) {
            endCount[k2] = this.rf.readUnsignedShort();
        }
        this.rf.skipBytes(2);
        int[] startCount = new int[segCount];
        for (int k3 = 0; k3 < segCount; ++k3) {
            startCount[k3] = this.rf.readUnsignedShort();
        }
        int[] idDelta = new int[segCount];
        for (int k4 = 0; k4 < segCount; ++k4) {
            idDelta[k4] = this.rf.readUnsignedShort();
        }
        int[] idRO = new int[segCount];
        for (int k5 = 0; k5 < segCount; ++k5) {
            idRO[k5] = this.rf.readUnsignedShort();
        }
        int[] glyphId = new int[table_lenght / 2 - 8 - segCount * 4];
        for (k = 0; k < glyphId.length; ++k) {
            glyphId[k] = this.rf.readUnsignedShort();
        }
        for (k = 0; k < segCount; ++k) {
            for (int j = startCount[k]; j <= endCount[k] && j != 65535; ++j) {
                int glyph;
                if (idRO[k] == 0) {
                    glyph = j + idDelta[k] & 65535;
                } else {
                    int idx = k + idRO[k] / 2 - segCount + j - startCount[k];
                    if (idx >= glyphId.length) continue;
                    glyph = glyphId[idx] + idDelta[k] & 65535;
                }
                int[] r = new int[]{glyph, this.getGlyphWidth(r[0])};
                h.put(new Integer(this.fontSpecific ? ((j & 65280) == 61440 ? j & 255 : j) : j), r);
            }
        }
        return h;
    }

    HashMap readFormat6() throws IOException {
        HashMap<Integer, int[]> h = new HashMap<Integer, int[]>();
        this.rf.skipBytes(4);
        int start_code = this.rf.readUnsignedShort();
        int code_count = this.rf.readUnsignedShort();
        for (int k = 0; k < code_count; ++k) {
            int[] r = new int[]{this.rf.readUnsignedShort(), this.getGlyphWidth(r[0])};
            h.put(new Integer(k + start_code), r);
        }
        return h;
    }

    void readKerning() throws IOException {
        int[] table_location = (int[])this.tables.get("kern");
        if (table_location == null) {
            return;
        }
        this.rf.seek(table_location[0] + 2);
        int nTables = this.rf.readUnsignedShort();
        int checkpoint = table_location[0] + 4;
        int length = 0;
        for (int k = 0; k < nTables; ++k) {
            this.rf.seek(checkpoint+=length);
            this.rf.skipBytes(2);
            length = this.rf.readUnsignedShort();
            int coverage = this.rf.readUnsignedShort();
            if ((coverage & 65527) != 1) continue;
            int nPairs = this.rf.readUnsignedShort();
            this.rf.skipBytes(6);
            for (int j = 0; j < nPairs; ++j) {
                int pair = this.rf.readInt();
                int value = this.rf.readShort() * 1000 / this.head.unitsPerEm;
                this.kerning.put(pair, value);
            }
        }
    }

    public int getKerning(char char1, char char2) {
        int[] metrics = this.getMetricsTT(char1);
        if (metrics == null) {
            return 0;
        }
        int c1 = metrics[0];
        metrics = this.getMetricsTT(char2);
        if (metrics == null) {
            return 0;
        }
        int c2 = metrics[0];
        return this.kerning.get((c1 << 16) + c2);
    }

    int getRawWidth(int c, String name) {
        HashMap map = null;
        map = name == null || this.cmap31 == null ? this.cmap10 : this.cmap31;
        if (map == null) {
            return 0;
        }
        int[] metric = (int[])map.get(new Integer(c));
        if (metric == null) {
            return 0;
        }
        return metric[1];
    }

    protected PdfDictionary getFontDescriptor(PdfIndirectReference fontStream, String subsetPrefix) throws DocumentException {
        PdfDictionary dic = new PdfDictionary(PdfName.FONTDESCRIPTOR);
        dic.put(PdfName.ASCENT, new PdfNumber(this.os_2.sTypoAscender * 1000 / this.head.unitsPerEm));
        dic.put(PdfName.CAPHEIGHT, new PdfNumber(this.os_2.sCapHeight * 1000 / this.head.unitsPerEm));
        dic.put(PdfName.DESCENT, new PdfNumber(this.os_2.sTypoDescender * 1000 / this.head.unitsPerEm));
        dic.put(PdfName.FONTBBOX, new PdfRectangle(this.head.xMin * 1000 / this.head.unitsPerEm, this.head.yMin * 1000 / this.head.unitsPerEm, this.head.xMax * 1000 / this.head.unitsPerEm, this.head.yMax * 1000 / this.head.unitsPerEm));
        if (this.cff) {
            if (this.encoding.startsWith("Identity-")) {
                dic.put(PdfName.FONTNAME, new PdfName(String.valueOf(this.fontName) + "-" + this.encoding));
            } else {
                dic.put(PdfName.FONTNAME, new PdfName(String.valueOf(this.fontName) + this.style));
            }
        } else {
            dic.put(PdfName.FONTNAME, new PdfName(String.valueOf(subsetPrefix) + this.fontName + this.style));
        }
        dic.put(PdfName.ITALICANGLE, new PdfNumber(this.italicAngle));
        dic.put(PdfName.STEMV, new PdfNumber(80));
        if (fontStream != null) {
            if (this.cff) {
                dic.put(PdfName.FONTFILE3, fontStream);
            } else {
                dic.put(PdfName.FONTFILE2, fontStream);
            }
        }
        int flags = 0;
        if (this.isFixedPitch) {
            flags|=true;
        }
        flags|=this.fontSpecific ? 4 : 32;
        if ((this.head.macStyle & 2) != 0) {
            flags|=64;
        }
        if ((this.head.macStyle & 1) != 0) {
            flags|=262144;
        }
        dic.put(PdfName.FLAGS, new PdfNumber(flags));
        return dic;
    }

    protected PdfDictionary getFontBaseType(PdfIndirectReference fontDescriptor, String subsetPrefix, int firstChar, int lastChar, byte[] shortTag) throws DocumentException {
        PdfDictionary dic = new PdfDictionary(PdfName.FONT);
        if (this.cff) {
            dic.put(PdfName.SUBTYPE, PdfName.TYPE1);
            dic.put(PdfName.BASEFONT, new PdfName(String.valueOf(this.fontName) + this.style));
        } else {
            dic.put(PdfName.SUBTYPE, PdfName.TRUETYPE);
            dic.put(PdfName.BASEFONT, new PdfName(String.valueOf(subsetPrefix) + this.fontName + this.style));
        }
        dic.put(PdfName.BASEFONT, new PdfName(String.valueOf(subsetPrefix) + this.fontName + this.style));
        if (!this.fontSpecific) {
            for (int k = firstChar; k <= lastChar; ++k) {
                if (this.differences[k].equals(".notdef")) continue;
                firstChar = k;
                break;
            }
            if (this.encoding.equals("Cp1252") || this.encoding.equals("MacRoman")) {
                dic.put(PdfName.ENCODING, this.encoding.equals("Cp1252") ? PdfName.WIN_ANSI_ENCODING : PdfName.MAC_ROMAN_ENCODING);
            } else {
                PdfDictionary enc = new PdfDictionary(PdfName.ENCODING);
                PdfArray dif = new PdfArray();
                boolean gap = true;
                for (int k2 = firstChar; k2 <= lastChar; ++k2) {
                    if (shortTag[k2] != 0) {
                        if (gap) {
                            dif.add(new PdfNumber(k2));
                            gap = false;
                        }
                        dif.add(new PdfName(this.differences[k2]));
                        continue;
                    }
                    gap = true;
                }
                enc.put(PdfName.DIFFERENCES, dif);
                dic.put(PdfName.ENCODING, enc);
            }
        }
        dic.put(PdfName.FIRSTCHAR, new PdfNumber(firstChar));
        dic.put(PdfName.LASTCHAR, new PdfNumber(lastChar));
        PdfArray wd = new PdfArray();
        for (int k = firstChar; k <= lastChar; ++k) {
            if (shortTag[k] == 0) {
                wd.add(new PdfNumber(0));
                continue;
            }
            wd.add(new PdfNumber(this.widths[k]));
        }
        dic.put(PdfName.WIDTHS, wd);
        if (fontDescriptor != null) {
            dic.put(PdfName.FONTDESCRIPTOR, fontDescriptor);
        }
        return dic;
    }

    private byte[] getFullFont() throws IOException {
        RandomAccessFileOrArray rf2 = new RandomAccessFileOrArray(this.rf);
        rf2.reOpen();
        byte[] b = new byte[rf2.length()];
        rf2.readFully(b);
        rf2.close();
        return b;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    void writeFont(PdfWriter writer, PdfIndirectReference ref, Object[] params) throws DocumentException, IOException {
        firstChar = (Integer)params[0];
        lastChar = (Integer)params[1];
        shortTag = (byte[])params[2];
        if (!this.subset) {
            firstChar = 0;
            lastChar = shortTag.length - 1;
            for (k = 0; k < shortTag.length; ++k) {
                shortTag[k] = 1;
            }
        }
        ind_font = null;
        pobj = null;
        obj = null;
        subsetPrefix = "";
        if (!this.embedded) ** GOTO lbl56
        if (!this.cff) ** GOTO lbl43
        rf2 = new RandomAccessFileOrArray(this.rf);
        b = new byte[this.cffLength];
        try {
            rf2.reOpen();
            rf2.seek(this.cffOffset);
            rf2.readFully(b);
            var13_20 = null;
        }
        catch (Throwable var14_17) {
            var13_19 = null;
            try {
                rf2.close();
                throw var14_17;
            }
            catch (Exception e) {
                // empty catch block
            }
            throw var14_17;
        }
        ** try [egrp 1[TRYBLOCK] [2 : 152->160)] { 
lbl35: // 1 sources:
        rf2.close();
        ** GOTO lbl39
lbl37: // 1 sources:
        catch (Exception e) {
            // empty catch block
        }
lbl39: // 2 sources:
        pobj = new BaseFont.StreamFont(b, "Type1C");
        obj = writer.addToBody(pobj);
        ind_font = obj.getIndirectReference();
        ** GOTO lbl56
lbl43: // 1 sources:
        if (this.subset) {
            subsetPrefix = BaseFont.createSubsetPrefix();
        }
        glyphs = new HashMap<Integer, Object>();
        for (k = firstChar; k <= lastChar; ++k) {
            if (shortTag[k] == 0 || (metrics = this.fontSpecific != false ? this.getMetricsTT(k) : this.getMetricsTT(this.unicodeDifferences[k])) == null) continue;
            glyphs.put(new Integer(metrics[0]), null);
        }
        sb = new TrueTypeFontSubSet(this.fileName, new RandomAccessFileOrArray(this.rf), glyphs, this.directoryOffset, true);
        b = sb.process();
        lengths = new int[]{b.length};
        pobj = new BaseFont.StreamFont(b, lengths);
        obj = writer.addToBody(pobj);
        ind_font = obj.getIndirectReference();
lbl56: // 3 sources:
        if ((pobj = this.getFontDescriptor(ind_font, subsetPrefix)) != null) {
            obj = writer.addToBody(pobj);
            ind_font = obj.getIndirectReference();
        }
        pobj = this.getFontBaseType(ind_font, subsetPrefix, firstChar, lastChar, shortTag);
        writer.addToBody((PdfObject)pobj, ref);
    }

    public float getFontDescriptor(int key, float fontSize) {
        switch (key) {
            case 1: {
                return (float)this.os_2.sTypoAscender * fontSize / (float)this.head.unitsPerEm;
            }
            case 2: {
                return (float)this.os_2.sCapHeight * fontSize / (float)this.head.unitsPerEm;
            }
            case 3: {
                return (float)this.os_2.sTypoDescender * fontSize / (float)this.head.unitsPerEm;
            }
            case 4: {
                return (float)this.italicAngle;
            }
            case 5: {
                return fontSize * (float)this.head.xMin / (float)this.head.unitsPerEm;
            }
            case 6: {
                return fontSize * (float)this.head.yMin / (float)this.head.unitsPerEm;
            }
            case 7: {
                return fontSize * (float)this.head.xMax / (float)this.head.unitsPerEm;
            }
            case 8: {
                return fontSize * (float)this.head.yMax / (float)this.head.unitsPerEm;
            }
            case 9: {
                return fontSize * (float)this.hhea.Ascender / (float)this.head.unitsPerEm;
            }
            case 10: {
                return fontSize * (float)this.hhea.Descender / (float)this.head.unitsPerEm;
            }
            case 11: {
                return fontSize * (float)this.hhea.LineGap / (float)this.head.unitsPerEm;
            }
            case 12: {
                return fontSize * (float)this.hhea.advanceWidthMax / (float)this.head.unitsPerEm;
            }
        }
        return 0.0f;
    }

    public int[] getMetricsTT(int c) {
        if (!(this.fontSpecific || this.cmap31 == null)) {
            return (int[])this.cmap31.get(new Integer(c));
        }
        if (this.fontSpecific && this.cmap10 != null) {
            return (int[])this.cmap10.get(new Integer(c));
        }
        if (this.cmap31 != null) {
            return (int[])this.cmap31.get(new Integer(c));
        }
        if (this.cmap10 != null) {
            return (int[])this.cmap10.get(new Integer(c));
        }
        return null;
    }

    public String getPostscriptFontName() {
        return this.fontName;
    }

    public String[] getCodePagesSupported() {
        long cp = ((long)this.os_2.ulCodePageRange2 << 32) + ((long)this.os_2.ulCodePageRange1 & 0xFFFFFFFFL);
        int count = 0;
        long bit = 1;
        for (int k = 0; k < 64; ++k) {
            if ((cp & bit) != 0 && codePages[k] != null) {
                ++count;
            }
            bit<<=true;
        }
        String[] ret = new String[count];
        count = 0;
        bit = 1;
        for (int k2 = 0; k2 < 64; ++k2) {
            if ((cp & bit) != 0 && codePages[k2] != null) {
                ret[count++] = codePages[k2];
            }
            bit<<=true;
        }
        return ret;
    }

    public String[][] getFullFontName() {
        return this.fullName;
    }

    public String[][] getFamilyFontName() {
        return this.familyName;
    }

    public boolean hasKernPairs() {
        if (this.kerning.size() > 0) {
            return true;
        }
        return false;
    }

    public void setPostscriptFontName(String name) {
        this.fontName = name;
    }

    public boolean setKerning(char char1, char char2, int kern) {
        int[] metrics = this.getMetricsTT(char1);
        if (metrics == null) {
            return false;
        }
        int c1 = metrics[0];
        metrics = this.getMetricsTT(char2);
        if (metrics == null) {
            return false;
        }
        int c2 = metrics[0];
        this.kerning.put((c1 << 16) + c2, kern);
        return true;
    }

    protected int[] getRawCharBBox(int c, String name) {
        HashMap map = null;
        map = name == null || this.cmap31 == null ? this.cmap10 : this.cmap31;
        if (map == null) {
            return null;
        }
        int[] metric = (int[])map.get(new Integer(c));
        if (metric == null || this.bboxes == null) {
            return null;
        }
        return this.bboxes[metric[0]];
    }

    protected static class FontHeader {
        int flags;
        int unitsPerEm;
        short xMin;
        short yMin;
        short xMax;
        short yMax;
        int macStyle;

        protected FontHeader() {
        }
    }

    protected static class HorizontalHeader {
        short Ascender;
        short Descender;
        short LineGap;
        int advanceWidthMax;
        short minLeftSideBearing;
        short minRightSideBearing;
        short xMaxExtent;
        short caretSlopeRise;
        short caretSlopeRun;
        int numberOfHMetrics;

        protected HorizontalHeader() {
        }
    }

    protected static class WindowsMetrics {
        short xAvgCharWidth;
        int usWeightClass;
        int usWidthClass;
        short fsType;
        short ySubscriptXSize;
        short ySubscriptYSize;
        short ySubscriptXOffset;
        short ySubscriptYOffset;
        short ySuperscriptXSize;
        short ySuperscriptYSize;
        short ySuperscriptXOffset;
        short ySuperscriptYOffset;
        short yStrikeoutSize;
        short yStrikeoutPosition;
        short sFamilyClass;
        byte[] panose = new byte[10];
        byte[] achVendID = new byte[4];
        int fsSelection;
        int usFirstCharIndex;
        int usLastCharIndex;
        short sTypoAscender;
        short sTypoDescender;
        short sTypoLineGap;
        int usWinAscent;
        int usWinDescent;
        int ulCodePageRange1;
        int ulCodePageRange2;
        int sCapHeight;

        protected WindowsMetrics() {
        }
    }

}

