/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.RandomAccessFileOrArray;
import java.io.IOException;

public class PRTokeniser {
    public static final int TK_NUMBER = 1;
    public static final int TK_STRING = 2;
    public static final int TK_NAME = 3;
    public static final int TK_COMMENT = 4;
    public static final int TK_START_ARRAY = 5;
    public static final int TK_END_ARRAY = 6;
    public static final int TK_START_DIC = 7;
    public static final int TK_END_DIC = 8;
    public static final int TK_REF = 9;
    public static final int TK_OTHER = 10;
    public static final boolean[] delims;
    static final String EMPTY = "";
    protected RandomAccessFileOrArray file;
    protected int type;
    protected String stringValue;
    protected int reference;
    protected int generation;
    protected boolean hexString;
    private static final int LINE_SEGMENT_SIZE = 256;

    static {
        boolean[] arrbl = new boolean[257];
        arrbl[0] = true;
        arrbl[1] = true;
        arrbl[10] = true;
        arrbl[11] = true;
        arrbl[13] = true;
        arrbl[14] = true;
        arrbl[33] = true;
        arrbl[38] = true;
        arrbl[41] = true;
        arrbl[42] = true;
        arrbl[48] = true;
        arrbl[61] = true;
        arrbl[63] = true;
        arrbl[92] = true;
        arrbl[94] = true;
        delims = arrbl;
    }

    public PRTokeniser(String filename) throws IOException {
        this.file = new RandomAccessFileOrArray(filename);
    }

    public PRTokeniser(byte[] pdfIn) {
        this.file = new RandomAccessFileOrArray(pdfIn);
    }

    public PRTokeniser(RandomAccessFileOrArray file) {
        this.file = file;
    }

    public void seek(int pos) throws IOException {
        this.file.seek(pos);
    }

    public int getFilePointer() throws IOException {
        return this.file.getFilePointer();
    }

    public void close() throws IOException {
        this.file.close();
    }

    public int length() throws IOException {
        return this.file.length();
    }

    public int read() throws IOException {
        return this.file.read();
    }

    public RandomAccessFileOrArray getSafeFile() {
        return new RandomAccessFileOrArray(this.file);
    }

    public RandomAccessFileOrArray getFile() {
        return this.file;
    }

    public String readString(int size) throws IOException {
        StringBuffer buf = new StringBuffer();
        while (size-- > 0) {
            int ch = this.file.read();
            if (ch == -1) break;
            buf.append((char)ch);
        }
        return buf.toString();
    }

    public static final boolean isWhitespace(int ch) {
        if (ch != 0 && ch != 9 && ch != 10 && ch != 12 && ch != 13 && ch != 32) {
            return false;
        }
        return true;
    }

    public static final boolean isDelimiter(int ch) {
        if (ch != 40 && ch != 41 && ch != 60 && ch != 62 && ch != 91 && ch != 93 && ch != 47 && ch != 37) {
            return false;
        }
        return true;
    }

    public static final boolean isDelimiterWhitespace(int ch) {
        return delims[ch + 1];
    }

    public int getTokenType() {
        return this.type;
    }

    public String getStringValue() {
        return this.stringValue;
    }

    public int getReference() {
        return this.reference;
    }

    public int getGeneration() {
        return this.generation;
    }

    public void backOnePosition(int ch) throws IOException {
        if (ch != -1) {
            this.file.pushBack((byte)ch);
        }
    }

    public void throwError(String error) throws IOException {
        throw new IOException(String.valueOf(error) + " at file pointer " + this.file.getFilePointer());
    }

    public char checkPdfHeader() throws IOException {
        this.file.setStartOffset(0);
        String str = this.readString(1024);
        int idx = str.indexOf("%PDF-1.");
        if (idx < 0) {
            throw new IOException("PDF header signature not found.");
        }
        this.file.setStartOffset(idx);
        return str.charAt(idx + 7);
    }

    public void checkFdfHeader() throws IOException {
        this.file.setStartOffset(0);
        String str = this.readString(1024);
        int idx = str.indexOf("%FDF-1.2");
        if (idx < 0) {
            throw new IOException("FDF header signature not found.");
        }
        this.file.setStartOffset(idx);
    }

    public int getStartxref() throws IOException {
        int size = Math.min(1024, this.file.length());
        int pos = this.file.length() - size;
        this.file.seek(pos);
        String str = this.readString(1024);
        int idx = str.lastIndexOf("startxref");
        if (idx < 0) {
            throw new IOException("PDF startxref not found.");
        }
        return pos + idx;
    }

    public static int getHex(int v) {
        if (v >= 48 && v <= 57) {
            return v - 48;
        }
        if (v >= 65 && v <= 70) {
            return v - 65 + 10;
        }
        if (v >= 97 && v <= 102) {
            return v - 97 + 10;
        }
        return -1;
    }

    public void nextValidToken() throws IOException {
        int level = 0;
        String n1 = null;
        String n2 = null;
        int ptr = 0;
        while (this.nextToken()) {
            if (this.type == 4) continue;
            switch (level) {
                case 0: {
                    if (this.type != 1) {
                        return;
                    }
                    ptr = this.file.getFilePointer();
                    n1 = this.stringValue;
                    ++level;
                    break;
                }
                case 1: {
                    if (this.type != 1) {
                        this.file.seek(ptr);
                        this.type = 1;
                        this.stringValue = n1;
                        return;
                    }
                    n2 = this.stringValue;
                    ++level;
                    break;
                }
                default: {
                    if (!(this.type == 10 && this.stringValue.equals("R"))) {
                        this.file.seek(ptr);
                        this.type = 1;
                        this.stringValue = n1;
                        return;
                    }
                    this.type = 9;
                    this.reference = Integer.valueOf(n1);
                    this.generation = Integer.valueOf(n2);
                    return;
                }
            }
        }
        this.throwError("Unexpected end of file");
    }

    /*
     * Exception decompiling
     */
    public boolean nextToken() throws IOException {
        // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
        // org.benf.cfr.reader.util.ConfusedCFRException: Tried to end blocks [25[DOLOOP]], but top level block is 36[SIMPLE_IF_TAKEN]
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.processEndingBlocks(Op04StructuredStatement.java:392)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.buildNestedBlocks(Op04StructuredStatement.java:444)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op03SimpleStatement.createInitialStructuredBlock(Op03SimpleStatement.java:2802)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:787)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:214)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:159)
        // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:91)
        // org.benf.cfr.reader.entities.Method.analyse(Method.java:353)
        // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:731)
        // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:663)
        // org.benf.cfr.reader.Main.doJar(Main.java:126)
        // org.benf.cfr.reader.Main.main(Main.java:178)
        throw new IllegalStateException("Decompilation failed");
    }

    public int intValue() {
        return Integer.valueOf(this.stringValue);
    }

    public boolean readLineSegment(byte[] input) throws IOException {
        int cur;
        int c = -1;
        boolean eol = false;
        int ptr = 0;
        int len = input.length;
        if (ptr < len) {
            while (PRTokeniser.isWhitespace(c = this.read())) {
            }
        }
        while (!(eol || ptr >= len)) {
            switch (c) {
                case -1: 
                case 10: {
                    eol = true;
                    break;
                }
                case 13: {
                    eol = true;
                    cur = this.getFilePointer();
                    if (this.read() == 10) break;
                    this.seek(cur);
                    break;
                }
                default: {
                    input[ptr++] = (byte)c;
                }
            }
            if (eol) break;
            if (len <= ptr) break;
            c = this.read();
        }
        if (ptr >= len) {
            eol = false;
            while (!eol) {
                c = this.read();
                switch (c) {
                    case -1: 
                    case 10: {
                        eol = true;
                        break;
                    }
                    case 13: {
                        eol = true;
                        cur = this.getFilePointer();
                        if (this.read() == 10) break;
                        this.seek(cur);
                    }
                }
            }
        }
        if (c == -1 && ptr == 0) {
            return false;
        }
        if (ptr + 2 <= len) {
            input[ptr++] = 32;
            input[ptr] = 88;
        }
        return true;
    }

    public static int[] checkObjectStart(byte[] line) {
        try {
            PRTokeniser tk = new PRTokeniser(line);
            int num = 0;
            int gen = 0;
            if (!(tk.nextToken() && tk.getTokenType() == 1)) {
                return null;
            }
            num = tk.intValue();
            if (!(tk.nextToken() && tk.getTokenType() == 1)) {
                return null;
            }
            gen = tk.intValue();
            if (!tk.nextToken()) {
                return null;
            }
            if (!tk.getStringValue().equals("obj")) {
                return null;
            }
            return new int[]{num, gen};
        }
        catch (Exception tk) {
            return null;
        }
    }

    public boolean isHexString() {
        return this.hexString;
    }
}

