/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseField;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.GrayColor;
import com.lowagie.text.pdf.PdfAnnotation;
import com.lowagie.text.pdf.PdfAppearance;
import com.lowagie.text.pdf.PdfBorderDictionary;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfDashPattern;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;

public class TextField
extends BaseField {
    private String defaultText;
    private String[] choices;
    private String[] choiceExports;
    private int choiceSelection;
    private int topFirst;

    public TextField(PdfWriter writer, Rectangle box, String fieldName) {
        super(writer, box, fieldName);
    }

    public PdfAppearance getAppearance() throws IOException, DocumentException {
        PdfAppearance app = this.getBorderAppearance();
        app.beginVariableText();
        if (this.text == null || this.text.length() == 0) {
            app.endVariableText();
            return app;
        }
        BaseFont ufont = this.getRealFont();
        boolean borderExtra = this.borderStyle == 2 || this.borderStyle == 3;
        float h = this.box.height() - this.borderWidth * 2.0f;
        float bw2 = this.borderWidth;
        if (borderExtra) {
            h-=this.borderWidth * 2.0f;
            bw2*=2.0f;
        }
        float offsetX = borderExtra ? 2.0f * this.borderWidth : this.borderWidth;
        offsetX = Math.max(offsetX, 1.0f);
        float offX = Math.min(bw2, offsetX);
        app.saveState();
        app.rectangle(offX, offX, this.box.width() - 2.0f * offX, this.box.height() - 2.0f * offX);
        app.clip();
        app.newPath();
        if (this.textColor == null) {
            app.setGrayFill(0.0f);
        } else {
            app.setColorFill(this.textColor);
        }
        app.beginText();
        if ((this.options & 4) != 0) {
            float wd;
            ArrayList breaks;
            float usize = this.fontSize;
            float width = this.box.width() - 3.0f * offsetX;
            ArrayList lines = breaks = BaseField.getHardBreaks(this.text);
            float factor = ufont.getFontDescriptor(8, 1.0f) - ufont.getFontDescriptor(6, 1.0f);
            if (usize == 0.0f) {
                usize = h / (float)breaks.size() / factor;
                if (usize > 4.0f) {
                    if (usize > 12.0f) {
                        usize = 12.0f;
                    }
                    float step = Math.max((usize - 4.0f) / 10.0f, 0.2f);
                    while (usize > 4.0f) {
                        lines = BaseField.breakLines(breaks, ufont, usize, width);
                        if ((float)lines.size() * usize * factor <= h) break;
                        usize-=step;
                    }
                }
                if (usize <= 4.0f) {
                    usize = 4.0f;
                    lines = BaseField.breakLines(breaks, ufont, usize, width);
                }
            } else {
                lines = BaseField.breakLines(breaks, ufont, usize, width);
            }
            app.setFontAndSize(ufont, usize);
            app.setLeading(usize * factor);
            float offsetY = offsetX + h - ufont.getFontDescriptor(8, usize);
            String nt = (String)lines.get(0);
            if (this.alignment == 2) {
                wd = ufont.getWidthPoint(nt, usize);
                app.moveText(this.box.width() - 2.0f * offsetX - wd, offsetY);
            } else if (this.alignment == 1) {
                nt = nt.trim();
                wd = ufont.getWidthPoint(nt, usize);
                app.moveText(this.box.width() / 2.0f - wd / 2.0f, offsetY);
            } else {
                app.moveText(2.0f * offsetX, offsetY);
            }
            app.showText(nt);
            int maxline = (int)(h / usize / factor) + 1;
            maxline = Math.min(maxline, lines.size());
            for (int k = 1; k < maxline; ++k) {
                float wd2;
                nt = (String)lines.get(k);
                if (this.alignment == 2) {
                    wd2 = ufont.getWidthPoint(nt, usize);
                    app.moveText(this.box.width() - 2.0f * offsetX - wd2 - app.getXTLM(), 0.0f);
                } else if (this.alignment == 1) {
                    nt = nt.trim();
                    wd2 = ufont.getWidthPoint(nt, usize);
                    app.moveText(this.box.width() / 2.0f - wd2 / 2.0f - app.getXTLM(), 0.0f);
                }
                app.newlineShowText(nt);
            }
        } else {
            float wd;
            float usize = this.fontSize;
            if (usize == 0.0f) {
                float maxCalculatedSize = h / (ufont.getFontDescriptor(7, 1.0f) - ufont.getFontDescriptor(6, 1.0f));
                wd = ufont.getWidthPoint(this.text, 1.0f);
                usize = wd == 0.0f ? maxCalculatedSize : (this.box.width() - 2.0f * offsetX) / wd;
                if (usize > maxCalculatedSize) {
                    usize = maxCalculatedSize;
                }
                if (usize < 4.0f) {
                    usize = 4.0f;
                }
            }
            app.setFontAndSize(ufont, usize);
            float offsetY = offX + (this.box.height() - 2.0f * offX - ufont.getFontDescriptor(1, usize)) / 2.0f;
            if (offsetY < offX) {
                offsetY = offX;
            }
            if (offsetY - offX < - ufont.getFontDescriptor(3, usize)) {
                float ny = - ufont.getFontDescriptor(3, usize) + offX;
                float dy = this.box.height() - offX - ufont.getFontDescriptor(1, usize);
                offsetY = Math.min(ny, Math.max(offsetY, dy));
            }
            if ((this.options & 256) != 0 && this.maxCharacterLength > 0) {
                int textLen = Math.min(this.maxCharacterLength, this.text.length());
                int position = 0;
                if (this.alignment == 2) {
                    position = this.maxCharacterLength - textLen;
                } else if (this.alignment == 1) {
                    position = (this.maxCharacterLength - textLen) / 2;
                }
                float step = this.box.width() / (float)this.maxCharacterLength;
                float start = step / 2.0f + (float)position * step;
                for (int k = 0; k < textLen; ++k) {
                    String c = this.text.substring(k, k + 1);
                    float wd3 = ufont.getWidthPoint(c, usize);
                    app.setTextMatrix(start - wd3 / 2.0f, offsetY);
                    app.showText(c);
                    start+=step;
                }
            } else {
                if (this.alignment == 2) {
                    wd = ufont.getWidthPoint(this.text, usize);
                    app.moveText(this.box.width() - 2.0f * offsetX - wd, offsetY);
                } else if (this.alignment == 1) {
                    wd = ufont.getWidthPoint(this.text, usize);
                    app.moveText(this.box.width() / 2.0f - wd / 2.0f, offsetY);
                } else {
                    app.moveText(2.0f * offsetX, offsetY);
                }
                app.showText(this.text);
            }
        }
        app.endText();
        app.restoreState();
        app.endVariableText();
        return app;
    }

    PdfAppearance getListAppearance() throws IOException, DocumentException {
        PdfAppearance app = this.getBorderAppearance();
        app.beginVariableText();
        if (this.choices == null || this.choices.length == 0) {
            app.endVariableText();
            return app;
        }
        int topChoice = this.choiceSelection;
        if (topChoice >= this.choices.length) {
            topChoice = this.choices.length - 1;
        }
        if (topChoice < 0) {
            topChoice = 0;
        }
        BaseFont ufont = this.getRealFont();
        float usize = this.fontSize;
        if (usize == 0.0f) {
            usize = 12.0f;
        }
        boolean borderExtra = this.borderStyle == 2 || this.borderStyle == 3;
        float h = this.box.height() - this.borderWidth * 2.0f;
        if (borderExtra) {
            h-=this.borderWidth * 2.0f;
        }
        float offsetX = borderExtra ? 2.0f * this.borderWidth : this.borderWidth;
        float leading = ufont.getFontDescriptor(8, usize) - ufont.getFontDescriptor(6, usize);
        int maxFit = (int)(h / leading) + 1;
        int first = 0;
        int last = 0;
        last = topChoice + maxFit / 2 + 1;
        first = last - maxFit;
        if (first < 0) {
            last+=first;
            first = 0;
        }
        if ((last = first + maxFit) > this.choices.length) {
            last = this.choices.length;
        }
        this.topFirst = first;
        app.saveState();
        app.rectangle(offsetX, offsetX, this.box.width() - 2.0f * offsetX, this.box.height() - 2.0f * offsetX);
        app.clip();
        app.newPath();
        Color mColor = this.textColor == null ? new GrayColor(0) : this.textColor;
        app.setColorFill(new Color(10, 36, 106));
        app.rectangle(offsetX, offsetX + h - (float)(topChoice - first + 1) * leading, this.box.width() - 2.0f * offsetX, leading);
        app.fill();
        app.beginText();
        app.setFontAndSize(ufont, usize);
        app.setLeading(leading);
        app.moveText(offsetX * 2.0f, offsetX + h - ufont.getFontDescriptor(8, usize) + leading);
        app.setColorFill(mColor);
        for (int idx = first; idx < last; ++idx) {
            if (idx == topChoice) {
                app.setGrayFill(1.0f);
                app.newlineShowText(this.choices[idx]);
                app.setColorFill(mColor);
                continue;
            }
            app.newlineShowText(this.choices[idx]);
        }
        app.endText();
        app.restoreState();
        app.endVariableText();
        return app;
    }

    public PdfFormField getTextField() throws IOException, DocumentException {
        if (this.maxCharacterLength <= 0) {
            this.options&=-257;
        }
        if ((this.options & 256) != 0) {
            this.options&=-5;
        }
        PdfFormField field = PdfFormField.createTextField(this.writer, false, false, this.maxCharacterLength);
        field.setWidget(this.box, PdfAnnotation.HIGHLIGHT_INVERT);
        switch (this.alignment) {
            case 1: {
                field.setQuadding(1);
                break;
            }
            case 2: {
                field.setQuadding(2);
            }
        }
        if (this.rotation != 0) {
            field.setMKRotation(this.rotation);
        }
        if (this.fieldName != null) {
            field.setFieldName(this.fieldName);
            field.setValueAsString(this.text);
            if (this.defaultText != null) {
                field.setDefaultValueAsString(this.defaultText);
            }
            if ((this.options & 1) != 0) {
                field.setFieldFlags(1);
            }
            if ((this.options & 2) != 0) {
                field.setFieldFlags(2);
            }
            if ((this.options & 4) != 0) {
                field.setFieldFlags(4096);
            }
            if ((this.options & 8) != 0) {
                field.setFieldFlags(8388608);
            }
            if ((this.options & 16) != 0) {
                field.setFieldFlags(8192);
            }
            if ((this.options & 32) != 0) {
                field.setFieldFlags(1048576);
            }
            if ((this.options & 64) != 0) {
                field.setFieldFlags(4194304);
            }
            if ((this.options & 256) != 0) {
                field.setFieldFlags(16777216);
            }
        }
        field.setBorderStyle(new PdfBorderDictionary(this.borderWidth, this.borderStyle, new PdfDashPattern(3.0f)));
        PdfAppearance tp = this.getAppearance();
        field.setAppearance(PdfAnnotation.APPEARANCE_NORMAL, tp);
        PdfAppearance da = (PdfAppearance)tp.getDuplicate();
        da.setFontAndSize(this.getRealFont(), this.fontSize);
        if (this.textColor == null) {
            da.setGrayFill(0.0f);
        } else {
            da.setColorFill(this.textColor);
        }
        field.setDefaultAppearanceString(da);
        if (this.borderColor != null) {
            field.setMKBorderColor(this.borderColor);
        }
        if (this.backgroundColor != null) {
            field.setMKBackgroundColor(this.backgroundColor);
        }
        switch (this.visibility) {
            case 1: {
                field.setFlags(6);
                break;
            }
            case 2: {
                break;
            }
            case 3: {
                field.setFlags(36);
                break;
            }
            default: {
                field.setFlags(4);
            }
        }
        return field;
    }

    public PdfFormField getComboField() throws IOException, DocumentException {
        return this.getChoiceField(false);
    }

    public PdfFormField getListField() throws IOException, DocumentException {
        return this.getChoiceField(true);
    }

    protected PdfFormField getChoiceField(boolean isList) throws IOException, DocumentException {
        PdfAppearance tp;
        int topChoice;
        this.options&=-261;
        String[] uchoices = this.choices;
        if (uchoices == null) {
            uchoices = new String[]{};
        }
        if ((topChoice = this.choiceSelection) >= uchoices.length) {
            topChoice = uchoices.length - 1;
        }
        this.text = "";
        if (topChoice >= 0) {
            this.text = uchoices[topChoice];
        }
        if (topChoice < 0) {
            topChoice = 0;
        }
        PdfFormField field = null;
        String[][] mix = null;
        if (this.choiceExports == null) {
            field = isList ? PdfFormField.createList(this.writer, uchoices, topChoice) : PdfFormField.createCombo(this.writer, (this.options & 128) != 0, uchoices, topChoice);
        } else {
            mix = new String[uchoices.length][2];
            for (int k = 0; k < mix.length; ++k) {
                String string = uchoices[k];
                mix[k][1] = string;
                mix[k][0] = string;
            }
            int top = Math.min(uchoices.length, this.choiceExports.length);
            for (int k2 = 0; k2 < top; ++k2) {
                if (this.choiceExports[k2] == null) continue;
                mix[k2][0] = this.choiceExports[k2];
            }
            field = isList ? PdfFormField.createList(this.writer, mix, topChoice) : PdfFormField.createCombo(this.writer, (this.options & 128) != 0, mix, topChoice);
        }
        field.setWidget(this.box, PdfAnnotation.HIGHLIGHT_INVERT);
        if (this.rotation != 0) {
            field.setMKRotation(this.rotation);
        }
        if (this.fieldName != null) {
            field.setFieldName(this.fieldName);
            if (uchoices.length > 0) {
                if (mix != null) {
                    field.setValueAsString(mix[topChoice][0]);
                    field.setDefaultValueAsString(mix[topChoice][0]);
                } else {
                    field.setValueAsString(this.text);
                    field.setDefaultValueAsString(this.text);
                }
            }
            if ((this.options & 1) != 0) {
                field.setFieldFlags(1);
            }
            if ((this.options & 2) != 0) {
                field.setFieldFlags(2);
            }
            if ((this.options & 64) != 0) {
                field.setFieldFlags(4194304);
            }
        }
        field.setBorderStyle(new PdfBorderDictionary(this.borderWidth, this.borderStyle, new PdfDashPattern(3.0f)));
        if (isList) {
            tp = this.getListAppearance();
            if (this.topFirst > 0) {
                field.put(PdfName.TI, new PdfNumber(this.topFirst));
            }
        } else {
            tp = this.getAppearance();
        }
        field.setAppearance(PdfAnnotation.APPEARANCE_NORMAL, tp);
        PdfAppearance da = (PdfAppearance)tp.getDuplicate();
        da.setFontAndSize(this.getRealFont(), this.fontSize);
        if (this.textColor == null) {
            da.setGrayFill(0.0f);
        } else {
            da.setColorFill(this.textColor);
        }
        field.setDefaultAppearanceString(da);
        if (this.borderColor != null) {
            field.setMKBorderColor(this.borderColor);
        }
        if (this.backgroundColor != null) {
            field.setMKBackgroundColor(this.backgroundColor);
        }
        switch (this.visibility) {
            case 1: {
                field.setFlags(6);
                break;
            }
            case 2: {
                break;
            }
            case 3: {
                field.setFlags(36);
                break;
            }
            default: {
                field.setFlags(4);
            }
        }
        return field;
    }

    public String getDefaultText() {
        return this.defaultText;
    }

    public void setDefaultText(String defaultText) {
        this.defaultText = defaultText;
    }

    public String[] getChoices() {
        return this.choices;
    }

    public void setChoices(String[] choices) {
        this.choices = choices;
    }

    public String[] getChoiceExports() {
        return this.choiceExports;
    }

    public void setChoiceExports(String[] choiceExports) {
        this.choiceExports = choiceExports;
    }

    public int getChoiceSelection() {
        return this.choiceSelection;
    }

    public void setChoiceSelection(int choiceSelection) {
        this.choiceSelection = choiceSelection;
    }

    int getTopFirst() {
        return this.topFirst;
    }
}

