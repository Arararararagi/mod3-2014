/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocumentException;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.GlyphList;
import com.lowagie.text.pdf.IntHashtable;
import com.lowagie.text.pdf.PRIndirectReference;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfEncodings;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class DocumentFont
extends BaseFont {
    String fontName;
    PRIndirectReference refFont;
    PdfDictionary font;
    IntHashtable uni2byte = new IntHashtable();
    float Ascender = 800.0f;
    float CapHeight = 700.0f;
    float Descender = -200.0f;
    float ItalicAngle = 0.0f;
    float llx = -50.0f;
    float lly = -200.0f;
    float urx = 100.0f;
    float ury = 900.0f;
    BaseFont cjkMirror;
    String[] cjkNames = new String[]{"HeiseiMin-W3", "HeiseiKakuGo-W5", "STSong-Light", "MHei-Medium", "MSung-Light", "HYGoThic-Medium", "HYSMyeongJo-Medium", "MSungStd-Light", "STSongStd-Light", "HYSMyeongJoStd-Medium", "KozMinPro-Regular"};
    String[] cjkEncs = new String[]{"UniJIS-UCS2-H", "UniJIS-UCS2-H", "UniGB-UCS2-H", "UniCNS-UCS2-H", "UniCNS-UCS2-H", "UniKS-UCS2-H", "UniKS-UCS2-H", "UniCNS-UCS2-H", "UniGB-UCS2-H", "UniKS-UCS2-H", "UniJIS-UCS2-H"};
    static final int[] stdEnc;

    static {
        int[] arrn = new int[256];
        arrn[32] = 32;
        arrn[33] = 33;
        arrn[34] = 34;
        arrn[35] = 35;
        arrn[36] = 36;
        arrn[37] = 37;
        arrn[38] = 38;
        arrn[39] = 8217;
        arrn[40] = 40;
        arrn[41] = 41;
        arrn[42] = 42;
        arrn[43] = 43;
        arrn[44] = 44;
        arrn[45] = 45;
        arrn[46] = 46;
        arrn[47] = 47;
        arrn[48] = 48;
        arrn[49] = 49;
        arrn[50] = 50;
        arrn[51] = 51;
        arrn[52] = 52;
        arrn[53] = 53;
        arrn[54] = 54;
        arrn[55] = 55;
        arrn[56] = 56;
        arrn[57] = 57;
        arrn[58] = 58;
        arrn[59] = 59;
        arrn[60] = 60;
        arrn[61] = 61;
        arrn[62] = 62;
        arrn[63] = 63;
        arrn[64] = 64;
        arrn[65] = 65;
        arrn[66] = 66;
        arrn[67] = 67;
        arrn[68] = 68;
        arrn[69] = 69;
        arrn[70] = 70;
        arrn[71] = 71;
        arrn[72] = 72;
        arrn[73] = 73;
        arrn[74] = 74;
        arrn[75] = 75;
        arrn[76] = 76;
        arrn[77] = 77;
        arrn[78] = 78;
        arrn[79] = 79;
        arrn[80] = 80;
        arrn[81] = 81;
        arrn[82] = 82;
        arrn[83] = 83;
        arrn[84] = 84;
        arrn[85] = 85;
        arrn[86] = 86;
        arrn[87] = 87;
        arrn[88] = 88;
        arrn[89] = 89;
        arrn[90] = 90;
        arrn[91] = 91;
        arrn[92] = 92;
        arrn[93] = 93;
        arrn[94] = 94;
        arrn[95] = 95;
        arrn[96] = 8216;
        arrn[97] = 97;
        arrn[98] = 98;
        arrn[99] = 99;
        arrn[100] = 100;
        arrn[101] = 101;
        arrn[102] = 102;
        arrn[103] = 103;
        arrn[104] = 104;
        arrn[105] = 105;
        arrn[106] = 106;
        arrn[107] = 107;
        arrn[108] = 108;
        arrn[109] = 109;
        arrn[110] = 110;
        arrn[111] = 111;
        arrn[112] = 112;
        arrn[113] = 113;
        arrn[114] = 114;
        arrn[115] = 115;
        arrn[116] = 116;
        arrn[117] = 117;
        arrn[118] = 118;
        arrn[119] = 119;
        arrn[120] = 120;
        arrn[121] = 121;
        arrn[122] = 122;
        arrn[123] = 123;
        arrn[124] = 124;
        arrn[125] = 125;
        arrn[126] = 126;
        arrn[161] = 161;
        arrn[162] = 162;
        arrn[163] = 163;
        arrn[164] = 8260;
        arrn[165] = 165;
        arrn[166] = 402;
        arrn[167] = 167;
        arrn[168] = 164;
        arrn[169] = 39;
        arrn[170] = 8220;
        arrn[171] = 171;
        arrn[172] = 8249;
        arrn[173] = 8250;
        arrn[174] = 64257;
        arrn[175] = 64258;
        arrn[177] = 8211;
        arrn[178] = 8224;
        arrn[179] = 8225;
        arrn[180] = 183;
        arrn[182] = 182;
        arrn[183] = 8226;
        arrn[184] = 8218;
        arrn[185] = 8222;
        arrn[186] = 8221;
        arrn[187] = 187;
        arrn[188] = 8230;
        arrn[189] = 8240;
        arrn[191] = 191;
        arrn[193] = 96;
        arrn[194] = 180;
        arrn[195] = 710;
        arrn[196] = 732;
        arrn[197] = 175;
        arrn[198] = 728;
        arrn[199] = 729;
        arrn[200] = 168;
        arrn[202] = 730;
        arrn[203] = 184;
        arrn[205] = 733;
        arrn[206] = 731;
        arrn[207] = 711;
        arrn[208] = 8212;
        arrn[225] = 198;
        arrn[227] = 170;
        arrn[232] = 321;
        arrn[233] = 216;
        arrn[234] = 338;
        arrn[235] = 186;
        arrn[241] = 230;
        arrn[245] = 305;
        arrn[248] = 322;
        arrn[249] = 248;
        arrn[250] = 339;
        arrn[251] = 223;
        stdEnc = arrn;
    }

    DocumentFont(PRIndirectReference refFont) {
        this.encoding = "";
        this.fontSpecific = false;
        this.refFont = refFont;
        this.fontType = 4;
        this.font = (PdfDictionary)PdfReader.getPdfObject(refFont);
        this.fontName = PdfName.decodeName(((PdfName)PdfReader.getPdfObject(this.font.get(PdfName.BASEFONT))).toString());
        PdfName subType = (PdfName)PdfReader.getPdfObject(this.font.get(PdfName.SUBTYPE));
        if (PdfName.TYPE1.equals(subType) || PdfName.TRUETYPE.equals(subType)) {
            this.doType1TT();
        } else {
            for (int k = 0; k < this.cjkNames.length; ++k) {
                if (!this.fontName.startsWith(this.cjkNames[k])) continue;
                this.fontName = this.cjkNames[k];
                try {
                    this.cjkMirror = BaseFont.createFont(this.fontName, this.cjkEncs[k], false);
                }
                catch (Exception e) {
                    throw new ExceptionConverter(e);
                }
                return;
            }
        }
    }

    public void doType1TT() {
        PdfObject enc = PdfReader.getPdfObject(this.font.get(PdfName.ENCODING));
        if (enc == null) {
            this.fillEncoding(null);
        } else if (enc.isName()) {
            this.fillEncoding((PdfName)enc);
        } else {
            PdfDictionary encDic = (PdfDictionary)enc;
            enc = PdfReader.getPdfObject(encDic.get(PdfName.BASEENCODING));
            if (enc == null) {
                this.fillEncoding(null);
            } else {
                this.fillEncoding((PdfName)enc);
            }
            PdfArray diffs = (PdfArray)PdfReader.getPdfObject(encDic.get(PdfName.DIFFERENCES));
            if (diffs != null) {
                ArrayList dif = diffs.getArrayList();
                int currentNumber = 0;
                for (int k = 0; k < dif.size(); ++k) {
                    PdfObject obj = (PdfObject)dif.get(k);
                    if (obj.isNumber()) {
                        currentNumber = ((PdfNumber)obj).intValue();
                        continue;
                    }
                    int[] c = GlyphList.nameToUnicode(PdfName.decodeName(((PdfName)obj).toString()));
                    if (c != null && c.length > 0) {
                        this.uni2byte.put(c[0], currentNumber);
                    }
                    ++currentNumber;
                }
            }
        }
        PdfArray newWidths = (PdfArray)PdfReader.getPdfObject(this.font.get(PdfName.WIDTHS));
        PdfNumber first = (PdfNumber)PdfReader.getPdfObject(this.font.get(PdfName.FIRSTCHAR));
        PdfNumber last = (PdfNumber)PdfReader.getPdfObject(this.font.get(PdfName.LASTCHAR));
        if (BaseFont.BuiltinFonts14.containsKey(this.fontName)) {
            BaseFont bf;
            try {
                bf = BaseFont.createFont(this.fontName, "Cp1252", false);
            }
            catch (Exception e) {
                throw new ExceptionConverter(e);
            }
            int[] e = this.uni2byte.toOrderedKeys();
            for (int k = 0; k < e.length; ++k) {
                int n = this.uni2byte.get(e[k]);
                this.widths[n] = bf.getRawWidth(n, GlyphList.unicodeToName(e[k]));
            }
            this.Ascender = bf.getFontDescriptor(1, 1000.0f);
            this.CapHeight = bf.getFontDescriptor(2, 1000.0f);
            this.Descender = bf.getFontDescriptor(3, 1000.0f);
            this.ItalicAngle = bf.getFontDescriptor(4, 1000.0f);
            this.llx = bf.getFontDescriptor(5, 1000.0f);
            this.lly = bf.getFontDescriptor(6, 1000.0f);
            this.urx = bf.getFontDescriptor(7, 1000.0f);
            this.ury = bf.getFontDescriptor(8, 1000.0f);
        }
        if (first != null && last != null && newWidths != null) {
            int f = first.intValue();
            ArrayList ar = newWidths.getArrayList();
            for (int k = 0; k < ar.size(); ++k) {
                this.widths[f + k] = ((PdfNumber)ar.get(k)).intValue();
            }
        }
        this.fillFontDesc();
    }

    void fillFontDesc() {
        PdfArray bbox;
        PdfDictionary fontDesc = (PdfDictionary)PdfReader.getPdfObject(this.font.get(PdfName.FONTDESCRIPTOR));
        if (fontDesc == null) {
            return;
        }
        PdfNumber v = (PdfNumber)PdfReader.getPdfObject(fontDesc.get(PdfName.ASCENT));
        if (v != null) {
            this.Ascender = v.floatValue();
        }
        if ((v = (PdfNumber)PdfReader.getPdfObject(fontDesc.get(PdfName.CAPHEIGHT))) != null) {
            this.CapHeight = v.floatValue();
        }
        if ((v = (PdfNumber)PdfReader.getPdfObject(fontDesc.get(PdfName.DESCENT))) != null) {
            this.Descender = v.floatValue();
        }
        if ((v = (PdfNumber)PdfReader.getPdfObject(fontDesc.get(PdfName.ITALICANGLE))) != null) {
            this.ItalicAngle = v.floatValue();
        }
        if ((bbox = (PdfArray)PdfReader.getPdfObject(fontDesc.get(PdfName.FONTBBOX))) != null) {
            float t;
            ArrayList ar = bbox.getArrayList();
            this.llx = ((PdfNumber)ar.get(0)).floatValue();
            this.lly = ((PdfNumber)ar.get(1)).floatValue();
            this.urx = ((PdfNumber)ar.get(2)).floatValue();
            this.ury = ((PdfNumber)ar.get(3)).floatValue();
            if (this.llx > this.urx) {
                t = this.llx;
                this.llx = this.urx;
                this.urx = t;
            }
            if (this.lly > this.ury) {
                t = this.lly;
                this.lly = this.ury;
                this.ury = t;
            }
        }
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    void fillEncoding(PdfName encoding) {
        if (PdfName.MAC_ROMAN_ENCODING.equals(encoding) || PdfName.WIN_ANSI_ENCODING.equals(encoding)) {
            byte[] b = new byte[256];
            for (int k = 0; k < 256; ++k) {
                b[k] = (byte)k;
            }
            String enc = "Cp1252";
            if (PdfName.MAC_ROMAN_ENCODING.equals(encoding)) {
                enc = "MacRoman";
            }
            String cv = PdfEncodings.convertToString(b, enc);
            char[] arr = cv.toCharArray();
            for (int k2 = 0; k2 < 256; ++k2) {
                this.uni2byte.put(arr[k2], k2);
            }
            return;
        } else {
            for (int k = 0; k < 256; ++k) {
                this.uni2byte.put(stdEnc[k], k);
            }
        }
    }

    public String[][] getFamilyFontName() {
        return null;
    }

    public float getFontDescriptor(int key, float fontSize) {
        if (this.cjkMirror != null) {
            return this.cjkMirror.getFontDescriptor(key, fontSize);
        }
        switch (key) {
            case 1: 
            case 9: {
                return this.Ascender * fontSize / 1000.0f;
            }
            case 2: {
                return this.CapHeight * fontSize / 1000.0f;
            }
            case 3: 
            case 10: {
                return this.Descender * fontSize / 1000.0f;
            }
            case 4: {
                return this.ItalicAngle;
            }
            case 5: {
                return this.llx * fontSize / 1000.0f;
            }
            case 6: {
                return this.lly * fontSize / 1000.0f;
            }
            case 7: {
                return this.urx * fontSize / 1000.0f;
            }
            case 8: {
                return this.ury * fontSize / 1000.0f;
            }
            case 11: {
                return 0.0f;
            }
            case 12: {
                return (this.urx - this.llx) * fontSize / 1000.0f;
            }
        }
        return 0.0f;
    }

    public String[][] getFullFontName() {
        return null;
    }

    public int getKerning(char char1, char char2) {
        return 0;
    }

    public String getPostscriptFontName() {
        return this.fontName;
    }

    int getRawWidth(int c, String name) {
        return 0;
    }

    public boolean hasKernPairs() {
        return false;
    }

    void writeFont(PdfWriter writer, PdfIndirectReference ref, Object[] params) throws DocumentException, IOException {
    }

    public int getWidth(String text) {
        if (this.cjkMirror != null) {
            return this.cjkMirror.getWidth(text);
        }
        return super.getWidth(text);
    }

    byte[] convertToBytes(String text) {
        if (this.cjkMirror != null) {
            return PdfEncodings.convertToBytes(text, "UnicodeBigUnmarked");
        }
        char[] cc = text.toCharArray();
        byte[] b = new byte[cc.length];
        for (int k = 0; k < cc.length; ++k) {
            b[k] = (byte)this.uni2byte.get(cc[k]);
        }
        return b;
    }

    PdfIndirectReference getIndirectReference() {
        return this.refFont;
    }

    public boolean charExists(char c) {
        if (this.cjkMirror != null) {
            return this.cjkMirror.charExists(c);
        }
        return super.charExists(c);
    }

    public void setPostscriptFontName(String name) {
    }

    public boolean setKerning(char char1, char char2, int kern) {
        return false;
    }

    public int[] getCharBBox(char c) {
        return null;
    }

    protected int[] getRawCharBBox(int c, String name) {
        return null;
    }
}

