/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocListener;
import com.lowagie.text.DocWriter;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Image;
import com.lowagie.text.ImgPostscript;
import com.lowagie.text.ImgWMF;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.ByteBuffer;
import com.lowagie.text.pdf.ColorDetails;
import com.lowagie.text.pdf.DocumentFont;
import com.lowagie.text.pdf.ExtendedColor;
import com.lowagie.text.pdf.FontDetails;
import com.lowagie.text.pdf.OutputStreamCounter;
import com.lowagie.text.pdf.PRIndirectReference;
import com.lowagie.text.pdf.PRStream;
import com.lowagie.text.pdf.PatternColor;
import com.lowagie.text.pdf.PdfAcroForm;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfAnnotation;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfContents;
import com.lowagie.text.pdf.PdfDestination;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfDocument;
import com.lowagie.text.pdf.PdfEncryption;
import com.lowagie.text.pdf.PdfException;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfGState;
import com.lowagie.text.pdf.PdfICCBased;
import com.lowagie.text.pdf.PdfImage;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfIndirectObject;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfLayer;
import com.lowagie.text.pdf.PdfLayerMembership;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfOCG;
import com.lowagie.text.pdf.PdfOCProperties;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfOutline;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPage;
import com.lowagie.text.pdf.PdfPageEvent;
import com.lowagie.text.pdf.PdfPageLabels;
import com.lowagie.text.pdf.PdfPages;
import com.lowagie.text.pdf.PdfPattern;
import com.lowagie.text.pdf.PdfPatternPainter;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfReaderInstance;
import com.lowagie.text.pdf.PdfShading;
import com.lowagie.text.pdf.PdfShadingPattern;
import com.lowagie.text.pdf.PdfSpotColor;
import com.lowagie.text.pdf.PdfStream;
import com.lowagie.text.pdf.PdfString;
import com.lowagie.text.pdf.PdfTable;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfTransition;
import com.lowagie.text.pdf.PdfXConformanceException;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.ShadingColor;
import com.lowagie.text.pdf.SpotColor;
import java.awt.Color;
import java.awt.color.ICC_Profile;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class PdfWriter
extends DocWriter {
    public static final int PageLayoutSinglePage = 1;
    public static final int PageLayoutOneColumn = 2;
    public static final int PageLayoutTwoColumnLeft = 4;
    public static final int PageLayoutTwoColumnRight = 8;
    public static final int PageModeUseNone = 16;
    public static final int PageModeUseOutlines = 32;
    public static final int PageModeUseThumbs = 64;
    public static final int PageModeFullScreen = 128;
    public static final int PageModeUseOC = 1048576;
    public static final int HideToolbar = 256;
    public static final int HideMenubar = 512;
    public static final int HideWindowUI = 1024;
    public static final int FitWindow = 2048;
    public static final int CenterWindow = 4096;
    public static final int NonFullScreenPageModeUseNone = 8192;
    public static final int NonFullScreenPageModeUseOutlines = 16384;
    public static final int NonFullScreenPageModeUseThumbs = 32768;
    public static final int NonFullScreenPageModeUseOC = 524288;
    public static final int DirectionL2R = 65536;
    public static final int DirectionR2L = 131072;
    public static final int DisplayDocTitle = 262144;
    public static final int PrintScalingNone = 1048576;
    static final int ViewerPreferencesMask = 16776960;
    public static final int AllowPrinting = 2052;
    public static final int AllowModifyContents = 8;
    public static final int AllowCopy = 16;
    public static final int AllowModifyAnnotations = 32;
    public static final int AllowFillIn = 256;
    public static final int AllowScreenReaders = 512;
    public static final int AllowAssembly = 1024;
    public static final int AllowDegradedPrinting = 4;
    public static final boolean STRENGTH40BITS = false;
    public static final boolean STRENGTH128BITS = true;
    public static final PdfName DOCUMENT_CLOSE = PdfName.WC;
    public static final PdfName WILL_SAVE = PdfName.WS;
    public static final PdfName DID_SAVE = PdfName.DS;
    public static final PdfName WILL_PRINT = PdfName.WP;
    public static final PdfName DID_PRINT = PdfName.DP;
    public static final PdfName PAGE_OPEN = PdfName.O;
    public static final PdfName PAGE_CLOSE = PdfName.C;
    public static final int SIGNATURE_EXISTS = 1;
    public static final int SIGNATURE_APPEND_ONLY = 2;
    public static final char VERSION_1_2 = '2';
    public static final char VERSION_1_3 = '3';
    public static final char VERSION_1_4 = '4';
    public static final char VERSION_1_5 = '5';
    public static final char VERSION_1_6 = '6';
    private static final int VPOINT = 7;
    protected byte[] HEADER = DocWriter.getISOBytes("%PDF-1.4\n%\u00e2\u00e3\u00cf\u00d3\n");
    protected int prevxref = 0;
    protected PdfPages root;
    protected PdfDictionary imageDictionary;
    private HashMap images;
    protected HashMap formXObjects;
    protected int formXObjectsCounter;
    protected int fontNumber;
    protected int colorNumber;
    protected int patternNumber;
    protected PdfContentByte directContent;
    protected PdfContentByte directContentUnder;
    protected HashMap documentFonts;
    protected HashMap documentColors;
    protected HashMap documentPatterns;
    protected HashMap documentShadings;
    protected HashMap documentShadingPatterns;
    protected ColorDetails patternColorspaceRGB;
    protected ColorDetails patternColorspaceGRAY;
    protected ColorDetails patternColorspaceCMYK;
    protected HashMap documentSpotPatterns;
    protected HashMap documentExtGState;
    protected HashMap documentLayers;
    protected HashSet documentOCG;
    protected ArrayList documentOCGorder;
    protected PdfOCProperties OCProperties;
    protected PdfArray OCGRadioGroup;
    protected PdfDictionary defaultColorspace;
    public static final int PDFXNONE = 0;
    public static final int PDFX1A2001 = 1;
    public static final int PDFX32002 = 2;
    private int pdfxConformance;
    static final int PDFXKEY_COLOR = 1;
    static final int PDFXKEY_CMYK = 2;
    static final int PDFXKEY_RGB = 3;
    static final int PDFXKEY_FONT = 4;
    static final int PDFXKEY_IMAGE = 5;
    static final int PDFXKEY_GSTATE = 6;
    static final int PDFXKEY_LAYER = 7;
    protected PdfBody body;
    protected PdfDocument pdf;
    private PdfPageEvent pageEvent;
    protected PdfEncryption crypto;
    protected HashMap importedPages;
    protected PdfReaderInstance currentPdfReaderInstance;
    protected ArrayList pageReferences;
    protected int currentPageNumber;
    protected PdfDictionary group;
    public static final float SPACE_CHAR_RATIO_DEFAULT = 2.5f;
    public static final float NO_SPACE_CHAR_RATIO = 1.0E7f;
    public static final int RUN_DIRECTION_DEFAULT = 0;
    public static final int RUN_DIRECTION_NO_BIDI = 1;
    public static final int RUN_DIRECTION_LTR = 2;
    public static final int RUN_DIRECTION_RTL = 3;
    protected int runDirection;
    private float spaceCharRatio;
    private PdfDictionary extraCatalog;
    protected boolean fullCompression;

    protected PdfWriter() {
        this.root = new PdfPages(this);
        this.imageDictionary = new PdfDictionary();
        this.images = new HashMap();
        this.formXObjects = new HashMap();
        this.formXObjectsCounter = 1;
        this.fontNumber = 1;
        this.colorNumber = 1;
        this.patternNumber = 1;
        this.documentFonts = new HashMap();
        this.documentColors = new HashMap();
        this.documentPatterns = new HashMap();
        this.documentShadings = new HashMap();
        this.documentShadingPatterns = new HashMap();
        this.documentSpotPatterns = new HashMap();
        this.documentExtGState = new HashMap();
        this.documentLayers = new HashMap();
        this.documentOCG = new HashSet();
        this.documentOCGorder = new ArrayList();
        this.OCGRadioGroup = new PdfArray();
        this.defaultColorspace = new PdfDictionary();
        this.pdfxConformance = 0;
        this.importedPages = new HashMap();
        this.pageReferences = new ArrayList();
        this.currentPageNumber = 1;
        this.runDirection = 1;
        this.spaceCharRatio = 2.5f;
        this.fullCompression = false;
    }

    protected PdfWriter(PdfDocument document, OutputStream os) {
        super(document, os);
        this.root = new PdfPages(this);
        this.imageDictionary = new PdfDictionary();
        this.images = new HashMap();
        this.formXObjects = new HashMap();
        this.formXObjectsCounter = 1;
        this.fontNumber = 1;
        this.colorNumber = 1;
        this.patternNumber = 1;
        this.documentFonts = new HashMap();
        this.documentColors = new HashMap();
        this.documentPatterns = new HashMap();
        this.documentShadings = new HashMap();
        this.documentShadingPatterns = new HashMap();
        this.documentSpotPatterns = new HashMap();
        this.documentExtGState = new HashMap();
        this.documentLayers = new HashMap();
        this.documentOCG = new HashSet();
        this.documentOCGorder = new ArrayList();
        this.OCGRadioGroup = new PdfArray();
        this.defaultColorspace = new PdfDictionary();
        this.pdfxConformance = 0;
        this.importedPages = new HashMap();
        this.pageReferences = new ArrayList();
        this.currentPageNumber = 1;
        this.runDirection = 1;
        this.spaceCharRatio = 2.5f;
        this.fullCompression = false;
        this.pdf = document;
        this.directContent = new PdfContentByte(this);
        this.directContentUnder = new PdfContentByte(this);
    }

    public static PdfWriter getInstance(Document document, OutputStream os) throws DocumentException {
        PdfDocument pdf = new PdfDocument();
        document.addDocListener(pdf);
        PdfWriter writer = new PdfWriter(pdf, os);
        pdf.addWriter(writer);
        return writer;
    }

    public static PdfWriter getInstance(Document document, OutputStream os, DocListener listener) throws DocumentException {
        PdfDocument pdf = new PdfDocument();
        pdf.addDocListener(listener);
        document.addDocListener(pdf);
        PdfWriter writer = new PdfWriter(pdf, os);
        pdf.addWriter(writer);
        return writer;
    }

    PdfIndirectReference add(PdfPage page, PdfContents contents) throws PdfException {
        PdfIndirectObject object;
        if (!this.open) {
            throw new PdfException("The document isn't open.");
        }
        try {
            object = this.addToBody(contents);
        }
        catch (IOException ioe) {
            throw new ExceptionConverter(ioe);
        }
        page.add(object.getIndirectReference());
        if (this.group != null) {
            page.put(PdfName.GROUP, this.group);
            this.group = null;
        }
        this.root.addPage(page);
        ++this.currentPageNumber;
        return null;
    }

    PdfName addDirectImageSimple(Image image) throws PdfException, DocumentException {
        PdfName name;
        if (this.images.containsKey(image.getMySerialId())) {
            name = (PdfName)this.images.get(image.getMySerialId());
        } else {
            if (image.isImgTemplate()) {
                name = new PdfName("img" + this.images.size());
                if (image.templateData() == null) {
                    if (image instanceof ImgWMF) {
                        try {
                            ImgWMF wmf = (ImgWMF)image;
                            wmf.readWMF(this.getDirectContent().createTemplate(0.0f, 0.0f));
                        }
                        catch (Exception e) {
                            throw new DocumentException(e);
                        }
                    } else {
                        try {
                            ((ImgPostscript)image).readPostscript(this.getDirectContent().createTemplate(0.0f, 0.0f));
                        }
                        catch (Exception e) {
                            throw new DocumentException(e);
                        }
                    }
                }
            } else {
                Image maskImage = image.getImageMask();
                PdfIndirectReference maskRef = null;
                if (maskImage != null) {
                    PdfName mname = (PdfName)this.images.get(maskImage.getMySerialId());
                    maskRef = this.getImageReference(mname);
                }
                PdfImage i = new PdfImage(image, "img" + this.images.size(), maskRef);
                if (image.hasICCProfile()) {
                    PdfICCBased icc = new PdfICCBased(image.getICCProfile());
                    PdfIndirectReference iccRef = this.add(icc);
                    PdfArray iccArray = new PdfArray();
                    iccArray.add(PdfName.ICCBASED);
                    iccArray.add(iccRef);
                    PdfObject colorspace = i.get(PdfName.COLORSPACE);
                    if (colorspace != null && colorspace.isArray()) {
                        ArrayList ar = ((PdfArray)colorspace).getArrayList();
                        if (ar.size() > 1 && PdfName.INDEXED.equals(ar.get(0))) {
                            ar.set(1, iccArray);
                        } else {
                            i.put(PdfName.COLORSPACE, iccArray);
                        }
                    } else {
                        i.put(PdfName.COLORSPACE, iccArray);
                    }
                }
                this.add(i);
                name = i.name();
            }
            this.images.put(image.getMySerialId(), name);
        }
        return name;
    }

    PdfIndirectReference add(PdfImage pdfImage) throws PdfException {
        if (!this.imageDictionary.contains(pdfImage.name())) {
            PdfIndirectObject object;
            PdfWriter.checkPDFXConformance(this, 5, pdfImage);
            try {
                object = this.addToBody(pdfImage);
            }
            catch (IOException ioe) {
                throw new ExceptionConverter(ioe);
            }
            this.imageDictionary.put(pdfImage.name(), object.getIndirectReference());
            return object.getIndirectReference();
        }
        return (PdfIndirectReference)this.imageDictionary.get(pdfImage.name());
    }

    protected PdfIndirectReference add(PdfICCBased icc) throws PdfException {
        PdfIndirectObject object;
        try {
            object = this.addToBody(icc);
        }
        catch (IOException ioe) {
            throw new ExceptionConverter(ioe);
        }
        return object.getIndirectReference();
    }

    PdfIndirectReference getImageReference(PdfName name) {
        return (PdfIndirectReference)this.imageDictionary.get(name);
    }

    public void open() {
        super.open();
        try {
            this.os.write(this.HEADER);
            this.body = new PdfBody(this);
            if (this.pdfxConformance == 2) {
                PdfDictionary sec = new PdfDictionary();
                sec.put(PdfName.GAMMA, new PdfArray(new float[]{2.2f, 2.2f, 2.2f}));
                sec.put(PdfName.MATRIX, new PdfArray(new float[]{0.4124f, 0.2126f, 0.0193f, 0.3576f, 0.7152f, 0.1192f, 0.1805f, 0.0722f, 0.9505f}));
                sec.put(PdfName.WHITEPOINT, new PdfArray(new float[]{0.9505f, 1.0f, 1.089f}));
                PdfArray arr = new PdfArray(PdfName.CALRGB);
                arr.add(sec);
                this.setDefaultColorspace(PdfName.DEFAULTRGB, this.addToBody(arr).getIndirectReference());
            }
        }
        catch (IOException ioe) {
            throw new ExceptionConverter(ioe);
        }
    }

    private static void getOCGOrder(PdfArray order, PdfLayer layer) {
        ArrayList children;
        if (!layer.isOnPanel()) {
            return;
        }
        if (layer.getTitle() == null) {
            order.add(layer.getRef());
        }
        if ((children = layer.getChildren()) == null) {
            return;
        }
        PdfArray kids = new PdfArray();
        if (layer.getTitle() != null) {
            kids.add(new PdfString(layer.getTitle(), "UnicodeBig"));
        }
        for (int k = 0; k < children.size(); ++k) {
            PdfWriter.getOCGOrder(kids, (PdfLayer)children.get(k));
        }
        if (kids.size() > 0) {
            order.add(kids);
        }
    }

    private void addASEvent(PdfName event, PdfName category) {
        PdfArray arr = new PdfArray();
        Iterator it = this.documentOCG.iterator();
        while (it.hasNext()) {
            PdfLayer layer = (PdfLayer)it.next();
            PdfDictionary usage = (PdfDictionary)layer.get(PdfName.USAGE);
            if (usage == null || usage.get(category) == null) continue;
            arr.add(layer.getRef());
        }
        if (arr.size() == 0) {
            return;
        }
        PdfDictionary d = (PdfDictionary)this.OCProperties.get(PdfName.D);
        PdfArray arras = (PdfArray)d.get(PdfName.AS);
        if (arras == null) {
            arras = new PdfArray();
            d.put(PdfName.AS, arras);
        }
        PdfDictionary as = new PdfDictionary();
        as.put(PdfName.EVENT, event);
        as.put(PdfName.CATEGORY, new PdfArray(category));
        as.put(PdfName.OCGS, arr);
        arras.add(as);
    }

    private void fillOCProperties(boolean erase) {
        PdfLayer layer;
        Iterator it;
        if (this.OCProperties == null) {
            this.OCProperties = new PdfOCProperties();
        }
        if (erase) {
            this.OCProperties.remove(PdfName.OCGS);
            this.OCProperties.remove(PdfName.D);
        }
        if (this.OCProperties.get(PdfName.OCGS) == null) {
            PdfArray gr = new PdfArray();
            it = this.documentOCG.iterator();
            while (it.hasNext()) {
                layer = (PdfLayer)it.next();
                gr.add(layer.getRef());
            }
            this.OCProperties.put(PdfName.OCGS, gr);
        }
        if (this.OCProperties.get(PdfName.D) != null) {
            return;
        }
        ArrayList docOrder = new ArrayList(this.documentOCGorder);
        it = docOrder.iterator();
        while (it.hasNext()) {
            layer = (PdfLayer)it.next();
            if (layer.getParent() == null) continue;
            it.remove();
        }
        PdfArray order = new PdfArray();
        Iterator it2 = docOrder.iterator();
        while (it2.hasNext()) {
            PdfLayer layer2 = (PdfLayer)it2.next();
            PdfWriter.getOCGOrder(order, layer2);
        }
        PdfDictionary d = new PdfDictionary();
        this.OCProperties.put(PdfName.D, d);
        d.put(PdfName.ORDER, order);
        PdfArray gr = new PdfArray();
        Iterator it3 = this.documentOCG.iterator();
        while (it3.hasNext()) {
            PdfLayer layer3 = (PdfLayer)it3.next();
            if (layer3.isOn()) continue;
            gr.add(layer3.getRef());
        }
        if (gr.size() > 0) {
            d.put(PdfName.OFF, gr);
        }
        if (this.OCGRadioGroup.size() > 0) {
            d.put(PdfName.RBGROUPS, this.OCGRadioGroup);
        }
        this.addASEvent(PdfName.VIEW, PdfName.ZOOM);
        this.addASEvent(PdfName.VIEW, PdfName.VIEW);
        this.addASEvent(PdfName.PRINT, PdfName.PRINT);
        this.addASEvent(PdfName.EXPORT, PdfName.EXPORT);
        d.put(PdfName.LISTMODE, PdfName.VISIBLEPAGES);
    }

    protected PdfDictionary getCatalog(PdfIndirectReference rootObj) {
        PdfDocument.PdfCatalog catalog = ((PdfDocument)this.document).getCatalog(rootObj);
        if (this.documentOCG.size() == 0) {
            return catalog;
        }
        this.fillOCProperties(false);
        catalog.put(PdfName.OCPROPERTIES, this.OCProperties);
        return catalog;
    }

    protected void addSharedObjectsToBody() throws IOException {
        Object template;
        PdfOCG layer;
        Iterator it = this.documentFonts.values().iterator();
        while (it.hasNext()) {
            FontDetails details = (FontDetails)it.next();
            details.writeFont(this);
        }
        it = this.formXObjects.values().iterator();
        while (it.hasNext()) {
            Object[] objs = (Object[])it.next();
            template = (PdfTemplate)objs[1];
            if (template != null && template.getIndirectReference() instanceof PRIndirectReference || template == null || template.getType() != 1) continue;
            PdfIndirectObject pdfIndirectObject = this.addToBody((PdfObject)template.getFormXObject(), template.getIndirectReference());
        }
        it = this.importedPages.values().iterator();
        while (it.hasNext()) {
            this.currentPdfReaderInstance = (PdfReaderInstance)it.next();
            this.currentPdfReaderInstance.writeAllPages();
        }
        this.currentPdfReaderInstance = null;
        it = this.documentColors.values().iterator();
        while (it.hasNext()) {
            ColorDetails color = (ColorDetails)it.next();
            template = this.addToBody(color.getSpotColor(this), color.getIndirectReference());
        }
        it = this.documentPatterns.keySet().iterator();
        while (it.hasNext()) {
            PdfPatternPainter pat = (PdfPatternPainter)it.next();
            template = this.addToBody((PdfObject)pat.getPattern(), pat.getIndirectReference());
        }
        it = this.documentShadingPatterns.keySet().iterator();
        while (it.hasNext()) {
            PdfShadingPattern shadingPattern = (PdfShadingPattern)it.next();
            shadingPattern.addToBody();
        }
        it = this.documentShadings.keySet().iterator();
        while (it.hasNext()) {
            PdfShading shading = (PdfShading)it.next();
            shading.addToBody();
        }
        it = this.documentExtGState.keySet().iterator();
        while (it.hasNext()) {
            PdfDictionary gstate = (PdfDictionary)it.next();
            PdfObject[] obj = (PdfObject[])this.documentExtGState.get(gstate);
            this.addToBody((PdfObject)gstate, (PdfIndirectReference)obj[1]);
        }
        it = this.documentLayers.keySet().iterator();
        while (it.hasNext()) {
            layer = (PdfOCG)it.next();
            if (!(layer instanceof PdfLayerMembership)) continue;
            this.addToBody(layer.getPdfObject(), layer.getRef());
        }
        it = this.documentOCG.iterator();
        while (it.hasNext()) {
            layer = (PdfOCG)it.next();
            this.addToBody(layer.getPdfObject(), layer.getRef());
        }
    }

    public synchronized void close() {
        if (this.open) {
            if (this.currentPageNumber - 1 != this.pageReferences.size()) {
                throw new RuntimeException("The page " + this.pageReferences.size() + " was requested but the document has only " + (this.currentPageNumber - 1) + " pages.");
            }
            this.pdf.close();
            try {
                this.addSharedObjectsToBody();
                PdfIndirectReference rootRef = this.root.writePageTree();
                PdfDictionary catalog = this.getCatalog(rootRef);
                PdfDictionary info = this.getInfo();
                if (this.pdfxConformance != 0) {
                    if (info.get(PdfName.GTS_PDFXVERSION) == null) {
                        if (this.pdfxConformance == 1) {
                            info.put(PdfName.GTS_PDFXVERSION, new PdfString("PDF/X-1:2001"));
                            info.put(new PdfName("GTS_PDFXConformance"), new PdfString("PDF/X-1a:2001"));
                        } else if (this.pdfxConformance == 2) {
                            info.put(PdfName.GTS_PDFXVERSION, new PdfString("PDF/X-3:2002"));
                        }
                    }
                    if (info.get(PdfName.TITLE) == null) {
                        info.put(PdfName.TITLE, new PdfString("Pdf document"));
                    }
                    if (info.get(PdfName.CREATOR) == null) {
                        info.put(PdfName.CREATOR, new PdfString("Unknown"));
                    }
                    if (info.get(PdfName.TRAPPED) == null) {
                        info.put(PdfName.TRAPPED, new PdfName("False"));
                    }
                    this.getExtraCatalog();
                    if (this.extraCatalog.get(PdfName.OUTPUTINTENTS) == null) {
                        PdfDictionary out = new PdfDictionary(PdfName.OUTPUTINTENT);
                        out.put(PdfName.OUTPUTCONDITION, new PdfString("SWOP CGATS TR 001-1995"));
                        out.put(PdfName.OUTPUTCONDITIONIDENTIFIER, new PdfString("CGATS TR 001"));
                        out.put(PdfName.REGISTRYNAME, new PdfString("http://www.color.org"));
                        out.put(PdfName.INFO, new PdfString(""));
                        out.put(PdfName.S, PdfName.GTS_PDFX);
                        this.extraCatalog.put(PdfName.OUTPUTINTENTS, new PdfArray(out));
                    }
                }
                if (this.extraCatalog != null) {
                    catalog.mergeDifferent(this.extraCatalog);
                }
                PdfIndirectObject indirectCatalog = this.addToBody((PdfObject)catalog, false);
                PdfIndirectObject infoObj = this.addToBody((PdfObject)info, false);
                PdfIndirectReference encryption = null;
                PdfObject fileID = null;
                this.body.flushObjStm();
                if (this.crypto != null) {
                    PdfIndirectObject encryptionObject = this.addToBody((PdfObject)this.crypto.getEncryptionDictionary(), false);
                    encryption = encryptionObject.getIndirectReference();
                    fileID = this.crypto.getFileID();
                } else {
                    fileID = PdfEncryption.createInfoId(PdfEncryption.createDocumentId());
                }
                this.body.writeCrossReferenceTable(this.os, indirectCatalog.getIndirectReference(), infoObj.getIndirectReference(), encryption, fileID, this.prevxref);
                if (this.fullCompression) {
                    this.os.write(DocWriter.getISOBytes("startxref\n"));
                    this.os.write(DocWriter.getISOBytes(String.valueOf(this.body.offset())));
                    this.os.write(DocWriter.getISOBytes("\n%%EOF\n"));
                } else {
                    PdfTrailer trailer = new PdfTrailer(this.body.size(), this.body.offset(), indirectCatalog.getIndirectReference(), infoObj.getIndirectReference(), encryption, fileID, this.prevxref);
                    trailer.toPdf(this, this.os);
                }
                super.close();
            }
            catch (IOException ioe) {
                throw new ExceptionConverter(ioe);
            }
        }
    }

    public float getTableBottom(Table table) {
        return this.pdf.bottom(table) - this.pdf.indentBottom();
    }

    public PdfTable getPdfTable(Table table) {
        return this.pdf.getPdfTable(table, true);
    }

    public boolean breakTableIfDoesntFit(PdfTable table) throws DocumentException {
        return this.pdf.breakTableIfDoesntFit(table);
    }

    public boolean fitsPage(Table table, float margin) {
        if (this.pdf.bottom(table) > this.pdf.indentBottom() + margin) {
            return true;
        }
        return false;
    }

    public boolean fitsPage(Table table) {
        return this.fitsPage(table, 0.0f);
    }

    public boolean fitsPage(PdfPTable table, float margin) {
        return this.pdf.fitsPage(table, margin);
    }

    public boolean fitsPage(PdfPTable table) {
        return this.pdf.fitsPage(table, 0.0f);
    }

    public float getVerticalPosition(boolean ensureNewLine) {
        return this.pdf.getVerticalPosition(ensureNewLine);
    }

    boolean isPaused() {
        return this.pause;
    }

    public PdfContentByte getDirectContent() {
        if (!this.open) {
            throw new RuntimeException("The document is not open.");
        }
        return this.directContent;
    }

    public PdfContentByte getDirectContentUnder() {
        if (!this.open) {
            throw new RuntimeException("The document is not open.");
        }
        return this.directContentUnder;
    }

    void resetContent() {
        this.directContent.reset();
        this.directContentUnder.reset();
    }

    public PdfAcroForm getAcroForm() {
        return this.pdf.getAcroForm();
    }

    public PdfOutline getRootOutline() {
        return this.directContent.getRootOutline();
    }

    public OutputStreamCounter getOs() {
        return this.os;
    }

    FontDetails addSimple(BaseFont bf) {
        if (bf.getFontType() == 4) {
            return new FontDetails(new PdfName("F" + this.fontNumber++), ((DocumentFont)bf).getIndirectReference(), bf);
        }
        FontDetails ret = (FontDetails)this.documentFonts.get(bf);
        if (ret == null) {
            PdfWriter.checkPDFXConformance(this, 4, bf);
            ret = new FontDetails(new PdfName("F" + this.fontNumber++), this.body.getPdfIndirectReference(), bf);
            this.documentFonts.put(bf, ret);
        }
        return ret;
    }

    void eliminateFontSubset(PdfDictionary fonts) {
        Iterator it = this.documentFonts.values().iterator();
        while (it.hasNext()) {
            FontDetails ft = (FontDetails)it.next();
            if (fonts.get(ft.getFontName()) == null) continue;
            ft.setSubset(false);
        }
    }

    ColorDetails addSimple(PdfSpotColor spc) {
        ColorDetails ret = (ColorDetails)this.documentColors.get(spc);
        if (ret == null) {
            ret = new ColorDetails(new PdfName("CS" + this.colorNumber++), this.body.getPdfIndirectReference(), spc);
            this.documentColors.put(spc, ret);
        }
        return ret;
    }

    ColorDetails addSimplePatternColorspace(Color color) {
        int type = ExtendedColor.getType(color);
        if (type == 4 || type == 5) {
            throw new RuntimeException("An uncolored tile pattern can not have another pattern or shading as color.");
        }
        try {
            switch (type) {
                case 0: {
                    if (this.patternColorspaceRGB == null) {
                        this.patternColorspaceRGB = new ColorDetails(new PdfName("CS" + this.colorNumber++), this.body.getPdfIndirectReference(), null);
                        PdfArray array = new PdfArray(PdfName.PATTERN);
                        array.add(PdfName.DEVICERGB);
                        PdfIndirectObject pdfIndirectObject = this.addToBody((PdfObject)array, this.patternColorspaceRGB.getIndirectReference());
                    }
                    return this.patternColorspaceRGB;
                }
                case 2: {
                    if (this.patternColorspaceCMYK == null) {
                        this.patternColorspaceCMYK = new ColorDetails(new PdfName("CS" + this.colorNumber++), this.body.getPdfIndirectReference(), null);
                        PdfArray array = new PdfArray(PdfName.PATTERN);
                        array.add(PdfName.DEVICECMYK);
                        PdfIndirectObject pdfIndirectObject = this.addToBody((PdfObject)array, this.patternColorspaceCMYK.getIndirectReference());
                    }
                    return this.patternColorspaceCMYK;
                }
                case 1: {
                    if (this.patternColorspaceGRAY == null) {
                        this.patternColorspaceGRAY = new ColorDetails(new PdfName("CS" + this.colorNumber++), this.body.getPdfIndirectReference(), null);
                        PdfArray array = new PdfArray(PdfName.PATTERN);
                        array.add(PdfName.DEVICEGRAY);
                        PdfIndirectObject pdfIndirectObject = this.addToBody((PdfObject)array, this.patternColorspaceGRAY.getIndirectReference());
                    }
                    return this.patternColorspaceGRAY;
                }
                case 3: {
                    ColorDetails details = this.addSimple(((SpotColor)color).getPdfSpotColor());
                    ColorDetails patternDetails = (ColorDetails)this.documentSpotPatterns.get(details);
                    if (patternDetails == null) {
                        patternDetails = new ColorDetails(new PdfName("CS" + this.colorNumber++), this.body.getPdfIndirectReference(), null);
                        PdfArray array = new PdfArray(PdfName.PATTERN);
                        array.add(details.getIndirectReference());
                        PdfIndirectObject cobj = this.addToBody((PdfObject)array, patternDetails.getIndirectReference());
                        this.documentSpotPatterns.put(details, patternDetails);
                    }
                    return patternDetails;
                }
            }
            throw new RuntimeException("Invalid color type in PdfWriter.addSimplePatternColorspace().");
        }
        catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    void addSimpleShadingPattern(PdfShadingPattern shading) {
        if (!this.documentShadingPatterns.containsKey(shading)) {
            shading.setName(this.patternNumber);
            ++this.patternNumber;
            this.documentShadingPatterns.put(shading, null);
            this.addSimpleShading(shading.getShading());
        }
    }

    void addSimpleShading(PdfShading shading) {
        if (!this.documentShadings.containsKey(shading)) {
            this.documentShadings.put(shading, null);
            shading.setName(this.documentShadings.size());
        }
    }

    PdfObject[] addSimpleExtGState(PdfDictionary gstate) {
        if (!this.documentExtGState.containsKey(gstate)) {
            PdfWriter.checkPDFXConformance(this, 6, gstate);
            this.documentExtGState.put(gstate, new PdfObject[]{new PdfName("GS" + (this.documentExtGState.size() + 1)), this.getPdfIndirectReference()});
        }
        return (PdfObject[])this.documentExtGState.get(gstate);
    }

    void registerLayer(PdfOCG layer) {
        PdfWriter.checkPDFXConformance(this, 7, null);
        if (layer instanceof PdfLayer) {
            PdfLayer la = (PdfLayer)layer;
            if (la.getTitle() == null) {
                if (!this.documentOCG.contains(layer)) {
                    this.documentOCG.add(layer);
                    this.documentOCGorder.add(layer);
                }
            } else {
                this.documentOCGorder.add(layer);
            }
        } else {
            throw new IllegalArgumentException("Only PdfLayer is accepted.");
        }
    }

    PdfName addSimpleLayer(PdfOCG layer) {
        if (!this.documentLayers.containsKey(layer)) {
            PdfWriter.checkPDFXConformance(this, 7, null);
            this.documentLayers.put(layer, new PdfName("OC" + (this.documentLayers.size() + 1)));
        }
        return (PdfName)this.documentLayers.get(layer);
    }

    PdfDocument getPdfDocument() {
        return this.pdf;
    }

    public PdfIndirectReference getPdfIndirectReference() {
        return this.body.getPdfIndirectReference();
    }

    int getIndirectReferenceNumber() {
        return this.body.getIndirectReferenceNumber();
    }

    PdfName addSimplePattern(PdfPatternPainter painter) {
        PdfName name = (PdfName)this.documentPatterns.get(painter);
        try {
            if (name == null) {
                name = new PdfName("P" + this.patternNumber);
                ++this.patternNumber;
                this.documentPatterns.put(painter, name);
            }
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
        return name;
    }

    PdfName addDirectTemplateSimple(PdfTemplate template, PdfName forcedName) {
        PdfIndirectReference ref = template.getIndirectReference();
        Object[] obj = (Object[])this.formXObjects.get(ref);
        PdfName name = null;
        try {
            if (obj == null) {
                if (forcedName == null) {
                    name = new PdfName("Xf" + this.formXObjectsCounter);
                    ++this.formXObjectsCounter;
                } else {
                    name = forcedName;
                }
                if (template.getType() == 2) {
                    template = null;
                }
                this.formXObjects.put(ref, new Object[]{name, template});
            } else {
                name = (PdfName)obj[0];
            }
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
        return name;
    }

    public void setPageEvent(PdfPageEvent pageEvent) {
        this.pageEvent = pageEvent;
    }

    public PdfPageEvent getPageEvent() {
        return this.pageEvent;
    }

    void addLocalDestinations(TreeMap dest) throws IOException {
        Iterator i = dest.keySet().iterator();
        while (i.hasNext()) {
            String name = (String)i.next();
            Object[] obj = (Object[])dest.get(name);
            PdfDestination destination = (PdfDestination)obj[2];
            if (destination == null) {
                throw new RuntimeException("The name '" + name + "' has no local destination.");
            }
            if (obj[1] == null) {
                obj[1] = this.getPdfIndirectReference();
            }
            PdfIndirectObject pdfIndirectObject = this.addToBody((PdfObject)destination, (PdfIndirectReference)obj[1]);
        }
    }

    public int getPageNumber() {
        return this.pdf.getPageNumber();
    }

    public void setViewerPreferences(int preferences) {
        this.pdf.setViewerPreferences(preferences);
    }

    public void setEncryption(byte[] userPassword, byte[] ownerPassword, int permissions, boolean strength128Bits) throws DocumentException {
        if (this.pdf.isOpen()) {
            throw new DocumentException("Encryption can only be added before opening the document.");
        }
        this.crypto = new PdfEncryption();
        this.crypto.setupAllKeys(userPassword, ownerPassword, permissions, strength128Bits);
    }

    public void setEncryption(boolean strength, String userPassword, String ownerPassword, int permissions) throws DocumentException {
        this.setEncryption(DocWriter.getISOBytes(userPassword), DocWriter.getISOBytes(ownerPassword), permissions, strength);
    }

    public PdfIndirectObject addToBody(PdfObject object) throws IOException {
        PdfIndirectObject iobj = this.body.add(object);
        return iobj;
    }

    public PdfIndirectObject addToBody(PdfObject object, boolean inObjStm) throws IOException {
        PdfIndirectObject iobj = this.body.add(object, inObjStm);
        return iobj;
    }

    public PdfIndirectObject addToBody(PdfObject object, PdfIndirectReference ref) throws IOException {
        PdfIndirectObject iobj = this.body.add(object, ref);
        return iobj;
    }

    public PdfIndirectObject addToBody(PdfObject object, PdfIndirectReference ref, boolean inObjStm) throws IOException {
        PdfIndirectObject iobj = this.body.add(object, ref, inObjStm);
        return iobj;
    }

    public PdfIndirectObject addToBody(PdfObject object, int refNumber) throws IOException {
        PdfIndirectObject iobj = this.body.add(object, refNumber);
        return iobj;
    }

    public PdfIndirectObject addToBody(PdfObject object, int refNumber, boolean inObjStm) throws IOException {
        PdfIndirectObject iobj = this.body.add(object, refNumber, inObjStm);
        return iobj;
    }

    public void setOpenAction(String name) {
        this.pdf.setOpenAction(name);
    }

    public void setAdditionalAction(PdfName actionType, PdfAction action) throws PdfException {
        if (!(actionType.equals(DOCUMENT_CLOSE) || actionType.equals(WILL_SAVE) || actionType.equals(DID_SAVE) || actionType.equals(WILL_PRINT) || actionType.equals(DID_PRINT))) {
            throw new PdfException("Invalid additional action type: " + actionType.toString());
        }
        this.pdf.addAdditionalAction(actionType, action);
    }

    public void setOpenAction(PdfAction action) {
        this.pdf.setOpenAction(action);
    }

    public void setPageLabels(PdfPageLabels pageLabels) {
        this.pdf.setPageLabels(pageLabels);
    }

    PdfEncryption getEncryption() {
        return this.crypto;
    }

    RandomAccessFileOrArray getReaderFile(PdfReader reader) {
        return this.currentPdfReaderInstance.getReaderFile();
    }

    protected int getNewObjectNumber(PdfReader reader, int number, int generation) {
        return this.currentPdfReaderInstance.getNewObjectNumber(number, generation);
    }

    public PdfImportedPage getImportedPage(PdfReader reader, int pageNumber) {
        PdfReaderInstance inst = (PdfReaderInstance)this.importedPages.get(reader);
        if (inst == null) {
            inst = reader.getPdfReaderInstance(this);
            this.importedPages.put(reader, inst);
        }
        return inst.getImportedPage(pageNumber);
    }

    public void addJavaScript(PdfAction js) {
        this.pdf.addJavaScript(js);
    }

    public void addJavaScript(String code, boolean unicode) {
        this.addJavaScript(PdfAction.javaScript(code, this, unicode));
    }

    public void addJavaScript(String code) {
        this.addJavaScript(code, false);
    }

    public void setCropBoxSize(Rectangle crop) {
        this.pdf.setCropBoxSize(crop);
    }

    public PdfIndirectReference getPageReference(int page) {
        PdfIndirectReference ref;
        if (--page < 0) {
            throw new IndexOutOfBoundsException("The page numbers start at 1.");
        }
        if (page < this.pageReferences.size()) {
            ref = (PdfIndirectReference)this.pageReferences.get(page);
            if (ref == null) {
                ref = this.body.getPdfIndirectReference();
                this.pageReferences.set(page, ref);
            }
        } else {
            int empty = page - this.pageReferences.size();
            for (int k = 0; k < empty; ++k) {
                this.pageReferences.add(null);
            }
            ref = this.body.getPdfIndirectReference();
            this.pageReferences.add(ref);
        }
        return ref;
    }

    PdfIndirectReference getCurrentPage() {
        return this.getPageReference(this.currentPageNumber);
    }

    int getCurrentPageNumber() {
        return this.currentPageNumber;
    }

    public void addCalculationOrder(PdfFormField annot) {
        this.pdf.addCalculationOrder(annot);
    }

    public void setSigFlags(int f) {
        this.pdf.setSigFlags(f);
    }

    public void addAnnotation(PdfAnnotation annot) {
        this.pdf.addAnnotation(annot);
    }

    void addAnnotation(PdfAnnotation annot, int page) {
        this.addAnnotation(annot);
    }

    public void setPdfVersion(char version) {
        if (this.HEADER.length > 7) {
            this.HEADER[7] = (byte)version;
        }
    }

    public int reorderPages(int[] order) throws DocumentException {
        return this.root.reorderPages(order);
    }

    public float getSpaceCharRatio() {
        return this.spaceCharRatio;
    }

    public void setSpaceCharRatio(float spaceCharRatio) {
        this.spaceCharRatio = spaceCharRatio < 0.001f ? 0.001f : spaceCharRatio;
    }

    public void setRunDirection(int runDirection) {
        if (runDirection < 1 || runDirection > 3) {
            throw new RuntimeException("Invalid run direction: " + runDirection);
        }
        this.runDirection = runDirection;
    }

    public int getRunDirection() {
        return this.runDirection;
    }

    public void setDuration(int seconds) {
        this.pdf.setDuration(seconds);
    }

    public void setTransition(PdfTransition transition) {
        this.pdf.setTransition(transition);
    }

    public void freeReader(PdfReader reader) throws IOException {
        this.currentPdfReaderInstance = (PdfReaderInstance)this.importedPages.get(reader);
        if (this.currentPdfReaderInstance == null) {
            return;
        }
        this.currentPdfReaderInstance.writeAllPages();
        this.currentPdfReaderInstance = null;
        this.importedPages.remove(reader);
    }

    public void setPageAction(PdfName actionType, PdfAction action) throws PdfException {
        if (!(actionType.equals(PAGE_OPEN) || actionType.equals(PAGE_CLOSE))) {
            throw new PdfException("Invalid page additional action type: " + actionType.toString());
        }
        this.pdf.setPageAction(actionType, action);
    }

    public int getCurrentDocumentSize() {
        return this.body.offset() + this.body.size() * 20 + 72;
    }

    public boolean isStrictImageSequence() {
        return this.pdf.isStrictImageSequence();
    }

    public void setStrictImageSequence(boolean strictImageSequence) {
        this.pdf.setStrictImageSequence(strictImageSequence);
    }

    public void setPageEmpty(boolean pageEmpty) {
        this.pdf.setPageEmpty(pageEmpty);
    }

    public PdfDictionary getInfo() {
        return ((PdfDocument)this.document).getInfo();
    }

    public PdfDictionary getExtraCatalog() {
        if (this.extraCatalog == null) {
            this.extraCatalog = new PdfDictionary();
        }
        return this.extraCatalog;
    }

    public void setLinearPageMode() {
        this.root.setLinearMode(null);
    }

    public PdfDictionary getGroup() {
        return this.group;
    }

    public void setGroup(PdfDictionary group) {
        this.group = group;
    }

    public void setPDFXConformance(int pdfxConformance) {
        if (this.pdfxConformance == pdfxConformance) {
            return;
        }
        if (this.pdf.isOpen()) {
            throw new PdfXConformanceException("PDFX conformance can only be set before opening the document.");
        }
        if (this.crypto != null) {
            throw new PdfXConformanceException("A PDFX conforming document cannot be encrypted.");
        }
        if (pdfxConformance != 0) {
            this.setPdfVersion('3');
        }
        this.pdfxConformance = pdfxConformance;
    }

    public int getPDFXConformance() {
        return this.pdfxConformance;
    }

    static void checkPDFXConformance(PdfWriter writer, int key, Object obj1) {
        if (writer == null || writer.pdfxConformance == 0) {
            return;
        }
        int conf = writer.pdfxConformance;
        block0 : switch (key) {
            case 1: {
                switch (conf) {
                    case 1: {
                        if (obj1 instanceof ExtendedColor) {
                            ExtendedColor ec = (ExtendedColor)obj1;
                            switch (ec.getType()) {
                                case 1: 
                                case 2: {
                                    return;
                                }
                                case 0: {
                                    throw new PdfXConformanceException("Colorspace RGB is not allowed.");
                                }
                                case 3: {
                                    SpotColor sc = (SpotColor)ec;
                                    PdfWriter.checkPDFXConformance(writer, 1, sc.getPdfSpotColor().getAlternativeCS());
                                    break block0;
                                }
                                case 5: {
                                    ShadingColor xc = (ShadingColor)ec;
                                    PdfWriter.checkPDFXConformance(writer, 1, xc.getPdfShadingPattern().getShading().getColorSpace());
                                    break block0;
                                }
                                case 4: {
                                    PatternColor pc = (PatternColor)ec;
                                    PdfWriter.checkPDFXConformance(writer, 1, pc.getPainter().getDefaultColor());
                                }
                            }
                            break block0;
                        }
                        if (!(obj1 instanceof Color)) break;
                        throw new PdfXConformanceException("Colorspace RGB is not allowed.");
                    }
                }
                break;
            }
            case 2: {
                break;
            }
            case 3: {
                if (conf != 1) break;
                throw new PdfXConformanceException("Colorspace RGB is not allowed.");
            }
            case 4: {
                if (((BaseFont)obj1).isEmbedded()) break;
                throw new PdfXConformanceException("All the fonts must be embedded.");
            }
            case 5: {
                PdfImage image = (PdfImage)obj1;
                if (image.get(PdfName.SMASK) != null) {
                    throw new PdfXConformanceException("The /SMask key is not allowed in images.");
                }
                switch (conf) {
                    case 1: {
                        PdfObject cs = image.get(PdfName.COLORSPACE);
                        if (cs == null) {
                            return;
                        }
                        if (cs.isName()) {
                            if (!PdfName.DEVICERGB.equals(cs)) break;
                            throw new PdfXConformanceException("Colorspace RGB is not allowed.");
                        }
                        if (!cs.isArray() || !PdfName.CALRGB.equals((PdfObject)((PdfArray)cs).getArrayList().get(0))) break;
                        throw new PdfXConformanceException("Colorspace CalRGB is not allowed.");
                    }
                }
                break;
            }
            case 6: {
                PdfDictionary gs = (PdfDictionary)obj1;
                PdfObject obj = gs.get(PdfName.BM);
                if (!(obj == null || PdfGState.BM_NORMAL.equals(obj) || PdfGState.BM_COMPATIBLE.equals(obj))) {
                    throw new PdfXConformanceException("Blend mode " + obj.toString() + " not allowed.");
                }
                obj = gs.get(PdfName.CA);
                double v = 0.0;
                if (obj != null && (v = ((PdfNumber)obj).doubleValue()) != 1.0) {
                    throw new PdfXConformanceException("Transparency is not allowed: /CA = " + v);
                }
                obj = gs.get(PdfName.ca);
                v = 0.0;
                if (obj == null || (v = ((PdfNumber)obj).doubleValue()) == 1.0) break;
                throw new PdfXConformanceException("Transparency is not allowed: /ca = " + v);
            }
            case 7: {
                throw new PdfXConformanceException("Layers are not allowed.");
            }
        }
    }

    public void setOutputIntents(String outputConditionIdentifier, String outputCondition, String registryName, String info, byte[] destOutputProfile) throws IOException {
        this.getExtraCatalog();
        PdfDictionary out = new PdfDictionary(PdfName.OUTPUTINTENT);
        if (outputCondition != null) {
            out.put(PdfName.OUTPUTCONDITION, new PdfString(outputCondition, "UnicodeBig"));
        }
        if (outputConditionIdentifier != null) {
            out.put(PdfName.OUTPUTCONDITIONIDENTIFIER, new PdfString(outputConditionIdentifier, "UnicodeBig"));
        }
        if (registryName != null) {
            out.put(PdfName.REGISTRYNAME, new PdfString(registryName, "UnicodeBig"));
        }
        if (info != null) {
            out.put(PdfName.INFO, new PdfString(registryName, "UnicodeBig"));
        }
        if (destOutputProfile != null) {
            PdfStream stream = new PdfStream(destOutputProfile);
            stream.flateCompress();
            out.put(PdfName.DESTOUTPUTPROFILE, this.addToBody(stream).getIndirectReference());
        }
        out.put(PdfName.S, PdfName.GTS_PDFX);
        this.extraCatalog.put(PdfName.OUTPUTINTENTS, new PdfArray(out));
    }

    private static String getNameString(PdfDictionary dic, PdfName key) {
        PdfObject obj = PdfReader.getPdfObject(dic.get(key));
        if (!(obj != null && obj.isString())) {
            return null;
        }
        return ((PdfString)obj).toUnicodeString();
    }

    public boolean setOutputIntents(PdfReader reader, boolean checkExistence) throws IOException {
        PdfDictionary catalog = reader.getCatalog();
        PdfArray outs = (PdfArray)PdfReader.getPdfObject(catalog.get(PdfName.OUTPUTINTENTS));
        if (outs == null) {
            return false;
        }
        ArrayList arr = outs.getArrayList();
        if (arr.size() == 0) {
            return false;
        }
        PdfDictionary out = (PdfDictionary)PdfReader.getPdfObject((PdfObject)arr.get(0));
        PdfObject obj = PdfReader.getPdfObject(out.get(PdfName.S));
        if (!(obj != null && PdfName.GTS_PDFX.equals(obj))) {
            return false;
        }
        if (checkExistence) {
            return true;
        }
        PRStream stream = (PRStream)PdfReader.getPdfObject(out.get(PdfName.DESTOUTPUTPROFILE));
        byte[] destProfile = null;
        if (stream != null) {
            destProfile = PdfReader.getStreamBytes(stream);
        }
        this.setOutputIntents(PdfWriter.getNameString(out, PdfName.OUTPUTCONDITIONIDENTIFIER), PdfWriter.getNameString(out, PdfName.OUTPUTCONDITION), PdfWriter.getNameString(out, PdfName.REGISTRYNAME), PdfWriter.getNameString(out, PdfName.INFO), destProfile);
        return true;
    }

    public void setBoxSize(String boxName, Rectangle size) {
        this.pdf.setBoxSize(boxName, size);
    }

    public PdfDictionary getDefaultColorspace() {
        return this.defaultColorspace;
    }

    public void setDefaultColorspace(PdfName key, PdfObject cs) {
        if (cs == null || cs.isNull()) {
            this.defaultColorspace.remove(key);
        }
        this.defaultColorspace.put(key, cs);
    }

    public boolean isFullCompression() {
        return this.fullCompression;
    }

    public void setFullCompression() {
        this.fullCompression = true;
        this.setPdfVersion('5');
    }

    public PdfOCProperties getOCProperties() {
        this.fillOCProperties(true);
        return this.OCProperties;
    }

    public void addOCGRadioGroup(ArrayList group) {
        PdfArray ar = new PdfArray();
        for (int k = 0; k < group.size(); ++k) {
            PdfLayer layer = (PdfLayer)group.get(k);
            if (layer.getTitle() != null) continue;
            ar.add(layer.getRef());
        }
        if (ar.size() == 0) {
            return;
        }
        this.OCGRadioGroup.add(ar);
    }

    public void setThumbnail(Image image) throws PdfException, DocumentException {
        this.pdf.setThumbnail(image);
    }

    public static class PdfBody {
        private TreeSet xrefs = new TreeSet();
        private int refnum;
        private int position;
        private PdfWriter writer;
        private static final int OBJSINSTREAM = 200;
        private ByteBuffer index;
        private ByteBuffer streamObjects;
        private int currentObjNum;
        private int numObj = 0;

        PdfBody(PdfWriter writer) {
            this.xrefs.add(new PdfCrossReference(0, 0, 65535));
            this.position = writer.getOs().getCounter();
            this.refnum = 1;
            this.writer = writer;
        }

        void setRefnum(int refnum) {
            this.refnum = refnum;
        }

        private PdfCrossReference addToObjStm(PdfObject obj, int nObj) throws IOException {
            if (this.numObj >= 200) {
                this.flushObjStm();
            }
            if (this.index == null) {
                this.index = new ByteBuffer();
                this.streamObjects = new ByteBuffer();
                this.currentObjNum = this.getIndirectReferenceNumber();
                this.numObj = 0;
            }
            int p = this.streamObjects.size();
            int idx = this.numObj++;
            PdfEncryption enc = this.writer.crypto;
            this.writer.crypto = null;
            obj.toPdf(this.writer, this.streamObjects);
            this.writer.crypto = enc;
            this.streamObjects.append(' ');
            this.index.append(nObj).append(' ').append(p).append(' ');
            return new PdfCrossReference(2, nObj, this.currentObjNum, idx);
        }

        private void flushObjStm() throws IOException {
            if (this.numObj == 0) {
                return;
            }
            int first = this.index.size();
            this.index.append(this.streamObjects);
            PdfStream stream = new PdfStream(this.index.toByteArray());
            stream.flateCompress();
            stream.put(PdfName.TYPE, PdfName.OBJSTM);
            stream.put(PdfName.N, new PdfNumber(this.numObj));
            stream.put(PdfName.FIRST, new PdfNumber(first));
            this.add((PdfObject)stream, this.currentObjNum);
            this.index = null;
            this.streamObjects = null;
            this.numObj = 0;
        }

        PdfIndirectObject add(PdfObject object) throws IOException {
            return this.add(object, this.getIndirectReferenceNumber());
        }

        PdfIndirectObject add(PdfObject object, boolean inObjStm) throws IOException {
            return this.add(object, this.getIndirectReferenceNumber(), inObjStm);
        }

        PdfIndirectReference getPdfIndirectReference() {
            return new PdfIndirectReference(0, this.getIndirectReferenceNumber());
        }

        int getIndirectReferenceNumber() {
            int n = this.refnum++;
            this.xrefs.add(new PdfCrossReference(n, 0, 65536));
            return n;
        }

        PdfIndirectObject add(PdfObject object, PdfIndirectReference ref) throws IOException {
            return this.add(object, ref.getNumber());
        }

        PdfIndirectObject add(PdfObject object, PdfIndirectReference ref, boolean inObjStm) throws IOException {
            return this.add(object, ref.getNumber(), inObjStm);
        }

        PdfIndirectObject add(PdfObject object, int refNumber) throws IOException {
            return this.add(object, refNumber, true);
        }

        PdfIndirectObject add(PdfObject object, int refNumber, boolean inObjStm) throws IOException {
            if (inObjStm && object.canBeInObjStm() && this.writer.isFullCompression()) {
                PdfCrossReference pxref = this.addToObjStm(object, refNumber);
                PdfIndirectObject indirect = new PdfIndirectObject(refNumber, object, this.writer);
                if (!this.xrefs.add(pxref)) {
                    this.xrefs.remove(pxref);
                    this.xrefs.add(pxref);
                }
                return indirect;
            }
            PdfIndirectObject indirect = new PdfIndirectObject(refNumber, object, this.writer);
            PdfCrossReference pxref = new PdfCrossReference(refNumber, this.position);
            if (!this.xrefs.add(pxref)) {
                this.xrefs.remove(pxref);
                this.xrefs.add(pxref);
            }
            indirect.writeTo(this.writer.getOs());
            this.position = this.writer.getOs().getCounter();
            return indirect;
        }

        int offset() {
            return this.position;
        }

        int size() {
            return Math.max(((PdfCrossReference)this.xrefs.last()).getRefnum() + 1, this.refnum);
        }

        void writeCrossReferenceTable(OutputStream os, PdfIndirectReference root, PdfIndirectReference info, PdfIndirectReference encryption, PdfObject fileID, int prevxref) throws IOException {
            int refNumber = 0;
            if (this.writer.isFullCompression()) {
                this.flushObjStm();
                refNumber = this.getIndirectReferenceNumber();
                this.xrefs.add(new PdfCrossReference(refNumber, this.position));
            }
            PdfCrossReference entry = (PdfCrossReference)this.xrefs.first();
            int first = entry.getRefnum();
            int len = 0;
            ArrayList<Integer> sections = new ArrayList<Integer>();
            Iterator i = this.xrefs.iterator();
            while (i.hasNext()) {
                entry = (PdfCrossReference)i.next();
                if (first + len == entry.getRefnum()) {
                    ++len;
                    continue;
                }
                sections.add(new Integer(first));
                sections.add(new Integer(len));
                first = entry.getRefnum();
                len = 1;
            }
            sections.add(new Integer(first));
            sections.add(new Integer(len));
            if (this.writer.isFullCompression()) {
                int mid;
                int mask = -16777216;
                for (mid = 4; mid > 1; --mid) {
                    if ((mask & this.position) != 0) break;
                    mask>>>=8;
                }
                ByteBuffer buf = new ByteBuffer();
                Iterator i2 = this.xrefs.iterator();
                while (i2.hasNext()) {
                    entry = (PdfCrossReference)i2.next();
                    entry.toPdf(mid, buf);
                }
                PdfStream xr = new PdfStream(buf.toByteArray());
                buf = null;
                xr.flateCompress();
                xr.put(PdfName.SIZE, new PdfNumber(this.size()));
                xr.put(PdfName.ROOT, root);
                if (info != null) {
                    xr.put(PdfName.INFO, info);
                }
                if (encryption != null) {
                    xr.put(PdfName.ENCRYPT, encryption);
                }
                if (fileID != null) {
                    xr.put(PdfName.ID, fileID);
                }
                xr.put(PdfName.W, new PdfArray(new int[]{1, mid, 2}));
                xr.put(PdfName.TYPE, PdfName.XREF);
                PdfArray idx = new PdfArray();
                for (int k = 0; k < sections.size(); ++k) {
                    idx.add(new PdfNumber((Integer)sections.get(k)));
                }
                xr.put(PdfName.INDEX, idx);
                if (prevxref > 0) {
                    xr.put(PdfName.PREV, new PdfNumber(prevxref));
                }
                PdfEncryption enc = this.writer.crypto;
                this.writer.crypto = null;
                PdfIndirectObject indirect = new PdfIndirectObject(refNumber, (PdfObject)xr, this.writer);
                indirect.writeTo(this.writer.getOs());
                this.writer.crypto = enc;
            } else {
                os.write(DocWriter.getISOBytes("xref\n"));
                i = this.xrefs.iterator();
                for (int k = 0; k < sections.size(); k+=2) {
                    first = (Integer)sections.get(k);
                    len = (Integer)sections.get(k + 1);
                    os.write(DocWriter.getISOBytes(String.valueOf(first)));
                    os.write(DocWriter.getISOBytes(" "));
                    os.write(DocWriter.getISOBytes(String.valueOf(len)));
                    os.write(10);
                    while (len-- > 0) {
                        entry = (PdfCrossReference)i.next();
                        entry.toPdf(os);
                    }
                }
            }
        }

        static class PdfCrossReference
        implements Comparable {
            private int type;
            private int offset;
            private int refnum;
            private int generation;

            PdfCrossReference(int refnum, int offset, int generation) {
                this.type = 0;
                this.offset = offset;
                this.refnum = refnum;
                this.generation = generation;
            }

            PdfCrossReference(int refnum, int offset) {
                this.type = 1;
                this.offset = offset;
                this.refnum = refnum;
                this.generation = 0;
            }

            PdfCrossReference(int type, int refnum, int offset, int generation) {
                this.type = type;
                this.offset = offset;
                this.refnum = refnum;
                this.generation = generation;
            }

            int getRefnum() {
                return this.refnum;
            }

            public void toPdf(OutputStream os) throws IOException {
                String s = "0000000000" + this.offset;
                StringBuffer off = new StringBuffer(s.substring(s.length() - 10));
                s = "00000" + this.generation;
                String gen = s.substring(s.length() - 5);
                if (this.generation == 65535) {
                    os.write(DocWriter.getISOBytes(off.append(' ').append(gen).append(" f \n").toString()));
                } else {
                    os.write(DocWriter.getISOBytes(off.append(' ').append(gen).append(" n \n").toString()));
                }
            }

            public void toPdf(int midSize, OutputStream os) throws IOException {
                os.write((byte)this.type);
                while (--midSize >= 0) {
                    os.write((byte)(this.offset >>> 8 * midSize & 255));
                }
                os.write((byte)(this.generation >>> 8 & 255));
                os.write((byte)(this.generation & 255));
            }

            public int compareTo(Object o) {
                PdfCrossReference other = (PdfCrossReference)o;
                return this.refnum < other.refnum ? -1 : (this.refnum == other.refnum ? 0 : 1);
            }

            public boolean equals(Object obj) {
                if (obj instanceof PdfCrossReference) {
                    PdfCrossReference other = (PdfCrossReference)obj;
                    if (this.refnum == other.refnum) {
                        return true;
                    }
                    return false;
                }
                return false;
            }
        }

    }

    static class PdfTrailer
    extends PdfDictionary {
        int offset;

        PdfTrailer(int size, int offset, PdfIndirectReference root, PdfIndirectReference info, PdfIndirectReference encryption, PdfObject fileID, int prevxref) {
            this.offset = offset;
            this.put(PdfName.SIZE, new PdfNumber(size));
            this.put(PdfName.ROOT, root);
            if (info != null) {
                this.put(PdfName.INFO, info);
            }
            if (encryption != null) {
                this.put(PdfName.ENCRYPT, encryption);
            }
            if (fileID != null) {
                this.put(PdfName.ID, fileID);
            }
            if (prevxref > 0) {
                this.put(PdfName.PREV, new PdfNumber(prevxref));
            }
        }

        public void toPdf(PdfWriter writer, OutputStream os) throws IOException {
            os.write(DocWriter.getISOBytes("trailer\n"));
            super.toPdf(null, os);
            os.write(DocWriter.getISOBytes("\nstartxref\n"));
            os.write(DocWriter.getISOBytes(String.valueOf(this.offset)));
            os.write(DocWriter.getISOBytes("\n%%EOF\n"));
        }
    }

}

