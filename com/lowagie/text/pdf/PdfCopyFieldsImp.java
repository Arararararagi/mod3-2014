/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocListener;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.IntHashtable;
import com.lowagie.text.pdf.PRIndirectReference;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfDocument;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfIndirectObject;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfPages;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfString;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.SimpleBookmark;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

class PdfCopyFieldsImp
extends PdfWriter {
    private static final PdfName iTextTag = new PdfName("_iTextTag_");
    private static final Integer zero = new Integer(0);
    ArrayList readers = new ArrayList();
    HashMap readers2intrefs = new HashMap();
    HashMap pages2intrefs = new HashMap();
    HashMap visited = new HashMap();
    ArrayList fields = new ArrayList();
    RandomAccessFileOrArray file;
    HashMap fieldTree = new HashMap();
    ArrayList pageRefs = new ArrayList();
    ArrayList pageDics = new ArrayList();
    PdfDictionary resources = new PdfDictionary();
    PdfDictionary form;
    protected List newBookmarks;
    boolean closing = false;
    Document nd;
    private HashMap tabOrder;
    private ArrayList calculationOrder = new ArrayList();
    private ArrayList calculationOrderRefs;
    protected static final HashMap widgetKeys = new HashMap();
    protected static final HashMap fieldKeys = new HashMap();

    static {
        Integer one = new Integer(1);
        widgetKeys.put(PdfName.SUBTYPE, one);
        widgetKeys.put(PdfName.CONTENTS, one);
        widgetKeys.put(PdfName.RECT, one);
        widgetKeys.put(PdfName.NM, one);
        widgetKeys.put(PdfName.M, one);
        widgetKeys.put(PdfName.F, one);
        widgetKeys.put(PdfName.BS, one);
        widgetKeys.put(PdfName.BORDER, one);
        widgetKeys.put(PdfName.AP, one);
        widgetKeys.put(PdfName.AS, one);
        widgetKeys.put(PdfName.C, one);
        widgetKeys.put(PdfName.A, one);
        widgetKeys.put(PdfName.STRUCTPARENT, one);
        widgetKeys.put(PdfName.OC, one);
        widgetKeys.put(PdfName.H, one);
        widgetKeys.put(PdfName.MK, one);
        widgetKeys.put(PdfName.DA, one);
        widgetKeys.put(PdfName.Q, one);
        fieldKeys.put(PdfName.AA, one);
        fieldKeys.put(PdfName.FT, one);
        fieldKeys.put(PdfName.TU, one);
        fieldKeys.put(PdfName.TM, one);
        fieldKeys.put(PdfName.FF, one);
        fieldKeys.put(PdfName.V, one);
        fieldKeys.put(PdfName.DV, one);
        fieldKeys.put(PdfName.DS, one);
        fieldKeys.put(PdfName.RV, one);
        fieldKeys.put(PdfName.OPT, one);
        fieldKeys.put(PdfName.MAXLEN, one);
        fieldKeys.put(PdfName.TI, one);
        fieldKeys.put(PdfName.I, one);
        fieldKeys.put(PdfName.LOCK, one);
        fieldKeys.put(PdfName.SV, one);
    }

    PdfCopyFieldsImp(OutputStream os) throws DocumentException, IOException {
        this(os, '\u0000');
    }

    PdfCopyFieldsImp(OutputStream os, char pdfVersion) throws DocumentException, IOException {
        super(new PdfDocument(), os);
        this.pdf.addWriter(this);
        if (pdfVersion != '\u0000') {
            super.setPdfVersion(pdfVersion);
        }
        this.nd = new Document();
        this.nd.addDocListener(this.pdf);
    }

    void addDocument(PdfReader reader, List pagesToKeep) throws DocumentException {
        if (!this.readers2intrefs.containsKey(reader) && reader.isTampered()) {
            throw new DocumentException("The document was reused.");
        }
        reader = new PdfReader(reader);
        reader.selectPages(pagesToKeep);
        if (reader.getNumberOfPages() == 0) {
            return;
        }
        reader.setTampered(false);
        this.addDocument(reader);
    }

    void addDocument(PdfReader reader) throws DocumentException {
        this.openDoc();
        if (this.readers2intrefs.containsKey(reader)) {
            reader = new PdfReader(reader);
        } else {
            if (reader.isTampered()) {
                throw new DocumentException("The document was reused.");
            }
            reader.consolidateNamedDestinations();
            reader.setTampered(true);
        }
        reader.shuffleSubsetNames();
        this.readers2intrefs.put(reader, new IntHashtable());
        this.readers.add(reader);
        int len = reader.getNumberOfPages();
        IntHashtable refs = new IntHashtable();
        for (int p = 1; p <= len; ++p) {
            refs.put(reader.getPageOrigRef(p).getNumber(), 1);
            reader.releasePage(p);
        }
        this.pages2intrefs.put(reader, refs);
        this.visited.put(reader, new IntHashtable());
        this.fields.add(reader.getAcroFields());
        this.updateCalculationOrder(reader);
    }

    private static String getCOName(PdfReader reader, PRIndirectReference ref) {
        String name = "";
        while (ref != null) {
            PdfObject obj = PdfReader.getPdfObject(ref);
            if (obj == null || obj.type() != 6) break;
            PdfDictionary dic = (PdfDictionary)obj;
            PdfString t = (PdfString)PdfReader.getPdfObject(dic.get(PdfName.T));
            if (t != null) {
                name = String.valueOf(t.toUnicodeString()) + "." + name;
            }
            ref = (PRIndirectReference)dic.get(PdfName.PARENT);
        }
        if (name.endsWith(".")) {
            name = name.substring(0, name.length() - 1);
        }
        return name;
    }

    private void updateCalculationOrder(PdfReader reader) {
        PdfDictionary catalog = reader.getCatalog();
        PdfDictionary acro = (PdfDictionary)PdfReader.getPdfObject(catalog.get(PdfName.ACROFORM));
        if (acro == null) {
            return;
        }
        PdfArray co = (PdfArray)PdfReader.getPdfObject(acro.get(PdfName.CO));
        if (co == null || co.size() == 0) {
            return;
        }
        AcroFields af = reader.getAcroFields();
        ArrayList coa = co.getArrayList();
        for (int k = 0; k < coa.size(); ++k) {
            String name = PdfCopyFieldsImp.getCOName(reader, (PRIndirectReference)coa.get(k));
            if (af.getFieldItem(name) == null) continue;
            if (this.calculationOrder.contains(name = "." + name)) continue;
            this.calculationOrder.add(name);
        }
    }

    void propagate(PdfObject obj, PdfIndirectReference refo, boolean restricted) throws IOException {
        if (obj == null) {
            return;
        }
        if (obj instanceof PdfIndirectReference) {
            return;
        }
        switch (obj.type()) {
            case 6: 
            case 7: {
                PdfDictionary dic = (PdfDictionary)obj;
                Iterator it = dic.getKeys().iterator();
                while (it.hasNext()) {
                    PdfName key = (PdfName)it.next();
                    if (restricted && (key.equals(PdfName.PARENT) || key.equals(PdfName.KIDS))) continue;
                    PdfObject ob = dic.get(key);
                    if (ob != null && ob.isIndirect()) {
                        PRIndirectReference ind = (PRIndirectReference)ob;
                        if (this.setVisited(ind) || this.isPage(ind)) continue;
                        PdfIndirectReference ref = this.getNewReference(ind);
                        this.propagate(PdfReader.getPdfObjectRelease(ind), ref, restricted);
                        continue;
                    }
                    this.propagate(ob, null, restricted);
                }
                break;
            }
            case 5: {
                ArrayList list = ((PdfArray)obj).getArrayList();
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    PdfObject ob = (PdfObject)it.next();
                    if (ob != null && ob.isIndirect()) {
                        PRIndirectReference ind = (PRIndirectReference)ob;
                        if (this.isVisited(ind) || this.isPage(ind)) continue;
                        PdfIndirectReference ref = this.getNewReference(ind);
                        this.propagate(PdfReader.getPdfObjectRelease(ind), ref, restricted);
                        continue;
                    }
                    this.propagate(ob, null, restricted);
                }
                break;
            }
            case 10: {
                throw new RuntimeException("Reference pointing to reference.");
            }
        }
    }

    private void adjustTabOrder(PdfArray annots, PdfIndirectReference ind, PdfNumber nn) {
        int v = nn.intValue();
        ArrayList<Integer> t = (ArrayList<Integer>)this.tabOrder.get(annots);
        if (t == null) {
            t = new ArrayList<Integer>();
            int size = annots.size() - 1;
            for (int k = 0; k < size; ++k) {
                t.add(zero);
            }
            t.add(new Integer(v));
            this.tabOrder.put(annots, t);
            annots.add(ind);
        } else {
            int size;
            for (int k = size = t.size() - 1; k >= 0; --k) {
                if ((Integer)t.get(k) > v) continue;
                t.add(k + 1, new Integer(v));
                annots.getArrayList().add(k + 1, ind);
                size = -2;
                break;
            }
            if (size != -2) {
                t.add(0, new Integer(v));
                annots.getArrayList().add(0, ind);
            }
        }
    }

    protected PdfArray branchForm(HashMap level, PdfIndirectReference parent, String fname) throws IOException {
        PdfArray arr = new PdfArray();
        Iterator it = level.keySet().iterator();
        while (it.hasNext()) {
            String name = (String)it.next();
            Object obj = level.get(name);
            PdfIndirectReference ind = this.getPdfIndirectReference();
            PdfDictionary dic = new PdfDictionary();
            if (parent != null) {
                dic.put(PdfName.PARENT, parent);
            }
            dic.put(PdfName.T, new PdfString(name, "UnicodeBig"));
            String fname2 = String.valueOf(fname) + "." + name;
            int coidx = this.calculationOrder.indexOf(fname2);
            if (coidx >= 0) {
                this.calculationOrderRefs.set(coidx, ind);
            }
            if (obj instanceof HashMap) {
                dic.put(PdfName.KIDS, this.branchForm((HashMap)obj, ind, fname2));
                arr.add(ind);
                this.addToBody((PdfObject)dic, ind);
                continue;
            }
            ArrayList list = (ArrayList)obj;
            dic.mergeDifferent((PdfDictionary)list.get(0));
            if (list.size() == 3) {
                dic.mergeDifferent((PdfDictionary)list.get(2));
                int page = (Integer)list.get(1);
                PdfDictionary pageDic = (PdfDictionary)this.pageDics.get(page - 1);
                PdfArray annots = (PdfArray)PdfReader.getPdfObject(pageDic.get(PdfName.ANNOTS));
                if (annots == null) {
                    annots = new PdfArray();
                    pageDic.put(PdfName.ANNOTS, annots);
                }
                this.adjustTabOrder(annots, ind, (PdfNumber)dic.remove(iTextTag));
            } else {
                PdfArray kids = new PdfArray();
                for (int k = 1; k < list.size(); k+=2) {
                    int page = (Integer)list.get(k);
                    PdfDictionary pageDic = (PdfDictionary)this.pageDics.get(page - 1);
                    PdfArray annots = (PdfArray)PdfReader.getPdfObject(pageDic.get(PdfName.ANNOTS));
                    if (annots == null) {
                        annots = new PdfArray();
                        pageDic.put(PdfName.ANNOTS, annots);
                    }
                    PdfDictionary widget = new PdfDictionary();
                    widget.merge((PdfDictionary)list.get(k + 1));
                    widget.put(PdfName.PARENT, ind);
                    PdfNumber nn = (PdfNumber)widget.remove(iTextTag);
                    PdfIndirectReference wref = this.addToBody(widget).getIndirectReference();
                    this.adjustTabOrder(annots, wref, nn);
                    kids.add(wref);
                    this.propagate(widget, null, false);
                }
                dic.put(PdfName.KIDS, kids);
            }
            arr.add(ind);
            this.addToBody((PdfObject)dic, ind);
            this.propagate(dic, null, false);
        }
        return arr;
    }

    protected void createAcroForms() throws IOException {
        if (this.fieldTree.size() == 0) {
            return;
        }
        this.form = new PdfDictionary();
        this.form.put(PdfName.DR, this.resources);
        this.propagate(this.resources, null, false);
        this.form.put(PdfName.DA, new PdfString("/Helv 0 Tf 0 g "));
        this.tabOrder = new HashMap();
        this.calculationOrderRefs = new ArrayList(this.calculationOrder);
        this.form.put(PdfName.FIELDS, this.branchForm(this.fieldTree, null, ""));
        PdfArray co = new PdfArray();
        for (int k = 0; k < this.calculationOrderRefs.size(); ++k) {
            Object obj = this.calculationOrderRefs.get(k);
            if (!(obj instanceof PdfIndirectReference)) continue;
            co.add((PdfIndirectReference)obj);
        }
        if (co.size() > 0) {
            this.form.put(PdfName.CO, co);
        }
    }

    public void close() {
        if (this.closing) {
            super.close();
            return;
        }
        this.closing = true;
        try {
            this.closeIt();
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    protected void closeIt() throws DocumentException, IOException {
        for (k = 0; k < this.readers.size(); ++k) {
            ((PdfReader)this.readers.get(k)).removeFields();
        }
        for (r = 0; r < this.readers.size(); ++r) {
            reader = (PdfReader)this.readers.get(r);
            for (page = 1; page <= reader.getNumberOfPages(); ++page) {
                this.pageRefs.add(this.getNewReference(reader.getPageOrigRef(page)));
                this.pageDics.add(reader.getPageN(page));
            }
        }
        this.mergeFields();
        this.createAcroForms();
        for (r = 0; r < this.readers.size(); ++r) {
            reader = (PdfReader)this.readers.get(r);
            for (page = 1; page <= reader.getNumberOfPages(); ++page) {
                dic = reader.getPageN(page);
                pageRef = this.getNewReference(reader.getPageOrigRef(page));
                parent = this.root.addPageRef(pageRef);
                dic.put(PdfName.PARENT, parent);
                this.propagate(dic, pageRef, false);
            }
        }
        it = this.readers2intrefs.keySet().iterator();
        while (it.hasNext()) {
            reader = (PdfReader)it.next();
            try {
                this.file = reader.getSafeFile();
                this.file.reOpen();
                t = (IntHashtable)this.readers2intrefs.get(reader);
                keys = t.toOrderedKeys();
                for (k = 0; k < keys.length; ++k) {
                    ref = new PRIndirectReference(reader, keys[k]);
                    this.addToBody(PdfReader.getPdfObjectRelease(ref), t.get(keys[k]));
                }
                var7_11 = null;
            }
            catch (Throwable var8_13) {
                var7_11 = null;
                try {
                    this.file.close();
                    reader.close();
                    throw var8_13;
                }
                catch (Exception e) {
                    // empty catch block
                }
                throw var8_13;
            }
            ** try [egrp 1[TRYBLOCK] [2 : 337->351)] { 
lbl48: // 1 sources:
            this.file.close();
            reader.close();
            continue;
lbl51: // 1 sources:
            catch (Exception e) {
                // empty catch block
            }
        }
        this.pdf.close();
    }

    void addPageOffsetToField(HashMap fd, int pageOffset) {
        if (pageOffset == 0) {
            return;
        }
        Iterator it = fd.values().iterator();
        while (it.hasNext()) {
            ArrayList page = ((AcroFields.Item)it.next()).page;
            for (int k = 0; k < page.size(); ++k) {
                page.set(k, new Integer((Integer)page.get(k) + pageOffset));
            }
        }
    }

    void createWidgets(ArrayList list, AcroFields.Item item) {
        for (int k = 0; k < item.merged.size(); ++k) {
            list.add(item.page.get(k));
            PdfDictionary merged = (PdfDictionary)item.merged.get(k);
            PdfObject dr = merged.get(PdfName.DR);
            if (dr != null) {
                PdfFormField.mergeResources(this.resources, (PdfDictionary)PdfReader.getPdfObject(dr));
            }
            PdfDictionary widget = new PdfDictionary();
            Iterator it = merged.getKeys().iterator();
            while (it.hasNext()) {
                PdfName key = (PdfName)it.next();
                if (!widgetKeys.containsKey(key)) continue;
                widget.put(key, merged.get(key));
            }
            widget.put(iTextTag, new PdfNumber((Integer)item.tabOrder.get(k) + 1));
            list.add(widget);
        }
    }

    void mergeField(String name, AcroFields.Item item) {
        HashMap map;
        String s;
        Object obj;
        block15 : {
            map = this.fieldTree;
            StringTokenizer tk = new StringTokenizer(name, ".");
            if (!tk.hasMoreTokens()) {
                return;
            }
            do {
                s = tk.nextToken();
                obj = map.get(s);
                if (!tk.hasMoreTokens()) break block15;
                if (obj == null) {
                    obj = new HashMap();
                    map.put(s, obj);
                    map = (HashMap)obj;
                    continue;
                }
                if (!(obj instanceof HashMap)) break;
                map = (HashMap)obj;
            } while (true);
            return;
        }
        if (obj instanceof HashMap) {
            return;
        }
        PdfDictionary merged = (PdfDictionary)item.merged.get(0);
        if (obj == null) {
            PdfDictionary field = new PdfDictionary();
            Iterator it = merged.getKeys().iterator();
            while (it.hasNext()) {
                PdfName key = (PdfName)it.next();
                if (!fieldKeys.containsKey(key)) continue;
                field.put(key, merged.get(key));
            }
            ArrayList<PdfDictionary> list = new ArrayList<PdfDictionary>();
            list.add(field);
            this.createWidgets(list, item);
            map.put(s, list);
        } else {
            ArrayList list = (ArrayList)obj;
            PdfDictionary field = (PdfDictionary)list.get(0);
            PdfName type1 = (PdfName)field.get(PdfName.FT);
            PdfName type2 = (PdfName)merged.get(PdfName.FT);
            if (!(type1 != null && type1.equals(type2))) {
                return;
            }
            int flag1 = 0;
            PdfObject f1 = field.get(PdfName.FF);
            if (f1 != null && f1.isNumber()) {
                flag1 = ((PdfNumber)f1).intValue();
            }
            int flag2 = 0;
            PdfObject f2 = merged.get(PdfName.FF);
            if (f2 != null && f2.isNumber()) {
                flag2 = ((PdfNumber)f2).intValue();
            }
            if (type1.equals(PdfName.BTN)) {
                if (((flag1 ^ flag2) & 65536) != 0) {
                    return;
                }
                if ((flag1 & 65536) == 0 && ((flag1 ^ flag2) & 32768) != 0) {
                    return;
                }
            } else if (type1.equals(PdfName.CH) && ((flag1 ^ flag2) & 131072) != 0) {
                return;
            }
            this.createWidgets(list, item);
        }
    }

    void mergeWithMaster(HashMap fd) {
        Iterator it = fd.keySet().iterator();
        while (it.hasNext()) {
            String name = (String)it.next();
            this.mergeField(name, (AcroFields.Item)fd.get(name));
        }
    }

    void mergeFields() {
        int pageOffset = 0;
        for (int k = 0; k < this.fields.size(); ++k) {
            HashMap fd = ((AcroFields)this.fields.get(k)).getFields();
            this.addPageOffsetToField(fd, pageOffset);
            this.mergeWithMaster(fd);
            pageOffset+=((PdfReader)this.readers.get(k)).getNumberOfPages();
        }
    }

    public PdfIndirectReference getPageReference(int page) {
        return (PdfIndirectReference)this.pageRefs.get(page - 1);
    }

    protected PdfDictionary getCatalog(PdfIndirectReference rootObj) {
        try {
            PdfDocument.PdfCatalog cat = ((PdfDocument)this.document).getCatalog(rootObj);
            if (this.form != null) {
                PdfIndirectReference ref = this.addToBody(this.form).getIndirectReference();
                cat.put(PdfName.ACROFORM, ref);
            }
            if (this.newBookmarks == null || this.newBookmarks.size() == 0) {
                return cat;
            }
            PdfDictionary top = new PdfDictionary();
            PdfIndirectReference topRef = this.getPdfIndirectReference();
            Object[] kids = SimpleBookmark.iterateOutlines(this, topRef, this.newBookmarks, false);
            top.put(PdfName.FIRST, (PdfIndirectReference)kids[0]);
            top.put(PdfName.LAST, (PdfIndirectReference)kids[1]);
            top.put(PdfName.COUNT, new PdfNumber((Integer)kids[2]));
            this.addToBody((PdfObject)top, topRef);
            cat.put(PdfName.OUTLINES, topRef);
            return cat;
        }
        catch (IOException e) {
            throw new ExceptionConverter(e);
        }
    }

    protected PdfIndirectReference getNewReference(PRIndirectReference ref) {
        return new PdfIndirectReference(0, this.getNewObjectNumber(ref.getReader(), ref.getNumber(), 0));
    }

    protected int getNewObjectNumber(PdfReader reader, int number, int generation) {
        IntHashtable refs = (IntHashtable)this.readers2intrefs.get(reader);
        int n = refs.get(number);
        if (n == 0) {
            n = this.getIndirectReferenceNumber();
            refs.put(number, n);
        }
        return n;
    }

    protected boolean isVisited(PdfReader reader, int number, int generation) {
        IntHashtable refs = (IntHashtable)this.readers2intrefs.get(reader);
        return refs.containsKey(number);
    }

    protected boolean isVisited(PRIndirectReference ref) {
        IntHashtable refs = (IntHashtable)this.visited.get(ref.getReader());
        return refs.containsKey(ref.getNumber());
    }

    protected boolean setVisited(PRIndirectReference ref) {
        IntHashtable refs = (IntHashtable)this.visited.get(ref.getReader());
        if (refs.put(ref.getNumber(), 1) != 0) {
            return true;
        }
        return false;
    }

    protected boolean isPage(PRIndirectReference ref) {
        IntHashtable refs = (IntHashtable)this.pages2intrefs.get(ref.getReader());
        return refs.containsKey(ref.getNumber());
    }

    RandomAccessFileOrArray getReaderFile(PdfReader reader) {
        return this.file;
    }

    public void setOutlines(List outlines) {
        this.newBookmarks = outlines;
    }

    public void openDoc() {
        if (!this.nd.isOpen()) {
            this.nd.open();
        }
    }
}

