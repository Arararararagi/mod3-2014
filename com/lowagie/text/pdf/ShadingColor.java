/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.ExtendedColor;
import com.lowagie.text.pdf.PdfShadingPattern;

public class ShadingColor
extends ExtendedColor {
    PdfShadingPattern shadingPattern;

    public ShadingColor(PdfShadingPattern shadingPattern) {
        super(5, 0.5f, 0.5f, 0.5f);
        this.shadingPattern = shadingPattern;
    }

    public PdfShadingPattern getPdfShadingPattern() {
        return this.shadingPattern;
    }
}

