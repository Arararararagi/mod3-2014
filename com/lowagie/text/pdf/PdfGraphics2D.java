/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.Image;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.DefaultFontMapper;
import com.lowagie.text.pdf.FontMapper;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfGState;
import com.lowagie.text.pdf.PdfPatternPainter;
import com.lowagie.text.pdf.PdfShading;
import com.lowagie.text.pdf.PdfShadingPattern;
import com.lowagie.text.pdf.PdfWriter;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.MediaTracker;
import java.awt.Paint;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.TexturePaint;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorModel;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.awt.image.renderable.RenderableImage;
import java.io.ByteArrayOutputStream;
import java.text.AttributedCharacterIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class PdfGraphics2D
extends Graphics2D {
    private static final int FILL = 1;
    private static final int STROKE = 2;
    private static final int CLIP = 3;
    private BasicStroke strokeOne = new BasicStroke(1.0f);
    private static AffineTransform IDENTITY = new AffineTransform();
    private Font font;
    private BaseFont baseFont;
    private float fontSize;
    private AffineTransform transform;
    private Paint paint;
    private Color background;
    private float width;
    private float height;
    private Area clip;
    private RenderingHints rhints = new RenderingHints(null);
    private Stroke stroke;
    private Stroke originalStroke;
    private PdfContentByte cb;
    private HashMap baseFonts;
    private boolean disposeCalled = false;
    private FontMapper fontMapper;
    private ArrayList kids;
    private boolean kid = false;
    private Graphics dg2 = new BufferedImage(2, 2, 1).getGraphics();
    private boolean onlyShapes = false;
    private Stroke oldStroke;
    private Paint paintFill;
    private Paint paintStroke;
    private MediaTracker mediaTracker;
    protected boolean underline;
    protected PdfGState[] fillGState = new PdfGState[256];
    protected PdfGState[] strokeGState = new PdfGState[256];
    protected int currentFillGState = 255;
    protected int currentStrokeGState = 255;
    public static int AFM_DIVISOR = 1000;
    private boolean convertImagesToJPEG = false;
    private float jpegQuality = 0.95f;

    private PdfGraphics2D() {
    }

    PdfGraphics2D(PdfContentByte cb, float width, float height, FontMapper fontMapper, boolean onlyShapes, boolean convertImagesToJPEG, float quality) {
        try {
            Class.forName("com.sun.image.codec.jpeg.JPEGCodec");
        }
        catch (Throwable t) {
            convertImagesToJPEG = false;
        }
        this.convertImagesToJPEG = convertImagesToJPEG;
        this.jpegQuality = quality;
        this.onlyShapes = onlyShapes;
        this.transform = new AffineTransform();
        this.baseFonts = new HashMap();
        if (!onlyShapes) {
            this.fontMapper = fontMapper;
            if (this.fontMapper == null) {
                this.fontMapper = new DefaultFontMapper();
            }
        }
        this.kids = new ArrayList();
        this.paint = Color.black;
        this.background = Color.white;
        this.setFont(new Font("sanserif", 0, 12));
        this.cb = cb;
        cb.saveState();
        this.width = width;
        this.height = height;
        this.clip = new Area(new Rectangle2D.Float(0.0f, 0.0f, width, height));
        this.clip(this.clip);
        this.stroke = this.oldStroke = this.strokeOne;
        this.originalStroke = this.oldStroke;
        this.setStrokeDiff(this.stroke, null);
        cb.saveState();
    }

    public void draw(Shape s) {
        this.followPath(s, 2);
    }

    public boolean drawImage(java.awt.Image img, AffineTransform xform, ImageObserver obs) {
        return this.drawImage(img, null, xform, null, obs);
    }

    public void drawImage(BufferedImage img, BufferedImageOp op, int x, int y) {
        BufferedImage result = op.createCompatibleDestImage(img, img.getColorModel());
        result = op.filter(img, result);
        this.drawImage((java.awt.Image)result, x, y, null);
    }

    public void drawRenderedImage(RenderedImage img, AffineTransform xform) {
        BufferedImage image = null;
        if (img instanceof BufferedImage) {
            image = (BufferedImage)img;
        } else {
            ColorModel cm = img.getColorModel();
            int width = img.getWidth();
            int height = img.getHeight();
            WritableRaster raster = cm.createCompatibleWritableRaster(width, height);
            boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
            Hashtable<String, Object> properties = new Hashtable<String, Object>();
            String[] keys = img.getPropertyNames();
            if (keys != null) {
                for (int i = 0; i < keys.length; ++i) {
                    properties.put(keys[i], img.getProperty(keys[i]));
                }
            }
            BufferedImage result = new BufferedImage(cm, raster, isAlphaPremultiplied, properties);
            img.copyData(raster);
            image = result;
        }
        this.drawImage(image, xform, null);
    }

    public void drawRenderableImage(RenderableImage img, AffineTransform xform) {
        this.drawRenderedImage(img.createDefaultRendering(), xform);
    }

    public void drawString(String s, int x, int y) {
        this.drawString(s, (float)x, (float)y);
    }

    public static double asPoints(double d, int i) {
        return d * (double)i / (double)AFM_DIVISOR;
    }

    protected void doAttributes(AttributedCharacterIterator iter) {
        this.underline = false;
        Set<AttributedCharacterIterator.Attribute> set = iter.getAttributes().keySet();
        Iterator<AttributedCharacterIterator.Attribute> iterator = set.iterator();
        while (iterator.hasNext()) {
            TextAttribute textattribute = (TextAttribute)iterator.next();
            if (textattribute.equals(TextAttribute.FONT)) {
                Font font = (Font)iter.getAttributes().get(textattribute);
                this.setFont(font);
                continue;
            }
            if (textattribute.equals(TextAttribute.UNDERLINE)) {
                if (iter.getAttributes().get(textattribute) != TextAttribute.UNDERLINE_ON) continue;
                this.underline = true;
                continue;
            }
            if (textattribute.equals(TextAttribute.SUPERSCRIPT)) continue;
            if (textattribute.equals(TextAttribute.SIZE)) {
                Object obj = iter.getAttributes().get(textattribute);
                Font font1 = null;
                if (obj instanceof Integer) {
                    int i = (Integer)obj;
                    font1 = this.getFont().deriveFont(this.getFont().getStyle(), i);
                } else if (obj instanceof Float) {
                    float f = ((Float)obj).floatValue();
                    font1 = this.getFont().deriveFont(this.getFont().getStyle(), f);
                } else {
                    return;
                }
                this.setFont(font1);
                continue;
            }
            String s = "only FONT/SIZE/UNDERLINE/SUPERSCRIPT supported";
            throw new RuntimeException(s);
        }
    }

    public void drawString(String s, float x, float y) {
        if (s.length() == 0) {
            return;
        }
        this.setFillPaint();
        if (this.onlyShapes) {
            TextLayout tl = new TextLayout(s, this.font, new FontRenderContext(new AffineTransform(), false, true));
            tl.draw(this, x, y);
        } else {
            AffineTransform at = this.getTransform();
            AffineTransform at2 = this.getTransform();
            at2.translate(x, y);
            at2.concatenate(this.font.getTransform());
            this.setTransform(at2);
            AffineTransform inverse = this.normalizeMatrix();
            AffineTransform flipper = AffineTransform.getScaleInstance(1.0, -1.0);
            inverse.concatenate(flipper);
            double[] mx = new double[6];
            inverse.getMatrix(mx);
            this.cb.beginText();
            this.cb.setFontAndSize(this.baseFont, this.fontSize);
            this.cb.setTextMatrix((float)mx[0], (float)mx[1], (float)mx[2], (float)mx[3], (float)mx[4], (float)mx[5]);
            double width = this.font.deriveFont(IDENTITY).getStringBounds(s, this.getFontRenderContext()).getWidth();
            if (s.length() > 1) {
                float adv = ((float)width - this.baseFont.getWidthPoint(s, this.fontSize)) / (float)(s.length() - 1);
                this.cb.setCharacterSpacing(adv);
            }
            this.cb.showText(s);
            if (s.length() > 1) {
                this.cb.setCharacterSpacing(0.0f);
            }
            this.cb.endText();
            this.setTransform(at);
            if (this.underline) {
                int UnderlinePosition = -100;
                int UnderlineThickness = 50;
                double d = PdfGraphics2D.asPoints(UnderlineThickness, (int)this.fontSize);
                this.setStroke(new BasicStroke((float)d));
                y = (float)((double)y + PdfGraphics2D.asPoints(- UnderlineThickness, (int)this.fontSize));
                Line2D.Double line = new Line2D.Double(x, y, width + (double)x, y);
                this.draw(line);
            }
        }
    }

    public void drawString(AttributedCharacterIterator iterator, int x, int y) {
        this.drawString(iterator, (float)x, (float)y);
    }

    public void drawString(AttributedCharacterIterator iter, float x, float y) {
        StringBuffer stringbuffer = new StringBuffer(iter.getEndIndex());
        char c = iter.first();
        while (c != '\uffff') {
            if (iter.getIndex() == iter.getRunStart()) {
                if (stringbuffer.length() > 0) {
                    this.drawString(stringbuffer.toString(), x, y);
                    FontMetrics fontmetrics = this.getFontMetrics();
                    x = (float)((double)x + fontmetrics.getStringBounds(stringbuffer.toString(), this).getWidth());
                    stringbuffer.delete(0, stringbuffer.length());
                }
                this.doAttributes(iter);
            }
            stringbuffer.append(c);
            c = iter.next();
        }
        this.drawString(stringbuffer.toString(), x, y);
        this.underline = false;
    }

    public void drawGlyphVector(GlyphVector g, float x, float y) {
        Shape s = g.getOutline(x, y);
        this.fill(s);
    }

    public void fill(Shape s) {
        this.followPath(s, 1);
    }

    public boolean hit(Rectangle rect, Shape s, boolean onStroke) {
        if (onStroke) {
            s = this.stroke.createStrokedShape(s);
        }
        s = this.transform.createTransformedShape(s);
        Area area = new Area(s);
        if (this.clip != null) {
            area.intersect(this.clip);
        }
        return area.intersects(rect.x, rect.y, rect.width, rect.height);
    }

    public GraphicsConfiguration getDeviceConfiguration() {
        return ((Graphics2D)this.dg2).getDeviceConfiguration();
    }

    public void setComposite(Composite comp) {
    }

    public void setPaint(Paint paint) {
        if (paint == null) {
            return;
        }
        this.paint = paint;
    }

    private Stroke transformStroke(Stroke stroke) {
        if (!(stroke instanceof BasicStroke)) {
            return stroke;
        }
        BasicStroke st = (BasicStroke)stroke;
        float scale = (float)Math.sqrt(Math.abs(this.transform.getDeterminant()));
        float[] dash = st.getDashArray();
        if (dash != null) {
            int k = 0;
            while (k < dash.length) {
                float[] arrf = dash;
                int n = k++;
                arrf[n] = arrf[n] * scale;
            }
        }
        return new BasicStroke(st.getLineWidth() * scale, st.getEndCap(), st.getLineJoin(), st.getMiterLimit(), dash, st.getDashPhase() * scale);
    }

    private void setStrokeDiff(Stroke newStroke, Stroke oldStroke) {
        if (newStroke == oldStroke) {
            return;
        }
        if (!(newStroke instanceof BasicStroke)) {
            return;
        }
        BasicStroke nStroke = (BasicStroke)newStroke;
        boolean oldOk = oldStroke instanceof BasicStroke;
        BasicStroke oStroke = null;
        if (oldOk) {
            oStroke = (BasicStroke)oldStroke;
        }
        if (!(oldOk && nStroke.getLineWidth() == oStroke.getLineWidth())) {
            this.cb.setLineWidth(nStroke.getLineWidth());
        }
        if (!(oldOk && nStroke.getEndCap() == oStroke.getEndCap())) {
            switch (nStroke.getEndCap()) {
                case 0: {
                    this.cb.setLineCap(0);
                    break;
                }
                case 2: {
                    this.cb.setLineCap(2);
                    break;
                }
                default: {
                    this.cb.setLineCap(1);
                }
            }
        }
        if (!(oldOk && nStroke.getLineJoin() == oStroke.getLineJoin())) {
            switch (nStroke.getLineJoin()) {
                case 0: {
                    this.cb.setLineJoin(0);
                    break;
                }
                case 2: {
                    this.cb.setLineJoin(2);
                    break;
                }
                default: {
                    this.cb.setLineJoin(1);
                }
            }
        }
        if (!(oldOk && nStroke.getMiterLimit() == oStroke.getMiterLimit())) {
            this.cb.setMiterLimit(nStroke.getMiterLimit());
        }
        boolean makeDash = oldOk ? (nStroke.getDashArray() != null ? (nStroke.getDashPhase() != oStroke.getDashPhase() ? true : !Arrays.equals(nStroke.getDashArray(), oStroke.getDashArray())) : oStroke.getDashArray() != null) : true;
        if (makeDash) {
            float[] dash = nStroke.getDashArray();
            if (dash == null) {
                this.cb.setLiteral("[]0 d\n");
            } else {
                this.cb.setLiteral('[');
                int lim = dash.length;
                for (int k = 0; k < lim; ++k) {
                    this.cb.setLiteral(dash[k]);
                    this.cb.setLiteral(' ');
                }
                this.cb.setLiteral(']');
                this.cb.setLiteral(nStroke.getDashPhase());
                this.cb.setLiteral(" d\n");
            }
        }
    }

    public void setStroke(Stroke s) {
        this.originalStroke = s;
        this.stroke = this.transformStroke(s);
    }

    public void setRenderingHint(RenderingHints.Key arg0, Object arg1) {
        this.rhints.put(arg0, arg1);
    }

    public Object getRenderingHint(RenderingHints.Key arg0) {
        return this.rhints.get(arg0);
    }

    public void setRenderingHints(Map hints) {
        this.rhints.clear();
        this.rhints.putAll(hints);
    }

    public void addRenderingHints(Map hints) {
        this.rhints.putAll(hints);
    }

    public RenderingHints getRenderingHints() {
        return this.rhints;
    }

    public void translate(int x, int y) {
        this.translate((double)x, (double)y);
    }

    public void translate(double tx, double ty) {
        this.transform.translate(tx, ty);
    }

    public void rotate(double theta) {
        this.transform.rotate(theta);
    }

    public void rotate(double theta, double x, double y) {
        this.transform.rotate(theta, x, y);
    }

    public void scale(double sx, double sy) {
        this.transform.scale(sx, sy);
        this.stroke = this.transformStroke(this.originalStroke);
    }

    public void shear(double shx, double shy) {
        this.transform.shear(shx, shy);
    }

    public void transform(AffineTransform tx) {
        this.transform.concatenate(tx);
        this.stroke = this.transformStroke(this.originalStroke);
    }

    public void setTransform(AffineTransform t) {
        this.transform = new AffineTransform(t);
        this.stroke = this.transformStroke(this.originalStroke);
    }

    public AffineTransform getTransform() {
        return new AffineTransform(this.transform);
    }

    public Paint getPaint() {
        return this.paint;
    }

    public Composite getComposite() {
        return null;
    }

    public void setBackground(Color color) {
        this.background = color;
    }

    public Color getBackground() {
        return this.background;
    }

    public Stroke getStroke() {
        return this.originalStroke;
    }

    public FontRenderContext getFontRenderContext() {
        return new FontRenderContext(null, true, true);
    }

    public Graphics create() {
        PdfGraphics2D g2 = new PdfGraphics2D();
        g2.onlyShapes = this.onlyShapes;
        g2.transform = new AffineTransform(this.transform);
        g2.baseFonts = this.baseFonts;
        g2.fontMapper = this.fontMapper;
        g2.kids = this.kids;
        g2.paint = this.paint;
        g2.fillGState = this.fillGState;
        g2.strokeGState = this.strokeGState;
        g2.background = this.background;
        g2.mediaTracker = this.mediaTracker;
        g2.convertImagesToJPEG = this.convertImagesToJPEG;
        g2.jpegQuality = this.jpegQuality;
        g2.setFont(this.font);
        g2.cb = this.cb.getDuplicate();
        g2.cb.saveState();
        g2.width = this.width;
        g2.height = this.height;
        g2.followPath(new Area(new Rectangle2D.Float(0.0f, 0.0f, this.width, this.height)), 3);
        if (this.clip != null) {
            g2.clip = new Area(this.clip);
        }
        g2.stroke = this.stroke;
        g2.originalStroke = this.originalStroke;
        g2.oldStroke = g2.strokeOne = (BasicStroke)g2.transformStroke(g2.strokeOne);
        g2.setStrokeDiff(g2.oldStroke, null);
        g2.cb.saveState();
        if (g2.clip != null) {
            g2.followPath(g2.clip, 3);
        }
        g2.kid = true;
        ArrayList arrayList = this.kids;
        synchronized (arrayList) {
            this.kids.add(g2);
        }
        return g2;
    }

    public PdfContentByte getContent() {
        return this.cb;
    }

    public Color getColor() {
        if (this.paint instanceof Color) {
            return (Color)this.paint;
        }
        return Color.black;
    }

    public void setColor(Color color) {
        this.setPaint(color);
    }

    public void setPaintMode() {
    }

    public void setXORMode(Color c1) {
    }

    public Font getFont() {
        return this.font;
    }

    public void setFont(Font f) {
        if (f == null) {
            return;
        }
        if (this.onlyShapes) {
            this.font = f;
            return;
        }
        if (f == this.font) {
            return;
        }
        this.font = f;
        this.fontSize = f.getSize2D();
        this.baseFont = this.getCachedBaseFont(f);
    }

    private BaseFont getCachedBaseFont(Font f) {
        HashMap hashMap = this.baseFonts;
        synchronized (hashMap) {
            BaseFont bf = (BaseFont)this.baseFonts.get(f.getFontName());
            if (bf == null) {
                bf = this.fontMapper.awtToPdf(f);
                this.baseFonts.put(f.getFontName(), bf);
            }
            return bf;
        }
    }

    public FontMetrics getFontMetrics(Font f) {
        return this.dg2.getFontMetrics(f);
    }

    public Rectangle getClipBounds() {
        if (this.clip == null) {
            return null;
        }
        return this.getClip().getBounds();
    }

    public void clipRect(int x, int y, int width, int height) {
        Rectangle2D.Double rect = new Rectangle2D.Double(x, y, width, height);
        this.clip(rect);
    }

    public void setClip(int x, int y, int width, int height) {
        Rectangle2D.Double rect = new Rectangle2D.Double(x, y, width, height);
        this.setClip(rect);
    }

    public void clip(Shape s) {
        if (s != null) {
            s = this.transform.createTransformedShape(s);
        }
        if (this.clip == null) {
            this.clip = new Area(s);
        } else {
            this.clip.intersect(new Area(s));
        }
        this.followPath(s, 3);
    }

    public Shape getClip() {
        try {
            return this.transform.createInverse().createTransformedShape(this.clip);
        }
        catch (NoninvertibleTransformException e) {
            return null;
        }
    }

    public void setClip(Shape s) {
        this.cb.restoreState();
        this.cb.saveState();
        if (s != null) {
            s = this.transform.createTransformedShape(s);
        }
        if (s == null) {
            this.clip = null;
        } else {
            this.clip = new Area(s);
            this.followPath(s, 3);
        }
        this.paintStroke = null;
        this.paintFill = null;
        this.currentStrokeGState = 255;
        this.currentFillGState = 255;
        this.oldStroke = this.strokeOne;
    }

    public void copyArea(int x, int y, int width, int height, int dx, int dy) {
    }

    public void drawLine(int x1, int y1, int x2, int y2) {
        Line2D.Double line = new Line2D.Double(x1, y1, x2, y2);
        this.draw(line);
    }

    public void drawRect(int x, int y, int width, int height) {
        this.draw(new Rectangle(x, y, width, height));
    }

    public void fillRect(int x, int y, int width, int height) {
        this.fill(new Rectangle(x, y, width, height));
    }

    public void clearRect(int x, int y, int width, int height) {
        Paint temp = this.paint;
        this.setPaint(this.background);
        this.fillRect(x, y, width, height);
        this.setPaint(temp);
    }

    public void drawRoundRect(int x, int y, int width, int height, int arcWidth, int arcHeight) {
        RoundRectangle2D.Double rect = new RoundRectangle2D.Double(x, y, width, height, arcWidth, arcHeight);
        this.draw(rect);
    }

    public void fillRoundRect(int x, int y, int width, int height, int arcWidth, int arcHeight) {
        RoundRectangle2D.Double rect = new RoundRectangle2D.Double(x, y, width, height, arcWidth, arcHeight);
        this.fill(rect);
    }

    public void drawOval(int x, int y, int width, int height) {
        Ellipse2D.Float oval = new Ellipse2D.Float(x, y, width, height);
        this.draw(oval);
    }

    public void fillOval(int x, int y, int width, int height) {
        Ellipse2D.Float oval = new Ellipse2D.Float(x, y, width, height);
        this.fill(oval);
    }

    public void drawArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
        Arc2D.Double arc = new Arc2D.Double(x, y, width, height, startAngle, arcAngle, 0);
        this.draw(arc);
    }

    public void fillArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
        Arc2D.Double arc = new Arc2D.Double(x, y, width, height, startAngle, arcAngle, 2);
        this.fill(arc);
    }

    public void drawPolyline(int[] x, int[] y, int nPoints) {
        Line2D.Double line = new Line2D.Double(x[0], y[0], x[0], y[0]);
        for (int i = 1; i < nPoints; ++i) {
            line.setLine(line.getX2(), line.getY2(), x[i], y[i]);
            this.draw(line);
        }
    }

    public void drawPolygon(int[] xPoints, int[] yPoints, int nPoints) {
        Polygon poly = new Polygon();
        for (int i = 0; i < nPoints; ++i) {
            poly.addPoint(xPoints[i], yPoints[i]);
        }
        this.draw(poly);
    }

    public void fillPolygon(int[] xPoints, int[] yPoints, int nPoints) {
        Polygon poly = new Polygon();
        for (int i = 0; i < nPoints; ++i) {
            poly.addPoint(xPoints[i], yPoints[i]);
        }
        this.fill(poly);
    }

    public boolean drawImage(java.awt.Image img, int x, int y, ImageObserver observer) {
        return this.drawImage(img, x, y, null, observer);
    }

    public boolean drawImage(java.awt.Image img, int x, int y, int width, int height, ImageObserver observer) {
        return this.drawImage(img, x, y, width, height, null, observer);
    }

    public boolean drawImage(java.awt.Image img, int x, int y, Color bgcolor, ImageObserver observer) {
        this.waitForImage(img);
        return this.drawImage(img, x, y, img.getWidth(observer), img.getHeight(observer), bgcolor, observer);
    }

    public boolean drawImage(java.awt.Image img, int x, int y, int width, int height, Color bgcolor, ImageObserver observer) {
        this.waitForImage(img);
        double scalex = (double)width / (double)img.getWidth(observer);
        double scaley = (double)height / (double)img.getHeight(observer);
        AffineTransform tx = AffineTransform.getTranslateInstance(x, y);
        tx.scale(scalex, scaley);
        return this.drawImage(img, null, tx, bgcolor, observer);
    }

    public boolean drawImage(java.awt.Image img, int dx1, int dy1, int dx2, int dy2, int sx1, int sy1, int sx2, int sy2, ImageObserver observer) {
        return this.drawImage(img, dx1, dy1, dx2, dy2, sx1, sy1, sx2, sy2, null, observer);
    }

    public boolean drawImage(java.awt.Image img, int dx1, int dy1, int dx2, int dy2, int sx1, int sy1, int sx2, int sy2, Color bgcolor, ImageObserver observer) {
        this.waitForImage(img);
        double dwidth = (double)dx2 - (double)dx1;
        double dheight = (double)dy2 - (double)dy1;
        double swidth = (double)sx2 - (double)sx1;
        double sheight = (double)sy2 - (double)sy1;
        if (dwidth == 0.0 || dheight == 0.0 || swidth == 0.0 || sheight == 0.0) {
            return true;
        }
        double scalex = dwidth / swidth;
        double scaley = dheight / sheight;
        double transx = (double)sx1 * scalex;
        double transy = (double)sy1 * scaley;
        AffineTransform tx = AffineTransform.getTranslateInstance((double)dx1 - transx, (double)dy1 - transy);
        tx.scale(scalex, scaley);
        BufferedImage mask = new BufferedImage(img.getWidth(observer), img.getHeight(observer), 12);
        Graphics g = mask.getGraphics();
        g.fillRect(sx1, sy1, (int)swidth, (int)sheight);
        this.drawImage(img, mask, tx, null, observer);
        return true;
    }

    public void dispose() {
        if (this.kid) {
            return;
        }
        if (!this.disposeCalled) {
            this.disposeCalled = true;
            this.cb.restoreState();
            this.cb.restoreState();
            for (int k = 0; k < this.kids.size(); ++k) {
                PdfGraphics2D g2 = (PdfGraphics2D)this.kids.get(k);
                g2.cb.restoreState();
                g2.cb.restoreState();
                this.cb.add(g2.cb);
            }
        }
    }

    private void followPath(Shape s, int drawType) {
        if (s == null) {
            return;
        }
        if (!(drawType != 2 || this.stroke instanceof BasicStroke)) {
            s = this.stroke.createStrokedShape(s);
            this.followPath(s, 1);
            return;
        }
        if (drawType == 2) {
            this.setStrokeDiff(this.stroke, this.oldStroke);
            this.oldStroke = this.stroke;
            this.setStrokePaint();
        } else if (drawType == 1) {
            this.setFillPaint();
        }
        PathIterator points = drawType == 3 ? s.getPathIterator(IDENTITY) : s.getPathIterator(this.transform);
        float[] coords = new float[6];
        int traces = 0;
        while (!points.isDone()) {
            ++traces;
            int segtype = points.currentSegment(coords);
            this.normalizeY(coords);
            switch (segtype) {
                case 4: {
                    this.cb.closePath();
                    break;
                }
                case 3: {
                    this.cb.curveTo(coords[0], coords[1], coords[2], coords[3], coords[4], coords[5]);
                    break;
                }
                case 1: {
                    this.cb.lineTo(coords[0], coords[1]);
                    break;
                }
                case 0: {
                    this.cb.moveTo(coords[0], coords[1]);
                    break;
                }
                case 2: {
                    this.cb.curveTo(coords[0], coords[1], coords[2], coords[3]);
                }
            }
            points.next();
        }
        switch (drawType) {
            case 1: {
                if (traces <= 0) break;
                if (points.getWindingRule() == 0) {
                    this.cb.eoFill();
                    break;
                }
                this.cb.fill();
                break;
            }
            case 2: {
                if (traces <= 0) break;
                this.cb.stroke();
                break;
            }
            default: {
                if (traces == 0) {
                    this.cb.rectangle(0.0f, 0.0f, 0.0f, 0.0f);
                }
                if (points.getWindingRule() == 0) {
                    this.cb.eoClip();
                } else {
                    this.cb.clip();
                }
                this.cb.newPath();
            }
        }
    }

    private float normalizeY(float y) {
        return this.height - y;
    }

    private void normalizeY(float[] coords) {
        coords[1] = this.normalizeY(coords[1]);
        coords[3] = this.normalizeY(coords[3]);
        coords[5] = this.normalizeY(coords[5]);
    }

    private AffineTransform normalizeMatrix() {
        double[] mx = new double[6];
        AffineTransform result = AffineTransform.getTranslateInstance(0.0, 0.0);
        result.getMatrix(mx);
        mx[3] = -1.0;
        mx[5] = this.height;
        result = new AffineTransform(mx);
        result.concatenate(this.transform);
        return result;
    }

    private boolean drawImage(java.awt.Image img, java.awt.Image mask, AffineTransform xform, Color bgColor, ImageObserver obs) {
        PdfGState gs;
        if (xform == null) {
            return true;
        }
        xform.translate(0.0, img.getHeight(obs));
        xform.scale(img.getWidth(obs), img.getHeight(obs));
        AffineTransform inverse = this.normalizeMatrix();
        AffineTransform flipper = AffineTransform.getScaleInstance(1.0, -1.0);
        inverse.concatenate(xform);
        inverse.concatenate(flipper);
        double[] mx = new double[6];
        inverse.getMatrix(mx);
        if (this.currentFillGState != 255) {
            gs = this.fillGState[255];
            if (gs == null) {
                gs = new PdfGState();
                gs.setFillOpacity(1.0f);
                this.fillGState[255] = gs;
            }
            this.cb.setGState(gs);
        }
        try {
            Image image = null;
            if (!this.convertImagesToJPEG) {
                image = Image.getInstance(img, bgColor);
            } else {
                BufferedImage scaled = new BufferedImage(img.getWidth(null), img.getHeight(null), 1);
                Graphics2D g3 = scaled.createGraphics();
                g3.drawImage(img, 0, 0, img.getWidth(null), img.getHeight(null), null);
                g3.dispose();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(baos);
                JPEGEncodeParam param = JPEGCodec.getDefaultJPEGEncodeParam(scaled);
                param.setQuality(this.jpegQuality, true);
                encoder.encode(scaled, param);
                scaled.flush();
                scaled = null;
                image = Image.getInstance(baos.toByteArray());
            }
            if (mask != null) {
                Image msk = Image.getInstance(mask, null, true);
                msk.makeMask();
                msk.setInvertMask(true);
                image.setImageMask(msk);
            }
            this.cb.addImage(image, (float)mx[0], (float)mx[1], (float)mx[2], (float)mx[3], (float)mx[4], (float)mx[5]);
        }
        catch (Exception ex) {
            throw new IllegalArgumentException();
        }
        if (this.currentFillGState != 255) {
            gs = this.fillGState[this.currentFillGState];
            this.cb.setGState(gs);
        }
        return true;
    }

    private boolean checkNewPaint(Paint oldPaint) {
        if (this.paint == oldPaint) {
            return false;
        }
        return !(this.paint instanceof Color) || !this.paint.equals(oldPaint);
    }

    private void setFillPaint() {
        if (this.checkNewPaint(this.paintFill)) {
            this.paintFill = this.paint;
            this.setPaint(false, 0.0, 0.0, true);
        }
    }

    private void setStrokePaint() {
        if (this.checkNewPaint(this.paintStroke)) {
            this.paintStroke = this.paint;
            this.setPaint(false, 0.0, 0.0, false);
        }
    }

    private void setPaint(boolean invert, double xoffset, double yoffset, boolean fill) {
        block25 : {
            if (this.paint instanceof Color) {
                Color color = (Color)this.paint;
                int alpha = color.getAlpha();
                if (fill) {
                    if (alpha != this.currentFillGState) {
                        this.currentFillGState = alpha;
                        PdfGState gs = this.fillGState[alpha];
                        if (gs == null) {
                            gs = new PdfGState();
                            gs.setFillOpacity((float)alpha / 255.0f);
                            this.fillGState[alpha] = gs;
                        }
                        this.cb.setGState(gs);
                    }
                    this.cb.setColorFill(color);
                } else {
                    if (alpha != this.currentStrokeGState) {
                        this.currentStrokeGState = alpha;
                        PdfGState gs = this.strokeGState[alpha];
                        if (gs == null) {
                            gs = new PdfGState();
                            gs.setStrokeOpacity((float)alpha / 255.0f);
                            this.strokeGState[alpha] = gs;
                        }
                        this.cb.setGState(gs);
                    }
                    this.cb.setColorStroke(color);
                }
            } else if (this.paint instanceof GradientPaint) {
                GradientPaint gp = (GradientPaint)this.paint;
                Point2D p1 = gp.getPoint1();
                this.transform.transform(p1, p1);
                Point2D p2 = gp.getPoint2();
                this.transform.transform(p2, p2);
                Color c1 = gp.getColor1();
                Color c2 = gp.getColor2();
                PdfShading shading = PdfShading.simpleAxial(this.cb.getPdfWriter(), (float)p1.getX(), this.normalizeY((float)p1.getY()), (float)p2.getX(), this.normalizeY((float)p2.getY()), c1, c2);
                PdfShadingPattern pat = new PdfShadingPattern(shading);
                if (fill) {
                    this.cb.setShadingFill(pat);
                } else {
                    this.cb.setShadingStroke(pat);
                }
            } else if (this.paint instanceof TexturePaint) {
                try {
                    TexturePaint tp = (TexturePaint)this.paint;
                    BufferedImage img = tp.getImage();
                    Rectangle2D rect = tp.getAnchorRect();
                    Image image = Image.getInstance(img, null);
                    PdfPatternPainter pattern = this.cb.createPattern(image.width(), image.height());
                    AffineTransform inverse = this.normalizeMatrix();
                    inverse.translate(rect.getX(), rect.getY());
                    inverse.scale(rect.getWidth() / (double)image.width(), (- rect.getHeight()) / (double)image.height());
                    double[] mx = new double[6];
                    inverse.getMatrix(mx);
                    pattern.setPatternMatrix((float)mx[0], (float)mx[1], (float)mx[2], (float)mx[3], (float)mx[4], (float)mx[5]);
                    image.setAbsolutePosition(0.0f, 0.0f);
                    pattern.addImage(image);
                    if (fill) {
                        this.cb.setPatternFill(pattern);
                        break block25;
                    }
                    this.cb.setPatternStroke(pattern);
                }
                catch (Exception ex) {
                    if (fill) {
                        this.cb.setColorFill(Color.gray);
                        break block25;
                    }
                    this.cb.setColorStroke(Color.gray);
                }
            } else {
                try {
                    BufferedImage img = null;
                    int type = 6;
                    if (this.paint.getTransparency() == 1) {
                        type = 5;
                    }
                    img = new BufferedImage((int)this.width, (int)this.height, type);
                    Graphics2D g = (Graphics2D)img.getGraphics();
                    g.transform(this.transform);
                    AffineTransform inv = this.transform.createInverse();
                    Shape fillRect = new Rectangle2D.Double(0.0, 0.0, img.getWidth(), img.getHeight());
                    fillRect = inv.createTransformedShape(fillRect);
                    g.setPaint(this.paint);
                    g.fill(fillRect);
                    if (invert) {
                        AffineTransform tx = new AffineTransform();
                        tx.scale(1.0, -1.0);
                        tx.translate(- xoffset, - yoffset);
                        g.drawImage(img, tx, null);
                    }
                    Image image = Image.getInstance(img, null);
                    PdfPatternPainter pattern = this.cb.createPattern(this.width, this.height);
                    image.setAbsolutePosition(0.0f, 0.0f);
                    pattern.addImage(image);
                    if (fill) {
                        this.cb.setPatternFill(pattern);
                    } else {
                        this.cb.setPatternStroke(pattern);
                    }
                }
                catch (Exception ex) {
                    if (fill) {
                        this.cb.setColorFill(Color.gray);
                    }
                    this.cb.setColorStroke(Color.gray);
                }
            }
        }
    }

    private synchronized void waitForImage(java.awt.Image image) {
        if (this.mediaTracker == null) {
            this.mediaTracker = new MediaTracker(new fakeComponent());
        }
        this.mediaTracker.addImage(image, 0);
        try {
            this.mediaTracker.waitForID(0);
        }
        catch (InterruptedException var2_2) {
            // empty catch block
        }
        this.mediaTracker.removeImage(image);
    }

    private static class fakeComponent
    extends Component {
        fakeComponent() {
        }
    }

}

