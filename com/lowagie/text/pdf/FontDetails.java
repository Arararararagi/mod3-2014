/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.CJKFont;
import com.lowagie.text.pdf.IntHashtable;
import com.lowagie.text.pdf.PdfEncodings;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.TrueTypeFontUnicode;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

class FontDetails {
    PdfIndirectReference indirectReference;
    PdfName fontName;
    BaseFont baseFont;
    TrueTypeFontUnicode ttu;
    CJKFont cjkFont;
    byte[] shortTag;
    HashMap longTag;
    IntHashtable cjkTag;
    int fontType;
    boolean symbolic;
    protected boolean subset = true;

    FontDetails(PdfName fontName, PdfIndirectReference indirectReference, BaseFont baseFont) {
        this.fontName = fontName;
        this.indirectReference = indirectReference;
        this.baseFont = baseFont;
        this.fontType = baseFont.getFontType();
        switch (this.fontType) {
            case 0: 
            case 1: {
                this.shortTag = new byte[256];
                break;
            }
            case 2: {
                this.cjkTag = new IntHashtable();
                this.cjkFont = (CJKFont)baseFont;
                break;
            }
            case 3: {
                this.longTag = new HashMap();
                this.ttu = (TrueTypeFontUnicode)baseFont;
                this.symbolic = baseFont.isFontSpecific();
            }
        }
    }

    PdfIndirectReference getIndirectReference() {
        return this.indirectReference;
    }

    PdfName getFontName() {
        return this.fontName;
    }

    BaseFont getBaseFont() {
        return this.baseFont;
    }

    byte[] convertToBytes(String text) {
        byte[] b = null;
        switch (this.fontType) {
            case 0: 
            case 1: {
                b = this.baseFont.convertToBytes(text);
                int len = b.length;
                for (int k = 0; k < len; ++k) {
                    this.shortTag[b[k] & 255] = 1;
                }
                break;
            }
            case 2: {
                int len = text.length();
                for (int k = 0; k < len; ++k) {
                    this.cjkTag.put(this.cjkFont.getCidCode(text.charAt(k)), 0);
                }
                b = this.baseFont.convertToBytes(text);
                break;
            }
            case 4: {
                b = this.baseFont.convertToBytes(text);
                break;
            }
            case 3: {
                try {
                    int k;
                    int len = text.length();
                    int[] metrics = null;
                    char[] glyph = new char[len];
                    int i = 0;
                    if (this.symbolic) {
                        b = PdfEncodings.convertToBytes(text, "symboltt");
                        len = b.length;
                        for (k = 0; k < len; ++k) {
                            metrics = this.ttu.getMetricsTT(b[k] & 255);
                            if (metrics == null) continue;
                            this.longTag.put(new Integer(metrics[0]), new int[]{metrics[0], metrics[1], this.ttu.getUnicodeDifferences(b[k] & 255)});
                            glyph[i++] = (char)metrics[0];
                        }
                    } else {
                        for (k = 0; k < len; ++k) {
                            char c = text.charAt(k);
                            metrics = this.ttu.getMetricsTT(c);
                            if (metrics == null) continue;
                            int m0 = metrics[0];
                            Integer gl = new Integer(m0);
                            if (!this.longTag.containsKey(gl)) {
                                this.longTag.put(gl, new int[]{m0, metrics[1], c});
                            }
                            glyph[i++] = (char)m0;
                        }
                    }
                    String s = new String(glyph, 0, i);
                    b = s.getBytes("UnicodeBigUnmarked");
                    break;
                }
                catch (UnsupportedEncodingException e) {
                    throw new ExceptionConverter(e);
                }
            }
        }
        return b;
    }

    void writeFont(PdfWriter writer) {
        try {
            switch (this.fontType) {
                case 0: 
                case 1: {
                    int firstChar;
                    int lastChar;
                    if (this.subset) {
                        for (firstChar = 0; firstChar < 256; ++firstChar) {
                            if (this.shortTag[firstChar] != 0) break;
                        }
                        for (lastChar = 255; lastChar >= firstChar; --lastChar) {
                            if (this.shortTag[lastChar] != 0) break;
                        }
                        if (firstChar > 255) {
                            firstChar = 255;
                            lastChar = 255;
                        }
                    } else {
                        for (int k = 0; k < this.shortTag.length; ++k) {
                            this.shortTag[k] = 1;
                        }
                        firstChar = 0;
                        lastChar = this.shortTag.length - 1;
                    }
                    this.baseFont.writeFont(writer, this.indirectReference, new Object[]{new Integer(firstChar), new Integer(lastChar), this.shortTag});
                    break;
                }
                case 2: {
                    this.baseFont.writeFont(writer, this.indirectReference, new Object[]{this.cjkTag});
                    break;
                }
                case 3: {
                    this.baseFont.writeFont(writer, this.indirectReference, new Object[]{this.longTag});
                }
            }
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    public boolean isSubset() {
        return this.subset;
    }

    public void setSubset(boolean subset) {
        this.subset = subset;
    }
}

