/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.SimpleXMLDocHandler;
import com.lowagie.text.pdf.SimpleXMLParser;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Stack;

public class XfdfReader
implements SimpleXMLDocHandler {
    private boolean foundRoot = false;
    private Stack fieldNames = new Stack();
    private Stack fieldValues = new Stack();
    HashMap fields;
    String fileSpec;

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     */
    public XfdfReader(String filename) throws IOException {
        FileInputStream fin = null;
        try {
            fin = new FileInputStream(filename);
            SimpleXMLParser.parse((SimpleXMLDocHandler)this, fin);
            Object var3_5 = null;
        }
        catch (Throwable var4_3) {
            Object var3_4 = null;
            try {
                fin.close();
                throw var4_3;
            }
            catch (Exception e) {
                // empty catch block
            }
            throw var4_3;
        }
        try {}
        catch (Exception e) {
            return;
        }
        fin.close();
    }

    public XfdfReader(byte[] xfdfIn) throws IOException {
        SimpleXMLParser.parse((SimpleXMLDocHandler)this, new ByteArrayInputStream(xfdfIn));
    }

    public HashMap getFields() {
        return this.fields;
    }

    public String getField(String name) {
        return (String)this.fields.get(name);
    }

    public String getFieldValue(String name) {
        String field = (String)this.fields.get(name);
        if (field == null) {
            return null;
        }
        return field;
    }

    public String getFileSpec() {
        return this.fileSpec;
    }

    public void startElement(String tag, HashMap h) {
        if (!this.foundRoot) {
            if (!tag.equals("xfdf")) {
                throw new RuntimeException("Root element is not Bookmark.");
            }
            this.foundRoot = true;
        }
        if (!tag.equals("xfdf")) {
            if (tag.equals("f")) {
                this.fileSpec = (String)h.get("href");
            } else if (tag.equals("fields")) {
                this.fields = new HashMap();
            } else if (tag.equals("field")) {
                String fName = (String)h.get("name");
                this.fieldNames.push(fName);
            } else if (tag.equals("value")) {
                this.fieldValues.push("");
            }
        }
    }

    public void endElement(String tag) {
        if (tag.equals("value")) {
            String fName = "";
            for (int k = 0; k < this.fieldNames.size(); ++k) {
                fName = String.valueOf(fName) + "." + (String)this.fieldNames.elementAt(k);
            }
            if (fName.startsWith(".")) {
                fName = fName.substring(1);
            }
            String fVal = (String)this.fieldValues.pop();
            this.fields.put(fName, fVal);
        } else if (tag.equals("field") && !this.fieldNames.isEmpty()) {
            this.fieldNames.pop();
        }
    }

    public void startDocument() {
        this.fileSpec = new String("");
    }

    public void endDocument() {
    }

    public void text(String str) {
        if (this.fieldNames.isEmpty() || this.fieldValues.isEmpty()) {
            return;
        }
        String val = (String)this.fieldValues.pop();
        val = String.valueOf(val) + str;
        this.fieldValues.push(val);
    }
}

