/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocWriter;
import com.lowagie.text.Document;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.pdf.OutputStreamCounter;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfEncryption;
import com.lowagie.text.pdf.PdfEncryptionStream;
import com.lowagie.text.pdf.PdfIndirectObject;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

public class PdfStream
extends PdfDictionary {
    protected boolean compressed = false;
    protected ByteArrayOutputStream streamBytes = null;
    protected InputStream inputStream;
    protected PdfIndirectReference ref;
    protected int inputStreamLength = -1;
    protected PdfWriter writer;
    static final byte[] STARTSTREAM = DocWriter.getISOBytes("stream\n");
    static final byte[] ENDSTREAM = DocWriter.getISOBytes("\nendstream");
    static final int SIZESTREAM = STARTSTREAM.length + ENDSTREAM.length;

    public PdfStream(byte[] bytes) {
        this.type = 7;
        this.bytes = bytes;
        this.put(PdfName.LENGTH, new PdfNumber(bytes.length));
    }

    public PdfStream(InputStream inputStream, PdfWriter writer) {
        this.type = 7;
        this.inputStream = inputStream;
        this.writer = writer;
        this.ref = writer.getPdfIndirectReference();
        this.put(PdfName.LENGTH, this.ref);
    }

    protected PdfStream() {
        this.type = 7;
    }

    public void writeLength() throws IOException {
        if (this.inputStream == null) {
            throw new UnsupportedOperationException("writeLength() can only be called in a contructed PdfStream(InputStream,PdfWriter).");
        }
        if (this.inputStreamLength == -1) {
            throw new IOException("writeLength() can only be called after output of the stream body.");
        }
        this.writer.addToBody((PdfObject)new PdfNumber(this.inputStreamLength), this.ref, false);
    }

    public void flateCompress() {
        if (!Document.compress) {
            return;
        }
        if (this.compressed) {
            return;
        }
        if (this.inputStream != null) {
            this.compressed = true;
            return;
        }
        PdfObject filter = this.get(PdfName.FILTER);
        if (filter != null) {
            if (filter.isName() && ((PdfName)filter).compareTo(PdfName.FLATEDECODE) == 0) {
                return;
            }
            if (filter.isArray() && ((PdfArray)filter).contains(PdfName.FLATEDECODE)) {
                return;
            }
            throw new RuntimeException("Stream could not be compressed: filter is not a name or array.");
        }
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            DeflaterOutputStream zip = new DeflaterOutputStream(stream);
            if (this.streamBytes != null) {
                this.streamBytes.writeTo(zip);
            } else {
                zip.write(this.bytes);
            }
            zip.close();
            this.streamBytes = stream;
            this.bytes = null;
            this.put(PdfName.LENGTH, new PdfNumber(this.streamBytes.size()));
            if (filter == null) {
                this.put(PdfName.FILTER, PdfName.FLATEDECODE);
            } else {
                PdfArray filters = new PdfArray(filter);
                filters.add(PdfName.FLATEDECODE);
                this.put(PdfName.FILTER, filters);
            }
            this.compressed = true;
        }
        catch (IOException ioe) {
            throw new ExceptionConverter(ioe);
        }
    }

    protected void superToPdf(PdfWriter writer, OutputStream os) throws IOException {
        super.toPdf(writer, os);
    }

    public void toPdf(PdfWriter writer, OutputStream os) throws IOException {
        if (this.inputStream != null && this.compressed) {
            this.put(PdfName.FILTER, PdfName.FLATEDECODE);
        }
        this.superToPdf(writer, os);
        os.write(STARTSTREAM);
        PdfEncryption crypto = null;
        if (writer != null) {
            crypto = writer.getEncryption();
        }
        if (crypto != null) {
            crypto.prepareKey();
        }
        if (this.inputStream != null) {
            int n;
            OutputStreamCounter osc;
            DeflaterOutputStream def = null;
            PdfEncryptionStream encs = null;
            OutputStream fout = osc = new OutputStreamCounter(os);
            if (crypto != null) {
                fout = encs = new PdfEncryptionStream(fout, crypto);
            }
            if (this.compressed) {
                fout = def = new DeflaterOutputStream(fout, new Deflater(9), 32768);
            }
            byte[] buf = new byte[65536];
            while ((n = this.inputStream.read(buf)) > 0) {
                fout.write(buf, 0, n);
            }
            if (def != null) {
                def.finish();
            }
            this.inputStreamLength = osc.getCounter();
        } else if (crypto == null) {
            if (this.streamBytes != null) {
                this.streamBytes.writeTo(os);
            } else {
                os.write(this.bytes);
            }
        } else {
            byte[] b;
            if (this.streamBytes != null) {
                b = this.streamBytes.toByteArray();
                crypto.encryptRC4(b);
            } else {
                b = new byte[this.bytes.length];
                crypto.encryptRC4(this.bytes, b);
            }
            os.write(b);
        }
        os.write(ENDSTREAM);
    }
}

