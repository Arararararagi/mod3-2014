/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.ExtendedColor;
import com.lowagie.text.pdf.PdfSpotColor;
import java.awt.Color;

public class SpotColor
extends ExtendedColor {
    PdfSpotColor spot;
    float tint;

    public SpotColor(PdfSpotColor spot, float tint) {
        super(3, (float)spot.getAlternativeCS().getRed() / 255.0f * tint, (float)spot.getAlternativeCS().getGreen() / 255.0f * tint, (float)spot.getAlternativeCS().getBlue() / 255.0f * tint);
        this.spot = spot;
        this.tint = tint;
    }

    public SpotColor(PdfSpotColor spot) {
        this(spot, spot.getTint());
    }

    public PdfSpotColor getPdfSpotColor() {
        return this.spot;
    }

    public float getTint() {
        return this.tint;
    }
}

