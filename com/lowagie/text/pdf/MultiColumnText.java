/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ElementListener;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfDocument;
import java.util.ArrayList;

public class MultiColumnText
implements Element {
    public static final float AUTOMATIC = -1.0f;
    private float desiredHeight;
    private float totalHeight;
    private boolean overflow;
    private float top;
    private float pageBottom;
    private ColumnText columnText;
    private ArrayList columnDefs = new ArrayList();
    private boolean simple = true;
    private int currentColumn = 0;
    private float nextY = -1.0f;
    private boolean columnsRightToLeft = false;
    private PdfDocument document;

    public MultiColumnText() {
        this(-1.0f);
    }

    public MultiColumnText(float height) {
        this.desiredHeight = height;
        this.top = -1.0f;
        this.columnText = new ColumnText(null);
        this.totalHeight = 0.0f;
    }

    public boolean isOverflow() {
        return this.overflow;
    }

    public void useColumnParams(ColumnText sourceColumn) {
        this.columnText.setSimpleVars(sourceColumn);
    }

    public void addColumn(float[] left, float[] right) {
        ColumnDef nextDef = new ColumnDef(this, left, right);
        this.simple = nextDef.isSimple();
        this.columnDefs.add(nextDef);
    }

    public void addSimpleColumn(float left, float right) {
        ColumnDef newCol = new ColumnDef(this, left, right);
        this.columnDefs.add(newCol);
    }

    public void addRegularColumns(float left, float right, float gutterWidth, int numColumns) {
        float currX = left;
        float width = right - left;
        float colWidth = (width - gutterWidth * (float)(numColumns - 1)) / (float)numColumns;
        for (int i = 0; i < numColumns; ++i) {
            this.addSimpleColumn(currX, currX + colWidth);
            currX+=colWidth + gutterWidth;
        }
    }

    public void addElement(Element element) throws DocumentException {
        if (this.simple) {
            this.columnText.addElement(element);
        } else if (element instanceof Phrase) {
            this.columnText.addText((Phrase)element);
        } else if (element instanceof Chunk) {
            this.columnText.addText((Chunk)element);
        } else {
            throw new DocumentException("Can't add " + element.getClass() + " to MultiColumnText with complex columns");
        }
    }

    public float write(PdfContentByte canvas, PdfDocument document, float documentY) throws DocumentException {
        float currentHeight;
        this.document = document;
        this.columnText.setCanvas(canvas);
        if (this.columnDefs.size() == 0) {
            throw new DocumentException("MultiColumnText has no columns");
        }
        this.overflow = false;
        this.pageBottom = document.bottom();
        currentHeight = 0.0f;
        boolean done = false;
        try {
            while (!done) {
                if (this.nextY == -1.0f) {
                    this.nextY = documentY;
                }
                if (this.top == -1.0f) {
                    this.top = documentY;
                }
                ColumnDef currentDef = (ColumnDef)this.columnDefs.get(this.getCurrentColumn());
                this.columnText.setYLine(this.top);
                float[] left = currentDef.resolvePositions(4);
                float[] right = currentDef.resolvePositions(8);
                currentHeight = Math.max(currentHeight, this.getHeight(left, right));
                if (currentDef.isSimple()) {
                    this.columnText.setSimpleColumn(left[2], left[3], right[0], right[1]);
                } else {
                    this.columnText.setColumns(left, right);
                }
                int result = this.columnText.go();
                if ((result & 1) != 0) {
                    done = true;
                    this.top = this.columnText.getYLine();
                    continue;
                }
                if (this.shiftCurrentColumn()) {
                    this.top = this.nextY;
                    continue;
                }
                this.totalHeight+=currentHeight;
                if (this.desiredHeight != -1.0f && this.totalHeight >= this.desiredHeight) {
                    this.overflow = true;
                    break;
                }
                this.newPage();
                currentHeight = 0.0f;
            }
        }
        catch (DocumentException ex) {
            ex.printStackTrace();
            throw ex;
        }
        return currentHeight;
    }

    private void newPage() throws DocumentException {
        this.resetCurrentColumn();
        this.nextY = -1.0f;
        this.top = -1.0f;
        if (this.document != null) {
            this.document.newPage();
        }
    }

    private float getHeight(float[] left, float[] right) {
        int i;
        float max = 1.4E-45f;
        float min = 3.4028235E38f;
        for (i = 0; i < left.length; i+=2) {
            min = Math.min(min, left[i + 1]);
            max = Math.max(max, left[i + 1]);
        }
        for (i = 0; i < right.length; i+=2) {
            min = Math.min(min, right[i + 1]);
            max = Math.max(max, right[i + 1]);
        }
        return max - min;
    }

    public boolean process(ElementListener listener) {
        try {
            return listener.add(this);
        }
        catch (DocumentException de) {
            return false;
        }
    }

    public int type() {
        return 40;
    }

    public ArrayList getChunks() {
        return null;
    }

    private float getColumnBottom() {
        if (this.desiredHeight == -1.0f) {
            return this.pageBottom;
        }
        return Math.max(this.top - (this.desiredHeight - this.totalHeight), this.pageBottom);
    }

    public void nextColumn() throws DocumentException {
        this.currentColumn = (this.currentColumn + 1) % this.columnDefs.size();
        this.top = this.nextY;
        if (this.currentColumn == 0) {
            this.newPage();
        }
    }

    public int getCurrentColumn() {
        if (this.columnsRightToLeft) {
            return this.columnDefs.size() - this.currentColumn - 1;
        }
        return this.currentColumn;
    }

    public void resetCurrentColumn() {
        this.currentColumn = 0;
    }

    public boolean shiftCurrentColumn() {
        if (this.currentColumn + 1 < this.columnDefs.size()) {
            ++this.currentColumn;
            return true;
        }
        return false;
    }

    public void setColumnsRightToLeft(boolean direction) {
        this.columnsRightToLeft = direction;
    }

    private class ColumnDef {
        private float[] left;
        private float[] right;
        final /* synthetic */ MultiColumnText this$0;

        ColumnDef(MultiColumnText multiColumnText, float[] newLeft, float[] newRight) {
            this.this$0 = multiColumnText;
            this.left = newLeft;
            this.right = newRight;
        }

        ColumnDef(MultiColumnText multiColumnText, float leftPosition, float rightPosition) {
            this.this$0 = multiColumnText;
            this.left = new float[4];
            this.left[0] = leftPosition;
            this.left[1] = multiColumnText.top;
            this.left[2] = leftPosition;
            this.left[3] = multiColumnText.desiredHeight == -1.0f || multiColumnText.top == -1.0f ? -1.0f : multiColumnText.top - multiColumnText.desiredHeight;
            this.right = new float[4];
            this.right[0] = rightPosition;
            this.right[1] = multiColumnText.top;
            this.right[2] = rightPosition;
            this.right[3] = multiColumnText.desiredHeight == -1.0f || multiColumnText.top == -1.0f ? -1.0f : multiColumnText.top - multiColumnText.desiredHeight;
        }

        float[] resolvePositions(int side) {
            if (side == 4) {
                return this.resolvePositions(this.left);
            }
            return this.resolvePositions(this.right);
        }

        private float[] resolvePositions(float[] positions) {
            if (!this.isSimple()) {
                return positions;
            }
            if (this.this$0.top == -1.0f) {
                throw new RuntimeException("resolvePositions called with top=AUTOMATIC (-1).  Top position must be set befure lines can be resolved");
            }
            positions[1] = this.this$0.top;
            positions[3] = this.this$0.getColumnBottom();
            return positions;
        }

        private boolean isSimple() {
            if (this.left.length == 4 && this.right.length == 4 && this.left[0] == this.left[2] && this.right[0] == this.right[2]) {
                return true;
            }
            return false;
        }
    }

}

