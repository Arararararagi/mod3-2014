/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.PRTokeniser;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfString;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class FdfReader
extends PdfReader {
    HashMap fields;
    String fileSpec;
    PdfName encoding;

    public FdfReader(String filename) throws IOException {
        super(filename);
    }

    public FdfReader(byte[] pdfIn) throws IOException {
        super(pdfIn);
    }

    public FdfReader(URL url) throws IOException {
        super(url);
    }

    public FdfReader(InputStream is) throws IOException {
        super(is);
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    protected void readPdf() throws IOException {
        this.fields = new HashMap<K, V>();
        try {
            this.tokens.checkFdfHeader();
            this.rebuildXref();
            this.readDocObj();
            var1_3 = null;
        }
        catch (Throwable var2_1) {
            var1_2 = null;
            try {
                this.tokens.close();
                throw var2_1;
            }
            catch (Exception e) {
                // empty catch block
            }
            throw var2_1;
        }
        ** try [egrp 1[TRYBLOCK] [2 : 36->46)] { 
lbl19: // 1 sources:
        this.tokens.close();
        ** GOTO lbl23
lbl21: // 1 sources:
        catch (Exception e) {
            // empty catch block
        }
lbl23: // 2 sources:
        this.readFields();
    }

    protected void kidNode(PdfDictionary merged, String name) {
        PdfArray kids = (PdfArray)PdfReader.getPdfObject(merged.get(PdfName.KIDS));
        if (kids == null || kids.getArrayList().size() == 0) {
            if (name.length() > 0) {
                name = name.substring(1);
            }
            this.fields.put(name, merged);
        } else {
            merged.remove(PdfName.KIDS);
            ArrayList ar = kids.getArrayList();
            for (int k = 0; k < ar.size(); ++k) {
                PdfDictionary dic = new PdfDictionary();
                dic.merge(merged);
                PdfDictionary newDic = (PdfDictionary)PdfReader.getPdfObject((PdfObject)ar.get(k));
                PdfString t = (PdfString)PdfReader.getPdfObject(newDic.get(PdfName.T));
                String newName = name;
                if (t != null) {
                    newName = String.valueOf(newName) + "." + t.toUnicodeString();
                }
                dic.merge(newDic);
                dic.remove(PdfName.T);
                this.kidNode(dic, newName);
            }
        }
    }

    protected void readFields() throws IOException {
        PdfArray fld;
        this.catalog = (PdfDictionary)PdfReader.getPdfObject(this.trailer.get(PdfName.ROOT));
        PdfDictionary fdf = (PdfDictionary)PdfReader.getPdfObject(this.catalog.get(PdfName.FDF));
        PdfString fs = (PdfString)PdfReader.getPdfObject(fdf.get(PdfName.F));
        if (fs != null) {
            this.fileSpec = fs.toUnicodeString();
        }
        if ((fld = (PdfArray)PdfReader.getPdfObject(fdf.get(PdfName.FIELDS))) == null) {
            return;
        }
        this.encoding = (PdfName)PdfReader.getPdfObject(fdf.get(PdfName.ENCODING));
        PdfDictionary merged = new PdfDictionary();
        merged.put(PdfName.KIDS, fld);
        this.kidNode(merged, "");
    }

    public HashMap getFields() {
        return this.fields;
    }

    public PdfDictionary getField(String name) {
        return (PdfDictionary)this.fields.get(name);
    }

    public String getFieldValue(String name) {
        PdfDictionary field = (PdfDictionary)this.fields.get(name);
        if (field == null) {
            return null;
        }
        PdfObject v = PdfReader.getPdfObject(field.get(PdfName.V));
        if (v == null) {
            return null;
        }
        if (v.isName()) {
            return PdfName.decodeName(((PdfName)v).toString());
        }
        if (v.isString()) {
            PdfString vs = (PdfString)v;
            if (this.encoding == null || vs.getEncoding() != null) {
                return vs.toUnicodeString();
            }
            byte[] b = vs.getBytes();
            if (b.length >= 2 && b[0] == -2 && b[1] == -1) {
                return vs.toUnicodeString();
            }
            try {
                if (this.encoding.equals(PdfName.SHIFT_JIS)) {
                    return new String(b, "SJIS");
                }
                if (this.encoding.equals(PdfName.UHC)) {
                    return new String(b, "MS949");
                }
                if (this.encoding.equals(PdfName.GBK)) {
                    return new String(b, "GBK");
                }
                if (this.encoding.equals(PdfName.BIGFIVE)) {
                    return new String(b, "Big5");
                }
            }
            catch (Exception var6_6) {
                // empty catch block
            }
            return vs.toUnicodeString();
        }
        return null;
    }

    public String getFileSpec() {
        return this.fileSpec;
    }
}

