/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class IntHashtable
implements Cloneable {
    private IntHashtableEntry[] table;
    private int count;
    private int threshold;
    private float loadFactor;

    public IntHashtable(int initialCapacity, float loadFactor) {
        if (initialCapacity <= 0 || (double)loadFactor <= 0.0) {
            throw new IllegalArgumentException();
        }
        this.loadFactor = loadFactor;
        this.table = new IntHashtableEntry[initialCapacity];
        this.threshold = (int)((float)initialCapacity * loadFactor);
    }

    public IntHashtable(int initialCapacity) {
        this(initialCapacity, 0.75f);
    }

    public IntHashtable() {
        this(101, 0.75f);
    }

    public int size() {
        return this.count;
    }

    public boolean isEmpty() {
        if (this.count == 0) {
            return true;
        }
        return false;
    }

    public boolean contains(int value) {
        IntHashtableEntry[] tab = this.table;
        int i = tab.length;
        while (i-- > 0) {
            IntHashtableEntry e = tab[i];
            while (e != null) {
                if (e.value == value) {
                    return true;
                }
                e = e.next;
            }
        }
        return false;
    }

    public boolean containsKey(int key) {
        IntHashtableEntry[] tab = this.table;
        int hash = key;
        int index = (hash & Integer.MAX_VALUE) % tab.length;
        IntHashtableEntry e = tab[index];
        while (e != null) {
            if (e.hash == hash && e.key == key) {
                return true;
            }
            e = e.next;
        }
        return false;
    }

    public int get(int key) {
        IntHashtableEntry[] tab = this.table;
        int hash = key;
        int index = (hash & Integer.MAX_VALUE) % tab.length;
        IntHashtableEntry e = tab[index];
        while (e != null) {
            if (e.hash == hash && e.key == key) {
                return e.value;
            }
            e = e.next;
        }
        return 0;
    }

    protected void rehash() {
        int oldCapacity = this.table.length;
        IntHashtableEntry[] oldTable = this.table;
        int newCapacity = oldCapacity * 2 + 1;
        IntHashtableEntry[] newTable = new IntHashtableEntry[newCapacity];
        this.threshold = (int)((float)newCapacity * this.loadFactor);
        this.table = newTable;
        int i = oldCapacity;
        while (i-- > 0) {
            IntHashtableEntry old = oldTable[i];
            while (old != null) {
                IntHashtableEntry e = old;
                old = old.next;
                int index = (e.hash & Integer.MAX_VALUE) % newCapacity;
                e.next = newTable[index];
                newTable[index] = e;
            }
        }
    }

    public int put(int key, int value) {
        IntHashtableEntry[] tab = this.table;
        int hash = key;
        int index = (hash & Integer.MAX_VALUE) % tab.length;
        IntHashtableEntry e = tab[index];
        while (e != null) {
            if (e.hash == hash && e.key == key) {
                int old = e.value;
                e.value = value;
                return old;
            }
            e = e.next;
        }
        if (this.count >= this.threshold) {
            this.rehash();
            return this.put(key, value);
        }
        e = new IntHashtableEntry();
        e.hash = hash;
        e.key = key;
        e.value = value;
        e.next = tab[index];
        tab[index] = e;
        ++this.count;
        return 0;
    }

    public int remove(int key) {
        IntHashtableEntry[] tab = this.table;
        int hash = key;
        int index = (hash & Integer.MAX_VALUE) % tab.length;
        IntHashtableEntry e = tab[index];
        IntHashtableEntry prev = null;
        while (e != null) {
            if (e.hash == hash && e.key == key) {
                if (prev != null) {
                    prev.next = e.next;
                } else {
                    tab[index] = e.next;
                }
                --this.count;
                return e.value;
            }
            prev = e;
            e = e.next;
        }
        return 0;
    }

    public void clear() {
        IntHashtableEntry[] tab = this.table;
        int index = tab.length;
        while (--index >= 0) {
            tab[index] = null;
        }
        this.count = 0;
    }

    public Object clone() {
        try {
            IntHashtable t = (IntHashtable)super.clone();
            t.table = new IntHashtableEntry[this.table.length];
            int i = this.table.length;
            while (i-- > 0) {
                IntHashtableEntry intHashtableEntry = t.table[i] = this.table[i] != null ? (IntHashtableEntry)this.table[i].clone() : null;
            }
            return t;
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }

    public int[] toOrderedKeys() {
        int[] res = this.getKeys();
        Arrays.sort(res);
        return res;
    }

    public int[] getKeys() {
        int[] res = new int[this.count];
        int ptr = 0;
        int index = this.table.length;
        IntHashtableEntry entry = null;
        do {
            if (entry == null) {
                while (index-- > 0 && (entry = this.table[index]) == null) {
                }
            }
            if (entry == null) break;
            IntHashtableEntry e = entry;
            entry = e.next;
            res[ptr++] = e.key;
        } while (true);
        return res;
    }

    public int getOneKey() {
        if (this.count == 0) {
            return 0;
        }
        int index = this.table.length;
        IntHashtableEntry entry = null;
        while (index-- > 0 && (entry = this.table[index]) == null) {
        }
        if (entry == null) {
            return 0;
        }
        return entry.key;
    }

    public Iterator getEntryIterator() {
        return new IntHashtableIterator(this.table);
    }

    static class IntHashtableEntry {
        int hash;
        int key;
        int value;
        IntHashtableEntry next;

        IntHashtableEntry() {
        }

        public int getKey() {
            return this.key;
        }

        public int getValue() {
            return this.value;
        }

        protected Object clone() {
            IntHashtableEntry entry = new IntHashtableEntry();
            entry.hash = this.hash;
            entry.key = this.key;
            entry.value = this.value;
            entry.next = this.next != null ? (IntHashtableEntry)this.next.clone() : null;
            return entry;
        }
    }

    static class IntHashtableIterator
    implements Iterator {
        int index;
        IntHashtableEntry[] table;
        IntHashtableEntry entry;

        IntHashtableIterator(IntHashtableEntry[] table) {
            this.table = table;
            this.index = table.length;
        }

        /*
         * Unable to fully structure code
         * Enabled aggressive block sorting
         * Lifted jumps to return sites
         */
        public boolean hasNext() {
            if (this.entry == null) ** GOTO lbl6
            return true;
lbl-1000: // 1 sources:
            {
                this.entry = this.table[this.index];
                if (this.entry == null) continue;
                return true;
lbl6: // 2 sources:
                ** while (this.index-- > 0)
            }
lbl7: // 1 sources:
            return false;
        }

        public Object next() {
            if (this.entry == null) {
                while (this.index-- > 0 && (this.entry = this.table[this.index]) == null) {
                }
            }
            if (this.entry != null) {
                IntHashtableEntry e = this.entry;
                this.entry = e.next;
                return this.entry;
            }
            throw new NoSuchElementException("IntHashtableIterator");
        }

        public void remove() {
            throw new UnsupportedOperationException("remove() not supported.");
        }
    }

}

