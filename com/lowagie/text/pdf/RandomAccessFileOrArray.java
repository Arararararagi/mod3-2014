/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.BaseFont;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URL;

public class RandomAccessFileOrArray
implements DataInput {
    RandomAccessFile rf;
    String filename;
    byte[] arrayIn;
    int arrayInPtr;
    byte back;
    boolean isBack = false;
    private int startOffset = 0;

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public RandomAccessFileOrArray(String filename) throws IOException {
        File file = new File(filename);
        if (!file.canRead()) {
            if (filename.startsWith("file:/") || filename.startsWith("http://") || filename.startsWith("https://") || filename.startsWith("jar:")) {
                InputStream is = new URL(filename).openStream();
                try {
                    this.arrayIn = this.InputStreamToArray(is);
                    Object var4_5 = null;
                }
                catch (Throwable var5_13) {
                    Object var4_6 = null;
                    try {
                        is.close();
                        throw var5_13;
                    }
                    catch (IOException ioe) {
                        // empty catch block
                    }
                    throw var5_13;
                }
                try {
                    is.close();
                    return;
                }
                catch (IOException ioe) {
                    // empty catch block
                }
                return;
            }
            InputStream is = BaseFont.getResourceStream(filename);
            if (is == null) {
                throw new IOException(String.valueOf(filename) + " not found as file or resource.");
            }
            try {
                this.arrayIn = this.InputStreamToArray(is);
                Object var4_7 = null;
            }
            catch (Throwable var5_14) {
                Object var4_8 = null;
                try {
                    is.close();
                    throw var5_14;
                }
                catch (IOException ioe) {
                    // empty catch block
                }
                throw var5_14;
            }
            try {
                is.close();
                return;
            }
            catch (IOException ioe) {
                // empty catch block
            }
            return;
        }
        this.filename = filename;
        this.rf = new RandomAccessFile(filename, "r");
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     */
    public RandomAccessFileOrArray(URL url) throws IOException {
        InputStream is = url.openStream();
        try {
            this.arrayIn = this.InputStreamToArray(is);
            Object var3_5 = null;
        }
        catch (Throwable var4_3) {
            Object var3_4 = null;
            try {
                is.close();
                throw var4_3;
            }
            catch (IOException ioe) {
                // empty catch block
            }
            throw var4_3;
        }
        try {}
        catch (IOException ioe) {
            return;
        }
        is.close();
    }

    public RandomAccessFileOrArray(InputStream is) throws IOException {
        this.arrayIn = this.InputStreamToArray(is);
    }

    public byte[] InputStreamToArray(InputStream is) throws IOException {
        int read;
        byte[] b = new byte[8192];
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        while ((read = is.read(b)) >= 1) {
            out.write(b, 0, read);
        }
        return out.toByteArray();
    }

    public RandomAccessFileOrArray(byte[] arrayIn) {
        this.arrayIn = arrayIn;
    }

    public RandomAccessFileOrArray(RandomAccessFileOrArray file) {
        this.filename = file.filename;
        this.arrayIn = file.arrayIn;
        this.startOffset = file.startOffset;
    }

    public void pushBack(byte b) {
        this.back = b;
        this.isBack = true;
    }

    public int read() throws IOException {
        if (this.isBack) {
            this.isBack = false;
            return this.back & 255;
        }
        if (this.arrayIn == null) {
            return this.rf.read();
        }
        if (this.arrayInPtr >= this.arrayIn.length) {
            return -1;
        }
        return this.arrayIn[this.arrayInPtr++] & 255;
    }

    public int read(byte[] b, int off, int len) throws IOException {
        if (len == 0) {
            return 0;
        }
        int n = 0;
        if (this.isBack) {
            this.isBack = false;
            if (len == 1) {
                b[off] = this.back;
                return 1;
            }
            n = 1;
            b[off++] = this.back;
            --len;
        }
        if (this.arrayIn == null) {
            return this.rf.read(b, off, len) + n;
        }
        if (this.arrayInPtr >= this.arrayIn.length) {
            return -1;
        }
        if (this.arrayInPtr + len > this.arrayIn.length) {
            len = this.arrayIn.length - this.arrayInPtr;
        }
        System.arraycopy(this.arrayIn, this.arrayInPtr, b, off, len);
        this.arrayInPtr+=len;
        return len + n;
    }

    public int read(byte[] b) throws IOException {
        return this.read(b, 0, b.length);
    }

    public void readFully(byte[] b) throws IOException {
        this.readFully(b, 0, b.length);
    }

    public void readFully(byte[] b, int off, int len) throws IOException {
        int count;
        int n = 0;
        do {
            if ((count = this.read(b, off + n, len - n)) >= 0) continue;
            throw new EOFException();
        } while ((n+=count) < len);
    }

    public long skip(long n) throws IOException {
        return this.skipBytes((int)n);
    }

    public int skipBytes(int n) throws IOException {
        int pos;
        int len;
        int newpos;
        if (n <= 0) {
            return 0;
        }
        int adj = 0;
        if (this.isBack) {
            this.isBack = false;
            if (n == 1) {
                return 1;
            }
            --n;
            adj = 1;
        }
        if ((newpos = (pos = this.getFilePointer()) + n) > (len = this.length())) {
            newpos = len;
        }
        this.seek(newpos);
        return newpos - pos + adj;
    }

    public void reOpen() throws IOException {
        if (this.filename != null && this.rf == null) {
            this.rf = new RandomAccessFile(this.filename, "r");
        }
        this.seek(0);
    }

    protected void insureOpen() throws IOException {
        if (this.filename != null && this.rf == null) {
            this.reOpen();
        }
    }

    public boolean isOpen() {
        if (this.filename != null && this.rf == null) {
            return false;
        }
        return true;
    }

    public void close() throws IOException {
        this.isBack = false;
        if (this.rf != null) {
            this.rf.close();
            this.rf = null;
        }
    }

    public int length() throws IOException {
        if (this.arrayIn == null) {
            this.insureOpen();
            return (int)this.rf.length() - this.startOffset;
        }
        return this.arrayIn.length - this.startOffset;
    }

    public void seek(int pos) throws IOException {
        pos+=this.startOffset;
        this.isBack = false;
        if (this.arrayIn == null) {
            this.insureOpen();
            this.rf.seek(pos);
        } else {
            this.arrayInPtr = pos;
        }
    }

    public void seek(long pos) throws IOException {
        this.seek((int)pos);
    }

    public int getFilePointer() throws IOException {
        int n;
        this.insureOpen();
        int n2 = n = this.isBack ? 1 : 0;
        if (this.arrayIn == null) {
            return (int)this.rf.getFilePointer() - n - this.startOffset;
        }
        return this.arrayInPtr - n - this.startOffset;
    }

    public boolean readBoolean() throws IOException {
        int ch = this.read();
        if (ch < 0) {
            throw new EOFException();
        }
        if (ch != 0) {
            return true;
        }
        return false;
    }

    public byte readByte() throws IOException {
        int ch = this.read();
        if (ch < 0) {
            throw new EOFException();
        }
        return (byte)ch;
    }

    public int readUnsignedByte() throws IOException {
        int ch = this.read();
        if (ch < 0) {
            throw new EOFException();
        }
        return ch;
    }

    public short readShort() throws IOException {
        int ch2;
        int ch1 = this.read();
        if ((ch1 | (ch2 = this.read())) < 0) {
            throw new EOFException();
        }
        return (short)((ch1 << 8) + ch2);
    }

    public final short readShortLE() throws IOException {
        int ch2;
        int ch1 = this.read();
        if ((ch1 | (ch2 = this.read())) < 0) {
            throw new EOFException();
        }
        return (short)((ch2 << 8) + (ch1 << 0));
    }

    public int readUnsignedShort() throws IOException {
        int ch2;
        int ch1 = this.read();
        if ((ch1 | (ch2 = this.read())) < 0) {
            throw new EOFException();
        }
        return (ch1 << 8) + ch2;
    }

    public final int readUnsignedShortLE() throws IOException {
        int ch2;
        int ch1 = this.read();
        if ((ch1 | (ch2 = this.read())) < 0) {
            throw new EOFException();
        }
        return (ch2 << 8) + (ch1 << 0);
    }

    public char readChar() throws IOException {
        int ch2;
        int ch1 = this.read();
        if ((ch1 | (ch2 = this.read())) < 0) {
            throw new EOFException();
        }
        return (char)((ch1 << 8) + ch2);
    }

    public final char readCharLE() throws IOException {
        int ch2;
        int ch1 = this.read();
        if ((ch1 | (ch2 = this.read())) < 0) {
            throw new EOFException();
        }
        return (char)((ch2 << 8) + (ch1 << 0));
    }

    public int readInt() throws IOException {
        int ch2;
        int ch3;
        int ch4;
        int ch1 = this.read();
        if ((ch1 | (ch2 = this.read()) | (ch3 = this.read()) | (ch4 = this.read())) < 0) {
            throw new EOFException();
        }
        return (ch1 << 24) + (ch2 << 16) + (ch3 << 8) + ch4;
    }

    public final int readIntLE() throws IOException {
        int ch2;
        int ch3;
        int ch4;
        int ch1 = this.read();
        if ((ch1 | (ch2 = this.read()) | (ch3 = this.read()) | (ch4 = this.read())) < 0) {
            throw new EOFException();
        }
        return (ch4 << 24) + (ch3 << 16) + (ch2 << 8) + (ch1 << 0);
    }

    public final long readUnsignedInt() throws IOException {
        long ch4;
        long ch2;
        long ch3;
        long ch1 = this.read();
        if ((ch1 | (ch2 = (long)this.read()) | (ch3 = (long)this.read()) | (ch4 = (long)this.read())) < 0) {
            throw new EOFException();
        }
        return (ch1 << 24) + (ch2 << 16) + (ch3 << 8) + (ch4 << false);
    }

    public final long readUnsignedIntLE() throws IOException {
        long ch4;
        long ch2;
        long ch3;
        long ch1 = this.read();
        if ((ch1 | (ch2 = (long)this.read()) | (ch3 = (long)this.read()) | (ch4 = (long)this.read())) < 0) {
            throw new EOFException();
        }
        return (ch4 << 24) + (ch3 << 16) + (ch2 << 8) + (ch1 << false);
    }

    public long readLong() throws IOException {
        return ((long)this.readInt() << 32) + ((long)this.readInt() & 0xFFFFFFFFL);
    }

    public final long readLongLE() throws IOException {
        int i1 = this.readIntLE();
        int i2 = this.readIntLE();
        return ((long)i2 << 32) + ((long)i1 & 0xFFFFFFFFL);
    }

    public float readFloat() throws IOException {
        return Float.intBitsToFloat(this.readInt());
    }

    public final float readFloatLE() throws IOException {
        return Float.intBitsToFloat(this.readIntLE());
    }

    public double readDouble() throws IOException {
        return Double.longBitsToDouble(this.readLong());
    }

    public final double readDoubleLE() throws IOException {
        return Double.longBitsToDouble(this.readLongLE());
    }

    public String readLine() throws IOException {
        StringBuffer input = new StringBuffer();
        int c = -1;
        boolean eol = false;
        block4 : while (!eol) {
            c = this.read();
            switch (c) {
                case -1: 
                case 10: {
                    eol = true;
                    break;
                }
                case 13: {
                    eol = true;
                    int cur = this.getFilePointer();
                    if (this.read() == 10) continue block4;
                    this.seek(cur);
                    break;
                }
                default: {
                    input.append((char)c);
                }
            }
        }
        if (c == -1 && input.length() == 0) {
            return null;
        }
        return input.toString();
    }

    public String readUTF() throws IOException {
        return DataInputStream.readUTF(this);
    }

    public int getStartOffset() {
        return this.startOffset;
    }

    public void setStartOffset(int startOffset) {
        this.startOffset = startOffset;
    }
}

