/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.Element;
import com.lowagie.text.Image;
import java.net.MalformedURLException;

public class Watermark
extends Image
implements Element {
    private float offsetX = 0.0f;
    private float offsetY = 0.0f;

    public Watermark(Image image, float offsetX, float offsetY) throws MalformedURLException {
        super(image);
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }

    public int type() {
        return this.type;
    }

    public float offsetX() {
        return this.offsetX;
    }

    public float offsetY() {
        return this.offsetY;
    }
}

