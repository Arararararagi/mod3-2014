/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.Element;
import com.lowagie.text.Meta;

public class Header
extends Meta
implements Element {
    private StringBuffer name;

    public Header(String name, String content) {
        super(0, content);
        this.name = new StringBuffer(name);
    }

    public String name() {
        return this.name.toString();
    }
}

