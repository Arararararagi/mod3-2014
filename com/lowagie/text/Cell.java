/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ElementListener;
import com.lowagie.text.List;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.TextElementArray;
import com.lowagie.text.markup.MarkupParser;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

public class Cell
extends Rectangle
implements TextElementArray {
    protected ArrayList arrayList;
    protected int horizontalAlignment;
    protected int verticalAlignment;
    protected String width;
    protected int colspan;
    protected int rowspan;
    float leading;
    protected boolean header;
    protected boolean useAscender;
    protected boolean useDescender;
    protected boolean useBorderPadding;
    protected boolean groupChange;
    protected int maxLines;
    String showTruncation;

    public static Cell getDummyCell() {
        Cell cell = new Cell(true);
        cell.setColspan(3);
        cell.setBorder(0);
        return cell;
    }

    public Cell() {
        super(0.0f, 0.0f, 0.0f, 0.0f);
        this.arrayList = null;
        this.horizontalAlignment = -1;
        this.verticalAlignment = -1;
        this.colspan = 1;
        this.rowspan = 1;
        this.leading = NaNf;
        this.useAscender = false;
        this.useDescender = false;
        this.groupChange = true;
        this.maxLines = Integer.MAX_VALUE;
        this.setBorder(-1);
        this.setBorderWidth(0.5f);
        this.arrayList = new ArrayList();
    }

    public Cell(boolean dummy) {
        this();
        this.arrayList.add(new Paragraph(0.0f));
    }

    public Cell(String content) {
        super(0.0f, 0.0f, 0.0f, 0.0f);
        this.arrayList = null;
        this.horizontalAlignment = -1;
        this.verticalAlignment = -1;
        this.colspan = 1;
        this.rowspan = 1;
        this.leading = NaNf;
        this.useAscender = false;
        this.useDescender = false;
        this.groupChange = true;
        this.maxLines = Integer.MAX_VALUE;
        this.setBorder(-1);
        this.setBorderWidth(0.5f);
        this.arrayList = new ArrayList();
        try {
            this.addElement(new Paragraph(content));
        }
        catch (BadElementException var2_2) {
            // empty catch block
        }
    }

    public Cell(Element element) throws BadElementException {
        super(0.0f, 0.0f, 0.0f, 0.0f);
        this.arrayList = null;
        this.horizontalAlignment = -1;
        this.verticalAlignment = -1;
        this.colspan = 1;
        this.rowspan = 1;
        this.leading = NaNf;
        this.useAscender = false;
        this.useDescender = false;
        this.groupChange = true;
        this.maxLines = Integer.MAX_VALUE;
        this.setBorder(-1);
        this.setBorderWidth(0.5f);
        if (element instanceof Phrase) {
            Phrase p = (Phrase)element;
            this.leading = p.leading();
        }
        this.arrayList = new ArrayList();
        this.addElement(element);
    }

    public Cell(Properties attributes) {
        int red;
        int blue;
        int green;
        this();
        String value = (String)attributes.remove("horizontalalign");
        if (value != null) {
            this.setHorizontalAlignment(value);
        }
        if ((value = (String)attributes.remove("verticalalign")) != null) {
            this.setVerticalAlignment(value);
        }
        if ((value = (String)attributes.remove("width")) != null) {
            this.setWidth(value);
        }
        if ((value = (String)attributes.remove("colspan")) != null) {
            this.setColspan(Integer.parseInt(value));
        }
        if ((value = (String)attributes.remove("rowspan")) != null) {
            this.setRowspan(Integer.parseInt(value));
        }
        if ((value = (String)attributes.remove("leading")) != null) {
            this.setLeading(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if ((value = (String)attributes.remove("header")) != null) {
            this.setHeader(new Boolean(value));
        }
        if ((value = (String)attributes.remove("nowrap")) != null) {
            this.setNoWrap(new Boolean(value));
        }
        if ((value = (String)attributes.remove("borderwidth")) != null) {
            this.setBorderWidth(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        int border = 0;
        value = (String)attributes.remove("left");
        if (value != null && new Boolean(value).booleanValue()) {
            border|=4;
        }
        if ((value = (String)attributes.remove("right")) != null && new Boolean(value).booleanValue()) {
            border|=8;
        }
        if ((value = (String)attributes.remove("top")) != null && new Boolean(value).booleanValue()) {
            border|=1;
        }
        if ((value = (String)attributes.remove("bottom")) != null && new Boolean(value).booleanValue()) {
            border|=2;
        }
        this.setBorder(border);
        String r = (String)attributes.remove("red");
        String g = (String)attributes.remove("green");
        String b = (String)attributes.remove("blue");
        if (r != null || g != null || b != null) {
            red = 0;
            green = 0;
            blue = 0;
            if (r != null) {
                red = Integer.parseInt(r);
            }
            if (g != null) {
                green = Integer.parseInt(g);
            }
            if (b != null) {
                blue = Integer.parseInt(b);
            }
            this.setBorderColor(new Color(red, green, blue));
        } else {
            value = (String)attributes.remove("bordercolor");
            if (value != null) {
                this.setBorderColor(MarkupParser.decodeColor(value));
            }
        }
        r = (String)attributes.remove("bgred");
        g = (String)attributes.remove("bggreen");
        b = (String)attributes.remove("bgblue");
        if (r != null || g != null || b != null) {
            red = 0;
            green = 0;
            blue = 0;
            if (r != null) {
                red = Integer.parseInt(r);
            }
            if (g != null) {
                green = Integer.parseInt(g);
            }
            if (b != null) {
                blue = Integer.parseInt(b);
            }
            this.setBackgroundColor(new Color(red, green, blue));
        } else {
            value = (String)attributes.remove("backgroundcolor");
            if (value != null) {
                this.setBackgroundColor(MarkupParser.decodeColor(value));
            }
        }
        value = (String)attributes.remove("grayfill");
        if (value != null) {
            this.setGrayFill(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if (attributes.size() > 0) {
            this.setMarkupAttributes(attributes);
        }
    }

    public boolean process(ElementListener listener) {
        try {
            return listener.add(this);
        }
        catch (DocumentException de) {
            return false;
        }
    }

    public int type() {
        return 20;
    }

    public ArrayList getChunks() {
        ArrayList tmp = new ArrayList();
        Iterator i = this.arrayList.iterator();
        while (i.hasNext()) {
            tmp.addAll(((Element)i.next()).getChunks());
        }
        return tmp;
    }

    public void addElement(Element element) throws BadElementException {
        if (this.isTable()) {
            Table table = (Table)this.arrayList.get(0);
            Cell tmp = new Cell(element);
            tmp.setBorder(0);
            tmp.setColspan(table.columns());
            table.addCell(tmp);
            return;
        }
        switch (element.type()) {
            case 15: 
            case 20: 
            case 21: {
                throw new BadElementException("You can't add listitems, rows or cells to a cell.");
            }
            case 14: {
                if (Float.isNaN(this.leading)) {
                    this.leading = ((List)element).leading();
                }
                if (((List)element).size() == 0) {
                    return;
                }
                this.arrayList.add(element);
                return;
            }
            case 11: 
            case 12: 
            case 17: {
                if (Float.isNaN(this.leading)) {
                    this.leading = ((Phrase)element).leading();
                }
                if (((Phrase)element).isEmpty()) {
                    return;
                }
                this.arrayList.add(element);
                return;
            }
            case 10: {
                if (((Chunk)element).isEmpty()) {
                    return;
                }
                this.arrayList.add(element);
                return;
            }
            case 22: {
                Cell tmp;
                Table table = new Table(3);
                float[] widths = new float[3];
                widths[1] = ((Table)element).widthPercentage();
                switch (((Table)element).alignment()) {
                    case 0: {
                        widths[0] = 0.0f;
                        widths[2] = 100.0f - widths[1];
                        break;
                    }
                    case 1: {
                        widths[0] = (100.0f - widths[1]) / 2.0f;
                        widths[2] = widths[0];
                        break;
                    }
                    case 2: {
                        widths[0] = 100.0f - widths[1];
                        widths[2] = 0.0f;
                    }
                }
                table.setWidths(widths);
                if (this.arrayList.size() == 0) {
                    table.addCell(Cell.getDummyCell());
                } else {
                    tmp = new Cell();
                    tmp.setBorder(0);
                    tmp.setColspan(3);
                    Iterator i = this.arrayList.iterator();
                    while (i.hasNext()) {
                        tmp.add((Element)i.next());
                    }
                    table.addCell(tmp);
                }
                tmp = new Cell();
                tmp.setBorder(0);
                table.addCell(tmp);
                table.insertTable((Table)element);
                table.addCell(tmp);
                table.addCell(Cell.getDummyCell());
                this.clear();
                this.arrayList.add(table);
                return;
            }
        }
        this.arrayList.add(element);
    }

    public boolean add(Object o) {
        try {
            this.addElement((Element)o);
            return true;
        }
        catch (ClassCastException cce) {
            throw new ClassCastException("You can only add objects that implement the Element interface.");
        }
        catch (BadElementException bee) {
            throw new ClassCastException(bee.getMessage());
        }
    }

    public void setLeading(float value) {
        this.leading = value;
    }

    public void setHorizontalAlignment(int value) {
        this.horizontalAlignment = value;
    }

    public void setHorizontalAlignment(String alignment) {
        if ("Center".equalsIgnoreCase(alignment)) {
            this.horizontalAlignment = 1;
            return;
        }
        if ("Right".equalsIgnoreCase(alignment)) {
            this.horizontalAlignment = 2;
            return;
        }
        if ("Justify".equalsIgnoreCase(alignment)) {
            this.horizontalAlignment = 3;
            return;
        }
        if ("JustifyAll".equalsIgnoreCase(alignment)) {
            this.horizontalAlignment = 8;
            return;
        }
        this.horizontalAlignment = 0;
    }

    public void setVerticalAlignment(int value) {
        this.verticalAlignment = value;
    }

    public void setVerticalAlignment(String alignment) {
        if ("Middle".equalsIgnoreCase(alignment)) {
            this.verticalAlignment = 5;
            return;
        }
        if ("Bottom".equalsIgnoreCase(alignment)) {
            this.verticalAlignment = 6;
            return;
        }
        if ("Baseline".equalsIgnoreCase(alignment)) {
            this.verticalAlignment = 7;
            return;
        }
        this.verticalAlignment = 4;
    }

    public void setWidth(String value) {
        this.width = value;
    }

    public void setColspan(int value) {
        this.colspan = value;
    }

    public void setRowspan(int value) {
        this.rowspan = value;
    }

    public void setHeader(boolean value) {
        this.header = value;
    }

    public void setNoWrap(boolean value) {
        this.maxLines = 1;
    }

    public int size() {
        return this.arrayList.size();
    }

    public boolean isEmpty() {
        switch (this.size()) {
            case 0: {
                return true;
            }
            case 1: {
                Element element = (Element)this.arrayList.get(0);
                switch (element.type()) {
                    case 10: {
                        return ((Chunk)element).isEmpty();
                    }
                    case 11: 
                    case 12: 
                    case 17: {
                        return ((Phrase)element).isEmpty();
                    }
                    case 14: {
                        if (((List)element).size() == 0) {
                            return true;
                        }
                        return false;
                    }
                }
                return false;
            }
        }
        return false;
    }

    void fill() {
        if (this.size() == 0) {
            this.arrayList.add(new Paragraph(0.0f));
        }
    }

    public boolean isTable() {
        if (this.size() == 1 && ((Element)this.arrayList.get(0)).type() == 22) {
            return true;
        }
        return false;
    }

    public Iterator getElements() {
        return this.arrayList.iterator();
    }

    public int horizontalAlignment() {
        return this.horizontalAlignment;
    }

    public int verticalAlignment() {
        return this.verticalAlignment;
    }

    public String cellWidth() {
        return this.width;
    }

    public int colspan() {
        return this.colspan;
    }

    public int rowspan() {
        return this.rowspan;
    }

    public float leading() {
        if (Float.isNaN(this.leading)) {
            return 16.0f;
        }
        return this.leading;
    }

    public boolean header() {
        return this.header;
    }

    public boolean noWrap() {
        if (this.maxLines == 1) {
            return true;
        }
        return false;
    }

    public void clear() {
        this.arrayList.clear();
    }

    public float top() {
        throw new UnsupportedOperationException("Dimensions of a Cell can't be calculated. See the FAQ.");
    }

    public float bottom() {
        throw new UnsupportedOperationException("Dimensions of a Cell can't be calculated. See the FAQ.");
    }

    public float left() {
        throw new UnsupportedOperationException("Dimensions of a Cell can't be calculated. See the FAQ.");
    }

    public float right() {
        throw new UnsupportedOperationException("Dimensions of a Cell can't be calculated. See the FAQ.");
    }

    public float top(int margin) {
        throw new UnsupportedOperationException("Dimensions of a Cell can't be calculated. See the FAQ.");
    }

    public float bottom(int margin) {
        throw new UnsupportedOperationException("Dimensions of a Cell can't be calculated. See the FAQ.");
    }

    public float left(int margin) {
        throw new UnsupportedOperationException("Dimensions of a Cell can't be calculated. See the FAQ.");
    }

    public float right(int margin) {
        throw new UnsupportedOperationException("Dimensions of a Cell can't be calculated. See the FAQ.");
    }

    public void setTop(int value) {
        throw new UnsupportedOperationException("Dimensions of a Cell are attributed automagically. See the FAQ.");
    }

    public void setBottom(int value) {
        throw new UnsupportedOperationException("Dimensions of a Cell are attributed automagically. See the FAQ.");
    }

    public void setLeft(int value) {
        throw new UnsupportedOperationException("Dimensions of a Cell are attributed automagically. See the FAQ.");
    }

    public void setRight(int value) {
        throw new UnsupportedOperationException("Dimensions of a Cell are attributed automagically. See the FAQ.");
    }

    public static boolean isTag(String tag) {
        return "cell".equals(tag);
    }

    public boolean getGroupChange() {
        return this.groupChange;
    }

    public void setGroupChange(boolean value) {
        this.groupChange = value;
    }

    public int getMaxLines() {
        return this.maxLines;
    }

    public void setMaxLines(int value) {
        this.maxLines = value;
    }

    public void setShowTruncation(String value) {
        this.showTruncation = value;
    }

    public String getShowTruncation() {
        return this.showTruncation;
    }

    public void setUseAscender(boolean use) {
        this.useAscender = use;
    }

    public boolean isUseAscender() {
        return this.useAscender;
    }

    public void setUseDescender(boolean use) {
        this.useDescender = use;
    }

    public boolean isUseDescender() {
        return this.useDescender;
    }

    public void setUseBorderPadding(boolean use) {
        this.useBorderPadding = use;
    }

    public boolean isUseBorderPadding() {
        return this.useBorderPadding;
    }

    public PdfPCell createPdfPCell() throws BadElementException {
        if (this.rowspan > 1) {
            throw new BadElementException("PdfPCells can't have a rowspan > 1");
        }
        if (this.isTable()) {
            return new PdfPCell(((Table)this.arrayList.get(0)).createPdfPTable());
        }
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(this.verticalAlignment);
        cell.setHorizontalAlignment(this.horizontalAlignment);
        cell.setColspan(this.colspan);
        cell.setUseBorderPadding(this.useBorderPadding);
        cell.setUseDescender(this.useDescender);
        cell.setLeading(this.leading(), 0.0f);
        cell.cloneNonPositionParameters(this);
        cell.setNoWrap(this.noWrap());
        Iterator i = this.getElements();
        while (i.hasNext()) {
            cell.addElement((Element)i.next());
        }
        return cell;
    }
}

