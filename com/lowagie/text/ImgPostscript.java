/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.BadElementException;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfTemplate;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.StringTokenizer;

public class ImgPostscript
extends Image
implements Element {
    ImgPostscript(Image image) {
        super(image);
    }

    public ImgPostscript(URL url) throws BadElementException, IOException {
        super(url);
        this.processParameters();
    }

    public ImgPostscript(String filename) throws BadElementException, MalformedURLException, IOException {
        this(Image.toURL(filename));
    }

    public ImgPostscript(byte[] img) throws BadElementException, IOException {
        super((URL)null);
        this.rawData = img;
        this.originalData = img;
        this.processParameters();
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     */
    private void processParameters() throws BadElementException, IOException {
        block12 : {
            this.type = 35;
            this.originalType = 7;
            InputStream is = null;
            try {
                String errorID;
                if (this.rawData == null) {
                    is = this.url.openStream();
                    errorID = this.url.toString();
                } else {
                    is = new ByteArrayInputStream(this.rawData);
                    errorID = "Byte array";
                }
                String boundingbox = null;
                BufferedReader r = new BufferedReader(new InputStreamReader(is));
                while (r.ready()) {
                    char c;
                    StringBuffer sb = new StringBuffer();
                    while ((c = (char)r.read()) != '\n') {
                        sb.append(c);
                    }
                    if (sb.toString().startsWith("%%BoundingBox:")) {
                        boundingbox = sb.toString();
                    }
                    if (sb.toString().startsWith("%%TemplateBox:")) {
                        boundingbox = sb.toString();
                    }
                    if (sb.toString().startsWith("%%EndComments")) break;
                }
                if (boundingbox == null) {
                    Object var15_8 = null;
                    if (is != null) {
                        is.close();
                    }
                    this.plainWidth = this.width();
                    this.plainHeight = this.height();
                    return;
                }
                StringTokenizer st = new StringTokenizer(boundingbox, ": \r\n");
                st.nextElement();
                String xx1 = st.nextToken();
                String yy1 = st.nextToken();
                String xx2 = st.nextToken();
                String yy2 = st.nextToken();
                int left = Integer.parseInt(xx1);
                int top = Integer.parseInt(yy1);
                int right = Integer.parseInt(xx2);
                int bottom = Integer.parseInt(yy2);
                boolean inch = true;
                this.dpiX = 72;
                this.dpiY = 72;
                this.scaledHeight = (float)(bottom - top) / (float)inch ? 1 : 0 * 1.0f;
                this.scaledHeight = 800.0f;
                this.setTop(this.scaledHeight);
                this.scaledWidth = (float)(right - left) / (float)inch ? 1 : 0 * 1.0f;
                this.scaledWidth = 800.0f;
                this.setRight(this.scaledWidth);
                Object var15_10 = null;
                if (is == null) break block12;
            }
            catch (Throwable var16_19) {
                Object var15_9 = null;
                if (is != null) {
                    is.close();
                }
                this.plainWidth = this.width();
                this.plainHeight = this.height();
                throw var16_19;
            }
            is.close();
        }
        this.plainWidth = this.width();
        this.plainHeight = this.height();
    }

    /*
     * Exception decompiling
     */
    public void readPostscript(PdfTemplate template) throws IOException, DocumentException {
        // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
        // java.lang.IllegalStateException: Backjump on non jumping statement [] lbl19 : TryStatement: try { 1[TRYBLOCK]

        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.Cleaner$1.call(Cleaner.java:44)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.Cleaner$1.call(Cleaner.java:22)
        // org.benf.cfr.reader.util.graph.GraphVisitorDFS.process(GraphVisitorDFS.java:68)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.Cleaner.removeUnreachableCode(Cleaner.java:54)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.RemoveDeterministicJumps.apply(RemoveDeterministicJumps.java:34)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:501)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:214)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:159)
        // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:91)
        // org.benf.cfr.reader.entities.Method.analyse(Method.java:353)
        // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:731)
        // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:663)
        // org.benf.cfr.reader.Main.doJar(Main.java:126)
        // org.benf.cfr.reader.Main.main(Main.java:178)
        throw new IllegalStateException("Decompilation failed");
    }
}

