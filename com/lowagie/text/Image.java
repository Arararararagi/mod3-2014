/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.Annotation;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.ImgCCITT;
import com.lowagie.text.ImgPostscript;
import com.lowagie.text.ImgRaw;
import com.lowagie.text.ImgTemplate;
import com.lowagie.text.ImgWMF;
import com.lowagie.text.Jpeg;
import com.lowagie.text.MarkupAttributes;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfOCG;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.codec.BmpImage;
import com.lowagie.text.pdf.codec.CCITTG4Encoder;
import com.lowagie.text.pdf.codec.GifImage;
import com.lowagie.text.pdf.codec.PngImage;
import com.lowagie.text.pdf.codec.TiffImage;
import java.awt.Color;
import java.awt.color.ICC_Profile;
import java.awt.image.PixelGrabber;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.Set;

public abstract class Image
extends Rectangle
implements Element,
MarkupAttributes {
    public static final int DEFAULT = 0;
    public static final int RIGHT = 2;
    public static final int LEFT = 0;
    public static final int MIDDLE = 1;
    public static final int TEXTWRAP = 4;
    public static final int UNDERLYING = 8;
    public static final int AX = 0;
    public static final int AY = 1;
    public static final int BX = 2;
    public static final int BY = 3;
    public static final int CX = 4;
    public static final int CY = 5;
    public static final int DX = 6;
    public static final int DY = 7;
    public static final int ORIGINAL_NONE = 0;
    public static final int ORIGINAL_JPEG = 1;
    public static final int ORIGINAL_PNG = 2;
    public static final int ORIGINAL_GIF = 3;
    public static final int ORIGINAL_BMP = 4;
    public static final int ORIGINAL_TIFF = 5;
    public static final int ORIGINAL_WMF = 6;
    public static final int ORIGINAL_PS = 7;
    protected boolean invert = false;
    protected int type;
    protected URL url;
    protected byte[] rawData;
    protected PdfTemplate[] template = new PdfTemplate[1];
    protected int alignment;
    protected String alt;
    protected float absoluteX = NaNf;
    protected float absoluteY = NaNf;
    protected float plainWidth;
    protected float plainHeight;
    protected float scaledWidth;
    protected float scaledHeight;
    protected float rotation;
    protected int colorspace = -1;
    protected int bpc = 1;
    protected int[] transparency;
    protected float indentationLeft = 0.0f;
    protected float indentationRight = 0.0f;
    protected Long mySerialId = Image.getSerialId();
    static long serialId = 0;
    protected int dpiX = 0;
    protected int dpiY = 0;
    protected boolean mask = false;
    protected Image imageMask;
    protected boolean interpolation;
    protected Annotation annotation = null;
    protected Properties markupAttributes;
    protected ICC_Profile profile = null;
    protected boolean deflated = false;
    private PdfDictionary additional = null;
    private boolean smask;
    private float XYRatio = 0.0f;
    protected int originalType = 0;
    protected byte[] originalData;
    protected float spacingBefore;
    protected float spacingAfter;
    private float widthPercentage = 100.0f;
    protected PdfOCG layer;
    static /* synthetic */ Class class$0;

    public Image(URL url) {
        super(0.0f, 0.0f);
        this.url = url;
        this.alignment = 0;
        this.rotation = 0.0f;
    }

    protected Image(Image image) {
        super(image);
        this.type = image.type;
        this.url = image.url;
        this.alignment = image.alignment;
        this.alt = image.alt;
        this.absoluteX = image.absoluteX;
        this.absoluteY = image.absoluteY;
        this.plainWidth = image.plainWidth;
        this.plainHeight = image.plainHeight;
        this.scaledWidth = image.scaledWidth;
        this.scaledHeight = image.scaledHeight;
        this.rotation = image.rotation;
        this.colorspace = image.colorspace;
        this.rawData = image.rawData;
        this.template = image.template;
        this.bpc = image.bpc;
        this.transparency = image.transparency;
        this.mySerialId = image.mySerialId;
        this.invert = image.invert;
        this.dpiX = image.dpiX;
        this.dpiY = image.dpiY;
        this.mask = image.mask;
        this.imageMask = image.imageMask;
        this.interpolation = image.interpolation;
        this.annotation = image.annotation;
        this.markupAttributes = image.markupAttributes;
        this.profile = image.profile;
        this.deflated = image.deflated;
        this.additional = image.additional;
        this.smask = image.smask;
        this.XYRatio = image.XYRatio;
        this.originalData = image.originalData;
        this.originalType = image.originalType;
        this.spacingAfter = image.spacingAfter;
        this.spacingBefore = image.spacingBefore;
        this.widthPercentage = image.widthPercentage;
        this.layer = image.layer;
    }

    public static Image getInstance(Image image) {
        if (image == null) {
            return null;
        }
        try {
            Class class_;
            Class[] arrclass;
            Class cs;
            cs = image.getClass();
            arrclass = new Class[1];
            class_ = class$0;
            if (class_ == null) {
                try {
                    class_ = Image.class$0 = Class.forName("com.lowagie.text.Image");
                }
                catch (ClassNotFoundException v2) {
                    throw new NoClassDefFoundError(v2.getMessage());
                }
            }
            arrclass[0] = class_;
            Constructor constructor = cs.getDeclaredConstructor(arrclass);
            return (Image)constructor.newInstance(image);
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     */
    public static Image getInstance(URL url) throws BadElementException, MalformedURLException, IOException {
        InputStream is = null;
        try {
            is = url.openStream();
            int c1 = is.read();
            int c2 = is.read();
            int c3 = is.read();
            int c4 = is.read();
            is.close();
            is = null;
            if (c1 == 71 && c2 == 73 && c3 == 70) {
                Image img;
                GifImage gif = new GifImage(url);
                Image image = img = gif.getImage(1);
                Object var11_16 = null;
                if (is == null) return image;
                is.close();
                return image;
            }
            if (c1 == 255 && c2 == 216) {
                Jpeg jpeg = new Jpeg(url);
                Object var11_17 = null;
                if (is == null) return jpeg;
                is.close();
                return jpeg;
            }
            if (c1 == PngImage.PNGID[0] && c2 == PngImage.PNGID[1] && c3 == PngImage.PNGID[2] && c4 == PngImage.PNGID[3]) {
                Image image = PngImage.getImage(url);
                Object var11_18 = null;
                if (is == null) return image;
                is.close();
                return image;
            }
            if (c1 == 37 && c2 == 33 && c3 == 80 && c4 == 83) {
                ImgPostscript imgPostscript = new ImgPostscript(url);
                Object var11_19 = null;
                if (is == null) return imgPostscript;
                is.close();
                return imgPostscript;
            }
            if (c1 == 215 && c2 == 205) {
                ImgWMF imgWMF = new ImgWMF(url);
                Object var11_20 = null;
                if (is == null) return imgWMF;
                is.close();
                return imgWMF;
            }
            if (c1 == 66 && c2 == 77) {
                Image image = BmpImage.getImage(url);
                Object var11_21 = null;
                if (is == null) return image;
                is.close();
                return image;
            }
            if (c1 == 77 && c2 == 77 && c3 == 0 && c4 == 42 || c1 == 73 && c2 == 73 && c3 == 42 && c4 == 0) {
                Image image;
                block15 : {
                    RandomAccessFileOrArray ra = null;
                    try {
                        if (url.getProtocol().equals("file")) {
                            String file = url.getFile();
                            ra = new RandomAccessFileOrArray(file);
                        } else {
                            ra = new RandomAccessFileOrArray(url);
                        }
                        Image img = TiffImage.getTiffImage(ra, 1);
                        img.url = url;
                        image = img;
                        Object var8_25 = null;
                        if (ra == null) break block15;
                    }
                    catch (Throwable var9_27) {
                        Object var8_26 = null;
                        if (ra != null) {
                            ra.close();
                        }
                        throw var9_27;
                    }
                    ra.close();
                }
                Object var11_22 = null;
                if (is == null) return image;
                is.close();
                return image;
            }
            throw new IOException(String.valueOf(url.toString()) + " is not a recognized imageformat.");
        }
        catch (Throwable var12_28) {
            Object var11_23 = null;
            if (is != null) {
                is.close();
            }
            throw var12_28;
        }
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     */
    public static Image getInstance(byte[] imgb) throws BadElementException, MalformedURLException, IOException {
        ByteArrayInputStream is = null;
        try {
            is = new ByteArrayInputStream(imgb);
            int c1 = is.read();
            int c2 = is.read();
            int c3 = is.read();
            int c4 = is.read();
            is.close();
            is = null;
            if (c1 == 71 && c2 == 73 && c3 == 70) {
                GifImage gif = new GifImage(imgb);
                Image image = gif.getImage(1);
                Object var11_14 = null;
                if (is == null) return image;
                is.close();
                return image;
            }
            if (c1 == 255 && c2 == 216) {
                Jpeg jpeg = new Jpeg(imgb);
                Object var11_15 = null;
                if (is == null) return jpeg;
                is.close();
                return jpeg;
            }
            if (c1 == PngImage.PNGID[0] && c2 == PngImage.PNGID[1] && c3 == PngImage.PNGID[2] && c4 == PngImage.PNGID[3]) {
                Image image = PngImage.getImage(imgb);
                Object var11_16 = null;
                if (is == null) return image;
                is.close();
                return image;
            }
            if (c1 == 37 && c2 == 33 && c3 == 80 && c4 == 83) {
                ImgPostscript imgPostscript = new ImgPostscript(imgb);
                Object var11_17 = null;
                if (is == null) return imgPostscript;
                is.close();
                return imgPostscript;
            }
            if (c1 == 215 && c2 == 205) {
                ImgWMF imgWMF = new ImgWMF(imgb);
                Object var11_18 = null;
                if (is == null) return imgWMF;
                is.close();
                return imgWMF;
            }
            if (c1 == 66 && c2 == 77) {
                Image image = BmpImage.getImage(imgb);
                Object var11_19 = null;
                if (is == null) return image;
                is.close();
                return image;
            }
            if (c1 == 77 && c2 == 77 && c3 == 0 && c4 == 42 || c1 == 73 && c2 == 73 && c3 == 42 && c4 == 0) {
                Image image;
                block13 : {
                    RandomAccessFileOrArray ra = null;
                    try {
                        ra = new RandomAccessFileOrArray(imgb);
                        Image img = TiffImage.getTiffImage(ra, 1);
                        img.setOriginalData(imgb);
                        image = img;
                        Object var8_24 = null;
                        if (ra == null) break block13;
                    }
                    catch (Throwable var9_26) {
                        Object var8_25 = null;
                        if (ra != null) {
                            ra.close();
                        }
                        throw var9_26;
                    }
                    ra.close();
                }
                Object var11_20 = null;
                if (is == null) return image;
                is.close();
                return image;
            }
            throw new IOException("The byte array is not a recognized imageformat.");
        }
        catch (Throwable var12_27) {
            Object var11_21 = null;
            if (is != null) {
                is.close();
            }
            throw var12_27;
        }
    }

    public static Image getInstance(java.awt.Image image, Color color, boolean forceBW) throws BadElementException, IOException {
        PixelGrabber pg = new PixelGrabber(image, 0, 0, -1, -1, true);
        try {
            pg.grabPixels();
        }
        catch (InterruptedException e) {
            throw new IOException("java.awt.Image Interrupted waiting for pixels!");
        }
        if ((pg.getStatus() & 128) != 0) {
            throw new IOException("java.awt.Image fetch aborted or errored");
        }
        int w = pg.getWidth();
        int h = pg.getHeight();
        int[] pixels = (int[])pg.getPixels();
        if (forceBW) {
            int byteWidth = w / 8 + ((w & 7) != 0 ? 1 : 0);
            byte[] pixelsByte = new byte[byteWidth * h];
            int index = 0;
            int size = h * w;
            boolean transColor = true;
            if (color != null) {
                transColor = color.getRed() + color.getGreen() + color.getBlue() >= 384;
            }
            int[] transparency = null;
            int cbyte = 128;
            int wMarker = 0;
            int currByte = 0;
            if (color != null) {
                for (int j = 0; j < size; ++j) {
                    int alpha = pixels[j] >> 24 & 255;
                    if (alpha < 250) {
                        if (transColor) {
                            currByte|=cbyte;
                        }
                    } else if ((pixels[j] & 2184) != 0) {
                        currByte|=cbyte;
                    }
                    if ((cbyte>>=1) == 0 || wMarker + 1 >= w) {
                        pixelsByte[index++] = (byte)currByte;
                        cbyte = 128;
                        currByte = 0;
                    }
                    if (++wMarker < w) continue;
                    wMarker = 0;
                }
            } else {
                for (int j = 0; j < size; ++j) {
                    int alpha;
                    if (transparency == null && (alpha = pixels[j] >> 24 & 255) == 0) {
                        transparency = new int[2];
                        transparency[1] = (pixels[j] & 2184) != 0 ? 1 : 0;
                        transparency[0] = transparency[1];
                    }
                    if ((pixels[j] & 2184) != 0) {
                        currByte|=cbyte;
                    }
                    if ((cbyte>>=1) == 0 || wMarker + 1 >= w) {
                        pixelsByte[index++] = (byte)currByte;
                        cbyte = 128;
                        currByte = 0;
                    }
                    if (++wMarker < w) continue;
                    wMarker = 0;
                }
            }
            return Image.getInstance(w, h, 1, 1, pixelsByte, transparency);
        }
        byte[] pixelsByte = new byte[w * h * 3];
        byte[] smask = null;
        int index = 0;
        int size = h * w;
        int red = 255;
        int green = 255;
        int blue = 255;
        if (color != null) {
            red = color.getRed();
            green = color.getGreen();
            blue = color.getBlue();
        }
        int[] transparency = null;
        if (color != null) {
            for (int j = 0; j < size; ++j) {
                int alpha = pixels[j] >> 24 & 255;
                if (alpha < 250) {
                    pixelsByte[index++] = (byte)red;
                    pixelsByte[index++] = (byte)green;
                    pixelsByte[index++] = (byte)blue;
                    continue;
                }
                pixelsByte[index++] = (byte)(pixels[j] >> 16 & 255);
                pixelsByte[index++] = (byte)(pixels[j] >> 8 & 255);
                pixelsByte[index++] = (byte)(pixels[j] & 255);
            }
        } else {
            int transparentPixel = 0;
            smask = new byte[w * h];
            boolean shades = false;
            for (int j = 0; j < size; ++j) {
                byte alpha = smask[j] = (byte)(pixels[j] >> 24 & 255);
                if (!shades) {
                    if (alpha != 0 && alpha != -1) {
                        shades = true;
                    } else if (transparency == null) {
                        if (alpha == 0) {
                            transparentPixel = pixels[j] & 16777215;
                            transparency = new int[6];
                            transparency[0] = transparency[1] = transparentPixel >> 16 & 255;
                            transparency[2] = transparency[3] = transparentPixel >> 8 & 255;
                            transparency[4] = transparency[5] = transparentPixel & 255;
                        }
                    } else if ((pixels[j] & 16777215) != transparentPixel) {
                        shades = true;
                    }
                }
                pixelsByte[index++] = (byte)(pixels[j] >> 16 & 255);
                pixelsByte[index++] = (byte)(pixels[j] >> 8 & 255);
                pixelsByte[index++] = (byte)(pixels[j] & 255);
            }
            if (shades) {
                transparency = null;
            } else {
                smask = null;
            }
        }
        Image img = Image.getInstance(w, h, 3, 8, pixelsByte, transparency);
        if (smask != null) {
            Image sm = Image.getInstance(w, h, 1, 8, smask);
            try {
                sm.makeMask();
                img.setImageMask(sm);
            }
            catch (DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }
        return img;
    }

    public static Image getInstance(java.awt.Image image, Color color) throws BadElementException, IOException {
        return Image.getInstance(image, color, false);
    }

    public static Image getInstance(String filename) throws BadElementException, MalformedURLException, IOException {
        return Image.getInstance(Image.toURL(filename));
    }

    public static Image getInstance(int width, int height, int components, int bpc, byte[] data) throws BadElementException {
        return Image.getInstance(width, height, components, bpc, data, null);
    }

    public static Image getInstance(PdfTemplate template) throws BadElementException {
        return new ImgTemplate(template);
    }

    public static Image getInstance(int width, int height, boolean reverseBits, int typeCCITT, int parameters, byte[] data) throws BadElementException {
        return Image.getInstance(width, height, reverseBits, typeCCITT, parameters, data, null);
    }

    public static Image getInstance(int width, int height, boolean reverseBits, int typeCCITT, int parameters, byte[] data, int[] transparency) throws BadElementException {
        if (transparency != null && transparency.length != 2) {
            throw new BadElementException("Transparency length must be equal to 2 with CCITT images");
        }
        ImgCCITT img = new ImgCCITT(width, height, reverseBits, typeCCITT, parameters, data);
        img.transparency = transparency;
        return img;
    }

    public static Image getInstance(int width, int height, int components, int bpc, byte[] data, int[] transparency) throws BadElementException {
        if (transparency != null && transparency.length != components * 2) {
            throw new BadElementException("Transparency length must be equal to (componentes * 2)");
        }
        if (components == 1 && bpc == 1) {
            byte[] g4 = CCITTG4Encoder.compress(data, width, height);
            return Image.getInstance(width, height, false, 256, 1, g4, transparency);
        }
        ImgRaw img = new ImgRaw(width, height, components, bpc, data);
        img.transparency = transparency;
        return img;
    }

    public static Image getInstance(Properties attributes) throws BadElementException, MalformedURLException, IOException {
        String y;
        String x;
        String value = (String)attributes.remove("url");
        if (value == null) {
            throw new MalformedURLException("The URL of the image is missing.");
        }
        Image image = Image.getInstance(value);
        int align = 0;
        value = (String)attributes.remove("align");
        if (value != null) {
            if ("Left".equalsIgnoreCase(value)) {
                align|=false;
            } else if ("Right".equalsIgnoreCase(value)) {
                align|=2;
            } else if ("Middle".equalsIgnoreCase(value)) {
                align|=1;
            }
        }
        if ((value = (String)attributes.remove("underlying")) != null && new Boolean(value).booleanValue()) {
            align|=8;
        }
        if ((value = (String)attributes.remove("textwrap")) != null && new Boolean(value).booleanValue()) {
            align|=4;
        }
        image.setAlignment(align);
        value = (String)attributes.remove("alt");
        if (value != null) {
            image.setAlt(value);
        }
        if ((x = (String)attributes.remove("absolutex")) != null && (y = (String)attributes.remove("absolutey")) != null) {
            image.setAbsolutePosition(Float.valueOf(String.valueOf(x) + "f").floatValue(), Float.valueOf(String.valueOf(y) + "f").floatValue());
        }
        if ((value = (String)attributes.remove("plainwidth")) != null) {
            image.scaleAbsoluteWidth(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if ((value = (String)attributes.remove("plainheight")) != null) {
            image.scaleAbsoluteHeight(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if ((value = (String)attributes.remove("rotation")) != null) {
            image.setRotation(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if (attributes.size() > 0) {
            image.setMarkupAttributes(attributes);
        }
        return image;
    }

    public void setAlignment(int alignment) {
        this.alignment = alignment;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public void setAbsolutePosition(float absoluteX, float absoluteY) {
        this.absoluteX = absoluteX;
        this.absoluteY = absoluteY;
    }

    public void scaleAbsolute(float newWidth, float newHeight) {
        this.plainWidth = newWidth;
        this.plainHeight = newHeight;
        float[] matrix = this.matrix();
        this.scaledWidth = matrix[6] - matrix[4];
        this.scaledHeight = matrix[7] - matrix[5];
    }

    public void scaleAbsoluteWidth(float newWidth) {
        this.plainWidth = newWidth;
        float[] matrix = this.matrix();
        this.scaledWidth = matrix[6] - matrix[4];
        this.scaledHeight = matrix[7] - matrix[5];
    }

    public void scaleAbsoluteHeight(float newHeight) {
        this.plainHeight = newHeight;
        float[] matrix = this.matrix();
        this.scaledWidth = matrix[6] - matrix[4];
        this.scaledHeight = matrix[7] - matrix[5];
    }

    public void scalePercent(float percent) {
        this.scalePercent(percent, percent);
    }

    public void scalePercent(float percentX, float percentY) {
        this.plainWidth = this.width() * percentX / 100.0f;
        this.plainHeight = this.height() * percentY / 100.0f;
        float[] matrix = this.matrix();
        this.scaledWidth = matrix[6] - matrix[4];
        this.scaledHeight = matrix[7] - matrix[5];
    }

    public void scaleToFit(float fitWidth, float fitHeight) {
        float percentY;
        float percentX = fitWidth * 100.0f / this.width();
        this.scalePercent(percentX < (percentY = fitHeight * 100.0f / this.height()) ? percentX : percentY);
    }

    public void setRotation(float r) {
        double d = 3.141592653589793;
        this.rotation = (float)((double)r % (2.0 * d));
        if (this.rotation < 0.0f) {
            this.rotation = (float)((double)this.rotation + 2.0 * d);
        }
        float[] matrix = this.matrix();
        this.scaledWidth = matrix[6] - matrix[4];
        this.scaledHeight = matrix[7] - matrix[5];
    }

    public void setRotationDegrees(float deg) {
        double d = 3.141592653589793;
        this.setRotation(deg / 180.0f * (float)d);
    }

    public void setAnnotation(Annotation annotation) {
        this.annotation = annotation;
    }

    public Annotation annotation() {
        return this.annotation;
    }

    public int bpc() {
        return this.bpc;
    }

    public byte[] rawData() {
        return this.rawData;
    }

    public PdfTemplate templateData() {
        return this.template[0];
    }

    public void setTemplateData(PdfTemplate template) {
        this.template[0] = template;
    }

    public boolean hasAbsolutePosition() {
        return !Float.isNaN(this.absoluteY);
    }

    public boolean hasAbsoluteX() {
        return !Float.isNaN(this.absoluteX);
    }

    public float absoluteX() {
        return this.absoluteX;
    }

    public float absoluteY() {
        return this.absoluteY;
    }

    public int type() {
        return this.type;
    }

    public boolean isJpeg() {
        if (this.type == 32) {
            return true;
        }
        return false;
    }

    public boolean isImgRaw() {
        if (this.type == 34) {
            return true;
        }
        return false;
    }

    public boolean isImgTemplate() {
        if (this.type == 35) {
            return true;
        }
        return false;
    }

    public URL url() {
        return this.url;
    }

    public int alignment() {
        return this.alignment;
    }

    public String alt() {
        return this.alt;
    }

    public float scaledWidth() {
        return this.scaledWidth;
    }

    public float scaledHeight() {
        return this.scaledHeight;
    }

    public int colorspace() {
        return this.colorspace;
    }

    public float[] matrix() {
        float[] matrix = new float[8];
        float cosX = (float)Math.cos(this.rotation);
        float sinX = (float)Math.sin(this.rotation);
        matrix[0] = this.plainWidth * cosX;
        matrix[1] = this.plainWidth * sinX;
        matrix[2] = (- this.plainHeight) * sinX;
        matrix[3] = this.plainHeight * cosX;
        if ((double)this.rotation < 1.5707963267948966) {
            matrix[4] = matrix[2];
            matrix[5] = 0.0f;
            matrix[6] = matrix[0];
            matrix[7] = matrix[1] + matrix[3];
        } else if ((double)this.rotation < 3.141592653589793) {
            matrix[4] = matrix[0] + matrix[2];
            matrix[5] = matrix[3];
            matrix[6] = 0.0f;
            matrix[7] = matrix[1];
        } else if ((double)this.rotation < 4.71238898038469) {
            matrix[4] = matrix[0];
            matrix[5] = matrix[1] + matrix[3];
            matrix[6] = matrix[2];
            matrix[7] = 0.0f;
        } else {
            matrix[4] = 0.0f;
            matrix[5] = matrix[1];
            matrix[6] = matrix[0] + matrix[2];
            matrix[7] = matrix[3];
        }
        return matrix;
    }

    public static void skip(InputStream is, int size) throws IOException {
        while (size > 0) {
            size = (int)((long)size - is.skip(size));
        }
    }

    public static URL toURL(String filename) throws MalformedURLException {
        if (filename.startsWith("file:/") || filename.startsWith("http://") || filename.startsWith("https://") || filename.startsWith("jar:")) {
            return new URL(filename);
        }
        File f = new File(filename);
        String path = f.getAbsolutePath();
        if (File.separatorChar != '/') {
            path = path.replace(File.separatorChar, '/');
        }
        if (!path.startsWith("/")) {
            path = "/" + path;
        }
        if (!path.endsWith("/") && f.isDirectory()) {
            path = String.valueOf(path) + "/";
        }
        return new URL("file", "", path);
    }

    public int[] getTransparency() {
        return this.transparency;
    }

    public void setTransparency(int[] transparency) {
        this.transparency = transparency;
    }

    public static boolean isTag(String tag) {
        return "image".equals(tag);
    }

    public float plainWidth() {
        return this.plainWidth;
    }

    public float plainHeight() {
        return this.plainHeight;
    }

    protected static synchronized Long getSerialId() {
        return new Long(++serialId);
    }

    public Long getMySerialId() {
        return this.mySerialId;
    }

    public int getDpiX() {
        return this.dpiX;
    }

    public int getDpiY() {
        return this.dpiY;
    }

    public void setDpi(int dpiX, int dpiY) {
        this.dpiX = dpiX;
        this.dpiY = dpiY;
    }

    public boolean isMaskCandidate() {
        if (this.type == 34 && this.bpc > 255) {
            return true;
        }
        if (this.colorspace == 1) {
            return true;
        }
        return false;
    }

    public void makeMask() throws DocumentException {
        if (!this.isMaskCandidate()) {
            throw new DocumentException("This image can not be an image mask.");
        }
        this.mask = true;
    }

    public void setImageMask(Image mask) throws DocumentException {
        if (this.mask) {
            throw new DocumentException("An image mask cannot contain another image mask.");
        }
        if (!mask.mask) {
            throw new DocumentException("The image mask is not a mask. Did you do makeMask()?");
        }
        this.imageMask = mask;
        this.smask = mask.bpc > 1 && mask.bpc <= 8;
    }

    public Image getImageMask() {
        return this.imageMask;
    }

    public boolean isMask() {
        return this.mask;
    }

    public void setInvertMask(boolean invert) {
        this.invert = invert;
    }

    public boolean isInvertMask() {
        return this.invert;
    }

    public boolean isInverted() {
        return this.invert;
    }

    public void setInverted(boolean invert) {
        this.invert = invert;
    }

    public boolean isInterpolation() {
        return this.interpolation;
    }

    public void setInterpolation(boolean interpolation) {
        this.interpolation = interpolation;
    }

    public void setMarkupAttribute(String name, String value) {
        if (this.markupAttributes == null) {
            this.markupAttributes = new Properties();
        }
        this.markupAttributes.put(name, value);
    }

    public void setMarkupAttributes(Properties markupAttributes) {
        this.markupAttributes = markupAttributes;
    }

    public String getMarkupAttribute(String name) {
        return this.markupAttributes == null ? null : String.valueOf(this.markupAttributes.get(name));
    }

    public Set getMarkupAttributeNames() {
        return Chunk.getKeySet(this.markupAttributes);
    }

    public Properties getMarkupAttributes() {
        return this.markupAttributes;
    }

    public void tagICC(ICC_Profile profile) {
        this.profile = profile;
    }

    public boolean hasICCProfile() {
        if (this.profile != null) {
            return true;
        }
        return false;
    }

    public ICC_Profile getICCProfile() {
        return this.profile;
    }

    public boolean isDeflated() {
        return this.deflated;
    }

    public void setDeflated(boolean deflated) {
        this.deflated = deflated;
    }

    public PdfDictionary getAdditional() {
        return this.additional;
    }

    public void setAdditional(PdfDictionary additional) {
        this.additional = additional;
    }

    public boolean isSmask() {
        return this.smask;
    }

    public void setSmask(boolean smask) {
        this.smask = smask;
    }

    public float getXYRatio() {
        return this.XYRatio;
    }

    public void setXYRatio(float XYRatio) {
        this.XYRatio = XYRatio;
    }

    public float indentationLeft() {
        return this.indentationLeft;
    }

    public float indentationRight() {
        return this.indentationRight;
    }

    public void setIndentationLeft(float f) {
        this.indentationLeft = f;
    }

    public void setIndentationRight(float f) {
        this.indentationRight = f;
    }

    public int getOriginalType() {
        return this.originalType;
    }

    public void setOriginalType(int originalType) {
        this.originalType = originalType;
    }

    public byte[] getOriginalData() {
        return this.originalData;
    }

    public void setOriginalData(byte[] originalData) {
        this.originalData = originalData;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public void setSpacingBefore(float spacing) {
        this.spacingBefore = spacing;
    }

    public void setSpacingAfter(float spacing) {
        this.spacingAfter = spacing;
    }

    public float spacingBefore() {
        return this.spacingBefore;
    }

    public float spacingAfter() {
        return this.spacingAfter;
    }

    public float getWidthPercentage() {
        return this.widthPercentage;
    }

    public void setWidthPercentage(float widthPercentage) {
        this.widthPercentage = widthPercentage;
    }

    public PdfOCG getLayer() {
        return this.layer;
    }

    public void setLayer(PdfOCG layer) {
        this.layer = layer;
    }
}

