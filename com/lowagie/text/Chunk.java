/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ElementListener;
import com.lowagie.text.ElementTags;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.MarkupAttributes;
import com.lowagie.text.SplitCharacter;
import com.lowagie.text.markup.MarkupParser;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.HyphenationEvent;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfAnnotation;
import java.awt.Color;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Set;

public class Chunk
implements Element,
MarkupAttributes {
    public static final String OBJECT_REPLACEMENT_CHARACTER = "\ufffc";
    public static final Chunk NEWLINE = new Chunk("\n");
    public static final Chunk NEXTPAGE = new Chunk("");
    public static final String SUBSUPSCRIPT = "SUBSUPSCRIPT";
    public static final String UNDERLINE = "UNDERLINE";
    public static final String COLOR = "COLOR";
    public static final String ENCODING = "ENCODING";
    public static final String REMOTEGOTO = "REMOTEGOTO";
    public static final String LOCALGOTO = "LOCALGOTO";
    public static final String LOCALDESTINATION = "LOCALDESTINATION";
    public static final String IMAGE = "IMAGE";
    public static final String GENERICTAG = "GENERICTAG";
    public static final String NEWPAGE = "NEWPAGE";
    public static final String SPLITCHARACTER = "SPLITCHARACTER";
    public static final String ACTION = "ACTION";
    public static final String BACKGROUND = "BACKGROUND";
    public static final String PDFANNOTATION = "PDFANNOTATION";
    public static final String HYPHENATION = "HYPHENATION";
    public static final String TEXTRENDERMODE = "TEXTRENDERMODE";
    public static final String SKEW = "SKEW";
    public static final String HSCALE = "HSCALE";
    protected StringBuffer content = null;
    protected Font font = null;
    protected HashMap attributes = null;
    protected Properties markupAttributes = null;

    static {
        NEXTPAGE.setNewPage();
    }

    protected Chunk() {
    }

    public Chunk(String content, Font font) {
        this.content = new StringBuffer(content);
        this.font = font;
    }

    public Chunk(String content) {
        this(content, new Font());
    }

    public Chunk(char c, Font font) {
        this.content = new StringBuffer();
        this.content.append(c);
        this.font = font;
    }

    public Chunk(char c) {
        this(c, new Font());
    }

    public Chunk(Image image, float offsetX, float offsetY) {
        this("\ufffc", new Font());
        Image copyImage = Image.getInstance(image);
        copyImage.setAbsolutePosition(NaNf, NaNf);
        this.setAttribute("IMAGE", new Object[]{copyImage, new Float(offsetX), new Float(offsetY), new Boolean(false)});
    }

    public Chunk(Image image, float offsetX, float offsetY, boolean changeLeading) {
        this("\ufffc", new Font());
        this.setAttribute("IMAGE", new Object[]{image, new Float(offsetX), new Float(offsetY), new Boolean(changeLeading)});
    }

    public Chunk(Properties attributes) {
        this("", FontFactory.getFont(attributes));
        String value = (String)attributes.remove("itext");
        if (value != null) {
            this.append(value);
        }
        if ((value = (String)attributes.remove(ElementTags.LOCALGOTO)) != null) {
            this.setLocalGoto(value);
        }
        if ((value = (String)attributes.remove(ElementTags.REMOTEGOTO)) != null) {
            String destination = (String)attributes.remove(ElementTags.DESTINATION);
            String page = (String)attributes.remove(ElementTags.PAGE);
            if (page != null) {
                this.setRemoteGoto(value, Integer.valueOf(page));
            } else if (destination != null) {
                this.setRemoteGoto(value, destination);
            }
        }
        if ((value = (String)attributes.remove(ElementTags.LOCALDESTINATION)) != null) {
            this.setLocalDestination(value);
        }
        if ((value = (String)attributes.remove(ElementTags.SUBSUPSCRIPT)) != null) {
            this.setTextRise(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if ((value = (String)attributes.remove("vertical-align")) != null && value.endsWith("%")) {
            float p = Float.valueOf(String.valueOf(value.substring(0, value.length() - 1)) + "f").floatValue() / 100.0f;
            this.setTextRise(p * this.font.size());
        }
        if ((value = (String)attributes.remove(ElementTags.GENERICTAG)) != null) {
            this.setGenericTag(value);
        }
        if ((value = (String)attributes.remove("backgroundcolor")) != null) {
            this.setBackground(MarkupParser.decodeColor(value));
        }
        if (attributes.size() > 0) {
            this.setMarkupAttributes(attributes);
        }
    }

    public boolean process(ElementListener listener) {
        try {
            return listener.add(this);
        }
        catch (DocumentException de) {
            return false;
        }
    }

    public int type() {
        return 10;
    }

    public ArrayList getChunks() {
        ArrayList<Chunk> tmp = new ArrayList<Chunk>();
        tmp.add(this);
        return tmp;
    }

    public StringBuffer append(String string) {
        return this.content.append(string);
    }

    public Font font() {
        return this.font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public String content() {
        return this.content.toString();
    }

    public boolean isEmpty() {
        if (this.content.toString().trim().length() == 0 && this.content.toString().indexOf("\n") == -1 && this.attributes == null) {
            return true;
        }
        return false;
    }

    public float getWidthPoint() {
        if (this.getImage() != null) {
            return this.getImage().scaledWidth();
        }
        return this.font.getCalculatedBaseFont(true).getWidthPoint(this.content(), this.font.getCalculatedSize()) * this.getHorizontalScaling();
    }

    public Chunk setTextRise(float rise) {
        return this.setAttribute("SUBSUPSCRIPT", new Float(rise));
    }

    public float getTextRise() {
        if (this.attributes.containsKey("SUBSUPSCRIPT")) {
            Float f = (Float)this.attributes.get("SUBSUPSCRIPT");
            return f.floatValue();
        }
        return 0.0f;
    }

    public Chunk setTextRenderMode(int mode, float strokeWidth, Color strokeColor) {
        return this.setAttribute("TEXTRENDERMODE", new Object[]{new Integer(mode), new Float(strokeWidth), strokeColor});
    }

    public Chunk setSkew(float alpha, float beta) {
        alpha = (float)Math.tan((double)alpha * 3.141592653589793 / 180.0);
        beta = (float)Math.tan((double)beta * 3.141592653589793 / 180.0);
        return this.setAttribute("SKEW", new float[]{alpha, beta});
    }

    public Chunk setHorizontalScaling(float scale) {
        return this.setAttribute("HSCALE", new Float(scale));
    }

    public float getHorizontalScaling() {
        if (this.attributes == null) {
            return 1.0f;
        }
        Float f = (Float)this.attributes.get("HSCALE");
        if (f == null) {
            return 1.0f;
        }
        return f.floatValue();
    }

    public Chunk setAction(PdfAction action) {
        return this.setAttribute("ACTION", action);
    }

    public Chunk setAnchor(URL url) {
        return this.setAttribute("ACTION", new PdfAction(url.toExternalForm()));
    }

    public Chunk setAnchor(String url) {
        return this.setAttribute("ACTION", new PdfAction(url));
    }

    public Chunk setLocalGoto(String name) {
        return this.setAttribute("LOCALGOTO", name);
    }

    public Chunk setBackground(Color color) {
        return this.setBackground(color, 0.0f, 0.0f, 0.0f, 0.0f);
    }

    public Chunk setBackground(Color color, float extraLeft, float extraBottom, float extraRight, float extraTop) {
        return this.setAttribute("BACKGROUND", new Object[]{color, new float[]{extraLeft, extraBottom, extraRight, extraTop}});
    }

    public Chunk setUnderline(float thickness, float yPosition) {
        return this.setUnderline(null, thickness, 0.0f, yPosition, 0.0f, 0);
    }

    public Chunk setUnderline(Color color, float thickness, float thicknessMul, float yPosition, float yPositionMul, int cap) {
        if (this.attributes == null) {
            this.attributes = new HashMap();
        }
        Object[] obj = new Object[]{color, new float[]{thickness, thicknessMul, yPosition, yPositionMul, cap}};
        Object[][] unders = Chunk.addToArray((Object[][])this.attributes.get("UNDERLINE"), obj);
        return this.setAttribute("UNDERLINE", unders);
    }

    public static Object[][] addToArray(Object[][] original, Object[] item) {
        if (original == null) {
            original = new Object[][]{item};
            return original;
        }
        Object[][] original2 = new Object[original.length + 1][];
        System.arraycopy(original, 0, original2, 0, original.length);
        original2[original.length] = item;
        return original2;
    }

    public Chunk setAnnotation(PdfAnnotation annotation) {
        return this.setAttribute("PDFANNOTATION", annotation);
    }

    public Chunk setHyphenation(HyphenationEvent hyphenation) {
        return this.setAttribute("HYPHENATION", hyphenation);
    }

    public Chunk setRemoteGoto(String filename, String name) {
        return this.setAttribute("REMOTEGOTO", new Object[]{filename, name});
    }

    public Chunk setRemoteGoto(String filename, int page) {
        return this.setAttribute("REMOTEGOTO", new Object[]{filename, new Integer(page)});
    }

    public Chunk setLocalDestination(String name) {
        return this.setAttribute("LOCALDESTINATION", name);
    }

    public Chunk setGenericTag(String text) {
        return this.setAttribute("GENERICTAG", text);
    }

    public Chunk setSplitCharacter(SplitCharacter splitCharacter) {
        return this.setAttribute("SPLITCHARACTER", splitCharacter);
    }

    public Chunk setNewPage() {
        return this.setAttribute("NEWPAGE", null);
    }

    private Chunk setAttribute(String name, Object obj) {
        if (this.attributes == null) {
            this.attributes = new HashMap();
        }
        this.attributes.put(name, obj);
        return this;
    }

    public HashMap getAttributes() {
        return this.attributes;
    }

    public boolean hasAttributes() {
        if (this.attributes != null) {
            return true;
        }
        return false;
    }

    public Image getImage() {
        if (this.attributes == null) {
            return null;
        }
        Object[] obj = (Object[])this.attributes.get("IMAGE");
        if (obj == null) {
            return null;
        }
        return (Image)obj[0];
    }

    public static boolean isTag(String tag) {
        return "chunk".equals(tag);
    }

    public void setMarkupAttribute(String name, String value) {
        if (this.markupAttributes == null) {
            this.markupAttributes = new Properties();
        }
        this.markupAttributes.put(name, value);
    }

    public void setMarkupAttributes(Properties markupAttributes) {
        this.markupAttributes = markupAttributes;
    }

    public String getMarkupAttribute(String name) {
        return this.markupAttributes == null ? null : String.valueOf(this.markupAttributes.get(name));
    }

    public Set getMarkupAttributeNames() {
        return Chunk.getKeySet(this.markupAttributes);
    }

    public Properties getMarkupAttributes() {
        return this.markupAttributes;
    }

    public static Set getKeySet(Hashtable table) {
        return table == null ? Collections.EMPTY_SET : table.keySet();
    }
}

