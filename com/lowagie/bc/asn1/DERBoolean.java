/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1OctetString;
import com.lowagie.bc.asn1.ASN1TaggedObject;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROutputStream;
import java.io.IOException;

public class DERBoolean
extends DERObject {
    byte value;
    public static final DERBoolean FALSE = new DERBoolean(false);
    public static final DERBoolean TRUE = new DERBoolean(true);

    public static DERBoolean getInstance(Object obj) {
        if (obj == null || obj instanceof DERBoolean) {
            return (DERBoolean)obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERBoolean(((ASN1OctetString)obj).getOctets());
        }
        if (obj instanceof ASN1TaggedObject) {
            return DERBoolean.getInstance(((ASN1TaggedObject)obj).getObject());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERBoolean getInstance(boolean value) {
        return value ? TRUE : FALSE;
    }

    public static DERBoolean getInstance(ASN1TaggedObject obj, boolean explicit) {
        return DERBoolean.getInstance(obj.getObject());
    }

    public DERBoolean(byte[] value) {
        this.value = value[0];
    }

    public DERBoolean(boolean value) {
        this.value = value ? -1 : 0;
    }

    public boolean isTrue() {
        if (this.value != 0) {
            return true;
        }
        return false;
    }

    void encode(DEROutputStream out) throws IOException {
        byte[] bytes = new byte[]{this.value};
        out.writeEncoded(1, bytes);
    }

    public boolean equals(Object o) {
        if (!(o != null && o instanceof DERBoolean)) {
            return false;
        }
        if (this.value == ((DERBoolean)o).value) {
            return true;
        }
        return false;
    }
}

