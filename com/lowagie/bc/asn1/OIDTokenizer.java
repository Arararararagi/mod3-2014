/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

public class OIDTokenizer {
    private String oid;
    private int index;

    public OIDTokenizer(String oid) {
        this.oid = oid;
        this.index = 0;
    }

    public boolean hasMoreTokens() {
        if (this.index != -1) {
            return true;
        }
        return false;
    }

    public String nextToken() {
        if (this.index == -1) {
            return null;
        }
        int end = this.oid.indexOf(46, this.index);
        if (end == -1) {
            String token = this.oid.substring(this.index);
            this.index = -1;
            return token;
        }
        String token = this.oid.substring(this.index, end);
        this.index = end + 1;
        return token;
    }
}

