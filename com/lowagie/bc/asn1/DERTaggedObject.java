/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1TaggedObject;
import com.lowagie.bc.asn1.DEREncodable;
import com.lowagie.bc.asn1.DEROutputStream;
import com.lowagie.bc.asn1.DERSequence;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class DERTaggedObject
extends ASN1TaggedObject {
    public DERTaggedObject(int tagNo, DEREncodable obj) {
        super(tagNo, obj);
    }

    public DERTaggedObject(boolean explicit, int tagNo, DEREncodable obj) {
        super(explicit, tagNo, obj);
    }

    public DERTaggedObject(int tagNo) {
        super(false, tagNo, new DERSequence());
    }

    void encode(DEROutputStream out) throws IOException {
        if (!this.empty) {
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            DEROutputStream dOut = new DEROutputStream(bOut);
            dOut.writeObject(this.obj);
            dOut.close();
            byte[] bytes = bOut.toByteArray();
            if (this.explicit) {
                out.writeEncoded(160 | this.tagNo, bytes);
            } else {
                bytes[0] = (bytes[0] & 32) != 0 ? (byte)(160 | this.tagNo) : (byte)(128 | this.tagNo);
                out.write(bytes);
            }
        } else {
            out.writeEncoded(160 | this.tagNo, new byte[0]);
        }
    }
}

