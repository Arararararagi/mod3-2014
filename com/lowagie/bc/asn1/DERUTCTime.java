/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1OctetString;
import com.lowagie.bc.asn1.ASN1TaggedObject;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class DERUTCTime
extends DERObject {
    String time;

    public static DERUTCTime getInstance(Object obj) {
        if (obj == null || obj instanceof DERUTCTime) {
            return (DERUTCTime)obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERUTCTime(((ASN1OctetString)obj).getOctets());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERUTCTime getInstance(ASN1TaggedObject obj, boolean explicit) {
        return DERUTCTime.getInstance(obj.getObject());
    }

    public DERUTCTime(String time) {
        this.time = time;
    }

    public DERUTCTime(Date time) {
        SimpleDateFormat dateF = new SimpleDateFormat("yyMMddHHmmss'Z'");
        dateF.setTimeZone(new SimpleTimeZone(0, "Z"));
        this.time = dateF.format(time);
    }

    DERUTCTime(byte[] bytes) {
        char[] dateC = new char[bytes.length];
        for (int i = 0; i != dateC.length; ++i) {
            dateC[i] = (char)(bytes[i] & 255);
        }
        this.time = new String(dateC);
    }

    public String getTime() {
        if (this.time.length() == 11) {
            return String.valueOf(this.time.substring(0, 10)) + "00GMT+00:00";
        }
        if (this.time.length() == 13) {
            return String.valueOf(this.time.substring(0, 12)) + "GMT+00:00";
        }
        if (this.time.length() == 17) {
            return String.valueOf(this.time.substring(0, 12)) + "GMT" + this.time.substring(12, 15) + ":" + this.time.substring(15, 17);
        }
        return this.time;
    }

    public String getAdjustedTime() {
        String d = this.getTime();
        if (d.charAt(0) < '5') {
            return "20" + d;
        }
        return "19" + d;
    }

    private byte[] getOctets() {
        char[] cs = this.time.toCharArray();
        byte[] bs = new byte[cs.length];
        for (int i = 0; i != cs.length; ++i) {
            bs[i] = (byte)cs[i];
        }
        return bs;
    }

    void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(23, this.getOctets());
    }

    public boolean equals(Object o) {
        if (!(o != null && o instanceof DERUTCTime)) {
            return false;
        }
        return this.time.equals(((DERUTCTime)o).time);
    }
}

