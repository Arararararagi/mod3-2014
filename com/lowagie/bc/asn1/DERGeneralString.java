/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1OctetString;
import com.lowagie.bc.asn1.ASN1TaggedObject;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROutputStream;
import com.lowagie.bc.asn1.DERString;
import java.io.IOException;

public class DERGeneralString
extends DERObject
implements DERString {
    private String string;

    public static DERGeneralString getInstance(Object obj) {
        if (obj == null || obj instanceof DERGeneralString) {
            return (DERGeneralString)obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERGeneralString(((ASN1OctetString)obj).getOctets());
        }
        if (obj instanceof ASN1TaggedObject) {
            return DERGeneralString.getInstance(((ASN1TaggedObject)obj).getObject());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERGeneralString getInstance(ASN1TaggedObject obj, boolean explicit) {
        return DERGeneralString.getInstance(obj.getObject());
    }

    public DERGeneralString(byte[] string) {
        char[] cs = new char[string.length];
        for (int i = 0; i != cs.length; ++i) {
            cs[i] = (char)(string[i] & 255);
        }
        this.string = new String(cs);
    }

    public DERGeneralString(String string) {
        this.string = string;
    }

    public String getString() {
        return this.string;
    }

    public byte[] getOctets() {
        char[] cs = this.string.toCharArray();
        byte[] bs = new byte[cs.length];
        for (int i = 0; i != cs.length; ++i) {
            bs[i] = (byte)cs[i];
        }
        return bs;
    }

    void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(27, this.getOctets());
    }

    public int hashCode() {
        return this.getString().hashCode();
    }

    public boolean equals(Object o) {
        if (!(o instanceof DERGeneralString)) {
            return false;
        }
        DERGeneralString s = (DERGeneralString)o;
        return this.getString().equals(s.getString());
    }
}

