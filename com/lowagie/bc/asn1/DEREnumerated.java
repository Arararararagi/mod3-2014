/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1OctetString;
import com.lowagie.bc.asn1.ASN1TaggedObject;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROutputStream;
import java.io.IOException;
import java.math.BigInteger;

public class DEREnumerated
extends DERObject {
    byte[] bytes;

    public static DEREnumerated getInstance(Object obj) {
        if (obj == null || obj instanceof DEREnumerated) {
            return (DEREnumerated)obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DEREnumerated(((ASN1OctetString)obj).getOctets());
        }
        if (obj instanceof ASN1TaggedObject) {
            return DEREnumerated.getInstance(((ASN1TaggedObject)obj).getObject());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DEREnumerated getInstance(ASN1TaggedObject obj, boolean explicit) {
        return DEREnumerated.getInstance(obj.getObject());
    }

    public DEREnumerated(int value) {
        this.bytes = BigInteger.valueOf(value).toByteArray();
    }

    public DEREnumerated(BigInteger value) {
        this.bytes = value.toByteArray();
    }

    public DEREnumerated(byte[] bytes) {
        this.bytes = bytes;
    }

    public BigInteger getValue() {
        return new BigInteger(this.bytes);
    }

    void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(10, this.bytes);
    }

    public boolean equals(Object o) {
        if (!(o != null && o instanceof DEREnumerated)) {
            return false;
        }
        DEREnumerated other = (DEREnumerated)o;
        if (this.bytes.length != other.bytes.length) {
            return false;
        }
        for (int i = 0; i != this.bytes.length; ++i) {
            if (this.bytes[i] == other.bytes[i]) continue;
            return false;
        }
        return true;
    }
}

