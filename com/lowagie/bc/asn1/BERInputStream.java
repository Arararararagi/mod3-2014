/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1EncodableVector;
import com.lowagie.bc.asn1.BERConstructedOctetString;
import com.lowagie.bc.asn1.BERConstructedSequence;
import com.lowagie.bc.asn1.BERSet;
import com.lowagie.bc.asn1.BERTaggedObject;
import com.lowagie.bc.asn1.DEREncodable;
import com.lowagie.bc.asn1.DEREncodableVector;
import com.lowagie.bc.asn1.DERInputStream;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROctetString;
import com.lowagie.bc.asn1.DEROutputStream;
import com.lowagie.bc.asn1.DERTaggedObject;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public class BERInputStream
extends DERInputStream {
    private DERObject END_OF_STREAM;

    public BERInputStream(InputStream is) {
        super(is);
        this.END_OF_STREAM = new DERObject(){

            void encode(DEROutputStream out) throws IOException {
                throw new IOException("Eeek!");
            }
        };
    }

    private byte[] readIndefiniteLengthFully() throws IOException {
        int b;
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        int b1 = this.read();
        while ((b = this.read()) >= 0) {
            if (b1 == 0 && b == 0) break;
            bOut.write(b1);
            b1 = b;
        }
        return bOut.toByteArray();
    }

    private BERConstructedOctetString buildConstructedOctetString() throws IOException {
        DERObject o;
        Vector<DERObject> octs = new Vector<DERObject>();
        while ((o = this.readObject()) != this.END_OF_STREAM) {
            octs.addElement(o);
        }
        return new BERConstructedOctetString(octs);
    }

    public DERObject readObject() throws IOException {
        int tag = this.read();
        if (tag == -1) {
            throw new EOFException();
        }
        int length = this.readLength();
        if (length < 0) {
            switch (tag) {
                case 5: {
                    return null;
                }
                case 48: {
                    DERObject obj;
                    BERConstructedSequence seq = new BERConstructedSequence();
                    while ((obj = this.readObject()) != this.END_OF_STREAM) {
                        seq.addObject(obj);
                    }
                    return seq;
                }
                case 36: {
                    return this.buildConstructedOctetString();
                }
                case 49: {
                    DERObject obj;
                    ASN1EncodableVector v = new ASN1EncodableVector();
                    while ((obj = this.readObject()) != this.END_OF_STREAM) {
                        v.add(obj);
                    }
                    return new BERSet(v);
                }
            }
            if ((tag & 128) != 0) {
                if ((tag & 31) == 31) {
                    throw new IOException("unsupported high tag encountered");
                }
                if ((tag & 32) == 0) {
                    byte[] bytes = this.readIndefiniteLengthFully();
                    return new BERTaggedObject(false, tag & 31, new DEROctetString(bytes));
                }
                DERObject dObj = this.readObject();
                if (dObj == this.END_OF_STREAM) {
                    return new DERTaggedObject(tag & 31);
                }
                DERObject next = this.readObject();
                if (next == this.END_OF_STREAM) {
                    return new BERTaggedObject(tag & 31, dObj);
                }
                BERConstructedSequence seq = new BERConstructedSequence();
                seq.addObject(dObj);
                do {
                    seq.addObject(next);
                } while ((next = this.readObject()) != this.END_OF_STREAM);
                return new BERTaggedObject(false, tag & 31, seq);
            }
            throw new IOException("unknown BER object encountered");
        }
        if (tag == 0 && length == 0) {
            return this.END_OF_STREAM;
        }
        byte[] bytes = new byte[length];
        this.readFully(bytes);
        return this.buildObject(tag, bytes);
    }

}

