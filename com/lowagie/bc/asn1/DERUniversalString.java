/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1OctetString;
import com.lowagie.bc.asn1.ASN1OutputStream;
import com.lowagie.bc.asn1.ASN1TaggedObject;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROutputStream;
import com.lowagie.bc.asn1.DERString;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class DERUniversalString
extends DERObject
implements DERString {
    private static final char[] table = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private byte[] string;

    public static DERUniversalString getInstance(Object obj) {
        if (obj == null || obj instanceof DERUniversalString) {
            return (DERUniversalString)obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERUniversalString(((ASN1OctetString)obj).getOctets());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERUniversalString getInstance(ASN1TaggedObject obj, boolean explicit) {
        return DERUniversalString.getInstance(obj.getObject());
    }

    public DERUniversalString(byte[] string) {
        this.string = string;
    }

    public String getString() {
        StringBuffer buf = new StringBuffer("#");
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        ASN1OutputStream aOut = new ASN1OutputStream(bOut);
        try {
            aOut.writeObject(this);
        }
        catch (IOException e) {
            throw new RuntimeException("internal error encoding BitString");
        }
        byte[] string = bOut.toByteArray();
        for (int i = 0; i != string.length; ++i) {
            buf.append(table[(string[i] >>> 4) % 15]);
            buf.append(table[string[i] & 15]);
        }
        return buf.toString();
    }

    public byte[] getOctets() {
        return this.string;
    }

    void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(28, this.getOctets());
    }

    public boolean equals(Object o) {
        if (!(o != null && o instanceof DERUniversalString)) {
            return false;
        }
        return this.getString().equals(((DERUniversalString)o).getString());
    }
}

