/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1OutputStream;
import com.lowagie.bc.asn1.BEROutputStream;
import com.lowagie.bc.asn1.DERConstructedSequence;
import com.lowagie.bc.asn1.DEROutputStream;
import java.io.IOException;
import java.util.Enumeration;

public class BERConstructedSequence
extends DERConstructedSequence {
    void encode(DEROutputStream out) throws IOException {
        if (out instanceof ASN1OutputStream || out instanceof BEROutputStream) {
            out.write(48);
            out.write(128);
            Enumeration e = this.getObjects();
            while (e.hasMoreElements()) {
                out.writeObject(e.nextElement());
            }
            out.write(0);
            out.write(0);
        } else {
            super.encode(out);
        }
    }
}

