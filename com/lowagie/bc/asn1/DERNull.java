/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1Null;
import com.lowagie.bc.asn1.DEROutputStream;
import java.io.IOException;

public class DERNull
extends ASN1Null {
    byte[] zeroBytes = new byte[0];

    void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(5, this.zeroBytes);
    }

    public boolean equals(Object o) {
        if (!(o != null && o instanceof DERNull)) {
            return false;
        }
        return true;
    }
}

