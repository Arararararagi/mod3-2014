/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1OutputStream;
import com.lowagie.bc.asn1.BEROutputStream;
import com.lowagie.bc.asn1.DERNull;
import com.lowagie.bc.asn1.DEROutputStream;
import java.io.IOException;

public class BERNull
extends DERNull {
    void encode(DEROutputStream out) throws IOException {
        if (out instanceof ASN1OutputStream || out instanceof BEROutputStream) {
            out.write(5);
            out.write(0);
            out.write(0);
        } else {
            super.encode(out);
        }
    }
}

