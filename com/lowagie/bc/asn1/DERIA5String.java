/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1OctetString;
import com.lowagie.bc.asn1.ASN1TaggedObject;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROutputStream;
import com.lowagie.bc.asn1.DERString;
import java.io.IOException;

public class DERIA5String
extends DERObject
implements DERString {
    String string;

    public static DERIA5String getInstance(Object obj) {
        if (obj == null || obj instanceof DERIA5String) {
            return (DERIA5String)obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERIA5String(((ASN1OctetString)obj).getOctets());
        }
        if (obj instanceof ASN1TaggedObject) {
            return DERIA5String.getInstance(((ASN1TaggedObject)obj).getObject());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERIA5String getInstance(ASN1TaggedObject obj, boolean explicit) {
        return DERIA5String.getInstance(obj.getObject());
    }

    public DERIA5String(byte[] string) {
        char[] cs = new char[string.length];
        for (int i = 0; i != cs.length; ++i) {
            cs[i] = (char)(string[i] & 255);
        }
        this.string = new String(cs);
    }

    public DERIA5String(String string) {
        this.string = string;
    }

    public String getString() {
        return this.string;
    }

    public byte[] getOctets() {
        char[] cs = this.string.toCharArray();
        byte[] bs = new byte[cs.length];
        for (int i = 0; i != cs.length; ++i) {
            bs[i] = (byte)cs[i];
        }
        return bs;
    }

    void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(22, this.getOctets());
    }

    public int hashCode() {
        return this.getString().hashCode();
    }

    public boolean equals(Object o) {
        if (!(o instanceof DERIA5String)) {
            return false;
        }
        DERIA5String s = (DERIA5String)o;
        return this.getString().equals(s.getString());
    }
}

