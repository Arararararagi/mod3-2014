/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1EncodableVector;
import com.lowagie.bc.asn1.BERConstructedOctetString;
import com.lowagie.bc.asn1.BERNull;
import com.lowagie.bc.asn1.BERSequence;
import com.lowagie.bc.asn1.BERSet;
import com.lowagie.bc.asn1.BERTaggedObject;
import com.lowagie.bc.asn1.DERApplicationSpecific;
import com.lowagie.bc.asn1.DERBMPString;
import com.lowagie.bc.asn1.DERBitString;
import com.lowagie.bc.asn1.DERBoolean;
import com.lowagie.bc.asn1.DEREncodable;
import com.lowagie.bc.asn1.DEREncodableVector;
import com.lowagie.bc.asn1.DEREnumerated;
import com.lowagie.bc.asn1.DERGeneralString;
import com.lowagie.bc.asn1.DERGeneralizedTime;
import com.lowagie.bc.asn1.DERIA5String;
import com.lowagie.bc.asn1.DERInputStream;
import com.lowagie.bc.asn1.DERInteger;
import com.lowagie.bc.asn1.DERNull;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DERObjectIdentifier;
import com.lowagie.bc.asn1.DEROctetString;
import com.lowagie.bc.asn1.DEROutputStream;
import com.lowagie.bc.asn1.DERPrintableString;
import com.lowagie.bc.asn1.DERSequence;
import com.lowagie.bc.asn1.DERSet;
import com.lowagie.bc.asn1.DERT61String;
import com.lowagie.bc.asn1.DERTaggedObject;
import com.lowagie.bc.asn1.DERUTCTime;
import com.lowagie.bc.asn1.DERUTF8String;
import com.lowagie.bc.asn1.DERUniversalString;
import com.lowagie.bc.asn1.DERUnknownTag;
import com.lowagie.bc.asn1.DERVisibleString;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public class ASN1InputStream
extends DERInputStream {
    private DERObject END_OF_STREAM;
    boolean eofFound;

    public ASN1InputStream(InputStream is) {
        super(is);
        this.END_OF_STREAM = new DERObject(){

            void encode(DEROutputStream out) throws IOException {
                throw new IOException("Eeek!");
            }
        };
        this.eofFound = false;
    }

    protected int readLength() throws IOException {
        int length = this.read();
        if (length < 0) {
            throw new IOException("EOF found when length expected");
        }
        if (length == 128) {
            return -1;
        }
        if (length > 127) {
            int size = length & 127;
            length = 0;
            for (int i = 0; i < size; ++i) {
                int next = this.read();
                if (next < 0) {
                    throw new IOException("EOF found reading length");
                }
                length = (length << 8) + next;
            }
        }
        return length;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    protected void readFully(byte[] bytes) throws IOException {
        left = bytes.length;
        if (left != 0) ** GOTO lbl6
        return;
lbl-1000: // 1 sources:
        {
            if ((left-=len) != 0) continue;
            return;
lbl6: // 2 sources:
            ** while ((len = this.read((byte[])bytes, (int)(bytes.length - left), (int)left)) > 0)
        }
lbl7: // 1 sources:
        if (left == 0) return;
        throw new EOFException("EOF encountered in middle of object");
    }

    protected DERObject buildObject(int tag, byte[] bytes) throws IOException {
        if ((tag & 64) != 0) {
            return new DERApplicationSpecific(tag, bytes);
        }
        switch (tag) {
            case 5: {
                return new DERNull();
            }
            case 48: {
                ByteArrayInputStream bIn = new ByteArrayInputStream(bytes);
                ASN1InputStream aIn = new ASN1InputStream(bIn);
                ASN1EncodableVector v = new ASN1EncodableVector();
                DERObject obj = aIn.readObject();
                while (obj != null) {
                    v.add(obj);
                    obj = aIn.readObject();
                }
                return new DERSequence(v);
            }
            case 49: {
                ByteArrayInputStream bIn = new ByteArrayInputStream(bytes);
                ASN1InputStream aIn = new ASN1InputStream(bIn);
                ASN1EncodableVector v = new ASN1EncodableVector();
                DERObject obj = aIn.readObject();
                while (obj != null) {
                    v.add(obj);
                    obj = aIn.readObject();
                }
                return new DERSet(v);
            }
            case 1: {
                return new DERBoolean(bytes);
            }
            case 2: {
                return new DERInteger(bytes);
            }
            case 10: {
                return new DEREnumerated(bytes);
            }
            case 6: {
                return new DERObjectIdentifier(bytes);
            }
            case 3: {
                byte padBits = bytes[0];
                byte[] data = new byte[bytes.length - 1];
                System.arraycopy(bytes, 1, data, 0, bytes.length - 1);
                return new DERBitString(data, (int)padBits);
            }
            case 12: {
                return new DERUTF8String(bytes);
            }
            case 19: {
                return new DERPrintableString(bytes);
            }
            case 22: {
                return new DERIA5String(bytes);
            }
            case 20: {
                return new DERT61String(bytes);
            }
            case 26: {
                return new DERVisibleString(bytes);
            }
            case 27: {
                return new DERGeneralString(bytes);
            }
            case 28: {
                return new DERUniversalString(bytes);
            }
            case 30: {
                return new DERBMPString(bytes);
            }
            case 4: {
                return new DEROctetString(bytes);
            }
            case 23: {
                return new DERUTCTime(bytes);
            }
            case 24: {
                return new DERGeneralizedTime(bytes);
            }
        }
        if ((tag & 128) != 0) {
            int tagNo = tag & 31;
            if (tagNo == 31) {
                int idx = 0;
                tagNo = 0;
                while ((bytes[idx] & 128) != 0) {
                    tagNo|=bytes[idx++] & 127;
                    tagNo<<=7;
                }
                tagNo|=bytes[idx] & 127;
                byte[] tmp = bytes;
                bytes = new byte[tmp.length - (idx + 1)];
                System.arraycopy(tmp, idx + 1, bytes, 0, bytes.length);
            }
            if (bytes.length == 0) {
                if ((tag & 32) == 0) {
                    return new DERTaggedObject(false, tagNo, new DERNull());
                }
                return new DERTaggedObject(false, tagNo, new DERSequence());
            }
            if ((tag & 32) == 0) {
                return new DERTaggedObject(false, tagNo, new DEROctetString(bytes));
            }
            ByteArrayInputStream bIn = new ByteArrayInputStream(bytes);
            ASN1InputStream aIn = new ASN1InputStream(bIn);
            DERObject dObj = aIn.readObject();
            if (aIn.available() == 0) {
                return new DERTaggedObject(tagNo, dObj);
            }
            ASN1EncodableVector v = new ASN1EncodableVector();
            while (dObj != null) {
                v.add(dObj);
                dObj = aIn.readObject();
            }
            return new DERTaggedObject(false, tagNo, new DERSequence(v));
        }
        return new DERUnknownTag(tag, bytes);
    }

    private byte[] readIndefiniteLengthFully() throws IOException {
        int b;
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        int b1 = this.read();
        while ((b = this.read()) >= 0) {
            if (b1 == 0 && b == 0) break;
            bOut.write(b1);
            b1 = b;
        }
        return bOut.toByteArray();
    }

    private BERConstructedOctetString buildConstructedOctetString() throws IOException {
        DERObject o;
        Vector<DERObject> octs = new Vector<DERObject>();
        while ((o = this.readObject()) != this.END_OF_STREAM) {
            octs.addElement(o);
        }
        return new BERConstructedOctetString(octs);
    }

    public DERObject readObject() throws IOException {
        int tag = this.read();
        if (tag == -1) {
            if (this.eofFound) {
                throw new EOFException("attempt to read past end of file.");
            }
            this.eofFound = true;
            return null;
        }
        int length = this.readLength();
        if (length < 0) {
            switch (tag) {
                case 5: {
                    return new BERNull();
                }
                case 48: {
                    DERObject obj;
                    ASN1EncodableVector v = new ASN1EncodableVector();
                    while ((obj = this.readObject()) != this.END_OF_STREAM) {
                        v.add(obj);
                    }
                    return new BERSequence(v);
                }
                case 49: {
                    DERObject obj;
                    ASN1EncodableVector v = new ASN1EncodableVector();
                    while ((obj = this.readObject()) != this.END_OF_STREAM) {
                        v.add(obj);
                    }
                    return new BERSet(v);
                }
                case 36: {
                    return this.buildConstructedOctetString();
                }
            }
            if ((tag & 128) != 0) {
                int tagNo = tag & 31;
                if (tagNo == 31) {
                    int b = this.read();
                    tagNo = 0;
                    while (b >= 0 && (b & 128) != 0) {
                        tagNo|=b & 127;
                        tagNo<<=7;
                        b = this.read();
                    }
                    tagNo|=b & 127;
                }
                if ((tag & 32) == 0) {
                    byte[] bytes = this.readIndefiniteLengthFully();
                    return new BERTaggedObject(false, tagNo, new DEROctetString(bytes));
                }
                DERObject dObj = this.readObject();
                if (dObj == this.END_OF_STREAM) {
                    return new DERTaggedObject(tagNo);
                }
                DERObject next = this.readObject();
                if (next == this.END_OF_STREAM) {
                    return new BERTaggedObject(tagNo, dObj);
                }
                ASN1EncodableVector v = new ASN1EncodableVector();
                v.add(dObj);
                do {
                    v.add(next);
                } while ((next = this.readObject()) != this.END_OF_STREAM);
                return new BERTaggedObject(false, tagNo, new BERSequence(v));
            }
            throw new IOException("unknown BER object encountered");
        }
        if (tag == 0 && length == 0) {
            return this.END_OF_STREAM;
        }
        byte[] bytes = new byte[length];
        this.readFully(bytes);
        return this.buildObject(tag, bytes);
    }

}

