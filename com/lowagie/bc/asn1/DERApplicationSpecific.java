/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1InputStream;
import com.lowagie.bc.asn1.DEREncodable;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DERApplicationSpecific
extends DERObject {
    private int tag;
    private byte[] octets;

    public DERApplicationSpecific(int tag, byte[] octets) {
        this.tag = tag;
        this.octets = octets;
    }

    public DERApplicationSpecific(int tag, DEREncodable object) throws IOException {
        this.tag = tag | 32;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DEROutputStream dos = new DEROutputStream(baos);
        dos.writeObject(object);
        this.octets = baos.toByteArray();
    }

    public boolean isConstructed() {
        if ((this.tag & 32) != 0) {
            return true;
        }
        return false;
    }

    public byte[] getContents() {
        return this.octets;
    }

    public int getApplicationTag() {
        return this.tag & 31;
    }

    public DERObject getObject() throws IOException {
        return new ASN1InputStream(new ByteArrayInputStream(this.getContents())).readObject();
    }

    void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(64 | this.tag, this.octets);
    }
}

