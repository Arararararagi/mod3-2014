/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1OutputStream;
import com.lowagie.bc.asn1.BEROutputStream;
import com.lowagie.bc.asn1.DEREncodable;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROctetString;
import com.lowagie.bc.asn1.DEROutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

public class BERConstructedOctetString
extends DEROctetString {
    private Vector octs;

    private static byte[] toBytes(Vector octs) {
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        for (int i = 0; i != octs.size(); ++i) {
            DEROctetString o = (DEROctetString)octs.elementAt(i);
            try {
                bOut.write(o.getOctets());
                continue;
            }
            catch (IOException e) {
                throw new RuntimeException("exception converting octets " + e.toString());
            }
        }
        return bOut.toByteArray();
    }

    public BERConstructedOctetString(byte[] string) {
        super(string);
    }

    public BERConstructedOctetString(Vector octs) {
        super(BERConstructedOctetString.toBytes(octs));
        this.octs = octs;
    }

    public BERConstructedOctetString(DERObject obj) {
        super(obj);
    }

    public BERConstructedOctetString(DEREncodable obj) {
        super(obj.getDERObject());
    }

    public byte[] getOctets() {
        return this.string;
    }

    public Enumeration getObjects() {
        if (this.octs == null) {
            return this.generateOcts().elements();
        }
        return this.octs.elements();
    }

    private Vector generateOcts() {
        byte[] nStr;
        int start = 0;
        int end = 0;
        Vector<DEROctetString> vec = new Vector<DEROctetString>();
        while (end + 1 < this.string.length) {
            if (this.string[end] == 0 && this.string[end + 1] == 0) {
                nStr = new byte[end - start + 1];
                System.arraycopy(this.string, start, nStr, 0, nStr.length);
                vec.addElement(new DEROctetString(nStr));
                start = end + 1;
            }
            ++end;
        }
        nStr = new byte[this.string.length - start];
        System.arraycopy(this.string, start, nStr, 0, nStr.length);
        vec.addElement(new DEROctetString(nStr));
        return vec;
    }

    public void encode(DEROutputStream out) throws IOException {
        if (out instanceof ASN1OutputStream || out instanceof BEROutputStream) {
            out.write(36);
            out.write(128);
            if (this.octs != null) {
                for (int i = 0; i != this.octs.size(); ++i) {
                    out.writeObject(this.octs.elementAt(i));
                }
            } else {
                byte[] nStr;
                int start = 0;
                int end = 0;
                while (end + 1 < this.string.length) {
                    if (this.string[end] == 0 && this.string[end + 1] == 0) {
                        nStr = new byte[end - start + 1];
                        System.arraycopy(this.string, start, nStr, 0, nStr.length);
                        out.writeObject(new DEROctetString(nStr));
                        start = end + 1;
                    }
                    ++end;
                }
                nStr = new byte[this.string.length - start];
                System.arraycopy(this.string, start, nStr, 0, nStr.length);
                out.writeObject(new DEROctetString(nStr));
            }
            out.write(0);
            out.write(0);
        } else {
            super.encode(out);
        }
    }
}

