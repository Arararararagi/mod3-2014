/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1OctetString;
import com.lowagie.bc.asn1.ASN1TaggedObject;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROutputStream;
import com.lowagie.bc.asn1.DERString;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class DERUTF8String
extends DERObject
implements DERString {
    String string;

    public static DERUTF8String getInstance(Object obj) {
        if (obj == null || obj instanceof DERUTF8String) {
            return (DERUTF8String)obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERUTF8String(((ASN1OctetString)obj).getOctets());
        }
        if (obj instanceof ASN1TaggedObject) {
            return DERUTF8String.getInstance(((ASN1TaggedObject)obj).getObject());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERUTF8String getInstance(ASN1TaggedObject obj, boolean explicit) {
        return DERUTF8String.getInstance(obj.getObject());
    }

    DERUTF8String(byte[] string) {
        int i = 0;
        int length = 0;
        while (i < string.length) {
            ++length;
            if ((string[i] & 224) == 224) {
                i+=3;
                continue;
            }
            if ((string[i] & 192) == 192) {
                i+=2;
                continue;
            }
            ++i;
        }
        char[] cs = new char[length];
        i = 0;
        length = 0;
        while (i < string.length) {
            char ch;
            if ((string[i] & 224) == 224) {
                ch = (char)((string[i] & 31) << 12 | (string[i + 1] & 63) << 6 | string[i + 2] & 63);
                i+=3;
            } else if ((string[i] & 192) == 192) {
                ch = (char)((string[i] & 63) << 6 | string[i + 1] & 63);
                i+=2;
            } else {
                ch = (char)(string[i] & 255);
                ++i;
            }
            cs[length++] = ch;
        }
        this.string = new String(cs);
    }

    public DERUTF8String(String string) {
        this.string = string;
    }

    public String getString() {
        return this.string;
    }

    public int hashCode() {
        return this.getString().hashCode();
    }

    public boolean equals(Object o) {
        if (!(o instanceof DERUTF8String)) {
            return false;
        }
        DERUTF8String s = (DERUTF8String)o;
        return this.getString().equals(s.getString());
    }

    void encode(DEROutputStream out) throws IOException {
        char[] c = this.string.toCharArray();
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        for (int i = 0; i != c.length; ++i) {
            char ch = c[i];
            if (ch < '') {
                bOut.write(ch);
                continue;
            }
            if (ch < '\u0800') {
                bOut.write(192 | ch >> 6);
                bOut.write(128 | ch & 63);
                continue;
            }
            bOut.write(224 | ch >> 12);
            bOut.write(128 | ch >> 6 & 63);
            bOut.write(128 | ch & 63);
        }
        out.writeEncoded(12, bOut.toByteArray());
    }
}

