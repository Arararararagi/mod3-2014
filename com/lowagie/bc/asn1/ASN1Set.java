/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1EncodableVector;
import com.lowagie.bc.asn1.ASN1Sequence;
import com.lowagie.bc.asn1.ASN1TaggedObject;
import com.lowagie.bc.asn1.DEREncodable;
import com.lowagie.bc.asn1.DEREncodableVector;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROutputStream;
import com.lowagie.bc.asn1.DERSet;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

public abstract class ASN1Set
extends DERObject {
    protected Vector set = new Vector();

    public static ASN1Set getInstance(Object obj) {
        if (obj == null || obj instanceof ASN1Set) {
            return (ASN1Set)obj;
        }
        throw new IllegalArgumentException("unknown object in getInstance");
    }

    public static ASN1Set getInstance(ASN1TaggedObject obj, boolean explicit) {
        if (explicit) {
            if (!obj.isExplicit()) {
                throw new IllegalArgumentException("object implicit - explicit expected.");
            }
            return (ASN1Set)obj.getObject();
        }
        if (obj.isExplicit()) {
            DERSet set = new DERSet(obj.getObject());
            return set;
        }
        if (obj.getObject() instanceof ASN1Set) {
            return (ASN1Set)obj.getObject();
        }
        ASN1EncodableVector v = new ASN1EncodableVector();
        if (obj.getObject() instanceof ASN1Sequence) {
            ASN1Sequence s = (ASN1Sequence)obj.getObject();
            Enumeration e = s.getObjects();
            while (e.hasMoreElements()) {
                v.add((DEREncodable)e.nextElement());
            }
            return new DERSet(v);
        }
        throw new IllegalArgumentException("unknown object in getInstanceFromTagged");
    }

    public Enumeration getObjects() {
        return this.set.elements();
    }

    public DEREncodable getObjectAt(int index) {
        return (DEREncodable)this.set.elementAt(index);
    }

    public int size() {
        return this.set.size();
    }

    public int hashCode() {
        Enumeration e = this.getObjects();
        int hashCode = 0;
        while (e.hasMoreElements()) {
            hashCode^=e.nextElement().hashCode();
        }
        return hashCode;
    }

    public boolean equals(Object o) {
        if (!(o != null && o instanceof ASN1Set)) {
            return false;
        }
        ASN1Set other = (ASN1Set)o;
        if (this.size() != other.size()) {
            return false;
        }
        Enumeration s1 = this.getObjects();
        Enumeration s2 = other.getObjects();
        while (s1.hasMoreElements()) {
            if (s1.nextElement().equals(s2.nextElement())) continue;
            return false;
        }
        return true;
    }

    protected void addObject(DEREncodable obj) {
        this.set.addElement(obj);
    }

    abstract void encode(DEROutputStream var1) throws IOException;
}

