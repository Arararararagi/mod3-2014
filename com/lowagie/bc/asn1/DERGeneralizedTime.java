/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1OctetString;
import com.lowagie.bc.asn1.ASN1TaggedObject;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class DERGeneralizedTime
extends DERObject {
    String time;

    public static DERGeneralizedTime getInstance(Object obj) {
        if (obj == null || obj instanceof DERGeneralizedTime) {
            return (DERGeneralizedTime)obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERGeneralizedTime(((ASN1OctetString)obj).getOctets());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERGeneralizedTime getInstance(ASN1TaggedObject obj, boolean explicit) {
        return DERGeneralizedTime.getInstance(obj.getObject());
    }

    public DERGeneralizedTime(String time) {
        this.time = time;
    }

    public DERGeneralizedTime(Date time) {
        SimpleDateFormat dateF = new SimpleDateFormat("yyyyMMddHHmmss'Z'");
        dateF.setTimeZone(new SimpleTimeZone(0, "Z"));
        this.time = dateF.format(time);
    }

    DERGeneralizedTime(byte[] bytes) {
        char[] dateC = new char[bytes.length];
        for (int i = 0; i != dateC.length; ++i) {
            dateC[i] = (char)(bytes[i] & 255);
        }
        this.time = new String(dateC);
    }

    public String getTime() {
        if (this.time.charAt(this.time.length() - 1) == 'Z') {
            return String.valueOf(this.time.substring(0, this.time.length() - 1)) + "GMT+00:00";
        }
        int signPos = this.time.length() - 5;
        char sign = this.time.charAt(signPos);
        if (sign == '-' || sign == '+') {
            return String.valueOf(this.time.substring(0, signPos)) + "GMT" + this.time.substring(signPos, signPos + 3) + ":" + this.time.substring(signPos + 3);
        }
        signPos = this.time.length() - 3;
        sign = this.time.charAt(signPos);
        if (sign == '-' || sign == '+') {
            return String.valueOf(this.time.substring(0, signPos)) + "GMT" + this.time.substring(signPos) + ":00";
        }
        return this.time;
    }

    public Date getDate() throws ParseException {
        SimpleDateFormat dateF = new SimpleDateFormat("yyyyMMddHHmmss'Z'");
        dateF.setTimeZone(new SimpleTimeZone(0, "Z"));
        return dateF.parse(this.time);
    }

    private byte[] getOctets() {
        char[] cs = this.time.toCharArray();
        byte[] bs = new byte[cs.length];
        for (int i = 0; i != cs.length; ++i) {
            bs[i] = (byte)cs[i];
        }
        return bs;
    }

    void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(24, this.getOctets());
    }

    public boolean equals(Object o) {
        if (!(o != null && o instanceof DERGeneralizedTime)) {
            return false;
        }
        return this.time.equals(((DERGeneralizedTime)o).time);
    }
}

