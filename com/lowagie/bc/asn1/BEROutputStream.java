/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.DEREncodable;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class BEROutputStream
extends DEROutputStream {
    public BEROutputStream(OutputStream os) {
        super(os);
    }

    public void writeObject(Object obj) throws IOException {
        if (obj == null) {
            this.writeNull();
        } else if (obj instanceof DERObject) {
            ((DERObject)obj).encode(this);
        } else if (obj instanceof DEREncodable) {
            ((DEREncodable)obj).getDERObject().encode(this);
        } else {
            throw new IOException("object not BEREncodable");
        }
    }
}

