/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1OctetString;
import com.lowagie.bc.asn1.DEREncodable;
import com.lowagie.bc.asn1.DEROutputStream;
import java.io.IOException;

public class DEROctetString
extends ASN1OctetString {
    public DEROctetString(byte[] string) {
        super(string);
    }

    public DEROctetString(DEREncodable obj) {
        super(obj);
    }

    void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(4, this.string);
    }
}

