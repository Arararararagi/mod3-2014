/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROutputStream;
import java.io.IOException;

public class DERUnknownTag
extends DERObject {
    int tag;
    byte[] data;

    public DERUnknownTag(int tag, byte[] data) {
        this.tag = tag;
        this.data = data;
    }

    public int getTag() {
        return this.tag;
    }

    public byte[] getData() {
        return this.data;
    }

    void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(this.tag, this.data);
    }

    public boolean equals(Object o) {
        if (!(o != null && o instanceof DERUnknownTag)) {
            return false;
        }
        DERUnknownTag other = (DERUnknownTag)o;
        if (this.tag != other.tag) {
            return false;
        }
        if (this.data.length != other.data.length) {
            return false;
        }
        for (int i = 0; i < this.data.length; ++i) {
            if (this.data[i] == other.data[i]) continue;
            return false;
        }
        return true;
    }
}

