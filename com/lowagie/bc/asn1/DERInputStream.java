/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1EncodableVector;
import com.lowagie.bc.asn1.BERInputStream;
import com.lowagie.bc.asn1.DERBMPString;
import com.lowagie.bc.asn1.DERBitString;
import com.lowagie.bc.asn1.DERBoolean;
import com.lowagie.bc.asn1.DERConstructedSequence;
import com.lowagie.bc.asn1.DERConstructedSet;
import com.lowagie.bc.asn1.DEREncodable;
import com.lowagie.bc.asn1.DEREncodableVector;
import com.lowagie.bc.asn1.DEREnumerated;
import com.lowagie.bc.asn1.DERGeneralString;
import com.lowagie.bc.asn1.DERGeneralizedTime;
import com.lowagie.bc.asn1.DERIA5String;
import com.lowagie.bc.asn1.DERInteger;
import com.lowagie.bc.asn1.DERNull;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DERObjectIdentifier;
import com.lowagie.bc.asn1.DEROctetString;
import com.lowagie.bc.asn1.DERPrintableString;
import com.lowagie.bc.asn1.DERT61String;
import com.lowagie.bc.asn1.DERTaggedObject;
import com.lowagie.bc.asn1.DERTags;
import com.lowagie.bc.asn1.DERUTCTime;
import com.lowagie.bc.asn1.DERUTF8String;
import com.lowagie.bc.asn1.DERUniversalString;
import com.lowagie.bc.asn1.DERUnknownTag;
import com.lowagie.bc.asn1.DERVisibleString;
import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class DERInputStream
extends FilterInputStream
implements DERTags {
    public DERInputStream(InputStream is) {
        super(is);
    }

    protected int readLength() throws IOException {
        int length = this.read();
        if (length < 0) {
            throw new IOException("EOF found when length expected");
        }
        if (length == 128) {
            return -1;
        }
        if (length > 127) {
            int size = length & 127;
            length = 0;
            for (int i = 0; i < size; ++i) {
                int next = this.read();
                if (next < 0) {
                    throw new IOException("EOF found reading length");
                }
                length = (length << 8) + next;
            }
        }
        return length;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    protected void readFully(byte[] bytes) throws IOException {
        left = bytes.length;
        if (left != 0) ** GOTO lbl8
        return;
lbl-1000: // 1 sources:
        {
            l = this.read(bytes, bytes.length - left, left);
            if (l < 0) {
                throw new EOFException("unexpected end of stream");
            }
            left-=l;
lbl8: // 2 sources:
            ** while (left > 0)
        }
lbl9: // 1 sources:
    }

    protected DERObject buildObject(int tag, byte[] bytes) throws IOException {
        switch (tag) {
            case 5: {
                return null;
            }
            case 48: {
                ByteArrayInputStream bIn = new ByteArrayInputStream(bytes);
                BERInputStream dIn = new BERInputStream(bIn);
                DERConstructedSequence seq = new DERConstructedSequence();
                try {
                    do {
                        DERObject obj = dIn.readObject();
                        seq.addObject(obj);
                    } while (true);
                }
                catch (EOFException ex) {
                    return seq;
                }
            }
            case 49: {
                ByteArrayInputStream bIn = new ByteArrayInputStream(bytes);
                BERInputStream dIn = new BERInputStream(bIn);
                ASN1EncodableVector v = new ASN1EncodableVector();
                try {
                    do {
                        DERObject obj = dIn.readObject();
                        v.add(obj);
                    } while (true);
                }
                catch (EOFException ex) {
                    return new DERConstructedSet(v);
                }
            }
            case 1: {
                return new DERBoolean(bytes);
            }
            case 2: {
                return new DERInteger(bytes);
            }
            case 10: {
                return new DEREnumerated(bytes);
            }
            case 6: {
                return new DERObjectIdentifier(bytes);
            }
            case 3: {
                byte padBits = bytes[0];
                byte[] data = new byte[bytes.length - 1];
                System.arraycopy(bytes, 1, data, 0, bytes.length - 1);
                return new DERBitString(data, (int)padBits);
            }
            case 12: {
                return new DERUTF8String(bytes);
            }
            case 19: {
                return new DERPrintableString(bytes);
            }
            case 22: {
                return new DERIA5String(bytes);
            }
            case 20: {
                return new DERT61String(bytes);
            }
            case 26: {
                return new DERVisibleString(bytes);
            }
            case 28: {
                return new DERUniversalString(bytes);
            }
            case 27: {
                return new DERGeneralString(bytes);
            }
            case 30: {
                return new DERBMPString(bytes);
            }
            case 4: {
                return new DEROctetString(bytes);
            }
            case 23: {
                return new DERUTCTime(bytes);
            }
            case 24: {
                return new DERGeneralizedTime(bytes);
            }
        }
        if ((tag & 128) != 0) {
            if ((tag & 31) == 31) {
                throw new IOException("unsupported high tag encountered");
            }
            if (bytes.length == 0) {
                if ((tag & 32) == 0) {
                    return new DERTaggedObject(false, tag & 31, new DERNull());
                }
                return new DERTaggedObject(false, tag & 31, new DERConstructedSequence());
            }
            if ((tag & 32) == 0) {
                return new DERTaggedObject(false, tag & 31, new DEROctetString(bytes));
            }
            ByteArrayInputStream bIn = new ByteArrayInputStream(bytes);
            BERInputStream dIn = new BERInputStream(bIn);
            DERObject dObj = dIn.readObject();
            if (dIn.available() == 0) {
                return new DERTaggedObject(tag & 31, dObj);
            }
            DERConstructedSequence seq = new DERConstructedSequence();
            seq.addObject(dObj);
            try {
                do {
                    dObj = dIn.readObject();
                    seq.addObject(dObj);
                } while (true);
            }
            catch (EOFException var10_19) {
                return new DERTaggedObject(false, tag & 31, seq);
            }
        }
        return new DERUnknownTag(tag, bytes);
    }

    public DERObject readObject() throws IOException {
        int tag = this.read();
        if (tag == -1) {
            throw new EOFException();
        }
        int length = this.readLength();
        byte[] bytes = new byte[length];
        this.readFully(bytes);
        return this.buildObject(tag, bytes);
    }
}

