/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1Sequence;
import com.lowagie.bc.asn1.ASN1TaggedObject;
import com.lowagie.bc.asn1.BERConstructedOctetString;
import com.lowagie.bc.asn1.DEREncodable;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROctetString;
import com.lowagie.bc.asn1.DEROutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Vector;

public abstract class ASN1OctetString
extends DERObject {
    byte[] string;

    public static ASN1OctetString getInstance(ASN1TaggedObject obj, boolean explicit) {
        return ASN1OctetString.getInstance(obj.getObject());
    }

    public static ASN1OctetString getInstance(Object obj) {
        if (obj == null || obj instanceof ASN1OctetString) {
            return (ASN1OctetString)obj;
        }
        if (obj instanceof ASN1TaggedObject) {
            return ASN1OctetString.getInstance(((ASN1TaggedObject)obj).getObject());
        }
        if (obj instanceof ASN1Sequence) {
            Vector v = new Vector();
            Enumeration e = ((ASN1Sequence)obj).getObjects();
            while (e.hasMoreElements()) {
                v.addElement(e.nextElement());
            }
            return new BERConstructedOctetString(v);
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public ASN1OctetString(byte[] string) {
        this.string = string;
    }

    public ASN1OctetString(DEREncodable obj) {
        try {
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            DEROutputStream dOut = new DEROutputStream(bOut);
            dOut.writeObject(obj);
            dOut.close();
            this.string = bOut.toByteArray();
        }
        catch (IOException e) {
            throw new IllegalArgumentException("Error processing object : " + e.toString());
        }
    }

    public byte[] getOctets() {
        return this.string;
    }

    public int hashCode() {
        byte[] b = this.getOctets();
        int value = 0;
        for (int i = 0; i != b.length; ++i) {
            value^=(b[i] & 255) << i % 4;
        }
        return value;
    }

    public boolean equals(Object o) {
        byte[] b2;
        if (!(o != null && o instanceof DEROctetString)) {
            return false;
        }
        DEROctetString other = (DEROctetString)o;
        byte[] b1 = other.getOctets();
        if (b1.length != (b2 = this.getOctets()).length) {
            return false;
        }
        for (int i = 0; i != b1.length; ++i) {
            if (b1[i] == b2[i]) continue;
            return false;
        }
        return true;
    }

    abstract void encode(DEROutputStream var1) throws IOException;
}

