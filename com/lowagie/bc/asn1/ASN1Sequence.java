/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1TaggedObject;
import com.lowagie.bc.asn1.BERSequence;
import com.lowagie.bc.asn1.BERTaggedObject;
import com.lowagie.bc.asn1.DEREncodable;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROutputStream;
import com.lowagie.bc.asn1.DERSequence;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

public abstract class ASN1Sequence
extends DERObject {
    private Vector seq = new Vector();

    public static ASN1Sequence getInstance(Object obj) {
        if (obj == null || obj instanceof ASN1Sequence) {
            return (ASN1Sequence)obj;
        }
        throw new IllegalArgumentException("unknown object in getInstance");
    }

    public static ASN1Sequence getInstance(ASN1TaggedObject obj, boolean explicit) {
        if (explicit) {
            if (!obj.isExplicit()) {
                throw new IllegalArgumentException("object implicit - explicit expected.");
            }
            return (ASN1Sequence)obj.getObject();
        }
        if (obj.isExplicit()) {
            if (obj instanceof BERTaggedObject) {
                return new BERSequence(obj.getObject());
            }
            return new DERSequence(obj.getObject());
        }
        if (obj.getObject() instanceof ASN1Sequence) {
            return (ASN1Sequence)obj.getObject();
        }
        throw new IllegalArgumentException("unknown object in getInstanceFromTagged");
    }

    public Enumeration getObjects() {
        return this.seq.elements();
    }

    public DEREncodable getObjectAt(int index) {
        return (DEREncodable)this.seq.elementAt(index);
    }

    public int size() {
        return this.seq.size();
    }

    public int hashCode() {
        Enumeration e = this.getObjects();
        int hashCode = 0;
        while (e.hasMoreElements()) {
            Object o = e.nextElement();
            if (o == null) continue;
            hashCode^=o.hashCode();
        }
        return hashCode;
    }

    public boolean equals(Object o) {
        if (!(o != null && o instanceof ASN1Sequence)) {
            return false;
        }
        ASN1Sequence other = (ASN1Sequence)o;
        if (this.size() != other.size()) {
            return false;
        }
        Enumeration s1 = this.getObjects();
        Enumeration s2 = other.getObjects();
        while (s1.hasMoreElements()) {
            Object o1 = s1.nextElement();
            Object o2 = s2.nextElement();
            if (o1 != null && o2 != null) {
                if (o1.equals(o2)) continue;
                return false;
            }
            if (o1 == null && o2 == null) continue;
            return false;
        }
        return true;
    }

    protected void addObject(DEREncodable obj) {
        this.seq.addElement(obj);
    }

    abstract void encode(DEROutputStream var1) throws IOException;
}

