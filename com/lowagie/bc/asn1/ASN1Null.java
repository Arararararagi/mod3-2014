/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROutputStream;
import java.io.IOException;

public abstract class ASN1Null
extends DERObject {
    public int hashCode() {
        return 0;
    }

    public boolean equals(Object o) {
        if (!(o != null && o instanceof ASN1Null)) {
            return false;
        }
        return true;
    }

    abstract void encode(DEROutputStream var1) throws IOException;
}

