/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1OctetString;
import com.lowagie.bc.asn1.ASN1OutputStream;
import com.lowagie.bc.asn1.ASN1Sequence;
import com.lowagie.bc.asn1.ASN1Set;
import com.lowagie.bc.asn1.BERConstructedOctetString;
import com.lowagie.bc.asn1.BERConstructedSequence;
import com.lowagie.bc.asn1.BEROutputStream;
import com.lowagie.bc.asn1.DEREncodable;
import com.lowagie.bc.asn1.DEROutputStream;
import com.lowagie.bc.asn1.DERTaggedObject;
import java.io.IOException;
import java.util.Enumeration;

public class BERTaggedObject
extends DERTaggedObject {
    public BERTaggedObject(int tagNo, DEREncodable obj) {
        super(tagNo, obj);
    }

    public BERTaggedObject(boolean explicit, int tagNo, DEREncodable obj) {
        super(explicit, tagNo, obj);
    }

    public BERTaggedObject(int tagNo) {
        super(false, tagNo, new BERConstructedSequence());
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    void encode(DEROutputStream out) throws IOException {
        if (out instanceof ASN1OutputStream || out instanceof BEROutputStream) {
            out.write(160 | this.tagNo);
            out.write(128);
            if (!this.empty) {
                if (!this.explicit) {
                    if (this.obj instanceof ASN1OctetString) {
                        Enumeration e;
                        if (this.obj instanceof BERConstructedOctetString) {
                            e = ((BERConstructedOctetString)this.obj).getObjects();
                        } else {
                            ASN1OctetString octs = (ASN1OctetString)this.obj;
                            BERConstructedOctetString berO = new BERConstructedOctetString(octs.getOctets());
                            e = berO.getObjects();
                        }
                        while (e.hasMoreElements()) {
                            out.writeObject(e.nextElement());
                        }
                    } else if (this.obj instanceof ASN1Sequence) {
                        Enumeration e = ((ASN1Sequence)this.obj).getObjects();
                        while (e.hasMoreElements()) {
                            out.writeObject(e.nextElement());
                        }
                    } else {
                        if (!(this.obj instanceof ASN1Set)) throw new RuntimeException("not implemented: " + this.obj.getClass().getName());
                        Enumeration e = ((ASN1Set)this.obj).getObjects();
                        while (e.hasMoreElements()) {
                            out.writeObject(e.nextElement());
                        }
                    }
                } else {
                    out.writeObject(this.obj);
                }
            }
            out.write(0);
            out.write(0);
            return;
        } else {
            super.encode(out);
        }
    }
}

