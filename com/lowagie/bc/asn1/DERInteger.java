/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1OctetString;
import com.lowagie.bc.asn1.ASN1TaggedObject;
import com.lowagie.bc.asn1.DERObject;
import com.lowagie.bc.asn1.DEROutputStream;
import java.io.IOException;
import java.math.BigInteger;

public class DERInteger
extends DERObject {
    byte[] bytes;

    public static DERInteger getInstance(Object obj) {
        if (obj == null || obj instanceof DERInteger) {
            return (DERInteger)obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERInteger(((ASN1OctetString)obj).getOctets());
        }
        if (obj instanceof ASN1TaggedObject) {
            return DERInteger.getInstance(((ASN1TaggedObject)obj).getObject());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERInteger getInstance(ASN1TaggedObject obj, boolean explicit) {
        return DERInteger.getInstance(obj.getObject());
    }

    public DERInteger(int value) {
        this.bytes = BigInteger.valueOf(value).toByteArray();
    }

    public DERInteger(BigInteger value) {
        this.bytes = value.toByteArray();
    }

    public DERInteger(byte[] bytes) {
        this.bytes = bytes;
    }

    public BigInteger getValue() {
        return new BigInteger(this.bytes);
    }

    public BigInteger getPositiveValue() {
        return new BigInteger(1, this.bytes);
    }

    void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(2, this.bytes);
    }

    public int hashCode() {
        int value = 0;
        for (int i = 0; i != this.bytes.length; ++i) {
            value^=(this.bytes[i] & 255) << i % 4;
        }
        return value;
    }

    public boolean equals(Object o) {
        if (!(o != null && o instanceof DERInteger)) {
            return false;
        }
        DERInteger other = (DERInteger)o;
        if (this.bytes.length != other.bytes.length) {
            return false;
        }
        for (int i = 0; i != this.bytes.length; ++i) {
            if (this.bytes[i] == other.bytes[i]) continue;
            return false;
        }
        return true;
    }
}

