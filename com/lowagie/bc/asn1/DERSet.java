/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.ASN1Set;
import com.lowagie.bc.asn1.DEREncodable;
import com.lowagie.bc.asn1.DEREncodableVector;
import com.lowagie.bc.asn1.DEROutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;

public class DERSet
extends ASN1Set {
    public DERSet() {
    }

    public DERSet(DEREncodable obj) {
        this.addObject(obj);
    }

    public DERSet(DEREncodableVector v) {
        for (int i = 0; i != v.size(); ++i) {
            this.addObject(v.get(i));
        }
    }

    void encode(DEROutputStream out) throws IOException {
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        DEROutputStream dOut = new DEROutputStream(bOut);
        Enumeration e = this.getObjects();
        while (e.hasMoreElements()) {
            Object obj = e.nextElement();
            dOut.writeObject(obj);
        }
        dOut.close();
        byte[] bytes = bOut.toByteArray();
        out.writeEncoded(49, bytes);
    }
}

