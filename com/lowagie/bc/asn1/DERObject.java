/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.bc.asn1;

import com.lowagie.bc.asn1.DEREncodable;
import com.lowagie.bc.asn1.DEROutputStream;
import com.lowagie.bc.asn1.DERTags;
import java.io.IOException;

public abstract class DERObject
implements DERTags,
DEREncodable {
    public DERObject getDERObject() {
        return this;
    }

    abstract void encode(DEROutputStream var1) throws IOException;
}

