/*
 * Decompiled with CFR 0_102.
 */
package calculo.common.util;

import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {
    public static String mascaraValor(double val) {
        NumberFormat nf = NumberFormat.getInstance(Locale.FRENCH);
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);
        return String.valueOf(nf.format(val));
    }

    public static String removeEsp(String str) {
        String aux = Utils.mascaraValor(1000.0);
        char[] newStr = new char[str.length()];
        int j = 0;
        for (int i = 0; i < str.length(); ++i) {
            if (str.charAt(i) == aux.charAt(1)) continue;
            newStr[j] = str.charAt(i);
            ++j;
        }
        return new String(newStr);
    }

    public static String mascVal(double val) {
        return Utils.removeEsp(Utils.mascaraValor(val));
    }

    public static String mascValImp(double val) {
        String valor = Utils.mascaraValor(val);
        if (valor.equals("0,00")) {
            valor = "&nbsp;";
        }
        return valor;
    }

    public static String requestValue(String rval) {
        if (rval == null) {
            return "null";
        }
        int ind1 = rval.lastIndexOf(44);
        if (ind1 == -1) {
            return rval;
        }
        return rval.substring(ind1 + 1, rval.length());
    }

    public static Date getDate(String Date) {
        String datePattern = "yyyy-MM";
        SimpleDateFormat formatter = new SimpleDateFormat(datePattern, new Locale("en", "US"));
        ParsePosition pos = new ParsePosition(0);
        Date date = formatter.parse(Date, pos);
        return date;
    }

    public static double DifMesDatas(String sData1, String sData2) {
        Date date1 = Utils.getDate(sData1);
        Date date2 = Utils.getDate(sData2);
        Date dif = new Date();
        dif.setTime(Math.abs(date1.getTime() - date2.getTime()));
        return dif.getTime() / 86400000;
    }

    public static double somaArrayInt(double[] arr, int ind) {
        double res = 0.0;
        for (int i = 0; i < ind; ++i) {
            res+=arr[i];
        }
        return res;
    }
}

