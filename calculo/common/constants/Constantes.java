/*
 * Decompiled with CFR 0_102.
 */
package calculo.common.constants;

import calculo.common.constants.Constante;
import calculo.common.constants.tables.desvalorizacao.Moeda2001;
import calculo.common.constants.tables.desvalorizacao.Moeda2002;
import calculo.common.constants.tables.desvalorizacao.Moeda2003;
import calculo.common.constants.tables.desvalorizacao.Moeda2004;
import calculo.common.constants.tables.desvalorizacao.Moeda2005;
import calculo.common.constants.tables.desvalorizacao.Moeda2006;
import calculo.common.constants.tables.desvalorizacao.Moeda2007;
import calculo.common.constants.tables.desvalorizacao.Moeda2008;
import calculo.common.constants.tables.desvalorizacao.Moeda2009;
import calculo.common.constants.tables.desvalorizacao.Moeda2010;
import calculo.common.constants.tables.desvalorizacao.Moeda2011;
import calculo.common.constants.tables.desvalorizacao.Moeda2012;
import calculo.common.constants.tables.desvalorizacao.Moeda2013;
import calculo.common.constants.tables.desvalorizacao.Moeda2014;

public class Constantes {
    private Constante[] Coef_Desv;
    private int max_Coef_Desv = 0;

    private void initCoef_Desv(char ano) {
        switch (ano) {
            case '1': {
                this.Coef_Desv = Moeda2007.getTable();
                break;
            }
            case '2': {
                this.Coef_Desv = Moeda2006.getTable();
                break;
            }
            case '3': {
                this.Coef_Desv = Moeda2005.getTable();
                break;
            }
            case '4': {
                this.Coef_Desv = Moeda2004.getTable();
                break;
            }
            case '5': {
                this.Coef_Desv = Moeda2003.getTable();
                break;
            }
            case '6': {
                this.Coef_Desv = Moeda2002.getTable();
                break;
            }
            case '7': {
                this.Coef_Desv = Moeda2001.getTable();
                break;
            }
            case '8': {
                this.Coef_Desv = Moeda2008.getTable();
                break;
            }
            case '9': {
                this.Coef_Desv = Moeda2009.getTable();
                break;
            }
            default: {
                this.Coef_Desv = Moeda2003.getTable();
            }
        }
        this.max_Coef_Desv = this.Coef_Desv.length;
    }

    public double getCoefDesv(int ano, int ano2) {
        int i2;
        switch (ano2) {
            case 1: {
                this.Coef_Desv = Moeda2007.getTable();
                break;
            }
            case 2: {
                this.Coef_Desv = Moeda2006.getTable();
                break;
            }
            case 3: {
                this.Coef_Desv = Moeda2005.getTable();
                break;
            }
            case 4: {
                this.Coef_Desv = Moeda2004.getTable();
                break;
            }
            case 8: {
                this.Coef_Desv = Moeda2008.getTable();
                break;
            }
            case 9: {
                this.Coef_Desv = Moeda2009.getTable();
                break;
            }
            case 10: {
                this.Coef_Desv = Moeda2010.getTable();
                break;
            }
            case 11: {
                this.Coef_Desv = Moeda2011.getTable();
                break;
            }
            case 12: {
                this.Coef_Desv = Moeda2012.getTable();
                break;
            }
            case 13: {
                this.Coef_Desv = Moeda2013.getTable();
                break;
            }
            case 14: {
                this.Coef_Desv = Moeda2014.getTable();
                break;
            }
            default: {
                this.Coef_Desv = Moeda2003.getTable();
            }
        }
        this.max_Coef_Desv = this.Coef_Desv.length;
        double res = this.Coef_Desv[i2].valor;
        for (i2 = 0; i2 < this.max_Coef_Desv && this.Coef_Desv[i2].id < ano; ++i2) {
        }
        if (i2 == this.max_Coef_Desv) {
            res = this.Coef_Desv[i2 - 1].valor;
        } else if (this.Coef_Desv[i2].id == ano) {
            res = this.Coef_Desv[i2].valor;
        } else if (i2 != 0) {
            res = this.Coef_Desv[i2 - 1].valor;
        }
        return res;
    }
}

