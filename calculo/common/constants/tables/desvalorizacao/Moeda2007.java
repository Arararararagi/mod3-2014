/*
 * Decompiled with CFR 0_102.
 */
package calculo.common.constants.tables.desvalorizacao;

import calculo.common.constants.Constante;

public class Moeda2007 {
    private static int max_Coef_Desv = 63;

    public static Constante[] getTable() {
        int i = 0;
        Constante[] Coef_Desv = new Constante[max_Coef_Desv];
        Coef_Desv[i++] = new Constante(1903, 4110.82);
        Coef_Desv[i++] = new Constante(1904, 3826.68);
        Coef_Desv[i++] = new Constante(1911, 3670.22);
        Coef_Desv[i++] = new Constante(1915, 3265.37);
        Coef_Desv[i++] = new Constante(1916, 2672.73);
        Coef_Desv[i++] = new Constante(1917, 2133.64);
        Coef_Desv[i++] = new Constante(1918, 1522.29);
        Coef_Desv[i++] = new Constante(1919, 1166.67);
        Coef_Desv[i++] = new Constante(1920, 770.88);
        Coef_Desv[i++] = new Constante(1921, 502.96);
        Coef_Desv[i++] = new Constante(1922, 372.49);
        Coef_Desv[i++] = new Constante(1923, 227.97);
        Coef_Desv[i++] = new Constante(1924, 191.9);
        Coef_Desv[i++] = new Constante(1925, 165.4);
        Coef_Desv[i++] = new Constante(1937, 160.62);
        Coef_Desv[i++] = new Constante(1940, 135.16);
        Coef_Desv[i++] = new Constante(1941, 120.04);
        Coef_Desv[i++] = new Constante(1942, 103.64);
        Coef_Desv[i++] = new Constante(1943, 88.25);
        Coef_Desv[i++] = new Constante(1944, 74.93);
        Coef_Desv[i++] = new Constante(1951, 68.72);
        Coef_Desv[i++] = new Constante(1958, 64.62);
        Coef_Desv[i++] = new Constante(1964, 61.76);
        Coef_Desv[i++] = new Constante(1965, 59.5);
        Coef_Desv[i++] = new Constante(1966, 56.84);
        Coef_Desv[i++] = new Constante(1967, 53.16);
        Coef_Desv[i++] = new Constante(1970, 49.23);
        Coef_Desv[i++] = new Constante(1971, 46.86);
        Coef_Desv[i++] = new Constante(1972, 43.8);
        Coef_Desv[i++] = new Constante(1973, 39.82);
        Coef_Desv[i++] = new Constante(1974, 30.54);
        Coef_Desv[i++] = new Constante(1975, 26.09);
        Coef_Desv[i++] = new Constante(1976, 21.85);
        Coef_Desv[i++] = new Constante(1977, 16.77);
        Coef_Desv[i++] = new Constante(1978, 13.12);
        Coef_Desv[i++] = new Constante(1979, 10.5);
        Coef_Desv[i++] = new Constante(1980, 9.33);
        Coef_Desv[i++] = new Constante(1981, 7.63);
        Coef_Desv[i++] = new Constante(1982, 6.34);
        Coef_Desv[i++] = new Constante(1983, 5.06);
        Coef_Desv[i++] = new Constante(1984, 3.93);
        Coef_Desv[i++] = new Constante(1985, 3.28);
        Coef_Desv[i++] = new Constante(1986, 2.97);
        Coef_Desv[i++] = new Constante(1987, 2.72);
        Coef_Desv[i++] = new Constante(1988, 2.46);
        Coef_Desv[i++] = new Constante(1989, 2.21);
        Coef_Desv[i++] = new Constante(1990, 1.97);
        Coef_Desv[i++] = new Constante(1991, 1.75);
        Coef_Desv[i++] = new Constante(1992, 1.61);
        Coef_Desv[i++] = new Constante(1993, 1.49);
        Coef_Desv[i++] = new Constante(1994, 1.42);
        Coef_Desv[i++] = new Constante(1995, 1.37);
        Coef_Desv[i++] = new Constante(1996, 1.33);
        Coef_Desv[i++] = new Constante(1997, 1.31);
        Coef_Desv[i++] = new Constante(1998, 1.27);
        Coef_Desv[i++] = new Constante(1999, 1.25);
        Coef_Desv[i++] = new Constante(2000, 1.22);
        Coef_Desv[i++] = new Constante(2001, 1.14);
        Coef_Desv[i++] = new Constante(2002, 1.1);
        Coef_Desv[i++] = new Constante(2003, 1.07);
        Coef_Desv[i++] = new Constante(2004, 1.05);
        Coef_Desv[i++] = new Constante(2005, 1.03);
        Coef_Desv[i++] = new Constante(2006, 1.0);
        return Coef_Desv;
    }
}

