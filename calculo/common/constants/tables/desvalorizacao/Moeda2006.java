/*
 * Decompiled with CFR 0_102.
 */
package calculo.common.constants.tables.desvalorizacao;

import calculo.common.constants.Constante;

public class Moeda2006 {
    private static int max_Coef_Desv = 62;

    public static Constante[] getTable() {
        int i = 0;
        Constante[] Coef_Desv = new Constante[max_Coef_Desv];
        Coef_Desv[i] = new Constante(1903, 3987.22);
        Coef_Desv[++i] = new Constante(1904, 3711.62);
        Coef_Desv[++i] = new Constante(1911, 3559.86);
        Coef_Desv[++i] = new Constante(1915, 3167.19);
        Coef_Desv[++i] = new Constante(1916, 2592.37);
        Coef_Desv[++i] = new Constante(1917, 2069.49);
        Coef_Desv[++i] = new Constante(1918, 1476.52);
        Coef_Desv[++i] = new Constante(1919, 1131.59);
        Coef_Desv[++i] = new Constante(1920, 747.7);
        Coef_Desv[++i] = new Constante(1921, 487.84);
        Coef_Desv[++i] = new Constante(1922, 361.29);
        Coef_Desv[++i] = new Constante(1923, 221.12);
        Coef_Desv[++i] = new Constante(1924, 186.13);
        Coef_Desv[++i] = new Constante(1925, 160.43);
        Coef_Desv[++i] = new Constante(1937, 155.79);
        Coef_Desv[++i] = new Constante(1940, 131.1);
        Coef_Desv[++i] = new Constante(1941, 116.43);
        Coef_Desv[++i] = new Constante(1942, 100.52);
        Coef_Desv[++i] = new Constante(1943, 85.6);
        Coef_Desv[++i] = new Constante(1944, 72.68);
        Coef_Desv[++i] = new Constante(1951, 66.65);
        Coef_Desv[++i] = new Constante(1958, 62.68);
        Coef_Desv[++i] = new Constante(1964, 59.9);
        Coef_Desv[++i] = new Constante(1965, 57.71);
        Coef_Desv[++i] = new Constante(1966, 55.13);
        Coef_Desv[++i] = new Constante(1967, 51.56);
        Coef_Desv[++i] = new Constante(1970, 47.75);
        Coef_Desv[++i] = new Constante(1971, 45.45);
        Coef_Desv[++i] = new Constante(1972, 42.48);
        Coef_Desv[++i] = new Constante(1973, 38.62);
        Coef_Desv[++i] = new Constante(1974, 29.62);
        Coef_Desv[++i] = new Constante(1975, 25.31);
        Coef_Desv[++i] = new Constante(1976, 21.19);
        Coef_Desv[++i] = new Constante(1977, 16.27);
        Coef_Desv[++i] = new Constante(1978, 12.73);
        Coef_Desv[++i] = new Constante(1979, 10.04);
        Coef_Desv[++i] = new Constante(1980, 9.05);
        Coef_Desv[++i] = new Constante(1981, 7.4);
        Coef_Desv[++i] = new Constante(1982, 6.15);
        Coef_Desv[++i] = new Constante(1983, 4.91);
        Coef_Desv[++i] = new Constante(1984, 3.81);
        Coef_Desv[++i] = new Constante(1985, 3.18);
        Coef_Desv[++i] = new Constante(1986, 2.88);
        Coef_Desv[++i] = new Constante(1987, 2.64);
        Coef_Desv[++i] = new Constante(1988, 2.39);
        Coef_Desv[++i] = new Constante(1989, 2.14);
        Coef_Desv[++i] = new Constante(1990, 1.91);
        Coef_Desv[++i] = new Constante(1991, 1.7);
        Coef_Desv[++i] = new Constante(1992, 1.56);
        Coef_Desv[++i] = new Constante(1993, 1.45);
        Coef_Desv[++i] = new Constante(1994, 1.38);
        Coef_Desv[++i] = new Constante(1995, 1.33);
        Coef_Desv[++i] = new Constante(1996, 1.29);
        Coef_Desv[++i] = new Constante(1997, 1.27);
        Coef_Desv[++i] = new Constante(1998, 1.23);
        Coef_Desv[++i] = new Constante(1999, 1.21);
        Coef_Desv[++i] = new Constante(2000, 1.18);
        Coef_Desv[++i] = new Constante(2001, 1.11);
        Coef_Desv[++i] = new Constante(2002, 1.07);
        Coef_Desv[++i] = new Constante(2003, 1.04);
        Coef_Desv[++i] = new Constante(2004, 1.02);
        Coef_Desv[++i] = new Constante(2005, 1.0);
        ++i;
        return Coef_Desv;
    }
}

