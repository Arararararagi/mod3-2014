/*
 * Decompiled with CFR 0_102.
 */
package calculo.common.constants.tables.desvalorizacao;

import calculo.common.constants.Constante;

public class Moeda2002 {
    private static int max_Coef_Desv = 58;

    public static Constante[] getTable() {
        int i = 0;
        Constante[] Coef_Desv = new Constante[max_Coef_Desv];
        Coef_Desv[i] = new Constante(1903, 3565.39);
        Coef_Desv[++i] = new Constante(1904, 3318.94);
        Coef_Desv[++i] = new Constante(1911, 3183.24);
        Coef_Desv[++i] = new Constante(1915, 2832.11);
        Coef_Desv[++i] = new Constante(1916, 2318.11);
        Coef_Desv[++i] = new Constante(1917, 1850.54);
        Coef_Desv[++i] = new Constante(1918, 1320.31);
        Coef_Desv[++i] = new Constante(1919, 1011.88);
        Coef_Desv[++i] = new Constante(1920, 668.59);
        Coef_Desv[++i] = new Constante(1921, 436.23);
        Coef_Desv[++i] = new Constante(1922, 323.06);
        Coef_Desv[++i] = new Constante(1923, 197.73);
        Coef_Desv[++i] = new Constante(1924, 166.44);
        Coef_Desv[++i] = new Constante(1925, 143.46);
        Coef_Desv[++i] = new Constante(1937, 139.31);
        Coef_Desv[++i] = new Constante(1940, 117.23);
        Coef_Desv[++i] = new Constante(1941, 104.11);
        Coef_Desv[++i] = new Constante(1942, 89.89);
        Coef_Desv[++i] = new Constante(1943, 76.55);
        Coef_Desv[++i] = new Constante(1944, 64.99);
        Coef_Desv[++i] = new Constante(1951, 59.6);
        Coef_Desv[++i] = new Constante(1958, 56.05);
        Coef_Desv[++i] = new Constante(1964, 53.56);
        Coef_Desv[++i] = new Constante(1965, 51.61);
        Coef_Desv[++i] = new Constante(1966, 49.3);
        Coef_Desv[++i] = new Constante(1967, 46.11);
        Coef_Desv[++i] = new Constante(1970, 42.69);
        Coef_Desv[++i] = new Constante(1971, 40.64);
        Coef_Desv[++i] = new Constante(1972, 38.0);
        Coef_Desv[++i] = new Constante(1973, 34.54);
        Coef_Desv[++i] = new Constante(1974, 26.48);
        Coef_Desv[++i] = new Constante(1975, 22.63);
        Coef_Desv[++i] = new Constante(1976, 18.94);
        Coef_Desv[++i] = new Constante(1977, 14.55);
        Coef_Desv[++i] = new Constante(1978, 11.39);
        Coef_Desv[++i] = new Constante(1979, 8.97);
        Coef_Desv[++i] = new Constante(1980, 8.1);
        Coef_Desv[++i] = new Constante(1981, 6.62);
        Coef_Desv[++i] = new Constante(1982, 5.5);
        Coef_Desv[++i] = new Constante(1983, 4.38);
        Coef_Desv[++i] = new Constante(1984, 3.41);
        Coef_Desv[++i] = new Constante(1985, 2.84);
        Coef_Desv[++i] = new Constante(1986, 2.58);
        Coef_Desv[++i] = new Constante(1987, 2.36);
        Coef_Desv[++i] = new Constante(1988, 2.15);
        Coef_Desv[++i] = new Constante(1989, 1.9);
        Coef_Desv[++i] = new Constante(1990, 1.71);
        Coef_Desv[++i] = new Constante(1991, 1.51);
        Coef_Desv[++i] = new Constante(1992, 1.41);
        Coef_Desv[++i] = new Constante(1993, 1.3);
        Coef_Desv[++i] = new Constante(1994, 1.24);
        Coef_Desv[++i] = new Constante(1995, 1.19);
        Coef_Desv[++i] = new Constante(1996, 1.15);
        Coef_Desv[++i] = new Constante(1997, 1.13);
        Coef_Desv[++i] = new Constante(1998, 1.1);
        Coef_Desv[++i] = new Constante(1999, 1.07);
        Coef_Desv[++i] = new Constante(2000, 1.04);
        Coef_Desv[++i] = new Constante(2001, 1.0);
        ++i;
        return Coef_Desv;
    }
}

