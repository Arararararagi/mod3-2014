/*
 * Decompiled with CFR 0_102.
 */
package calculo.common.constants.tables.desvalorizacao;

import calculo.common.constants.Constante;

public class Moeda2003 {
    private static int max_Coef_Desv = 59;

    public static Constante[] getTable() {
        int i = 0;
        Constante[] Coef_Desv = new Constante[max_Coef_Desv];
        Coef_Desv[i] = new Constante(1903, 3691.85);
        Coef_Desv[++i] = new Constante(1904, 3436.66);
        Coef_Desv[++i] = new Constante(1911, 3396.15);
        Coef_Desv[++i] = new Constante(1915, 2932.57);
        Coef_Desv[++i] = new Constante(1916, 2400.33);
        Coef_Desv[++i] = new Constante(1917, 1916.18);
        Coef_Desv[++i] = new Constante(1918, 1367.18);
        Coef_Desv[++i] = new Constante(1919, 1047.77);
        Coef_Desv[++i] = new Constante(1920, 692.3);
        Coef_Desv[++i] = new Constante(1921, 451.7);
        Coef_Desv[++i] = new Constante(1922, 334.52);
        Coef_Desv[++i] = new Constante(1923, 204.74);
        Coef_Desv[++i] = new Constante(1924, 172.34);
        Coef_Desv[++i] = new Constante(1925, 148.55);
        Coef_Desv[++i] = new Constante(1937, 144.25);
        Coef_Desv[++i] = new Constante(1940, 121.39);
        Coef_Desv[++i] = new Constante(1941, 107.8);
        Coef_Desv[++i] = new Constante(1942, 93.08);
        Coef_Desv[++i] = new Constante(1943, 79.27);
        Coef_Desv[++i] = new Constante(1944, 67.3);
        Coef_Desv[++i] = new Constante(1951, 61.71);
        Coef_Desv[++i] = new Constante(1958, 58.04);
        Coef_Desv[++i] = new Constante(1964, 55.46);
        Coef_Desv[++i] = new Constante(1965, 53.44);
        Coef_Desv[++i] = new Constante(1966, 51.04);
        Coef_Desv[++i] = new Constante(1967, 47.74);
        Coef_Desv[++i] = new Constante(1970, 44.21);
        Coef_Desv[++i] = new Constante(1971, 42.08);
        Coef_Desv[++i] = new Constante(1972, 39.34);
        Coef_Desv[++i] = new Constante(1973, 35.76);
        Coef_Desv[++i] = new Constante(1974, 27.42);
        Coef_Desv[++i] = new Constante(1975, 23.43);
        Coef_Desv[++i] = new Constante(1976, 19.62);
        Coef_Desv[++i] = new Constante(1977, 15.06);
        Coef_Desv[++i] = new Constante(1978, 11.8);
        Coef_Desv[++i] = new Constante(1979, 9.29);
        Coef_Desv[++i] = new Constante(1980, 8.38);
        Coef_Desv[++i] = new Constante(1981, 6.85);
        Coef_Desv[++i] = new Constante(1982, 5.69);
        Coef_Desv[++i] = new Constante(1983, 4.54);
        Coef_Desv[++i] = new Constante(1984, 3.54);
        Coef_Desv[++i] = new Constante(1985, 2.94);
        Coef_Desv[++i] = new Constante(1986, 2.68);
        Coef_Desv[++i] = new Constante(1987, 2.44);
        Coef_Desv[++i] = new Constante(1988, 2.22);
        Coef_Desv[++i] = new Constante(1989, 1.97);
        Coef_Desv[++i] = new Constante(1990, 1.77);
        Coef_Desv[++i] = new Constante(1991, 1.56);
        Coef_Desv[++i] = new Constante(1992, 1.46);
        Coef_Desv[++i] = new Constante(1993, 1.35);
        Coef_Desv[++i] = new Constante(1994, 1.28);
        Coef_Desv[++i] = new Constante(1995, 1.23);
        Coef_Desv[++i] = new Constante(1996, 1.19);
        Coef_Desv[++i] = new Constante(1997, 1.17);
        Coef_Desv[++i] = new Constante(1998, 1.14);
        Coef_Desv[++i] = new Constante(1999, 1.11);
        Coef_Desv[++i] = new Constante(2000, 1.08);
        Coef_Desv[++i] = new Constante(2001, 1.04);
        Coef_Desv[++i] = new Constante(2002, 1.0);
        ++i;
        return Coef_Desv;
    }
}

