/*
 * Decompiled with CFR 0_102.
 */
package calculo.common.constants.tables.desvalorizacao;

import calculo.common.constants.Constante;

public class Moeda2005 {
    private static int max_Coef_Desv = 61;

    public static Constante[] getTable() {
        int i = 0;
        Constante[] Coef_Desv = new Constante[max_Coef_Desv];
        Coef_Desv[i] = new Constante(1903, 3901.39);
        Coef_Desv[++i] = new Constante(1904, 3631.72);
        Coef_Desv[++i] = new Constante(1911, 3483.23);
        Coef_Desv[++i] = new Constante(1915, 3099.01);
        Coef_Desv[++i] = new Constante(1916, 2536.57);
        Coef_Desv[++i] = new Constante(1917, 2024.94);
        Coef_Desv[++i] = new Constante(1918, 1444.74);
        Coef_Desv[++i] = new Constante(1919, 1107.23);
        Coef_Desv[++i] = new Constante(1920, 731.6);
        Coef_Desv[++i] = new Constante(1921, 477.34);
        Coef_Desv[++i] = new Constante(1922, 353.51);
        Coef_Desv[++i] = new Constante(1923, 216.36);
        Coef_Desv[++i] = new Constante(1924, 182.12);
        Coef_Desv[++i] = new Constante(1925, 156.98);
        Coef_Desv[++i] = new Constante(1937, 152.44);
        Coef_Desv[++i] = new Constante(1940, 128.28);
        Coef_Desv[++i] = new Constante(1941, 113.92);
        Coef_Desv[++i] = new Constante(1942, 98.36);
        Coef_Desv[++i] = new Constante(1943, 83.76);
        Coef_Desv[++i] = new Constante(1944, 71.12);
        Coef_Desv[++i] = new Constante(1951, 65.22);
        Coef_Desv[++i] = new Constante(1958, 61.33);
        Coef_Desv[++i] = new Constante(1964, 58.61);
        Coef_Desv[++i] = new Constante(1965, 56.47);
        Coef_Desv[++i] = new Constante(1966, 53.94);
        Coef_Desv[++i] = new Constante(1967, 50.45);
        Coef_Desv[++i] = new Constante(1970, 46.72);
        Coef_Desv[++i] = new Constante(1971, 44.47);
        Coef_Desv[++i] = new Constante(1972, 41.57);
        Coef_Desv[++i] = new Constante(1973, 37.79);
        Coef_Desv[++i] = new Constante(1974, 28.98);
        Coef_Desv[++i] = new Constante(1975, 24.77);
        Coef_Desv[++i] = new Constante(1976, 20.73);
        Coef_Desv[++i] = new Constante(1977, 15.92);
        Coef_Desv[++i] = new Constante(1978, 12.46);
        Coef_Desv[++i] = new Constante(1979, 9.82);
        Coef_Desv[++i] = new Constante(1980, 8.86);
        Coef_Desv[++i] = new Constante(1981, 7.24);
        Coef_Desv[++i] = new Constante(1982, 6.02);
        Coef_Desv[++i] = new Constante(1983, 4.8);
        Coef_Desv[++i] = new Constante(1984, 3.73);
        Coef_Desv[++i] = new Constante(1985, 3.11);
        Coef_Desv[++i] = new Constante(1986, 2.82);
        Coef_Desv[++i] = new Constante(1987, 2.58);
        Coef_Desv[++i] = new Constante(1988, 2.34);
        Coef_Desv[++i] = new Constante(1989, 2.09);
        Coef_Desv[++i] = new Constante(1990, 1.87);
        Coef_Desv[++i] = new Constante(1991, 1.66);
        Coef_Desv[++i] = new Constante(1992, 1.53);
        Coef_Desv[++i] = new Constante(1993, 1.42);
        Coef_Desv[++i] = new Constante(1994, 1.35);
        Coef_Desv[++i] = new Constante(1995, 1.3);
        Coef_Desv[++i] = new Constante(1996, 1.26);
        Coef_Desv[++i] = new Constante(1997, 1.24);
        Coef_Desv[++i] = new Constante(1998, 1.2);
        Coef_Desv[++i] = new Constante(1999, 1.18);
        Coef_Desv[++i] = new Constante(2000, 1.15);
        Coef_Desv[++i] = new Constante(2001, 1.09);
        Coef_Desv[++i] = new Constante(2002, 1.05);
        Coef_Desv[++i] = new Constante(2003, 1.02);
        Coef_Desv[++i] = new Constante(2004, 1.0);
        ++i;
        return Coef_Desv;
    }
}

