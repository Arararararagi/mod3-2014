/*
 * Decompiled with CFR 0_102.
 */
package calculo.common.constants.tables.desvalorizacao;

import calculo.common.constants.Constante;

public class Moeda2008 {
    private static int max_Coef_Desv = 64;

    public static Constante[] getTable() {
        int i = 0;
        Constante[] Coef_Desv = new Constante[max_Coef_Desv];
        Coef_Desv[i++] = new Constante(1903, 4209.48);
        Coef_Desv[i++] = new Constante(1904, 3918.52);
        Coef_Desv[i++] = new Constante(1911, 3758.31);
        Coef_Desv[i++] = new Constante(1915, 3343.74);
        Coef_Desv[i++] = new Constante(1916, 2736.88);
        Coef_Desv[i++] = new Constante(1917, 2184.85);
        Coef_Desv[i++] = new Constante(1918, 1558.82);
        Coef_Desv[i++] = new Constante(1919, 1194.67);
        Coef_Desv[i++] = new Constante(1920, 789.38);
        Coef_Desv[i++] = new Constante(1921, 515.03);
        Coef_Desv[i++] = new Constante(1922, 381.43);
        Coef_Desv[i++] = new Constante(1923, 233.44);
        Coef_Desv[i++] = new Constante(1924, 196.51);
        Coef_Desv[i++] = new Constante(1925, 169.37);
        Coef_Desv[i++] = new Constante(1937, 164.47);
        Coef_Desv[i++] = new Constante(1940, 138.4);
        Coef_Desv[i++] = new Constante(1941, 122.92);
        Coef_Desv[i++] = new Constante(1942, 106.13);
        Coef_Desv[i++] = new Constante(1943, 90.37);
        Coef_Desv[i++] = new Constante(1944, 76.73);
        Coef_Desv[i++] = new Constante(1951, 70.37);
        Coef_Desv[i++] = new Constante(1958, 66.17);
        Coef_Desv[i++] = new Constante(1964, 63.24);
        Coef_Desv[i++] = new Constante(1965, 60.93);
        Coef_Desv[i++] = new Constante(1966, 58.2);
        Coef_Desv[i++] = new Constante(1967, 54.44);
        Coef_Desv[i++] = new Constante(1970, 50.41);
        Coef_Desv[i++] = new Constante(1971, 47.98);
        Coef_Desv[i++] = new Constante(1972, 44.85);
        Coef_Desv[i++] = new Constante(1973, 40.78);
        Coef_Desv[i++] = new Constante(1974, 31.27);
        Coef_Desv[i++] = new Constante(1975, 26.72);
        Coef_Desv[i++] = new Constante(1976, 22.37);
        Coef_Desv[i++] = new Constante(1977, 17.17);
        Coef_Desv[i++] = new Constante(1978, 13.43);
        Coef_Desv[i++] = new Constante(1979, 10.6);
        Coef_Desv[i++] = new Constante(1980, 9.55);
        Coef_Desv[i++] = new Constante(1981, 7.81);
        Coef_Desv[i++] = new Constante(1982, 6.49);
        Coef_Desv[i++] = new Constante(1983, 5.18);
        Coef_Desv[i++] = new Constante(1984, 4.02);
        Coef_Desv[i++] = new Constante(1985, 3.36);
        Coef_Desv[i++] = new Constante(1986, 3.04);
        Coef_Desv[i++] = new Constante(1987, 2.79);
        Coef_Desv[i++] = new Constante(1988, 2.52);
        Coef_Desv[i++] = new Constante(1989, 2.26);
        Coef_Desv[i++] = new Constante(1990, 2.02);
        Coef_Desv[i++] = new Constante(1991, 1.79);
        Coef_Desv[i++] = new Constante(1992, 1.65);
        Coef_Desv[i++] = new Constante(1993, 1.53);
        Coef_Desv[i++] = new Constante(1994, 1.45);
        Coef_Desv[i++] = new Constante(1995, 1.4);
        Coef_Desv[i++] = new Constante(1996, 1.36);
        Coef_Desv[i++] = new Constante(1997, 1.34);
        Coef_Desv[i++] = new Constante(1998, 1.3);
        Coef_Desv[i++] = new Constante(1999, 1.28);
        Coef_Desv[i++] = new Constante(2000, 1.25);
        Coef_Desv[i++] = new Constante(2001, 1.17);
        Coef_Desv[i++] = new Constante(2002, 1.13);
        Coef_Desv[i++] = new Constante(2003, 1.1);
        Coef_Desv[i++] = new Constante(2004, 1.08);
        Coef_Desv[i++] = new Constante(2005, 1.05);
        Coef_Desv[i++] = new Constante(2006, 1.02);
        Coef_Desv[i++] = new Constante(2007, 1.0);
        return Coef_Desv;
    }
}

