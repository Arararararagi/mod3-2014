/*
 * Decompiled with CFR 0_102.
 */
package calculo.common.constants.tables.desvalorizacao;

import calculo.common.constants.Constante;

public class Moeda2001 {
    private static int max_Coef_Desv = 57;

    public static Constante[] getTable() {
        int i = 0;
        Constante[] Coef_Desv = new Constante[max_Coef_Desv];
        Coef_Desv[i] = new Constante(1903, 3416.835);
        Coef_Desv[++i] = new Constante(1904, 3180.65);
        Coef_Desv[++i] = new Constante(1911, 3050.61);
        Coef_Desv[++i] = new Constante(1915, 2714.11);
        Coef_Desv[++i] = new Constante(1916, 2221.52);
        Coef_Desv[++i] = new Constante(1917, 1773.44);
        Coef_Desv[++i] = new Constante(1918, 1265.3);
        Coef_Desv[++i] = new Constante(1919, 969.71);
        Coef_Desv[++i] = new Constante(1920, 640.73);
        Coef_Desv[++i] = new Constante(1921, 418.06);
        Coef_Desv[++i] = new Constante(1922, 309.6);
        Coef_Desv[++i] = new Constante(1923, 189.49);
        Coef_Desv[++i] = new Constante(1924, 159.51);
        Coef_Desv[++i] = new Constante(1925, 137.48);
        Coef_Desv[++i] = new Constante(1937, 133.5);
        Coef_Desv[++i] = new Constante(1940, 112.35);
        Coef_Desv[++i] = new Constante(1941, 99.77);
        Coef_Desv[++i] = new Constante(1942, 86.14);
        Coef_Desv[++i] = new Constante(1943, 73.36);
        Coef_Desv[++i] = new Constante(1944, 62.28);
        Coef_Desv[++i] = new Constante(1951, 57.12);
        Coef_Desv[++i] = new Constante(1958, 53.71);
        Coef_Desv[++i] = new Constante(1964, 51.32);
        Coef_Desv[++i] = new Constante(1965, 49.46);
        Coef_Desv[++i] = new Constante(1966, 47.24);
        Coef_Desv[++i] = new Constante(1967, 44.19);
        Coef_Desv[++i] = new Constante(1970, 40.92);
        Coef_Desv[++i] = new Constante(1971, 38.95);
        Coef_Desv[++i] = new Constante(1972, 36.41);
        Coef_Desv[++i] = new Constante(1973, 33.1);
        Coef_Desv[++i] = new Constante(1974, 25.38);
        Coef_Desv[++i] = new Constante(1975, 21.69);
        Coef_Desv[++i] = new Constante(1976, 18.16);
        Coef_Desv[++i] = new Constante(1977, 13.94);
        Coef_Desv[++i] = new Constante(1978, 10.92);
        Coef_Desv[++i] = new Constante(1979, 8.6);
        Coef_Desv[++i] = new Constante(1980, 7.76);
        Coef_Desv[++i] = new Constante(1981, 6.34);
        Coef_Desv[++i] = new Constante(1982, 5.27);
        Coef_Desv[++i] = new Constante(1983, 4.2);
        Coef_Desv[++i] = new Constante(1984, 3.27);
        Coef_Desv[++i] = new Constante(1985, 2.72);
        Coef_Desv[++i] = new Constante(1986, 2.48);
        Coef_Desv[++i] = new Constante(1987, 2.26);
        Coef_Desv[++i] = new Constante(1988, 2.06);
        Coef_Desv[++i] = new Constante(1989, 1.82);
        Coef_Desv[++i] = new Constante(1990, 1.64);
        Coef_Desv[++i] = new Constante(1991, 1.45);
        Coef_Desv[++i] = new Constante(1992, 1.35);
        Coef_Desv[++i] = new Constante(1993, 1.25);
        Coef_Desv[++i] = new Constante(1994, 1.19);
        Coef_Desv[++i] = new Constante(1995, 1.14);
        Coef_Desv[++i] = new Constante(1996, 1.1);
        Coef_Desv[++i] = new Constante(1997, 1.08);
        Coef_Desv[++i] = new Constante(1998, 1.05);
        Coef_Desv[++i] = new Constante(1999, 1.03);
        Coef_Desv[++i] = new Constante(2000, 1.0);
        ++i;
        return Coef_Desv;
    }
}

