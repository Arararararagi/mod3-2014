/*
 * Decompiled with CFR 0_102.
 */
package calculo.common.constants.tables.desvalorizacao;

import calculo.common.constants.Constante;

public class Moeda2010 {
    private static int max_Coef_Desv = 66;

    public static Constante[] getTable() {
        int i = 0;
        Constante[] Coef_Desv = new Constante[max_Coef_Desv];
        Coef_Desv[i++] = new Constante(1903, 4275.74);
        Coef_Desv[i++] = new Constante(1904, 3980.2);
        Coef_Desv[i++] = new Constante(1911, 3817.46);
        Coef_Desv[i++] = new Constante(1915, 3396.37);
        Coef_Desv[i++] = new Constante(1916, 2779.95);
        Coef_Desv[i++] = new Constante(1917, 2219.23);
        Coef_Desv[i++] = new Constante(1918, 1583.36);
        Coef_Desv[i++] = new Constante(1919, 1213.47);
        Coef_Desv[i++] = new Constante(1920, 801.81);
        Coef_Desv[i++] = new Constante(1921, 523.14);
        Coef_Desv[i++] = new Constante(1922, 387.44);
        Coef_Desv[i++] = new Constante(1923, 237.11);
        Coef_Desv[i++] = new Constante(1924, 199.59);
        Coef_Desv[i++] = new Constante(1925, 172.03);
        Coef_Desv[i++] = new Constante(1937, 167.06);
        Coef_Desv[i++] = new Constante(1940, 140.58);
        Coef_Desv[i++] = new Constante(1941, 124.86);
        Coef_Desv[i++] = new Constante(1942, 107.8);
        Coef_Desv[i++] = new Constante(1943, 91.79);
        Coef_Desv[i++] = new Constante(1944, 77.93);
        Coef_Desv[i++] = new Constante(1951, 71.48);
        Coef_Desv[i++] = new Constante(1958, 67.21);
        Coef_Desv[i++] = new Constante(1964, 64.24);
        Coef_Desv[i++] = new Constante(1965, 61.88);
        Coef_Desv[i++] = new Constante(1966, 59.12);
        Coef_Desv[i++] = new Constante(1967, 55.29);
        Coef_Desv[i++] = new Constante(1970, 51.2);
        Coef_Desv[i++] = new Constante(1971, 48.74);
        Coef_Desv[i++] = new Constante(1972, 45.56);
        Coef_Desv[i++] = new Constante(1973, 41.42);
        Coef_Desv[i++] = new Constante(1974, 31.77);
        Coef_Desv[i++] = new Constante(1975, 27.14);
        Coef_Desv[i++] = new Constante(1976, 22.73);
        Coef_Desv[i++] = new Constante(1977, 17.44);
        Coef_Desv[i++] = new Constante(1978, 13.64);
        Coef_Desv[i++] = new Constante(1979, 10.76);
        Coef_Desv[i++] = new Constante(1980, 9.7);
        Coef_Desv[i++] = new Constante(1981, 7.94);
        Coef_Desv[i++] = new Constante(1982, 6.59);
        Coef_Desv[i++] = new Constante(1983, 5.27);
        Coef_Desv[i++] = new Constante(1984, 4.09);
        Coef_Desv[i++] = new Constante(1985, 3.42);
        Coef_Desv[i++] = new Constante(1986, 3.09);
        Coef_Desv[i++] = new Constante(1987, 2.83);
        Coef_Desv[i++] = new Constante(1988, 2.55);
        Coef_Desv[i++] = new Constante(1989, 2.3);
        Coef_Desv[i++] = new Constante(1990, 2.05);
        Coef_Desv[i++] = new Constante(1991, 1.82);
        Coef_Desv[i++] = new Constante(1992, 1.67);
        Coef_Desv[i++] = new Constante(1993, 1.55);
        Coef_Desv[i++] = new Constante(1994, 1.48);
        Coef_Desv[i++] = new Constante(1995, 1.43);
        Coef_Desv[i++] = new Constante(1996, 1.39);
        Coef_Desv[i++] = new Constante(1997, 1.37);
        Coef_Desv[i++] = new Constante(1998, 1.32);
        Coef_Desv[i++] = new Constante(1999, 1.3);
        Coef_Desv[i++] = new Constante(2000, 1.27);
        Coef_Desv[i++] = new Constante(2001, 1.19);
        Coef_Desv[i++] = new Constante(2002, 1.15);
        Coef_Desv[i++] = new Constante(2003, 1.11);
        Coef_Desv[i++] = new Constante(2004, 1.09);
        Coef_Desv[i++] = new Constante(2005, 1.07);
        Coef_Desv[i++] = new Constante(2006, 1.04);
        Coef_Desv[i++] = new Constante(2007, 1.02);
        Coef_Desv[i++] = new Constante(2008, 0.99);
        Coef_Desv[i++] = new Constante(2009, 1.0);
        return Coef_Desv;
    }
}

