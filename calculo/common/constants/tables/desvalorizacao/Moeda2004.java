/*
 * Decompiled with CFR 0_102.
 */
package calculo.common.constants.tables.desvalorizacao;

import calculo.common.constants.Constante;

public class Moeda2004 {
    private static int max_Coef_Desv = 60;

    public static Constante[] getTable() {
        int i = 0;
        Constante[] Coef_Desv = new Constante[max_Coef_Desv];
        Coef_Desv[i] = new Constante(1903, 3813.68);
        Coef_Desv[++i] = new Constante(1904, 3550.07);
        Coef_Desv[++i] = new Constante(1911, 3404.92);
        Coef_Desv[++i] = new Constante(1915, 3029.34);
        Coef_Desv[++i] = new Constante(1916, 2479.54);
        Coef_Desv[++i] = new Constante(1917, 1979.41);
        Coef_Desv[++i] = new Constante(1918, 1412.26);
        Coef_Desv[++i] = new Constante(1919, 1082.34);
        Coef_Desv[++i] = new Constante(1920, 715.15);
        Coef_Desv[++i] = new Constante(1921, 466.61);
        Coef_Desv[++i] = new Constante(1922, 345.56);
        Coef_Desv[++i] = new Constante(1923, 211.5);
        Coef_Desv[++i] = new Constante(1924, 178.03);
        Coef_Desv[++i] = new Constante(1925, 153.45);
        Coef_Desv[++i] = new Constante(1937, 149.01);
        Coef_Desv[++i] = new Constante(1940, 125.4);
        Coef_Desv[++i] = new Constante(1941, 111.36);
        Coef_Desv[++i] = new Constante(1942, 96.15);
        Coef_Desv[++i] = new Constante(1943, 81.88);
        Coef_Desv[++i] = new Constante(1944, 69.52);
        Coef_Desv[++i] = new Constante(1951, 63.75);
        Coef_Desv[++i] = new Constante(1958, 59.95);
        Coef_Desv[++i] = new Constante(1964, 57.29);
        Coef_Desv[++i] = new Constante(1965, 55.2);
        Coef_Desv[++i] = new Constante(1966, 52.73);
        Coef_Desv[++i] = new Constante(1967, 49.32);
        Coef_Desv[++i] = new Constante(1970, 45.67);
        Coef_Desv[++i] = new Constante(1971, 43.47);
        Coef_Desv[++i] = new Constante(1972, 40.64);
        Coef_Desv[++i] = new Constante(1973, 36.94);
        Coef_Desv[++i] = new Constante(1974, 28.33);
        Coef_Desv[++i] = new Constante(1975, 24.21);
        Coef_Desv[++i] = new Constante(1976, 20.26);
        Coef_Desv[++i] = new Constante(1977, 15.56);
        Coef_Desv[++i] = new Constante(1978, 12.18);
        Coef_Desv[++i] = new Constante(1979, 9.6);
        Coef_Desv[++i] = new Constante(1980, 8.66);
        Coef_Desv[++i] = new Constante(1981, 7.08);
        Coef_Desv[++i] = new Constante(1982, 5.88);
        Coef_Desv[++i] = new Constante(1983, 4.69);
        Coef_Desv[++i] = new Constante(1984, 3.65);
        Coef_Desv[++i] = new Constante(1985, 3.04);
        Coef_Desv[++i] = new Constante(1986, 2.76);
        Coef_Desv[++i] = new Constante(1987, 2.52);
        Coef_Desv[++i] = new Constante(1988, 2.29);
        Coef_Desv[++i] = new Constante(1989, 2.04);
        Coef_Desv[++i] = new Constante(1990, 1.83);
        Coef_Desv[++i] = new Constante(1991, 1.62);
        Coef_Desv[++i] = new Constante(1992, 1.5);
        Coef_Desv[++i] = new Constante(1993, 1.39);
        Coef_Desv[++i] = new Constante(1994, 1.32);
        Coef_Desv[++i] = new Constante(1995, 1.27);
        Coef_Desv[++i] = new Constante(1996, 1.23);
        Coef_Desv[++i] = new Constante(1997, 1.21);
        Coef_Desv[++i] = new Constante(1998, 1.17);
        Coef_Desv[++i] = new Constante(1999, 1.15);
        Coef_Desv[++i] = new Constante(2000, 1.12);
        Coef_Desv[++i] = new Constante(2001, 1.07);
        Coef_Desv[++i] = new Constante(2002, 1.03);
        Coef_Desv[++i] = new Constante(2003, 1.0);
        ++i;
        return Coef_Desv;
    }
}

