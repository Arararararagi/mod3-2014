/*
 * Decompiled with CFR 0_102.
 */
package calculo.common.constants.tables.desvalorizacao;

import calculo.common.constants.Constante;

public class Moeda2009 {
    private static int max_Coef_Desv = 65;

    public static Constante[] getTable() {
        int i = 0;
        Constante[] Coef_Desv = new Constante[max_Coef_Desv];
        Coef_Desv[i++] = new Constante(1903, 4318.93);
        Coef_Desv[i++] = new Constante(1904, 4020.4);
        Coef_Desv[i++] = new Constante(1911, 3856.02);
        Coef_Desv[i++] = new Constante(1915, 3430.68);
        Coef_Desv[i++] = new Constante(1916, 2808.03);
        Coef_Desv[i++] = new Constante(1917, 2241.65);
        Coef_Desv[i++] = new Constante(1918, 1599.35);
        Coef_Desv[i++] = new Constante(1919, 1225.73);
        Coef_Desv[i++] = new Constante(1920, 809.91);
        Coef_Desv[i++] = new Constante(1921, 528.42);
        Coef_Desv[i++] = new Constante(1922, 391.45);
        Coef_Desv[i++] = new Constante(1923, 239.61);
        Coef_Desv[i++] = new Constante(1924, 201.61);
        Coef_Desv[i++] = new Constante(1925, 173.77);
        Coef_Desv[i++] = new Constante(1937, 168.75);
        Coef_Desv[i++] = new Constante(1940, 142.0);
        Coef_Desv[i++] = new Constante(1941, 126.12);
        Coef_Desv[i++] = new Constante(1942, 108.89);
        Coef_Desv[i++] = new Constante(1943, 92.72);
        Coef_Desv[i++] = new Constante(1944, 78.72);
        Coef_Desv[i++] = new Constante(1951, 72.2);
        Coef_Desv[i++] = new Constante(1958, 67.89);
        Coef_Desv[i++] = new Constante(1964, 64.89);
        Coef_Desv[i++] = new Constante(1965, 62.51);
        Coef_Desv[i++] = new Constante(1966, 59.72);
        Coef_Desv[i++] = new Constante(1967, 55.85);
        Coef_Desv[i++] = new Constante(1970, 51.72);
        Coef_Desv[i++] = new Constante(1971, 49.23);
        Coef_Desv[i++] = new Constante(1972, 46.02);
        Coef_Desv[i++] = new Constante(1973, 41.84);
        Coef_Desv[i++] = new Constante(1974, 32.09);
        Coef_Desv[i++] = new Constante(1975, 27.41);
        Coef_Desv[i++] = new Constante(1976, 22.96);
        Coef_Desv[i++] = new Constante(1977, 17.62);
        Coef_Desv[i++] = new Constante(1978, 13.78);
        Coef_Desv[i++] = new Constante(1979, 10.87);
        Coef_Desv[i++] = new Constante(1980, 9.8);
        Coef_Desv[i++] = new Constante(1981, 8.02);
        Coef_Desv[i++] = new Constante(1982, 6.66);
        Coef_Desv[i++] = new Constante(1983, 5.32);
        Coef_Desv[i++] = new Constante(1984, 4.13);
        Coef_Desv[i++] = new Constante(1985, 3.45);
        Coef_Desv[i++] = new Constante(1986, 3.12);
        Coef_Desv[i++] = new Constante(1987, 2.86);
        Coef_Desv[i++] = new Constante(1988, 2.58);
        Coef_Desv[i++] = new Constante(1989, 2.32);
        Coef_Desv[i++] = new Constante(1990, 2.07);
        Coef_Desv[i++] = new Constante(1991, 1.84);
        Coef_Desv[i++] = new Constante(1992, 1.69);
        Coef_Desv[i++] = new Constante(1993, 1.57);
        Coef_Desv[i++] = new Constante(1994, 1.49);
        Coef_Desv[i++] = new Constante(1995, 1.44);
        Coef_Desv[i++] = new Constante(1996, 1.4);
        Coef_Desv[i++] = new Constante(1997, 1.38);
        Coef_Desv[i++] = new Constante(1998, 1.33);
        Coef_Desv[i++] = new Constante(1999, 1.31);
        Coef_Desv[i++] = new Constante(2000, 1.28);
        Coef_Desv[i++] = new Constante(2001, 1.2);
        Coef_Desv[i++] = new Constante(2002, 1.16);
        Coef_Desv[i++] = new Constante(2003, 1.12);
        Coef_Desv[i++] = new Constante(2004, 1.1);
        Coef_Desv[i++] = new Constante(2005, 1.08);
        Coef_Desv[i++] = new Constante(2006, 1.05);
        Coef_Desv[i++] = new Constante(2007, 1.03);
        Coef_Desv[i++] = new Constante(2008, 1.0);
        return Coef_Desv;
    }
}

