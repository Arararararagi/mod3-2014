/*
 * Decompiled with CFR 0_102.
 */
package calculo.common.constants;

public class Constante {
    final int id;
    final double valor;

    public Constante() {
        this.id = 0;
        this.valor = 0.0;
    }

    public Constante(int idi, double valori) {
        this.id = idi;
        this.valor = valori;
    }
}

