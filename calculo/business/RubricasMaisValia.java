/*
 * Decompiled with CFR 0_102.
 */
package calculo.business;

import calculo.business.Rubricas;
import calculo.business.pojos.Rubrica;
import calculo.business.pojos.RubricaMaisValia;
import calculo.common.util.Utils;

public class RubricasMaisValia
extends Rubricas {
    public String getDataAquisicao(String idRubrica) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubrica(idRubrica);
        return rmv.getDataAquisicao();
    }

    public String getDataRealizacao(String idRubrica) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubrica(idRubrica);
        return rmv.getDataRealizacao();
    }

    public double getValAquisicao(String idRubrica) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubrica(idRubrica);
        return rmv.getValAquisicao();
    }

    public double getValRealizacao(String idRubrica) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubrica(idRubrica);
        return rmv.getValRealizacao();
    }

    public double getEncargos(String idRubrica) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubrica(idRubrica);
        return rmv.getValEncargos();
    }

    public String getImpressao(boolean header) {
        String linha = "";
        String html = "";
        RubricaMaisValia rmv = null;
        int i = 0;
        if (header) {
            html = "<tr><td>&nbsp</td><td align=center>D.Realiza&ccedil;&atilde;o</td><td align=center>V.Realiza&ccedil;&atilde;o</td><td align=center>D.Aquisi&ccedil;&atilde;o</td><td align=center>V.Aquisi&ccedil;&atilde;o</td><td align=center>V.Despesas</td></tr>";
        }
        while (i < this.getNumRubricas()) {
            rmv = (RubricaMaisValia)this.rubricas[i];
            if (!rmv.getDesc().equals("") && rmv.isPreenchida()) {
                linha = "<tr><td>&nbsp;" + rmv.getDesc() + "</td>" + "<td align=center width=12%>" + rmv.getDataRealizacao() + "</td>" + "<td align=right width=12%>" + Utils.mascaraValor(rmv.getValRealizacao()) + "</td>" + "<td align=center width=12%>" + rmv.getDataAquisicao() + "</td>" + "<td align=right width=12%>" + Utils.mascaraValor(rmv.getValAquisicao()) + "</td>" + "<td align=right width=12%>" + Utils.mascaraValor(rmv.getValEncargos()) + "</td></tr>";
                html = String.valueOf(html) + linha;
            }
            ++i;
        }
        return html;
    }

    @Override
    public int comValor() {
        int comValor = 0;
        RubricaMaisValia rmv = null;
        for (int i = 0; i < this.getNumRubricas(); ++i) {
            rmv = (RubricaMaisValia)this.rubricas[i];
            if (!rmv.isPreenchida() || rmv.getDesc().equals("")) continue;
            ++comValor;
        }
        return comValor;
    }
}

