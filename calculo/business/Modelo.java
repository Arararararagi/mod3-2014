/*
 * Decompiled with CFR 0_102.
 */
package calculo.business;

import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;

public abstract class Modelo {
    protected Rubricas[] modelo;
    private int indexRubr;
    private int maxIndexRubr;
    public double resSimulacao;
    protected Contribuinte[] contrib;
    private int indexCont;
    private int maxIndexCont;

    public void inicializa(int maxIndRubr, int maxIndCont) {
        this.maxIndexRubr = maxIndRubr;
        this.indexRubr = 0;
        this.modelo = null;
        this.modelo = new Rubricas[this.maxIndexRubr];
        this.maxIndexCont = maxIndCont;
        this.indexCont = 0;
        this.contrib = null;
        this.contrib = new Contribuinte[this.maxIndexCont];
    }

    public void addRubricas(Rubricas rubricas) {
        this.modelo[this.indexRubr] = new Rubricas();
        this.modelo[this.indexRubr] = rubricas;
        ++this.indexRubr;
    }

    public void addContribuinte(Contribuinte cont) {
        this.contrib[this.indexCont] = new Contribuinte();
        this.contrib[this.indexCont] = cont;
        ++this.indexCont;
    }

    public Rubricas getRubricas(String idRubricas) {
        int i;
        for (i = 0; !(i >= this.indexRubr || this.modelo[i].getID().equals(idRubricas)); ++i) {
        }
        if (i == this.indexRubr) {
            return null;
        }
        return this.modelo[i];
    }

    public Contribuinte getContribuinte(String idCont) {
        int i;
        for (i = 0; !(i >= this.indexCont || this.contrib[i].getIdCont().equals(idCont)); ++i) {
        }
        if (i == this.indexCont) {
            return null;
        }
        return this.contrib[i];
    }

    public Contribuinte getContribuinte(int index) {
        return this.contrib[index];
    }

    public Contribuinte[] getContribuintes() {
        return this.contrib;
    }

    public int numContComRend() {
        int res = 0;
        for (int i = 0; i < this.indexCont; ++i) {
            if (!this.contrib[i].isComRendim()) continue;
            res = res++;
        }
        return res;
    }
}

