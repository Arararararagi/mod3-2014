/*
 * Decompiled with CFR 0_102.
 */
package calculo.business;

import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;
import calculo.common.util.Utils;

public class Rubricas {
    public Rubrica[] rubricas;
    private int index;
    private int maxIndex;
    private String id;

    public void inicializa(int maxInd) {
        this.maxIndex = maxInd;
        this.rubricas = null;
        this.rubricas = new Rubrica[this.maxIndex];
        this.index = 0;
    }

    public void addRubrica(Rubrica rubrica) {
        this.rubricas[this.index] = new Rubrica();
        this.rubricas[this.index] = rubrica;
        ++this.index;
    }

    public Rubrica getRubrica(String idRubrica) {
        int i;
        for (i = 0; !(i >= this.index || this.rubricas[i].getID().equals(idRubrica)); ++i) {
        }
        if (i == this.index) {
            return null;
        }
        return this.rubricas[i];
    }

    public Rubrica getRubrica(int i) {
        return this.rubricas[i];
    }

    public Rubrica getRubrica(String idRubrica, String idCont) {
        int i;
        for (i = 0; !(i >= this.index || this.rubricas[i].getID().equals(idRubrica) && this.rubricas[i].getContrib().getIdCont().equals(idCont)); ++i) {
        }
        if (i == this.index) {
            return null;
        }
        return this.rubricas[i];
    }

    public void setID(String idRubricas) {
        this.id = idRubricas;
    }

    public String getID() {
        return this.id;
    }

    public String getImpRubricas() {
        int tr = 0;
        String desc = "";
        String imp = "";
        for (int i = 0; i < this.index; ++i) {
            if (this.rubricas[i].getDesc().equals("") || this.rubricas[i].getValor() == 0.0) continue;
            desc = this.rubricas[i].getContrib() == null ? this.rubricas[i].getDesc() : String.valueOf(this.rubricas[i].getContrib().getIdCont()) + " " + this.rubricas[i].getDesc();
            if (tr == 0) {
                imp = String.valueOf(imp) + "<tr>\n";
            }
            imp = String.valueOf(imp) + "<td width=38%>&nbsp;" + desc + "</td>\n";
            imp = this.rubricas[i].getID().equals("valdataContPreSPA") || this.rubricas[i].getID().equals("valdataContPreSPB") || this.rubricas[i].getID().equals("valdataPriPagPreSPA") || this.rubricas[i].getID().equals("valdataPriPagPreSPB") ? String.valueOf(imp) + "<td align=right width=12%>" + (int)this.rubricas[i].getValor() + "</td>\n" : String.valueOf(imp) + "<td align=right width=12%>" + Utils.mascaraValor(this.rubricas[i].getValor()) + "</td>\n";
            if (++tr != 2) continue;
            tr = 0;
            imp = String.valueOf(imp) + "</tr>";
        }
        return imp;
    }

    public double[] getValRubricas(String cont) {
        double[] valCont = new double[this.index];
        int j = 0;
        for (int i = 0; i < this.index; ++i) {
            if (!this.rubricas[i].getContrib().getIdCont().equals(cont)) continue;
            valCont[j] = this.rubricas[i].getValor();
            ++j;
        }
        return valCont;
    }

    public String getImpRubricasPorCont(boolean header, String AgrProf, String Enquadra) {
        double[] valSPA = this.getValRubricas("SPA");
        double[] valSPB = this.getValRubricas("SPB");
        double[] valD1 = this.getValRubricas("D1");
        double[] valD2 = this.getValRubricas("D2");
        double[] valD3 = this.getValRubricas("D3");
        double[] valSPF = this.getValRubricas("SPF");
        double somValSPF = Utils.somaArrayInt(valSPF, this.index);
        int[] indices = new int[this.index];
        int i = 0;
        int j = 0;
        String linha = "";
        if (header) {
            linha = "<td>&nbsp</td><td align=center nowrap>Sujeito A</td><td align=center nowrap>Sujeito B</td><td align=center nowrap>Depend. 1</td><td align=center nowrap>Depend. 2</td><td align=center nowrap>Depend. 3</td>";
        }
        if (somValSPF != 0.0) {
            linha = String.valueOf(linha) + "<td align=center nowrap>S. Falecido</td>";
        }
        String html = "";
        if (header || somValSPF != 0.0) {
            html = "<tr>" + linha + "</tr>\n";
        }
        if (header) {
            html = String.valueOf(html) + AgrProf + Enquadra;
        }
        while (i < this.index) {
            if (valSPA[i] != 0.0 || valSPB[i] != 0.0 || valD1[i] != 0.0 || valD2[i] != 0.0 || valD3[i] != 0.0 || valSPF[i] != 0.0) {
                indices[j] = i;
                ++j;
            }
            ++i;
        }
        for (i = 0; i < j; ++i) {
            linha = "<td>&nbsp;" + this.rubricas[indices[i]].getDesc() + "</td>";
            linha = String.valueOf(linha) + "<td align=right width=12%>" + Utils.mascValImp(valSPA[indices[i]]) + "</td>";
            linha = String.valueOf(linha) + "<td align=right width=12%>" + Utils.mascValImp(valSPB[indices[i]]) + "</td>";
            linha = String.valueOf(linha) + "<td align=right width=12%>" + Utils.mascValImp(valD1[indices[i]]) + "</td>";
            linha = String.valueOf(linha) + "<td align=right width=12%>" + Utils.mascValImp(valD2[indices[i]]) + "</td>";
            linha = String.valueOf(linha) + "<td align=right width=12%>" + Utils.mascValImp(valD3[indices[i]]) + "</td>";
            if (somValSPF != 0.0) {
                linha = String.valueOf(linha) + "<td align=right width=12%>" + Utils.mascValImp(valSPF[indices[i]]) + "</td>";
            }
            if (this.rubricas[indices[i]].getDesc().equals("")) continue;
            html = String.valueOf(html) + "<tr>" + linha + "</tr>\n";
        }
        return html;
    }

    public int comValor() {
        int comValor = 0;
        for (int i = 0; i < this.index; ++i) {
            if (this.rubricas[i].getValor() == 0.0 || this.rubricas[i].getDesc().equals("")) continue;
            ++comValor;
        }
        return comValor;
    }

    public int getNumRubricas() {
        return this.index;
    }

    public void inicialSujeito(String idSujeito) {
        for (int i = 0; i < this.index; ++i) {
            if (this.rubricas[i].getContrib() == null || !this.rubricas[i].getContrib().getIdCont().equals(idSujeito)) continue;
            this.rubricas[i].setValor(0.0);
        }
    }
}

