/*
 * Decompiled with CFR 0_102.
 */
package calculo.business;

import calculo.business.IntMod3;
import calculo.business.Modelo;
import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;
import calculo.business.pojos.RubricaMaisValia;
import calculo.business.pojos.SPA;
import calculo.common.util.Utils;
import calculo.rubricas.AbatDedColecta;
import calculo.rubricas.Acrescimos;
import calculo.rubricas.AnexoC;
import calculo.rubricas.AnexoD;
import calculo.rubricas.BenefFiscais;
import calculo.rubricas.BensImoveis;
import calculo.rubricas.Capitais;
import calculo.rubricas.CategoriaB;
import calculo.rubricas.DespEducacao;
import calculo.rubricas.DespesaAgrB;
import calculo.rubricas.DespesaProB;
import calculo.rubricas.IncrePatrim;
import calculo.rubricas.MaisValias;
import calculo.rubricas.OpFinancei;
import calculo.rubricas.PPA;
import calculo.rubricas.PPRPPE;
import calculo.rubricas.Pensoes;
import calculo.rubricas.PreReforma;
import calculo.rubricas.Prediais;
import calculo.rubricas.RPC;
import calculo.rubricas.RendAnosAnter;
import calculo.rubricas.RendIsentos;
import calculo.rubricas.RendTrabDep;
import calculo.rubricas.RendimAgrB;
import calculo.rubricas.RendimProB;
import calculo.rubricas.TribAutonomas;

public class Modelo3Med
extends Modelo
implements IntMod3 {
    private final int numContribs = 6;
    private final int numRubricas = 30;

    public Modelo3Med() {
        this.mod3Inicializa();
    }

    public int getNumContribs() {
        return 6;
    }

    public void mod3Inicializa() {
        this.inicializa(30, 6);
        this.addContribuinte(new SPA());
        this.addContribuinte(new Contribuinte("SPB"));
        this.addContribuinte(new Contribuinte("D1"));
        this.addContribuinte(new Contribuinte("D2"));
        this.addContribuinte(new Contribuinte("D3"));
        this.addContribuinte(new Contribuinte("SPF"));
        this.addRubricas(new RendTrabDep("RenTrabDep", this.getContribuintes()));
        this.addRubricas(new Pensoes("Pensoes", this.getContribuintes()));
        this.addRubricas(new AbatDedColecta("AbatDedColecta"));
        this.addRubricas(new BenefFiscais("BenefFiscais"));
        this.addRubricas(new PPA("PPA", this.getContribuintes()));
        this.addRubricas(new PPRPPE("PPRPPE", this.getContribuintes()));
        this.addRubricas(new RPC("RPC", this.getContribuintes()));
        this.addRubricas(new DespEducacao("DespEducacao"));
        this.addRubricas(new Prediais("Prediais"));
        this.addRubricas(new Capitais("Capitais"));
        this.addRubricas(new MaisValias("MaisValias"));
        this.addRubricas(new BensImoveis("BensImoveis"));
        this.addRubricas(new IncrePatrim("IncrePatrim"));
        this.addRubricas(new OpFinancei("OpFinancei"));
        this.addRubricas(new Acrescimos("Acrescimos"));
        this.addRubricas(new CategoriaB("CategoriaB", this.getContribuintes()));
        this.addRubricas(new AnexoC("AnexoC", this.getContribuintes()));
        this.addRubricas(new AnexoD("AnexoD", this.getContribuintes()));
        this.addRubricas(new RendimProB("RendimProB", this.getContribuintes()));
        this.addRubricas(new DespesaProB("DespesaProB", this.getContribuintes()));
        this.addRubricas(new RendimAgrB("RendimAgrB", this.getContribuintes()));
        this.addRubricas(new DespesaAgrB("DespesaAgrB", this.getContribuintes()));
        this.addRubricas(new RendIsentos("RendIsentos", this.getContribuintes()));
        this.addRubricas(new PreReforma("PreReforma"));
        this.addRubricas(new RendAnosAnter("RendAnosAnter"));
        this.addRubricas(new TribAutonomas("TribAutonomas", this.getContribuintes()));
    }

    @Override
    public void setVal(String idRubricas, String idRubrica, String value) {
        if (value.equals("")) {
            value = "0";
        }
        double val = Double.valueOf(value.replace(',', '.'));
        this.getRubricas(idRubricas).getRubrica(idRubrica).setValor(val);
    }

    public void setVal(String idRubricas, String idRubrica, String idCont, String value) {
        if (value.equals("")) {
            value = "0";
        }
        double val = Double.valueOf(value.replace(',', '.'));
        this.getRubricas(idRubricas).getRubrica(idRubrica, idCont).setValor(val);
    }

    @Override
    public void setVal(String idRubricas, String idRubrica, double value) {
        this.getRubricas(idRubricas).getRubrica(idRubrica).setValor(value);
    }

    public void setValHt(String idRubricas, String idRubrica, Object valObj) {
        double val = 0.0;
        val = valObj instanceof Integer ? ((Integer)valObj).doubleValue() : (valObj instanceof Double ? (Double)valObj : (valObj == null || "".equals(valObj) ? 0.0 : Double.valueOf((String)valObj)));
        this.getRubricas(idRubricas).getRubrica(idRubrica).setValor(val);
    }

    public void setValHt2(String idRubricas, String idRubrica, Object valObj) {
        double val = 0.0;
        if (valObj != null || "".equals(valObj)) {
            Integer val1 = (Integer)valObj;
            val = val1.doubleValue();
        }
        this.getRubricas(idRubricas).getRubrica(idRubrica).setValor(val);
    }

    @Override
    public void setVal(String idRubricas, String idRubrica, String idCont, double value) {
        this.getRubricas(idRubricas).getRubrica(idRubrica, idCont).setValor(value);
    }

    public void setValHt(String idRubricas, String idRubrica, String idCont, Object valObj) {
        double val = 0.0;
        val = valObj instanceof Integer ? ((Integer)valObj).doubleValue() : (valObj == null || "".equals(valObj) ? 0.0 : (Double)valObj);
        this.getRubricas(idRubricas).getRubrica(idRubrica, idCont).setValor(val);
    }

    public void setDataAquisicao(String idRubricas, String idRubrica, String value) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setDataAquisicao(value);
    }

    public void setDataAquisicaoHt(String idRubricas, String idRubrica, Object valObj) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setDataAquisicao((String)valObj);
    }

    public void setDataRealizacao(String idRubricas, String idRubrica, String value) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setDataRealizacao(value);
    }

    public void setDataRealizacaoHt(String idRubricas, String idRubrica, Object valObj) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setDataRealizacao((String)valObj);
    }

    public void setValAquisicao(String idRubricas, String idRubrica, String value) {
        if (value.equals("")) {
            value = "0";
        }
        double val = Double.valueOf(value.replace(',', '.'));
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setValAquisicao(val);
    }

    public void setValAquisicaoHt(String idRubricas, String idRubrica, Object valObj) {
        Double val1 = (Double)valObj;
        double val = val1;
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setValAquisicao(val);
    }

    public void setValRealizacao(String idRubricas, String idRubrica, String value) {
        if (value.equals("")) {
            value = "0";
        }
        double val = Double.valueOf(value.replace(',', '.'));
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setValRealizacao(val);
    }

    public void setValRealizacaoHt(String idRubricas, String idRubrica, Object valObj) {
        Double val1 = (Double)valObj;
        double val = val1;
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setValRealizacao(val);
    }

    public void setValEncargos(String idRubricas, String idRubrica, String value) {
        if (value.equals("")) {
            value = "0";
        }
        double val = Double.valueOf(value.replace(',', '.'));
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setValEncargos(val);
    }

    public void setValEncargosHt(String idRubricas, String idRubrica, Object valObj) {
        Double val1 = (Double)valObj;
        double val = val1;
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setValEncargos(val);
    }

    @Override
    public double getVal(String idRubricas, String idRubrica) {
        return this.getRubricas(idRubricas).getRubrica(idRubrica).getValor();
    }

    public String getDataAquisicao(String idRubricas, String idRubrica) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        return rmv.getDataAquisicao();
    }

    public String getCampoDataAquisicao(String idRubricas, String idRubrica) {
        String val = this.getDataAquisicao(idRubricas, idRubrica);
        String html = "<input type=text maxlength=14 size=7 class=txtbox1 name=" + idRubrica + " onFocus=\"this.select()\" onClick=\"this.select()\" value=" + val + ">";
        return html;
    }

    public String getDataRealizacao(String idRubricas, String idRubrica) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        return rmv.getDataRealizacao();
    }

    public String getCampoDataRealizacao(String idRubricas, String idRubrica) {
        String val = this.getDataRealizacao(idRubricas, idRubrica);
        String html = "<input type=text maxlength=14 size=7 class=txtbox1 name=" + idRubrica + " onFocus=\"this.select()\" onClick=\"this.select()\" value=" + val + ">";
        return html;
    }

    public double getValAquisicao(String idRubricas, String idRubrica) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        return rmv.getValAquisicao();
    }

    public String getCampoValAquisicao(String idRubricas, String idRubrica) {
        double val = this.getValAquisicao(idRubricas, idRubrica);
        String html = "<input type=text maxlength=14 size=10 class=txtbox1 name=" + idRubrica + " onFocus=\"this.select()\" onClick=\"this.select()\" value=" + Utils.mascVal(val) + ">";
        return html;
    }

    public double getValRealizacao(String idRubricas, String idRubrica) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        return rmv.getValRealizacao();
    }

    public String getCampoValRealizacao(String idRubricas, String idRubrica) {
        double val = this.getValRealizacao(idRubricas, idRubrica);
        String html = "<input type=text maxlength=14 size=10 class=txtbox1 name=" + idRubrica + " onFocus=\"this.select()\" onClick=\"this.select()\" value=" + Utils.mascVal(val) + ">";
        return html;
    }

    public double getValEncargos(String idRubricas, String idRubrica) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        return rmv.getValEncargos();
    }

    public String getCampoEncargos(String idRubricas, String idRubrica) {
        double val = this.getValEncargos(idRubricas, idRubrica);
        String html = "<input type=text maxlength=14 size=10 class=txtbox1 name=" + idRubrica + " onFocus=\"this.select()\" onClick=\"this.select()\" value=" + Utils.mascVal(val) + ">";
        return html;
    }

    @Override
    public double getVal(String idRubricas, String idRubrica, String numFisc) {
        if (this.getRubricas(idRubricas) != null) {
            if (this.getRubricas(idRubricas).getRubrica(idRubrica, numFisc) != null) {
                return this.getRubricas(idRubricas).getRubrica(idRubrica, numFisc).getValor();
            }
            return 0.0;
        }
        return 0.0;
    }

    @Override
    public String getCampo(String idRubricas, String idRubrica) {
        double val = this.getVal(idRubricas, idRubrica);
        String html = "<input type=text maxlength=14 size=10 class=txtbox1 name=" + idRubrica + " onFocus=\"this.select()\" onClick=\"this.select()\" value=" + Utils.mascVal(val) + ">";
        return html;
    }

    public String getCampo(String idRubricas, String idRubrica, String numFisc) {
        double val = this.getVal(idRubricas, idRubrica, numFisc);
        String html = "<input type=text maxlength=14 size=10 class=txtbox1 name=" + idRubrica + numFisc + " onFocus=\"this.select()\" onClick=\"this.select()\" value=" + Utils.mascVal(val) + ">";
        return html;
    }

    public void setClassifA(String idRubricas, String idRubrica, Object valObj) {
        Boolean val = (Boolean)valObj;
        Rubrica rubrica = this.getRubricas(idRubricas).getRubrica(idRubrica);
        if (val.booleanValue()) {
            rubrica.setValor(1.0);
        } else {
            rubrica.setValor(0.0);
        }
    }

    public boolean getClassifA(String idRubricas, String idRubrica) {
        boolean hasClassifA = false;
        double valor = this.getRubricas(idRubricas).getRubrica(idRubrica).getValor();
        hasClassifA = valor != 0.0;
        return hasClassifA;
    }

    public void setDataRealizacaoRec(String idRubricas, String idRubrica, String value) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setDataRealizacaoRec(value);
    }

    public String getDataRealizacaoRec(String idRubricas, String idRubrica) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        return rmv.getDataRealizacaoRec();
    }

    public void setDataAquisicaoRec(String idRubricas, String idRubrica, String value) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setDataAquisicaoRec(value);
    }

    public String getDataAquisicaoRec(String idRubricas, String idRubrica) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        return rmv.getDataAquisicaoRec();
    }

    public void setValAquisicaoRec(String idRubricas, String idRubrica, String value) {
        if (value.equals("")) {
            value = "0";
        }
        double val = Double.valueOf(value.replace(',', '.'));
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setValAquisicaoRec(val);
    }

    public double getValAquisicaoRec(String idRubricas, String idRubrica) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        return rmv.getValAquisicaoRec();
    }

    public void setValRealizacaoRec(String idRubricas, String idRubrica, String value) {
        if (value.equals("")) {
            value = "0";
        }
        double val = Double.valueOf(value.replace(',', '.'));
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setValRealizacaoRec(val);
    }

    public double getValRealizacaoRec(String idRubricas, String idRubrica) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        return rmv.getValRealizacaoRec();
    }

    public void setValEncargosRec(String idRubricas, String idRubrica, String value) {
        if (value.equals("")) {
            value = "0";
        }
        double val = Double.valueOf(value.replace(',', '.'));
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setValEncargosRec(val);
    }

    public double getValEncargosRec(String idRubricas, String idRubrica) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        return rmv.getValEncargosRec();
    }

    public void setDataRealizacaoRecHt(String idRubricas, String idRubrica, Object valObj) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setDataRealizacaoRec((String)valObj);
    }

    public void setDataAquisicaoRecHt(String idRubricas, String idRubrica, Object valObj) {
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setDataAquisicaoRec((String)valObj);
    }

    public void setValRealizacaoRecHt(String idRubricas, String idRubrica, Object valObj) {
        Double val1 = (Double)valObj;
        double val = val1;
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setValRealizacaoRec(val);
    }

    public void setValAquisicaoRecHt(String idRubricas, String idRubrica, Object valObj) {
        Double val1 = (Double)valObj;
        double val = val1;
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setValAquisicaoRec(val);
    }

    public void setValEncargosRecHt(String idRubricas, String idRubrica, Object valObj) {
        Double val1 = (Double)valObj;
        double val = val1;
        RubricaMaisValia rmv = (RubricaMaisValia)this.getRubricas(idRubricas).getRubrica(idRubrica);
        rmv.setValEncargosRec(val);
    }

    public void setImovelAlienado(String idRubricas, String value) {
        MaisValias mv = (MaisValias)this.getRubricas(idRubricas);
        mv.setBemAlienado(value);
    }

    public String getImovelAlienado(String idRubricas) {
        MaisValias mv = (MaisValias)this.getRubricas(idRubricas);
        if (mv.getBemAlienado() == null) {
            return "";
        }
        return mv.getBemAlienado();
    }
}

