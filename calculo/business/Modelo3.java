/*
 * Decompiled with CFR 0_102.
 */
package calculo.business;

import calculo.business.Modelo3Med;
import calculo.business.logic.Calc2007;
import calculo.business.logic.Calc2008;
import calculo.business.logic.Calc2009;
import calculo.business.logic.Calc2010;
import calculo.business.logic.Calc2011;
import calculo.business.logic.Calc2012;
import calculo.business.logic.Calc2013;
import calculo.business.logic.Calc2014;
import calculo.business.logic.CalcInt;
import calculo.business.logic.CalcOld;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.SPA;
import calculo.common.constants.tables.taxasgerais.Taxas2001;
import calculo.common.constants.tables.taxasgerais.Taxas2002;
import calculo.common.constants.tables.taxasgerais.Taxas2003;
import calculo.common.constants.tables.taxasgerais.Taxas2004;
import calculo.common.constants.tables.taxasgerais.Taxas2005;
import calculo.common.constants.tables.taxasgerais.Taxas2006;
import calculo.common.constants.tables.taxasgerais.Taxas2007;
import calculo.common.constants.tables.taxasgerais.Taxas2008;
import calculo.common.constants.tables.taxasgerais.Taxas2009;
import calculo.common.constants.tables.taxasgerais.Taxas2010;
import calculo.common.constants.tables.taxasgerais.Taxas2011;
import calculo.common.constants.tables.taxasgerais.Taxas2012;
import calculo.common.constants.tables.taxasgerais.Taxas2013;
import calculo.common.constants.tables.taxasgerais.Taxas2014;
import java.util.HashMap;
import java.util.Map;

public class Modelo3
extends Modelo3Med {
    protected CalcInt calcLogic;

    public Map setupCalc(int ano) {
        int anoSim = ((SPA)this.getContribuinte("SPA")).getAnoSim();
        Map tab = new HashMap();
        if (ano != 0) {
            anoSim = ano;
        }
        switch (anoSim) {
            case 14: {
                tab = Taxas2014.getTable();
                this.calcLogic = new Calc2014(this);
                break;
            }
            case 13: {
                tab = Taxas2013.getTable();
                this.calcLogic = new Calc2013(this);
                break;
            }
            case 12: {
                tab = Taxas2012.getTable();
                this.calcLogic = new Calc2012(this);
                break;
            }
            case 11: {
                tab = Taxas2011.getTable();
                this.calcLogic = new Calc2011(this);
                break;
            }
            case 10: {
                tab = Taxas2010.getTable();
                this.calcLogic = new Calc2010(this);
                break;
            }
            case 9: {
                tab = Taxas2009.getTable();
                this.calcLogic = new Calc2009(this);
                break;
            }
            case 8: {
                tab = Taxas2008.getTable();
                this.calcLogic = new Calc2008(this);
                break;
            }
            case 1: {
                tab = Taxas2007.getTable();
                this.calcLogic = new Calc2007(this);
                break;
            }
            case 2: {
                tab = Taxas2006.getTable();
                this.calcLogic = new CalcOld(this);
                break;
            }
            case 3: {
                tab = Taxas2005.getTable();
                this.calcLogic = new CalcOld(this);
                break;
            }
            case 4: {
                tab = Taxas2004.getTable();
                this.calcLogic = new CalcOld(this);
                break;
            }
            case 5: {
                tab = Taxas2003.getTable();
                this.calcLogic = new CalcOld(this);
                break;
            }
            case 6: {
                tab = Taxas2002.getTable();
                this.calcLogic = new CalcOld(this);
                break;
            }
            case 7: {
                tab = Taxas2001.getTable();
                this.calcLogic = new CalcOld(this);
            }
        }
        return tab;
    }

    public double efectuaSimulacaoBatch(HashMap<String, Double> taxas, int ano) {
        this.setupCalc(ano);
        this.calcLogic.defineConstantes(taxas);
        this.resSimulacao = this.calcLogic.doSimulacao();
        return this.resSimulacao;
    }

    public double efectuaSimulacao() {
        Map map = this.setupCalc(0);
        this.calcLogic.defineConstantes(map);
        this.resSimulacao = this.calcLogic.doSimulacao();
        return this.resSimulacao;
    }

    public double calcPagamConta() {
        return this.calcLogic.calcPagamConta();
    }

    public double getValorMaisVal() {
        return this.calcLogic.getValorMaisVal();
    }

    public double getValRendimGlobal() {
        return this.calcLogic.getValRendimGlobal();
    }

    public double getValDedEspecificas() {
        return this.calcLogic.getValDedEspecificas();
    }

    public double getValPerdasRecup() {
        return this.calcLogic.getValPerdasRecup();
    }

    public double getValAbatimentos() {
        return this.calcLogic.getValAbatimentos();
    }

    public double getValRendimDetTaxas() {
        return this.calcLogic.getValRendimDetTaxas();
    }

    public double getValRendimColectavel() {
        return this.calcLogic.getValRendimColectavel();
    }

    public double getValRendimIsentos() {
        return this.calcLogic.getValRendimIsentos();
    }

    public double getValQuoRendAnter() {
        return this.calcLogic.getValQuoRendAnter();
    }

    public double getValCoefConjugal() {
        return this.calcLogic.getValCoefConjugal();
    }

    public double getValTaxaImposto() {
        return this.calcLogic.getValTaxaImposto();
    }

    public double getValImportApurada() {
        return this.calcLogic.getValImportApurada();
    }

    public double getValParcelaAbater() {
        return this.calcLogic.getValParcelaAbater();
    }

    public double getValorColRendIsent() {
        return this.calcLogic.getValorColRendIsent();
    }

    public double getValorApurado() {
        return this.calcLogic.getValorApurado();
    }

    public double getValImpTribAutonoma() {
        return this.calcLogic.getValImpTribAutonoma();
    }

    public double getValColectaTotal() {
        return this.calcLogic.getValColectaTotal();
    }

    public double getValDeduColecta() {
        return this.calcLogic.getValDeduColecta();
    }

    public double getValAcrescColecta() {
        return this.calcLogic.getValAcrescColecta();
    }

    public double getValColectaLiquida() {
        return this.calcLogic.getValColectaLiquida();
    }

    public double getValPagamentoConta() {
        return this.calcLogic.getValPagamentoConta();
    }

    public double getValRetencoesFonte() {
        return this.calcLogic.getValRetencoesFonte();
    }

    public double getValImpostoApurado() {
        return this.calcLogic.getValImpostoApurado();
    }

    public boolean getMinExistencia() {
        return this.calcLogic.getMinExistencia();
    }

    public double getImpostoPago() {
        return this.calcLogic.getImpostoPago();
    }

    public double getImpostoRecebido() {
        return this.calcLogic.getImpostoRecebido();
    }

    public double getValImpostAnosAnt() {
        return this.calcLogic.getValImpostAnosAnt();
    }

    public double getValBenFiscalSF() {
        return this.calcLogic.getValBenFiscalSF();
    }

    public double getValStRendimento() {
        return this.calcLogic.getValStRendimento();
    }

    public double getValStDeducoes() {
        return this.calcLogic.getValStDeducoes();
    }

    public double getValStColecta() {
        return this.calcLogic.getValStColecta();
    }

    public double getValStRetencoes() {
        return this.calcLogic.getValStRetencoes();
    }

    public double getValStRendimentoXTaxa() {
        return this.calcLogic.getValStRendimentoXTaxa();
    }

    public double getValTaxaAdicional() {
        return this.calcLogic.getValTaxaAdicional();
    }
}

