/*
 * Decompiled with CFR 0_102.
 */
package calculo.business.pojos;

import calculo.business.pojos.Contribuinte;

public class Rubrica {
    private double valor;
    private String id;
    private Contribuinte contribuinte;
    private String desc;

    public Rubrica() {
        this.setID("");
        this.setValor(0.0);
    }

    public Rubrica(String idRubrica) {
        this.setID(idRubrica);
        this.setValor(0.0);
    }

    public Rubrica(String idRubrica, Contribuinte Cnt) {
        this.setID(idRubrica);
        this.setValor(0.0);
        this.setContrib(Cnt);
    }

    public Rubrica(String idRubrica, String descr) {
        this.setID(idRubrica);
        this.setValor(0.0);
        this.desc = descr;
    }

    public Rubrica(String idRubrica, Contribuinte Cnt, String descr) {
        this.setID(idRubrica);
        this.setValor(0.0);
        this.setContrib(Cnt);
        this.desc = descr;
    }

    public void setValor(double val) {
        this.valor = val;
    }

    public double getValor() {
        return this.valor;
    }

    public void setID(String idRubrica) {
        this.id = idRubrica;
    }

    public String getID() {
        return this.id;
    }

    public void setContrib(Contribuinte cont) {
        this.contribuinte = cont;
    }

    public Contribuinte getContrib() {
        return this.contribuinte;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String descr) {
        this.desc = descr;
    }
}

