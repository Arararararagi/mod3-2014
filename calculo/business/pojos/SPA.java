/*
 * Decompiled with CFR 0_102.
 */
package calculo.business.pojos;

import calculo.business.pojos.Contribuinte;

public class SPA
extends Contribuinte {
    private int anosim;
    private char reside;
    private char estadoCivil;
    private int nDepNDef;
    private int nDepDef;
    private int nAscNDef;
    private int nAscDef;
    private int nDep03;
    private int anosim2;
    private char obitoConjuge;
    private char englobamentoE;
    private char juntaDocs;
    private char englobamentoG;
    private String servFin;
    private int nAfilhados;
    private char englobamentoImvG;
    private char englobamentoF;
    private boolean opcaoEnglobCatF;
    private int nDepGCNDef;
    private int nDepGCDef;
    private int nDepGC03;

    public SPA() {
        this.setIdCont("SPA");
        this.anosim = 14;
        this.reside = 49;
        this.estadoCivil = 49;
        this.nDepNDef = 0;
        this.nDepDef = 0;
        this.nAscNDef = 0;
        this.nAscDef = 0;
        this.anosim2 = 0;
        this.nDep03 = 0;
        this.obitoConjuge = 45;
        this.englobamentoE = 49;
        this.juntaDocs = 49;
        this.englobamentoG = 49;
        this.englobamentoF = 48;
        this.opcaoEnglobCatF = false;
        this.servFin = "";
        this.nAfilhados = 0;
        this.nDepGCNDef = 0;
        this.nDepGCDef = 0;
        this.nDepGC03 = 0;
    }

    public void setAnoSim(int eAno) {
        this.anosim = eAno;
    }

    public void setAnoSimHt(Object eAno) {
        Integer c = (Integer)eAno;
        this.anosim = c;
    }

    public int getAnoSim() {
        return this.anosim;
    }

    public void setReside(String local) {
        this.reside = local.charAt(0);
    }

    public void setResideHt(Object local) {
        Character c = (Character)local;
        this.reside = c.charValue();
    }

    public char getReside() {
        return this.reside;
    }

    public void setEstadoCivil(String eCivil) {
        this.estadoCivil = eCivil.charAt(0);
    }

    public void setEstadoCivilHt(Object eCivil) {
        Character c = (Character)eCivil;
        this.estadoCivil = c.charValue();
    }

    public char getEstadoCivil() {
        return this.estadoCivil;
    }

    public void setNumAscNDef(String numAscNDef) {
        this.nAscNDef = Integer.parseInt(numAscNDef);
    }

    public void setNumAscNDefHt(Object numAscNDef) {
        Integer naNDef = (Integer)numAscNDef;
        this.nAscNDef = naNDef;
    }

    public int getNumAscNDef() {
        return this.nAscNDef;
    }

    public void setNumAscDef(String numAscDef) {
        this.nAscDef = Integer.parseInt(numAscDef);
    }

    public void setNumAscDefHt(Object numAscDef) {
        Integer naDef = (Integer)numAscDef;
        this.nAscDef = naDef;
    }

    public int getNumAscDef() {
        return this.nAscDef;
    }

    public void setNumDep03(String numDep03) {
        this.nDep03 = Integer.parseInt(numDep03);
    }

    public void setNumDep03Ht(Object numDep03) {
        Integer nd03 = (Integer)numDep03;
        this.nDep03 = nd03;
    }

    public int getNumDep03() {
        return this.nDep03;
    }

    public void setAnoSim2(String eAno) {
        this.anosim2 = Integer.parseInt(eAno);
    }

    public void setAnoSimHt2(Object eAno) {
        Integer c = (Integer)eAno;
        this.anosim2 = c;
    }

    public int getAnoSim2() {
        return this.anosim2;
    }

    public void setNumDepNDef(String numDepNDef) {
        this.nDepNDef = Integer.parseInt(numDepNDef);
    }

    public void setNumDepNDefHt(Object numDepNDef) {
        Integer ndnd = (Integer)numDepNDef;
        this.nDepNDef = ndnd;
    }

    public int getNumDepNDef() {
        return this.nDepNDef;
    }

    public void setNumDepDef(String numDepDef) {
        this.nDepDef = Integer.parseInt(numDepDef);
    }

    public void setNumDepDefHt(Object numDepDef) {
        Integer ndd = (Integer)numDepDef;
        this.nDepDef = ndd;
    }

    public int getNumDepDef() {
        return this.nDepDef;
    }

    public void setObito(String obitoConjug) {
        this.obitoConjuge = obitoConjug.charAt(0);
    }

    public void setObitoHt(Object obitoConjug) {
        Character c = (Character)obitoConjug;
        this.obitoConjuge = c.charValue();
    }

    public char getObito() {
        return this.obitoConjuge;
    }

    public void setEnglobEHt(Object englobE) {
        Character c = (Character)englobE;
        this.englobamentoE = c.charValue();
    }

    public void setEnglobE(String englobE) {
        this.englobamentoE = englobE.charAt(0);
    }

    public char getEnglobE() {
        return this.englobamentoE;
    }

    public void setDocsE(String juntDocs) {
        this.juntaDocs = juntDocs.charAt(0);
    }

    public void setDocsEHt(Object juntDocs) {
        Character c = (Character)juntDocs;
        this.juntaDocs = c.charValue();
    }

    public char getDocsE() {
        return this.juntaDocs;
    }

    public void setEnglobG(String englobG) {
        this.englobamentoG = englobG.charAt(0);
    }

    public void setEnglobGHt(Object englobG) {
        Character c = (Character)englobG;
        this.englobamentoG = c.charValue();
    }

    public char getEnglobG() {
        return this.englobamentoG;
    }

    public void setEnglobF(String englobF) {
        this.englobamentoF = englobF.charAt(0);
    }

    public void setOpcaoEnglobCatF(boolean englobF2) {
        this.opcaoEnglobCatF = englobF2;
    }

    public void setEnglobFHt(Object englobF) {
        Character c = (Character)englobF;
        this.englobamentoF = c.charValue();
    }

    public void setOpcaoEnglobCatFHt(Object englobF2) {
        boolean c;
        this.opcaoEnglobCatF = englobF2 != null ? (c = ((Boolean)englobF2).booleanValue()) : false;
    }

    public void setOpcaoEnglobCatFHt(boolean englobF2) {
        this.opcaoEnglobCatF = englobF2;
    }

    public char getEnglobF() {
        return this.englobamentoF;
    }

    public boolean getOpcaoEnglobCatF() {
        return this.opcaoEnglobCatF;
    }

    public void setEnglobImvG(String englobImvG) {
        this.englobamentoImvG = englobImvG.charAt(0);
    }

    public void setEnglobImvGHt(Object englobImvG) {
        Character c = (Character)englobImvG;
        this.englobamentoImvG = c.charValue();
    }

    public char getEnglobImvG() {
        return this.englobamentoImvG;
    }

    public void setServFinHt(Object servFin) {
        String nServFin;
        this.servFin = nServFin = (String)servFin;
    }

    public void setServFin(String servFin) {
        this.servFin = servFin;
    }

    public String getServFin() {
        return this.servFin;
    }

    public boolean isFamiliaCasado() {
        if (this.estadoCivil == '1' || this.estadoCivil == '4') {
            return true;
        }
        return false;
    }

    public boolean isFamiliaMonoParental() {
        if (this.estadoCivil == '2' && (this.nDepNDef > 0 || this.nDepDef > 0 || this.nDepGCNDef > 0 || this.nDepGCDef > 0)) {
            return true;
        }
        return false;
    }

    public boolean isObito() {
        if (this.obitoConjuge == '1') {
            return true;
        }
        return false;
    }

    public String getImpSPA() {
        String resi = "";
        switch (this.getReside()) {
            case '1': {
                resi = "&nbsp;Continente";
                break;
            }
            case '2': {
                resi = "&nbsp;Regi&atilde;o Aut. dos A&ccedil;ores";
                break;
            }
            case '3': {
                resi = "&nbsp;Regi&atilde;o Aut. da Madeira";
                break;
            }
            case '4': {
                resi = "&nbsp;N&atilde;o Residente";
            }
        }
        String imp = "<tr><td>&nbsp;Resid&ecirc;ncia fiscal</td><td>" + resi + "</td></tr>\n";
        String ec = "";
        switch (this.getEstadoCivil()) {
            case '1': {
                ec = "&nbsp;Casado";
                break;
            }
            case '2': {
                ec = "&nbsp;Solteiro, vi&uacute;vo, divorciado";
                break;
            }
            case '3': {
                ec = "&nbsp;Separado de facto";
                break;
            }
            case '4': {
                ec = "&nbsp;Unido de facto";
            }
        }
        imp = String.valueOf(imp) + "<tr><td>&nbsp;Estado civil</td><td>" + ec + "</td></tr>\n";
        imp = String.valueOf(imp) + "<tr><td>&nbsp;N&ordm; depend. n&atilde;o deficientes</td><td>&nbsp;" + this.getNumDepNDef() + "</td></tr>\n";
        imp = String.valueOf(imp) + "<tr><td>&nbsp;N&ordm; depend. deficientes</td><td>&nbsp;" + this.getNumDepDef() + "</td></tr>\n";
        imp = String.valueOf(imp) + "<tr><td>&nbsp;N&ordm; ascend. n&atilde;o deficientes</td><td>&nbsp;" + this.getNumAscNDef() + "</td></tr>\n";
        imp = String.valueOf(imp) + "<tr><td>&nbsp;N&ordm; ascend. deficientes</td><td>&nbsp;" + this.getNumAscDef() + "</td></tr>\n";
        imp = String.valueOf(imp) + "<tr><td>&nbsp;N&ordm; afilhados civis</td><td>&nbsp;" + this.getNumAfilhados() + "</td></tr>\n";
        imp = String.valueOf(imp) + "<tr><td>&nbsp;N&ordm; depend. com idade igual ou inferior a 3 anos</td><td>&nbsp;" + this.getNumDep03() + "</td></tr>\n";
        imp = String.valueOf(imp) + "<tr><td>&nbsp;N&ordm; depend. n&atilde;o deficientes em guarda conjunta</td><td>&nbsp;" + this.getNumDepGCNDef() + "</td></tr>\n";
        imp = String.valueOf(imp) + "<tr><td>&nbsp;N&ordm; depend. deficientes em guarda conjunta</td><td>&nbsp;" + this.getNumDepGCDef() + "</td></tr>\n";
        imp = String.valueOf(imp) + "<tr><td>&nbsp;N&ordm; depend. em guarda conjunta com idade igual ou inferior a 3 anos</td><td>&nbsp;" + this.getNumDepGC03() + "</td></tr>\n";
        if (this.getObito() == '1') {
            imp = String.valueOf(imp) + "<tr><td>&nbsp;&Oacute;bito do c&ocirc;njuge?</td><td>&nbsp;Sim</td></tr>\n";
        }
        return imp;
    }

    public String getAno() {
        String ano = "";
        switch (this.getAnoSim()) {
            case 49: {
                ano = "&nbsp;2007";
                break;
            }
            case 50: {
                ano = "&nbsp;2006";
                break;
            }
            case 51: {
                ano = "&nbsp;2005";
                break;
            }
            case 52: {
                ano = "&nbsp;2004";
                break;
            }
            case 53: {
                ano = "&nbsp;2003";
                break;
            }
            case 56: {
                ano = "&nbsp;2008";
                break;
            }
            case 57: {
                ano = "&nbsp;2009";
            }
        }
        String imp = "<tr><td>&nbsp;Ano da Simula&ccedil;&atilde;o</td><td>" + ano + "</td></tr>\n";
        return imp;
    }

    public String getImpOpcoesCatE() {
        String opcaoE = this.getEnglobE() == '0' ? "N&atilde;o opta pelo englobamento" : "Opta pelo englobamento";
        String opcaoJ = this.getDocsE() == '0' ? "n&atilde;o junta os documentos" : "junta os documentos";
        String imp = "(*) " + opcaoE + "; " + opcaoJ;
        return imp;
    }

    public String getImpOpcoesCatG() {
        String opcaoE = this.getEnglobG() == '0' ? "N&atilde;o opta pelo englobamento" : "Opta pelo englobamento";
        String imp = "(*) " + opcaoE;
        return imp;
    }

    public void setNumAfilhados(String numAfilhados) {
        this.nAfilhados = Integer.parseInt(numAfilhados);
    }

    public void setNumAfilhadosHt(Object numAfilhados) {
        Integer ndd = (Integer)numAfilhados;
        this.nAfilhados = ndd;
    }

    public int getNumAfilhados() {
        return this.nAfilhados;
    }

    public void setNumDepGCNDef(String nDepGCNDef) {
        this.nDepGCNDef = nDepGCNDef != null ? Integer.parseInt(nDepGCNDef) : 0;
    }

    public void setNumDepGCNDefHt(Object nDepGCNDef) {
        if (nDepGCNDef != null) {
            Integer ndd = (Integer)nDepGCNDef;
            this.nDepGCNDef = ndd;
        } else {
            this.nDepGCDef = 0;
        }
    }

    public int getNumDepGCNDef() {
        return this.nDepGCNDef;
    }

    public void setNumDepGCDef(String nDepGCDef) {
        this.nDepGCDef = nDepGCDef != null && !"".equals(nDepGCDef) ? Integer.parseInt(nDepGCDef) : 0;
    }

    public void setNumDepGCDefHt(Object nDepGCDef) {
        if (nDepGCDef != null) {
            Integer ndd = (Integer)nDepGCDef;
            this.nDepGCDef = ndd;
        } else {
            this.nDepGCDef = 0;
        }
    }

    public int getNumDepGCDef() {
        return this.nDepGCDef;
    }

    public void setNumDepGC03(String nDepGC03) {
        this.nDepGC03 = nDepGC03 != null && !"".equals(nDepGC03) ? Integer.parseInt(nDepGC03) : 0;
    }

    public void setNumDepGC03Ht(Object nDepGC03) {
        if (nDepGC03 != null) {
            Integer ndd = (Integer)nDepGC03;
            this.nDepGC03 = ndd;
        } else {
            this.nDepGC03 = 0;
        }
    }

    public int getNumDepGC03() {
        return this.nDepGC03;
    }
}

