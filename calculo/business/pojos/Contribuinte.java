/*
 * Decompiled with CFR 0_102.
 */
package calculo.business.pojos;

public class Contribuinte {
    private String idCont;
    private String numContrib;
    private char grauDef;
    private String grauDefaux;
    private boolean defFArm;
    private boolean comRendim;
    private int idade;
    private char enquadramB;
    private char naturRendB;
    private char tipoRendB;
    private char tipoRendC;
    private char tipoRendD;
    private boolean guardaConjunta;

    public Contribuinte() {
        this.idCont = "";
        this.numContrib = "";
        this.grauDef = 45;
        this.grauDefaux = "0";
        this.defFArm = false;
        this.comRendim = false;
        this.idade = 0;
        this.enquadramB = 49;
        this.naturRendB = 49;
        this.tipoRendB = 50;
        this.tipoRendC = 50;
        this.tipoRendD = 50;
        this.guardaConjunta = false;
    }

    public Contribuinte(String idContrib) {
        this.idCont = idContrib;
        this.numContrib = "";
        this.grauDef = 45;
        this.grauDefaux = "0";
        this.defFArm = false;
        this.comRendim = false;
        this.idade = 0;
        this.enquadramB = 49;
        this.naturRendB = 49;
        this.tipoRendB = 50;
        this.tipoRendC = 50;
        this.tipoRendD = 50;
        this.guardaConjunta = false;
    }

    public void setIdCont(String tipCont) {
        this.idCont = tipCont;
    }

    public void setIdContHt(Object tipCont) {
        this.idCont = (String)tipCont;
    }

    public String getIdCont() {
        return this.idCont;
    }

    public void setnumContrib(String nif) {
        this.numContrib = nif;
    }

    public String getnumContrib() {
        return this.numContrib;
    }

    public void setGrauDef(String graudef) {
        this.grauDef = graudef.charAt(0);
    }

    public void setGrauDefaux(String graudef) {
        this.grauDefaux = graudef;
    }

    public void setGrauDefauxHt(Object graudef) {
        String aux;
        this.grauDefaux = aux = (String)graudef;
    }

    public void setGrauDefHt(Object graudefic) {
        Character c = (Character)graudefic;
        this.grauDef = c.charValue();
    }

    public char getGrauDef() {
        return this.grauDef;
    }

    public String getGrauDefaux() {
        return this.grauDefaux;
    }

    public void setenquadramB(char nenqB) {
        this.enquadramB = nenqB;
    }

    public void setnaturRendB(String nRendB) {
        this.naturRendB = nRendB.charAt(0);
    }

    public void settipoRendB(String tRendB) {
        this.tipoRendB = tRendB.charAt(0);
    }

    public void settipoRendBHt(Object tRendB) {
        Character c = (Character)tRendB;
        this.tipoRendB = c.charValue();
    }

    public void setenquadramBHt(Object nenqB) {
        Character c = (Character)nenqB;
        this.enquadramB = c.charValue();
    }

    public void setnaturRendBHt(Object nRendB) {
        Character c = (Character)nRendB;
        this.naturRendB = c.charValue();
    }

    public char getenquadramB() {
        return this.enquadramB;
    }

    public char getnaturRendB() {
        return this.naturRendB;
    }

    public char gettipoRendB() {
        return this.tipoRendB;
    }

    public void settipoRendC(String tRendC) {
        this.tipoRendC = tRendC.charAt(0);
    }

    public void settipoRendCHt(Object tRendC) {
        Character c = (Character)tRendC;
        this.tipoRendC = c.charValue();
    }

    public char gettipoRendC() {
        return this.tipoRendC;
    }

    public void settipoRendD(String tRendD) {
        this.tipoRendD = tRendD.charAt(0);
    }

    public void settipoRendDHt(Object tRendD) {
        Character c = (Character)tRendD;
        this.tipoRendD = c.charValue();
    }

    public char gettipoRendD() {
        return this.tipoRendD;
    }

    public String getDescEnqB() {
        String res = "";
        if (this.isComRendim()) {
            if (this.enquadramB == '1') {
                res = "R. simplificado";
            }
            if (this.enquadramB == '2') {
                res = "Acto isolado";
            }
            if (this.enquadramB == '3') {
                res = "R. acess&oacute;rios";
            }
        } else {
            res = "&nbsp;";
        }
        return res;
    }

    public String getDescNaturB() {
        String res = "";
        res = this.isComRendim() ? (this.naturRendB == '1' ? "R. simplificado" : "Acto isolado") : "&nbsp;";
        return res;
    }

    public String getDescRendB() {
        String res = "";
        res = this.isComRendim() ? (this.tipoRendB == '2' ? "Profissional" : "Agr&iacute;cola") : "&nbsp;";
        return res;
    }

    public String getDescRendC() {
        String res = "";
        res = this.isComRendim() ? (this.tipoRendC == '2' ? "Profissional" : "Agr&iacute;cola") : "&nbsp;";
        return res;
    }

    public String getDescRendD() {
        String res = "";
        res = this.isComRendim() ? (this.tipoRendD == '2' ? "Profissional" : "Agr&iacute;cola") : "&nbsp;";
        return res;
    }

    public void setDefFArm(boolean def) {
        this.defFArm = def;
    }

    public void setDefFArm(String def) {
        this.defFArm = def.equals("true");
    }

    public void setDefFArmHt(Object def) {
        Boolean b = (Boolean)def;
        this.defFArm = b;
    }

    public boolean isDefFArm() {
        return this.defFArm;
    }

    public void setGuardaConjunta(boolean guardaConjunta) {
        this.guardaConjunta = guardaConjunta;
    }

    public void setGuardaConjunta(String guardaConjunta) {
        this.guardaConjunta = guardaConjunta.equals("true");
    }

    public void setGuardaConjuntaHt(Object guardaConjunta) {
        if (guardaConjunta != null) {
            Boolean b = (Boolean)guardaConjunta;
            this.guardaConjunta = b;
        } else {
            this.guardaConjunta = false;
        }
    }

    public boolean isGuardaConjunta() {
        return this.guardaConjunta;
    }

    public void setComRendim(boolean rendim) {
        this.comRendim = rendim;
    }

    public void setComRendim(String rendim) {
        this.comRendim = rendim.equals("true");
    }

    public void setComRendimHt(Object rendim) {
        Boolean b = (Boolean)rendim;
        this.comRendim = b;
    }

    public boolean isComRendim() {
        return this.comRendim;
    }

    public boolean isDeficiente() {
        if (this.grauDef == '0' || this.grauDef == '-') {
            return false;
        }
        return true;
    }

    public void setIdade(Object idade) {
        String i = (String)idade;
        idade = Integer.parseInt(i);
    }

    public void setIdade(String nIdade) {
        this.idade = nIdade.equals("") ? 0 : Integer.parseInt(nIdade);
    }

    public void setIdadeHt(Object nIdade) {
        if (nIdade != null) {
            Integer idad = (Integer)nIdade;
            this.idade = idad;
        } else {
            this.idade = 0;
        }
    }

    public int getIdade() {
        return this.idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public void initRendimentos() {
        this.grauDef = 45;
        this.defFArm = false;
        this.comRendim = false;
    }

    public String getImpContribuinte() {
        String imp = "";
        String dfa = "";
        if (this.isDefFArm()) {
            dfa = " das F.A.";
        }
        if (this.isComRendim()) {
            String gd = "";
            switch (this.getGrauDef()) {
                case '0': {
                    gd = "&nbsp;N&atilde;o deficiente" + dfa;
                    break;
                }
                case '1': {
                    gd = "&nbsp;Deficiente - 60 a 89%" + dfa;
                    break;
                }
                case '2': {
                    gd = "&nbsp;Deficiente - Superior a 89%" + dfa;
                }
            }
            String id = "";
            if (this.getIdCont().equals("SPA")) {
                id = "&nbsp;Sujeito passivo A";
            }
            if (this.getIdCont().equals("SPB")) {
                id = "&nbsp;Sujeito passivo B";
            }
            if (this.getIdCont().equals("D1")) {
                id = "&nbsp;Dependente 1";
            }
            if (this.getIdCont().equals("D2")) {
                id = "&nbsp;Dependente 2";
            }
            if (this.getIdCont().equals("D3")) {
                id = "&nbsp;Dependente 3";
            }
            if (this.getIdCont().equals("SPF")) {
                id = "&nbsp;Sujeito falecido";
            }
            imp = "<tr><td>" + id + "</td><td>" + gd + "</td></tr>\n";
        }
        return imp;
    }
}

