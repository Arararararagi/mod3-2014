/*
 * Decompiled with CFR 0_102.
 */
package calculo.business.pojos;

import calculo.business.pojos.Rubrica;
import calculo.common.constants.Constantes;
import calculo.common.util.Utils;

public class RubricaMaisValia
extends Rubrica {
    final int mesCoefDesv = 24;
    private String dataAquisicao;
    private String dataRealizacao;
    private double valAquisicao;
    private double valRealizacao;
    private double valEncargos;
    private String dataAquisicaoRec;
    private String dataRealizacaoRec;
    private double valAquisicaoRec;
    private double valRealizacaoRec;
    private double valEncargosRec;

    public RubricaMaisValia() {
    }

    public RubricaMaisValia(String idinput, String desc) {
        super.setID(idinput);
        super.setDesc(desc);
        this.dataAquisicao = "";
        this.dataRealizacao = "";
        this.valAquisicao = 0.0;
        this.valRealizacao = 0.0;
        this.valEncargos = 0.0;
        this.dataAquisicaoRec = "";
        this.dataRealizacaoRec = "";
        this.valAquisicaoRec = 0.0;
        this.valRealizacaoRec = 0.0;
        this.valEncargosRec = 0.0;
    }

    public void setDataAquisicao(String data) {
        this.dataAquisicao = data;
    }

    public void setDataRealizacao(String data) {
        this.dataRealizacao = data;
    }

    public void setValAquisicao(double val) {
        this.valAquisicao = val;
    }

    public void setValRealizacao(double val) {
        this.valRealizacao = val;
    }

    public void setValEncargos(double val) {
        this.valEncargos = val;
    }

    public String getDataAquisicao() {
        return this.dataAquisicao;
    }

    public String getDataRealizacao() {
        return this.dataRealizacao;
    }

    public double getValAquisicao() {
        return this.valAquisicao;
    }

    public double getValRealizacao() {
        return this.valRealizacao;
    }

    public double getValEncargos() {
        return this.valEncargos;
    }

    public String getDataAquisicaoRec() {
        return this.dataAquisicaoRec;
    }

    public void setDataAquisicaoRec(String dataAquisicaoRec) {
        this.dataAquisicaoRec = dataAquisicaoRec;
    }

    public String getDataRealizacaoRec() {
        return this.dataRealizacaoRec;
    }

    public void setDataRealizacaoRec(String dataRealizacaoRec) {
        this.dataRealizacaoRec = dataRealizacaoRec;
    }

    public double getValAquisicaoRec() {
        return this.valAquisicaoRec;
    }

    public void setValAquisicaoRec(double valAquisicaoRec) {
        this.valAquisicaoRec = valAquisicaoRec;
    }

    public double getValEncargosRec() {
        return this.valEncargosRec;
    }

    public void setValEncargosRec(double valEncargosRec) {
        this.valEncargosRec = valEncargosRec;
    }

    public double getValRealizacaoRec() {
        return this.valRealizacaoRec;
    }

    public void setValRealizacaoRec(double valRealizacaoRec) {
        this.valRealizacaoRec = valRealizacaoRec;
    }

    public double difMeses() {
        return Utils.DifMesDatas(this.dataRealizacao, this.dataAquisicao) / 30.0;
    }

    public int getAnoAquisicao() {
        return Integer.parseInt(this.dataAquisicao.substring(0, 4));
    }

    public int getAnoRealizacao() {
        return Integer.parseInt(this.dataRealizacao.substring(0, 4));
    }

    public boolean isPreenchida() {
        if (this.dataAquisicao.equals("")) {
            return false;
        }
        return true;
    }

    public double getCoefDesv(int ano) {
        double cd = 0.0;
        if (this.difMeses() > 24.0) {
            Constantes cons = new Constantes();
            cd = cons.getCoefDesv(this.getAnoAquisicao(), ano);
            cons = null;
        } else {
            cd = 1.0;
        }
        return cd;
    }

    public double getMaisValiaBemImovel(int ano) {
        return this.getValRealizacao() - this.getValAquisicao() * this.getCoefDesv(ano) - this.getValEncargos();
    }

    public double difMesesRec() {
        return Utils.DifMesDatas(this.dataRealizacaoRec, this.dataAquisicaoRec) / 30.0;
    }

    public int getAnoAquisicaoRec() {
        return Integer.parseInt(this.dataAquisicaoRec.substring(0, 4));
    }

    public int getAnoRealizacaoRec() {
        return Integer.parseInt(this.dataRealizacaoRec.substring(0, 4));
    }

    public boolean isPreenchidaRec() {
        if (this.dataAquisicaoRec.equals("")) {
            return false;
        }
        return true;
    }

    public double getCoefDesvRec(int ano) {
        double cd = 0.0;
        if (this.difMesesRec() > 24.0) {
            Constantes cons = new Constantes();
            cd = cons.getCoefDesv(this.getAnoAquisicaoRec(), ano);
            cons = null;
        } else {
            cd = 1.0;
        }
        return cd;
    }

    public double getMaisValiaBemImovelRec(int ano) {
        return this.getValRealizacaoRec() - this.getValAquisicaoRec() * this.getCoefDesvRec(ano) - this.getValEncargosRec();
    }
}

