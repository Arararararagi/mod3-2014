/*
 * Decompiled with CFR 0_102.
 */
package calculo.business.logic;

import calculo.business.pojos.Contribuinte;
import java.util.Map;

public interface CalcInt {
    public double doSimulacao();

    public void defineConstantes(Map var1);

    public double calcRendLiqCatA();

    public double calcRendBrutoCatA(Contribuinte var1);

    public double calcDeducEspecifica(Contribuinte var1);

    public double calcDeducao1Especifica(Contribuinte var1);

    public double calcDeducaoQuotizaA(Contribuinte var1);

    public double calcCSS(Contribuinte var1);

    public double calcQDFP(Contribuinte var1);

    public double calcRendLiqCatH();

    public double calcRendBrutoCatH(Contribuinte var1);

    public double calcDeducEspecificaH(Contribuinte var1);

    public double calcDeducao1EspecificaH(Contribuinte var1);

    public double calcDeducaoQuotizaH(Contribuinte var1);

    public double calcRendLiqCatE();

    public double calcRendDeclaradosE();

    public double calcRendLiqCatF();

    public double calcRendLiqCatG();

    public double calcMaisValBensImoveis();

    public double calcMaisValPropIntelect();

    public double calcMaisValPosContratua();

    public double calcMaisValPartesSociais();

    public double calcIncremPatrimon();

    public double calcIncremPatrimonRF();

    public double calcRBLimAgr();

    public char calcEnquadramento(Contribuinte var1);

    public double calcRBAnexoB(Contribuinte var1);

    public double calcRBAnexoProC(Contribuinte var1);

    public double calcRBAnexoAgrC(Contribuinte var1);

    public double calcRBAnexoProD(Contribuinte var1);

    public double calcRBAnexoAgrD(Contribuinte var1);

    public double calcRendLiqCatB();

    public double calcRendLiqAnexoBAgr(Contribuinte var1);

    public double calcRendLiqAnexoBPro(Contribuinte var1);

    public double calcRendLiqAnexoCAgr(Contribuinte var1);

    public double calcRendLiqAnexoCPro(Contribuinte var1);

    public double calcRendLiqAnexoDAgr(Contribuinte var1);

    public double calcRendLiqAnexoDPro(Contribuinte var1);

    public double calcRLiqActivFinanc(double var1);

    public double calcRBActivFinanc(Contribuinte var1);

    public double calcColActivFinanc(double var1, double var3, double var5);

    public double calcAcrescRendLiq();

    public double calcAbatimentos();

    public double calcRendIsentSujEng();

    public int calcCoefConjugal();

    public double calcRCC(double var1, double var3);

    public double calcTaxaImposto(double var1);

    public double calcParcelaAbat(double var1);

    public double calculoDeducoesColecta(double var1, double var3, double var5, double var7, double var9, double var11);

    public double calcDedSPDA();

    public double calcDedSujeitosPassivos();

    public double calcDedDependentes();

    public double calcDedAscendentes();

    public double calcDespSaude5();

    public double calcDespSaudeOutras();

    public double calcDedEducFormProf();

    public double calcDedEncLares();

    public double calcDedEducLares(double var1, double var3);

    public double calcDedEncImoveis();

    public double calcDedEnergiasRenov();

    public double calcMaiorDed();

    public double calcDedSegVida();

    public double calcDedSeguroSaude();

    public double calcDedSPDef();

    public double calcDedSujeitosPassivosDef();

    public double calcDedDependentesDef();

    public double calcDedAscendentesDef();

    public double calcAconselhamento();

    public double calcDedPPRPPE(double var1, double var3, double var5, double var7);

    public double calcValDedPPR(String var1, int var2);

    public double calcValTRB(Contribuinte var1);

    public double calcPPHCooperativas();

    public double calcDeducaoPPA();

    public double calcDedDespEducReabDef();

    public double calcDedPremiosSegurDef(double var1);

    public double calcDedComputadores(double var1);

    public double calcDedCooperadores();

    public double calcDedDonatPublic();

    public double calcDedDonativOutr(double var1);

    public double calcIvaAquiServicos();

    public char verificaCasado();

    public double calcColectaMinExistencia(double var1, double var3, double var5, int var7, double var8, double var10, double var12, double var14, double var16);

    public double calcAcrescColecta();

    public double calculaR1();

    public double calculaR2();

    public double calcRendimGratifica();

    public double calcColFuturos();

    public double calcColMaisValNEng();

    public double calcColPPA();

    public double calcResgateFPR();

    public double calcRetencoesFonte();

    public double calcRetencoesCatA(Contribuinte var1);

    public double calcRetencoesCatH(Contribuinte var1);

    public double calcRetencoesCatB(Contribuinte var1);

    public double calcPagamConta();

    public double calcPCB(Contribuinte var1);

    public double getValorMaisVal();

    public double getValRendimGlobal();

    public double getValDedEspecificas();

    public double getValPerdasRecup();

    public double getValAbatimentos();

    public double getValRendimDetTaxas();

    public double getValRendimColectavel();

    public double getValRendimIsentos();

    public double getValCoefConjugal();

    public double getValTaxaImposto();

    public double getValImportApurada();

    public double getValParcelaAbater();

    public double getValorColRendIsent();

    public double getValorApurado();

    public double getValImpTribAutonoma();

    public double getValColectaTotal();

    public double getValDeduColecta();

    public double getValAcrescColecta();

    public double getValColectaLiquida();

    public double getValPagamentoConta();

    public double getValRetencoesFonte();

    public double getValImpostoApurado();

    public boolean getMinExistencia();

    public double getImpostoPago();

    public double getImpostoRecebido();

    public double getValQuoRendAnter();

    public double getValImpostAnosAnt();

    public double getValBenFiscalSF();

    public double getValStRendimento();

    public double getValStDeducoes();

    public double getValStColecta();

    public double getValStRetencoes();

    public double getValStRendimentoXTaxa();

    public double getValTaxaAdicional();
}

