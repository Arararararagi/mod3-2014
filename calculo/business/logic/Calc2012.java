/*
 * Decompiled with CFR 0_102.
 */
package calculo.business.logic;

import calculo.business.Modelo3;
import calculo.business.logic.Calc2011;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.SPA;

public class Calc2012
extends Calc2011 {
    public final String[][] servFinArray = new String[][]{{"0027", "0.05"}, {"0035", "0.050"}, {"0043", "0.050"}, {"0051", "0.045"}, {"0078", "0.0495"}, {"0086", "0.050"}, {"0094", "0.050"}, {"0108", "0.050"}, {"0116", "0.030"}, {"0132", "0.050"}, {"0140", "0.050"}, {"0167", "0.050"}, {"0191", "0.050"}, {"0221", "0.050"}, {"0248", "0.050"}, {"0256", "0.050"}, {"0264", "0.050"}, {"0299", "0.050"}, {"0302", "0.025"}, {"0310", "0.050"}, {"0329", "0.050"}, {"0337", "0.050"}, {"0353", "0.050"}, {"0370", "0.050"}, {"0400", "0.030"}, {"0418", "0.050"}, {"0426", "0.050"}, {"0434", "0.050"}, {"0450", "0.050"}, {"0477", "0.050"}, {"0485", "0.050"}, {"0493", "0.050"}, {"0507", "0.050"}, {"0515", "0.050"}, {"0523", "0.050"}, {"0531", "0.050"}, {"0558", "0.050"}, {"0566", "0.020"}, {"0574", "0.050"}, {"0582", "0.025"}, {"0590", "0.025"}, {"0612", "0.050"}, {"0620", "0.050"}, {"0639", "0.050"}, {"0655", "0.050"}, {"0663", "0.050"}, {"0680", "0.025"}, {"0698", "0.050"}, {"0710", "0.050"}, {"0736", "0.050"}, {"0744", "0.050"}, {"0752", "0.025"}, {"0760", "0.050"}, {"0779", "0.050"}, {"0795", "0.050"}, {"0817", "0.050"}, {"0825", "0.050"}, {"0841", "0.050"}, {"0850", "0.050"}, {"0868", "0.050"}, {"0884", "0.050"}, {"0922", "0.050"}, {"0930", "0.050"}, {"0965", "0.050"}, {"0981", "0.050"}, {"0990", "0.040"}, {"1007", "0.050"}, {"1023", "0.050"}, {"1031", "0.050"}, {"1040", "0.050"}, {"1058", "0.050"}, {"1074", "0.050"}, {"1082", "0.050"}, {"1104", "0.050"}, {"1112", "0.050"}, {"1120", "0.050"}, {"1139", "0.050"}, {"1155", "0.050"}, {"1198", "0.020"}, {"1210", "0.050"}, {"1260", "0.050"}, {"1279", "0.050"}, {"1287", "0.030"}, {"1295", "0.050"}, {"1309", "0.050"}, {"1317", "0.050"}, {"1333", "0.050"}, {"1350", "0.025"}, {"1384", "0.050"}, {"1406", "0.020"}, {"1414", "0.010"}, {"1430", "0.050"}, {"1449", "0.050"}, {"1457", "0.050"}, {"1465", "0.050"}, {"1473", "0.050"}, {"1503", "0.0375"}, {"1520", "0.050"}, {"1538", "0.050"}, {"1546", "0.050"}, {"1554", "0.050"}, {"1562", "0.050"}, {"1570", "0.050"}, {"1589", "0.050"}, {"1600", "0.050"}, {"1627", "0.050"}, {"1635", "0.050"}, {"1643", "0.050"}, {"1660", "0.030"}, {"1678", "0.050"}, {"1716", "0.030"}, {"1724", "0.050"}, {"1732", "0.050"}, {"1775", "0.050"}, {"1805", "0.050"}, {"1813", "0.050"}, {"1821", "0.050"}, {"1830", "0.050"}, {"1856", "0.050"}, {"1872", "0.050"}, {"1899", "0.050"}, {"1929", "0.045"}, {"1937", "0.050"}, {"1945", "0.050"}, {"1953", "0.050"}, {"1961", "0.045"}, {"1970", "0.050"}, {"1988", "0.040"}, {"1996", "0.050"}, {"2003", "0.050"}, {"2011", "0.050"}, {"2020", "0.050"}, {"2038", "0.050"}, {"2046", "0.050"}, {"2070", "0.050"}, {"2089", "0.050"}, {"2097", "0.050"}, {"2119", "0.050"}, {"2127", "0.050"}, {"2135", "0.040"}, {"2143", "0.050"}, {"2151", "0.050"}, {"2160", "0.050"}, {"2178", "0.045"}, {"2208", "0.050"}, {"2216", "0.050"}, {"2232", "0.050"}, {"2259", "0.050"}, {"2275", "0.050"}, {"2283", "0.050"}, {"2305", "0.030"}, {"2313", "0.050"}, {"2330", "0.050"}, {"2356", "0.050"}, {"2372", "0.025"}, {"2380", "0.050"}, {"2399", "0.050"}, {"2402", "0.050"}, {"2437", "0.050"}, {"2470", "0.050"}, {"2496", "0.050"}, {"2500", "0.020"}, {"2518", "0.050"}, {"2534", "0.030"}, {"2542", "0.050"}, {"2550", "0.040"}, {"2577", "0.025"}, {"2585", "0.050"}, {"2593", "0.050"}, {"2607", "0.025"}, {"2615", "0.020"}, {"2623", "0.010"}, {"2631", "0.040"}, {"2674", "0.050"}, {"2682", "0.050"}, {"2704", "0.050"}, {"2739", "0.050"}, {"2755", "0.050"}, {"2810", "0.050"}, {"2836", "0.050"}, {"2860", "0.050"}, {"2933", "0.050"}, {"2992", "0.050"}, {"3026", "0.050"}, {"3042", "0.050"}, {"3069", "0.050"}, {"3085", "0.050"}, {"3107", "0.050"}, {"3131", "0.045"}, {"3140", "0.045"}, {"3158", "0.050"}, {"3166", "0.050"}, {"3174", "0.050"}, {"3182", "0.050"}, {"3190", "0.050"}, {"3212", "0.050"}, {"3239", "0.050"}, {"3247", "0.050"}, {"3255", "0.050"}, {"3263", "0.050"}, {"3301", "0.050"}, {"3328", "0.050"}, {"3336", "0.050"}, {"3344", "0.050"}, {"3360", "0.050"}, {"3387", "0.050"}, {"3409", "0.050"}, {"3417", "0.045"}, {"3433", "0.0375"}, {"3441", "0.050"}, {"3450", "0.050"}, {"3476", "0.050"}, {"3492", "0.050"}, {"3514", "0.050"}, {"3522", "0.050"}, {"3530", "0.050"}, {"3549", "0.050"}, {"3557", "0.050"}, {"3565", "0.050"}, {"3590", "0.050"}, {"3603", "0.050"}, {"3611", "0.045"}, {"3654", "0.050"}, {"3735", "0.050"}, {"3824", "0.050"}, {"3859", "0.050"}, {"4170", "0.050"}, {"4200", "0.045"}, {"4219", "0.050"}, {"4227", "0.050"}};

    public Calc2012(Modelo3 m3) {
        super(m3);
    }

    @Override
    public double doSimulacao() {
        try {
            this.rendLiquiCategA = this.calcRendLiqCatA();
            this.acrescimosRendLiq = this.calcAcrescRendLiq();
            this.rendimIsentSE = this.calcRendIsentSujEng();
            this.rendLiquiCategE = this.calcRendLiqCatE();
            this.rendLiquiCategB = this.calcRendLiqCatB();
            this.rendLiquiCategF = this.calcRendLiqCatF();
            this.rendLiquiCategG = this.calcRendLiqCatG();
            this.rendLiquiCategH = this.calcRendLiqCatH();
            this.quoRendAnosAnt = this.calcQuoRendAnosAnter();
            this.rendimLiquido = this.rendLiquiCategA + this.rendLiquiCategB + this.rendLiquiCategE + this.rendLiquiCategF + this.rendLiquiCategG + this.rendLiquiCategH;
            if (this.rendimLiquido < 0.0) {
                this.rendimLiquido = 0.0;
            }
            this.rendimLiquido+=this.acrescimosRendLiq;
            this.rendimLiqAposDedu = this.rendimLiquido - this.deducRendLiquido;
            this.abatimentos = 0.0;
            this.rendimColectavel = this.rendimLiqAposDedu - this.abatimentos < 0.0 ? 0.0 : this.rendimLiqAposDedu - this.abatimentos;
            this.rendDetermtx = this.rendimColectavel + this.rendimIsentSE - this.quoRendAnosAnt;
            this.coefConjugal = this.calcCoefConjugal();
            this.valorRCC = this.calcRCC(this.rendDetermtx, this.coefConjugal);
            this.txImposto = this.calcTaxaImposto(this.valorRCC);
            this.importApurada = this.rendDetermtx == 0.0 ? 0.0 : this.rendDetermtx / this.coefConjugal * this.txImposto;
            this.parcelaAbater = this.calcParcelaAbat(this.valorRCC);
            this.valorColecta = (this.importApurada - this.parcelaAbater) * this.coefConjugal;
            this.colectaAnosAnter = this.quoRendAnosAnt > 0.0 ? this.valorColecta * this.quoRendAnosAnt / this.rendDetermtx : 0.0;
            this.colectaTribAutonomas = this.calcColectaDespTA();
            this.colectaActivFinanc = this.calcColActivFinanc(this.valorColecta, this.rendimLiquido, this.rendLiquiCategB);
            this.colectaActivFinancCorrig = this.escalaoContribuinte == 1 ? this.colectaActivFinanc / this.taxaReducColAcores_Esc1 : (this.escalaoContribuinte == 2 ? this.colectaActivFinanc / this.taxaReducColAcores_Esc2 : this.colectaActivFinanc / this.taxaReducColAcores);
            if (this.colectaActivFinanc > 0.0 && ((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '2') {
                this.valorColecta = this.valorColecta - this.colectaActivFinanc + this.colectaActivFinancCorrig;
            }
            this.colectaRendimIsent = this.valorColecta * this.rendimIsentSE == 0.0 ? 0.0 : this.valorColecta * this.rendimIsentSE / this.rendDetermtx;
            this.colectaRendimSujeitos = this.valorColecta - this.colectaRendimIsent + this.colectaAnosAnter;
            this.colectaGratificacoes = this.calcRendimGratifica();
            this.colectaPPA = this.calcColPPA();
            this.colectaResgateFPR = this.calcResgateFPR();
            this.colectaFuturoOpcoes = this.calcColFuturos();
            this.valDeducoesColecta = this.calculoDeducoesColecta(this.colectaRendimSujeitos, this.rendimLiquido, this.rendimIsentSE, this.acrescimosRendLiq, this.colectaGratificacoes, this.colectaFuturoOpcoes);
            this.valorDeduzidoSF = this.colectaRendimSujeitos - this.valDeducoesColecta > 0.0 ? this.recolhaSF(this.colectaRendimSujeitos - this.valDeducoesColecta) : 0.0;
            this.valDeducoesColecta+=this.valorDeduzidoSF;
            if (this.valDeducoesColecta > this.colectaRendimSujeitos) {
                this.valDeducoesColecta = this.colectaRendimSujeitos;
            }
            this.colectaAposDeducoes = this.colectaRendimSujeitos - this.valDeducoesColecta < 0.0 ? 0.0 : this.colectaRendimSujeitos - this.valDeducoesColecta;
            this.colectaDesportistas = 0.0;
            this.colectaMinExistencia = this.calcColectaMinExistencia(this.rendimIsentSE, this.colectaAposDeducoes, this.rendDetermtx, new Double(this.coefConjugal).intValue(), this.rendimColectavel, this.colectaFuturoOpcoes, this.colectaGratificacoes, this.colectaDesportistas, this.acrescimosRendLiq);
            this.acrescimosColecta = this.calcAcrescColecta();
            this.colectaMaisValNEngl = this.calcColMaisValNEng();
            this.colectaTribAutCatF = this.calcColTribAutCatF();
            this.colectaTribAutCatG = this.calcColTribAutCatG();
            this.colectaAdicional = this.calcColectaAdicional();
            this.colectaDespConfiden = 0.0;
            this.colectaDespRepresen = 0.0;
            this.colectaEncarViatura = 0.0;
            this.colectaTotal = this.colectaMinExistencia + this.acrescimosColecta + this.colectaGratificacoes + this.colectaDesportistas + this.colectaTribAutonomas + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaPPA + this.colectaResgateFPR + this.colectaDespConfiden + this.colectaDespRepresen + this.colectaEncarViatura + this.colectaTribAutCatG + this.colectaTribAutCatF + this.colectaAdicional;
            if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
                double gratifAux1 = this.modelo3.getVal("RenTrabDep", "valgratific", "SPA") + this.modelo3.getVal("RenTrabDep", "valgratific", "SPB") + this.modelo3.getVal("RenTrabDep", "valgratific", "D1") + this.modelo3.getVal("RenTrabDep", "valgratific", "D2") + this.modelo3.getVal("RenTrabDep", "valgratific", "D3") + this.modelo3.getVal("RenTrabDep", "valgratific", "SPF");
                this.colecta1 = (this.rendLiquiCategA + this.rendLiquiCategH + gratifAux1) * this.Taxa_Lib_Col1;
                this.colecta1B = this.rendLiquiCategB * this.Taxa_Lib_Col1;
                this.colecta3 = this.rendLiquiCategE * this.Taxa_Lib_Col3;
                this.colecta4 = (this.calcMaisValBensImoveis() + this.calcMaisValPropIntelect() + this.calcMaisValPosContratua()) * this.Taxa_Lib_Col4;
                this.colecta4a = this.calcIncremPatrimon() * this.Taxa_Lib_Col1;
                this.colecta6 = this.rendLiquiCategF * this.Taxa_Lib_Col6;
                this.colecta7 = this.calcMaisValPartesSociais() * this.Taxa_Lib_Col7;
                SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
                if (spa.getEnglobG() != '1') {
                    this.colectaTribAutCatG+=this.calcMaisValPartesSociaisNEnglob() * this.Taxa_Lib_Col7;
                }
                this.colectaTotal = this.colecta1 + this.colecta1B + this.colecta2 + this.colecta3 + this.colecta4 + this.colecta6 + this.colecta7 + this.colecta4a + this.colectaTribAutCatG + this.colectaTribAutCatF;
            }
            this.retencoesFonte = this.calcRetencoesFonte();
            this.pagamentosConta = this.calcPagamConta();
            if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() != '4') {
                this.StRendimento = 0.0;
                this.StDeducoes = 0.0;
                this.StColecta = 0.0;
                this.StRetencoes = 0.0;
                this.StRendimentoXTaxa = 0.0;
            } else {
                this.StRendimento = 0.0;
                this.StDeducoes = 0.0;
                this.StColecta = 0.0;
                this.StRetencoes = 0.0;
                this.StRendimentoXTaxa = 0.0;
            }
            this.modelo3.resSimulacao = this.impostoLiquidado = this.colectaTotal - this.retencoesFonte - this.pagamentosConta + this.StColecta;
        }
        catch (Exception exc) {
            exc.printStackTrace();
        }
        return this.modelo3.resSimulacao;
    }

    public double calcColectaAdicional() {
        double valColAdicional = 0.0;
        double valColetaRendIsentos = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (this.rendDetermtx / this.coefConjugal > this.TaxasGerais_EscMax_Min) {
            valColAdicional = spa.isFamiliaCasado() ? (this.rendDetermtx / this.coefConjugal - this.TaxasGerais_EscMax_Min) * 2.0 * this.ColectaAdicionalTx : (this.rendDetermtx / this.coefConjugal - this.TaxasGerais_EscMax_Min) * this.ColectaAdicionalTx;
            valColetaRendIsentos = valColAdicional * this.rendimIsentSE / this.rendDetermtx;
        } else {
            valColAdicional = 0.0;
        }
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '2') {
            valColAdicional*=this.taxaReducColAcores;
        }
        return valColAdicional - valColetaRendIsentos;
    }

    @Override
    public double calcDeducEspecifica(Contribuinte cont) {
        double dedparc = 0.0;
        double rbparc = 0.0;
        double difparc = 0.0;
        double valPremioSeg = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (spa.getReside() == '4') {
            return 0.0;
        }
        valPremioSeg = this.modelo3.getVal("RenTrabDep", "valseguDesg", cont.getIdCont()) > this.Limite_PrSeguro_DesgRapido ? this.Limite_PrSeguro_DesgRapido : this.modelo3.getVal("RenTrabDep", "valseguDesg", cont.getIdCont());
        dedparc = this.calcDeducao1Especifica(cont) + this.modelo3.getVal("RenTrabDep", "valindmnRescUni", cont.getIdCont()) + valPremioSeg;
        rbparc = this.calcRendBrutoCatA(cont);
        difparc = rbparc - dedparc;
        if (difparc > 0.0) {
            this.quotCatA = this.calcDeducaoQuotizaA(cont);
            if (this.quotCatA > difparc) {
                this.quotCatA = difparc;
            }
        } else {
            this.quotCatA = 0.0;
        }
        return dedparc+=this.quotCatA;
    }

    @Override
    public double calcLimBeneFiscais(double colectaRendimSujeitos, double rendimIsentSE, double acrescimosRendLiq, double colectaGratificacoes, double colectaFuturoOpcoes) {
        double valorLimBeneFiscais = 0.0;
        double dedSegurosSaude = this.calcDedSeguroSaude();
        double dedPPRPPE = this.calcDedPPRPPE(rendimIsentSE, acrescimosRendLiq, colectaGratificacoes, colectaFuturoOpcoes);
        double dedImovReab = this.calcDedImovReab();
        double dedDonativosOutrasEnt = this.calcDedDonativOutr(colectaRendimSujeitos);
        double dedRPC = this.calculaRPC();
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        valorLimBeneFiscais = dedSegurosSaude + dedRPC + dedPPRPPE + dedDonativosOutrasEnt + dedImovReab;
        if (spa.getEstadoCivil() == '3') {
            if (this.escalaoContribuinte == 3) {
                if (valorLimBeneFiscais > this.Bendeficios_ESC3 / 2.0) {
                    valorLimBeneFiscais = this.Bendeficios_ESC3 / 2.0;
                }
            } else if (this.escalaoContribuinte == 4) {
                if (valorLimBeneFiscais > this.Bendeficios_ESC4 / 2.0) {
                    valorLimBeneFiscais = this.Bendeficios_ESC4 / 2.0;
                }
            } else if (this.escalaoContribuinte == 5) {
                if (valorLimBeneFiscais > this.Bendeficios_ESC5 / 2.0) {
                    valorLimBeneFiscais = this.Bendeficios_ESC5 / 2.0;
                }
            } else if (this.escalaoContribuinte == 6) {
                if (valorLimBeneFiscais > this.Bendeficios_ESC6 / 2.0) {
                    valorLimBeneFiscais = this.Bendeficios_ESC6 / 2.0;
                }
            } else if (this.escalaoContribuinte == 7) {
                if (valorLimBeneFiscais > this.Bendeficios_ESC7 / 2.0) {
                    valorLimBeneFiscais = this.Bendeficios_ESC7 / 2.0;
                }
            } else if (this.escalaoContribuinte == 8 && valorLimBeneFiscais > this.Bendeficios_ESC8 / 2.0) {
                valorLimBeneFiscais = this.Bendeficios_ESC8 / 2.0;
            }
        } else if (this.escalaoContribuinte == 3) {
            if (valorLimBeneFiscais > this.Bendeficios_ESC3) {
                valorLimBeneFiscais = this.Bendeficios_ESC3;
            }
        } else if (this.escalaoContribuinte == 4) {
            if (valorLimBeneFiscais > this.Bendeficios_ESC4) {
                valorLimBeneFiscais = this.Bendeficios_ESC4;
            }
        } else if (this.escalaoContribuinte == 5) {
            if (valorLimBeneFiscais > this.Bendeficios_ESC5) {
                valorLimBeneFiscais = this.Bendeficios_ESC5;
            }
        } else if (this.escalaoContribuinte == 6) {
            if (valorLimBeneFiscais > this.Bendeficios_ESC6) {
                valorLimBeneFiscais = this.Bendeficios_ESC6;
            }
        } else if (this.escalaoContribuinte == 7) {
            if (valorLimBeneFiscais > this.Bendeficios_ESC7) {
                valorLimBeneFiscais = this.Bendeficios_ESC7;
            }
        } else if (this.escalaoContribuinte == 8 && valorLimBeneFiscais > this.Bendeficios_ESC8) {
            valorLimBeneFiscais = this.Bendeficios_ESC8;
        }
        return valorLimBeneFiscais;
    }

    @Override
    public double calculoDeducoesColecta(double colectaRendimSujeitos, double rendimLiquido, double rendimIsentSE, double acrescimosRendLiq, double colectaGratificacoes, double colectaFuturoOpcoes) {
        double valorDeducoesColecta = 0.0;
        double dedSPDepeAscend = this.calcDedSPDA();
        double dedDespesasEducDeficientes = this.calcDedDespEducReabDef();
        double dedSegurosDeficientes = this.calcDedPremiosSegurDef(colectaRendimSujeitos);
        double dedContribVelhiceDeficientes = this.calcDedContribVelhiceDef();
        double dedSujPassDeficientes = this.calcDedSPDef();
        valorDeducoesColecta = this.calcLimDeducoesColecta() + this.calcLimBeneFiscais(colectaRendimSujeitos, rendimIsentSE, acrescimosRendLiq, colectaGratificacoes, colectaFuturoOpcoes) + dedSPDepeAscend + dedSujPassDeficientes + dedDespesasEducDeficientes + dedSegurosDeficientes + dedContribVelhiceDeficientes;
        return valorDeducoesColecta;
    }

    @Override
    public double calcDedEncImoveis() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double valH1 = this.modelo3.getVal("AbatDedColecta", "valjurosDiviImovHabi");
        double valH3 = this.modelo3.getVal("AbatDedColecta", "valimportRendasHabiP") + this.modelo3.getVal("AbatDedColecta", "valRendasLocFinan");
        double val30DH1 = this.DC_ImovER_Taxa * valH1;
        double val30DH3 = this.DC_ImovER_Taxa * valH3;
        double valLimDH1 = 0.0;
        double valLimDH3 = 0.0;
        double valLimFinal = 0.0;
        double valDedImoveisTot = 0.0;
        if (spa.getEstadoCivil() == '3') {
            if (this.escalaoContribuinte == 1) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc1_Valor;
            } else if (this.escalaoContribuinte == 2) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc2_Valor;
            } else if (this.escalaoContribuinte == 3) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc3_Valor;
            } else if (this.escalaoContribuinte == 4) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc4_Valor;
            } else if (this.escalaoContribuinte == 5) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc5_Valor;
            } else if (this.escalaoContribuinte == 6) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc6_Valor;
            } else if (this.escalaoContribuinte == 7) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc7_Valor;
            } else if (this.escalaoContribuinte == 8) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc8_Valor;
            }
            valLimDH3 = this.DC_ImovER_SepFact_DH3_Valor;
        } else {
            if (this.escalaoContribuinte == 1) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc1_Valor;
            } else if (this.escalaoContribuinte == 2) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc2_Valor;
            } else if (this.escalaoContribuinte == 3) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc3_Valor;
            } else if (this.escalaoContribuinte == 4) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc4_Valor;
            } else if (this.escalaoContribuinte == 5) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc5_Valor;
            } else if (this.escalaoContribuinte == 6) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc6_Valor;
            } else if (this.escalaoContribuinte == 7) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc7_Valor;
            } else if (this.escalaoContribuinte == 8) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc8_Valor;
            }
            valLimDH3 = this.DC_ImovER_OutrAgreg_DH3_Valor;
        }
        if (valH1 <= 0.0) {
            val30DH1 = 0.0;
        }
        if (valH3 <= 0.0) {
            val30DH3 = 0.0;
        }
        if (val30DH1 > 0.0) {
            valLimFinal = val30DH3 > 0.0 ? (valLimDH1 > valLimDH3 ? valLimDH1 : valLimDH3) : valLimDH1;
        } else if (val30DH3 > 0.0) {
            valLimFinal = val30DH1 > 0.0 ? (valLimDH1 > valLimDH3 ? valLimDH1 : valLimDH3) : valLimDH3;
        }
        if (valH1 <= 0.0) {
            val30DH1 = 0.0;
        }
        if (valH3 <= 0.0) {
            val30DH3 = 0.0;
        }
        valDedImoveisTot = val30DH1 + val30DH3 > valLimFinal ? valLimFinal : val30DH1 + val30DH3;
        return valDedImoveisTot;
    }

    @Override
    public double calcDedDependentes() {
        double resultDepend = 0.0;
        double resultDepe = 0.0;
        double resultAfilhados = 0.0;
        double resultDepe03 = 0.0;
        double resultDepGC = 0.0;
        double resultDepGC03 = 0.0;
        double resultDepDef = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        resultDepe = this.DC_Familia_Dependente_Ndef * (double)(spa.getNumDepNDef() + spa.getNumDepDef() - spa.getNumDep03());
        resultDepe03 = this.DC_Familia_Dependente_3Anos * (double)spa.getNumDep03();
        resultAfilhados = this.DC_Familia_Dependente_Ndef * (double)spa.getNumAfilhados();
        resultDepGC = this.DC_Familia_Dependente_GConj * (double)(spa.getNumDepGCNDef() + spa.getNumDepGCDef() - spa.getNumDepGC03());
        resultDepGC03 = this.DC_Familia_Dependente_GConj3Anos * (double)spa.getNumDepGC03();
        resultDepend = resultDepe + resultDepe03 + resultAfilhados + resultDepGC + resultDepGC03;
        return resultDepend;
    }

    @Override
    public double calcRendLiqAnexoBAgr(Contribuinte cont) {
        double res = 0.0;
        double resRendimentos = this.modelo3.getVal("CategoriaB", "RendimAgrB", cont.getIdCont());
        double resparcRend = 0.0;
        double resEncargos = 0.0;
        double enc63 = this.modelo3.getVal("DespesaAgrB", "valDeslocacoesAB", cont.getIdCont());
        double encTot = 0.0;
        char enquadra = this.calcEnquadramento(cont);
        if (enquadra == '1') {
            resparcRend = this.Taxa_Vendas * this.modelo3.getVal("RendimAgrB", "valVendasMercadoA", cont.getIdCont()) + this.Taxa_Prest_Servicos * this.modelo3.getVal("RendimAgrB", "valOutrosServicosA", cont.getIdCont());
            resparcRend+=this.Taxa_Vendas * this.modelo3.getVal("RendimAgrB", "valSubsidiosExplA", cont.getIdCont());
            res = (resparcRend+=this.Taxa_Prest_Servicos * this.modelo3.getVal("RendimAgrB", "valOutrosSubsidiosA", cont.getIdCont())) * this.Coef_Corr_Def_CatB;
        } else {
            resparcRend = resRendimentos * this.Coef_Corr_Def_CatB;
            resEncargos = this.modelo3.getVal("DespesaAgrB", "valValProfissionalAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valDespRepresentacaoAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valEncViaturaAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valCustosExistAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valOutrosEncgAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valContObrSegSocA", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valQuotSindA", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valQuotOrdProfA", cont.getIdCont());
            if (enc63 > 0.1 * resRendimentos) {
                enc63 = 0.1 * resRendimentos;
            }
            encTot = resEncargos + enc63;
            res = resparcRend - encTot;
        }
        return res*=this.Taxa_Rend_Agricolas_CD;
    }

    @Override
    public double calcRendLiqAnexoBPro(Contribuinte cont) {
        double res = 0.0;
        double resRendimentos = this.modelo3.getVal("CategoriaB", "RendimProB", cont.getIdCont());
        double resEncargos = 0.0;
        double resparcRend = 0.0;
        double enc63 = this.modelo3.getVal("DespesaProB", "valDeslocacoesPB", cont.getIdCont());
        double encTot = 0.0;
        char enquadra = this.calcEnquadramento(cont);
        if (enquadra == '1') {
            resparcRend = this.Taxa_Vendas * this.modelo3.getVal("RendimProB", "valVendasMercadoP", cont.getIdCont());
            resparcRend+=this.Taxa_Vendas * this.modelo3.getVal("RendimProB", "valServicosHotP", cont.getIdCont());
            resparcRend+=this.Taxa_Prest_Servicos * this.modelo3.getVal("RendimProB", "valOutrosServicosP", cont.getIdCont());
            resparcRend+=this.Taxa_Prest_Servicos * this.modelo3.getVal("RendimProB", "valRendimActFinP", cont.getIdCont());
            resparcRend+=this.modelo3.getVal("RendimProB", "valServPrestSocP", cont.getIdCont());
            resparcRend+=this.Taxa_Prest_Servicos * this.modelo3.getVal("RendimProB", "valRendPredImp", cont.getIdCont());
            resparcRend+=this.Taxa_Prest_Servicos * this.modelo3.getVal("RendimProB", "valRendCapImp", cont.getIdCont());
            resparcRend+=this.Taxa_Vendas * this.modelo3.getVal("RendimProB", "valSubsidiosExplP", cont.getIdCont());
            resparcRend+=this.Taxa_Prest_Servicos * this.modelo3.getVal("RendimProB", "valOutrosSubsidiosP", cont.getIdCont());
            resparcRend+=this.Taxa_Prest_Servicos_Red * this.modelo3.getVal("RendimProB", "valPropriedadeIntP", cont.getIdCont());
            if (this.modelo3.getVal("RendimProB", "valMicroEletr", cont.getIdCont()) >= this.Microproducao_Electric_Lim) {
                resparcRend+=this.Taxa_Vendas * this.modelo3.getVal("RendimProB", "valMicroEletr", cont.getIdCont());
            }
            res = resparcRend * this.Coef_Corr_Def_CatB;
        } else {
            resparcRend = resRendimentos * this.Coef_Corr_Def_CatB;
            resEncargos = this.modelo3.getVal("DespesaProB", "valValProfissionalPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valDespRepresentacaoPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valEncViaturaPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valCustosExistPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valOutrosEncgPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valContObrSegSocP", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valQuotSindP", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valQuotOrdProfP", cont.getIdCont());
            if (enc63 > 0.1 * resRendimentos) {
                enc63 = 0.1 * resRendimentos;
            }
            encTot = resEncargos + enc63;
            res = resparcRend - encTot;
        }
        return res;
    }

    @Override
    public double calcColectaMinExistencia(double rendimIsentSE, double colectaAposDeducoes, double rendDetermtx, int coefConjugal, double rendimColectavel, double colectaFuturoOpcoes, double colectaGratificacoes, double colectaDesportistas, double acrescimosRendLiq) {
        double totRendim = 0.0;
        double colMEaux1 = 0.0;
        double colMEaux2 = 0.0;
        double colMEaux3 = 0.0;
        double colMEaux4 = 0.0;
        double colR1 = 0.0;
        double colR2 = 0.0;
        double colectaME = 0.0;
        double rBCatA = 0.0;
        double rBCatH = 0.0;
        double resultpA = 0.0;
        double resultpH = 0.0;
        double valAuxG = 0.0;
        double valAuxGA = 0.0;
        double valAuxGB = 0.0;
        double valAuxGC = 0.0;
        double valAuxRenDT = rendDetermtx / (double)coefConjugal;
        int i = 0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        int numdeps = spa.getNumDepNDef() + spa.getNumDepDef() + spa.getNumDepGCDef() + spa.getNumDepGCNDef();
        valAuxGA = ((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4' ? this.calcMaisValBensImoveis() + this.calcMaisValPropIntelect() + this.calcMaisValPosContratua() : this.Taxa_Trib_Mais_Valias * this.calcMaisValBensImoveis() + this.Taxa_Trib_Mais_Valias * this.calcMaisValPropIntelect() + this.Taxa_Trib_Mais_Valias * this.calcMaisValPosContratua();
        if (valAuxGA < 0.0) {
            valAuxGA = 0.0;
        }
        if ((valAuxGB = this.calcMaisValPartesSociais()) < 0.0) {
            valAuxGB = 0.0;
        }
        if ((valAuxG = valAuxGA + valAuxGB + (valAuxGC = this.calcIncremPatrimon())) < 0.0) {
            valAuxG = 0.0;
        }
        while (i < this.modelo3.getNumContribs()) {
            resultpA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            rBCatA+=resultpA;
            resultpH = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
            rBCatH+=resultpH;
            ++i;
        }
        totRendim = rBCatA + rendimIsentSE + rBCatH + acrescimosRendLiq;
        totRendim = totRendim + this.rendLiquiCategE + this.rendLiquiCategB;
        totRendim = totRendim + this.rendLiquiCategF + valAuxG;
        totRendim = totRendim + colectaFuturoOpcoes + colectaGratificacoes;
        colMEaux1 = 0.5 * (totRendim+=colectaDesportistas);
        colMEaux2 = rBCatA + rendimIsentSE + rBCatH;
        colMEaux3 = totRendim - colectaAposDeducoes;
        colR1 = this.calculaR1();
        colR2 = this.calculaR2();
        colMEaux4 = colR1 + colR2;
        colectaME = colMEaux2 <= colMEaux1 ? colectaAposDeducoes : ((spa.getEstadoCivil() == '3' || spa.getEstadoCivil() == '2') && rendDetermtx <= this.CME_Limite_Valor || (spa.getEstadoCivil() == '1' || spa.getEstadoCivil() == '4') && valAuxRenDT <= this.CME_Limite_Valor ? 0.0 : (numdeps > 2 && numdeps < 5 && this.roundDouble(rendimColectavel) <= this.CME_Lim_Dep_3_4_Valor || numdeps >= 5 && this.roundDouble(rendimColectavel) <= this.CME_Lim_Dep_5_Valor ? 0.0 : (colMEaux3 < colMEaux4 ? totRendim - colMEaux4 : colectaAposDeducoes)));
        return colectaME;
    }

    @Override
    public double calculaR1() {
        int numTit = 0;
        double valR1 = 0.0;
        double valRBAR1 = 0.0;
        for (int l = 0; l < this.modelo3.getNumContribs(); ++l) {
            valRBAR1 = this.calcRendBrutoCatA(this.modelo3.getContribuinte(l)) + this.calcRendBrutoCatH(this.modelo3.getContribuinte(l));
            if (valRBAR1 >= this.SMNa) {
                ++numTit;
            }
            valRBAR1 = 0.0;
        }
        valR1 = (double)numTit * this.SMNa;
        return valR1;
    }

    @Override
    public double calculaR2() {
        double valorTitSupRBA = 0.0;
        double valR2 = 0.0;
        for (int m = 0; m < this.modelo3.getNumContribs(); ++m) {
            valorTitSupRBA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(m)) + this.calcRendBrutoCatH(this.modelo3.getContribuinte(m));
            if (valorTitSupRBA < this.SMNa) {
                valR2+=valorTitSupRBA;
            }
            valorTitSupRBA = 0.0;
        }
        return valR2;
    }

    @Override
    public double calcDespSaude5() {
        double valorApurado = 0.0;
        double valorGuardaConj = this.modelo3.getVal("AbatDedColecta", "valdespesasSaude5GC") * this.DC_Saude_Geral_Taxa;
        valorApurado = valorGuardaConj > this.DC_Saude_Limite_Total_Sep_Facto ? this.DC_Saude_Limite_Total_Sep_Facto : valorGuardaConj;
        double valorDespSaude5 = (this.modelo3.getVal("AbatDedColecta", "valdespesasSaude5SP") + this.modelo3.getVal("AbatDedColecta", "valdespesasSaude5Dep")) * this.DC_Saude_Geral_Taxa + valorApurado;
        return valorDespSaude5;
    }

    @Override
    public double calcDespSaudeOutras() {
        double valorTotal = 0.0;
        double valorOutros = 0.0;
        double valorGuardaConj = this.modelo3.getVal("AbatDedColecta", "valdespesasSaudeOutDepGC") * this.DC_Saude_Geral_Taxa;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (valorGuardaConj > this.DC_Saude_OutrosBens_Limite_GConj_Valor) {
            valorGuardaConj = this.DC_Saude_OutrosBens_Limite_GConj_Valor;
        }
        valorOutros = (this.modelo3.getVal("AbatDedColecta", "valdespesasSaudeOutSP") + this.modelo3.getVal("AbatDedColecta", "valdespesasSaudeOutDep")) * this.DC_Saude_Geral_Taxa;
        valorTotal = valorGuardaConj + valorOutros;
        if (spa.getEstadoCivil() == '3') {
            if (valorTotal > this.DC_Saude_OutrosBens_Limite_SepFact_Valor) {
                valorTotal = this.DC_Saude_OutrosBens_Limite_SepFact_Valor;
            }
        } else if (valorTotal > this.DC_Saude_OutrosBens_Limite_OutrAgreg_Valor) {
            valorTotal = this.DC_Saude_OutrosBens_Limite_OutrAgreg_Valor;
        }
        return valorTotal;
    }

    public double calcDedTotalSaude() {
        double valorApurado = 0.0;
        double valorLimiteAux = 0.0;
        int numDepGC = new Double(this.modelo3.getVal("AbatDedColecta", "NumDepGCDespSaude5")).intValue() + new Double(this.modelo3.getVal("AbatDedColecta", "NumDepGCDespSaudeOut")).intValue();
        int numDep = new Double(this.modelo3.getVal("AbatDedColecta", "NumDepDespSaude5")).intValue() + new Double(this.modelo3.getVal("AbatDedColecta", "NumDepDespSaudeOut")).intValue();
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double dedTotalSaude = this.calcDespSaude5() + this.calcDespSaudeOutras();
        valorApurado = spa.getEstadoCivil() == '3' ? (numDepGC + numDep >= 3 ? (dedTotalSaude > (valorLimiteAux = this.DC_Saude_Limite_Total_Sep_Facto + (double)numDepGC * this.DC_Saude_Limite_Total_Sep_Facto * this.DC_Saude_Tx_GuardaConjunta + (double)numDep * this.DC_Saude_Limite_Total_Sep_Facto * this.DC_Saude_Tx_OutrosDep) ? valorLimiteAux : dedTotalSaude) : (dedTotalSaude > this.DC_Saude_Limite_Total_Sep_Facto ? this.DC_Saude_Limite_Total_Sep_Facto : dedTotalSaude)) : (numDepGC + numDep >= 3 ? (dedTotalSaude > (valorLimiteAux = this.DC_Saude_Limite_Total_OutrosAgr + (double)numDepGC * this.DC_Saude_Limite_Total_Sep_Facto * this.DC_Saude_Tx_GuardaConjunta + (double)numDep * this.DC_Saude_Limite_Total_Sep_Facto * this.DC_Saude_Tx_OutrosDep) ? valorLimiteAux : dedTotalSaude) : (dedTotalSaude > this.DC_Saude_Limite_Total_OutrosAgr ? this.DC_Saude_Limite_Total_OutrosAgr : dedTotalSaude));
        return valorApurado;
    }

    @Override
    public double calcLimDeducoesColecta() {
        double valorLimDedColecta = 0.0;
        double valorLimiteAux = 0.0;
        double dedDespesasSaude = this.calcDedTotalSaude();
        double dedDespesasEducacao = this.calcDedEducFormProf();
        double dedEncargosLares = this.calcDedEncLares();
        double dedEncImov = this.calcDedEncImoveis();
        double dedPensoesObrig = this.calcAbatimentos();
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        int numdeps = spa.getNumDepNDef() + spa.getNumDepDef();
        int numDepsGC = spa.getNumDepGCDef() + spa.getNumDepGCNDef();
        int numAfilhados = spa.getNumAfilhados();
        valorLimDedColecta = dedDespesasSaude + dedDespesasEducacao + dedEncargosLares + dedEncImov + dedPensoesObrig;
        if (this.escalaoContribuinte == 3) {
            valorLimiteAux = spa.getEstadoCivil() == '3' ? (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC3_SepFacto * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC3_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC3_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC3_SepFacto) : (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC3_Out * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC3_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC3_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC3_Out);
            if (valorLimDedColecta > valorLimiteAux) {
                valorLimDedColecta = valorLimiteAux;
            }
        } else if (this.escalaoContribuinte == 4) {
            valorLimiteAux = spa.getEstadoCivil() == '3' ? (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC4_SepFacto * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC4_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC4_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC4_SepFacto) : (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC4_Out * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC4_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC4_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC4_Out);
            if (valorLimDedColecta > valorLimiteAux) {
                valorLimDedColecta = valorLimiteAux;
            }
        } else if (this.escalaoContribuinte == 5) {
            valorLimiteAux = spa.getEstadoCivil() == '3' ? (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC5_SepFacto * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC5_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC5_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC5_SepFacto) : (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC5_Out * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC5_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC5_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC5_Out);
            if (valorLimDedColecta > valorLimiteAux) {
                valorLimDedColecta = valorLimiteAux;
            }
        } else if (this.escalaoContribuinte == 6) {
            valorLimiteAux = spa.getEstadoCivil() == '3' ? (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC6_SepFacto * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC6_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC6_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC6_SepFacto) : (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC6_Out * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC6_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC6_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC6_Out);
            if (valorLimDedColecta > valorLimiteAux) {
                valorLimDedColecta = valorLimiteAux;
            }
        } else if (this.escalaoContribuinte == 7) {
            valorLimDedColecta = 0.0;
        } else if (this.escalaoContribuinte == 8) {
            valorLimDedColecta = 0.0;
        }
        return valorLimDedColecta;
    }

    @Override
    public double calcDedEducFormProf() {
        double valorApurado = 0.0;
        double valorLimiteAux = 0.0;
        double dedEducGC = this.modelo3.getVal("AbatDedColecta", "valdespesasEducDepGC") * this.DC_EducFP_Limite_TxSobreSMN_NDMaior2;
        if (dedEducGC > this.DC_EducFP_Limite_TxSobreSMN160 / 2.0) {
            dedEducGC = this.DC_EducFP_Limite_TxSobreSMN160 / 2.0;
        }
        double dedEduc = dedEducGC + (this.modelo3.getVal("AbatDedColecta", "valdespesasEducDep") + this.modelo3.getVal("AbatDedColecta", "valdespesasEducSP")) * 0.3;
        int numDep = new Double(this.modelo3.getVal("AbatDedColecta", "NumDepDespEduc")).intValue();
        int numDepGC = new Double(this.modelo3.getVal("AbatDedColecta", "NumDepGCDespEduc")).intValue();
        valorApurado = numDepGC + numDep >= 3 ? (dedEduc > (valorLimiteAux = this.DC_EducFP_Limite_TxSobreSMN160 + (double)numDepGC * this.DC_EducFP_ValorDepMais3 * (this.DC_EducFP_Limite_TxSobreSMN_NDMaior2 / 2.0) + (double)numDep * this.DC_EducFP_ValorDepMais3 * this.DC_EducFP_Limite_TxSobreSMN_NDMaior2) ? valorLimiteAux : dedEduc) : (dedEduc > this.DC_EducFP_Limite_TxSobreSMN160 ? this.DC_EducFP_Limite_TxSobreSMN160 : dedEduc);
        return valorApurado;
    }

    @Override
    public double calcDedEncLares() {
        double valorApurado = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double limite = 0.0;
        limite = spa.getEstadoCivil() == '3' ? this.DC_Lares_Limite_SepFact_Valor : this.DC_Lares_Limite_OutrAgreg_Valor;
        double dedLaresGC = this.modelo3.getVal("AbatDedColecta", "valencargosLaresGC") * this.DC_Lares_Tx;
        double dedLaresOut = this.modelo3.getVal("AbatDedColecta", "valencargosLaresOut") * this.DC_Lares_Tx;
        if (dedLaresGC > this.DC_Lares_Limite_SepFact_Valor) {
            dedLaresGC = this.DC_Lares_Limite_SepFact_Valor;
        }
        if ((valorApurado = dedLaresGC + dedLaresOut) > limite) {
            valorApurado = limite;
        }
        return valorApurado;
    }

    @Override
    public double calcDedPremiosSegurDef(double colectaRendimSujeitos) {
        double valorApurado = 0.0;
        Contribuinte spa = this.modelo3.getContribuinte("SPA");
        Contribuinte spb = this.modelo3.getContribuinte("SPB");
        Contribuinte spf = this.modelo3.getContribuinte("SPF");
        Contribuinte d1 = this.modelo3.getContribuinte("D1");
        Contribuinte d2 = this.modelo3.getContribuinte("D2");
        Contribuinte d3 = this.modelo3.getContribuinte("D3");
        double valorPremGC = this.DC_SegDef / 2.0 * this.modelo3.getVal("BenefFiscais", "valpSegurosSPDdefGC");
        double valorPremOut = this.DC_SegDef * this.modelo3.getVal("BenefFiscais", "valpSegurosSPDdefOut");
        if (valorPremGC > this.Limite_Colecta_SegDef_Tx / 2.0 * colectaRendimSujeitos) {
            valorPremGC = this.Limite_Colecta_SegDef_Tx / 2.0 * colectaRendimSujeitos;
        }
        if (valorPremOut > this.Limite_Colecta_SegDef_Tx * colectaRendimSujeitos) {
            valorPremOut = this.Limite_Colecta_SegDef_Tx * colectaRendimSujeitos;
        }
        if (spa.getGrauDef() == '1' || spa.getGrauDef() == '2' || spb.getGrauDef() == '1' || spb.getGrauDef() == '2' || spf.getGrauDef() == '1' || spf.getGrauDef() == '2' || d1.getGrauDef() == '1' || d1.getGrauDef() == '2' || d2.getGrauDef() == '1' || d2.getGrauDef() == '2' || d3.getGrauDef() == '1' || d3.getGrauDef() == '2') {
            valorApurado = valorPremGC + valorPremOut;
        }
        return valorApurado;
    }

    @Override
    public double calcDedContribVelhiceDef() {
        Contribuinte spa = this.modelo3.getContribuinte("SPA");
        Contribuinte spb = this.modelo3.getContribuinte("SPB");
        Contribuinte spf = this.modelo3.getContribuinte("SPF");
        SPA spx = (SPA)this.modelo3.getContribuinte("SPA");
        double valContribVelhiceDef = this.modelo3.getVal("AbatDedColecta", "valContribReformaDef");
        double valContribVelhiceDef25 = this.DC_ContribReformaDef_Taxa * valContribVelhiceDef;
        double valContribVelhiceDefTot = 0.0;
        if (spa.isDeficiente() || spb.isDeficiente() || spf.isDeficiente()) {
            valContribVelhiceDefTot = valContribVelhiceDef25;
            if (spx.getEstadoCivil() == '1' || spx.getEstadoCivil() == '4' || spx.isObito()) {
                if (valContribVelhiceDefTot > this.DC_ContribReformaDef_Lim_Cas) {
                    valContribVelhiceDefTot = this.DC_ContribReformaDef_Lim_Cas;
                } else if (valContribVelhiceDefTot > this.Limite_Colecta_SegDef_Tx * this.colectaRendimSujeitos) {
                    valContribVelhiceDefTot = this.Limite_Colecta_SegDef_Tx * this.colectaRendimSujeitos;
                }
            } else if (spx.getEstadoCivil() == '2' || spx.getEstadoCivil() == '3') {
                if (valContribVelhiceDefTot > this.DC_ContribReformaDef_Lim_NCas) {
                    valContribVelhiceDefTot = this.DC_ContribReformaDef_Lim_NCas;
                } else if (valContribVelhiceDefTot > this.Limite_Colecta_SegDef_Tx * this.colectaRendimSujeitos) {
                    valContribVelhiceDefTot = this.Limite_Colecta_SegDef_Tx * this.colectaRendimSujeitos;
                }
            }
        }
        return valContribVelhiceDefTot;
    }

    @Override
    public double calcDedDependentesDef() {
        double resultDepeDef = 0.0;
        double resultD1Def = 0.0;
        double resultD2Def = 0.0;
        double resultD3Def = 0.0;
        Contribuinte spd1 = this.modelo3.getContribuinte("D1");
        Contribuinte spd2 = this.modelo3.getContribuinte("D2");
        Contribuinte spd3 = this.modelo3.getContribuinte("D3");
        if (spd1.isDeficiente()) {
            if (spd1.getGrauDef() == '1') {
                resultD1Def = !spd1.isGuardaConjunta() ? this.DC_DepeDef60 : this.DC_DepeDef60GC;
            }
            if (spd1.getGrauDef() == '2') {
                resultD1Def = !spd1.isGuardaConjunta() ? this.DC_DepeDef60 + this.DC_SujPassDef90 : this.DC_DepeDef60GC + this.DC_DepeDef90GC;
            }
        }
        if (spd2.isDeficiente()) {
            if (spd2.getGrauDef() == '1') {
                resultD2Def = !spd2.isGuardaConjunta() ? this.DC_DepeDef60 : this.DC_DepeDef60GC;
            }
            if (spd2.getGrauDef() == '2') {
                resultD2Def = !spd2.isGuardaConjunta() ? this.DC_DepeDef60 + this.DC_SujPassDef90 : this.DC_DepeDef60GC + this.DC_DepeDef90GC;
            }
        }
        if (spd3.isDeficiente()) {
            if (spd3.getGrauDef() == '1') {
                resultD3Def = !spd3.isGuardaConjunta() ? this.DC_DepeDef60 : this.DC_DepeDef60GC;
            }
            if (spd3.getGrauDef() == '2') {
                resultD3Def = !spd3.isGuardaConjunta() ? this.DC_DepeDef60 + this.DC_SujPassDef90 : this.DC_DepeDef60GC + this.DC_DepeDef60GC;
            }
        }
        resultDepeDef = resultD1Def + resultD2Def + resultD3Def;
        return resultDepeDef;
    }

    @Override
    public double calcDedDespEducReabDef() {
        double valDespReabDefGC = this.modelo3.getVal("BenefFiscais", "valdespEducSPDdefGC");
        double valDespReabDefOut = this.modelo3.getVal("BenefFiscais", "valdespEducSPDdefOut");
        double valDespReabDefTot = this.DC_DespEducDef * valDespReabDefOut + this.DC_DespEducDef / 2.0 * valDespReabDefGC;
        return valDespReabDefTot;
    }

    @Override
    public double calcDedImovReab() {
        double valorApurado = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double valorReabImovGC = this.modelo3.getVal("AbatDedColecta", "valReabImvArrGC") * this.Ded_Despesas_Imov_Reab_Tx;
        double valorReabImovOut = this.modelo3.getVal("AbatDedColecta", "valReabImvArrOut") * this.Ded_Despesas_Imov_Reab_Tx;
        if (valorReabImovGC > this.Ded_Despesas_Imov_Reab / 2.0) {
            valorReabImovGC = this.Ded_Despesas_Imov_Reab / 2.0;
        }
        if (valorReabImovOut > this.Ded_Despesas_Imov_Reab) {
            valorReabImovOut = this.Ded_Despesas_Imov_Reab;
        }
        valorApurado = valorReabImovGC + valorReabImovOut;
        if (spa.getEstadoCivil() != '3') {
            if (valorApurado > this.Ded_Despesas_Imov_Reab) {
                valorApurado = this.Ded_Despesas_Imov_Reab;
            }
        } else if (valorApurado > this.Ded_Despesas_Imov_Reab / 2.0) {
            valorApurado = this.Ded_Despesas_Imov_Reab / 2.0;
        }
        return valorApurado;
    }

    @Override
    public double calcDedSeguroSaude() {
        double valorApurado = 0.0;
        double valorLimiteAux = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double valorPremiosSaudeGC = this.modelo3.getVal("AbatDedColecta", "valpremiosSegRiSaudeGC") * this.DC_SegSaude_Taxa;
        double valorPremiosSaudeOut = this.modelo3.getVal("AbatDedColecta", "valpremiosSegRiSaudeOut") * this.DC_SegSaude_Taxa;
        double valorTotal = valorPremiosSaudeGC + valorPremiosSaudeOut;
        double numGC = this.modelo3.getVal("AbatDedColecta", "numDepSegRiSaudeGC");
        double numDep = this.modelo3.getVal("AbatDedColecta", "numDepSegRiSaudeOut");
        valorApurado = spa.isFamiliaCasado() || spa.isObito() ? (numGC + numDep >= 3.0 ? (valorTotal > (valorLimiteAux = this.DC_SegSaude_Lim_Cas_Valor + numGC * (this.DC_SegSaude_Lim_Cas_ValorSobreDep / 2.0) + numDep * this.DC_SegSaude_Lim_Cas_ValorSobreDep) ? valorLimiteAux : valorTotal) : (valorTotal > this.DC_SegSaude_Lim_Cas_Valor ? this.DC_SegSaude_Lim_Cas_Valor : valorTotal)) : (numGC + numDep >= 3.0 ? (valorTotal > (valorLimiteAux = this.DC_SegSaude_Lim_NCas_Valor + numGC * (this.DC_SegSaude_Lim_NCas_ValorSobreDep / 2.0) + numDep * this.DC_SegSaude_Lim_NCas_ValorSobreDep) ? valorLimiteAux : valorTotal) : (valorTotal > this.DC_SegSaude_Lim_NCas_Valor ? this.DC_SegSaude_Lim_NCas_Valor : valorTotal));
        return valorApurado;
    }

    @Override
    public double getValColectaTotal() {
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
            return this.colectaTotal;
        }
        return this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas + this.colectaTribAutCatG + this.colectaTribAutCatF + this.colectaAdicional;
    }

    @Override
    public double getValColectaLiquida() {
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
            return this.colectaTotal + this.acrescimosColecta;
        }
        double aux1 = 0.0;
        double aux2 = 0.0;
        aux1 = this.valDeducoesColecta > this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas + this.colectaTribAutCatG + this.colectaTribAutCatF ? this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas + this.colectaTribAutCatG + this.colectaTribAutCatF : this.valDeducoesColecta;
        aux2 = this.colectaMinExistencia == this.colectaAposDeducoes ? this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas + this.colectaTribAutCatG + this.colectaTribAutCatF - aux1 : this.colectaMinExistencia + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas + this.colectaTribAutCatG + this.colectaTribAutCatF;
        if (aux2 + this.acrescimosColecta + this.colectaAdicional > 0.0) {
            return aux2 + this.acrescimosColecta + this.colectaAdicional;
        }
        return 0.0;
    }

    @Override
    public double recolhaSF(double colecta) {
        double valor = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        String servFin = spa.getServFin();
        double taxa = 0.05;
        for (int i = 0; i < this.servFinArray.length; ++i) {
            if (servFin == null || !servFin.equals(this.servFinArray[i][0])) continue;
            taxa = Double.parseDouble(this.servFinArray[i][1]);
            break;
        }
        valor = (0.05 - taxa) * colecta;
        return valor;
    }
}

