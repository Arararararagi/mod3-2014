/*
 * Decompiled with CFR 0_102.
 */
package calculo.business.logic;

import calculo.business.Modelo3;
import calculo.business.logic.Calc2007;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.SPA;
import java.math.BigDecimal;

public class Calc2008
extends Calc2007 {
    public Calc2008(Modelo3 m3) {
        super(m3);
    }

    @Override
    public double doSimulacao() {
        this.rendLiquiCategA = this.calcRendLiqCatA();
        this.acrescimosRendLiq = this.calcAcrescRendLiq();
        this.rendimIsentSE = this.calcRendIsentSujEng();
        this.rendLiquiCategE = this.calcRendLiqCatE();
        this.rendLiquiCategB = this.calcRendLiqCatB();
        this.rendLiquiCategF = this.calcRendLiqCatF();
        this.rendLiquiCategG = this.calcRendLiqCatG();
        this.rendLiquiCategH = this.calcRendLiqCatH();
        this.quoRendAnosAnt = this.calcQuoRendAnosAnter();
        this.rendimLiquido = this.rendLiquiCategA + this.rendLiquiCategB + this.rendLiquiCategE + this.rendLiquiCategF + this.rendLiquiCategG + this.rendLiquiCategH;
        if (this.rendimLiquido < 0.0) {
            this.rendimLiquido = 0.0;
        }
        this.rendimLiquido+=this.acrescimosRendLiq;
        this.rendimLiqAposDedu = this.rendimLiquido - this.deducRendLiquido;
        this.abatimentos = this.calcAbatimentos();
        this.rendimColectavel = this.rendimLiqAposDedu - this.abatimentos < 0.0 ? 0.0 : this.rendimLiqAposDedu - this.abatimentos;
        this.rendDetermtx = this.rendimColectavel + this.rendimIsentSE - this.quoRendAnosAnt;
        this.coefConjugal = this.calcCoefConjugal();
        this.valorRCC = this.calcRCC(this.rendDetermtx, this.coefConjugal);
        this.txImposto = this.calcTaxaImposto(this.valorRCC);
        this.importApurada = this.rendDetermtx == 0.0 ? 0.0 : this.rendDetermtx / this.coefConjugal * this.txImposto;
        this.parcelaAbater = this.calcParcelaAbat(this.valorRCC);
        this.valorColecta = (this.importApurada - this.parcelaAbater) * this.coefConjugal;
        this.colectaAnosAnter = this.quoRendAnosAnt > 0.0 ? this.valorColecta * this.quoRendAnosAnt / this.rendDetermtx : 0.0;
        this.colectaTribAutonomas = this.calcColectaDespTA();
        this.colectaActivFinanc = this.calcColActivFinanc(this.valorColecta, this.rendimLiquido, this.rendLiquiCategB);
        this.colectaActivFinancCorrig = this.colectaActivFinanc / this.taxaReducColAcores;
        if (this.colectaActivFinanc > 0.0 && ((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '2') {
            this.valorColecta = this.valorColecta - this.colectaActivFinanc + this.colectaActivFinancCorrig;
        }
        this.colectaRendimIsent = this.valorColecta * this.rendimIsentSE == 0.0 ? 0.0 : this.valorColecta * this.rendimIsentSE / this.rendDetermtx;
        this.colectaRendimSujeitos = this.valorColecta - this.colectaRendimIsent + this.colectaAnosAnter;
        this.colectaGratificacoes = this.calcRendimGratifica();
        this.colectaPPA = this.calcColPPA();
        this.colectaResgateFPR = this.calcResgateFPR();
        this.colectaFuturoOpcoes = this.calcColFuturos();
        this.valDeducoesColecta = this.calculoDeducoesColecta(this.colectaRendimSujeitos, this.rendimLiquido, this.rendimIsentSE, this.acrescimosRendLiq, this.colectaGratificacoes, this.colectaFuturoOpcoes);
        this.valorDeduzidoSF = this.colectaRendimSujeitos - this.valDeducoesColecta > 0.0 ? this.recolhaSF(this.colectaRendimSujeitos - this.valDeducoesColecta) : 0.0;
        this.valDeducoesColecta+=this.valorDeduzidoSF;
        if (this.valDeducoesColecta > this.colectaRendimSujeitos) {
            this.valDeducoesColecta = this.colectaRendimSujeitos;
        }
        this.colectaAposDeducoes = this.colectaRendimSujeitos - this.valDeducoesColecta < 0.0 ? 0.0 : this.colectaRendimSujeitos - this.valDeducoesColecta;
        this.colectaDesportistas = 0.0;
        this.colectaMinExistencia = this.calcColectaMinExistencia(this.rendimIsentSE, this.colectaAposDeducoes, this.rendDetermtx, new Double(this.coefConjugal).intValue(), this.rendimColectavel, this.colectaFuturoOpcoes, this.colectaGratificacoes, this.colectaDesportistas, this.acrescimosRendLiq);
        this.acrescimosColecta = this.calcAcrescColecta();
        this.colectaMaisValNEngl = this.calcColMaisValNEng();
        this.colectaDespConfiden = 0.0;
        this.colectaDespRepresen = 0.0;
        this.colectaEncarViatura = 0.0;
        this.colectaTotal = this.colectaMinExistencia + this.acrescimosColecta + this.colectaGratificacoes + this.colectaDesportistas + this.colectaTribAutonomas + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaPPA + this.colectaResgateFPR + this.colectaDespConfiden + this.colectaDespRepresen + this.colectaEncarViatura;
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
            this.colecta1 = (this.rendLiquiCategA + this.rendLiquiCategH) * this.Taxa_Lib_Col1;
            this.colecta1B = this.rendLiquiCategB * this.Taxa_Lib_Col4;
            this.colecta2 = this.rendLiquiCategE * this.Taxa_Lib_Col2;
            this.colecta3 = this.rendLiquiCategE * this.Taxa_Lib_Col3;
            this.colecta4 = (this.calcIncremPatrimon() + this.calcMaisValBensImoveis() + this.calcMaisValPropIntelect() + this.calcMaisValPosContratua()) * this.Taxa_Lib_Col4;
            this.colecta6 = this.rendLiquiCategF * this.Taxa_Lib_Col6;
            this.colecta7 = this.calcMaisValPartesSociais() * this.Taxa_Lib_Col7;
            this.colectaTotal = this.colecta1 + this.colecta1B + this.colecta2 + this.colecta3 + this.colecta4 + this.colecta6 + this.colecta7 + this.colectaTribAutCatG + this.colectaTribAutCatF;
        }
        this.retencoesFonte = this.calcRetencoesFonte();
        this.pagamentosConta = this.calcPagamConta();
        this.modelo3.resSimulacao = this.impostoLiquidado = this.colectaTotal - this.retencoesFonte - this.pagamentosConta;
        return this.modelo3.resSimulacao;
    }

    public double recolhaSF(double colecta) {
        double valor = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        String servFin = spa.getServFin();
        double taxa = 0.05;
        for (int i = 0; i < this.servFinArray.length; ++i) {
            if (servFin == null || !servFin.equals(this.servFinArray[i][0])) continue;
            taxa = Double.parseDouble(this.servFinArray[i][1]);
            break;
        }
        valor = (0.05 - taxa) * colecta;
        return valor;
    }

    @Override
    public double getValDeduColecta() {
        if (this.valDeducoesColecta > this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR) {
            return this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR;
        }
        return this.valDeducoesColecta;
    }

    @Override
    public double getValColectaLiquida() {
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
            return this.colectaTotal + this.acrescimosColecta;
        }
        double aux1 = 0.0;
        double aux2 = 0.0;
        aux1 = this.valDeducoesColecta > this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas ? this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas : this.valDeducoesColecta;
        aux2 = this.colectaMinExistencia == this.colectaAposDeducoes ? this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas - aux1 : this.colectaMinExistencia + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas;
        if (aux2 + this.acrescimosColecta > 0.0) {
            return aux2 + this.acrescimosColecta;
        }
        return 0.0;
    }

    @Override
    public double calcDedDependentes() {
        double resultDepend = 0.0;
        double resultDepe = 0.0;
        double resultDepe03 = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        resultDepe = this.DC_Familia_Dependente_Ndef * (double)(spa.getNumDepNDef() + spa.getNumDepDef() - spa.getNumDep03());
        resultDepe03 = this.DC_Familia_Dependente_3Anos * (double)spa.getNumDep03();
        resultDepend = resultDepe + resultDepe03;
        return resultDepend;
    }

    @Override
    public double calcDeducEspecificaH(Contribuinte cont) {
        double dedparcH = 0.0;
        dedparcH = this.calcDeducao1EspecificaH(cont) + this.calcDeducaoQuotizaH(cont) + this.modelo3.getVal("Pensoes", "ContribH", cont.getIdCont());
        return dedparcH;
    }

    @Override
    public double calculoDeducoesColecta(double colectaRendimSujeitos, double rendimLiquido, double rendimIsentSE, double acrescimosRendLiq, double colectaGratificacoes, double colectaFuturoOpcoes) {
        double valorDeducoesColecta = 0.0;
        double dedSPDepeAscend = this.calcDedSPDA();
        double dedDespesasSaude = this.calcDespSaude5();
        double dedDespesasSaudeOutros = this.calcDespSaudeOutras();
        double dedDespesasEducacao = this.calcDedEducFormProf();
        double dedEncargosLares = this.calcDedEncLares();
        double dedEncImovEngRenov = this.calcMaiorDed();
        double dedSegurosVida = this.calcDedSegVida();
        double dedSegurosSaude = this.calcDedSeguroSaude();
        double dedPPRPPE = this.calcDedPPRPPE(rendimIsentSE, acrescimosRendLiq, colectaGratificacoes, colectaFuturoOpcoes);
        double dedDespesasEducDeficientes = this.calcDedDespEducReabDef();
        double dedSegurosDeficientes = this.calcDedPremiosSegurDef(colectaRendimSujeitos);
        double dedComputadores = this.calcDedComputadores(this.txImposto);
        double dedDonativosOutrasEnt = this.calcDedDonativOutr(colectaRendimSujeitos);
        double dedSujPassDeficientes = this.calcDedSPDef();
        double dedRPC = this.calculaRPC();
        double deducaoCentenarioRepublica = this.calculaDedCentRepublica();
        valorDeducoesColecta = dedSPDepeAscend + dedDespesasSaude + dedDespesasSaudeOutros + dedDespesasEducacao + dedEncargosLares + dedEncImovEngRenov + dedSegurosVida + dedSegurosSaude + dedSujPassDeficientes + dedDespesasEducDeficientes + dedSegurosDeficientes + dedRPC + dedPPRPPE + dedDonativosOutrasEnt + dedComputadores + deducaoCentenarioRepublica;
        return valorDeducoesColecta;
    }

    public double calculaDedCentRepublica() {
        double deducao = 0.0;
        double dedCentRep = this.modelo3.getVal("BenefFiscais", "valCentRepublica");
        double dedCentRepPluri = this.modelo3.getVal("BenefFiscais", "valCentRepubPluri");
        deducao = this.Centenario_Taxa * this.Centenario_Maj * dedCentRep + this.Centenario_Taxa * this.Cent_Pluri_Maj * dedCentRepPluri;
        return deducao;
    }

    public double calculaRPC() {
        double rpcSPA = 0.0;
        double rpcSPB = 0.0;
        double rpcSPCF = 0.0;
        double spa = this.modelo3.getVal("RPC", "valRPC", "SPA");
        double spb = this.modelo3.getVal("RPC", "valRPC", "SPB");
        double spf = this.modelo3.getVal("RPC", "valRPCSPF");
        rpcSPA = this.DC_RPC_Taxa * spa;
        rpcSPB = this.DC_RPC_Taxa * spb;
        rpcSPCF = this.DC_RPC_Taxa * spf;
        if (rpcSPA > this.DC_RPC) {
            rpcSPA = this.DC_RPC;
        }
        if (rpcSPB > this.DC_RPC) {
            rpcSPB = this.DC_RPC;
        }
        if (rpcSPCF > this.DC_RPC) {
            rpcSPCF = this.DC_RPC;
        }
        return rpcSPA + rpcSPB + rpcSPCF;
    }

    @Override
    public double calcDeducao1EspecificaH(Contribuinte cont) {
        double ded1EspecificaH = 0.0;
        double dedrbh = 0.0;
        double dedHaux1 = 0.0;
        double dedHaux2 = 0.0;
        if (cont.getIdCont().equals("SPA")) {
            dedrbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", cont.getIdCont());
            dedrbh = dedrbh + this.modelo3.getVal("Pensoes", "valrendPensAlim", cont.getIdCont()) + this.modelo3.getVal("Pensoes", "PSobrev", cont.getIdCont());
            dedrbh-=this.modelo3.getVal("Pensoes", "valrendBrutoCatH", "SPF");
        } else {
            dedrbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", cont.getIdCont());
            dedrbh = dedrbh + this.modelo3.getVal("Pensoes", "valrendPensAlim", cont.getIdCont()) + this.modelo3.getVal("Pensoes", "PSobrev", cont.getIdCont());
        }
        if (!this.prereformaIsCatA(cont)) {
            if (cont.getIdCont().equals("SPA")) {
                dedrbh+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
            }
            if (cont.getIdCont().equals("SPB")) {
                dedrbh+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
            }
        }
        dedHaux1 = dedrbh - this.SPM;
        dedHaux2 = this.SPM_Taxa * dedHaux1;
        ded1EspecificaH = !cont.isDeficiente() ? (dedrbh > this.SPM ? (this.CatH_Ded1_Ndef - dedHaux2 > 0.0 ? this.CatH_Ded1_Ndef - dedHaux2 : 0.0) : (dedrbh > this.CatH_Ded1_Ndef ? this.CatH_Ded1_Ndef : dedrbh)) : (dedrbh > this.SPM ? (this.CatH_Ded1_Def - dedHaux2 > 0.0 ? this.CatH_Ded1_Def - dedHaux2 : 0.0) : (dedrbh > this.CatH_Ded1_Def ? this.CatH_Ded1_Def : dedrbh));
        return ded1EspecificaH;
    }

    @Override
    public double calcTaxaImposto(double valorRCC) {
        double taxaImposto = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (valorRCC <= this.TaxasGerais_Esc2_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc1_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc1_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc1_Madeira_Taxa;
            }
            this.escalaoContribuinte = 1;
        } else if (valorRCC <= this.TaxasGerais_Esc3_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc2_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc2_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc2_Madeira_Taxa;
            }
            this.escalaoContribuinte = 2;
        } else if (valorRCC <= this.TaxasGerais_Esc4_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc3_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc3_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc3_Madeira_Taxa;
            }
            this.escalaoContribuinte = 3;
        } else if (valorRCC <= this.TaxasGerais_Esc5_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc4_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc4_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc4_Madeira_Taxa;
            }
            this.escalaoContribuinte = 4;
        } else if (valorRCC <= this.TaxasGerais_Esc6_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc5_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc5_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc5_Madeira_Taxa;
            }
            this.escalaoContribuinte = 5;
        } else if (valorRCC <= this.TaxasGerais_Esc7_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc6_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc6_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc6_Madeira_Taxa;
            }
            this.escalaoContribuinte = 6;
        } else {
            if (valorRCC > this.TaxasGerais_Esc7_Min) {
                if (spa.getReside() == '1') {
                    taxaImposto = this.TaxasGerais_Esc7_Cont_Taxa;
                } else if (spa.getReside() == '2') {
                    taxaImposto = this.TaxasGerais_Esc7_Acores_Taxa;
                } else if (spa.getReside() == '3') {
                    taxaImposto = this.TaxasGerais_Esc7_Madeira_Taxa;
                }
            }
            this.escalaoContribuinte = 7;
        }
        return taxaImposto;
    }

    @Override
    public double calcDedEncImoveis() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double valH1 = this.modelo3.getVal("AbatDedColecta", "valjurosDiviImovHabi");
        double valH3 = this.modelo3.getVal("AbatDedColecta", "valimportRendasHabiP");
        double val30DH1 = this.DC_ImovER_Taxa * valH1;
        double val30DH3 = this.DC_ImovER_Taxa * valH3;
        double valLimDH1 = 0.0;
        double valLimDH3 = 0.0;
        double valDH1 = 0.0;
        double valDH3 = 0.0;
        double valDedImoveisTot = 0.0;
        double isClassA = this.modelo3.getVal("BenefFiscais", "classEnergetica");
        if (spa.getEstadoCivil() == '3') {
            if (isClassA == 0.0) {
                if (this.escalaoContribuinte == 1) {
                    valLimDH1 = this.DC_ImovJur_SepFact_Esc1_Valor;
                } else if (this.escalaoContribuinte == 2) {
                    valLimDH1 = this.DC_ImovJur_SepFact_Esc2_Valor;
                } else if (this.escalaoContribuinte == 3) {
                    valLimDH1 = this.DC_ImovJur_SepFact_Esc3_Valor;
                } else if (this.escalaoContribuinte == 4) {
                    valLimDH1 = this.DC_ImovJur_SepFact_Esc4_Valor;
                } else if (this.escalaoContribuinte == 5) {
                    valLimDH1 = this.DC_ImovJur_SepFact_Esc5_Valor;
                } else if (this.escalaoContribuinte == 6) {
                    valLimDH1 = this.DC_ImovJur_SepFact_Esc6_Valor;
                } else if (this.escalaoContribuinte == 7) {
                    valLimDH1 = this.DC_ImovJur_SepFact_Esc7_Valor;
                }
            } else if (this.escalaoContribuinte == 1) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc1_A_Valor;
            } else if (this.escalaoContribuinte == 2) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc2_A_Valor;
            } else if (this.escalaoContribuinte == 3) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc3_A_Valor;
            } else if (this.escalaoContribuinte == 4) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc4_A_Valor;
            } else if (this.escalaoContribuinte == 5) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc5_A_Valor;
            } else if (this.escalaoContribuinte == 6) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc6_A_Valor;
            } else if (this.escalaoContribuinte == 7) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc7_A_Valor;
            }
            valLimDH3 = this.DC_ImovER_SepFact_DH3_Valor;
        } else {
            if (isClassA == 0.0) {
                if (this.escalaoContribuinte == 1) {
                    valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc1_Valor;
                } else if (this.escalaoContribuinte == 2) {
                    valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc2_Valor;
                } else if (this.escalaoContribuinte == 3) {
                    valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc3_Valor;
                } else if (this.escalaoContribuinte == 4) {
                    valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc4_Valor;
                } else if (this.escalaoContribuinte == 5) {
                    valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc5_Valor;
                } else if (this.escalaoContribuinte == 6) {
                    valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc6_Valor;
                } else if (this.escalaoContribuinte == 7) {
                    valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc7_Valor;
                }
            } else if (this.escalaoContribuinte == 1) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc1_A_Valor;
            } else if (this.escalaoContribuinte == 2) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc2_A_Valor;
            } else if (this.escalaoContribuinte == 3) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc3_A_Valor;
            } else if (this.escalaoContribuinte == 4) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc4_A_Valor;
            } else if (this.escalaoContribuinte == 5) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc5_A_Valor;
            } else if (this.escalaoContribuinte == 6) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc6_A_Valor;
            } else if (this.escalaoContribuinte == 7) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc7_A_Valor;
            }
            valLimDH3 = this.DC_ImovER_OutrAgreg_DH3_Valor;
        }
        valDH1 = valH1 > 0.0 ? (val30DH1 > valLimDH1 ? valLimDH1 : val30DH1) : 0.0;
        valDH3 = valH3 > 0.0 ? (val30DH3 > valLimDH3 ? valLimDH3 : val30DH3) : 0.0;
        valDedImoveisTot = valDH1 > valDH3 ? valDH1 : valDH3;
        return valDedImoveisTot;
    }

    @Override
    public double calcRendBrutoCatA(Contribuinte cont) {
        double rba = 0.0;
        double rb50 = 0.0;
        double rbtot = 0.0;
        if (cont.getIdCont().equals("SPA")) {
            rba = this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont());
            rba-=this.modelo3.getVal("RenTrabDep", "valrendBruto", "SPF");
        } else {
            rba = this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont());
        }
        if (this.usarRegrasCatA(cont)) {
            rba+=this.valorRendCatBTribViaCatA(cont);
        }
        rba+=this.getDifIsento404(cont);
        if (this.prereformaIsCatA(cont)) {
            if (cont.getIdCont().equals("SPA")) {
                rba+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
            }
            if (cont.getIdCont().equals("SPB")) {
                rba+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
            }
        }
        if (!cont.isDeficiente()) {
            return rba;
        }
        if (cont.getIdCont().equals("D1") || cont.getIdCont().equals("D2") || cont.getIdCont().equals("D3")) {
            rbtot = rba;
        } else if (cont.getGrauDef() == '1' || cont.getGrauDef() == '2') {
            rb50 = this.CatA_Rb_Def_60_80_Taxa * rba;
            rbtot = rb50 > this.CatA_Rb_Def_60_80 ? rba - this.CatA_Rb_Def_60_80 : rba - rb50;
        }
        return rbtot;
    }

    @Override
    public double calcRendBrutoCatH(Contribuinte cont) {
        double rbh = 0.0;
        double rtvh = this.modelo3.getVal("Pensoes", "valrendTempVit", cont.getIdCont());
        double palh = this.modelo3.getVal("Pensoes", "valrendPensAlim", cont.getIdCont());
        double pensSobr = this.modelo3.getVal("Pensoes", "PSobrev", cont.getIdCont());
        double rh30 = 0.0;
        double rhaux1 = 0.0;
        double rhtot = 0.0;
        if (cont.getIdCont().equals("SPA")) {
            rbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", cont.getIdCont());
            rbh-=this.modelo3.getVal("Pensoes", "valrendBrutoCatH", "SPF");
        } else {
            rbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", cont.getIdCont());
        }
        if (!this.prereformaIsCatA(cont)) {
            if (cont.getIdCont().equals("SPA")) {
                rbh+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
            }
            if (cont.getIdCont().equals("SPB")) {
                rbh+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
            }
        }
        rh30 = this.CatH_RB_NDefFA_60_80_Taxa * (rbh + this.modelo3.getVal("Pensoes", "valrendTempVit", cont.getIdCont()) + this.modelo3.getVal("Pensoes", "valrendPensAlim", cont.getIdCont()) + this.modelo3.getVal("Pensoes", "PSobrev", cont.getIdCont()));
        if (!cont.isDeficiente()) {
            rhtot = rbh + rtvh;
            rhtot = rhtot + palh + pensSobr;
            return rhtot;
        }
        if (cont.getIdCont().equals("D1") || cont.getIdCont().equals("D2") || cont.getIdCont().equals("D3")) {
            rhtot = rbh + rtvh;
            return rhtot+=palh;
        }
        rhaux1 = rh30 > this.CatH_RB_NDefFA_60_80 ? this.CatH_RB_NDefFA_60_80 : rh30;
        rhtot = rbh + rtvh + palh - rhaux1;
        return rhtot;
    }

    @Override
    public double calcMaiorDed() {
        double valMaior = 0.0;
        double valEncImoveis = this.calcDedEncImoveis();
        double valEngRenovaveis = this.calcDedEnergiasRenov();
        valMaior = valEncImoveis + valEngRenovaveis;
        return valMaior;
    }

    @Override
    public double calcColectaMinExistencia(double rendimIsentSE, double colectaAposDeducoes, double rendDetermtx, int coefConjugal, double rendimColectavel, double colectaFuturoOpcoes, double colectaGratificacoes, double colectaDesportistas, double acrescimosRendLiq) {
        double totRendim = 0.0;
        double colMEaux1 = 0.0;
        double colMEaux2 = 0.0;
        double colMEaux3 = 0.0;
        double colMEaux4 = 0.0;
        double colR1 = 0.0;
        double colR2 = 0.0;
        double colectaME = 0.0;
        double rBCatA = 0.0;
        double rBCatH = 0.0;
        double resultpA = 0.0;
        double resultpH = 0.0;
        double valAuxG = 0.0;
        double valAuxGA = 0.0;
        double valAuxGB = 0.0;
        double valAuxGC = 0.0;
        double valAuxRenDT = rendDetermtx / (double)coefConjugal;
        int i = 0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        int numdeps = spa.getNumDepNDef() + spa.getNumDepDef();
        valAuxGA = ((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4' ? this.calcMaisValBensImoveis() + this.calcMaisValPropIntelect() + this.calcMaisValPosContratua() : this.Taxa_Trib_Mais_Valias * this.calcMaisValBensImoveis() + this.Taxa_Trib_Mais_Valias * this.calcMaisValPropIntelect() + this.Taxa_Trib_Mais_Valias * this.calcMaisValPosContratua();
        if (valAuxGA < 0.0) {
            valAuxGA = 0.0;
        }
        if ((valAuxGB = this.calcMaisValPartesSociais()) < 0.0) {
            valAuxGB = 0.0;
        }
        if ((valAuxG = valAuxGA + valAuxGB + (valAuxGC = this.calcIncremPatrimon())) < 0.0) {
            valAuxG = 0.0;
        }
        while (i < this.modelo3.getNumContribs()) {
            resultpA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            rBCatA+=resultpA;
            resultpH = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
            rBCatH+=resultpH;
            ++i;
        }
        totRendim = rBCatA + rendimIsentSE + rBCatH + acrescimosRendLiq;
        totRendim = totRendim + this.rendLiquiCategE + this.rendLiquiCategB;
        totRendim = totRendim + this.rendLiquiCategF + valAuxG;
        totRendim = totRendim + colectaFuturoOpcoes + colectaGratificacoes;
        colMEaux1 = 0.5 * (totRendim+=colectaDesportistas);
        colMEaux2 = rBCatA + rendimIsentSE;
        colMEaux3 = totRendim - colectaAposDeducoes;
        colR1 = this.calculaR1();
        colR2 = this.calculaR2();
        colMEaux4 = colR1 + colR2;
        colectaME = colMEaux2 <= colMEaux1 ? colectaAposDeducoes : ((spa.getEstadoCivil() == '3' || spa.getEstadoCivil() == '2') && rendDetermtx <= this.CME_Limite_Valor || (spa.getEstadoCivil() == '1' || spa.getEstadoCivil() == '4') && valAuxRenDT <= this.CME_Limite_Valor ? 0.0 : (numdeps > 2 && numdeps < 5 && this.roundDouble(rendimColectavel) <= this.CME_Lim_Dep_3_4_Valor || numdeps >= 5 && this.roundDouble(rendimColectavel) <= this.CME_Lim_Dep_5_Valor ? 0.0 : (colMEaux3 < colMEaux4 ? totRendim - colMEaux4 : colectaAposDeducoes)));
        return colectaME;
    }

    public double roundDouble(double value) {
        int decimalPlace = 2;
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(decimalPlace, 4);
        value = bd.doubleValue();
        return value;
    }

    @Override
    public double calcColectaDespTA() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        for (int i = 0; i < this.modelo3.getNumContribs(); ++i) {
            Contribuinte cont = this.modelo3.getContribuinte(i);
            if (cont.getIdCont().equals("SPF")) continue;
            this.colectaDespTA1+=this.modelo3.getVal("TribAutonomas", "valDespesaTA1", cont.getIdCont());
            this.colectaDespTA2+=this.modelo3.getVal("TribAutonomas", "valDespesaTA2", cont.getIdCont());
            this.colectaDespTA3+=this.modelo3.getVal("TribAutonomas", "valDespesaTA3", cont.getIdCont());
            this.colectaDespTA4+=this.modelo3.getVal("TribAutonomas", "valDespesaTA4", cont.getIdCont());
            this.colectaDespTA5+=this.modelo3.getVal("TribAutonomas", "valDespesaTA5", cont.getIdCont());
        }
        this.colectaDespTA1*=this.Taxa_DespTA1;
        this.colectaDespTA2*=this.Taxa_DespTA2;
        this.colectaDespTA3*=this.Taxa_DespTA3;
        this.colectaDespTA4*=this.Taxa_DespTA4;
        this.colectaDespTA5*=this.Taxa_DespTA5;
        if (spa.getReside() == '2') {
            this.colectaDespTA1*=this.taxaReducColAcores;
            this.colectaDespTA2*=this.taxaReducColAcores;
            this.colectaDespTA3*=this.taxaReducColAcores;
            this.colectaDespTA4*=this.taxaReducColAcores;
            this.colectaDespTA5*=this.taxaReducColAcores;
        }
        if (spa.getReside() == '3') {
            this.colectaDespTA2*=this.taxaReducColMadeira;
        }
        return this.colectaDespTA1 + this.colectaDespTA2 + this.colectaDespTA3 + this.colectaDespTA4 + this.colectaDespTA5;
    }

    @Override
    public double calcRendLiqCatG() {
        double res = 0.0;
        double resA = 0.0;
        double resB = 0.0;
        double resC = 0.0;
        resA = ((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4' ? this.calcMaisValBensImoveis() : this.Taxa_Trib_Mais_Valias * this.calcMaisValBensImoveis();
        resA = ((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4' ? (resA+=this.calcMaisValPropIntelect()) : (resA+=this.Taxa_Trib_Mais_Valias * this.calcMaisValPropIntelect());
        resA = ((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4' ? (resA+=this.calcMaisValPosContratua()) : (resA+=this.Taxa_Trib_Mais_Valias * this.calcMaisValPosContratua());
        if (resA < 0.0) {
            resA = 0.0;
        }
        if ((resB = this.calcMaisValPartesSociais()) < 0.0) {
            resB = 0.0;
        }
        if ((res = resA + resB + (resC+=this.calcIncremPatrimon())) > 0.0) {
            return res;
        }
        return 0.0;
    }

    @Override
    public double getValorMaisVal() {
        double rCatG = 0.0;
        double rCatGA = 0.0;
        double rCatGB = 0.0;
        double rCatGC = 0.0;
        rCatGB = this.calcMaisValPartesSociais();
        if (rCatGB < 0.0) {
            rCatGB = 0.0;
        }
        rCatGA = ((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4' ? this.calcMaisValBensImoveis() + this.calcMaisValPropIntelect() + this.calcMaisValPosContratua() : this.Taxa_Trib_Mais_Valias * this.calcMaisValBensImoveis() + this.Taxa_Trib_Mais_Valias * this.calcMaisValPropIntelect() + this.Taxa_Trib_Mais_Valias * this.calcMaisValPosContratua();
        if (rCatGA < 0.0) {
            rCatGA = 0.0;
        }
        if ((rCatG = rCatGA + rCatGB + (rCatGC = this.calcIncremPatrimon())) < 0.0) {
            rCatG = 0.0;
        }
        return rCatG;
    }

    @Override
    public double getValRendimGlobal() {
        int i = 0;
        double resauxA = 0.0;
        double resauxH = 0.0;
        double rCatA = 0.0;
        double rCatH = 0.0;
        double rCatE = this.rendLiquiCategE;
        double rCatF = this.modelo3.getVal("Prediais", "valtotalRendas") + this.calcSublocacao();
        double rCatG = this.getValorMaisVal();
        double rCatB = this.rendLiquiCategB;
        if (rCatF < 0.0) {
            rCatF = 0.0;
        }
        while (i < this.modelo3.getNumContribs()) {
            resauxA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            rCatA+=resauxA;
            resauxH = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
            rCatH+=resauxH;
            ++i;
        }
        return rCatA + rCatH + rCatB + rCatE + rCatF + this.acrescimosRendLiq + rCatG;
    }
}

