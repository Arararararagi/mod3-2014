/*
 * Decompiled with CFR 0_102.
 */
package calculo.business.logic;

import calculo.business.Modelo3;
import calculo.business.logic.Calc2013;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.SPA;

public class Calc2014
extends Calc2013 {
    public final String[][] servFinArray = new String[][]{{"0019", "2.00"}, {"0027", "4.00"}, {"0035", "5.00"}, {"0051", "5.00"}, {"0060", "4.00"}, {"0078", "5.00"}, {"0086", "5.00"}, {"0094", "5.00"}, {"0108", "5.00"}, {"0116", "0.00"}, {"0132", "5.00"}, {"0140", "5.00"}, {"0159", "3.00"}, {"0167", "4.50"}, {"0191", "4.00"}, {"0213", "5.00"}, {"0221", "5.00"}, {"0230", "5.00"}, {"0248", "5.00"}, {"0256", "5.00"}, {"0264", "5.00"}, {"0299", "5.00"}, {"0302", "4.75"}, {"0310", "5.00"}, {"0329", "5.00"}, {"0337", "5.00"}, {"0353", "5.00"}, {"0361", "4.85"}, {"0370", "5.00"}, {"0388", "5.00"}, {"0400", "3.00"}, {"0418", "5.00"}, {"0426", "5.00"}, {"0434", "2.50"}, {"0450", "5.00"}, {"0477", "5.00"}, {"0485", "5.00"}, {"0493", "0.00"}, {"0507", "5.00"}, {"0515", "4.00"}, {"0523", "5.00"}, {"0531", "5.00"}, {"0540", "2.50"}, {"0558", "5.00"}, {"0566", "2.00"}, {"0574", "5.00"}, {"0582", "2.50"}, {"0590", "2.50"}, {"0612", "5.00"}, {"0620", "5.00"}, {"0639", "0.00"}, {"0647", "0.00"}, {"0655", "2.50"}, {"0663", "5.00"}, {"0680", "2.50"}, {"0698", "5.00"}, {"0701", "0.00"}, {"0710", "5.00"}, {"0736", "5.00"}, {"0744", "5.00"}, {"0752", "0.00"}, {"0760", "5.00"}, {"0787", "4.00"}, {"0795", "5.00"}, {"0817", "5.00"}, {"0825", "5.00"}, {"0841", "5.00"}, {"0850", "5.00"}, {"0868", "5.00"}, {"0884", "5.00"}, {"0892", "5.00"}, {"0914", "5.00"}, {"0922", "5.00"}, {"0930", "5.00"}, {"0949", "5.00"}, {"0957", "5.00"}, {"0965", "5.00"}, {"0973", "5.00"}, {"0981", "5.00"}, {"0990", "4.00"}, {"1007", "5.00"}, {"1015", "0.00"}, {"1023", "5.00"}, {"1031", "5.00"}, {"1040", "5.00"}, {"1058", "5.00"}, {"1074", "5.00"}, {"1082", "5.00"}, {"1090", "2.50"}, {"1104", "5.00"}, {"1112", "5.00"}, {"1120", "5.00"}, {"1139", "5.00"}, {"1147", "5.00"}, {"1155", "5.00"}, {"1163", "2.50"}, {"1180", "5.00"}, {"1198", "2.00"}, {"1210", "5.00"}, {"1236", "0.00"}, {"1260", "0.00"}, {"1279", "5.00"}, {"1295", "5.00"}, {"1309", "3.75"}, {"1333", "5.00"}, {"1341", "4.00"}, {"1350", "3.00"}, {"1384", "5.00"}, {"1414", "1.00"}, {"1430", "5.00"}, {"1449", "5.00"}, {"1457", "5.00"}, {"1465", "4.80"}, {"1473", "4.75"}, {"1490", "5.00"}, {"1503", "3.75"}, {"1538", "5.00"}, {"1546", "4.75"}, {"1554", "5.00"}, {"1562", "4.00"}, {"1570", "5.00"}, {"1589", "5.00"}, {"1600", "2.50"}, {"1619", "2.50"}, {"1627", "5.00"}, {"1635", "5.00"}, {"1643", "3.50"}, {"1660", "3.00"}, {"1678", "2.00"}, {"1686", "0.00"}, {"1716", "3.00"}, {"1724", "5.00"}, {"1732", "5.00"}, {"1740", "5.00"}, {"1775", "5.00"}, {"1783", "5.00"}, {"1791", "4.00"}, {"1805", "5.00"}, {"1813", "5.00"}, {"1821", "5.00"}, {"1848", "4.00"}, {"1856", "5.00"}, {"1872", "4.00"}, {"1910", "5.00"}, {"1929", "4.50"}, {"1937", "5.00"}, {"1945", "5.00"}, {"1953", "5.00"}, {"1961", "4.50"}, {"1970", "5.00"}, {"1988", "5.00"}, {"1996", "5.00"}, {"2003", "5.00"}, {"2011", "3.00"}, {"2020", "5.00"}, {"2038", "5.00"}, {"2054", "4.00"}, {"2070", "4.00"}, {"2089", "5.00"}, {"2097", "5.00"}, {"2119", "5.00"}, {"2127", "5.00"}, {"2135", "4.00"}, {"2143", "5.00"}, {"2160", "5.00"}, {"2194", "4.00"}, {"2208", "5.00"}, {"2216", "5.00"}, {"2232", "5.00"}, {"2259", "4.90"}, {"2275", "2.00"}, {"2283", "5.00"}, {"2305", "3.00"}, {"2313", "5.00"}, {"2321", "0.00"}, {"2330", "2.50"}, {"2356", "1.50"}, {"2372", "0.00"}, {"2399", "5.00"}, {"2402", "5.00"}, {"2410", "5.00"}, {"2437", "5.00"}, {"2496", "5.00"}, {"2500", "1.00"}, {"2518", "5.00"}, {"2534", "3.00"}, {"2542", "5.00"}, {"2550", "4.00"}, {"2569", "5.00"}, {"2577", "2.50"}, {"2585", "5.00"}, {"2593", "5.00"}, {"2607", "1.00"}, {"2615", "2.00"}, {"2623", "0.00"}, {"2631", "4.00"}, {"2674", "5.00"}, {"2682", "5.00"}, {"2704", "5.00"}, {"2720", "4.00"}, {"2739", "5.00"}, {"2755", "5.00"}, {"2771", "5.00"}, {"2798", "5.00"}, {"2810", "4.00"}, {"2836", "5.00"}, {"2844", "5.00"}, {"2860", "5.00"}, {"2879", "5.00"}, {"2887", "5.00"}, {"2895", "2.50"}, {"2933", "5.00"}, {"2941", "5.00"}, {"2950", "5.00"}, {"2968", "4.00"}, {"2984", "5.00"}, {"3018", "5.00"}, {"3026", "5.00"}, {"3034", "5.00"}, {"3042", "5.00"}, {"3069", "2.50"}, {"3085", "2.50"}, {"3107", "2.50"}, {"3131", "3.80"}, {"3140", "3.80"}, {"3158", "5.00"}, {"3166", "4.00"}, {"3174", "5.00"}, {"3182", "5.00"}, {"3190", "5.00"}, {"3204", "5.00"}, {"3239", "2.50"}, {"3247", "2.50"}, {"3255", "2.50"}, {"3263", "2.50"}, {"3301", "2.50"}, {"3328", "2.50"}, {"3336", "2.50"}, {"3344", "2.50"}, {"3360", "5.00"}, {"3387", "5.00"}, {"3417", "5.00"}, {"3425", "4.85"}, {"3433", "3.75"}, {"3441", "5.00"}, {"3450", "4.00"}, {"3468", "5.00"}, {"3476", "5.00"}, {"3514", "5.00"}, {"3522", "5.00"}, {"3530", "5.00"}, {"3549", "4.00"}, {"3557", "4.00"}, {"3590", "5.00"}, {"3603", "5.00"}, {"3611", "3.80"}, {"3654", "5.00"}, {"3735", "5.00"}, {"3824", "5.00"}, {"3859", "5.00"}, {"3964", "5.00"}, {"4170", "5.00"}, {"4200", "5.00"}, {"4219", "5.00"}, {"4227", "5.00"}, {"4235", "2.50"}, {"9997", "2.50"}};

    public Calc2014(Modelo3 m3) {
        super(m3);
    }

    @Override
    public double doSimulacao() {
        try {
            this.rendLiquiCategA = this.calcRendLiqCatA();
            this.acrescimosRendLiq = this.calcAcrescRendLiq();
            this.rendimIsentSE = this.calcRendIsentSujEng();
            this.rendLiquiCategE = this.calcRendLiqCatE();
            this.rendLiquiCategB = this.calcRendLiqCatB();
            this.rendLiquiCategF = this.calcRendLiqCatF();
            this.rendLiquiCategG = this.calcRendLiqCatG();
            this.rendLiquiCategH = this.calcRendLiqCatH();
            this.quoRendAnosAnt = this.calcQuoRendAnosAnter();
            this.rendimLiquido = this.rendLiquiCategA + this.rendLiquiCategB + this.rendLiquiCategE + this.rendLiquiCategF + this.rendLiquiCategG + this.rendLiquiCategH;
            if (this.rendimLiquido < 0.0) {
                this.rendimLiquido = 0.0;
            }
            this.rendimLiquido+=this.acrescimosRendLiq;
            this.rendimLiqAposDedu = this.rendimLiquido - this.deducRendLiquido;
            this.abatimentos = 0.0;
            this.rendimColectavel = this.rendimLiqAposDedu - this.abatimentos < 0.0 ? 0.0 : this.rendimLiqAposDedu - this.abatimentos;
            this.rendDetermtx = this.rendimColectavel + this.rendimIsentSE - this.quoRendAnosAnt;
            this.coefConjugal = this.calcCoefConjugal();
            this.valorRCC = this.calcRCC(this.rendDetermtx, this.coefConjugal);
            this.txImposto = this.calcTaxaImposto(this.valorRCC);
            this.importApurada = this.rendDetermtx == 0.0 ? 0.0 : this.rendDetermtx / this.coefConjugal * this.txImposto;
            this.parcelaAbater = this.calcParcelaAbat(this.valorRCC);
            this.valorColecta = (this.importApurada - this.parcelaAbater) * this.coefConjugal;
            this.colectaAnosAnter = this.quoRendAnosAnt > 0.0 ? this.valorColecta * this.quoRendAnosAnt / this.rendDetermtx : 0.0;
            this.colectaTribAutonomas = this.calcColectaDespTA();
            this.colectaActivFinanc = this.calcColActivFinanc(this.valorColecta, this.rendimLiquido, this.rendLiquiCategB);
            this.colectaActivFinancCorrig = this.escalaoContribuinte == 1 ? this.colectaActivFinanc / this.taxaReducColAcores_Esc1 : (this.escalaoContribuinte == 2 ? this.colectaActivFinanc / this.taxaReducColAcores_Esc2 : this.colectaActivFinanc / this.taxaReducColAcores);
            if (this.colectaActivFinanc > 0.0 && ((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '2') {
                this.valorColecta = this.valorColecta - this.colectaActivFinanc + this.colectaActivFinancCorrig;
            }
            this.colectaRendimIsent = this.valorColecta * this.rendimIsentSE == 0.0 ? 0.0 : this.valorColecta * this.rendimIsentSE / this.rendDetermtx;
            this.colectaRendimSujeitos = this.valorColecta - this.colectaRendimIsent + this.colectaAnosAnter;
            this.colectaGratificacoes = this.calcRendimGratifica();
            this.colectaPPA = this.calcColPPA();
            this.colectaResgateFPR = this.calcResgateFPR();
            this.colectaFuturoOpcoes = this.calcColFuturos();
            this.valDeducoesColecta = this.calculoDeducoesColecta(this.colectaRendimSujeitos, this.rendimLiquido, this.rendimIsentSE, this.acrescimosRendLiq, this.colectaGratificacoes, this.colectaFuturoOpcoes);
            this.valorDeduzidoSF = this.colectaRendimSujeitos - this.valDeducoesColecta > 0.0 ? this.recolhaSF(this.colectaRendimSujeitos - this.valDeducoesColecta) : 0.0;
            this.valDeducoesColecta+=this.valorDeduzidoSF;
            if (this.valDeducoesColecta > this.colectaRendimSujeitos) {
                this.valDeducoesColecta = this.colectaRendimSujeitos;
            }
            this.colectaAposDeducoes = this.colectaRendimSujeitos - this.valDeducoesColecta < 0.0 ? 0.0 : this.colectaRendimSujeitos - this.valDeducoesColecta;
            this.colectaDesportistas = 0.0;
            this.colectaMinExistencia = this.calcColectaMinExistencia(this.rendimIsentSE, this.colectaAposDeducoes, this.rendDetermtx, new Double(this.coefConjugal).intValue(), this.rendimColectavel, this.colectaFuturoOpcoes, this.colectaGratificacoes, this.colectaDesportistas, this.acrescimosRendLiq);
            this.acrescimosColecta = this.calcAcrescColecta();
            this.colectaMaisValNEngl = this.calcColMaisValNEng();
            this.colectaTribAutCatF = this.calcColTribAutCatF();
            this.colectaTribAutCatG = this.calcColTribAutCatG();
            this.colectaAdicional = this.calcColectaAdicional();
            this.colectaDespConfiden = 0.0;
            this.colectaDespRepresen = 0.0;
            this.colectaEncarViatura = 0.0;
            this.colectaTotal = this.colectaMinExistencia + this.acrescimosColecta + this.colectaGratificacoes + this.colectaDesportistas + this.colectaTribAutonomas + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaPPA + this.colectaResgateFPR + this.colectaDespConfiden + this.colectaDespRepresen + this.colectaEncarViatura + this.colectaTribAutCatG + this.colectaTribAutCatF + this.colectaAdicional;
            if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
                double gratifAux1 = this.modelo3.getVal("RenTrabDep", "valgratific", "SPA") + this.modelo3.getVal("RenTrabDep", "valgratific", "SPB") + this.modelo3.getVal("RenTrabDep", "valgratific", "D1") + this.modelo3.getVal("RenTrabDep", "valgratific", "D2") + this.modelo3.getVal("RenTrabDep", "valgratific", "D3") + this.modelo3.getVal("RenTrabDep", "valgratific", "SPF");
                this.colecta1 = (this.rendLiquiCategA + this.rendLiquiCategH + gratifAux1) * this.Taxa_Lib_Col1;
                this.colecta1B = this.rendLiquiCategB * this.Taxa_Lib_Col4;
                this.colecta3 = this.rendLiquiCategE * this.Taxa_Lib_Col3;
                this.colecta4 = (this.calcMaisValBensImoveis() + this.calcMaisValPropIntelect() + this.calcMaisValPosContratua()) * this.Taxa_Lib_Col4;
                this.colecta4a = this.calcIncremPatrimon() * this.Taxa_Lib_Col1;
                this.colecta6 = this.rendLiquiCategF * this.Taxa_Lib_Col6;
                this.colecta7 = this.calcMaisValPartesSociais() * this.Taxa_Lib_Col7;
                SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
                if (!this.categoriasFGEnglobadas()) {
                    this.colectaTribAutCatG+=this.calcMaisValPartesSociaisNEnglob() * this.Taxa_Lib_Col7;
                }
                this.colectaTotal = this.colecta1 + this.colecta1B + this.colecta2 + this.colecta3 + this.colecta4 + this.colecta6 + this.colecta7 + this.colecta4a + this.colectaTribAutCatG + this.colectaTribAutCatF;
            }
            this.retencoesFonte = this.calcRetencoesFonte();
            this.pagamentosConta = this.calcPagamConta();
            if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() != '4') {
                this.StRendimento = this.calcSobretaxaRendimento();
                this.StDeducoes = this.calcSobretaxaDeducoes();
                this.StColecta = this.calcSobretaxaColecta();
                this.StRetencoes = this.calcSobretaxaRetencoes();
                this.StRendimentoXTaxa = this.calcSobretaxaRendimentoXTaxa();
            } else {
                this.StRendimento = 0.0;
                this.StDeducoes = 0.0;
                this.StColecta = 0.0;
                this.StRetencoes = 0.0;
                this.StRendimentoXTaxa = 0.0;
            }
            this.modelo3.resSimulacao = this.impostoLiquidado = this.colectaTotal - this.retencoesFonte - this.pagamentosConta + this.StColecta;
        }
        catch (Exception exc) {
            exc.printStackTrace();
        }
        return this.modelo3.resSimulacao;
    }

    @Override
    public double calcRendLiqCatA() {
        double res = 0.0;
        double resparc = 0.0;
        double resparc1 = 0.0;
        double resparc2 = 0.0;
        double resparc3 = 0.0;
        double resparc4 = 0.0;
        double resparc5 = 0.0;
        double dedEspTemp = 0.0;
        double valorRendIsentos = 0.0;
        for (int i = 0; i < this.modelo3.getNumContribs(); ++i) {
            resparc1 = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            dedEspTemp = ((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4' ? 0.0 : this.calcDeducEspecificaLiq(this.modelo3.getContribuinte(i));
            resparc2 = resparc1 - dedEspTemp;
            res+=resparc2;
        }
        return res;
    }

    @Override
    public double calcRendBrutoCatA(Contribuinte cont) {
        double rba = 0.0;
        double rb50 = 0.0;
        double rbtot = 0.0;
        rba = cont.getIdCont().equals("SPA") ? this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont()) : this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont());
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
            rba = rba + this.modelo3.getVal("RendIsentos", "cod401", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod402", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod404", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod405", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod406", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod407", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod409", cont.getIdCont());
        }
        if (this.usarRegrasCatA(cont)) {
            rba+=this.valorRendCatBTribViaCatA(cont);
        }
        rba+=this.getDifIsento404(cont);
        if (this.prereformaIsCatA(cont)) {
            if (cont.getIdCont().equals("SPA")) {
                rba+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
            }
            if (cont.getIdCont().equals("SPB")) {
                rba+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
            }
        }
        if (!cont.isDeficiente()) {
            return rba;
        }
        if (cont.getIdCont().equals("D1") || cont.getIdCont().equals("D2") || cont.getIdCont().equals("D3")) {
            rbtot = rba;
        } else if (cont.getGrauDef() == '1' || cont.getGrauDef() == '2') {
            rb50 = 0.1 * rba;
            rbtot = rb50 > this.CatA_Rb_Def_60_80 ? rba - this.CatA_Rb_Def_60_80 : rba - rb50;
        }
        return rbtot;
    }

    @Override
    public double calcDeducEspecificaLiq(Contribuinte cont) {
        double res = 0.0;
        double resparc = 0.0;
        double resparc1 = 0.0;
        double resparc2 = 0.0;
        double resparc3 = 0.0;
        double resparc4 = 0.0;
        double resparc5 = 0.0;
        double quotasOrdem = 0.0;
        double limite = 0.0;
        double valorRendIsentos = 0.0;
        int i = 0;
        resparc1 = this.calcRendBrutoCatA(cont);
        resparc2 = this.calcDeducEspecifica(cont);
        if (!cont.getIdCont().equals("SPF")) {
            valorRendIsentos = valorRendIsentos + this.modelo3.getVal("RendIsentos", "cod401", this.modelo3.getContribuinte(i).getIdCont()) + this.modelo3.getVal("RendIsentos", "cod402", this.modelo3.getContribuinte(i).getIdCont()) + this.modelo3.getVal("RendIsentos", "cod404", this.modelo3.getContribuinte(i).getIdCont()) + this.modelo3.getVal("RendIsentos", "cod405", this.modelo3.getContribuinte(i).getIdCont()) + this.modelo3.getVal("RendIsentos", "cod406", this.modelo3.getContribuinte(i).getIdCont()) + this.modelo3.getVal("RendIsentos", "cod407", this.modelo3.getContribuinte(i).getIdCont()) + this.modelo3.getVal("RendIsentos", "cod409", this.modelo3.getContribuinte(i).getIdCont());
        }
        resparc4 = resparc2 * resparc1;
        resparc5 = resparc1 - this.getDifIsento404(this.modelo3.getContribuinte(i)) + valorRendIsentos;
        if (resparc5 != 0.0) {
            if (resparc4 > 0.0) {
                resparc3 = resparc4 / resparc5;
            }
        } else {
            resparc3 = 0.0;
        }
        quotasOrdem = this.calcRendLiqCatB() > 0.0 ? 0.0 : this.calcQDFP(cont);
        if (resparc3 < this.CatA_Ded1_Sem_QOPeDFP_Valor_Def && quotasOrdem == 0.0) {
            resparc3 = this.CatA_Ded1_Sem_QOPeDFP_Valor_Def;
        } else if (quotasOrdem > 0.0) {
            limite = this.CatA_Ded1_Sem_QOPeDFP_Valor_Def + quotasOrdem;
            if (limite > this.CatA_Ded1_Com_QOPouDFP_Valor_NDef) {
                limite = this.CatA_Ded1_Com_QOPouDFP_Valor_NDef;
            }
            if (resparc3 < limite) {
                resparc3 = limite;
            }
        }
        resparc = resparc1 < resparc3 ? resparc1 : resparc3;
        return res+=resparc;
    }

    @Override
    public double calcRendLiqCatF() {
        double res = 0.0;
        double resparc = 0.0;
        double resAnosAntF = this.modelo3.getVal("Prediais", "valperdasAnosAntF");
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        res = this.categoriasFGEnglobadas() ? (spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendas") - this.modelo3.getVal("Prediais", "valdespManutencao") - this.modelo3.getVal("Prediais", "valdespConservacao") - this.modelo3.getVal("Prediais", "valtaxasAutarq") - this.modelo3.getVal("Prediais", "valcontribAutarq") - this.modelo3.getVal("Prediais", "valdespCondominio") + (this.modelo3.getVal("Prediais", "valtotalRendasImv") - this.modelo3.getVal("Prediais", "valdespManutencaoImv") - this.modelo3.getVal("Prediais", "valdespConservacaoImv") - this.modelo3.getVal("Prediais", "valtaxasAutarqImv") - this.modelo3.getVal("Prediais", "valcontribAutarqImv") - this.modelo3.getVal("Prediais", "valdespCondominioImv")) + this.calcSublocacao() : this.modelo3.getVal("Prediais", "valtotalRendas") - this.modelo3.getVal("Prediais", "valdespManutencao") - this.modelo3.getVal("Prediais", "valdespConservacao") - this.modelo3.getVal("Prediais", "valtaxasAutarq") - this.modelo3.getVal("Prediais", "valcontribAutarq") - this.modelo3.getVal("Prediais", "valdespCondominio") + this.calcSublocacao()) : (spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendasImv") - this.modelo3.getVal("Prediais", "valdespManutencaoImv") - this.modelo3.getVal("Prediais", "valdespConservacaoImv") - this.modelo3.getVal("Prediais", "valtaxasAutarqImv") - this.modelo3.getVal("Prediais", "valcontribAutarqImv") - this.modelo3.getVal("Prediais", "valdespCondominioImv") : 0.0);
        if (res > 0.0) {
            resparc = res - resAnosAntF;
            if (resparc < 0.0) {
                return 0.0;
            }
            return resparc;
        }
        return 0.0;
    }

    @Override
    public double calcColTribAutCatF() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double valorC = 0.0;
        double valorColectaNaoEngl = 0.0;
        valorC = this.modelo3.getVal("Prediais", "valtotalRendasImv");
        double valorColecta = (valorC - this.modelo3.getVal("Prediais", "valdespManutencaoImv") - this.modelo3.getVal("Prediais", "valdespConservacaoImv") - this.modelo3.getVal("Prediais", "valtaxasAutarqImv") - this.modelo3.getVal("Prediais", "valcontribAutarqImv") - this.modelo3.getVal("Prediais", "valdespCondominioImv")) * this.Colecta_Desp_Imov_Reav_CatF_Tx;
        valorColectaNaoEngl = (this.modelo3.getVal("Prediais", "valtotalRendas") - this.modelo3.getVal("Prediais", "valdespManutencao") - this.modelo3.getVal("Prediais", "valdespConservacao") - this.modelo3.getVal("Prediais", "valtaxasAutarq") - this.modelo3.getVal("Prediais", "valcontribAutarq") - this.modelo3.getVal("Prediais", "valdespCondominio") + this.calcSublocacao()) * this.Taxa_col_rend_CatF_Nao_Englob;
        if (spa.getReside() == '2') {
            valorColectaNaoEngl*=this.taxaReducColAcores;
        }
        if (valorColecta < 0.0) {
            valorColecta = 0.0;
        }
        if (valorColectaNaoEngl < 0.0) {
            valorColectaNaoEngl = 0.0;
        }
        if (!(this.categoriasFGEnglobadas() || spa.getEnglobF() == '1')) {
            return valorColecta + valorColectaNaoEngl;
        }
        if (!(this.categoriasFGEnglobadas() || spa.getEnglobF() != '1')) {
            return valorColectaNaoEngl;
        }
        if (this.categoriasFGEnglobadas() && spa.getEnglobF() == '1') {
            return 0.0;
        }
        return valorColecta;
    }

    @Override
    public char calcEnquadramento(Contribuinte cont) {
        char enquadra = '1';
        double rendBrutoB = 0.0;
        double totalRendims = 0.0;
        double totalRendimsSP = 0.0;
        int i = 0;
        int j = 0;
        double resauxA = 0.0;
        double resaux1B = 0.0;
        double resaux2B = 0.0;
        double resaux2ProB = 0.0;
        double resaux2AgrB = 0.0;
        double resaux3B = 0.0;
        double resaux3ProB = 0.0;
        double resaux3AgrB = 0.0;
        double resauxH = 0.0;
        double rCatA = 0.0;
        double rCatH = 0.0;
        double rCatB = 0.0;
        double rCatE = this.rendLiquiCategE;
        double rCatF = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        rCatF = this.categoriasFGEnglobadas() ? (spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendas") + this.modelo3.getVal("Prediais", "valtotalRendasImv") : this.modelo3.getVal("Prediais", "valtotalRendas")) : (spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendasImv") : 0.0);
        double rCatG = this.getValorMaisVal();
        double rIsent = this.rendimIsentSE;
        double rAcres = this.acrescimosRendLiq;
        double valVendas = 0.0;
        if (rCatF < 0.0) {
            rCatF = 0.0;
        }
        while (i < this.modelo3.getNumContribs()) {
            resauxA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            rCatA+=resauxA;
            resauxH = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
            rCatH+=resauxH;
            ++i;
        }
        while (j < this.modelo3.getNumContribs() - 1) {
            resaux1B = this.calcRBAnexoB(this.modelo3.getContribuinte(j));
            rCatB+=resaux1B;
            resaux2ProB+=this.calcRBAnexoProC(this.modelo3.getContribuinte(j));
            resaux2AgrB+=this.calcRBAnexoAgrC(this.modelo3.getContribuinte(j));
            resaux3ProB+=this.calcRBAnexoProD(this.modelo3.getContribuinte(j));
            resaux3AgrB+=this.calcRBAnexoAgrD(this.modelo3.getContribuinte(j));
            ++j;
        }
        if (resaux2ProB > 0.0) {
            rCatB+=resaux2ProB;
        }
        if (resaux2AgrB > 0.0) {
            rCatB+=resaux2AgrB;
        }
        if (resaux3ProB > 0.0) {
            rCatB+=resaux3ProB;
        }
        if (resaux3AgrB > 0.0) {
            rCatB+=resaux3AgrB;
        }
        totalRendimsSP = this.calcRendBrutoCatA(cont) + this.calcRendBrutoCatH(cont) + this.calcRBAnexoB(cont);
        if (this.calcRBAnexoProC(cont) > 0.0) {
            totalRendimsSP+=this.calcRBAnexoProC(cont);
        }
        if (this.calcRBAnexoAgrC(cont) > 0.0) {
            totalRendimsSP+=this.calcRBAnexoAgrC(cont);
        }
        if (this.calcRBAnexoProD(cont) > 0.0) {
            totalRendimsSP+=this.calcRBAnexoProD(cont);
        }
        if (this.calcRBAnexoAgrD(cont) > 0.0) {
            totalRendimsSP+=this.calcRBAnexoAgrD(cont);
        }
        totalRendims = rCatA + rCatH + rCatB + rCatE + rCatF + rCatG + rIsent + rAcres;
        rendBrutoB = this.modelo3.getVal("CategoriaB", "RendimProB", cont.getIdCont()) + this.modelo3.getVal("CategoriaB", "RendimAgrB", cont.getIdCont()) + this.getValRendIsento("cod403", cont) + this.getValRendIsento("cod408", cont) + this.getValRendIsento("cod410", cont);
        valVendas = this.modelo3.getVal("RendimAgrB", "valVendasMercadoA", cont.getIdCont());
        valVendas+=this.modelo3.getVal("RendimProB", "valVendasMercadoP", cont.getIdCont());
        enquadra = cont.getnaturRendB() == '2' && rendBrutoB > this.Catb_acto_isolado ? '2' : '1';
        cont.setenquadramB(enquadra);
        return enquadra;
    }

    @Override
    public boolean categoriasFGEnglobadas() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        boolean englobadas = spa.getOpcaoEnglobCatF() && (this.modelo3.getVal("Prediais", "valtotalRendas") != 0.0 || this.modelo3.getVal("Prediais", "valdespManutencao") != 0.0 || this.modelo3.getVal("Prediais", "valdespConservacao") != 0.0 || this.modelo3.getVal("Prediais", "valtaxasAutarq") != 0.0 || this.modelo3.getVal("Prediais", "valcontribAutarq") != 0.0 || this.modelo3.getVal("Prediais", "valdespCondominio") != 0.0) || spa.getEnglobG() == '1' && (this.modelo3.getVal("MaisValias", "valtotDespePartSoc") != 0.0 || this.modelo3.getVal("OpFinancei", "valoutros") != 0.0 || this.modelo3.getVal("OpFinancei", "vali4") != 0.0 || this.modelo3.getVal("OpFinancei", "valoutros") != 0.0 || this.modelo3.getVal("OpFinancei", "valcertif") != 0.0 || this.modelo3.getVal("MaisValias", "valtotRealiPartSoc") != 0.0 || this.modelo3.getVal("MaisValias", "valtotAquisPartSoc") != 0.0 || this.modelo3.getVal("MaisValias", "valtotDespePartSoc") != 0.0 || this.calcMaisValPropIntelect() != 0.0 || this.calcMaisValPosContratua() != 0.0 || this.calcIncremPatrimon() != 0.0) || this.modelo3.getVal("Capitais", "valJurosDepositos") != 0.0 || this.modelo3.getVal("Capitais", "valJurosPremios") != 0.0 || this.modelo3.getVal("Capitais", "valJurosSuprimento") != 0.0 || this.modelo3.getVal("Capitais", "valLucrosAdianta") != 0.0 || this.modelo3.getVal("Capitais", "valoutrosCapitais1") != 0.0 || this.modelo3.getVal("Capitais", "valoutrosCapitais2") != 0.0 || this.modelo3.getVal("Capitais", "valRendFundCapRisco") != 0.0;
        return englobadas;
    }

    @Override
    public double calcMaisValPartesSociais() {
        double res = 0.0;
        double warrants = this.modelo3.getVal("OpFinancei", "vali4");
        double futOpcoes = this.modelo3.getVal("OpFinancei", "valoutros");
        double certificados = this.modelo3.getVal("OpFinancei", "valcertif");
        double totalRealiza = this.modelo3.getVal("MaisValias", "valtotRealiPartSoc");
        double totalAquisic = this.modelo3.getVal("MaisValias", "valtotAquisPartSoc");
        double totalDespEnc = this.modelo3.getVal("MaisValias", "valtotDespePartSoc");
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (this.categoriasFGEnglobadas()) {
            res = totalRealiza - totalAquisic - totalDespEnc;
            res = res + warrants + futOpcoes + certificados;
        }
        return res;
    }

    @Override
    public double getValPerdasRecup() {
        double perdasB = 0.0;
        double perdasF = 0.0;
        double perdasG = 0.0;
        double resG = this.getValorMaisVal();
        double resAnosAntG = this.modelo3.getVal("MaisValias", "valperdasAnosAntG");
        double resAnosAntF = this.modelo3.getVal("Prediais", "valperdasAnosAntF");
        double resF = this.categoriasFGEnglobadas() ? this.modelo3.getVal("Prediais", "valtotalRendas") - this.modelo3.getVal("Prediais", "valdespManutencao") - this.modelo3.getVal("Prediais", "valdespConservacao") - this.modelo3.getVal("Prediais", "valtaxasAutarq") - this.modelo3.getVal("Prediais", "valcontribAutarq") - this.modelo3.getVal("Prediais", "valdespCondominio") : 0.0;
        perdasF = resF > 0.0 ? (resF - resAnosAntF < 0.0 ? resF : resAnosAntF) : 0.0;
        perdasG = resG > 0.0 ? (resG - resAnosAntG < 0.0 ? resG : resAnosAntG) : 0.0;
        return perdasB + perdasF + perdasG;
    }

    @Override
    public double getValRendimGlobal() {
        int i = 0;
        double resauxA = 0.0;
        double resauxH = 0.0;
        double rCatA = 0.0;
        double rCatH = 0.0;
        double rCatE = this.rendLiquiCategE;
        double rCatF = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        rCatF = spa.getEnglobF() == '1' ? (this.categoriasFGEnglobadas() ? this.modelo3.getVal("Prediais", "valtotalRendas") + this.modelo3.getVal("Prediais", "valtotalRendasImv") + this.calcSublocacao() : this.modelo3.getVal("Prediais", "valtotalRendasImv")) : (this.categoriasFGEnglobadas() ? this.modelo3.getVal("Prediais", "valtotalRendas") + this.calcSublocacao() : 0.0);
        double rCatG = this.getValorMaisVal();
        double rCatB = this.calcRendLiqCatB();
        if (rCatF < 0.0) {
            rCatF = 0.0;
        }
        while (i < this.modelo3.getNumContribs()) {
            resauxA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            rCatA+=resauxA;
            resauxH = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
            rCatH+=resauxH;
            ++i;
        }
        return rCatA + rCatH + rCatB + rCatE + rCatF + this.acrescimosRendLiq + rCatG;
    }

    @Override
    public double calcColectaAdicional() {
        double valColAdicional = 0.0;
        double valColAd2 = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double valColetaRendIsentos = 0.0;
        if (this.rendDetermtx / this.coefConjugal > this.TaxasGerais_EscMax_Min) {
            valColAdicional = spa.isFamiliaCasado() ? (this.rendDetermtx / this.coefConjugal > this.TaxasGerais_EscMax_Max ? (this.TaxasGerais_EscMax_Max - this.TaxasGerais_EscMax_Min) * 2.0 * this.ColectaAdicionalTx : (this.rendDetermtx / this.coefConjugal - this.TaxasGerais_EscMax_Min) * 2.0 * this.ColectaAdicionalTx) : (this.rendDetermtx / this.coefConjugal > this.TaxasGerais_EscMax_Max ? (this.TaxasGerais_EscMax_Max - this.TaxasGerais_EscMax_Min) * this.ColectaAdicionalTx : (this.rendDetermtx / this.coefConjugal - this.TaxasGerais_EscMax_Min) * this.ColectaAdicionalTx);
        }
        if (this.rendDetermtx / this.coefConjugal > this.TaxasGerais_EscMax_Max) {
            valColAd2 = spa.isFamiliaCasado() ? (this.rendDetermtx / this.coefConjugal - this.TaxasGerais_EscMax_Max) * 2.0 * (this.ColectaAdicionalTx * 2.0) : (this.rendDetermtx / this.coefConjugal - this.TaxasGerais_EscMax_Max) * (this.ColectaAdicionalTx * 2.0);
        }
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '2') {
            valColAdicional*=this.taxaReducColAcores;
            valColAd2*=this.taxaReducColAcores;
        }
        valColetaRendIsentos = this.rendDetermtx > 0.0 ? (valColAdicional + valColAd2) * this.rendimIsentSE / this.rendDetermtx : 0.0;
        return valColAdicional + valColAd2 - valColetaRendIsentos;
    }

    @Override
    public double calcSobretaxaDeducoes() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double valDeducoes = 0.0;
        int numDep = 0;
        numDep = spa.getNumDepNDef() + spa.getNumDepDef() + spa.getNumAfilhados() + spa.getNumDepGCNDef() + spa.getNumDepGCDef();
        valDeducoes = (double)numDep * this.SobretaxaDeducoes_Taxa * this.SobretaxaDeducoes / 14.0;
        return valDeducoes;
    }

    @Override
    public double calcSobretaxaColecta() {
        double valColecta = 0.0;
        valColecta = this.calcSobretaxaRendimento() * this.Taxa_trib_sobretaxa - this.calcSobretaxaDeducoes();
        if (valColecta < 0.0) {
            valColecta = 0.0;
        }
        return valColecta-=this.calcSobretaxaRetencoes();
    }

    @Override
    public double calcDedPPRPPE(double rendimIsentSE, double acrescimosRendLiq, double colectaGratificacoes, double colectaFuturoOpcoes) {
        double valorDedPPRA = 0.0;
        double valorDedPPRB = 0.0;
        double valorDedPPRF = 0.0;
        double valPPRPPETot = 0.0;
        double valRBrutoF = 0.0;
        SPA spa2 = (SPA)this.modelo3.getContribuinte("SPA");
        valRBrutoF = this.categoriasFGEnglobadas() || spa2.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendas") + this.modelo3.getVal("Prediais", "valtotalRendasImv") : this.modelo3.getVal("Prediais", "valtotalRendas");
        Contribuinte spa = this.modelo3.getContribuinte("SPA");
        Contribuinte spb = this.modelo3.getContribuinte("SPB");
        Contribuinte spf = this.modelo3.getContribuinte("SPF");
        int valIdadeSujeitoA = spa.getIdade();
        int valIdadeSujeitoB = spb.getIdade();
        int valIdadeSujeitoF = spf.getIdade();
        double valorRBAgregado = this.calcValTRB(spa) + this.calcValTRB(spb) + this.calcValTRB(spf) + valRBrutoF + this.calcRendLiqCatE() + rendimIsentSE + acrescimosRendLiq + colectaFuturoOpcoes;
        double valAuxG = this.getValorMaisVal();
        boolean spaExclusPensionista = this.calcRendBrutoCatA(spa) == 0.0 && this.modelo3.getVal("Pensoes", "valrendBrutoCatH", spa.getIdCont()) != 0.0;
        boolean spbExclusPensionista = this.calcRendBrutoCatA(spb) == 0.0 && this.modelo3.getVal("Pensoes", "valrendBrutoCatH", spb.getIdCont()) != 0.0;
        boolean spfExclusPensionista = this.calcRendBrutoCatA(spf) == 0.0 && this.modelo3.getVal("Pensoes", "valrendBrutoCatH", spf.getIdCont()) != 0.0;
        valorRBAgregado = valorRBAgregado + valAuxG + this.rendLiquiCategB;
        valorDedPPRA = !spaExclusPensionista ? this.calcValDedPPR(this.modelo3.getContribuinte(0).getIdCont(), valIdadeSujeitoA) : 0.0;
        valorDedPPRB = !spbExclusPensionista ? this.calcValDedPPR(this.modelo3.getContribuinte(1).getIdCont(), valIdadeSujeitoB) : 0.0;
        valorDedPPRF = !spfExclusPensionista ? this.calcValDedPPR("SPF", valIdadeSujeitoF) : 0.0;
        valPPRPPETot = valorDedPPRA + valorDedPPRB + valorDedPPRF;
        return valPPRPPETot;
    }

    @Override
    public double calcDedEncLares() {
        double valorApurado = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double limite = 0.0;
        limite = spa.getEstadoCivil() == '3' ? this.DC_Lares_Limite_SepFact_Valor : this.DC_Lares_Limite_OutrAgreg_Valor;
        double dedLaresGC = this.modelo3.getVal("AbatDedColecta", "valencargosLaresGC") * this.DC_Lares_Tx;
        double dedLaresOut = this.modelo3.getVal("AbatDedColecta", "valencargosLaresOut") * this.DC_Lares_Tx;
        if (dedLaresGC > this.DC_Lares_Limite_SepFact_Valor) {
            dedLaresGC = this.DC_Lares_Limite_SepFact_Valor;
        }
        if ((valorApurado = dedLaresGC + dedLaresOut) > limite) {
            valorApurado = limite;
        }
        return valorApurado;
    }

    @Override
    public double getValDedEspecificas() {
        int j = 0;
        double resRBA = 0.0;
        double resRBH = 0.0;
        double resdedA = 0.0;
        double resdedH = 0.0;
        double rDedA = 0.0;
        double rDedH = 0.0;
        double rDedF = 0.0;
        double rCatF = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        rCatF = this.categoriasFGEnglobadas() ? (spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendas") + this.modelo3.getVal("Prediais", "valtotalRendasImv") + this.calcSublocacao() : this.modelo3.getVal("Prediais", "valtotalRendas") + this.calcSublocacao()) : (spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendasImv") : 0.0);
        if (rCatF < 0.0) {
            rCatF = 0.0;
        }
        while (j < this.modelo3.getNumContribs()) {
            resRBA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(j));
            resdedA = this.calcDeducEspecificaLiq(this.modelo3.getContribuinte(j));
            if (resdedA > resRBA) {
                resdedA = resRBA;
            }
            rDedA+=resdedA;
            resRBH = this.calcRendBrutoCatH(this.modelo3.getContribuinte(j));
            resdedH = this.calcDeducEspecificaH(this.modelo3.getContribuinte(j));
            if (resdedH > resRBH) {
                resdedH = resRBH;
            }
            rDedH+=resdedH;
            ++j;
        }
        if (spa.getReside() == '4') {
            rDedA = 0.0;
        }
        rDedF = this.categoriasFGEnglobadas() ? (spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valdespManutencao") + this.modelo3.getVal("Prediais", "valdespConservacao") + this.modelo3.getVal("Prediais", "valtaxasAutarq") + this.modelo3.getVal("Prediais", "valcontribAutarq") + this.modelo3.getVal("Prediais", "valdespCondominio") + this.modelo3.getVal("Prediais", "valdespManutencaoImv") + this.modelo3.getVal("Prediais", "valdespConservacaoImv") + this.modelo3.getVal("Prediais", "valtaxasAutarqImv") + this.modelo3.getVal("Prediais", "valcontribAutarqImv") + this.modelo3.getVal("Prediais", "valdespCondominioImv") : this.modelo3.getVal("Prediais", "valdespManutencao") + this.modelo3.getVal("Prediais", "valdespConservacao") + this.modelo3.getVal("Prediais", "valtaxasAutarq") + this.modelo3.getVal("Prediais", "valcontribAutarq") + this.modelo3.getVal("Prediais", "valdespCondominio")) : (spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valdespManutencaoImv") + this.modelo3.getVal("Prediais", "valdespConservacaoImv") + this.modelo3.getVal("Prediais", "valtaxasAutarqImv") + this.modelo3.getVal("Prediais", "valcontribAutarqImv") + this.modelo3.getVal("Prediais", "valdespCondominioImv") : 0.0);
        return rDedA + rDedH + rDedF;
    }

    @Override
    public double calcDedEncImoveis() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double valH1 = this.modelo3.getVal("AbatDedColecta", "valjurosDiviImovHabi");
        double valH3 = this.modelo3.getVal("AbatDedColecta", "valimportRendasHabiP") + this.modelo3.getVal("AbatDedColecta", "valRendasLocFinan");
        double val30DH1 = this.DC_ImovER_Taxa * valH1;
        double val30DH3 = this.DC_ImovER_Taxa * valH3;
        double valLimDH1 = 0.0;
        double valLimDH3 = 0.0;
        double valLimFinal = 0.0;
        double valDedImoveisTot = 0.0;
        if (spa.getEstadoCivil() == '3') {
            if (this.escalaoContribuinte == 1) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc1_Valor;
                valLimDH3 = this.DC_ImovER_SepFact_DH1_Valor;
            } else if (this.escalaoContribuinte == 2) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc2_Valor;
                valLimDH3 = this.DC_ImovER_SepFact_DH2_Valor;
            } else if (this.escalaoContribuinte == 3) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc3_Valor;
                valLimDH3 = this.DC_ImovER_SepFact_DH3_Valor;
            } else if (this.escalaoContribuinte == 4) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc4_Valor;
                valLimDH3 = this.DC_ImovER_SepFact_DH3_Valor;
            } else if (this.escalaoContribuinte == 5) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc5_Valor;
                valLimDH3 = this.DC_ImovER_SepFact_DH3_Valor;
            } else if (this.escalaoContribuinte == 6) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc6_Valor;
                valLimDH3 = this.DC_ImovER_SepFact_DH3_Valor;
            } else if (this.escalaoContribuinte == 7) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc7_Valor;
                valLimDH3 = this.DC_ImovER_SepFact_DH3_Valor;
            } else if (this.escalaoContribuinte == 8) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc8_Valor;
                valLimDH3 = this.DC_ImovER_SepFact_DH3_Valor;
            }
        } else if (this.escalaoContribuinte == 1) {
            valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc1_Valor;
            valLimDH3 = this.DC_ImovER_OutrAgreg_DH1_Valor;
        } else if (this.escalaoContribuinte == 2) {
            valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc2_Valor;
            valLimDH3 = this.DC_ImovER_OutrAgreg_DH2_Valor;
        } else if (this.escalaoContribuinte == 3) {
            valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc3_Valor;
            valLimDH3 = this.DC_ImovER_OutrAgreg_DH3_Valor;
        } else if (this.escalaoContribuinte == 4) {
            valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc4_Valor;
            valLimDH3 = this.DC_ImovER_OutrAgreg_DH3_Valor;
        } else if (this.escalaoContribuinte == 5) {
            valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc5_Valor;
            valLimDH3 = this.DC_ImovER_OutrAgreg_DH3_Valor;
        } else if (this.escalaoContribuinte == 6) {
            valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc6_Valor;
            valLimDH3 = this.DC_ImovER_OutrAgreg_DH3_Valor;
        } else if (this.escalaoContribuinte == 7) {
            valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc7_Valor;
            valLimDH3 = this.DC_ImovER_OutrAgreg_DH3_Valor;
        } else if (this.escalaoContribuinte == 8) {
            valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc8_Valor;
            valLimDH3 = this.DC_ImovER_OutrAgreg_DH3_Valor;
        }
        if (valH1 <= 0.0) {
            val30DH1 = 0.0;
        }
        if (valH3 <= 0.0) {
            val30DH3 = 0.0;
        }
        if (val30DH1 > 0.0) {
            valLimFinal = val30DH3 > 0.0 ? (valLimDH1 > valLimDH3 ? valLimDH1 : valLimDH3) : valLimDH1;
        } else if (val30DH3 > 0.0) {
            valLimFinal = val30DH1 > 0.0 ? (valLimDH1 > valLimDH3 ? valLimDH1 : valLimDH3) : valLimDH3;
        }
        if (valH1 <= 0.0) {
            val30DH1 = 0.0;
        }
        if (valH3 <= 0.0) {
            val30DH3 = 0.0;
        }
        valDedImoveisTot = val30DH1 + val30DH3 > valLimFinal ? valLimFinal : val30DH1 + val30DH3;
        return valDedImoveisTot;
    }

    @Override
    public double calcLimDeducoesColecta() {
        double valorLimDedColecta = 0.0;
        double valorLimiteAux = 0.0;
        double dedDespesasSaude = this.calcDedTotalSaude();
        double dedDespesasEducacao = this.calcDedEducFormProf();
        double dedEncargosLares = this.calcDedEncLares();
        double dedEncImov = this.calcDedEncImoveis();
        double dedPensoesObrig = this.calcAbatimentos();
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        int numdeps = spa.getNumDepNDef() + spa.getNumDepDef();
        int numDepsGC = spa.getNumDepGCDef() + spa.getNumDepGCNDef();
        int numAfilhados = spa.getNumAfilhados();
        valorLimDedColecta = dedDespesasSaude + dedDespesasEducacao + dedEncargosLares + dedEncImov + dedPensoesObrig;
        if (this.escalaoContribuinte == 2) {
            valorLimiteAux = spa.getEstadoCivil() == '3' ? (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC2_SepFacto * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC2_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC2_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC2_SepFacto) : (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC2_Out * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC2_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC2_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC2_Out);
            if (valorLimDedColecta > valorLimiteAux) {
                valorLimDedColecta = valorLimiteAux;
            }
        } else if (this.escalaoContribuinte == 3) {
            valorLimiteAux = spa.getEstadoCivil() == '3' ? (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC3_SepFacto * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC3_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC3_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC3_SepFacto) : (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC3_Out * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC3_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC3_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC3_Out);
            if (valorLimDedColecta > valorLimiteAux) {
                valorLimDedColecta = valorLimiteAux;
            }
        } else if (this.escalaoContribuinte == 4) {
            valorLimiteAux = spa.getEstadoCivil() == '3' ? (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC4_SepFacto * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC4_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC4_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC4_SepFacto) : (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC4_Out * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC4_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC4_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC4_Out);
            if (valorLimDedColecta > valorLimiteAux) {
                valorLimDedColecta = valorLimiteAux;
            }
        } else if (this.escalaoContribuinte == 5) {
            valorLimiteAux = spa.getEstadoCivil() == '3' ? (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC5_SepFacto * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC5_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC5_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC5_SepFacto) : (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC5_Out * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC5_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC5_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC5_Out);
            if (valorLimDedColecta > valorLimiteAux) {
                valorLimDedColecta = valorLimiteAux;
            }
        } else if (this.escalaoContribuinte == 6) {
            valorLimiteAux = spa.getEstadoCivil() == '3' ? (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC6_SepFacto * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC6_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC6_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC6_SepFacto) : (numdeps + numAfilhados + numDepsGC > 0 ? this.Saude_Educac_Lares_Imoveis_ESC6_Out * (1.0 + this.Saude_Educac_Lares_Imoveis_ESC6_taxa * (double)(numdeps + numAfilhados) + this.Saude_Educac_Lares_Imoveis_ESC6_taxa / 2.0 * (double)numDepsGC) : this.Saude_Educac_Lares_Imoveis_ESC6_Out);
            if (valorLimDedColecta > valorLimiteAux) {
                valorLimDedColecta = valorLimiteAux;
            }
        } else if (this.escalaoContribuinte == 7) {
            valorLimDedColecta = 0.0;
        } else if (this.escalaoContribuinte == 8) {
            valorLimDedColecta = 0.0;
        }
        return valorLimDedColecta;
    }

    @Override
    public double recolhaSF(double colecta) {
        double valor = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        String servFin = spa.getServFin();
        double taxa = 5.0;
        for (int i = 0; i < this.servFinArray.length; ++i) {
            if (servFin == null || !servFin.equals(this.servFinArray[i][0])) continue;
            taxa = Double.parseDouble(this.servFinArray[i][1]);
            break;
        }
        valor = (5.0 - taxa) * 0.01 * colecta;
        return valor;
    }

    @Override
    public double calcLimBeneFiscais(double colectaRendimSujeitos, double rendimIsentSE, double acrescimosRendLiq, double colectaGratificacoes, double colectaFuturoOpcoes) {
        double valorLimBeneFiscais = 0.0;
        double dedSegurosSaude = this.calcDedSeguroSaude();
        double dedPPRPPE = this.calcDedPPRPPE(rendimIsentSE, acrescimosRendLiq, colectaGratificacoes, colectaFuturoOpcoes);
        double dedImovReab = this.calcDedImovReab();
        double dedDonativosOutrasEnt = this.calcDedDonativOutr(colectaRendimSujeitos);
        double dedRPC = this.calculaRPC();
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        valorLimBeneFiscais = dedSegurosSaude + dedRPC + dedPPRPPE + dedDonativosOutrasEnt + dedImovReab;
        if (spa.getEstadoCivil() == '3') {
            if (this.escalaoContribuinte == 2) {
                if (valorLimBeneFiscais > this.Bendeficios_ESC2 / 2.0) {
                    valorLimBeneFiscais = this.Bendeficios_ESC2 / 2.0;
                }
            } else if (this.escalaoContribuinte == 3) {
                if (valorLimBeneFiscais > this.Bendeficios_ESC3 / 2.0) {
                    valorLimBeneFiscais = this.Bendeficios_ESC3 / 2.0;
                }
            } else if (this.escalaoContribuinte == 4) {
                if (valorLimBeneFiscais > this.Bendeficios_ESC4 / 2.0) {
                    valorLimBeneFiscais = this.Bendeficios_ESC4 / 2.0;
                }
            } else if (this.escalaoContribuinte == 5) {
                if (valorLimBeneFiscais > this.Bendeficios_ESC5 / 2.0) {
                    valorLimBeneFiscais = this.Bendeficios_ESC5 / 2.0;
                }
            } else if (this.escalaoContribuinte == 6) {
                if (valorLimBeneFiscais > this.Bendeficios_ESC6 / 2.0) {
                    valorLimBeneFiscais = this.Bendeficios_ESC6 / 2.0;
                }
            } else if (this.escalaoContribuinte == 7) {
                if (valorLimBeneFiscais > this.Bendeficios_ESC7 / 2.0) {
                    valorLimBeneFiscais = this.Bendeficios_ESC7 / 2.0;
                }
            } else if (this.escalaoContribuinte == 8 && valorLimBeneFiscais > this.Bendeficios_ESC8 / 2.0) {
                valorLimBeneFiscais = this.Bendeficios_ESC8 / 2.0;
            }
        } else if (this.escalaoContribuinte == 2) {
            if (valorLimBeneFiscais > this.Bendeficios_ESC2) {
                valorLimBeneFiscais = this.Bendeficios_ESC2;
            }
        } else if (this.escalaoContribuinte == 3) {
            if (valorLimBeneFiscais > this.Bendeficios_ESC3) {
                valorLimBeneFiscais = this.Bendeficios_ESC3;
            }
        } else if (this.escalaoContribuinte == 4) {
            if (valorLimBeneFiscais > this.Bendeficios_ESC4) {
                valorLimBeneFiscais = this.Bendeficios_ESC4;
            }
        } else if (this.escalaoContribuinte == 5) {
            if (valorLimBeneFiscais > this.Bendeficios_ESC5) {
                valorLimBeneFiscais = this.Bendeficios_ESC5;
            }
        } else if (this.escalaoContribuinte == 6) {
            if (valorLimBeneFiscais > this.Bendeficios_ESC6) {
                valorLimBeneFiscais = this.Bendeficios_ESC6;
            }
        } else if (this.escalaoContribuinte == 7) {
            if (valorLimBeneFiscais > this.Bendeficios_ESC7) {
                valorLimBeneFiscais = this.Bendeficios_ESC7;
            }
        } else if (this.escalaoContribuinte == 8 && valorLimBeneFiscais > this.Bendeficios_ESC8) {
            valorLimBeneFiscais = this.Bendeficios_ESC8;
        }
        return valorLimBeneFiscais;
    }

    @Override
    public double calcDedDependentes() {
        double resultDepend = 0.0;
        double resultDepe = 0.0;
        double resultAfilhados = 0.0;
        double resultDepe03 = 0.0;
        double resultDepGC = 0.0;
        double resultDepGC03 = 0.0;
        double resultDepDef = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        resultAfilhados = this.DC_Familia_Dependente_Ndef * (double)spa.getNumAfilhados();
        if (spa.getNumDepNDef() + spa.getNumDepDef() + spa.getNumDepGCNDef() + spa.getNumDepGCDef() < 3) {
            resultDepe = this.DC_Familia_Dependente_Ndef * (double)(spa.getNumDepNDef() + spa.getNumDepDef() - spa.getNumDep03());
            if (resultDepe < 0.0) {
                resultDepe = 0.0;
            }
            if ((resultDepe03 = this.DC_Familia_Dependente_3Anos * (double)(spa.getNumDep03() - spa.getNumDepGC03())) < 0.0) {
                resultDepe03 = 0.0;
            }
            if ((resultDepGC = this.DC_Familia_Dependente_GConj * (double)(spa.getNumDepGCDef() + spa.getNumDepGCNDef() - spa.getNumDepGC03())) < 0.0) {
                resultDepGC = 0.0;
            }
            resultDepGC03 = this.DC_Familia_Dependente_GConj3Anos * (double)spa.getNumDepGC03();
            resultDepend = resultDepe + resultDepe03 + resultAfilhados + resultDepGC + resultDepGC03 + resultDepDef;
        } else {
            resultDepe = this.DC_Depe_Mais3anos_mais3Dep * (double)(spa.getNumDepNDef() + spa.getNumDepDef() - spa.getNumDep03());
            resultDepe03 = this.DC_Depe_Menos3anos_mais3Dep * (double)(spa.getNumDep03() - spa.getNumDepGC03());
            resultDepGC = this.DC_Depe_Mais3anos_mais3Dep / 2.0 * (double)(spa.getNumDepGCNDef() + spa.getNumDepGCDef() - spa.getNumDepGC03());
            resultDepGC03 = this.DC_Depe_Menos3anos_mais3Dep / 2.0 * (double)spa.getNumDepGC03();
            if (resultDepe < 0.0) {
                resultDepe = 0.0;
            }
            if (resultDepGC < 0.0) {
                resultDepGC = 0.0;
            }
            if (resultDepe03 < 0.0) {
                resultDepe03 = 0.0;
            }
            resultDepend = resultDepe + resultDepe03 + resultAfilhados + resultDepGC + resultDepGC03;
        }
        return resultDepend;
    }

    @Override
    public double calcDedSPDA() {
        double result = 0.0;
        double resultParcial = 0.0;
        result = this.calcDedSujeitosPassivos();
        result+=this.calcDedDependentes();
        resultParcial = result+=this.calcDedAscendentes();
        return resultParcial;
    }

    @Override
    public double calculoDeducoesColecta(double colectaRendimSujeitos, double rendimLiquido, double rendimIsentSE, double acrescimosRendLiq, double colectaGratificacoes, double colectaFuturoOpcoes) {
        double valorDeducoesColecta = 0.0;
        double dedSPDepeAscend = this.calcDedSPDA();
        double dedDespesasEducDeficientes = this.calcDedDespEducReabDef();
        double dedSegurosDeficientes = this.calcDedPremiosSegurDef(colectaRendimSujeitos);
        double dedContribVelhiceDeficientes = this.calcDedContribVelhiceDef();
        double dedSujPassDeficientes = this.calcDedSPDef();
        valorDeducoesColecta = this.calcLimDeducoesColecta() + this.calcLimBeneFiscais(colectaRendimSujeitos, rendimIsentSE, acrescimosRendLiq, colectaGratificacoes, colectaFuturoOpcoes) + dedSPDepeAscend + dedSujPassDeficientes + dedDespesasEducDeficientes + dedSegurosDeficientes + dedContribVelhiceDeficientes;
        return valorDeducoesColecta;
    }

    @Override
    public double calcSobretaxaRendimento() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double valRendimento = 0.0;
        double sobreTaxaEstadoCivil = 1.0;
        double gratifAux1 = this.modelo3.getVal("RenTrabDep", "valgratific", "SPA") + this.modelo3.getVal("RenTrabDep", "valgratific", "SPB") + this.modelo3.getVal("RenTrabDep", "valgratific", "D1") + this.modelo3.getVal("RenTrabDep", "valgratific", "D2") + this.modelo3.getVal("RenTrabDep", "valgratific", "D3") + this.modelo3.getVal("RenTrabDep", "valgratific", "SPF");
        double partSoc = this.modelo3.getVal("MaisValias", "valtotRealiPartSoc") - this.modelo3.getVal("MaisValias", "valtotAquisPartSoc") - this.modelo3.getVal("MaisValias", "valtotDespePartSoc");
        double maisValiasPS = this.modelo3.getVal("OpFinancei", "vali4") + this.modelo3.getVal("OpFinancei", "valoutros") + this.modelo3.getVal("OpFinancei", "valcertif") + partSoc;
        maisValiasPS = maisValiasPS < 0.0 ? 0.0 : maisValiasPS;
        valRendimento = this.rendimColectavel + gratifAux1;
        if (spa.isFamiliaCasado() || spa.isObito()) {
            sobreTaxaEstadoCivil = 2.0;
        }
        this.categoriasFGEnglobadas();
        if ((valRendimento-=this.SobretaxaDeducoes * sobreTaxaEstadoCivil) < 0.0) {
            valRendimento = 0.0;
        }
        return valRendimento;
    }

    @Override
    public double calcColMaisValNEng() {
        double res = 0.0;
        double warrants = this.modelo3.getVal("OpFinancei", "vali4");
        double totalRealiza = this.modelo3.getVal("MaisValias", "valtotRealiPartSoc");
        double totalAquisic = this.modelo3.getVal("MaisValias", "valtotAquisPartSoc");
        double totalDespEnc = this.modelo3.getVal("MaisValias", "valtotDespePartSoc");
        double futOpcoes = this.modelo3.getVal("OpFinancei", "valoutros");
        double certificados = this.modelo3.getVal("OpFinancei", "valcertif");
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (!this.categoriasFGEnglobadas()) {
            res = totalRealiza - totalAquisic - totalDespEnc;
            if (spa.getAnoSim() != 13 && spa.getAnoSim() != 14) {
                res = res <= 500.0 ? 0.0 : (res-=500.0);
            }
            res = res + warrants + futOpcoes + certificados;
        }
        res = spa.getReside() == '2' ? (res*=this.TaxaAnexoG_Acores_Taxa) : (res*=this.TaxaAnexoG_Geral_Taxa);
        if (res < 0.0) {
            res = 0.0;
        }
        return res;
    }

    @Override
    public double calcRendCatFApuraSTx() {
        double res = 0.0;
        double resparc = 0.0;
        double resAnosAntF = this.modelo3.getVal("Prediais", "valperdasAnosAntF");
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        res = spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendas") - this.modelo3.getVal("Prediais", "valdespManutencao") - this.modelo3.getVal("Prediais", "valdespConservacao") - this.modelo3.getVal("Prediais", "valtaxasAutarq") - this.modelo3.getVal("Prediais", "valcontribAutarq") - this.modelo3.getVal("Prediais", "valdespCondominio") + (this.modelo3.getVal("Prediais", "valtotalRendasImv") - this.modelo3.getVal("Prediais", "valdespManutencaoImv") - this.modelo3.getVal("Prediais", "valdespConservacaoImv") - this.modelo3.getVal("Prediais", "valtaxasAutarqImv") - this.modelo3.getVal("Prediais", "valcontribAutarqImv") - this.modelo3.getVal("Prediais", "valdespCondominioImv")) + this.calcSublocacao() : this.modelo3.getVal("Prediais", "valtotalRendas") - this.modelo3.getVal("Prediais", "valdespManutencao") - this.modelo3.getVal("Prediais", "valdespConservacao") - this.modelo3.getVal("Prediais", "valtaxasAutarq") - this.modelo3.getVal("Prediais", "valcontribAutarq") - this.modelo3.getVal("Prediais", "valdespCondominio") + this.calcSublocacao();
        if (res > 0.0) {
            resparc = res - resAnosAntF;
            if (resparc < 0.0) {
                return 0.0;
            }
            return resparc;
        }
        return 0.0;
    }

    @Override
    public double calcRendLiqCatB() {
        double resAgr = 0.0;
        double resPro = 0.0;
        double resConcoAgr = 0.0;
        double resConcoPro = 0.0;
        double resparcBAgr = 0.0;
        double resparcBPro = 0.0;
        double resparcBAgr1 = 0.0;
        double resparcBPro1 = 0.0;
        double resparcCAgr = 0.0;
        double resparcCPro = 0.0;
        double resparcDAgr = 0.0;
        double resparcDPro = 0.0;
        double resTotB = 0.0;
        double res = 0.0;
        double limite1 = this.CatB_Min_Tributacao_Valor;
        double coef_corr_def = 1.0;
        int enquadra = 49;
        double resValDecl = this.calcRBLimAgr();
        double resparcBAgr1tot = 0.0;
        double totRendIsent = 0.0;
        for (int i = 0; i < this.modelo3.getNumContribs() - 1; ++i) {
            resparcBAgr = 0.0;
            resparcBPro = 0.0;
            resparcBAgr1 = 0.0;
            resparcBPro1 = 0.0;
            resTotB = 0.0;
            resparcCAgr = 0.0;
            resparcCPro = 0.0;
            resparcDAgr = 0.0;
            resparcDPro = 0.0;
            double totalRend = this.modelo3.getVal("CategoriaB", "RendimAgrB", this.modelo3.getContribuinte(i).getIdCont()) + this.modelo3.getVal("CategoriaB", "RendimProB", this.modelo3.getContribuinte(i).getIdCont());
            if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
                totRendIsent = totRendIsent + this.modelo3.getVal("RendIsentos", "cod403", this.modelo3.getContribuinte(i).getIdCont()) + this.modelo3.getVal("RendIsentos", "cod408", this.modelo3.getContribuinte(i).getIdCont()) + this.modelo3.getVal("RendIsentos", "cod410", this.modelo3.getContribuinte(i).getIdCont());
            }
            this.Coef_Corr_Def_CatB = this.calcCoefCorrDef(this.modelo3.getContribuinte(i), totalRend);
            if (resValDecl <= this.CatB_Limite_Agricola) {
                resparcBAgr1 = 0.0;
            } else if (!this.usarRegrasCatA(this.modelo3.getContribuinte(i))) {
                resparcBAgr1 = this.calcRendLiqAnexoBAgr(this.modelo3.getContribuinte(i));
            }
            if (!this.usarRegrasCatA(this.modelo3.getContribuinte(i))) {
                resparcBPro1 = this.calcRendLiqAnexoBPro(this.modelo3.getContribuinte(i));
            }
            if ((enquadra = (int)this.calcEnquadramento(this.modelo3.getContribuinte(i))) == 49) {
                resTotB = resparcBAgr1 + resparcBPro1;
                if (resTotB < limite1) {
                    if (resparcBAgr1 > 0.0) {
                        resparcBAgr = resparcBAgr1 * limite1 / resTotB;
                    }
                    resConcoAgr+=resparcBAgr;
                    if (resparcBPro1 > 0.0) {
                        resparcBPro = resparcBPro1 * limite1 / resTotB;
                    }
                    resConcoPro+=resparcBPro;
                } else if (resparcBAgr1 > 0.0 && resparcBPro1 > 0.0) {
                    resparcBAgr = resparcBAgr1;
                    resparcBPro = resparcBPro1;
                    resConcoAgr+=limite1 / 2.0;
                    resConcoPro+=limite1 / 2.0;
                } else if (resparcBAgr1 > 0.0 && resparcBPro1 == 0.0) {
                    resparcBAgr = resparcBAgr1;
                    resConcoAgr+=limite1;
                } else if (resparcBAgr1 == 0.0 && resparcBPro1 > 0.0) {
                    resparcBPro = resparcBPro1;
                    resConcoPro+=limite1;
                }
            } else {
                resparcBAgr = resparcBAgr1;
                resparcBPro = resparcBPro1;
            }
            resparcCAgr = resValDecl <= this.CatB_Limite_Agricola ? 0.0 : this.calcRendLiqAnexoCAgr(this.modelo3.getContribuinte(i));
            resparcCPro = this.calcRendLiqAnexoCPro(this.modelo3.getContribuinte(i));
            resparcDAgr = resValDecl <= this.CatB_Limite_Agricola ? 0.0 : this.calcRendLiqAnexoDAgr(this.modelo3.getContribuinte(i));
            resparcDPro = this.calcRendLiqAnexoDPro(this.modelo3.getContribuinte(i));
            resAgr = resAgr + resparcBAgr + resparcCAgr + resparcDAgr;
            resPro = resPro + resparcBPro + resparcCPro + resparcDPro;
            enquadra = 49;
        }
        if (resparcBAgr + resparcCAgr + resparcDAgr < 0.0) {
            resparcBAgr = 0.0;
            resparcCAgr = 0.0;
            resparcDAgr = 0.0;
        }
        if (resparcBPro + resparcCPro + resparcDPro < 0.0) {
            resparcBPro = 0.0;
            resparcCPro = 0.0;
            resparcDPro = 0.0;
        }
        if (resAgr < 0.0) {
            resAgr = 0.0;
        }
        if (resPro < 0.0) {
            resPro = 0.0;
        }
        if (resConcoAgr == 0.0 && resConcoPro == 0.0) {
            res = resAgr <= 0.0 && resPro <= 0.0 ? 0.0 : (resAgr <= 0.0 && resPro > 0.0 ? resPro : (resAgr > 0.0 && resPro <= 0.0 ? resAgr : resAgr + resPro));
        }
        if (resConcoAgr > 0.0 && resConcoPro == 0.0) {
            if (resAgr <= 0.0) {
                resAgr = resConcoAgr;
            }
            res = resAgr > 0.0 && resPro <= 0.0 ? resAgr : resAgr + resPro;
        }
        if (resConcoAgr == 0.0 && resConcoPro > 0.0) {
            if (resPro <= 0.0) {
                resPro = resConcoPro;
            }
            res = resAgr <= 0.0 && resPro > 0.0 ? resPro : resAgr + resPro;
        }
        if (resConcoAgr > 0.0 && resConcoPro > 0.0) {
            if (resAgr <= 0.0) {
                resAgr = resConcoAgr;
            }
            if (resPro <= 0.0) {
                resPro = resConcoPro;
            }
            res = resAgr + resPro;
        }
        if (res < resConcoAgr + resConcoPro) {
            res = resConcoAgr + resConcoPro;
        }
        return res+=totRendIsent;
    }

    @Override
    public double calcColectaDespTA() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        for (int i = 0; i < this.modelo3.getNumContribs(); ++i) {
            Contribuinte cont = this.modelo3.getContribuinte(i);
            if (cont.getIdCont().equals("SPF")) continue;
            this.colectaDespTA1+=this.modelo3.getVal("TribAutonomas", "valDespesaTA1", cont.getIdCont());
            this.colectaDespTA2+=this.modelo3.getVal("TribAutonomas", "valEncargAquisMenorTA", cont.getIdCont());
            this.colectaDespTA3+=this.modelo3.getVal("TribAutonomas", "valEncargAquisMaiorTA", cont.getIdCont());
            this.colectaDespTA4+=this.modelo3.getVal("TribAutonomas", "valDespesaTA4", cont.getIdCont());
            this.colectaDespTA5+=this.modelo3.getVal("TribAutonomas", "valDespesaTA5", cont.getIdCont());
        }
        this.colectaDespTA1*=this.Taxa_DespTA1;
        this.colectaDespTA2*=this.Taxa_Desp_Repr_Via_Men_20000_geral;
        this.colectaDespTA3*=this.Taxa_Desp_Repr_Via_IgMai_20000_geral;
        this.colectaDespTA4*=this.Taxa_DespTA4;
        this.colectaDespTA5*=this.Taxa_DespTA5;
        if (spa.getReside() == '2') {
            this.colectaDespTA2*=this.taxaReducColAcores;
            this.colectaDespTA3*=this.taxaReducColAcores;
        }
        return this.colectaDespTA1 + this.colectaDespTA2 + this.colectaDespTA3 + this.colectaDespTA4 + this.colectaDespTA5;
    }

    @Override
    public double calcRendLiqAnexoBAgr(Contribuinte cont) {
        double res = 0.0;
        double resRendimentos = this.modelo3.getVal("CategoriaB", "RendimAgrB", cont.getIdCont());
        double resparcRend = 0.0;
        double resEncargos = 0.0;
        double enc63 = this.modelo3.getVal("DespesaAgrB", "valDeslocacoesAB", cont.getIdCont());
        double encTot = 0.0;
        char enquadra = this.calcEnquadramento(cont);
        if (enquadra == '1') {
            if ('4' != ((SPA)this.modelo3.getContribuinte("SPA")).getReside()) {
                resparcRend = this.Taxa_Vendas_men * this.modelo3.getVal("RendimAgrB", "valVendasMercadoA", cont.getIdCont()) + this.Taxa_Prest_Servicos * this.modelo3.getVal("RendimAgrB", "valOutrosServicosA", cont.getIdCont());
                resparcRend+=this.Taxa_Apl_Outr_Act * this.modelo3.getVal("RendimAgrB", "valRendimentoDiversosBA", cont.getIdCont());
                resparcRend+=this.Taxa_Apl_Subs_Expl * this.modelo3.getVal("RendimAgrB", "valSubsidiosExplA", cont.getIdCont());
                resparcRend+=this.Taxa_Apl_Outros_Subs * this.modelo3.getVal("RendimAgrB", "valOutrosSubsidiosA", cont.getIdCont());
                resparcRend+=this.Taxa_Outros_CatB_A * this.modelo3.getVal("RendimAgrB", "valRendimentoNaoIncluidoA", cont.getIdCont());
            } else {
                resparcRend = this.modelo3.getVal("RendimAgrB", "valVendasMercadoA", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimAgrB", "valOutrosServicosA", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimAgrB", "valRendimentoDiversosBA", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimAgrB", "valSubsidiosExplA", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimAgrB", "valOutrosSubsidiosA", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimAgrB", "valRendimentoNaoIncluidoA", cont.getIdCont());
            }
            res = resparcRend * this.Coef_Corr_Def_CatB;
        } else {
            resparcRend = resRendimentos * this.Coef_Corr_Def_CatB;
            resEncargos = this.modelo3.getVal("DespesaAgrB", "valValProfissionalAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valDespRepresentacaoAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valEncViaturaAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valCustosExistAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valOutrosEncgAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valContObrSegSocA", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valQuotSindA", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valQuotOrdProfA", cont.getIdCont());
            if (enc63 > 0.1 * resRendimentos) {
                enc63 = 0.1 * resRendimentos;
            }
            encTot = resEncargos + enc63;
            res = resparcRend - encTot;
        }
        return res*=this.Taxa_Rend_Agricolas_CD;
    }

    @Override
    public double calcRendLiqAnexoBPro(Contribuinte cont) {
        double res = 0.0;
        double resRendimentos = this.modelo3.getVal("CategoriaB", "RendimProB", cont.getIdCont());
        double resEncargos = 0.0;
        double resparcRend = 0.0;
        double enc63 = this.modelo3.getVal("DespesaProB", "valDeslocacoesPB", cont.getIdCont());
        double encTot = 0.0;
        char enquadra = this.calcEnquadramento(cont);
        if (enquadra == '1') {
            if ('4' != ((SPA)this.modelo3.getContribuinte("SPA")).getReside()) {
                resparcRend = this.Taxa_Vendas_men * this.modelo3.getVal("RendimProB", "valVendasMercadoP", cont.getIdCont());
                resparcRend+=this.Taxa_Vendas_men * this.modelo3.getVal("RendimProB", "valServicosHotP", cont.getIdCont());
                resparcRend+=this.Taxa_Apl_Act_CIRS_CAE * this.modelo3.getVal("RendimProB", "valRendimCIRSP", cont.getIdCont());
                resparcRend+=this.Taxa_Prop_Int_Ind_P * this.modelo3.getVal("RendimProB", "valPropIndustrialP", cont.getIdCont());
                resparcRend+=this.Taxa_Apl_Outr_Act * this.modelo3.getVal("RendimProB", "valSaldoPositivoP", cont.getIdCont());
                resparcRend+=this.Taxa_Outros_CatB_P * this.modelo3.getVal("RendimProB", "valOutrosCatBP", cont.getIdCont());
                resparcRend+=this.Taxa_Rend_Act_Fin * this.modelo3.getVal("RendimProB", "valRendimActFinP", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimProB", "valServPrestSocP", cont.getIdCont());
                resparcRend+=this.Taxa_Rend_PreCap_Imp * this.modelo3.getVal("RendimProB", "valRendPredImp", cont.getIdCont());
                resparcRend+=this.Taxa_Rend_PreCap_Imp * this.modelo3.getVal("RendimProB", "valRendCapImp", cont.getIdCont());
                resparcRend+=this.Taxa_Apl_Subs_Expl * this.modelo3.getVal("RendimProB", "valSubsidiosExplP", cont.getIdCont());
                resparcRend+=this.Taxa_Apl_Outros_Subs * this.modelo3.getVal("RendimProB", "valOutrosSubsidiosP", cont.getIdCont());
                if (this.modelo3.getVal("RendimProB", "valMicroEletr", cont.getIdCont()) >= this.Microproducao_Electric_Lim) {
                    resparcRend+=this.Taxa_Apl_Venda_Micro_Elect * this.modelo3.getVal("RendimProB", "valMicroEletr", cont.getIdCont());
                }
            } else {
                resparcRend = this.modelo3.getVal("RendimProB", "valVendasMercadoP", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimProB", "valServicosHotP", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimProB", "valRendimCIRSP", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimProB", "valPropIndustrialP", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimProB", "valSaldoPositivoP", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimProB", "valOutrosCatBP", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimProB", "valRendimActFinP", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimProB", "valServPrestSocP", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimProB", "valRendPredImp", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimProB", "valRendCapImp", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimProB", "valSubsidiosExplP", cont.getIdCont());
                resparcRend+=this.modelo3.getVal("RendimProB", "valOutrosSubsidiosP", cont.getIdCont());
                if (this.modelo3.getVal("RendimProB", "valMicroEletr", cont.getIdCont()) >= this.Microproducao_Electric_Lim) {
                    resparcRend+=this.modelo3.getVal("RendimProB", "valMicroEletr", cont.getIdCont());
                }
            }
            res = resparcRend * this.Coef_Corr_Def_CatB;
        } else {
            resparcRend = resRendimentos * this.Coef_Corr_Def_CatB;
            resEncargos = this.modelo3.getVal("DespesaProB", "valValProfissionalPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valDespRepresentacaoPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valEncViaturaPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valCustosExistPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valOutrosEncgPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valContObrSegSocP", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valQuotSindP", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valQuotOrdProfP", cont.getIdCont());
            if (enc63 > 0.1 * resRendimentos) {
                enc63 = 0.1 * resRendimentos;
            }
            encTot = resEncargos + enc63;
            res = resparcRend - encTot;
        }
        return res;
    }
}

