/*
 * Decompiled with CFR 0_102.
 */
package calculo.business.logic;

import calculo.business.Modelo3;
import calculo.business.Rubricas;
import calculo.business.RubricasMaisValia;
import calculo.business.logic.CalcGeral;
import calculo.business.logic.CalcInt;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;
import calculo.business.pojos.RubricaMaisValia;
import calculo.business.pojos.SPA;
import calculo.rubricas.AbatDedColecta;
import calculo.rubricas.DespEducacao;
import calculo.rubricas.MaisValias;

public class Calc2007
extends CalcGeral
implements CalcInt {
    protected Modelo3 modelo3;

    public Calc2007(Modelo3 m3) {
        this.modelo3 = m3;
    }

    @Override
    public double doSimulacao() {
        this.rendLiquiCategA = this.calcRendLiqCatA();
        this.acrescimosRendLiq = this.calcAcrescRendLiq();
        this.rendimIsentSE = this.calcRendIsentSujEng();
        this.rendLiquiCategE = this.calcRendLiqCatE();
        this.rendLiquiCategB = this.calcRendLiqCatB();
        this.rendLiquiCategF = this.calcRendLiqCatF();
        this.rendLiquiCategG = this.calcRendLiqCatG();
        this.rendLiquiCategH = this.calcRendLiqCatH();
        this.quoRendAnosAnt = this.calcQuoRendAnosAnter();
        this.rendimLiquido = this.rendLiquiCategA + this.rendLiquiCategB + this.rendLiquiCategE + this.rendLiquiCategF + this.rendLiquiCategG + this.rendLiquiCategH;
        if (this.rendimLiquido < 0.0) {
            this.rendimLiquido = 0.0;
        }
        this.rendimLiquido+=this.acrescimosRendLiq;
        this.rendimLiqAposDedu = this.rendimLiquido - this.deducRendLiquido;
        this.abatimentos = this.calcAbatimentos();
        this.rendimColectavel = this.rendimLiqAposDedu - this.abatimentos < 0.0 ? 0.0 : this.rendimLiqAposDedu - this.abatimentos;
        this.rendDetermtx = this.rendimColectavel + this.rendimIsentSE - this.quoRendAnosAnt;
        this.coefConjugal = this.calcCoefConjugal();
        this.valorRCC = this.calcRCC(this.rendDetermtx, this.coefConjugal);
        this.txImposto = this.calcTaxaImposto(this.valorRCC);
        this.importApurada = this.rendDetermtx == 0.0 ? 0.0 : this.rendDetermtx / this.coefConjugal * this.txImposto;
        this.parcelaAbater = this.calcParcelaAbat(this.valorRCC);
        this.valorColecta = (this.importApurada - this.parcelaAbater) * this.coefConjugal;
        this.colectaAnosAnter = this.quoRendAnosAnt > 0.0 ? this.valorColecta * this.quoRendAnosAnt / this.rendDetermtx : 0.0;
        this.colectaTribAutonomas = this.calcColectaDespTA();
        this.colectaActivFinanc = this.calcColActivFinanc(this.valorColecta, this.rendimLiquido, this.rendLiquiCategB);
        this.colectaActivFinancCorrig = this.colectaActivFinanc / this.taxaReducColAcores;
        if (this.colectaActivFinanc > 0.0 && ((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '2') {
            this.valorColecta = this.valorColecta - this.colectaActivFinanc + this.colectaActivFinancCorrig;
        }
        this.colectaRendimIsent = this.valorColecta * this.rendimIsentSE == 0.0 ? 0.0 : this.valorColecta * this.rendimIsentSE / this.rendDetermtx;
        this.colectaRendimSujeitos = this.valorColecta - this.colectaRendimIsent + this.colectaAnosAnter;
        this.colectaGratificacoes = this.calcRendimGratifica();
        this.colectaPPA = this.calcColPPA();
        this.colectaResgateFPR = this.calcResgateFPR();
        this.colectaFuturoOpcoes = this.calcColFuturos();
        this.valDeducoesColecta = this.calculoDeducoesColecta(this.colectaRendimSujeitos, this.rendimLiquido, this.rendimIsentSE, this.acrescimosRendLiq, this.colectaGratificacoes, this.colectaFuturoOpcoes);
        if (this.valDeducoesColecta > this.colectaRendimSujeitos) {
            this.valDeducoesColecta = this.colectaRendimSujeitos;
        }
        this.colectaAposDeducoes = this.colectaRendimSujeitos - this.valDeducoesColecta < 0.0 ? 0.0 : this.colectaRendimSujeitos - this.valDeducoesColecta;
        this.colectaDesportistas = 0.0;
        this.colectaMinExistencia = this.calcColectaMinExistencia(this.rendimIsentSE, this.colectaAposDeducoes, this.rendDetermtx, new Double(this.coefConjugal).intValue(), this.rendimColectavel, this.colectaFuturoOpcoes, this.colectaGratificacoes, this.colectaDesportistas, this.acrescimosRendLiq);
        this.acrescimosColecta = this.calcAcrescColecta();
        this.colectaMaisValNEngl = this.calcColMaisValNEng();
        this.colectaDespConfiden = 0.0;
        this.colectaDespRepresen = 0.0;
        this.colectaEncarViatura = 0.0;
        this.colectaTotal = this.colectaMinExistencia + this.acrescimosColecta + this.colectaGratificacoes + this.colectaDesportistas + this.colectaTribAutonomas + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaPPA + this.colectaResgateFPR + this.colectaDespConfiden + this.colectaDespRepresen + this.colectaEncarViatura;
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
            this.colecta1 = (this.rendLiquiCategA + this.rendLiquiCategH) * this.Taxa_Lib_Col1;
            this.colecta1B = this.rendLiquiCategB * this.Taxa_Lib_Col4;
            this.colecta2 = this.rendLiquiCategE * this.Taxa_Lib_Col2;
            this.colecta3 = this.rendLiquiCategE * this.Taxa_Lib_Col3;
            this.colecta4 = (this.calcIncremPatrimon() + this.calcMaisValBensImoveis() + this.calcMaisValPropIntelect() + this.calcMaisValPosContratua()) * this.Taxa_Lib_Col4;
            this.colecta6 = this.rendLiquiCategF * this.Taxa_Lib_Col6;
            this.colecta7 = this.calcMaisValPartesSociais() * this.Taxa_Lib_Col7;
            this.colectaTotal = this.colecta1 + this.colecta1B + this.colecta2 + this.colecta3 + this.colecta4 + this.colecta6 + this.colecta7 + this.colectaTribAutCatG + this.colectaTribAutCatF;
        }
        this.retencoesFonte = this.calcRetencoesFonte();
        this.pagamentosConta = this.calcPagamConta();
        this.modelo3.resSimulacao = this.impostoLiquidado = this.colectaTotal - this.retencoesFonte - this.pagamentosConta;
        return this.modelo3.resSimulacao;
    }

    @Override
    public double calcRendLiqCatA() {
        double res = 0.0;
        double resparc = 0.0;
        double resparc1 = 0.0;
        double resparc2 = 0.0;
        for (int i = 0; i < this.modelo3.getNumContribs(); ++i) {
            resparc1 = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            resparc = resparc1 < (resparc2 = this.calcDeducEspecifica(this.modelo3.getContribuinte(i))) ? 0.0 : resparc1 - resparc2;
            res+=resparc;
        }
        return res;
    }

    public int getAnoInt() {
        switch (((SPA)this.modelo3.getContribuinte("SPA")).getAnoSim()) {
            case 49: {
                return 2007;
            }
            case 50: {
                return 2006;
            }
            case 51: {
                return 2005;
            }
            case 52: {
                return 2004;
            }
            case 53: {
                return 2003;
            }
            case 54: {
                return 2002;
            }
            case 55: {
                return 2001;
            }
            case 56: {
                return 2008;
            }
        }
        return 2008;
    }

    public boolean prereformaIsCatA(Contribuinte cont) {
        int anoCont = 0;
        int anoPriPag = 0;
        if (cont.getIdCont().equals("SPA")) {
            anoCont = new Double(this.modelo3.getVal("PreReforma", "valdataContPreSPA")).intValue();
            anoPriPag = new Double(this.modelo3.getVal("PreReforma", "valdataPriPagPreSPA")).intValue();
        }
        if (cont.getIdCont().equals("SPB")) {
            anoCont = new Double(this.modelo3.getVal("PreReforma", "valdataContPreSPB")).intValue();
            anoPriPag = new Double(this.modelo3.getVal("PreReforma", "valdataPriPagPreSPB")).intValue();
        }
        if (anoCont >= 2001 || anoPriPag >= 2001) {
            return true;
        }
        return false;
    }

    public double calcColectaDespTA() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        for (int i = 0; i < this.modelo3.getNumContribs(); ++i) {
            Contribuinte cont = this.modelo3.getContribuinte(i);
            if (cont.getIdCont().equals("SPF")) continue;
            this.colectaDespTA1+=this.modelo3.getVal("TribAutonomas", "valDespesaTA1", cont.getIdCont());
            this.colectaDespTA2+=this.modelo3.getVal("TribAutonomas", "valDespesaTA2", cont.getIdCont());
            this.colectaDespTA3+=this.modelo3.getVal("TribAutonomas", "valDespesaTA3", cont.getIdCont());
            this.colectaDespTA4+=this.modelo3.getVal("TribAutonomas", "valDespesaTA4", cont.getIdCont());
            this.colectaDespTA5+=this.modelo3.getVal("TribAutonomas", "valDespesaTA5", cont.getIdCont());
        }
        this.colectaDespTA1*=this.Taxa_DespTA1;
        this.colectaDespTA2*=this.Taxa_DespTA2;
        this.colectaDespTA3*=this.Taxa_DespTA3;
        this.colectaDespTA4*=this.Taxa_DespTA4;
        this.colectaDespTA5*=this.Taxa_DespTA5;
        if (spa.getReside() == '2') {
            this.colectaDespTA1*=this.taxaReducColAcores;
            this.colectaDespTA2*=this.taxaReducColAcores;
            this.colectaDespTA3*=this.taxaReducColAcores;
            this.colectaDespTA4*=this.taxaReducColAcores;
            this.colectaDespTA5*=this.taxaReducColAcores;
        }
        if (spa.getReside() == '3') {
            this.colectaDespTA2*=this.taxaReducColMadeira;
            this.colectaDespTA3*=this.taxaReducColMadeira;
        }
        return this.colectaDespTA1 + this.colectaDespTA2 + this.colectaDespTA3 + this.colectaDespTA4 + this.colectaDespTA5;
    }

    public double calcQuoRendAnosAnter() {
        double rendAntASPA = this.modelo3.getVal("RendAnosAnter", "valrendAntASPA");
        double rendAntASPB = this.modelo3.getVal("RendAnosAnter", "valrendAntASPB");
        int anosAntASPA = new Double(this.modelo3.getVal("RendAnosAnter", "anosrendAntASPA")).intValue();
        int anosAntASPB = new Double(this.modelo3.getVal("RendAnosAnter", "anosrendAntASPB")).intValue();
        if (anosAntASPA > 4) {
            anosAntASPA = 4;
        }
        if (anosAntASPB > 4) {
            anosAntASPB = 4;
        }
        double rendAntHSPA = this.modelo3.getVal("RendAnosAnter", "valrendAntHSPA");
        double rendAntHSPB = this.modelo3.getVal("RendAnosAnter", "valrendAntHSPB");
        int anosAntHSPA = new Double(this.modelo3.getVal("RendAnosAnter", "anosrendAntHSPA")).intValue();
        int anosAntHSPB = new Double(this.modelo3.getVal("RendAnosAnter", "anosrendAntHSPB")).intValue();
        if (anosAntHSPA > 4) {
            anosAntHSPA = 4;
        }
        if (anosAntHSPB > 4) {
            anosAntHSPB = 4;
        }
        double tempQuo = 0.0;
        double rendBrutoASPA = 0.0;
        double dedEspecASPA = 0.0;
        double rendLiqASPA = 0.0;
        double rendBrutoASPB = 0.0;
        double dedEspecASPB = 0.0;
        double rendLiqASPB = 0.0;
        double rendBrutoHSPA = 0.0;
        double dedEspecHSPA = 0.0;
        double rendLiqHSPA = 0.0;
        double rendBrutoHSPB = 0.0;
        double dedEspecHSPB = 0.0;
        double rendLiqHSPB = 0.0;
        for (int i = 0; i < this.modelo3.getNumContribs(); ++i) {
            if (this.modelo3.getContribuinte(i).getIdCont().equals("SPA")) {
                rendBrutoASPA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
                rendLiqASPA = rendBrutoASPA < (dedEspecASPA = this.calcDeducEspecifica(this.modelo3.getContribuinte(i))) ? 0.0 : rendBrutoASPA - dedEspecASPA;
                rendBrutoHSPA = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
                dedEspecHSPA = this.calcDeducEspecificaH(this.modelo3.getContribuinte(i));
                double d = rendLiqHSPA = rendBrutoHSPA < dedEspecHSPA ? 0.0 : rendBrutoHSPA - dedEspecHSPA;
            }
            if (!this.modelo3.getContribuinte(i).getIdCont().equals("SPB")) continue;
            rendBrutoASPB = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            rendLiqASPB = rendBrutoASPB < (dedEspecASPB = this.calcDeducEspecifica(this.modelo3.getContribuinte(i))) ? 0.0 : rendBrutoASPB - dedEspecASPB;
            rendBrutoHSPB = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
            dedEspecHSPB = this.calcDeducEspecificaH(this.modelo3.getContribuinte(i));
            rendLiqHSPB = rendBrutoHSPB < dedEspecHSPB ? 0.0 : rendBrutoHSPB - dedEspecHSPB;
        }
        if (rendAntASPA > 0.0 && anosAntASPA > 1 && rendBrutoASPA > 0.0 && rendLiqASPA > 0.0) {
            tempQuo+=rendAntASPA / rendBrutoASPA * rendLiqASPA * (double)(anosAntASPA - 1) / (double)anosAntASPA;
        }
        if (rendAntASPB > 0.0 && anosAntASPB > 1 && rendBrutoASPB > 0.0 && rendLiqASPB > 0.0) {
            tempQuo+=rendAntASPB / rendBrutoASPB * rendLiqASPB * (double)(anosAntASPB - 1) / (double)anosAntASPB;
        }
        if (rendAntHSPA > 0.0 && anosAntHSPA > 1 && rendBrutoHSPA > 0.0 && rendLiqHSPA > 0.0) {
            tempQuo+=rendAntHSPA / rendBrutoHSPA * rendLiqHSPA * (double)(anosAntHSPA - 1) / (double)anosAntHSPA;
        }
        if (rendAntHSPB > 0.0 && anosAntHSPB > 1 && rendBrutoHSPB > 0.0 && rendLiqHSPB > 0.0) {
            tempQuo+=rendAntHSPB / rendBrutoHSPB * rendLiqHSPB * (double)(anosAntHSPB - 1) / (double)anosAntHSPB;
        }
        return tempQuo;
    }

    @Override
    public double calcRendBrutoCatA(Contribuinte cont) {
        double rba = 0.0;
        double rb50 = 0.0;
        double rbtot = 0.0;
        if (cont.getIdCont().equals("SPA")) {
            rba = this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont());
            rba-=this.modelo3.getVal("RenTrabDep", "valrendBruto", "SPF");
        } else {
            rba = this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont());
        }
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
            rba = rba + this.modelo3.getVal("RendIsentos", "cod401", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod402", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod404", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod405", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod406", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod407", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod409", cont.getIdCont());
        }
        if (this.usarRegrasCatA(cont)) {
            rba+=this.valorRendCatBTribViaCatA(cont);
        }
        rba+=this.getDifIsento404(cont);
        if (this.prereformaIsCatA(cont)) {
            if (cont.getIdCont().equals("SPA")) {
                rba+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
            }
            if (cont.getIdCont().equals("SPB")) {
                rba+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
            }
        }
        if (!cont.isDeficiente()) {
            return rba;
        }
        if (cont.getIdCont().equals("D1") || cont.getIdCont().equals("D2") || cont.getIdCont().equals("D3")) {
            rbtot = rba;
        } else if (cont.getGrauDef() == '1' || cont.getGrauDef() == '2') {
            rb50 = 0.2 * rba;
            rbtot = rb50 > this.CatA_Rb_Def_60_80 ? rba - this.CatA_Rb_Def_60_80 : rba - rb50;
        }
        return rbtot;
    }

    @Override
    public double calcDeducEspecifica(Contribuinte cont) {
        double dedparc = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (spa.getReside() == '4') {
            return 0.0;
        }
        dedparc = this.calcDeducao1Especifica(cont) + this.modelo3.getVal("RenTrabDep", "valindmnRescUni", cont.getIdCont()) + this.modelo3.getVal("RenTrabDep", "valseguDesg", cont.getIdCont()) + this.calcDeducaoQuotizaA(cont);
        return dedparc;
    }

    @Override
    public double calcDeducao1Especifica(Contribuinte cont) {
        double ded1Especifica = 0.0;
        double dedaux1 = 0.0;
        double dedaux2 = 0.0;
        double dedaux3 = 0.0;
        double dedaux4 = 0.0;
        if (this.modelo3.getVal("RenTrabDep", "valquotizOP", cont.getIdCont()) == 0.0) {
            if (!cont.isDeficiente()) {
                dedaux1 = cont.getIdCont().equals("SPA") ? this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont()) - this.modelo3.getVal("RenTrabDep", "valrendBruto", "SPF") : this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont());
                if (this.prereformaIsCatA(cont)) {
                    if (cont.getIdCont().equals("SPA")) {
                        dedaux1+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
                    }
                    if (cont.getIdCont().equals("SPB")) {
                        dedaux1+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
                    }
                }
                if (this.usarRegrasCatA(cont)) {
                    dedaux1+=this.valorRendCatBTribViaCatA(cont);
                }
                dedaux2 = (dedaux1+=this.getDifIsento404(cont)) > this.CatA_Ded1_Sem_QOPeDFP_Valor_NDef ? this.CatA_Ded1_Sem_QOPeDFP_Valor_NDef : dedaux1;
            } else {
                dedaux1 = cont.getIdCont().equals("SPA") ? this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont()) - this.modelo3.getVal("RenTrabDep", "valrendBruto", "SPF") : this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont());
                if (this.usarRegrasCatA(cont)) {
                    dedaux1+=this.valorRendCatBTribViaCatA(cont);
                }
                dedaux2 = (dedaux1+=this.getDifIsento404(cont)) > this.CatA_Ded1_Sem_QOPeDFP_Valor_Def ? this.CatA_Ded1_Sem_QOPeDFP_Valor_Def : dedaux1;
            }
        } else {
            dedaux1 = cont.getIdCont().equals("SPA") ? this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont()) - this.modelo3.getVal("RenTrabDep", "valrendBruto", "SPF") : this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont());
            if (this.prereformaIsCatA(cont)) {
                if (cont.getIdCont().equals("SPA")) {
                    dedaux1+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
                }
                if (cont.getIdCont().equals("SPB")) {
                    dedaux1+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
                }
            }
            if (dedaux1 > this.CatA_Ded1_Sem_QOPeDFP_Valor_NDef) {
                dedaux1 = this.CatA_Ded1_Sem_QOPeDFP_Valor_NDef;
            }
            dedaux4 = this.calcRBAnexoB(cont);
            dedaux4+=this.calcRBAnexoProC(cont);
            dedaux4+=this.calcRBAnexoAgrC(cont);
            dedaux4+=this.calcRBAnexoProD(cont);
            dedaux2 = (dedaux1+=this.calcQDFP(cont)) > this.CatA_Ded1_Com_QOPouDFP_Valor_NDef && (dedaux4+=this.calcRBAnexoAgrD(cont)) == 0.0 ? this.CatA_Ded1_Com_QOPouDFP_Valor_NDef : (dedaux1 <= this.CatA_Ded1_Com_QOPouDFP_Valor_NDef && dedaux4 == 0.0 ? dedaux1 : dedaux1 - this.calcQDFP(cont));
        }
        dedaux3 = this.calcCSS(cont);
        ded1Especifica = dedaux2 > dedaux3 ? dedaux2 : dedaux3;
        return ded1Especifica;
    }

    @Override
    public double calcDeducaoQuotizaA(Contribuinte cont) {
        double ded3Quotiza = 0.0;
        double ded3valquot = this.modelo3.getVal("RenTrabDep", "valquotizSind", cont.getIdCont());
        double ded3aux1 = 0.0;
        double ded3rba = this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont());
        ded3aux1 = this.Taxa_Quotz_Sindical * ded3rba;
        ded3Quotiza = (ded3valquot*=1.5) > ded3aux1 ? ded3aux1 : ded3valquot;
        return ded3Quotiza;
    }

    @Override
    public double calcCSS(Contribuinte cont) {
        double dedCSSaux1 = 0.0;
        double dedContObSS = 0.0;
        if (cont.getIdCont().equals("SPA")) {
            dedContObSS = this.modelo3.getVal("RenTrabDep", "valcontObrigSS", cont.getIdCont());
            dedContObSS-=this.modelo3.getVal("RenTrabDep", "valcontObrigSS", "SPF");
        } else {
            dedContObSS = this.modelo3.getVal("RenTrabDep", "valcontObrigSS", cont.getIdCont());
        }
        dedCSSaux1 = dedContObSS;
        return dedCSSaux1;
    }

    @Override
    public double calcQDFP(Contribuinte cont) {
        double dedquotizOP;
        double dedQDFPaux1 = 0.0;
        dedQDFPaux1 = dedquotizOP = this.modelo3.getVal("RenTrabDep", "valquotizOP", cont.getIdCont());
        return dedQDFPaux1;
    }

    @Override
    public double calcRendLiqCatH() {
        double resH = 0.0;
        double resparcH = 0.0;
        double resparcH1 = 0.0;
        double resparcH2 = 0.0;
        for (int j = 0; j < this.modelo3.getNumContribs(); ++j) {
            resparcH1 = this.calcRendBrutoCatH(this.modelo3.getContribuinte(j));
            resparcH = resparcH1 < (resparcH2 = this.calcDeducEspecificaH(this.modelo3.getContribuinte(j))) ? 0.0 : resparcH1 - resparcH2;
            resH+=resparcH;
        }
        return resH;
    }

    @Override
    public double calcRendBrutoCatH(Contribuinte cont) {
        double rbh = 0.0;
        double rtvh = this.modelo3.getVal("Pensoes", "valrendTempVit", cont.getIdCont());
        double palh = this.modelo3.getVal("Pensoes", "valrendPensAlim", cont.getIdCont());
        double pensSobr = this.modelo3.getVal("Pensoes", "PSobrev", cont.getIdCont());
        double rh30 = 0.0;
        double rhaux1 = 0.0;
        double rhaux2 = 0.0;
        double rhtot = 0.0;
        if (cont.getIdCont().equals("SPA")) {
            rbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", cont.getIdCont());
            rbh-=this.modelo3.getVal("Pensoes", "valrendBrutoCatH", "SPF");
        } else {
            rbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", cont.getIdCont());
        }
        if (!this.prereformaIsCatA(cont)) {
            if (cont.getIdCont().equals("SPA")) {
                rbh+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
            }
            if (cont.getIdCont().equals("SPB")) {
                rbh+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
            }
        }
        rh30 = 0.2 * (rbh + this.modelo3.getVal("Pensoes", "valrendTempVit", cont.getIdCont()) + this.modelo3.getVal("Pensoes", "valrendPensAlim", cont.getIdCont()) + this.modelo3.getVal("Pensoes", "PSobrev", cont.getIdCont()));
        if (!cont.isDeficiente()) {
            rhtot = rbh + rtvh;
            rhtot = rhtot + palh + pensSobr;
            return rhtot;
        }
        if (cont.getIdCont().equals("D1") || cont.getIdCont().equals("D2") || cont.getIdCont().equals("D3")) {
            rhtot = rbh + rtvh;
            return rhtot+=palh;
        }
        rhaux1 = rh30 > this.CatH_RB_NDefFA_60_80 ? this.CatH_RB_NDefFA_60_80 : rh30;
        rhtot = rbh + rtvh + palh - rhaux1;
        return rhtot;
    }

    @Override
    public double calcDeducEspecificaH(Contribuinte cont) {
        double dedparcH = 0.0;
        dedparcH = this.calcDeducao1EspecificaH(cont) + this.calcDeducaoQuotizaH(cont);
        return dedparcH;
    }

    @Override
    public double calcDeducao1EspecificaH(Contribuinte cont) {
        double ded1EspecificaH = 0.0;
        double dedrbh = 0.0;
        double dedHaux1 = 0.0;
        double dedHaux2 = 0.0;
        if (cont.getIdCont().equals("SPA")) {
            dedrbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", cont.getIdCont());
            dedrbh = dedrbh + this.modelo3.getVal("Pensoes", "valrendPensAlim", cont.getIdCont()) + this.modelo3.getVal("Pensoes", "PSobrev", cont.getIdCont());
            dedrbh-=this.modelo3.getVal("Pensoes", "valrendBrutoCatH", "SPF");
        } else {
            dedrbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", cont.getIdCont());
            dedrbh = dedrbh + this.modelo3.getVal("Pensoes", "valrendPensAlim", cont.getIdCont()) + this.modelo3.getVal("Pensoes", "PSobrev", cont.getIdCont());
        }
        if (!this.prereformaIsCatA(cont)) {
            if (cont.getIdCont().equals("SPA")) {
                dedrbh+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
            }
            if (cont.getIdCont().equals("SPB")) {
                dedrbh+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
            }
        }
        dedHaux1 = dedrbh - this.SPM;
        dedHaux2 = 0.15 * dedHaux1;
        ded1EspecificaH = !cont.isDeficiente() ? (dedrbh > this.SPM ? (this.CatH_Ded1_Ndef - dedHaux2 > 0.0 ? this.CatH_Ded1_Ndef - dedHaux2 : 0.0) : (dedrbh > this.CatH_Ded1_Ndef ? this.CatH_Ded1_Ndef : dedrbh)) : (dedrbh > this.SPM ? (this.CatH_Ded1_Def - dedHaux2 > 0.0 ? this.CatH_Ded1_Def - dedHaux2 : 0.0) : (dedrbh > this.CatH_Ded1_Def ? this.CatH_Ded1_Def : dedrbh));
        return ded1EspecificaH;
    }

    @Override
    public double calcDeducaoQuotizaH(Contribuinte cont) {
        double dedHQuotiza = 0.0;
        double dedHvalquot = this.modelo3.getVal("Pensoes", "valquotizSindCatH", cont.getIdCont());
        double dedHaux1 = 0.0;
        double dedHrbh = 0.0;
        dedHrbh = cont.getIdCont().equals("SPA") ? this.modelo3.getVal("Pensoes", "valrendBrutoCatH", cont.getIdCont()) : this.modelo3.getVal("Pensoes", "valrendBrutoCatH", cont.getIdCont());
        dedHaux1 = this.Taxa_Quotz_Sindical * dedHrbh;
        dedHQuotiza = (dedHvalquot*=1.5) > dedHaux1 ? dedHaux1 : dedHvalquot;
        return dedHQuotiza;
    }

    @Override
    public double calcRendLiqCatE() {
        double res = this.calcRendDeclaradosE();
        return res;
    }

    @Override
    public double calcRendDeclaradosE() {
        double totalCapitais = this.modelo3.getVal("Capitais", "valJurosDepositos") + this.modelo3.getVal("Capitais", "valJurosPremios") + this.modelo3.getVal("Capitais", "valJurosSuprimento") + this.modelo3.getVal("Capitais", "valLucrosAdianta") + this.modelo3.getVal("Capitais", "valoutrosCapitais1") + this.modelo3.getVal("Capitais", "valoutrosCapitais2");
        return totalCapitais;
    }

    public double calcSublocacao() {
        double valSublocacao = 0.0;
        valSublocacao = this.modelo3.getVal("Prediais", "valrendRecebSub") - this.modelo3.getVal("Prediais", "valrendPagaSenhor");
        if (valSublocacao <= 0.0) {
            return 0.0;
        }
        return valSublocacao;
    }

    @Override
    public double calcRendLiqCatF() {
        double res = 0.0;
        double resparc = 0.0;
        double resAnosAntF = this.modelo3.getVal("Prediais", "valperdasAnosAntF");
        res = this.modelo3.getVal("Prediais", "valtotalRendas") - this.modelo3.getVal("Prediais", "valdespManutencao") - this.modelo3.getVal("Prediais", "valdespConservacao") - this.modelo3.getVal("Prediais", "valtaxasAutarq") - this.modelo3.getVal("Prediais", "valcontribAutarq") - this.modelo3.getVal("Prediais", "valdespCondominio") + this.calcSublocacao();
        if (res > 0.0) {
            resparc = res - resAnosAntF;
            if (resparc < 0.0) {
                return 0.0;
            }
            return resparc;
        }
        return 0.0;
    }

    public double calcReinvParcImoveis(double totRealiza, double tempMaisVal) {
        double valReinv;
        double tempResult = 0.0;
        double valDivida = this.modelo3.getVal("MaisValias", "valdividEmp");
        if (totRealiza - valDivida - (valReinv = this.modelo3.getVal("MaisValias", "valreinvParc")) <= 0.0 || totRealiza - valDivida <= 0.0 || tempMaisVal <= 0.0) {
            return 0.0;
        }
        tempResult = tempMaisVal * (totRealiza - valDivida - valReinv) / (totRealiza - valDivida);
        return tempResult;
    }

    @Override
    public double calcRendLiqCatG() {
        double res = 0.0;
        double resA = 0.0;
        double resB = 0.0;
        double resC = 0.0;
        resA = this.Taxa_Trib_Mais_Valias * this.calcMaisValBensImoveis();
        resA+=this.Taxa_Trib_Mais_Valias * this.calcMaisValPropIntelect();
        if ((resA+=this.Taxa_Trib_Mais_Valias * this.calcMaisValPosContratua()) < 0.0) {
            resA = 0.0;
        }
        if ((resB = this.calcMaisValPartesSociais()) < 0.0) {
            resB = 0.0;
        }
        if ((res = resA + resB + (resC+=this.calcIncremPatrimon())) > 0.0) {
            return res;
        }
        return 0.0;
    }

    @Override
    public double calcMaisValBensImoveis() {
        double res = 0.0;
        double valorTotal = 0.0;
        double resparc = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        RubricasMaisValia bensImoveis = (RubricasMaisValia)this.modelo3.getRubricas("BensImoveis");
        for (int i = 0; i < bensImoveis.getNumRubricas(); ++i) {
            RubricaMaisValia rmv = (RubricaMaisValia)bensImoveis.getRubrica(i);
            if (i < 4) {
                if (!rmv.isPreenchida()) continue;
                valorTotal = rmv.getValRealizacao();
                resparc = rmv.getMaisValiaBemImovel(((SPA)this.modelo3.getContribuinte("SPA")).getAnoSim());
                if (Integer.toString(i + 1).equals(((MaisValias)this.modelo3.getRubricas("MaisValias")).getBemAlienado())) {
                    res+=this.calcReinvParcImoveis(valorTotal, resparc);
                    continue;
                }
                res+=resparc;
                continue;
            }
            if (!rmv.isPreenchidaRec() || spa.getEnglobImvG() != '1') continue;
            valorTotal = rmv.getValRealizacaoRec();
            resparc = rmv.getMaisValiaBemImovelRec(((SPA)this.modelo3.getContribuinte("SPA")).getAnoSim());
            if (Integer.toString(i + 1).equals(((MaisValias)this.modelo3.getRubricas("MaisValias")).getBemAlienado())) {
                res+=this.calcReinvParcImoveis(valorTotal, resparc);
                continue;
            }
            res+=resparc;
        }
        return res;
    }

    @Override
    public double calcMaisValPropIntelect() {
        double res = 0.0;
        double totalRealiza = this.modelo3.getVal("MaisValias", "valtotRealiPropInt");
        double totalAquisic = this.modelo3.getVal("MaisValias", "valtotAquisPropInt");
        double totalDespEnc = this.modelo3.getVal("MaisValias", "valtotDespePropInt");
        res = totalRealiza - totalAquisic - totalDespEnc;
        return res;
    }

    @Override
    public double calcMaisValPosContratua() {
        double res = 0.0;
        double totalRealiza = this.modelo3.getVal("MaisValias", "valtotRealiPosCont");
        double totalAquisic = this.modelo3.getVal("MaisValias", "valtotAquisPosCont");
        res = totalRealiza - totalAquisic;
        return res;
    }

    @Override
    public double calcMaisValPartesSociais() {
        double res = 0.0;
        double warrants = this.modelo3.getVal("OpFinancei", "vali4");
        double futOpcoes = this.modelo3.getVal("OpFinancei", "valoutros");
        double certificados = this.modelo3.getVal("OpFinancei", "valcertif");
        double totalRealiza = this.modelo3.getVal("MaisValias", "valtotRealiPartSoc");
        double totalAquisic = this.modelo3.getVal("MaisValias", "valtotAquisPartSoc");
        double totalDespEnc = this.modelo3.getVal("MaisValias", "valtotDespePartSoc");
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (spa.getEnglobG() == '1') {
            res = totalRealiza - totalAquisic - totalDespEnc;
            if (spa.getReside() != '4') {
                res = res <= 500.0 ? 0.0 : (res-=500.0);
            }
            res = res + warrants + futOpcoes + certificados;
        }
        return res;
    }

    @Override
    public double calcIncremPatrimon() {
        double res = 0.0;
        double indemnizDanos = this.modelo3.getVal("IncrePatrim", "valIndemDanos");
        double obrigNConcorr = this.modelo3.getVal("IncrePatrim", "valImpNConc");
        res = indemnizDanos + obrigNConcorr;
        return res;
    }

    @Override
    public double calcIncremPatrimonRF() {
        double res = 0.0;
        double indemnizDanosRF = this.modelo3.getVal("IncrePatrim", "valIndemDanosRF");
        double obrigNConcorrRF = this.modelo3.getVal("IncrePatrim", "valImpNConcRF");
        res = indemnizDanosRF + obrigNConcorrRF;
        return res;
    }

    @Override
    public double calcRBLimAgr() {
        double res = 0.0;
        int i = 0;
        int j = 0;
        double resauxA = 0.0;
        double resaux1B = 0.0;
        double resaux2B = 0.0;
        double resaux2ProB = 0.0;
        double resaux2AgrB = 0.0;
        double resaux3B = 0.0;
        double resaux3ProB = 0.0;
        double resaux3AgrB = 0.0;
        double resauxH = 0.0;
        double rCatA = 0.0;
        double rCatH = 0.0;
        double rCatB = 0.0;
        double rCatE = this.rendLiquiCategE;
        double rCatF = this.modelo3.getVal("Prediais", "valtotalRendas");
        double rCatG = this.getValorMaisVal();
        if (rCatF < 0.0) {
            rCatF = 0.0;
        }
        while (i < this.modelo3.getNumContribs()) {
            resauxA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            rCatA+=resauxA;
            resauxH = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
            rCatH+=resauxH;
            ++i;
        }
        while (j < this.modelo3.getNumContribs() - 1) {
            resaux1B = this.calcRBAnexoB(this.modelo3.getContribuinte(j));
            rCatB+=resaux1B;
            resaux2ProB+=this.calcRBAnexoProC(this.modelo3.getContribuinte(j));
            resaux2AgrB+=this.calcRBAnexoAgrC(this.modelo3.getContribuinte(j));
            resaux3ProB+=this.calcRBAnexoProD(this.modelo3.getContribuinte(j));
            resaux3AgrB+=this.calcRBAnexoAgrD(this.modelo3.getContribuinte(j));
            ++j;
        }
        if (resaux2ProB > 0.0) {
            rCatB+=resaux2ProB;
        }
        if (resaux2AgrB > 0.0) {
            rCatB+=resaux2AgrB;
        }
        if (resaux3ProB > 0.0) {
            rCatB+=resaux3ProB;
        }
        if (resaux3AgrB > 0.0) {
            rCatB+=resaux3AgrB;
        }
        res = rCatA + rCatH + rCatB + rCatE + rCatF + rCatG;
        return res;
    }

    @Override
    public char calcEnquadramento(Contribuinte cont) {
        int enquadra = 49;
        double rendBrutoB = 0.0;
        double totalRendims = 0.0;
        double totalRendimsSP = 0.0;
        int i = 0;
        int j = 0;
        double resauxA = 0.0;
        double resaux1B = 0.0;
        double resaux2B = 0.0;
        double resaux2ProB = 0.0;
        double resaux2AgrB = 0.0;
        double resaux3B = 0.0;
        double resaux3ProB = 0.0;
        double resaux3AgrB = 0.0;
        double resauxH = 0.0;
        double rCatA = 0.0;
        double rCatH = 0.0;
        double rCatB = 0.0;
        double rCatE = this.rendLiquiCategE;
        double rCatF = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        rCatF = spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendas") + this.modelo3.getVal("Prediais", "valtotalRendasImv") : this.modelo3.getVal("Prediais", "valtotalRendas");
        double rCatG = this.getValorMaisVal();
        double rIsent = this.rendimIsentSE;
        double rAcres = this.acrescimosRendLiq;
        double valVendas = 0.0;
        if (rCatF < 0.0) {
            rCatF = 0.0;
        }
        while (i < this.modelo3.getNumContribs()) {
            resauxA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            rCatA+=resauxA;
            resauxH = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
            rCatH+=resauxH;
            ++i;
        }
        while (j < this.modelo3.getNumContribs() - 1) {
            resaux1B = this.calcRBAnexoB(this.modelo3.getContribuinte(j));
            rCatB+=resaux1B;
            resaux2ProB+=this.calcRBAnexoProC(this.modelo3.getContribuinte(j));
            resaux2AgrB+=this.calcRBAnexoAgrC(this.modelo3.getContribuinte(j));
            resaux3ProB+=this.calcRBAnexoProD(this.modelo3.getContribuinte(j));
            resaux3AgrB+=this.calcRBAnexoAgrD(this.modelo3.getContribuinte(j));
            ++j;
        }
        if (resaux2ProB > 0.0) {
            rCatB+=resaux2ProB;
        }
        if (resaux2AgrB > 0.0) {
            rCatB+=resaux2AgrB;
        }
        if (resaux3ProB > 0.0) {
            rCatB+=resaux3ProB;
        }
        if (resaux3AgrB > 0.0) {
            rCatB+=resaux3AgrB;
        }
        totalRendimsSP = this.calcRendBrutoCatA(cont) + this.calcRendBrutoCatH(cont) + this.calcRBAnexoB(cont);
        if (this.calcRBAnexoProC(cont) > 0.0) {
            totalRendimsSP+=this.calcRBAnexoProC(cont);
        }
        if (this.calcRBAnexoAgrC(cont) > 0.0) {
            totalRendimsSP+=this.calcRBAnexoAgrC(cont);
        }
        if (this.calcRBAnexoProD(cont) > 0.0) {
            totalRendimsSP+=this.calcRBAnexoProD(cont);
        }
        if (this.calcRBAnexoAgrD(cont) > 0.0) {
            totalRendimsSP+=this.calcRBAnexoAgrD(cont);
        }
        totalRendims = rCatA + rCatH + rCatB + rCatE + rCatF + rCatG + rIsent + rAcres;
        rendBrutoB = this.modelo3.getVal("CategoriaB", "RendimProB", cont.getIdCont()) + this.modelo3.getVal("CategoriaB", "RendimAgrB", cont.getIdCont()) + this.getValRendIsento("cod403", cont) + this.getValRendIsento("cod408", cont) + this.getValRendIsento("cod410", cont);
        valVendas = this.modelo3.getVal("RendimAgrB", "valVendasMercadoA", cont.getIdCont());
        enquadra = cont.getnaturRendB() == '2' && (rendBrutoB <= 0.5 * totalRendimsSP || rendBrutoB == totalRendimsSP) ? 50 : (rendBrutoB > 0.5 * totalRendims ? 49 : ((valVendas+=this.modelo3.getVal("RendimProB", "valVendasMercadoP", cont.getIdCont())) > 0.0 ? (rendBrutoB > this.CatB_RS1_Limite_Valor ? 49 : 51) : (rendBrutoB > this.CatB_RS_Limite_Valor ? 49 : 51)));
        cont.setenquadramB((char)enquadra);
        return enquadra;
    }

    @Override
    public double calcRBAnexoB(Contribuinte cont) {
        double rbb = 0.0;
        rbb = this.modelo3.getVal("CategoriaB", "RendimProB", cont.getIdCont()) + this.modelo3.getVal("CategoriaB", "RendimAgrB", cont.getIdCont());
        return rbb;
    }

    @Override
    public double calcRBAnexoProC(Contribuinte cont) {
        double rbc1 = 0.0;
        rbc1 = this.modelo3.getVal("AnexoC", "valLucroPrejProC", cont.getIdCont()) + this.modelo3.getVal("AnexoC", "valLucroPrejActFinC", cont.getIdCont());
        return rbc1;
    }

    @Override
    public double calcRBAnexoAgrC(Contribuinte cont) {
        double rbc1 = 0.0;
        rbc1 = this.modelo3.getVal("AnexoC", "valLucroPrejAgrC", cont.getIdCont());
        return rbc1;
    }

    @Override
    public double calcRBAnexoProD(Contribuinte cont) {
        double rbd1 = 0.0;
        rbd1 = this.modelo3.getVal("AnexoD", "valLucroPrejProD", cont.getIdCont());
        return rbd1;
    }

    @Override
    public double calcRBAnexoAgrD(Contribuinte cont) {
        double rbd1 = 0.0;
        rbd1 = this.modelo3.getVal("AnexoD", "valLucroPrejAgrD", cont.getIdCont());
        return rbd1;
    }

    @Override
    public double calcRendLiqCatB() {
        double resAgr = 0.0;
        double resPro = 0.0;
        double resConcoAgr = 0.0;
        double resConcoPro = 0.0;
        double resparcBAgr = 0.0;
        double resparcBPro = 0.0;
        double resparcBAgr1 = 0.0;
        double resparcBPro1 = 0.0;
        double resparcCAgr = 0.0;
        double resparcCPro = 0.0;
        double resparcDAgr = 0.0;
        double resparcDPro = 0.0;
        double resTotB = 0.0;
        double res = 0.0;
        double limite1 = this.CatB_Min_Tributacao_Valor;
        double coef_corr_def = 1.0;
        int enquadra = 49;
        double resValDecl = this.calcRBLimAgr();
        double resparcBAgr1tot = 0.0;
        for (int i = 0; i < this.modelo3.getNumContribs() - 1; ++i) {
            resparcBAgr = 0.0;
            resparcBPro = 0.0;
            resparcBAgr1 = 0.0;
            resparcBPro1 = 0.0;
            resTotB = 0.0;
            resparcCAgr = 0.0;
            resparcCPro = 0.0;
            resparcDAgr = 0.0;
            resparcDPro = 0.0;
            double totalRend = this.modelo3.getVal("CategoriaB", "RendimAgrB", this.modelo3.getContribuinte(i).getIdCont()) + this.modelo3.getVal("CategoriaB", "RendimProB", this.modelo3.getContribuinte(i).getIdCont());
            this.Coef_Corr_Def_CatB = this.calcCoefCorrDef(this.modelo3.getContribuinte(i), totalRend);
            if (resValDecl <= this.CatB_Limite_Agricola) {
                resparcBAgr1 = 0.0;
            } else if (!this.usarRegrasCatA(this.modelo3.getContribuinte(i))) {
                resparcBAgr1 = this.calcRendLiqAnexoBAgr(this.modelo3.getContribuinte(i));
            }
            if (!this.usarRegrasCatA(this.modelo3.getContribuinte(i))) {
                resparcBPro1 = this.calcRendLiqAnexoBPro(this.modelo3.getContribuinte(i));
            }
            if ((enquadra = (int)this.calcEnquadramento(this.modelo3.getContribuinte(i))) == 49) {
                resTotB = resparcBAgr1 + resparcBPro1;
                if (resTotB < limite1) {
                    if (resparcBAgr1 > 0.0) {
                        resparcBAgr = resparcBAgr1 * limite1 / resTotB;
                    }
                    resConcoAgr+=resparcBAgr;
                    if (resparcBPro1 > 0.0) {
                        resparcBPro = resparcBPro1 * limite1 / resTotB;
                    }
                    resConcoPro+=resparcBPro;
                } else if (resparcBAgr1 > 0.0 && resparcBPro1 > 0.0) {
                    resparcBAgr = resparcBAgr1;
                    resparcBPro = resparcBPro1;
                    resConcoAgr+=limite1 / 2.0;
                    resConcoPro+=limite1 / 2.0;
                } else if (resparcBAgr1 > 0.0 && resparcBPro1 == 0.0) {
                    resparcBAgr = resparcBAgr1;
                    resConcoAgr+=limite1;
                } else if (resparcBAgr1 == 0.0 && resparcBPro1 > 0.0) {
                    resparcBPro = resparcBPro1;
                    resConcoPro+=limite1;
                }
            } else {
                resparcBAgr = resparcBAgr1;
                resparcBPro = resparcBPro1;
            }
            resparcCAgr = resValDecl <= this.CatB_Limite_Agricola ? 0.0 : this.calcRendLiqAnexoCAgr(this.modelo3.getContribuinte(i));
            resparcCPro = this.calcRendLiqAnexoCPro(this.modelo3.getContribuinte(i));
            resparcDAgr = resValDecl <= this.CatB_Limite_Agricola ? 0.0 : this.calcRendLiqAnexoDAgr(this.modelo3.getContribuinte(i));
            resparcDPro = this.calcRendLiqAnexoDPro(this.modelo3.getContribuinte(i));
            resAgr = resAgr + resparcBAgr + resparcCAgr + resparcDAgr;
            resPro = resPro + resparcBPro + resparcCPro + resparcDPro;
            enquadra = 49;
        }
        if (resparcBAgr + resparcCAgr + resparcDAgr < 0.0) {
            resparcBAgr = 0.0;
            resparcCAgr = 0.0;
            resparcDAgr = 0.0;
        }
        if (resparcBPro + resparcCPro + resparcDPro < 0.0) {
            resparcBPro = 0.0;
            resparcCPro = 0.0;
            resparcDPro = 0.0;
        }
        if (resAgr < 0.0) {
            resAgr = 0.0;
        }
        if (resPro < 0.0) {
            resPro = 0.0;
        }
        if (resConcoAgr == 0.0 && resConcoPro == 0.0) {
            res = resAgr <= 0.0 && resPro <= 0.0 ? 0.0 : (resAgr <= 0.0 && resPro > 0.0 ? resPro : (resAgr > 0.0 && resPro <= 0.0 ? resAgr : resAgr + resPro));
        }
        if (resConcoAgr > 0.0 && resConcoPro == 0.0) {
            if (resAgr <= 0.0) {
                resAgr = resConcoAgr;
            }
            res = resAgr > 0.0 && resPro <= 0.0 ? resAgr : resAgr + resPro;
        }
        if (resConcoAgr == 0.0 && resConcoPro > 0.0) {
            if (resPro <= 0.0) {
                resPro = resConcoPro;
            }
            res = resAgr <= 0.0 && resPro > 0.0 ? resPro : resAgr + resPro;
        }
        if (resConcoAgr > 0.0 && resConcoPro > 0.0) {
            if (resAgr <= 0.0) {
                resAgr = resConcoAgr;
            }
            if (resPro <= 0.0) {
                resPro = resConcoPro;
            }
            res = resAgr + resPro;
        }
        if (res < resConcoAgr + resConcoPro) {
            res = resConcoAgr + resConcoPro;
        }
        return res;
    }

    public double calcCoefCorrDef(Contribuinte cont, double Rend) {
        double coef_corr_def = 1.0;
        double tempBenef = 0.0;
        boolean isDef = false;
        if (!(cont.getIdCont().equals("D1") || cont.getIdCont().equals("D2") || cont.getIdCont().equals("D3"))) {
            if (cont.getGrauDef() == '1') {
                tempBenef = this.Benef_Def60_CatB;
                isDef = true;
            }
            if (cont.getGrauDef() == '2') {
                tempBenef = this.Benef_Def80_CatB;
                isDef = true;
            }
            if (isDef) {
                double tempRend = Rend * this.Taxa_Def_CatB;
                double tempParc = tempRend < tempBenef ? tempRend : tempBenef;
                coef_corr_def = Rend > 0.0 ? (Rend - tempParc) / Rend : 0.0;
            }
        }
        return coef_corr_def;
    }

    @Override
    public double calcRendLiqAnexoBAgr(Contribuinte cont) {
        double res = 0.0;
        double resRendimentos = this.modelo3.getVal("CategoriaB", "RendimAgrB", cont.getIdCont());
        double resparcRend = 0.0;
        double resEncargos = 0.0;
        double enc63 = this.modelo3.getVal("DespesaAgrB", "valDeslocacoesAB", cont.getIdCont());
        double encTot = 0.0;
        char enquadra = this.calcEnquadramento(cont);
        if (enquadra == '1') {
            resparcRend = this.Taxa_Vendas * this.modelo3.getVal("RendimAgrB", "valVendasMercadoA", cont.getIdCont()) + this.Taxa_Prest_Servicos * this.modelo3.getVal("RendimAgrB", "valOutrosServicosA", cont.getIdCont());
            res = (resparcRend+=this.Taxa_Vendas * this.modelo3.getVal("RendimAgrB", "valSubsidiosExplA", cont.getIdCont())) * this.Coef_Corr_Def_CatB;
        } else {
            resparcRend = resRendimentos * this.Coef_Corr_Def_CatB;
            resEncargos = this.modelo3.getVal("DespesaAgrB", "valValProfissionalAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valDespRepresentacaoAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valEncViaturaAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valCustosExistAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valOutrosEncgAB", cont.getIdCont());
            if (enc63 > 0.1 * resRendimentos) {
                enc63 = 0.1 * resRendimentos;
            }
            encTot = resEncargos + enc63;
            res = resparcRend - encTot;
        }
        return res*=this.Taxa_Rend_Agricolas_CD;
    }

    @Override
    public double calcRendLiqAnexoBPro(Contribuinte cont) {
        double res = 0.0;
        double resRendimentos = this.modelo3.getVal("CategoriaB", "RendimProB", cont.getIdCont());
        double resEncargos = 0.0;
        double resparcRend = 0.0;
        double enc63 = this.modelo3.getVal("DespesaProB", "valDeslocacoesPB", cont.getIdCont());
        double encTot = 0.0;
        char enquadra = this.calcEnquadramento(cont);
        if (enquadra == '1') {
            resparcRend = this.Taxa_Vendas * this.modelo3.getVal("RendimProB", "valVendasMercadoP", cont.getIdCont());
            resparcRend+=this.Taxa_Vendas * this.modelo3.getVal("RendimProB", "valServicosHotP", cont.getIdCont());
            resparcRend+=this.Taxa_Prest_Servicos * this.modelo3.getVal("RendimProB", "valOutrosServicosP", cont.getIdCont());
            resparcRend+=this.Taxa_Prest_Servicos * this.modelo3.getVal("RendimProB", "valRendimActFinP", cont.getIdCont());
            resparcRend+=this.modelo3.getVal("RendimProB", "valServPrestSocP", cont.getIdCont());
            res = (resparcRend+=this.Taxa_Prest_Servicos_Red * this.modelo3.getVal("RendimProB", "valPropriedadeIntP", cont.getIdCont())) * this.Coef_Corr_Def_CatB;
        } else {
            resparcRend = resRendimentos * this.Coef_Corr_Def_CatB;
            resEncargos = this.modelo3.getVal("DespesaProB", "valValProfissionalPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valDespRepresentacaoPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valEncViaturaPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valCustosExistPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valOutrosEncgPB", cont.getIdCont());
            if (enc63 > 0.1 * resRendimentos) {
                enc63 = 0.1 * resRendimentos;
            }
            encTot = resEncargos + enc63;
            res = resparcRend - encTot;
        }
        return res;
    }

    public boolean usarRegrasCatA(Contribuinte cont) {
        double checkbox = 0.0;
        if (!cont.getIdCont().equals("SPF")) {
            checkbox = this.modelo3.getVal("CategoriaB", "valTributacaoCatA", cont.getIdCont());
        }
        if (checkbox > 0.0) {
            return true;
        }
        return false;
    }

    public double valorRendCatBTribViaCatA(Contribuinte cont) {
        double resRendimentos1 = this.modelo3.getVal("CategoriaB", "RendimAgrB", cont.getIdCont());
        double resRendimentos2 = this.modelo3.getVal("CategoriaB", "RendimProB", cont.getIdCont());
        return resRendimentos1 + resRendimentos2;
    }

    public double calcExcessoRendDefAnxC(Contribuinte cont, double Rend) {
        double tempBenef = 0.0;
        double excesso = 0.0;
        double rendDef = this.modelo3.getVal("AnexoC", "valRendAufDefC", cont.getIdCont());
        if (!(cont.getIdCont().equals("D1") || cont.getIdCont().equals("D2") || cont.getIdCont().equals("D3"))) {
            if (cont.getGrauDef() == '1') {
                tempBenef = this.Benef_Def60_CatB;
            }
            if (cont.getGrauDef() == '2') {
                tempBenef = this.Benef_Def80_CatB;
            }
            excesso = rendDef - tempBenef;
        }
        return excesso > 0.0 ? excesso : 0.0;
    }

    @Override
    public double calcRendLiqAnexoCAgr(Contribuinte cont) {
        double res = 0.0;
        res = this.modelo3.getVal("AnexoC", "valLucroPrejAgrC", cont.getIdCont());
        res+=this.calcExcessoRendDefAnxC(cont, res);
        return res*=this.Taxa_Rend_Agricolas_CD;
    }

    @Override
    public double calcRendLiqAnexoCPro(Contribuinte cont) {
        double res = 0.0;
        res = this.modelo3.getVal("AnexoC", "valLucroPrejProC", cont.getIdCont()) + this.modelo3.getVal("AnexoC", "valLucroPrejActFinC", cont.getIdCont());
        res+=this.calcExcessoRendDefAnxC(cont, res);
        return res;
    }

    @Override
    public double calcRendLiqAnexoDAgr(Contribuinte cont) {
        double res = 0.0;
        res = this.Taxa_Rend_Agricolas_CD * this.modelo3.getVal("AnexoD", "valLucroPrejAgrD", cont.getIdCont());
        return res;
    }

    @Override
    public double calcRendLiqAnexoDPro(Contribuinte cont) {
        double res = 0.0;
        res = this.modelo3.getVal("AnexoD", "valLucroPrejProD", cont.getIdCont());
        return res;
    }

    @Override
    public double calcRLiqActivFinanc(double rendLiquiCategB) {
        double rbAF = 0.0;
        double rlAF = 0.0;
        double rCatB = 0.0;
        double resaux1B = 0.0;
        double resaux2ProB = 0.0;
        double resaux2AgrB = 0.0;
        double resaux3ProB = 0.0;
        double resaux3AgrB = 0.0;
        for (int j = 0; j < this.modelo3.getNumContribs() - 1; ++j) {
            rbAF+=this.calcRBActivFinanc(this.modelo3.getContribuinte(j));
            resaux1B = this.calcRBAnexoB(this.modelo3.getContribuinte(j));
            rCatB+=resaux1B;
            resaux2ProB+=this.calcRBAnexoProC(this.modelo3.getContribuinte(j));
            resaux2AgrB+=this.calcRBAnexoAgrC(this.modelo3.getContribuinte(j));
            resaux3ProB+=this.calcRBAnexoProD(this.modelo3.getContribuinte(j));
            resaux3AgrB+=this.calcRBAnexoAgrD(this.modelo3.getContribuinte(j));
        }
        if (resaux2ProB > 0.0) {
            rCatB+=resaux2ProB;
        }
        if (resaux2AgrB > 0.0) {
            rCatB+=resaux2AgrB;
        }
        if (resaux3ProB > 0.0) {
            rCatB+=resaux3ProB;
        }
        if (resaux3AgrB > 0.0) {
            rCatB+=resaux3AgrB;
        }
        rlAF = rbAF / rCatB * rendLiquiCategB;
        return rlAF;
    }

    @Override
    public double calcRBActivFinanc(Contribuinte cont) {
        double rbaf = 0.0;
        rbaf = this.modelo3.getVal("RendimProB", "valRendimActFinP", cont.getIdCont()) + this.modelo3.getVal("AnexoC", "valLucroPrejActFinC", cont.getIdCont());
        return rbaf;
    }

    @Override
    public double calcColActivFinanc(double valorColecta, double rendimLiquido, double rendLiquiCategB) {
        double valorColActivFinanc = 0.0;
        double rendLiqActFinanc = 0.0;
        rendLiqActFinanc = this.calcRLiqActivFinanc(rendLiquiCategB);
        valorColActivFinanc = valorColecta * (rendLiqActFinanc / rendimLiquido);
        return valorColActivFinanc;
    }

    @Override
    public double calcAcrescRendLiq() {
        double res = 0.0;
        res = this.modelo3.getVal("Acrescimos", "valsegurosRend") + this.modelo3.getVal("Acrescimos", "valPPRRend") + this.modelo3.getVal("Acrescimos", "valPPALevaRend") + this.modelo3.getVal("Acrescimos", "valPPAIncuRend") + this.modelo3.getVal("Acrescimos", "valPPHRend") + this.modelo3.getVal("Acrescimos", "valCoopRend") + this.modelo3.getVal("Acrescimos", "valInobsRend") + this.modelo3.getVal("Acrescimos", "valCondomRend");
        return res;
    }

    @Override
    public double calcAbatimentos() {
        double abatPenJudic;
        double abatimCalcul = 0.0;
        abatimCalcul = abatPenJudic = this.modelo3.getVal("AbatDedColecta", "valpensoesObrigJudic");
        return abatimCalcul;
    }

    public double calcRendIsentSujEng2() {
        double res = 0.0;
        double res1 = 0.0;
        double res2 = 0.0;
        double resparcH = 0.0;
        double resparcE = 0.0;
        double resparcG = 0.0;
        double warrants = this.modelo3.getVal("OpFinancei", "vali4");
        double totalRealiza = this.modelo3.getVal("MaisValias", "valtotRealiPartSoc");
        double totalAquisic = this.modelo3.getVal("MaisValias", "valtotAquisPartSoc");
        double totalDespEnc = this.modelo3.getVal("MaisValias", "valtotDespePartSoc");
        double outroscontrFO = this.modelo3.getVal("OpFinancei", "valoutros");
        double certificados = this.modelo3.getVal("OpFinancei", "valcertif");
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (spa.getEnglobE() == '0' || spa.getDocsE() == '0') {
            resparcE = 0.0;
        }
        if (spa.getEnglobG() == '1') {
            res1 = totalRealiza - totalAquisic - totalDespEnc;
            res1 = res1 + warrants + certificados + outroscontrFO;
        }
        return res1;
    }

    public double getValRendIsento(String idVal, Contribuinte cont) {
        return this.modelo3.getVal("RendIsentos", idVal, cont.getIdCont());
    }

    public double getDifIsento404(Contribuinte cont) {
        double temp404 = 0.0;
        if (!cont.getIdCont().equals("SPF")) {
            temp404 = this.modelo3.getVal("RendIsentos", "cod404", cont.getIdCont());
        }
        return temp404 > this.Lim_Rend_Isent_404_H ? temp404 - this.Lim_Rend_Isent_404_H : 0.0;
    }

    @Override
    public double calcRendIsentSujEng() {
        double tempRend = 0.0;
        for (int i = 1; i <= 10; ++i) {
            String idVal = new String();
            idVal = "cod4" + (i < 10 ? new StringBuilder("0").append(i).toString() : new StringBuilder().append(i).toString());
            int j = 0;
            while (j < this.modelo3.getNumContribs()) {
                Contribuinte cont;
                if ((cont = this.modelo3.getContribuinte(j++)).getIdCont().equals("SPF")) continue;
                if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') continue;
                double auxRend = this.getValRendIsento(idVal, cont);
                if (!(idVal.equals("cod404") && auxRend > this.Lim_Rend_Isent_404_H)) {
                    tempRend+=auxRend;
                    continue;
                }
                tempRend+=this.Lim_Rend_Isent_404_H;
            }
        }
        return tempRend;
    }

    public double calcRetenRendIsentos() {
        double tempReten = 0.0;
        for (int i = 1; i <= 10; ++i) {
            String idVal = new String();
            idVal = "ret4" + (i < 10 ? new StringBuilder("0").append(i).toString() : new StringBuilder().append(i).toString());
            int j = 0;
            while (j < this.modelo3.getNumContribs()) {
                Contribuinte cont;
                if ((cont = this.modelo3.getContribuinte(j++)).getIdCont().equals("SPF")) continue;
                tempReten+=this.getValRendIsento(idVal, cont);
            }
        }
        return tempReten;
    }

    @Override
    public int calcCoefConjugal() {
        int coeficienteConj = 2;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        coeficienteConj = spa.getEstadoCivil() == '1' || spa.getEstadoCivil() == '4' ? 2 : (spa.getObito() == '1' ? 2 : 1);
        return coeficienteConj;
    }

    @Override
    public double calcRCC(double rendDetermtx, double coefConjugal) {
        double valorRendimCConjug = 0.0;
        valorRendimCConjug = rendDetermtx == 0.0 ? 0.0 : rendDetermtx / coefConjugal;
        return valorRendimCConjug;
    }

    @Override
    public double calcTaxaImposto(double valorRCC) {
        double taxaImposto = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (valorRCC <= this.TaxasGerais_Esc2_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc1_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc1_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc1_Madeira_Taxa;
            }
        } else if (valorRCC <= this.TaxasGerais_Esc3_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc2_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc2_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc2_Madeira_Taxa;
            }
        } else if (valorRCC <= this.TaxasGerais_Esc4_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc3_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc3_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc3_Madeira_Taxa;
            }
        } else if (valorRCC <= this.TaxasGerais_Esc5_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc4_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc4_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc4_Madeira_Taxa;
            }
        } else if (valorRCC <= this.TaxasGerais_Esc6_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc5_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc5_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc5_Madeira_Taxa;
            }
        } else if (valorRCC <= this.TaxasGerais_Esc7_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc6_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc6_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc6_Madeira_Taxa;
            }
        } else if (valorRCC > this.TaxasGerais_Esc7_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc7_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc7_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc7_Madeira_Taxa;
            }
        }
        return taxaImposto;
    }

    @Override
    public double calcParcelaAbat(double valorRCC) {
        double parcelaAbater = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (valorRCC <= this.TaxasGerais_Esc2_Min) {
            if (spa.getReside() == '1') {
                parcelaAbater = this.TaxasGerais_Esc1_Cont_Parc_Abater;
            } else if (spa.getReside() == '2') {
                parcelaAbater = this.TaxasGerais_Esc1_Acores_Parc_Abater;
            } else if (spa.getReside() == '3') {
                parcelaAbater = this.TaxasGerais_Esc1_Madeira_Parc_Abater;
            }
        } else if (valorRCC <= this.TaxasGerais_Esc3_Min) {
            if (spa.getReside() == '1') {
                parcelaAbater = this.TaxasGerais_Esc2_Cont_Parc_Abater;
            } else if (spa.getReside() == '2') {
                parcelaAbater = this.TaxasGerais_Esc2_Acores_Parc_Abater;
            } else if (spa.getReside() == '3') {
                parcelaAbater = this.TaxasGerais_Esc2_Madeira_Parc_Abater;
            }
        } else if (valorRCC <= this.TaxasGerais_Esc4_Min) {
            if (spa.getReside() == '1') {
                parcelaAbater = this.TaxasGerais_Esc3_Cont_Parc_Abater;
            } else if (spa.getReside() == '2') {
                parcelaAbater = this.TaxasGerais_Esc3_Acores_Parc_Abater;
            } else if (spa.getReside() == '3') {
                parcelaAbater = this.TaxasGerais_Esc3_Madeira_Parc_Abater;
            }
        } else if (valorRCC <= this.TaxasGerais_Esc5_Min) {
            if (spa.getReside() == '1') {
                parcelaAbater = this.TaxasGerais_Esc4_Cont_Parc_Abater;
            } else if (spa.getReside() == '2') {
                parcelaAbater = this.TaxasGerais_Esc4_Acores_Parc_Abater;
            } else if (spa.getReside() == '3') {
                parcelaAbater = this.TaxasGerais_Esc4_Madeira_Parc_Abater;
            }
        } else if (valorRCC <= this.TaxasGerais_Esc6_Min) {
            if (spa.getReside() == '1') {
                parcelaAbater = this.TaxasGerais_Esc5_Cont_Parc_Abater;
            } else if (spa.getReside() == '2') {
                parcelaAbater = this.TaxasGerais_Esc5_Acores_Parc_Abater;
            } else if (spa.getReside() == '3') {
                parcelaAbater = this.TaxasGerais_Esc5_Madeira_Parc_Abater;
            }
        } else if (valorRCC <= this.TaxasGerais_Esc7_Min) {
            if (spa.getReside() == '1') {
                parcelaAbater = this.TaxasGerais_Esc6_Cont_Parc_Abater;
            } else if (spa.getReside() == '2') {
                parcelaAbater = this.TaxasGerais_Esc6_Acores_Parc_Abater;
            } else if (spa.getReside() == '3') {
                parcelaAbater = this.TaxasGerais_Esc6_Madeira_Parc_Abater;
            }
        } else if (valorRCC > this.TaxasGerais_Esc7_Min) {
            if (spa.getReside() == '1') {
                parcelaAbater = this.TaxasGerais_Esc7_Cont_Parc_Abater;
            } else if (spa.getReside() == '2') {
                parcelaAbater = this.TaxasGerais_Esc7_Acores_Parc_Abater;
            } else if (spa.getReside() == '3') {
                parcelaAbater = this.TaxasGerais_Esc7_Madeira_Parc_Abater;
            }
        }
        return parcelaAbater;
    }

    @Override
    public double calculoDeducoesColecta(double colectaRendimSujeitos, double rendimLiquido, double rendimIsentSE, double acrescimosRendLiq, double colectaGratificacoes, double colectaFuturoOpcoes) {
        double valorDeducoesColecta = 0.0;
        double dedSPDepeAscend = this.calcDedSPDA();
        double dedCreditoImposto = 0.0;
        double dedDupTribInter = 0.0;
        double dedDespesasSaude = this.calcDespSaude5();
        double dedDespesasSaudeOutros = this.calcDespSaudeOutras();
        double dedDespesasEducacao = this.calcDedEducFormProf();
        double dedEncargosLares = this.calcDedEncLares();
        double dedDespEducEncLares = this.calcDedEducLares(dedDespesasEducacao, dedEncargosLares);
        double dedEncargosImoveis = this.calcDedEncImoveis();
        double dedEnergiasRenov = this.calcDedEnergiasRenov();
        double dedEncImovEngRenov = this.calcMaiorDed();
        double dedSegurosVida = this.calcDedSegVida();
        double dedSegurosSaude = this.calcDedSeguroSaude();
        double dedAconselJuridico = this.calcAconselhamento();
        double dedCFI = 0.0;
        double dedPPRPPE = this.calcDedPPRPPE(rendimIsentSE, acrescimosRendLiq, colectaGratificacoes, colectaFuturoOpcoes);
        double dedCPH = this.calcPPHCooperativas();
        double dedPPA = this.calcDeducaoPPA();
        double dedDespesasEducDeficientes = this.calcDedDespEducReabDef();
        double dedSegurosDeficientes = this.calcDedPremiosSegurDef(colectaRendimSujeitos);
        double dedComputadores = this.calcDedComputadores(this.txImposto);
        double dedCooperadores = this.calcDedCooperadores();
        double dedDonativosEntPub = this.calcDedDonatPublic();
        double dedDonativosOutrasEnt = this.calcDedDonativOutr(colectaRendimSujeitos);
        double dedIvaAquiServicos = this.calcIvaAquiServicos();
        double dedSujPassDeficientes = this.calcDedSPDef();
        valorDeducoesColecta = dedSPDepeAscend + dedDespesasSaude + dedDespesasSaudeOutros + dedDespesasEducacao + dedEncargosLares + dedEncImovEngRenov + dedSegurosVida + dedSegurosSaude + dedSujPassDeficientes + dedDespesasEducDeficientes + dedSegurosDeficientes + dedPPRPPE + dedDonativosOutrasEnt + dedComputadores;
        return valorDeducoesColecta;
    }

    @Override
    public double calcDedSPDA() {
        double result = 0.0;
        double resultParcial = 0.0;
        result = this.calcDedSujeitosPassivos();
        result+=this.calcDedDependentes();
        resultParcial = result+=this.calcDedAscendentes();
        return resultParcial;
    }

    @Override
    public double calcDedSujeitosPassivos() {
        double resultSujPass = 0.0;
        double resultSujPassA = 0.0;
        double resultSujPassB = 0.0;
        double resultSujPassF = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        Contribuinte spb = this.modelo3.getContribuinte("SPB");
        Contribuinte spf = this.modelo3.getContribuinte("SPF");
        if (spa.isFamiliaCasado() || spa.getEstadoCivil() == '3') {
            if (spa.getEstadoCivil() == '3') {
                resultSujPassA = spa.isDeficiente() ? this.DC_Familia_Casado_Def : this.DC_Familia_Casado_Ndef;
            } else {
                resultSujPassA = spa.isDeficiente() ? this.DC_Familia_Casado_Def : this.DC_Familia_Casado_Ndef;
                resultSujPassB = spb.isDeficiente() ? this.DC_Familia_Casado_Def : this.DC_Familia_Casado_Ndef;
                if (spa.getObito() == '1') {
                    resultSujPassF = spf.isDeficiente() ? this.DC_Familia_Casado_Def : this.DC_Familia_Casado_Ndef;
                }
            }
        } else if (spa.getEstadoCivil() == '2' && spa.getObito() == '1') {
            resultSujPassA = spa.isDeficiente() ? this.DC_Familia_Casado_Def : this.DC_Familia_Casado_Ndef;
            resultSujPassF = spf.isDeficiente() ? this.DC_Familia_Casado_Def : this.DC_Familia_Casado_Ndef;
        } else {
            resultSujPassA = spa.isFamiliaMonoParental() ? (spa.isDeficiente() ? this.DC_Familia_Mono_Def : this.DC_Familia_Mono_Ndef) : (spa.isDeficiente() ? this.DC_Familia_Solteiro_Def : this.DC_Familia_Solteiro_Ndef);
        }
        resultSujPass = resultSujPassA + resultSujPassB + resultSujPassF;
        return resultSujPass;
    }

    @Override
    public double calcDedDependentes() {
        double resultDepend = 0.0;
        double resultDepe = 0.0;
        double resultDepeDef = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        resultDepe = this.DC_Familia_Dependente_Ndef * (double)spa.getNumDepNDef();
        resultDepeDef = this.DC_Familia_Dependente_Def * (double)spa.getNumDepDef();
        resultDepend = resultDepeDef + resultDepe;
        return resultDepend;
    }

    @Override
    public double calcDedAscendentes() {
        double resultAsc = 0.0;
        int nAscNDef = 0;
        int nAscDef = 0;
        int nAscendentes = 0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        nAscNDef = spa.getNumAscNDef();
        nAscendentes = nAscNDef + (nAscDef = spa.getNumAscDef());
        resultAsc = nAscendentes == 1 ? this.DC_Familia_1_Ascendente * (double)nAscendentes : this.DC_Familia_Ascendente * (double)nAscendentes;
        return resultAsc;
    }

    @Override
    public double calcDespSaude5() {
        double valorDespSaude5 = this.modelo3.getVal("AbatDedColecta", "valdespesasSaude5");
        double valSaude5 = 0.0;
        valSaude5 = this.DC_Saude_Geral_Taxa * valorDespSaude5;
        return valSaude5;
    }

    @Override
    public double calcDespSaudeOutras() {
        double valSaudeAux1 = 0.0;
        double valLimSaude = 0.0;
        double valSaudeOutros = this.modelo3.getVal("AbatDedColecta", "valdespesasSaudeOut");
        double valSaudeOutros30 = this.DC_Saude_Geral_Taxa * valSaudeOutros;
        double valDespSaude = this.modelo3.getVal("AbatDedColecta", "valdespesasSaude5");
        double valSaudeOutrosTot = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        valLimSaude = spa.getEstadoCivil() == '3' ? ((valSaudeAux1 = this.DC_Saude_OutrosBens_Limite_SepFact_Tx * valDespSaude) > this.DC_Saude_OutrosBens_Limite_SepFact_Valor ? valSaudeAux1 : this.DC_Saude_OutrosBens_Limite_SepFact_Valor) : ((valSaudeAux1 = this.DC_Saude_OutrosBens_Limite_OutrAgreg_Tx * valDespSaude) > this.DC_Saude_OutrosBens_Limite_OutrAgreg_Valor ? valSaudeAux1 : this.DC_Saude_OutrosBens_Limite_OutrAgreg_Valor);
        valSaudeOutrosTot = valSaudeOutros30 > valLimSaude ? valLimSaude : valSaudeOutros30;
        return valSaudeOutrosTot;
    }

    @Override
    public double calcDedEducFormProf() {
        DespEducacao despEdu = (DespEducacao)this.modelo3.getRubricas("DespEducacao");
        int valNumDepDespEduc = despEdu.getNumDepDespFPr();
        double valCampoDespEducFProf = this.modelo3.getVal("DespEducacao", "valdiscrimDEducSP") + this.modelo3.getVal("DespEducacao", "valdiscrimDEducD");
        double valDespEducFProf30 = this.DC_EducFP_Taxa * valCampoDespEducFProf;
        double valEducFProfTot = 0.0;
        double valEducFProfLim = 0.0;
        valEducFProfLim = valNumDepDespEduc < 3 ? this.DC_EducFP_Limite_TxSobreSMN160 : this.DC_EducFP_Limite_TxSobreSMN160 + this.DC_EducFP_Limite_TxSobreSMN30 * (double)valNumDepDespEduc;
        valEducFProfTot = valEducFProfLim < valDespEducFProf30 ? valEducFProfLim : valDespEducFProf30;
        return valEducFProfTot;
    }

    @Override
    public double calcDedEncLares() {
        double valLimLares = 0.0;
        double valCampoLares = this.modelo3.getVal("AbatDedColecta", "valencargosLares");
        double valCampoLares25 = this.DC_Lares_Tx * valCampoLares;
        double valLaresTot = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        valLimLares = spa.getEstadoCivil() == '3' ? this.DC_Lares_Limite_SepFact_Valor : this.DC_Lares_Limite_OutrAgreg_Valor;
        valLaresTot = valCampoLares25 > valLimLares ? valLimLares : valCampoLares25;
        return valLaresTot;
    }

    @Override
    public double calcDedEducLares(double dedDespesasEducacao, double dedEncargosLares) {
        DespEducacao despEdu = (DespEducacao)this.modelo3.getRubricas("DespEducacao");
        int valNDepDespEduc = despEdu.getNumDepDespFPr();
        double valdedAnteriores = dedDespesasEducacao + dedEncargosLares;
        double valEducLaresAux = 0.0;
        double valEducLaresTot = 0.0;
        if (valNDepDespEduc < 3) {
            valEducLaresTot = valdedAnteriores > this.DC_EducLares_Limite_Valor ? this.DC_EducLares_Limite_Valor : valdedAnteriores;
        } else {
            if (dedDespesasEducacao > this.DC_EducFP_Limite_TxSobreSMN160) {
                valEducLaresAux = dedDespesasEducacao - this.DC_EducFP_Limite_TxSobreSMN160;
            }
            valEducLaresTot = valdedAnteriores > this.DC_EducLares_Limite_Valor + valEducLaresAux ? this.DC_EducLares_Limite_Valor + valEducLaresAux : valdedAnteriores;
        }
        return valEducLaresTot;
    }

    @Override
    public double calcDedEncImoveis() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double valH1 = this.modelo3.getVal("AbatDedColecta", "valjurosDiviImovHabi");
        double valH3 = this.modelo3.getVal("AbatDedColecta", "valimportRendasHabiP");
        double val30DH1 = this.DC_ImovER_Taxa * valH1;
        double val30DH3 = this.DC_ImovER_Taxa * valH3;
        double valLimDH1 = 0.0;
        double valLimDH3 = 0.0;
        double valDH1 = 0.0;
        double valDH3 = 0.0;
        double valDedImoveisTot = 0.0;
        if (spa.getEstadoCivil() == '3') {
            valLimDH1 = this.DC_ImovER_SepFact_DH1_Valor;
            valLimDH3 = this.DC_ImovER_SepFact_DH3_Valor;
        } else {
            valLimDH1 = this.DC_ImovER_OutrAgreg_DH1_Valor;
            valLimDH3 = this.DC_ImovER_OutrAgreg_DH3_Valor;
        }
        valDH1 = valH1 > 0.0 ? (val30DH1 > valLimDH1 ? valLimDH1 : val30DH1) : 0.0;
        valDH3 = valH3 > 0.0 ? (val30DH3 > valLimDH3 ? valLimDH3 : val30DH3) : 0.0;
        valDedImoveisTot = valDH1 > valDH3 ? valDH1 : valDH3;
        return valDedImoveisTot;
    }

    @Override
    public double calcDedEnergiasRenov() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        AbatDedColecta abat = (AbatDedColecta)this.modelo3.getRubricas("AbatDedColecta");
        double valLimEnerRenov = 0.0;
        double valER = this.modelo3.getVal("AbatDedColecta", "valdespAquEnergRenov");
        double valCP = this.modelo3.getVal("AbatDedColecta", "valaquisEquipCompUlt");
        double valER30 = this.DC_ImovER_Taxa * (valER + valCP);
        double valDedEnerRenovTot = 0.0;
        valLimEnerRenov = spa.getEstadoCivil() == '3' ? this.DC_ImovER_SepFact_DH4_NaoAqEquip_Valor : this.DC_ImovER_OutrAgreg_DH4_NaoAqEquip_Valor;
        valDedEnerRenovTot = valER30 > valLimEnerRenov ? valLimEnerRenov : valER30;
        return valDedEnerRenovTot;
    }

    @Override
    public double calcMaiorDed() {
        double valEngRenovaveis;
        double valMaior = 0.0;
        double valEncImoveis = this.calcDedEncImoveis();
        valMaior = valEncImoveis > (valEngRenovaveis = this.calcDedEnergiasRenov()) ? valEncImoveis : valEngRenovaveis;
        return valMaior;
    }

    @Override
    public double calcDedSegVida() {
        double valLimSegVida = 0.0;
        double valPremiosSegVida = this.modelo3.getVal("AbatDedColecta", "valpremiosSegVidaCSS");
        double valPremiosSegVida25 = this.DC_ContribReformaDef_Taxa * valPremiosSegVida;
        double valSegurosVidaTot = 0.0;
        char indicadorCasado = this.verificaCasado();
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        valLimSegVida = indicadorCasado == 'F' && spa.getObito() != '1' ? this.DC_ContribReformaDef_Lim_NCas : this.DC_ContribReformaDef_Lim_Cas;
        valSegurosVidaTot = valPremiosSegVida25 > valLimSegVida ? valLimSegVida : valPremiosSegVida25;
        return valSegurosVidaTot;
    }

    @Override
    public double calcDedSeguroSaude() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        int numeroDependentes = spa.getNumDepDef() + spa.getNumDepNDef();
        double valLimSegRiscosSaude = 0.0;
        double valPremioSegRisSaude = this.modelo3.getVal("AbatDedColecta", "valpremiosSegRiSaude");
        double valPremioSegRisSaude25 = this.DC_SegSaude_Taxa * valPremioSegRisSaude;
        double valPremioSegRisSaudeTot = 0.0;
        char indicadorCasado = this.verificaCasado();
        valLimSegRiscosSaude = indicadorCasado == 'V' ? this.DC_SegSaude_Lim_Cas_Valor + this.DC_SegSaude_Lim_Cas_ValorSobreDep * (double)numeroDependentes : this.DC_SegSaude_Lim_NCas_Valor + this.DC_SegSaude_Lim_NCas_ValorSobreDep * (double)numeroDependentes;
        valPremioSegRisSaudeTot = valPremioSegRisSaude25 > valLimSegRiscosSaude ? valLimSegRiscosSaude : valPremioSegRisSaude25;
        return valPremioSegRisSaudeTot;
    }

    @Override
    public double calcDedSPDef() {
        double result = 0.0;
        double resultParcial = 0.0;
        result = this.calcDedSujeitosPassivosDef();
        result+=this.calcDedDependentesDef();
        resultParcial = result+=this.calcDedAscendentesDef();
        return resultParcial;
    }

    @Override
    public double calcDedSujeitosPassivosDef() {
        double resultSujPass = 0.0;
        double resultSujPassA = 0.0;
        double resultSujPassB = 0.0;
        double resultSujPassF = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        Contribuinte spb = this.modelo3.getContribuinte("SPB");
        Contribuinte spf = this.modelo3.getContribuinte("SPF");
        if (spa.isDeficiente()) {
            if (spa.getGrauDef() == '1') {
                resultSujPassA = !spa.isDefFArm() ? this.DC_SujPassDef60 : this.DC_SujPassDefFA60;
            }
            if (spa.getGrauDef() == '2') {
                resultSujPassA = !spa.isDefFArm() ? this.DC_SujPassDef60 + this.DC_SujPassDef90 : this.DC_SujPassDefFA60 + this.DC_SujPassDef90;
            }
        }
        if (spb.isDeficiente()) {
            if (spb.getGrauDef() == '1') {
                resultSujPassB = !spb.isDefFArm() ? this.DC_SujPassDef60 : this.DC_SujPassDefFA60;
            }
            if (spb.getGrauDef() == '2') {
                resultSujPassB = !spb.isDefFArm() ? this.DC_SujPassDef60 + this.DC_SujPassDef90 : this.DC_SujPassDefFA60 + this.DC_SujPassDef90;
            }
        }
        if (spf.isDeficiente()) {
            if (spf.getGrauDef() == '1') {
                resultSujPassF = !spf.isDefFArm() ? this.DC_SujPassDef60 : this.DC_SujPassDefFA60;
            }
            if (spf.getGrauDef() == '2') {
                resultSujPassF = !spf.isDefFArm() ? this.DC_SujPassDef60 + this.DC_SujPassDef90 : this.DC_SujPassDefFA60 + this.DC_SujPassDef90;
            }
        }
        resultSujPass = resultSujPassA + resultSujPassB + resultSujPassF;
        return resultSujPass;
    }

    @Override
    public double calcDedDependentesDef() {
        double resultDepeDef = 0.0;
        double resultD1Def = 0.0;
        double resultD2Def = 0.0;
        double resultD3Def = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        Contribuinte spd1 = this.modelo3.getContribuinte("D1");
        Contribuinte spd2 = this.modelo3.getContribuinte("D2");
        Contribuinte spd3 = this.modelo3.getContribuinte("D3");
        if (spd1.isDeficiente()) {
            if (spd1.getGrauDef() == '1') {
                resultD1Def = this.DC_DepeDef60;
            }
            if (spd1.getGrauDef() == '2') {
                resultD1Def = this.DC_DepeDef60 + this.DC_SujPassDef90;
            }
        }
        if (spd2.isDeficiente()) {
            if (spd2.getGrauDef() == '1') {
                resultD2Def = this.DC_DepeDef60;
            }
            if (spd2.getGrauDef() == '2') {
                resultD2Def = this.DC_DepeDef60 + this.DC_SujPassDef90;
            }
        }
        if (spd3.isDeficiente()) {
            if (spd3.getGrauDef() == '1') {
                resultD3Def = this.DC_DepeDef60;
            }
            if (spd3.getGrauDef() == '2') {
                resultD3Def = this.DC_DepeDef60 + this.DC_SujPassDef90;
            }
        }
        resultDepeDef = resultD1Def + resultD2Def + resultD3Def;
        return resultDepeDef;
    }

    @Override
    public double calcDedAscendentesDef() {
        double resultAscDef = 0.0;
        int nAscDef = 0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        nAscDef = spa.getNumAscDef();
        resultAscDef = this.DC_AscendDef * (double)nAscDef;
        return resultAscDef;
    }

    @Override
    public double calcAconselhamento() {
        double valLimAconselha = 0.0;
        double valCampoAconselha = this.modelo3.getVal("AbatDedColecta", "valaconselhaJuridico");
        double valCampoAconselha20 = this.DC_DespAconsJur_Taxa * valCampoAconselha;
        double valAconselhamentoTot = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        valLimAconselha = spa.getEstadoCivil() == '3' ? this.DC_DespAconsJur_Limite_SepFact_Valor : this.DC_DespAconsJur_Limite_OutrAgreg_Valor;
        valAconselhamentoTot = valCampoAconselha20 > valLimAconselha ? valLimAconselha : valCampoAconselha20;
        return valAconselhamentoTot;
    }

    @Override
    public double calcDedPPRPPE(double rendimIsentSE, double acrescimosRendLiq, double colectaGratificacoes, double colectaFuturoOpcoes) {
        double psbv;
        double rtvh;
        double palh;
        double rbh;
        double valorDedPPRA = 0.0;
        double valorDedPPRB = 0.0;
        double valorDedPPRF = 0.0;
        double valPPRPPETot = 0.0;
        double valRBrutoF = this.modelo3.getVal("Prediais", "valtotalRendas");
        Contribuinte spa = this.modelo3.getContribuinte("SPA");
        Contribuinte spb = this.modelo3.getContribuinte("SPB");
        Contribuinte spf = this.modelo3.getContribuinte("SPF");
        int valIdadeSujeitoA = spa.getIdade();
        int valIdadeSujeitoB = spb.getIdade();
        int valIdadeSujeitoF = spf.getIdade();
        double valorRBAgregado = this.calcValTRB(spa) + this.calcValTRB(spb) + this.calcValTRB(spf) + valRBrutoF + this.calcRendLiqCatE() + rendimIsentSE + acrescimosRendLiq + colectaFuturoOpcoes;
        double valAuxG = this.getValorMaisVal();
        boolean spaExclusPensionista = this.calcRendBrutoCatA(spa) == 0.0 && this.calcRendBrutoCatH(spa) == this.modelo3.getVal("Pensoes", "valrendBrutoCatH", spa.getIdCont()) && this.modelo3.getVal("Pensoes", "valrendBrutoCatH", spa.getIdCont()) != 0.0;
        boolean spbExclusPensionista = this.calcRendBrutoCatA(spb) == 0.0 && this.calcRendBrutoCatH(spb) == this.modelo3.getVal("Pensoes", "valrendBrutoCatH", spb.getIdCont()) && this.modelo3.getVal("Pensoes", "valrendBrutoCatH", spb.getIdCont()) != 0.0;
        boolean spfExclusPensionista = this.calcRendBrutoCatA(spf) == 0.0 && this.calcRendBrutoCatH(spf) == this.modelo3.getVal("Pensoes", "valrendBrutoCatH", spf.getIdCont()) && this.modelo3.getVal("Pensoes", "valrendBrutoCatH", spf.getIdCont()) != 0.0;
        valorRBAgregado = valorRBAgregado + valAuxG + this.rendLiquiCategB;
        if (!spaExclusPensionista) {
            rbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", this.modelo3.getContribuinte(0).getIdCont());
            rtvh = this.modelo3.getVal("Pensoes", "valrendTempVit", this.modelo3.getContribuinte(0).getIdCont());
            palh = this.modelo3.getVal("Pensoes", "valrendPensAlim", this.modelo3.getContribuinte(0).getIdCont());
            psbv = this.modelo3.getVal("Pensoes", "PSobrev", this.modelo3.getContribuinte(0).getIdCont());
            if (rbh == 0.0 && (rtvh != 0.0 || palh != 0.0 || psbv != 0.0)) {
                valorDedPPRA = this.calcValDedPPR(this.modelo3.getContribuinte(0).getIdCont(), valIdadeSujeitoA);
            }
        } else {
            valorDedPPRA = 0.0;
        }
        if (!spbExclusPensionista) {
            rbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", this.modelo3.getContribuinte(1).getIdCont());
            rtvh = this.modelo3.getVal("Pensoes", "valrendTempVit", this.modelo3.getContribuinte(1).getIdCont());
            palh = this.modelo3.getVal("Pensoes", "valrendPensAlim", this.modelo3.getContribuinte(1).getIdCont());
            psbv = this.modelo3.getVal("Pensoes", "PSobrev", this.modelo3.getContribuinte(1).getIdCont());
            if (rbh == 0.0 && (rtvh != 0.0 || palh != 0.0 || psbv != 0.0)) {
                valorDedPPRB = this.calcValDedPPR(this.modelo3.getContribuinte(1).getIdCont(), valIdadeSujeitoB);
            }
        } else {
            valorDedPPRB = 0.0;
        }
        if (!spfExclusPensionista) {
            rbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", "SPF");
            rtvh = this.modelo3.getVal("Pensoes", "valrendTempVit", "SPF");
            palh = this.modelo3.getVal("Pensoes", "valrendPensAlim", "SPF");
            psbv = this.modelo3.getVal("Pensoes", "PSobrev", "SPF");
            if (rbh == 0.0 && (rtvh != 0.0 || palh != 0.0 || psbv != 0.0)) {
                valorDedPPRF = this.calcValDedPPR("SPF", valIdadeSujeitoF);
            }
        } else {
            valorDedPPRF = 0.0;
        }
        valPPRPPETot = valorDedPPRA + valorDedPPRB + valorDedPPRF;
        return valPPRPPETot;
    }

    @Override
    public double calcValDedPPR(String cont, int idadeCont) {
        double valLimPPRPPE = 0.0;
        double valorTRBTaxa = 0.0;
        int valIdadeSujeito = idadeCont;
        double valCampoPPR = 0.0;
        valCampoPPR = cont.equals("SPF") ? this.modelo3.getVal("PPRPPE", "valPPRSPF") + this.modelo3.getVal("PPRPPE", "valPPESPF") : this.modelo3.getVal("PPRPPE", "valPPR", cont) + this.modelo3.getVal("PPRPPE", "valPPE", cont);
        double valDedPPRSujPassTot = 0.0;
        if (valIdadeSujeito < 35) {
            valLimPPRPPE = this.DC_PprPpe_Esc1_Limite_Valor;
            valorTRBTaxa = this.DC_PprPpe_Esc1_Taxa;
        } else if (valIdadeSujeito >= 35 && valIdadeSujeito <= 50) {
            valLimPPRPPE = this.DC_PprPpe_Esc2_Limite_Valor;
            valorTRBTaxa = this.DC_PprPpe_Esc2_Taxa;
        } else {
            valLimPPRPPE = this.DC_PprPpe_Esc3_Limite_Valor;
            valorTRBTaxa = this.DC_PprPpe_Esc3_Taxa;
        }
        valDedPPRSujPassTot = valorTRBTaxa * valCampoPPR > valLimPPRPPE ? valLimPPRPPE : valorTRBTaxa * valCampoPPR;
        return valDedPPRSujPassTot;
    }

    @Override
    public double calcValTRB(Contribuinte cont) {
        double valorTRBRBA = 0.0;
        double valorTRBRBH = 0.0;
        double valorTRBGrat = 0.0;
        double valTRBTot = 0.0;
        valorTRBRBA = this.calcRendBrutoCatA(cont);
        valorTRBRBH = this.calcRendBrutoCatH(cont);
        valorTRBGrat = this.modelo3.getVal("RenTrabDep", "valgratific", cont.getIdCont());
        valTRBTot = valorTRBRBA + valorTRBRBH + valorTRBGrat;
        return valTRBTot;
    }

    @Override
    public double calcPPHCooperativas() {
        double valLimCPH = 0.0;
        double valCampoCPH = this.modelo3.getVal("BenefFiscais", "valPPH") + this.modelo3.getVal("BenefFiscais", "valentregaCoopHabi");
        double valCampoCPH25 = this.DC_ContasPhCh_Taxa * valCampoCPH;
        double valCPHTot = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        valLimCPH = spa.getEstadoCivil() == '3' ? this.DC_ContasPhCh_SepFact_Lim : this.DC_ContasPhCh_OutrAgreg_Lim;
        valCPHTot = valCampoCPH25 > valLimCPH ? valLimCPH : valCampoCPH25;
        return valCPHTot;
    }

    @Override
    public double calcDeducaoPPA() {
        double valLimPPA = this.DC_PlPoupAcc_Lim;
        double valPPASPA = this.modelo3.getVal("PPA", "valPPA", "SPA");
        double valPPASPB = this.modelo3.getVal("PPA", "valPPA", "SPB");
        double valPPASPA7 = this.DC_PlPoupAcc_Tx * valPPASPA;
        double valPPASPB7 = this.DC_PlPoupAcc_Tx * valPPASPB;
        double valPPATotSPA = 0.0;
        double valPPATotSPB = 0.0;
        double valPPATot = 0.0;
        valPPATotSPA = valPPASPA7 > valLimPPA ? valLimPPA : valPPASPA7;
        valPPATotSPB = valPPASPB7 > valLimPPA ? valLimPPA : valPPASPB7;
        valPPATot = valPPATotSPA + valPPATotSPB;
        return valPPATot;
    }

    @Override
    public double calcDedDespEducReabDef() {
        double valDespReabDef = this.modelo3.getVal("BenefFiscais", "valdespEducSPDdef");
        double valDespReabDef30 = this.DC_DespEducDef * valDespReabDef;
        double valDespReabDefTot = 0.0;
        valDespReabDefTot = valDespReabDef30;
        return valDespReabDefTot;
    }

    @Override
    public double calcDedPremiosSegurDef(double colectaRendimSujeitos) {
        double valPremSegDef = this.modelo3.getVal("BenefFiscais", "valpSegurosSPDdef");
        double valPremSegDef25 = this.DC_SegDef * valPremSegDef;
        double valPremSegDefTot = 0.0;
        valPremSegDefTot = valPremSegDef25;
        if (valPremSegDefTot > 0.15 * colectaRendimSujeitos) {
            valPremSegDefTot = 0.15 * colectaRendimSujeitos;
        }
        return valPremSegDefTot;
    }

    @Override
    public double calcDedComputadores(double txImposto) {
        double valLimComput = 0.0;
        double valCampoComput = this.modelo3.getVal("BenefFiscais", "valaquisicaoComput");
        double valCampoComput25 = this.DC_AqComput_Taxa * valCampoComput;
        double valComputadoresTot = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        valLimComput = spa.getEstadoCivil() == '3' ? this.DC_AqComput_Limite_SepFact_Valor : this.DC_AqComput_Limite_OutrAgreg_Valor;
        valComputadoresTot = valCampoComput25 > valLimComput ? valLimComput : valCampoComput25;
        if (txImposto >= this.TaxasGerais_Esc7_Cont_Taxa) {
            valComputadoresTot = 0.0;
        }
        return valComputadoresTot;
    }

    @Override
    public double calcDedCooperadores() {
        double valLimCooperad = 0.0;
        double valCampoCooperad = this.modelo3.getVal("BenefFiscais", "valentregaCooperad");
        double valCampoCooperad5 = this.DC_EntregCoopCs_Taxa * valCampoCooperad;
        double valCooperadTot = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        valLimCooperad = spa.getEstadoCivil() == '3' ? this.DC_EntregCoopCs_Limite_SepFact_Valor : this.DC_EntregCoopCs_Limite_OutrAgreg_Valor;
        valCooperadTot = valCampoCooperad5 > valLimCooperad ? valLimCooperad : valCampoCooperad5;
        return valCooperadTot;
    }

    @Override
    public double calcDedDonatPublic() {
        double valDedDonatPub = this.modelo3.getVal("AbatDedColecta", "valdonativosEntPubli");
        double valDedDonatPub25 = this.DC_Donat_EntPublic * valDedDonatPub;
        double valDedDonatPubTot = 0.0;
        valDedDonatPubTot = valDedDonatPub25;
        return valDedDonatPubTot;
    }

    @Override
    public double calcDedDonativOutr(double colectaRendimSujeitos) {
        double valLimDonatOutr = this.DC_Donat_OutrEnt_Limite_TaxaSobreColecta * colectaRendimSujeitos;
        double valCampoDonatOutr = this.modelo3.getVal("AbatDedColecta", "valdonativosOutEntid");
        double valCampoDonatRelig = this.modelo3.getVal("AbatDedColecta", "valdonativosLibRelig");
        double valCampoDonatCatol = this.modelo3.getVal("AbatDedColecta", "valdonativosIgrCatolica");
        double valCampoDoMecCient = this.modelo3.getVal("BenefFiscais", "valmecenatoCientifico");
        double valCampoDoMecCult = this.modelo3.getVal("BenefFiscais", "valmecenatoCultural");
        double valCampoDoMecCPlu = this.modelo3.getVal("BenefFiscais", "valcontratosPluriA");
        double valCampoDoMecSoc = this.modelo3.getVal("BenefFiscais", "valmecenatoSocial");
        double valCampoDoMecSocA = this.modelo3.getVal("BenefFiscais", "valmecenatoSocEsp");
        double valCampoDoMecFami = this.modelo3.getVal("BenefFiscais", "valmecenatoFamiliar");
        double valCampoDoMecECien = this.modelo3.getVal("BenefFiscais", "valestadoMecCientifico");
        double valCampoDoMecECult = this.modelo3.getVal("BenefFiscais", "valestadoMecCultural");
        double valCampoDoMecECPlu = this.modelo3.getVal("BenefFiscais", "valestadoContPluri");
        double valCampoDoMecESoc = this.modelo3.getVal("BenefFiscais", "valestadoMecSocial");
        double valCampoDoMecEFam = this.modelo3.getVal("BenefFiscais", "valestadoMecFamiliar");
        double valDonatOutr25 = 0.0;
        double valDonatOutrTot = 0.0;
        valDonatOutr25 = valCampoDonatOutr + valCampoDonatCatol * this.Taxa_Major_Don_Relig + valCampoDonatRelig * this.Taxa_Major_Don_Relig;
        valDonatOutr25 = valDonatOutr25 + valCampoDoMecCient + valCampoDoMecCult * this.Taxa_Major_Don_Cult;
        valDonatOutr25 = valDonatOutr25 + valCampoDoMecCPlu * this.Taxa_Major_Don_ECPlu + valCampoDoMecSoc * this.Taxa_Major_Don_MecSoc;
        valDonatOutr25+=valCampoDoMecSocA * this.Taxa_Major_Don_Soc;
        valDonatOutr25 = valDonatOutr25 + valCampoDoMecFami * this.Taxa_Major_Don_Fam + valCampoDoMecECien;
        valDonatOutr25 = valDonatOutr25 + valCampoDoMecECult * this.Taxa_Major_Don_Cult + valCampoDoMecECPlu * this.Taxa_Major_Don_ECPlu;
        valDonatOutr25 = valDonatOutr25 + valCampoDoMecESoc * this.Taxa_Major_Don_Soc + valCampoDoMecEFam * this.Taxa_Major_Don_Fam;
        valDonatOutrTot = (valDonatOutr25 = this.DC_Donat_OutrEnt_Taxa * valDonatOutr25) > valLimDonatOutr ? valLimDonatOutr : valDonatOutr25;
        return valDonatOutrTot;
    }

    @Override
    public double calcIvaAquiServicos() {
        double valLimIva = 0.0;
        double valDedIVAAqSv = this.modelo3.getVal("BenefFiscais", "valIvaAquiServicos");
        double valDedIVAAqSv30 = this.DC_IvaAqui_Taxa * valDedIVAAqSv;
        double valIvaTot = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        valLimIva = spa.getEstadoCivil() == '3' ? this.DC_IvaAqui_Limite_SepFact_Valor : this.DC_IvaAqui_Limite_OutrAgreg_Valor;
        valIvaTot = valDedIVAAqSv30 > valLimIva ? valLimIva : valDedIVAAqSv30;
        return valIvaTot;
    }

    @Override
    public char verificaCasado() {
        char indicCasado = 'V';
        char indCasado = 'V';
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        indicCasado = spa.getEstadoCivil() == '1' || spa.getEstadoCivil() == '4' ? 'V' : 'F';
        indCasado = indicCasado;
        return indCasado;
    }

    @Override
    public double calcColectaMinExistencia(double rendimIsentSE, double colectaAposDeducoes, double rendDetermtx, int coefConjugal, double rendimColectavel, double colectaFuturoOpcoes, double colectaGratificacoes, double colectaDesportistas, double acrescimosRendLiq) {
        double totRendim = 0.0;
        double colMEaux1 = 0.0;
        double colMEaux2 = 0.0;
        double colMEaux3 = 0.0;
        double colMEaux4 = 0.0;
        double colR1 = 0.0;
        double colR2 = 0.0;
        double colectaME = 0.0;
        double rBCatA = 0.0;
        double rBCatH = 0.0;
        double resultpA = 0.0;
        double resultpH = 0.0;
        double valAuxG = 0.0;
        double valAuxGA = 0.0;
        double valAuxGB = 0.0;
        double valAuxGC = 0.0;
        double valAuxRenDT = rendDetermtx / (double)coefConjugal;
        int i = 0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        int numdeps = spa.getNumDepNDef() + spa.getNumDepDef();
        valAuxGA = this.Taxa_Trib_Mais_Valias * this.calcMaisValBensImoveis() + this.Taxa_Trib_Mais_Valias * this.calcMaisValPropIntelect() + this.Taxa_Trib_Mais_Valias * this.calcMaisValPosContratua();
        if (valAuxGA < 0.0) {
            valAuxGA = 0.0;
        }
        if ((valAuxGB = this.calcMaisValPartesSociais()) < 0.0) {
            valAuxGB = 0.0;
        }
        if ((valAuxG = valAuxGA + valAuxGB + (valAuxGC = this.calcIncremPatrimon())) < 0.0) {
            valAuxG = 0.0;
        }
        while (i < this.modelo3.getNumContribs()) {
            resultpA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            rBCatA+=resultpA;
            resultpH = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
            rBCatH+=resultpH;
            ++i;
        }
        totRendim = rBCatA + rendimIsentSE + rBCatH + acrescimosRendLiq;
        totRendim = totRendim + this.rendLiquiCategE + this.rendLiquiCategB;
        totRendim = totRendim + this.rendLiquiCategF + valAuxG;
        totRendim = totRendim + colectaFuturoOpcoes + colectaGratificacoes;
        colMEaux1 = 0.5 * (totRendim+=colectaDesportistas);
        colMEaux2 = rBCatA + rendimIsentSE;
        colMEaux3 = totRendim - colectaAposDeducoes;
        colR1 = this.calculaR1();
        colR2 = this.calculaR2();
        colMEaux4 = colR1 + colR2;
        colectaME = colMEaux2 <= colMEaux1 ? colectaAposDeducoes : ((spa.getEstadoCivil() == '3' || spa.getEstadoCivil() == '2') && rendDetermtx <= this.CME_Limite_Valor || (spa.getEstadoCivil() == '1' || spa.getEstadoCivil() == '4') && valAuxRenDT <= this.CME_Limite_Valor ? 0.0 : (numdeps > 2 && numdeps < 5 && rendimColectavel <= this.CME_Lim_Dep_3_4_Valor || numdeps >= 5 && rendimColectavel <= this.CME_Lim_Dep_5_Valor ? 0.0 : (colMEaux3 < colMEaux4 ? totRendim - colMEaux4 : colectaAposDeducoes)));
        return colectaME;
    }

    @Override
    public double calcAcrescColecta() {
        double res = 0.0;
        res = this.modelo3.getVal("Acrescimos", "valsegurosCol") + this.modelo3.getVal("Acrescimos", "valPPRCol") + this.modelo3.getVal("Acrescimos", "valPPALevaCol") + this.modelo3.getVal("Acrescimos", "valPPAIncuCol") + this.modelo3.getVal("Acrescimos", "valPPHCol") + this.modelo3.getVal("Acrescimos", "valCoopCol") + this.modelo3.getVal("Acrescimos", "valInobsCol") + this.modelo3.getVal("Acrescimos", "valCondomCol");
        return res;
    }

    @Override
    public double calculaR1() {
        int numTit = 0;
        double valR1 = 0.0;
        double valRBAR1 = 0.0;
        for (int l = 0; l < this.modelo3.getNumContribs(); ++l) {
            valRBAR1 = this.calcRendBrutoCatA(this.modelo3.getContribuinte(l));
            if (valRBAR1 >= this.SMNa) {
                ++numTit;
            }
            valRBAR1 = 0.0;
        }
        valR1 = (double)numTit * this.SMNa;
        return valR1;
    }

    @Override
    public double calculaR2() {
        boolean numTit = false;
        double valorTitSupRBA = 0.0;
        double valR2 = 0.0;
        for (int m = 0; m < this.modelo3.getNumContribs(); ++m) {
            valorTitSupRBA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(m));
            if (valorTitSupRBA < this.SMNa) {
                valR2+=valorTitSupRBA;
            }
            valorTitSupRBA = 0.0;
        }
        return valR2;
    }

    @Override
    public double calcRendimGratifica() {
        double gratifAux1 = 0.0;
        double valorGratifica = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        gratifAux1 = this.modelo3.getVal("RenTrabDep", "valgratific", "SPA") + this.modelo3.getVal("RenTrabDep", "valgratific", "SPB") + this.modelo3.getVal("RenTrabDep", "valgratific", "D1") + this.modelo3.getVal("RenTrabDep", "valgratific", "D2") + this.modelo3.getVal("RenTrabDep", "valgratific", "D3");
        if (spa.getReside() == '1' || spa.getReside() == '3') {
            valorGratifica = gratifAux1 * this.ColecGratif_Geral;
        } else if (spa.getReside() == '2') {
            valorGratifica = gratifAux1 * this.ColecGratif_Acores;
        }
        return valorGratifica;
    }

    @Override
    public double calcColFuturos() {
        double res = 0.0;
        double valFuturo = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        res = spa.getReside() == '2' ? valFuturo * this.TaxaAnexoG_Acores_Taxa : valFuturo * this.TaxaAnexoG_Geral_Taxa;
        return res;
    }

    @Override
    public double calcColMaisValNEng() {
        double res = 0.0;
        double warrants = this.modelo3.getVal("OpFinancei", "vali4");
        double totalRealiza = this.modelo3.getVal("MaisValias", "valtotRealiPartSoc");
        double totalAquisic = this.modelo3.getVal("MaisValias", "valtotAquisPartSoc");
        double totalDespEnc = this.modelo3.getVal("MaisValias", "valtotDespePartSoc");
        double futOpcoes = this.modelo3.getVal("OpFinancei", "valoutros");
        double certificados = this.modelo3.getVal("OpFinancei", "valcertif");
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (spa.getEnglobG() == '0') {
            res = totalRealiza - totalAquisic - totalDespEnc;
            res = res <= 500.0 ? 0.0 : (res-=500.0);
            res = res + warrants + futOpcoes + certificados;
        }
        res = spa.getReside() == '2' ? (res*=this.TaxaAnexoG_Acores_Taxa) : (res*=this.TaxaAnexoG_Geral_Taxa);
        if (res < 0.0) {
            res = 0.0;
        }
        return res;
    }

    @Override
    public double calcColPPA() {
        double res = 0.0;
        double DifPosN7 = this.modelo3.getVal("Capitais", "valDifPosN7");
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        res = spa.getReside() == '2' ? DifPosN7 * this.TaxaAnexoE_Acores_Taxa : DifPosN7 * this.TaxaAnexoE_Geral_Taxa;
        return res;
    }

    @Override
    public double calcResgateFPR() {
        double res = 0.0;
        double ResgateFPR = this.modelo3.getVal("Capitais", "valResgateFPR") / 5.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        res = spa.getReside() == '2' ? ResgateFPR * this.TaxaAnexoE_Acores_Taxa : ResgateFPR * this.TaxaAnexoE_Geral_Taxa;
        return res;
    }

    @Override
    public double calcRetencoesFonte() {
        double ret = 0.0;
        double retparc = 0.0;
        double re = 0.0;
        double reparc = 0.0;
        double retFonE = this.modelo3.getVal("Capitais", "valretFonteE");
        double retFonF = this.modelo3.getVal("Prediais", "valretFonteF");
        double retPreR = this.modelo3.getVal("PreReforma", "valretFontePreSPA") + this.modelo3.getVal("PreReforma", "valretFontePreSPB");
        double retFonG = this.calcIncremPatrimonRF();
        int f = 0;
        for (int k = 0; k < this.modelo3.getNumContribs(); ++k) {
            retparc = this.calcRetencoesCatA(this.modelo3.getContribuinte(k)) + this.calcRetencoesCatH(this.modelo3.getContribuinte(k));
            ret+=retparc;
        }
        while (f < this.modelo3.getNumContribs() - 1) {
            reparc = this.calcRetencoesCatB(this.modelo3.getContribuinte(f));
            re+=reparc;
            ++f;
        }
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
            if (retFonG > this.colecta4) {
                retFonG = this.colecta4;
            }
            if (retFonE > this.colecta3) {
                retFonE = this.colecta3;
            }
            if (ret + re > this.colecta1) {
                ret = this.colecta1;
                re = 0.0;
            }
        }
        ret = ret + re + retFonE + retFonF + retFonG + retPreR + this.calcRetenRendIsentos();
        return ret;
    }

    @Override
    public double calcRetencoesCatA(Contribuinte cont) {
        double retCatA = this.modelo3.getVal("RenTrabDep", "valretFonteCatA", cont.getIdCont());
        return retCatA;
    }

    @Override
    public double calcRetencoesCatH(Contribuinte cont) {
        double retCatH = this.modelo3.getVal("Pensoes", "valretencoesFCatH", cont.getIdCont());
        return retCatH;
    }

    @Override
    public double calcRetencoesCatB(Contribuinte cont) {
        double retCatB = this.modelo3.getVal("CategoriaB", "valRetFonteB", cont.getIdCont()) + this.modelo3.getVal("AnexoC", "valRetFonteC", cont.getIdCont()) + this.modelo3.getVal("AnexoD", "valRetFonteD", cont.getIdCont());
        return retCatB;
    }

    @Override
    public double calcPagamConta() {
        double res = 0.0;
        double resparc = 0.0;
        for (int k = 0; k < this.modelo3.getNumContribs() - 1; ++k) {
            resparc = this.calcPCB(this.modelo3.getContribuinte(k));
            res+=resparc;
        }
        return res;
    }

    @Override
    public double calcPCB(Contribuinte cont) {
        double pagContB = this.modelo3.getVal("CategoriaB", "valPagamenContaB", cont.getIdCont()) + this.modelo3.getVal("AnexoC", "valPagamenContaC", cont.getIdCont()) + this.modelo3.getVal("AnexoD", "valPagamenContaD", cont.getIdCont());
        return pagContB;
    }

    @Override
    public double getValorMaisVal() {
        double rCatG = 0.0;
        double rCatGA = 0.0;
        double rCatGB = 0.0;
        double rCatGC = 0.0;
        rCatGB = this.calcMaisValPartesSociais();
        if (rCatGB < 0.0) {
            rCatGB = 0.0;
        }
        if ((rCatGA = this.Taxa_Trib_Mais_Valias * this.calcMaisValBensImoveis() + this.Taxa_Trib_Mais_Valias * this.calcMaisValPropIntelect() + this.Taxa_Trib_Mais_Valias * this.calcMaisValPosContratua()) < 0.0) {
            rCatGA = 0.0;
        }
        if ((rCatG = rCatGA + rCatGB + (rCatGC = this.calcIncremPatrimon())) < 0.0) {
            rCatG = 0.0;
        }
        return rCatG;
    }

    @Override
    public double getValRendimGlobal() {
        int i = 0;
        double resauxA = 0.0;
        double resauxH = 0.0;
        double rCatA = 0.0;
        double rCatH = 0.0;
        double rCatE = this.rendLiquiCategE;
        double rCatF = this.modelo3.getVal("Prediais", "valtotalRendas") + this.calcSublocacao();
        double rCatG = this.getValorMaisVal();
        double rCatB = this.rendLiquiCategB;
        if (rCatF < 0.0) {
            rCatF = 0.0;
        }
        while (i < this.modelo3.getNumContribs()) {
            resauxA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            rCatA+=resauxA;
            resauxH = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
            rCatH+=resauxH;
            ++i;
        }
        return rCatA + rCatH + rCatB + rCatE + rCatF + this.acrescimosRendLiq + rCatG;
    }

    @Override
    public double getValDedEspecificas() {
        int j = 0;
        double resRBA = 0.0;
        double resRBH = 0.0;
        double resdedA = 0.0;
        double resdedH = 0.0;
        double rDedA = 0.0;
        double rDedH = 0.0;
        double rDedF = 0.0;
        double rCatF = this.modelo3.getVal("Prediais", "valtotalRendas");
        if (rCatF < 0.0) {
            rCatF = 0.0;
        }
        while (j < this.modelo3.getNumContribs()) {
            resRBA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(j));
            resdedA = this.calcDeducEspecifica(this.modelo3.getContribuinte(j));
            if (resdedA > resRBA) {
                resdedA = resRBA;
            }
            rDedA+=resdedA;
            resRBH = this.calcRendBrutoCatH(this.modelo3.getContribuinte(j));
            resdedH = this.calcDeducEspecificaH(this.modelo3.getContribuinte(j));
            if (resdedH > resRBH) {
                resdedH = resRBH;
            }
            rDedH+=resdedH;
            ++j;
        }
        rDedF = this.modelo3.getVal("Prediais", "valdespManutencao") + this.modelo3.getVal("Prediais", "valdespConservacao") + this.modelo3.getVal("Prediais", "valtaxasAutarq") + this.modelo3.getVal("Prediais", "valcontribAutarq") + this.modelo3.getVal("Prediais", "valdespCondominio");
        if (rDedF > rCatF) {
            rDedF = rCatF;
        }
        return rDedA + rDedH + rDedF;
    }

    @Override
    public double getValPerdasRecup() {
        double perdasB = 0.0;
        double perdasF = 0.0;
        double perdasG = 0.0;
        double resG = this.getValorMaisVal();
        double resAnosAntG = this.modelo3.getVal("MaisValias", "valperdasAnosAntG");
        double resAnosAntF = this.modelo3.getVal("Prediais", "valperdasAnosAntF");
        double resF = this.modelo3.getVal("Prediais", "valtotalRendas") - this.modelo3.getVal("Prediais", "valdespManutencao") - this.modelo3.getVal("Prediais", "valdespConservacao") - this.modelo3.getVal("Prediais", "valtaxasAutarq") - this.modelo3.getVal("Prediais", "valcontribAutarq") - this.modelo3.getVal("Prediais", "valdespCondominio");
        perdasF = resF > 0.0 ? (resF - resAnosAntF < 0.0 ? resF : resAnosAntF) : 0.0;
        perdasG = resG > 0.0 ? (resG - resAnosAntG < 0.0 ? resG : resAnosAntG) : 0.0;
        return perdasB + perdasF + perdasG;
    }

    @Override
    public double getValAbatimentos() {
        if (this.rendimLiqAposDedu - this.abatimentos < 0.0) {
            return this.rendimLiqAposDedu;
        }
        return this.abatimentos;
    }

    @Override
    public double getValRendimDetTaxas() {
        return this.rendDetermtx;
    }

    @Override
    public double getValRendimColectavel() {
        return this.rendimColectavel;
    }

    @Override
    public double getValRendimIsentos() {
        return this.rendimIsentSE;
    }

    @Override
    public double getValCoefConjugal() {
        return this.coefConjugal;
    }

    @Override
    public double getValTaxaImposto() {
        return this.txImposto * 100.0;
    }

    @Override
    public double getValImportApurada() {
        return this.importApurada;
    }

    @Override
    public double getValParcelaAbater() {
        return this.parcelaAbater;
    }

    @Override
    public double getValorColRendIsent() {
        return this.colectaRendimIsent;
    }

    @Override
    public double getValorApurado() {
        return this.colectaRendimSujeitos;
    }

    @Override
    public double getValImpTribAutonoma() {
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
            return this.colectaTribAutCatG + this.colectaTribAutCatF;
        }
        return this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas;
    }

    @Override
    public double getValColectaTotal() {
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
            return this.colectaTotal;
        }
        return this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas;
    }

    @Override
    public double getValDeduColecta() {
        if (this.valDeducoesColecta > this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR) {
            return this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR;
        }
        return this.valDeducoesColecta;
    }

    @Override
    public double getValAcrescColecta() {
        return this.acrescimosColecta;
    }

    @Override
    public double getValColectaLiquida() {
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
            return this.colectaTotal + this.acrescimosColecta;
        }
        double aux1 = 0.0;
        double aux2 = 0.0;
        aux1 = this.valDeducoesColecta > this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas ? this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas : this.valDeducoesColecta;
        aux2 = this.colectaMinExistencia == this.colectaAposDeducoes ? this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas - aux1 : this.colectaMinExistencia + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas;
        if (aux2 + this.acrescimosColecta > 0.0) {
            return aux2 + this.acrescimosColecta;
        }
        return 0.0;
    }

    @Override
    public double getValPagamentoConta() {
        return this.pagamentosConta;
    }

    @Override
    public double getValRetencoesFonte() {
        return this.retencoesFonte;
    }

    @Override
    public double getValImpostoApurado() {
        return this.impostoLiquidado;
    }

    @Override
    public boolean getMinExistencia() {
        if (this.colectaMinExistencia != this.colectaAposDeducoes) {
            return true;
        }
        return false;
    }

    @Override
    public double getImpostoPago() {
        return this.ImpostoPago;
    }

    @Override
    public double getImpostoRecebido() {
        return this.ImpostoRecebido;
    }

    @Override
    public double getValQuoRendAnter() {
        return this.quoRendAnosAnt;
    }

    @Override
    public double getValImpostAnosAnt() {
        return this.colectaAnosAnter;
    }

    @Override
    public double getValBenFiscalSF() {
        return 0.0;
    }

    @Override
    public double getValStRendimento() {
        return this.StRendimento;
    }

    @Override
    public double getValStDeducoes() {
        return this.StDeducoes;
    }

    @Override
    public double getValStColecta() {
        return this.StColecta;
    }

    @Override
    public double getValStRetencoes() {
        return this.StRetencoes;
    }

    @Override
    public double getValStRendimentoXTaxa() {
        return this.StRendimentoXTaxa;
    }

    @Override
    public double getValTaxaAdicional() {
        return this.colectaAdicional;
    }
}

