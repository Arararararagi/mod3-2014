/*
 * Decompiled with CFR 0_102.
 */
package calculo.business.logic;

import calculo.business.Modelo3;
import calculo.business.Rubricas;
import calculo.business.RubricasMaisValia;
import calculo.business.logic.Calc2010;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;
import calculo.business.pojos.RubricaMaisValia;
import calculo.business.pojos.SPA;
import calculo.rubricas.AbatDedColecta;
import calculo.rubricas.MaisValias;

public class Calc2011
extends Calc2010 {
    public final String[][] servFinArray = new String[][]{{"0051", "0.045"}, {"0078", "0.0495"}, {"0116", "0.03"}, {"0400", "0.03"}, {"0442", "0"}, {"0523", "0.025"}, {"0566", "0.02"}, {"0590", "0"}, {"0647", "0"}, {"0680", "0.025"}, {"0752", "0.025"}, {"0825", "0.025"}, {"0906", "0.045"}, {"0990", "0.04"}, {"1007", "0"}, {"1015", "0"}, {"1198", "0.02"}, {"1287", "0.04"}, {"1350", "0.03"}, {"1384", "0.04"}, {"1392", "0.04"}, {"1414", "0.01"}, {"1554", "0.0475"}, {"1589", "0.04"}, {"1660", "0.03"}, {"1678", "0.025"}, {"1686", "0"}, {"1716", "0.03"}, {"1929", "0.045"}, {"1945", "0.04"}, {"1961", "0.045"}, {"1988", "0.04"}, {"2119", "0.04"}, {"2135", "0.04"}, {"2178", "0.04"}, {"2259", "0.045"}, {"2267", "0.03"}, {"2305", "0.03"}, {"2313", "0.03"}, {"2321", "0"}, {"2372", "0"}, {"2500", "0.02"}, {"2534", "0.03"}, {"2550", "0.04"}, {"2577", "0.025"}, {"2607", "0.025"}, {"2615", "0.02"}, {"2623", "0.01"}, {"2631", "0.04"}, {"2771", "0.005"}, {"3131", "0.04"}, {"3140", "0.04"}, {"3417", "0.045"}, {"3522", "0.0475"}, {"3603", "0.04"}, {"3611", "0.04"}, {"3654", "0.045"}, {"4200", "0.045"}};

    public Calc2011(Modelo3 m3) {
        super(m3);
    }

    @Override
    public double doSimulacao() {
        try {
            this.rendLiquiCategA = this.calcRendLiqCatA();
            this.acrescimosRendLiq = this.calcAcrescRendLiq();
            this.rendimIsentSE = this.calcRendIsentSujEng();
            this.rendLiquiCategE = this.calcRendLiqCatE();
            this.rendLiquiCategB = this.calcRendLiqCatB();
            this.rendLiquiCategF = this.calcRendLiqCatF();
            this.rendLiquiCategG = this.calcRendLiqCatG();
            this.rendLiquiCategH = this.calcRendLiqCatH();
            this.quoRendAnosAnt = this.calcQuoRendAnosAnter();
            this.rendimLiquido = this.rendLiquiCategA + this.rendLiquiCategB + this.rendLiquiCategE + this.rendLiquiCategF + this.rendLiquiCategG + this.rendLiquiCategH;
            if (this.rendimLiquido < 0.0) {
                this.rendimLiquido = 0.0;
            }
            this.rendimLiquido+=this.acrescimosRendLiq;
            this.rendimLiqAposDedu = this.rendimLiquido - this.deducRendLiquido;
            this.abatimentos = 0.0;
            this.rendimColectavel = this.rendimLiqAposDedu - this.abatimentos < 0.0 ? 0.0 : this.rendimLiqAposDedu - this.abatimentos;
            this.rendDetermtx = this.rendimColectavel + this.rendimIsentSE - this.quoRendAnosAnt;
            this.coefConjugal = this.calcCoefConjugal();
            this.valorRCC = this.calcRCC(this.rendDetermtx, this.coefConjugal);
            this.txImposto = this.calcTaxaImposto(this.valorRCC);
            this.importApurada = this.rendDetermtx == 0.0 ? 0.0 : this.rendDetermtx / this.coefConjugal * this.txImposto;
            this.parcelaAbater = this.calcParcelaAbat(this.valorRCC);
            this.valorColecta = (this.importApurada - this.parcelaAbater) * this.coefConjugal;
            this.colectaAnosAnter = this.quoRendAnosAnt > 0.0 ? this.valorColecta * this.quoRendAnosAnt / this.rendDetermtx : 0.0;
            this.colectaTribAutonomas = this.calcColectaDespTA();
            this.colectaActivFinanc = this.calcColActivFinanc(this.valorColecta, this.rendimLiquido, this.rendLiquiCategB);
            this.colectaActivFinancCorrig = this.escalaoContribuinte == 1 ? this.colectaActivFinanc / this.taxaReducColAcores_Esc1 : (this.escalaoContribuinte == 2 ? this.colectaActivFinanc / this.taxaReducColAcores_Esc2 : this.colectaActivFinanc / this.taxaReducColAcores);
            if (this.colectaActivFinanc > 0.0 && ((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '2') {
                this.valorColecta = this.valorColecta - this.colectaActivFinanc + this.colectaActivFinancCorrig;
            }
            this.colectaRendimIsent = this.valorColecta * this.rendimIsentSE == 0.0 ? 0.0 : this.valorColecta * this.rendimIsentSE / this.rendDetermtx;
            this.colectaRendimSujeitos = this.valorColecta - this.colectaRendimIsent + this.colectaAnosAnter;
            this.colectaGratificacoes = this.calcRendimGratifica();
            this.colectaPPA = this.calcColPPA();
            this.colectaResgateFPR = this.calcResgateFPR();
            this.colectaFuturoOpcoes = this.calcColFuturos();
            this.valDeducoesColecta = this.calculoDeducoesColecta(this.colectaRendimSujeitos, this.rendimLiquido, this.rendimIsentSE, this.acrescimosRendLiq, this.colectaGratificacoes, this.colectaFuturoOpcoes);
            this.valorDeduzidoSF = this.colectaRendimSujeitos - this.valDeducoesColecta > 0.0 ? this.recolhaSF(this.colectaRendimSujeitos - this.valDeducoesColecta) : 0.0;
            this.valDeducoesColecta+=this.valorDeduzidoSF;
            if (this.valDeducoesColecta > this.colectaRendimSujeitos) {
                this.valDeducoesColecta = this.colectaRendimSujeitos;
            }
            this.colectaAposDeducoes = this.colectaRendimSujeitos - this.valDeducoesColecta < 0.0 ? 0.0 : this.colectaRendimSujeitos - this.valDeducoesColecta;
            this.colectaDesportistas = 0.0;
            this.colectaMinExistencia = this.calcColectaMinExistencia(this.rendimIsentSE, this.colectaAposDeducoes, this.rendDetermtx, new Double(this.coefConjugal).intValue(), this.rendimColectavel, this.colectaFuturoOpcoes, this.colectaGratificacoes, this.colectaDesportistas, this.acrescimosRendLiq);
            this.acrescimosColecta = this.calcAcrescColecta();
            this.colectaMaisValNEngl = this.calcColMaisValNEng();
            this.colectaTribAutCatF = this.calcColTribAutCatF();
            this.colectaTribAutCatG = this.calcColTribAutCatG();
            this.colectaDespConfiden = 0.0;
            this.colectaDespRepresen = 0.0;
            this.colectaEncarViatura = 0.0;
            this.colectaTotal = this.colectaMinExistencia + this.acrescimosColecta + this.colectaGratificacoes + this.colectaDesportistas + this.colectaTribAutonomas + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaPPA + this.colectaResgateFPR + this.colectaDespConfiden + this.colectaDespRepresen + this.colectaEncarViatura + this.colectaTribAutCatG + this.colectaTribAutCatF;
            if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
                double gratifAux1 = this.modelo3.getVal("RenTrabDep", "valgratific", "SPA") + this.modelo3.getVal("RenTrabDep", "valgratific", "SPB") + this.modelo3.getVal("RenTrabDep", "valgratific", "D1") + this.modelo3.getVal("RenTrabDep", "valgratific", "D2") + this.modelo3.getVal("RenTrabDep", "valgratific", "D3") + this.modelo3.getVal("RenTrabDep", "valgratific", "SPF");
                this.colecta1 = (this.rendLiquiCategA + this.rendLiquiCategH + gratifAux1) * this.Taxa_Lib_Col1;
                this.colecta1B = this.rendLiquiCategB * this.Taxa_Lib_Col4;
                this.colecta3 = this.rendLiquiCategE * this.Taxa_Lib_Col3;
                this.colecta4 = (this.calcMaisValBensImoveis() + this.calcMaisValPropIntelect() + this.calcMaisValPosContratua()) * this.Taxa_Lib_Col4;
                this.colecta4a = this.calcIncremPatrimon() * this.Taxa_Lib_Col1;
                this.colecta6 = this.rendLiquiCategF * this.Taxa_Lib_Col6;
                this.colecta7 = this.calcMaisValPartesSociais() * this.Taxa_Lib_Col7;
                SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
                if (spa.getEnglobG() != '1') {
                    this.colectaTribAutCatG+=this.calcMaisValPartesSociaisNEnglob() * this.Taxa_Lib_Col7;
                }
                this.colectaTotal = this.colecta1 + this.colecta1B + this.colecta2 + this.colecta3 + this.colecta4 + this.colecta6 + this.colecta7 + this.colecta4a + this.colectaTribAutCatG + this.colectaTribAutCatF;
            }
            this.retencoesFonte = this.calcRetencoesFonte();
            this.pagamentosConta = this.calcPagamConta();
            if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() != '4') {
                this.StRendimento = this.calcSobretaxaRendimento();
                this.StDeducoes = this.calcSobretaxaDeducoes();
                this.StColecta = this.calcSobretaxaColecta();
                this.StRetencoes = this.calcSobretaxaRetencoes();
                this.StRendimentoXTaxa = this.calcSobretaxaRendimentoXTaxa();
            } else {
                this.StRendimento = 0.0;
                this.StDeducoes = 0.0;
                this.StColecta = 0.0;
                this.StRetencoes = 0.0;
                this.StRendimentoXTaxa = 0.0;
            }
            this.modelo3.resSimulacao = this.impostoLiquidado = this.colectaTotal - this.retencoesFonte - this.pagamentosConta + this.StColecta;
        }
        catch (Exception exc) {
            exc.printStackTrace();
        }
        return this.modelo3.resSimulacao;
    }

    public double calcMaisValPartesSociaisNEnglob() {
        double res = 0.0;
        double warrants = this.modelo3.getVal("OpFinancei", "vali4");
        double futOpcoes = this.modelo3.getVal("OpFinancei", "valoutros");
        double certificados = this.modelo3.getVal("OpFinancei", "valcertif");
        double totalRealiza = this.modelo3.getVal("MaisValias", "valtotRealiPartSoc");
        double totalAquisic = this.modelo3.getVal("MaisValias", "valtotAquisPartSoc");
        double totalDespEnc = this.modelo3.getVal("MaisValias", "valtotDespePartSoc");
        res = totalRealiza - totalAquisic - totalDespEnc;
        res = res + warrants + futOpcoes + certificados;
        return res;
    }

    @Override
    public double getValDeduColecta() {
        if (this.valDeducoesColecta > this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR) {
            return this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR;
        }
        return this.valDeducoesColecta - this.valorDeduzidoSF;
    }

    @Override
    public double getValBenFiscalSF() {
        return this.valorDeduzidoSF;
    }

    @Override
    public double getValColectaLiquida() {
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
            return this.colectaTotal + this.acrescimosColecta;
        }
        double aux1 = 0.0;
        double aux2 = 0.0;
        aux1 = this.valDeducoesColecta > this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas + this.colectaTribAutCatG + this.colectaTribAutCatF ? this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas + this.colectaTribAutCatG + this.colectaTribAutCatF : this.valDeducoesColecta;
        aux2 = this.colectaMinExistencia == this.colectaAposDeducoes ? this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas + this.colectaTribAutCatG + this.colectaTribAutCatF - aux1 : this.colectaMinExistencia + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas + this.colectaTribAutCatG + this.colectaTribAutCatF;
        if (aux2 + this.acrescimosColecta > 0.0) {
            return aux2 + this.acrescimosColecta;
        }
        return 0.0;
    }

    @Override
    public double getValColectaTotal() {
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
            return this.colectaTotal;
        }
        return this.colectaRendimSujeitos + this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas + this.colectaTribAutCatG + this.colectaTribAutCatF;
    }

    @Override
    public double getValImpTribAutonoma() {
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
            return this.colectaTribAutCatG + this.colectaTribAutCatF;
        }
        return this.colectaGratificacoes + this.colectaPPA + this.colectaResgateFPR + this.colectaFuturoOpcoes + this.colectaMaisValNEngl + this.colectaTribAutonomas + this.colectaTribAutCatG + this.colectaTribAutCatF;
    }

    @Override
    public double calcReinvParcImoveis(double totRealiza, double tempMaisVal) {
        double valReinv;
        double tempResult = 0.0;
        double valDivida = this.modelo3.getVal("MaisValias", "valdividEmp");
        if (totRealiza - valDivida - (valReinv = this.modelo3.getVal("MaisValias", "valreinvParc")) <= 0.0 || totRealiza - valDivida <= 0.0 || tempMaisVal <= 0.0) {
            return 0.0;
        }
        tempResult = tempMaisVal * (totRealiza - valDivida - valReinv) / (totRealiza - valDivida);
        return tempResult;
    }

    @Override
    public double calcQDFP(Contribuinte cont) {
        double dedQDFPaux1 = 0.0;
        double dedquotizOP = this.modelo3.getVal("RenTrabDep", "valquotizOP", cont.getIdCont());
        double dedqotizBAgr = this.modelo3.getVal("DespesaAgrB", "valQuotOrdProfA", cont.getIdCont());
        double dedqotizBP = this.modelo3.getVal("DespesaProB", "valQuotOrdProfP", cont.getIdCont());
        dedQDFPaux1 = dedquotizOP;
        if (this.usarRegrasCatA(this.modelo3.getContribuinte(cont.getIdCont()))) {
            dedQDFPaux1 = dedQDFPaux1 + dedqotizBAgr + dedqotizBP;
        }
        return dedQDFPaux1;
    }

    @Override
    public double calcRendLiqAnexoBAgr(Contribuinte cont) {
        double res = 0.0;
        double resRendimentos = this.modelo3.getVal("CategoriaB", "RendimAgrB", cont.getIdCont());
        double resparcRend = 0.0;
        double resEncargos = 0.0;
        double enc63 = this.modelo3.getVal("DespesaAgrB", "valDeslocacoesAB", cont.getIdCont());
        double encTot = 0.0;
        char enquadra = this.calcEnquadramento(cont);
        if (enquadra == '1') {
            resparcRend = this.Taxa_Vendas * this.modelo3.getVal("RendimAgrB", "valVendasMercadoA", cont.getIdCont()) + this.Taxa_Prest_Servicos * this.modelo3.getVal("RendimAgrB", "valOutrosServicosA", cont.getIdCont());
            res = (resparcRend+=this.Taxa_Vendas * this.modelo3.getVal("RendimAgrB", "valSubsidiosExplA", cont.getIdCont())) * this.Coef_Corr_Def_CatB;
        } else {
            resparcRend = resRendimentos * this.Coef_Corr_Def_CatB;
            resEncargos = this.modelo3.getVal("DespesaAgrB", "valValProfissionalAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valDespRepresentacaoAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valEncViaturaAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valCustosExistAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valOutrosEncgAB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valContObrSegSocA", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valQuotSindA", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaAgrB", "valQuotOrdProfA", cont.getIdCont());
            if (enc63 > 0.1 * resRendimentos) {
                enc63 = 0.1 * resRendimentos;
            }
            encTot = resEncargos + enc63;
            res = resparcRend - encTot;
        }
        return res*=this.Taxa_Rend_Agricolas_CD;
    }

    @Override
    public double calcColTribAutCatG() {
        double valorMaisValias = this.calcMaisValBensImoveisRec();
        double valorCalc = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (spa.getEnglobImvG() != '1') {
            valorCalc = valorMaisValias * 0.05;
        }
        if (valorCalc < 0.0) {
            valorCalc = 0.0;
        }
        return valorCalc;
    }

    @Override
    public double calcMaisValBensImoveis() {
        double res = 0.0;
        double valorTotal = 0.0;
        double resparc = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        RubricasMaisValia bensImoveis = (RubricasMaisValia)this.modelo3.getRubricas("BensImoveis");
        for (int i = 0; i < bensImoveis.getNumRubricas(); ++i) {
            RubricaMaisValia rmv = (RubricaMaisValia)bensImoveis.getRubrica(i);
            if (i < 4) {
                if (!rmv.isPreenchida()) continue;
                valorTotal = rmv.getValRealizacao();
                resparc = rmv.getMaisValiaBemImovel(((SPA)this.modelo3.getContribuinte("SPA")).getAnoSim());
                if (Integer.toString(i + 1).equals(((MaisValias)this.modelo3.getRubricas("MaisValias")).getBemAlienado())) {
                    res+=this.calcReinvParcImoveis(valorTotal, resparc);
                    continue;
                }
                res+=resparc;
                continue;
            }
            if (!rmv.isPreenchidaRec() || spa.getEnglobImvG() != '1') continue;
            valorTotal = rmv.getValRealizacaoRec();
            resparc = rmv.getMaisValiaBemImovelRec(((SPA)this.modelo3.getContribuinte("SPA")).getAnoSim());
            if (Integer.toString(i + 1).equals(((MaisValias)this.modelo3.getRubricas("MaisValias")).getBemAlienado())) {
                res+=this.calcReinvParcImoveis(valorTotal, resparc);
                continue;
            }
            res+=resparc;
        }
        return res;
    }

    @Override
    public double calcMaisValBensImoveisRec() {
        double res = 0.0;
        double valorTotal = 0.0;
        double resparc = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        RubricasMaisValia bensImoveis = (RubricasMaisValia)this.modelo3.getRubricas("BensImoveis");
        for (int i = 0; i < bensImoveis.getNumRubricas(); ++i) {
            RubricaMaisValia rmv = (RubricaMaisValia)bensImoveis.getRubrica(i);
            if (i < 4 || !rmv.isPreenchidaRec() || spa.getEnglobImvG() == '1') continue;
            valorTotal = rmv.getValRealizacaoRec();
            resparc = rmv.getMaisValiaBemImovelRec(((SPA)this.modelo3.getContribuinte("SPA")).getAnoSim());
            if (Integer.toString(i + 1).equals(((MaisValias)this.modelo3.getRubricas("MaisValias")).getBemAlienado())) {
                res+=this.calcReinvParcImoveis(valorTotal, resparc);
                continue;
            }
            res+=resparc;
        }
        return res;
    }

    @Override
    public double calcIncremPatrimon() {
        double res = 0.0;
        double indemnizDanos = this.modelo3.getVal("IncrePatrim", "valIndemDanos");
        double obrigNConcorr = this.modelo3.getVal("IncrePatrim", "valImpNConc");
        double acrPatNaoJustif1 = this.modelo3.getVal("IncrePatrim", "valAcrPatNaoJust1Rend");
        double acrPatNaoJustif3 = this.modelo3.getVal("IncrePatrim", "valAcrPatNaoJust3Rend");
        res = indemnizDanos + obrigNConcorr + acrPatNaoJustif1 + acrPatNaoJustif3;
        return res;
    }

    @Override
    public double calcIncremPatrimonRF() {
        double res = 0.0;
        double indemnizDanosRF = this.modelo3.getVal("IncrePatrim", "valIndemDanosRF");
        double obrigNConcorrRF = this.modelo3.getVal("IncrePatrim", "valImpNConcRF");
        double acrPatNaoJustif1RF = this.modelo3.getVal("IncrePatrim", "valAcrPatNaoJust1Ret");
        double acrPatNaoJustif3RF = this.modelo3.getVal("IncrePatrim", "valAcrPatNaoJust3Ret");
        res = indemnizDanosRF + obrigNConcorrRF + acrPatNaoJustif1RF + acrPatNaoJustif3RF;
        return res;
    }

    public double calcColTribAutCatF() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double valorC = 0.0;
        valorC = this.modelo3.getVal("Prediais", "valtotalRendasImv");
        double valorColecta = (valorC - this.modelo3.getVal("Prediais", "valdespManutencaoImv") - this.modelo3.getVal("Prediais", "valdespConservacaoImv") - this.modelo3.getVal("Prediais", "valtaxasAutarqImv") - this.modelo3.getVal("Prediais", "valcontribAutarqImv") - this.modelo3.getVal("Prediais", "valdespCondominioImv")) * 0.05;
        if (valorColecta < 0.0) {
            valorColecta = 0.0;
        }
        return valorColecta;
    }

    @Override
    public double calcRetencoesFonte() {
        double ret = 0.0;
        double retparc = 0.0;
        double re = 0.0;
        double reparc = 0.0;
        double retFonE = this.modelo3.getVal("Capitais", "valretFonteE");
        double retFonF = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (spa.getEnglobF() == '1') {
            retFonF = this.modelo3.getVal("Prediais", "valretFonteF") + this.modelo3.getVal("Prediais", "valretFonteFImv");
        } else {
            double retFImv = this.modelo3.getVal("Prediais", "valretFonteFImv");
            retFonF = this.modelo3.getVal("Prediais", "valretFonteF") + retFImv;
        }
        double retPreR = this.modelo3.getVal("PreReforma", "valretFontePreSPA") + this.modelo3.getVal("PreReforma", "valretFontePreSPB");
        double retFonG = this.calcIncremPatrimonRF();
        int f = 0;
        for (int k = 0; k < this.modelo3.getNumContribs(); ++k) {
            retparc = this.calcRetencoesCatA(this.modelo3.getContribuinte(k)) + this.calcRetencoesCatH(this.modelo3.getContribuinte(k));
            ret+=retparc;
        }
        while (f < this.modelo3.getNumContribs() - 1) {
            reparc = this.calcRetencoesCatB(this.modelo3.getContribuinte(f));
            re+=reparc;
            ++f;
        }
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
            if (retFonG > this.colecta4a) {
                retFonG = this.colecta4a;
            }
            if (retFonE > this.colecta3) {
                retFonE = this.colecta3;
            }
            if (ret + re > this.colecta1 + this.colecta1B) {
                ret = this.colecta1;
                re = this.colecta1B;
            }
        }
        ret = ret + re + retFonE + retFonF + retFonG + retPreR + this.calcRetenRendIsentos();
        return ret;
    }

    @Override
    public char calcEnquadramento(Contribuinte cont) {
        char enquadra = '1';
        double rendBrutoB = 0.0;
        double totalRendims = 0.0;
        double totalRendimsSP = 0.0;
        int i = 0;
        int j = 0;
        double resauxA = 0.0;
        double resaux1B = 0.0;
        double resaux2B = 0.0;
        double resaux2ProB = 0.0;
        double resaux2AgrB = 0.0;
        double resaux3B = 0.0;
        double resaux3ProB = 0.0;
        double resaux3AgrB = 0.0;
        double resauxH = 0.0;
        double rCatA = 0.0;
        double rCatH = 0.0;
        double rCatB = 0.0;
        double rCatE = this.rendLiquiCategE;
        double rCatF = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        rCatF = spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendas") + this.modelo3.getVal("Prediais", "valtotalRendasImv") : this.modelo3.getVal("Prediais", "valtotalRendas");
        double rCatG = this.getValorMaisVal();
        double rIsent = this.rendimIsentSE;
        double rAcres = this.acrescimosRendLiq;
        double valVendas = 0.0;
        if (rCatF < 0.0) {
            rCatF = 0.0;
        }
        while (i < this.modelo3.getNumContribs()) {
            resauxA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            rCatA+=resauxA;
            resauxH = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
            rCatH+=resauxH;
            ++i;
        }
        while (j < this.modelo3.getNumContribs() - 1) {
            resaux1B = this.calcRBAnexoB(this.modelo3.getContribuinte(j));
            rCatB+=resaux1B;
            resaux2ProB+=this.calcRBAnexoProC(this.modelo3.getContribuinte(j));
            resaux2AgrB+=this.calcRBAnexoAgrC(this.modelo3.getContribuinte(j));
            resaux3ProB+=this.calcRBAnexoProD(this.modelo3.getContribuinte(j));
            resaux3AgrB+=this.calcRBAnexoAgrD(this.modelo3.getContribuinte(j));
            ++j;
        }
        if (resaux2ProB > 0.0) {
            rCatB+=resaux2ProB;
        }
        if (resaux2AgrB > 0.0) {
            rCatB+=resaux2AgrB;
        }
        if (resaux3ProB > 0.0) {
            rCatB+=resaux3ProB;
        }
        if (resaux3AgrB > 0.0) {
            rCatB+=resaux3AgrB;
        }
        totalRendimsSP = this.calcRendBrutoCatA(cont) + this.calcRendBrutoCatH(cont) + this.calcRBAnexoB(cont);
        if (this.calcRBAnexoProC(cont) > 0.0) {
            totalRendimsSP+=this.calcRBAnexoProC(cont);
        }
        if (this.calcRBAnexoAgrC(cont) > 0.0) {
            totalRendimsSP+=this.calcRBAnexoAgrC(cont);
        }
        if (this.calcRBAnexoProD(cont) > 0.0) {
            totalRendimsSP+=this.calcRBAnexoProD(cont);
        }
        if (this.calcRBAnexoAgrD(cont) > 0.0) {
            totalRendimsSP+=this.calcRBAnexoAgrD(cont);
        }
        totalRendims = rCatA + rCatH + rCatB + rCatE + rCatF + rCatG + rIsent + rAcres;
        rendBrutoB = this.modelo3.getVal("CategoriaB", "RendimProB", cont.getIdCont()) + this.modelo3.getVal("CategoriaB", "RendimAgrB", cont.getIdCont()) + this.getValRendIsento("cod403", cont) + this.getValRendIsento("cod408", cont) + this.getValRendIsento("cod410", cont);
        valVendas = this.modelo3.getVal("RendimAgrB", "valVendasMercadoA", cont.getIdCont());
        valVendas+=this.modelo3.getVal("RendimProB", "valVendasMercadoP", cont.getIdCont());
        enquadra = cont.getnaturRendB() == '2' && rendBrutoB > this.Catb_acto_isolado ? '2' : '1';
        cont.setenquadramB(enquadra);
        return enquadra;
    }

    @Override
    public double getValPerdasRecup() {
        double perdasB = 0.0;
        double perdasF = 0.0;
        double perdasG = 0.0;
        double resG = this.getValorMaisVal();
        double resAnosAntG = this.modelo3.getVal("MaisValias", "valperdasAnosAntG");
        double resAnosAntF = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        resAnosAntF = this.modelo3.getVal("Prediais", "valperdasAnosAntF");
        double resF = 0.0;
        resF = spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendas") - this.modelo3.getVal("Prediais", "valdespManutencao") - this.modelo3.getVal("Prediais", "valdespConservacao") - this.modelo3.getVal("Prediais", "valtaxasAutarq") - this.modelo3.getVal("Prediais", "valcontribAutarq") - this.modelo3.getVal("Prediais", "valdespCondominio") + (this.modelo3.getVal("Prediais", "valtotalRendasImv") - this.modelo3.getVal("Prediais", "valdespManutencaoImv") - this.modelo3.getVal("Prediais", "valdespConservacaoImv") - this.modelo3.getVal("Prediais", "valtaxasAutarqImv") - this.modelo3.getVal("Prediais", "valcontribAutarqImv") - this.modelo3.getVal("Prediais", "valdespCondominioImv")) : this.modelo3.getVal("Prediais", "valtotalRendas") - this.modelo3.getVal("Prediais", "valdespManutencao") - this.modelo3.getVal("Prediais", "valdespConservacao") - this.modelo3.getVal("Prediais", "valtaxasAutarq") - this.modelo3.getVal("Prediais", "valcontribAutarq") - this.modelo3.getVal("Prediais", "valdespCondominio");
        perdasF = resF > 0.0 ? (resF - resAnosAntF < 0.0 ? resF : resAnosAntF) : 0.0;
        perdasG = resG > 0.0 ? (resG - resAnosAntG < 0.0 ? resG : resAnosAntG) : 0.0;
        return perdasB + perdasF + perdasG;
    }

    @Override
    public double getValDedEspecificas() {
        int j = 0;
        double resRBA = 0.0;
        double resRBH = 0.0;
        double resdedA = 0.0;
        double resdedH = 0.0;
        double rDedA = 0.0;
        double rDedH = 0.0;
        double rDedF = 0.0;
        double rCatF = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        rCatF = spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendas") + this.modelo3.getVal("Prediais", "valtotalRendasImv") : this.modelo3.getVal("Prediais", "valtotalRendas");
        if (rCatF < 0.0) {
            rCatF = 0.0;
        }
        while (j < this.modelo3.getNumContribs()) {
            resRBA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(j));
            resdedA = this.calcDeducEspecificaLiq(this.modelo3.getContribuinte(j));
            if (resdedA > resRBA) {
                resdedA = resRBA;
            }
            rDedA+=resdedA;
            resRBH = this.calcRendBrutoCatH(this.modelo3.getContribuinte(j));
            resdedH = this.calcDeducEspecificaH(this.modelo3.getContribuinte(j));
            if (resdedH > resRBH) {
                resdedH = resRBH;
            }
            rDedH+=resdedH;
            ++j;
        }
        if (spa.getReside() == '4') {
            rDedA = 0.0;
        }
        rDedF = spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valdespManutencao") + this.modelo3.getVal("Prediais", "valdespConservacao") + this.modelo3.getVal("Prediais", "valtaxasAutarq") + this.modelo3.getVal("Prediais", "valcontribAutarq") + this.modelo3.getVal("Prediais", "valdespCondominio") + this.modelo3.getVal("Prediais", "valdespManutencaoImv") + this.modelo3.getVal("Prediais", "valdespConservacaoImv") + this.modelo3.getVal("Prediais", "valtaxasAutarqImv") + this.modelo3.getVal("Prediais", "valcontribAutarqImv") + this.modelo3.getVal("Prediais", "valdespCondominioImv") : this.modelo3.getVal("Prediais", "valdespManutencao") + this.modelo3.getVal("Prediais", "valdespConservacao") + this.modelo3.getVal("Prediais", "valtaxasAutarq") + this.modelo3.getVal("Prediais", "valcontribAutarq") + this.modelo3.getVal("Prediais", "valdespCondominio");
        if (rDedF > rCatF) {
            rDedF = rCatF;
        }
        return rDedA + rDedH + rDedF;
    }

    @Override
    public double calcDedPPRPPE(double rendimIsentSE, double acrescimosRendLiq, double colectaGratificacoes, double colectaFuturoOpcoes) {
        double rbh;
        double palh;
        double psbv;
        double rtvh;
        double valorDedPPRA = 0.0;
        double valorDedPPRB = 0.0;
        double valorDedPPRF = 0.0;
        double valPPRPPETot = 0.0;
        double valRBrutoF = 0.0;
        SPA spa2 = (SPA)this.modelo3.getContribuinte("SPA");
        valRBrutoF = spa2.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendas") + this.modelo3.getVal("Prediais", "valtotalRendasImv") : this.modelo3.getVal("Prediais", "valtotalRendas");
        Contribuinte spa = this.modelo3.getContribuinte("SPA");
        Contribuinte spb = this.modelo3.getContribuinte("SPB");
        Contribuinte spf = this.modelo3.getContribuinte("SPF");
        int valIdadeSujeitoA = spa.getIdade();
        int valIdadeSujeitoB = spb.getIdade();
        int valIdadeSujeitoF = spf.getIdade();
        double valorRBAgregado = this.calcValTRB(spa) + this.calcValTRB(spb) + this.calcValTRB(spf) + valRBrutoF + this.calcRendLiqCatE() + rendimIsentSE + acrescimosRendLiq + colectaFuturoOpcoes;
        double valAuxG = this.getValorMaisVal();
        boolean spaExclusPensionista = this.calcRendBrutoCatA(spa) == 0.0 && this.calcRendBrutoCatH(spa) == this.modelo3.getVal("Pensoes", "valrendBrutoCatH", spa.getIdCont()) && this.modelo3.getVal("Pensoes", "valrendBrutoCatH", spa.getIdCont()) != 0.0;
        boolean spbExclusPensionista = this.calcRendBrutoCatA(spb) == 0.0 && this.calcRendBrutoCatH(spb) == this.modelo3.getVal("Pensoes", "valrendBrutoCatH", spb.getIdCont()) && this.modelo3.getVal("Pensoes", "valrendBrutoCatH", spb.getIdCont()) != 0.0;
        boolean spfExclusPensionista = this.calcRendBrutoCatA(spf) == 0.0 && this.calcRendBrutoCatH(spf) == this.modelo3.getVal("Pensoes", "valrendBrutoCatH", spf.getIdCont()) && this.modelo3.getVal("Pensoes", "valrendBrutoCatH", spf.getIdCont()) != 0.0;
        valorRBAgregado = valorRBAgregado + valAuxG + this.rendLiquiCategB;
        if (!spaExclusPensionista) {
            rbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", this.modelo3.getContribuinte(0).getIdCont());
            rtvh = this.modelo3.getVal("Pensoes", "valrendTempVit", this.modelo3.getContribuinte(0).getIdCont());
            palh = this.modelo3.getVal("Pensoes", "valrendPensAlim", this.modelo3.getContribuinte(0).getIdCont());
            psbv = this.modelo3.getVal("Pensoes", "PSobrev", this.modelo3.getContribuinte(0).getIdCont());
            if (rbh == 0.0 && (rtvh != 0.0 || palh != 0.0 || psbv != 0.0)) {
                valorDedPPRA = this.calcValDedPPR(this.modelo3.getContribuinte(0).getIdCont(), valIdadeSujeitoA);
            }
        } else {
            valorDedPPRA = 0.0;
        }
        if (!spbExclusPensionista) {
            rbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", this.modelo3.getContribuinte(1).getIdCont());
            rtvh = this.modelo3.getVal("Pensoes", "valrendTempVit", this.modelo3.getContribuinte(1).getIdCont());
            palh = this.modelo3.getVal("Pensoes", "valrendPensAlim", this.modelo3.getContribuinte(1).getIdCont());
            psbv = this.modelo3.getVal("Pensoes", "PSobrev", this.modelo3.getContribuinte(1).getIdCont());
            if (rbh == 0.0 && (rtvh != 0.0 || palh != 0.0 || psbv != 0.0)) {
                valorDedPPRB = this.calcValDedPPR(this.modelo3.getContribuinte(1).getIdCont(), valIdadeSujeitoB);
            }
        } else {
            valorDedPPRB = 0.0;
        }
        if (!spfExclusPensionista) {
            rbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", "SPF");
            rtvh = this.modelo3.getVal("Pensoes", "valrendTempVit", "SPF");
            palh = this.modelo3.getVal("Pensoes", "valrendPensAlim", "SPF");
            psbv = this.modelo3.getVal("Pensoes", "PSobrev", "SPF");
            if (rbh == 0.0 && (rtvh != 0.0 || palh != 0.0 || psbv != 0.0)) {
                valorDedPPRF = this.calcValDedPPR("SPF", valIdadeSujeitoF);
            }
        } else {
            valorDedPPRF = 0.0;
        }
        valPPRPPETot = valorDedPPRA + valorDedPPRB + valorDedPPRF;
        return valPPRPPETot;
    }

    @Override
    public double calcRBLimAgr() {
        double res = 0.0;
        int i = 0;
        int j = 0;
        double resauxA = 0.0;
        double resaux1B = 0.0;
        double resaux2B = 0.0;
        double resaux2ProB = 0.0;
        double resaux2AgrB = 0.0;
        double resaux3B = 0.0;
        double resaux3ProB = 0.0;
        double resaux3AgrB = 0.0;
        double resauxH = 0.0;
        double rCatA = 0.0;
        double rCatH = 0.0;
        double rCatB = 0.0;
        double rCatE = this.rendLiquiCategE;
        double rCatF = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        rCatF = spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendas") + this.modelo3.getVal("Prediais", "valtotalRendasImv") : this.modelo3.getVal("Prediais", "valtotalRendas");
        double rCatG = this.getValorMaisVal();
        if (rCatF < 0.0) {
            rCatF = 0.0;
        }
        while (i < this.modelo3.getNumContribs()) {
            resauxA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            rCatA+=resauxA;
            resauxH = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
            rCatH+=resauxH;
            ++i;
        }
        while (j < this.modelo3.getNumContribs() - 1) {
            resaux1B = this.calcRBAnexoB(this.modelo3.getContribuinte(j));
            rCatB+=resaux1B;
            resaux2ProB+=this.calcRBAnexoProC(this.modelo3.getContribuinte(j));
            resaux2AgrB+=this.calcRBAnexoAgrC(this.modelo3.getContribuinte(j));
            resaux3ProB+=this.calcRBAnexoProD(this.modelo3.getContribuinte(j));
            resaux3AgrB+=this.calcRBAnexoAgrD(this.modelo3.getContribuinte(j));
            ++j;
        }
        if (resaux2ProB > 0.0) {
            rCatB+=resaux2ProB;
        }
        if (resaux2AgrB > 0.0) {
            rCatB+=resaux2AgrB;
        }
        if (resaux3ProB > 0.0) {
            rCatB+=resaux3ProB;
        }
        if (resaux3AgrB > 0.0) {
            rCatB+=resaux3AgrB;
        }
        res = rCatA + rCatH + rCatB + rCatE + rCatF + rCatG;
        return res;
    }

    @Override
    public double calcRendLiqCatF() {
        double res = 0.0;
        double resparc = 0.0;
        double resAnosAntF = this.modelo3.getVal("Prediais", "valperdasAnosAntF");
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        res = spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendas") - this.modelo3.getVal("Prediais", "valdespManutencao") - this.modelo3.getVal("Prediais", "valdespConservacao") - this.modelo3.getVal("Prediais", "valtaxasAutarq") - this.modelo3.getVal("Prediais", "valcontribAutarq") - this.modelo3.getVal("Prediais", "valdespCondominio") + (this.modelo3.getVal("Prediais", "valtotalRendasImv") - this.modelo3.getVal("Prediais", "valdespManutencaoImv") - this.modelo3.getVal("Prediais", "valdespConservacaoImv") - this.modelo3.getVal("Prediais", "valtaxasAutarqImv") - this.modelo3.getVal("Prediais", "valcontribAutarqImv") - this.modelo3.getVal("Prediais", "valdespCondominioImv")) + this.calcSublocacao() : this.modelo3.getVal("Prediais", "valtotalRendas") - this.modelo3.getVal("Prediais", "valdespManutencao") - this.modelo3.getVal("Prediais", "valdespConservacao") - this.modelo3.getVal("Prediais", "valtaxasAutarq") - this.modelo3.getVal("Prediais", "valcontribAutarq") - this.modelo3.getVal("Prediais", "valdespCondominio") + this.calcSublocacao();
        if (res > 0.0) {
            resparc = res - resAnosAntF;
            if (resparc < 0.0) {
                return 0.0;
            }
            return resparc;
        }
        return 0.0;
    }

    @Override
    public double getValRendimGlobal() {
        int i = 0;
        double resauxA = 0.0;
        double resauxH = 0.0;
        double rCatA = 0.0;
        double rCatH = 0.0;
        double rCatE = this.rendLiquiCategE;
        double rCatF = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        rCatF = spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendas") + this.modelo3.getVal("Prediais", "valtotalRendasImv") + this.calcSublocacao() : this.modelo3.getVal("Prediais", "valtotalRendas") + this.calcSublocacao();
        double rCatG = this.getValorMaisVal();
        double rCatB = this.rendLiquiCategB;
        if (rCatF < 0.0) {
            rCatF = 0.0;
        }
        while (i < this.modelo3.getNumContribs()) {
            resauxA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            rCatA+=resauxA;
            resauxH = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
            rCatH+=resauxH;
            ++i;
        }
        return rCatA + rCatH + rCatB + rCatE + rCatF + this.acrescimosRendLiq + rCatG;
    }

    @Override
    public double calcRendDeclaradosE() {
        double totalCapitais = this.modelo3.getVal("Capitais", "valJurosDepositos") + this.modelo3.getVal("Capitais", "valJurosPremios") + this.modelo3.getVal("Capitais", "valJurosSuprimento") + this.modelo3.getVal("Capitais", "valLucrosAdianta") + this.modelo3.getVal("Capitais", "valoutrosCapitais1") + this.modelo3.getVal("Capitais", "valoutrosCapitais2") + this.modelo3.getVal("Capitais", "valRendFundCapRisco");
        return totalCapitais;
    }

    @Override
    public double calcRendLiqAnexoBPro(Contribuinte cont) {
        double res = 0.0;
        double resRendimentos = this.modelo3.getVal("CategoriaB", "RendimProB", cont.getIdCont());
        double resEncargos = 0.0;
        double resparcRend = 0.0;
        double enc63 = this.modelo3.getVal("DespesaProB", "valDeslocacoesPB", cont.getIdCont());
        double encTot = 0.0;
        char enquadra = this.calcEnquadramento(cont);
        if (enquadra == '1') {
            resparcRend = this.Taxa_Vendas * this.modelo3.getVal("RendimProB", "valVendasMercadoP", cont.getIdCont());
            resparcRend+=this.Taxa_Vendas * this.modelo3.getVal("RendimProB", "valServicosHotP", cont.getIdCont());
            resparcRend+=this.Taxa_Prest_Servicos * this.modelo3.getVal("RendimProB", "valOutrosServicosP", cont.getIdCont());
            resparcRend+=this.Taxa_Prest_Servicos * this.modelo3.getVal("RendimProB", "valRendimActFinP", cont.getIdCont());
            resparcRend+=this.modelo3.getVal("RendimProB", "valServPrestSocP", cont.getIdCont());
            resparcRend+=this.Taxa_Prest_Servicos * this.modelo3.getVal("RendimProB", "valRendPredImp", cont.getIdCont());
            resparcRend+=this.Taxa_Prest_Servicos * this.modelo3.getVal("RendimProB", "valRendCapImp", cont.getIdCont());
            resparcRend+=this.Taxa_Prest_Servicos_Red * this.modelo3.getVal("RendimProB", "valPropriedadeIntP", cont.getIdCont());
            if (this.modelo3.getVal("RendimProB", "valMicroEletr", cont.getIdCont()) >= this.Microproducao_Electric_Lim) {
                resparcRend+=this.Taxa_Vendas * this.modelo3.getVal("RendimProB", "valMicroEletr", cont.getIdCont());
            }
            res = resparcRend * this.Coef_Corr_Def_CatB;
        } else {
            resparcRend = resRendimentos * this.Coef_Corr_Def_CatB;
            resEncargos = this.modelo3.getVal("DespesaProB", "valValProfissionalPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valDespRepresentacaoPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valEncViaturaPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valCustosExistPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valOutrosEncgPB", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valContObrSegSocP", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valQuotSindP", cont.getIdCont());
            resEncargos+=this.modelo3.getVal("DespesaProB", "valQuotOrdProfP", cont.getIdCont());
            if (enc63 > 0.1 * resRendimentos) {
                enc63 = 0.1 * resRendimentos;
            }
            encTot = resEncargos + enc63;
            res = resparcRend - encTot;
        }
        return res;
    }

    @Override
    public double calcDedEncImoveis() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double valH1 = this.modelo3.getVal("AbatDedColecta", "valjurosDiviImovHabi");
        double valH3 = this.modelo3.getVal("AbatDedColecta", "valimportRendasHabiP") + this.modelo3.getVal("AbatDedColecta", "valRendasLocFinan");
        double val30DH1 = this.DC_ImovER_Taxa * valH1;
        double val30DH3 = this.DC_ImovER_Taxa * valH3;
        double valLimDH1 = 0.0;
        double valLimDH3 = 0.0;
        double valLimFinal = 0.0;
        double valDedImoveisTot = 0.0;
        double isClassA = this.modelo3.getVal("BenefFiscais", "classEnergetica");
        if (spa.getEstadoCivil() == '3') {
            if (isClassA == 0.0) {
                if (this.escalaoContribuinte == 1) {
                    valLimDH1 = this.DC_ImovJur_SepFact_Esc1_Valor;
                } else if (this.escalaoContribuinte == 2) {
                    valLimDH1 = this.DC_ImovJur_SepFact_Esc2_Valor;
                } else if (this.escalaoContribuinte == 3) {
                    valLimDH1 = this.DC_ImovJur_SepFact_Esc3_Valor;
                } else if (this.escalaoContribuinte == 4) {
                    valLimDH1 = this.DC_ImovJur_SepFact_Esc4_Valor;
                } else if (this.escalaoContribuinte == 5) {
                    valLimDH1 = this.DC_ImovJur_SepFact_Esc5_Valor;
                } else if (this.escalaoContribuinte == 6) {
                    valLimDH1 = this.DC_ImovJur_SepFact_Esc6_Valor;
                } else if (this.escalaoContribuinte == 7) {
                    valLimDH1 = this.DC_ImovJur_SepFact_Esc7_Valor;
                } else if (this.escalaoContribuinte == 8) {
                    valLimDH1 = this.DC_ImovJur_SepFact_Esc8_Valor;
                }
            } else if (this.escalaoContribuinte == 1) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc1_A_Valor;
            } else if (this.escalaoContribuinte == 2) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc2_A_Valor;
            } else if (this.escalaoContribuinte == 3) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc3_A_Valor;
            } else if (this.escalaoContribuinte == 4) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc4_A_Valor;
            } else if (this.escalaoContribuinte == 5) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc5_A_Valor;
            } else if (this.escalaoContribuinte == 6) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc6_A_Valor;
            } else if (this.escalaoContribuinte == 7) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc7_A_Valor;
            } else if (this.escalaoContribuinte == 8) {
                valLimDH1 = this.DC_ImovJur_SepFact_Esc8_A_Valor;
            }
            valLimDH3 = this.DC_ImovER_SepFact_DH3_Valor;
        } else {
            if (isClassA == 0.0) {
                if (this.escalaoContribuinte == 1) {
                    valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc1_Valor;
                } else if (this.escalaoContribuinte == 2) {
                    valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc2_Valor;
                } else if (this.escalaoContribuinte == 3) {
                    valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc3_Valor;
                } else if (this.escalaoContribuinte == 4) {
                    valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc4_Valor;
                } else if (this.escalaoContribuinte == 5) {
                    valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc5_Valor;
                } else if (this.escalaoContribuinte == 6) {
                    valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc6_Valor;
                } else if (this.escalaoContribuinte == 7) {
                    valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc7_Valor;
                } else if (this.escalaoContribuinte == 8) {
                    valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc8_Valor;
                }
            } else if (this.escalaoContribuinte == 1) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc1_A_Valor;
            } else if (this.escalaoContribuinte == 2) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc2_A_Valor;
            } else if (this.escalaoContribuinte == 3) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc3_A_Valor;
            } else if (this.escalaoContribuinte == 4) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc4_A_Valor;
            } else if (this.escalaoContribuinte == 5) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc5_A_Valor;
            } else if (this.escalaoContribuinte == 6) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc6_A_Valor;
            } else if (this.escalaoContribuinte == 7) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc7_A_Valor;
            } else if (this.escalaoContribuinte == 8) {
                valLimDH1 = this.DC_ImovJur_OutrAgreg_Esc8_A_Valor;
            }
            valLimDH3 = this.DC_ImovER_OutrAgreg_DH3_Valor;
        }
        if (valH1 <= 0.0) {
            val30DH1 = 0.0;
        }
        if (valH3 <= 0.0) {
            val30DH3 = 0.0;
        }
        if (val30DH1 > 0.0) {
            valLimFinal = val30DH3 > 0.0 ? (valLimDH1 > valLimDH3 ? valLimDH1 : valLimDH3) : valLimDH1;
        } else if (val30DH3 > 0.0) {
            valLimFinal = val30DH1 > 0.0 ? (valLimDH1 > valLimDH3 ? valLimDH1 : valLimDH3) : valLimDH3;
        }
        if (valH1 <= 0.0) {
            val30DH1 = 0.0;
        }
        if (valH3 <= 0.0) {
            val30DH3 = 0.0;
        }
        valDedImoveisTot = val30DH1 + val30DH3 > valLimFinal ? valLimFinal : val30DH1 + val30DH3;
        return valDedImoveisTot;
    }

    @Override
    public double calcDeducEspecifica(Contribuinte cont) {
        double dedparc = 0.0;
        double rbparc = 0.0;
        double difparc = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (spa.getReside() == '4') {
            return 0.0;
        }
        dedparc = this.calcDeducao1Especifica(cont) + this.modelo3.getVal("RenTrabDep", "valindmnRescUni", cont.getIdCont()) + this.modelo3.getVal("RenTrabDep", "valseguDesg", cont.getIdCont());
        rbparc = this.calcRendBrutoCatA(cont);
        difparc = rbparc - dedparc;
        if (difparc > 0.0) {
            this.quotCatA = this.calcDeducaoQuotizaA(cont);
            if (this.quotCatA > difparc) {
                this.quotCatA = difparc;
            }
        } else {
            this.quotCatA = 0.0;
        }
        return dedparc+=this.quotCatA;
    }

    @Override
    public double calcDedDonativOutr(double colectaRendimSujeitos) {
        double valLimDonatOutr = this.DC_Donat_OutrEnt_Limite_TaxaSobreColecta * colectaRendimSujeitos;
        double valCampoDonatOutr = this.modelo3.getVal("AbatDedColecta", "valdonativosOutEntid");
        double valCampoDonatRelig = this.modelo3.getVal("AbatDedColecta", "valdonativosLibRelig");
        double valCampoDonatCatol = this.modelo3.getVal("AbatDedColecta", "valdonativosIgrCatolica");
        double valCampoDoMecCient = this.modelo3.getVal("BenefFiscais", "valmecenatoCientifico");
        double valCampoDoMecCult = this.modelo3.getVal("BenefFiscais", "valmecenatoCultural");
        double valCampoDoMecCPlu = this.modelo3.getVal("BenefFiscais", "valcontratosPluriA");
        double valCampoDoMecSoc = this.modelo3.getVal("BenefFiscais", "valmecenatoSocial");
        double valCampoDoMecSocA = this.modelo3.getVal("BenefFiscais", "valmecenatoSocEsp");
        double valCampoDoMecFami = this.modelo3.getVal("BenefFiscais", "valmecenatoFamiliar");
        double valCampoDoMecECien = this.modelo3.getVal("BenefFiscais", "valestadoMecCientifico");
        double valCampoDoMecECult = this.modelo3.getVal("BenefFiscais", "valestadoMecCultural");
        double valCampoDoMecECPlu = this.modelo3.getVal("BenefFiscais", "valestadoContPluri");
        double valCampoDoMecESoc = this.modelo3.getVal("BenefFiscais", "valestadoMecSocial");
        double valCampoDoMecEFam = this.modelo3.getVal("BenefFiscais", "valestadoMecFamiliar");
        double valDonatOutr25 = 0.0;
        double valDonatOutrTot = 0.0;
        valDonatOutr25 = valCampoDoMecCult * this.Taxa_Major_Don_Cult + valCampoDoMecCPlu * this.Taxa_Major_Don_ECPlu + valCampoDoMecSoc * this.Taxa_Major_Don_MecSoc + valCampoDoMecSocA * this.Taxa_Major_Don_Soc + valCampoDonatCatol * this.Taxa_Major_Don_Relig + valCampoDoMecFami * this.Taxa_Major_Don_Fam + valCampoDoMecCient + valCampoDonatRelig * this.Taxa_Major_Don_Relig;
        valDonatOutrTot = (valDonatOutr25 = this.DC_Donat_OutrEnt_Taxa * valDonatOutr25) > valLimDonatOutr ? valLimDonatOutr : valDonatOutr25;
        return valDonatOutrTot+=(valCampoDonatOutr + valCampoDoMecECien + valCampoDoMecECult * this.Taxa_Major_Don_Cult + valCampoDoMecECPlu * this.Taxa_Major_Don_ECPlu + valCampoDoMecESoc * this.Taxa_Major_Don_MecSoc + valCampoDoMecEFam * this.Taxa_Major_Don_Fam) * this.DC_Donat_OutrEnt_Taxa;
    }

    @Override
    public double calcRBAnexoProD(Contribuinte cont) {
        double rbd1 = this.modelo3.getVal("AnexoD", "valRendLiqImpP", cont.getIdCont());
        double rbd2 = this.modelo3.getVal("AnexoD", "valAdContLucrosP", cont.getIdCont());
        double rbd3 = this.modelo3.getVal("AnexoD", "valAjustamentosP", cont.getIdCont());
        double rbdf = 0.0;
        rbdf = rbd2 > 0.0 && rbd2 > rbd1 ? rbd2 - rbd3 : rbd1 - rbd3;
        if (rbdf < 0.0) {
            rbdf = 0.0;
        }
        return rbdf;
    }

    @Override
    public double calcRendLiqAnexoDPro(Contribuinte cont) {
        double rbd1 = this.modelo3.getVal("AnexoD", "valRendLiqImpP", cont.getIdCont());
        double rbd2 = this.modelo3.getVal("AnexoD", "valAdContLucrosP", cont.getIdCont());
        double rbd3 = this.modelo3.getVal("AnexoD", "valAjustamentosP", cont.getIdCont());
        double rbdf = 0.0;
        rbdf = rbd2 > 0.0 && rbd2 > rbd1 ? rbd2 - rbd3 : rbd1 - rbd3;
        if (rbdf < 0.0) {
            rbdf = 0.0;
        }
        return rbdf;
    }

    @Override
    public double calcRBAnexoAgrD(Contribuinte cont) {
        double rbd1 = this.modelo3.getVal("AnexoD", "valRendLiqImpA", cont.getIdCont());
        double rbd2 = this.modelo3.getVal("AnexoD", "valAdContLucrosA", cont.getIdCont());
        double rbd3 = this.modelo3.getVal("AnexoD", "valAjustamentosA", cont.getIdCont());
        double rbdf = 0.0;
        rbdf = rbd2 > 0.0 && rbd2 > rbd1 ? rbd2 - rbd3 : rbd1 - rbd3;
        if (rbdf < 0.0) {
            rbdf = 0.0;
        }
        return rbdf;
    }

    @Override
    public double calcRendLiqAnexoDAgr(Contribuinte cont) {
        double res = 0.0;
        double rbd1 = this.modelo3.getVal("AnexoD", "valRendLiqImpA", cont.getIdCont());
        double rbd2 = this.modelo3.getVal("AnexoD", "valAdContLucrosA", cont.getIdCont());
        double rbd3 = this.modelo3.getVal("AnexoD", "valAjustamentosA", cont.getIdCont());
        double rbdf = 0.0;
        rbdf = rbd2 > 0.0 && rbd2 > rbd1 ? rbd2 - rbd3 : rbd1 - rbd3;
        if (rbdf < 0.0) {
            rbdf = 0.0;
        }
        res = this.Taxa_Rend_Agricolas_CD * rbdf;
        return res;
    }

    @Override
    public double calcDeducaoQuotizaA(Contribuinte cont) {
        double dedHQuotiza = 0.0;
        double dedHvalquot = this.modelo3.getVal("RenTrabDep", "valquotizSind", cont.getIdCont()) + this.modelo3.getVal("Pensoes", "valquotizSindCatH", cont.getIdCont());
        double rendBrutoCatA = this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont());
        double dedHrbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", cont.getIdCont());
        double palh = this.modelo3.getVal("Pensoes", "valrendPensAlim", cont.getIdCont());
        double pensSobr = this.modelo3.getVal("Pensoes", "PSobrev", cont.getIdCont());
        double rendBrutoPreReforma = 0.0;
        double dedHaux1 = 0.0;
        double dedqotizBAgr = 0.0;
        double dedqotizBP = 0.0;
        if (!cont.getIdCont().equals("SPF")) {
            dedqotizBAgr = this.modelo3.getVal("DespesaAgrB", "valQuotSindA", cont.getIdCont());
            dedqotizBP = this.modelo3.getVal("DespesaProB", "valQuotSindP", cont.getIdCont());
        }
        if (this.usarRegrasCatA(this.modelo3.getContribuinte(cont.getIdCont()))) {
            dedHvalquot = dedHvalquot + dedqotizBAgr + dedqotizBP;
        }
        if (!this.prereformaIsCatA(cont)) {
            if (cont.getIdCont().equals("SPA")) {
                rendBrutoPreReforma = this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
            }
            if (cont.getIdCont().equals("SPB")) {
                rendBrutoPreReforma = this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
            }
        }
        if (this.usarRegrasCatA(this.modelo3.getContribuinte(cont.getIdCont()))) {
            if (rendBrutoCatA + this.valorRendCatBTribViaCatA(cont) > 0.0 && (dedHQuotiza = 1.5 * dedHvalquot) > (dedHaux1 = this.Taxa_Quotz_Sindical * (rendBrutoCatA + this.valorRendCatBTribViaCatA(cont)))) {
                dedHQuotiza = dedHaux1;
            }
        } else if (rendBrutoCatA > 0.0 && (dedHQuotiza = 1.5 * dedHvalquot) > (dedHaux1 = this.Taxa_Quotz_Sindical * rendBrutoCatA)) {
            dedHQuotiza = dedHaux1;
        }
        return dedHQuotiza;
    }

    @Override
    public double calcDeducaoQuotizaH(Contribuinte cont) {
        double dedHQuotiza = 0.0;
        double dedHvalquot = this.modelo3.getVal("RenTrabDep", "valquotizSind", cont.getIdCont()) + this.modelo3.getVal("Pensoes", "valquotizSindCatH", cont.getIdCont());
        double rendBrutoCatA = this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont());
        double dedHrbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", cont.getIdCont());
        double palh = this.modelo3.getVal("Pensoes", "valrendPensAlim", cont.getIdCont());
        double pensSobr = this.modelo3.getVal("Pensoes", "PSobrev", cont.getIdCont());
        double rendBrutoPreReforma = 0.0;
        double dedHaux1 = 0.0;
        double dedHaux2 = 0.0;
        double dedQuotiza = 0.0;
        double dedAaux1 = 0.0;
        if (!this.prereformaIsCatA(cont)) {
            if (cont.getIdCont().equals("SPA")) {
                rendBrutoPreReforma = this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
            }
            if (cont.getIdCont().equals("SPB")) {
                rendBrutoPreReforma = this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
            }
        }
        if (dedHrbh + palh + pensSobr + rendBrutoPreReforma > 0.0) {
            if (rendBrutoCatA > 0.0) {
                if (this.usarRegrasCatA(this.modelo3.getContribuinte(cont.getIdCont()))) {
                    if (rendBrutoCatA + this.valorRendCatBTribViaCatA(cont) > 0.0 && (dedQuotiza = 1.5 * dedHvalquot) > (dedAaux1 = (this.quotCatA = this.calcDeducaoQuotizaA(cont))) && (dedHQuotiza = dedQuotiza - dedAaux1) > (dedHaux2 = this.Taxa_Quotz_Sindical * (dedHrbh + palh + pensSobr + rendBrutoPreReforma))) {
                        dedHQuotiza = dedHaux2;
                    }
                } else if (rendBrutoCatA > 0.0 && (dedQuotiza = 1.5 * dedHvalquot) > (dedAaux1 = (this.quotCatA = this.calcDeducaoQuotizaA(cont))) && (dedHQuotiza = dedQuotiza - dedAaux1) > (dedHaux2 = this.Taxa_Quotz_Sindical * (dedHrbh + palh + pensSobr + rendBrutoPreReforma))) {
                    dedHQuotiza = dedHaux2;
                }
            } else {
                dedHQuotiza = 1.5 * dedHvalquot;
                dedHaux1 = this.Taxa_Quotz_Sindical * (dedHrbh + palh + pensSobr + rendBrutoPreReforma);
                if (dedHQuotiza > dedHaux1) {
                    dedHQuotiza = dedHaux1;
                }
            }
        }
        return dedHQuotiza;
    }

    @Override
    public double calcCSS(Contribuinte cont) {
        double dedCSSaux1 = 0.0;
        double dedContObSS = 0.0;
        double dedqotizBAgr = 0.0;
        double dedqotizBP = 0.0;
        dedContObSS = cont.getIdCont().equals("SPA") ? this.modelo3.getVal("RenTrabDep", "valcontObrigSS", cont.getIdCont()) : this.modelo3.getVal("RenTrabDep", "valcontObrigSS", cont.getIdCont());
        if (!cont.getIdCont().equals("SPF")) {
            dedqotizBAgr = this.modelo3.getVal("DespesaAgrB", "valContObrSegSocA", cont.getIdCont());
            dedqotizBP = this.modelo3.getVal("DespesaProB", "valContObrSegSocP", cont.getIdCont());
        }
        dedCSSaux1 = this.usarRegrasCatA(this.modelo3.getContribuinte(cont.getIdCont())) ? dedContObSS + dedqotizBAgr + dedqotizBP : dedContObSS;
        return dedCSSaux1;
    }

    @Override
    public double calcDeducao1Especifica(Contribuinte cont) {
        double ded1Especifica = 0.0;
        double dedaux1 = 0.0;
        double dedaux2 = 0.0;
        double dedaux3 = 0.0;
        double dedaux4 = 0.0;
        double valorQuotOP = 0.0;
        valorQuotOP = this.usarRegrasCatA(cont) ? this.modelo3.getVal("RenTrabDep", "valquotizOP", cont.getIdCont()) + this.modelo3.getVal("DespesaAgrB", "valQuotOrdProfA", cont.getIdCont()) + this.modelo3.getVal("DespesaProB", "valQuotOrdProfP", cont.getIdCont()) : this.modelo3.getVal("RenTrabDep", "valquotizOP", cont.getIdCont());
        if (valorQuotOP == 0.0) {
            if (!cont.isDeficiente()) {
                dedaux1 = cont.getIdCont().equals("SPA") ? this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont()) : this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont());
                if (this.prereformaIsCatA(cont)) {
                    if (cont.getIdCont().equals("SPA")) {
                        dedaux1+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
                    }
                    if (cont.getIdCont().equals("SPB")) {
                        dedaux1+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
                    }
                }
                if (this.usarRegrasCatA(cont)) {
                    dedaux1+=this.valorRendCatBTribViaCatA(cont);
                }
                dedaux2 = (dedaux1+=this.getDifIsento404(cont)) > this.CatA_Ded1_Sem_QOPeDFP_Valor_NDef ? this.CatA_Ded1_Sem_QOPeDFP_Valor_NDef : dedaux1;
            } else {
                dedaux1 = cont.getIdCont().equals("SPA") ? this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont()) : this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont());
                if (this.usarRegrasCatA(cont)) {
                    dedaux1+=this.valorRendCatBTribViaCatA(cont);
                }
                dedaux2 = (dedaux1+=this.getDifIsento404(cont)) > this.CatA_Ded1_Sem_QOPeDFP_Valor_Def ? this.CatA_Ded1_Sem_QOPeDFP_Valor_Def : dedaux1;
            }
        } else {
            dedaux1 = cont.getIdCont().equals("SPA") ? this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont()) : this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont());
            if (this.prereformaIsCatA(cont)) {
                if (cont.getIdCont().equals("SPA")) {
                    dedaux1+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
                }
                if (cont.getIdCont().equals("SPB")) {
                    dedaux1+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
                }
            }
            if (this.usarRegrasCatA(cont)) {
                dedaux1+=this.valorRendCatBTribViaCatA(cont);
            }
            if (dedaux1 > this.CatA_Ded1_Sem_QOPeDFP_Valor_NDef) {
                dedaux1 = this.CatA_Ded1_Sem_QOPeDFP_Valor_NDef;
            }
            dedaux4 = this.calcRBAnexoB(cont);
            dedaux4+=this.calcRBAnexoProC(cont);
            dedaux4+=this.calcRBAnexoAgrC(cont);
            dedaux4+=this.calcRBAnexoProD(cont);
            dedaux2 = (dedaux1+=this.calcQDFP(cont)) > this.CatA_Ded1_Com_QOPouDFP_Valor_NDef && (dedaux4+=this.calcRBAnexoAgrD(cont)) == 0.0 ? this.CatA_Ded1_Com_QOPouDFP_Valor_NDef : (dedaux1 <= this.CatA_Ded1_Com_QOPouDFP_Valor_NDef && dedaux4 == 0.0 ? dedaux1 : dedaux1 - this.calcQDFP(cont));
        }
        dedaux3 = this.calcCSS(cont);
        ded1Especifica = dedaux2 > dedaux3 ? dedaux2 : dedaux3;
        return ded1Especifica;
    }

    @Override
    public double calcRendBrutoCatA(Contribuinte cont) {
        double rba = 0.0;
        double rb50 = 0.0;
        double rbtot = 0.0;
        rba = cont.getIdCont().equals("SPA") ? this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont()) : this.modelo3.getVal("RenTrabDep", "valrendBruto", cont.getIdCont());
        if (((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4') {
            rba = rba + this.modelo3.getVal("RendIsentos", "cod401", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod402", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod404", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod405", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod406", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod407", cont.getIdCont()) + this.modelo3.getVal("RendIsentos", "cod409", cont.getIdCont());
        }
        if (this.usarRegrasCatA(cont)) {
            rba+=this.valorRendCatBTribViaCatA(cont);
        }
        rba+=this.getDifIsento404(cont);
        if (this.prereformaIsCatA(cont)) {
            if (cont.getIdCont().equals("SPA")) {
                rba+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
            }
            if (cont.getIdCont().equals("SPB")) {
                rba+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
            }
        }
        if (!cont.isDeficiente()) {
            return rba;
        }
        if (cont.getIdCont().equals("D1") || cont.getIdCont().equals("D2") || cont.getIdCont().equals("D3")) {
            rbtot = rba;
        } else if (cont.getGrauDef() == '1' || cont.getGrauDef() == '2') {
            rb50 = 0.1 * rba;
            rbtot = rb50 > this.CatA_Rb_Def_60_80 ? rba - this.CatA_Rb_Def_60_80 : rba - rb50;
        }
        return rbtot;
    }

    @Override
    public double calcAbatimentos() {
        double abatimCalcul = 0.0;
        double abatPenJudic = this.modelo3.getVal("AbatDedColecta", "valpensoesObrigJudic");
        abatimCalcul = abatPenJudic * 0.2;
        if (abatimCalcul > this.Abat_Pensoes_Alimentos_Lim) {
            abatimCalcul = this.Abat_Pensoes_Alimentos_Lim;
        }
        return abatimCalcul;
    }

    @Override
    public double calculoDeducoesColecta(double colectaRendimSujeitos, double rendimLiquido, double rendimIsentSE, double acrescimosRendLiq, double colectaGratificacoes, double colectaFuturoOpcoes) {
        double valorDeducoesColecta = 0.0;
        double dedSPDepeAscend = this.calcDedSPDA();
        double dedPensoesObrig = this.calcAbatimentos();
        double dedDespesasEducDeficientes = this.calcDedDespEducReabDef();
        double dedSegurosDeficientes = this.calcDedPremiosSegurDef(colectaRendimSujeitos);
        double dedContribVelhiceDeficientes = this.calcDedContribVelhiceDef();
        double dedSujPassDeficientes = this.calcDedSPDef();
        double deducaoCentenarioRepublica = this.calculaDedCentRepublica();
        valorDeducoesColecta = this.calcLimDeducoesColecta() + this.calcLimBeneFiscais(colectaRendimSujeitos, rendimIsentSE, acrescimosRendLiq, colectaGratificacoes, colectaFuturoOpcoes) + dedSPDepeAscend + dedPensoesObrig + dedSujPassDeficientes + dedDespesasEducDeficientes + dedSegurosDeficientes + dedContribVelhiceDeficientes + deducaoCentenarioRepublica;
        return valorDeducoesColecta;
    }

    public double calcDedImovReab() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double reabImoveisArrend = this.modelo3.getVal("AbatDedColecta", "valReabImvArr");
        double valReabImov = this.Ded_Despesas_Imov_Reab_Tx * reabImoveisArrend;
        double valLimReabImov = this.Ded_Despesas_Imov_Reab;
        double isClassA = this.modelo3.getVal("BenefFiscais", "classEnergeticaR");
        double valorDeducao = 0.0;
        valorDeducao = reabImoveisArrend > 0.0 ? (valReabImov > valLimReabImov ? valLimReabImov : valReabImov) : 0.0;
        return valorDeducao;
    }

    @Override
    public double calcRendimGratifica() {
        double gratifAux1 = 0.0;
        double valorGratifica = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        gratifAux1 = this.modelo3.getVal("RenTrabDep", "valgratific", "SPA") + this.modelo3.getVal("RenTrabDep", "valgratific", "SPB") + this.modelo3.getVal("RenTrabDep", "valgratific", "D1") + this.modelo3.getVal("RenTrabDep", "valgratific", "D2") + this.modelo3.getVal("RenTrabDep", "valgratific", "D3") + this.modelo3.getVal("RenTrabDep", "valgratific", "SPF");
        if (spa.getReside() == '1' || spa.getReside() == '3') {
            valorGratifica = gratifAux1 * this.ColecGratif_Geral;
        } else if (spa.getReside() == '2') {
            valorGratifica = gratifAux1 * this.ColecGratif_Acores;
        }
        return valorGratifica;
    }

    @Override
    public double calcQuoRendAnosAnter() {
        double rendAntASPA = this.modelo3.getVal("RendAnosAnter", "valrendAntASPA");
        double rendAntASPB = this.modelo3.getVal("RendAnosAnter", "valrendAntASPB");
        double rendAntAD1 = this.modelo3.getVal("RendAnosAnter", "valrendAntAD1");
        double rendAntAD2 = this.modelo3.getVal("RendAnosAnter", "valrendAntAD2");
        double rendAntAD3 = this.modelo3.getVal("RendAnosAnter", "valrendAntAD3");
        double rendAntASPF = this.modelo3.getVal("RendAnosAnter", "valrendAntASPF");
        int anosAntASPA = new Double(this.modelo3.getVal("RendAnosAnter", "anosrendAntASPA")).intValue();
        int anosAntASPB = new Double(this.modelo3.getVal("RendAnosAnter", "anosrendAntASPB")).intValue();
        int anosAntAD1 = new Double(this.modelo3.getVal("RendAnosAnter", "anosrendAntAD1")).intValue();
        int anosAntAD2 = new Double(this.modelo3.getVal("RendAnosAnter", "anosrendAntAD2")).intValue();
        int anosAntAD3 = new Double(this.modelo3.getVal("RendAnosAnter", "anosrendAntAD3")).intValue();
        int anosAntASPF = new Double(this.modelo3.getVal("RendAnosAnter", "anosrendAntASPF")).intValue();
        int anosAntF = new Double(this.modelo3.getVal("Prediais", "valnAnoscatF")).intValue();
        if (anosAntASPA > 5) {
            anosAntASPA = 5;
        }
        if (anosAntASPB > 5) {
            anosAntASPB = 5;
        }
        if (anosAntAD1 > 5) {
            anosAntAD1 = 5;
        }
        if (anosAntAD2 > 5) {
            anosAntAD2 = 5;
        }
        if (anosAntAD3 > 5) {
            anosAntAD3 = 5;
        }
        if (anosAntASPF > 5) {
            anosAntASPF = 5;
        }
        if (anosAntF > 5) {
            anosAntF = 5;
        }
        double rendAntHSPA = this.modelo3.getVal("RendAnosAnter", "valrendAntHSPA");
        double rendAntHSPB = this.modelo3.getVal("RendAnosAnter", "valrendAntHSPB");
        double rendAntHD1 = this.modelo3.getVal("RendAnosAnter", "valrendAntHD1");
        double rendAntHD2 = this.modelo3.getVal("RendAnosAnter", "valrendAntHD2");
        double rendAntHD3 = this.modelo3.getVal("RendAnosAnter", "valrendAntHD3");
        double rendAntHSPF = this.modelo3.getVal("RendAnosAnter", "valrendAntHSPF");
        double rendAntF = this.modelo3.getVal("Prediais", "valrendAnosAntcatF");
        int anosAntHSPA = new Double(this.modelo3.getVal("RendAnosAnter", "anosrendAntHSPA")).intValue();
        int anosAntHSPB = new Double(this.modelo3.getVal("RendAnosAnter", "anosrendAntHSPB")).intValue();
        int anosAntHD1 = new Double(this.modelo3.getVal("RendAnosAnter", "anosrendAntHD1")).intValue();
        int anosAntHD2 = new Double(this.modelo3.getVal("RendAnosAnter", "anosrendAntHD2")).intValue();
        int anosAntHD3 = new Double(this.modelo3.getVal("RendAnosAnter", "anosrendAntHD3")).intValue();
        int anosAntHSPF = new Double(this.modelo3.getVal("RendAnosAnter", "anosrendAntHSPF")).intValue();
        if (anosAntHSPA > 5) {
            anosAntHSPA = 5;
        }
        if (anosAntHSPB > 5) {
            anosAntHSPB = 5;
        }
        if (anosAntHD1 > 5) {
            anosAntHD1 = 5;
        }
        if (anosAntHD2 > 5) {
            anosAntHD2 = 5;
        }
        if (anosAntHD3 > 5) {
            anosAntHD3 = 5;
        }
        if (anosAntHSPF > 5) {
            anosAntHSPF = 5;
        }
        double tempQuo = 0.0;
        double rendBrutoASPA = 0.0;
        double dedEspecASPA = 0.0;
        double rendLiqASPA = 0.0;
        double rendBrutoASPB = 0.0;
        double dedEspecASPB = 0.0;
        double rendLiqASPB = 0.0;
        double rendBrutoAD1 = 0.0;
        double dedEspecAD1 = 0.0;
        double rendLiqAD1 = 0.0;
        double rendBrutoAD2 = 0.0;
        double dedEspecAD2 = 0.0;
        double rendLiqAD2 = 0.0;
        double rendBrutoAD3 = 0.0;
        double dedEspecAD3 = 0.0;
        double rendLiqAD3 = 0.0;
        double rendBrutoASPF = 0.0;
        double dedEspecASPF = 0.0;
        double rendLiqASPF = 0.0;
        double rendBrutoHSPA = 0.0;
        double dedEspecHSPA = 0.0;
        double rendLiqHSPA = 0.0;
        double rendBrutoHSPB = 0.0;
        double dedEspecHSPB = 0.0;
        double rendLiqHSPB = 0.0;
        double rendBrutoHD1 = 0.0;
        double dedEspecHD1 = 0.0;
        double rendLiqHD1 = 0.0;
        double rendBrutoHD2 = 0.0;
        double dedEspecHD2 = 0.0;
        double rendLiqHD2 = 0.0;
        double rendBrutoHD3 = 0.0;
        double dedEspecHD3 = 0.0;
        double rendLiqHD3 = 0.0;
        double rendBrutoHSPF = 0.0;
        double dedEspecHSPF = 0.0;
        double rendLiqHSPF = 0.0;
        double rendLiqF = 0.0;
        double rendBrutoF = 0.0;
        for (int i = 0; i < this.modelo3.getNumContribs(); ++i) {
            if (this.modelo3.getContribuinte(i).getIdCont().equals("SPA")) {
                rendBrutoASPA = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
                rendLiqASPA = rendBrutoASPA < (dedEspecASPA = this.calcDeducEspecificaLiq(this.modelo3.getContribuinte(i))) ? 0.0 : rendBrutoASPA - dedEspecASPA;
                rendBrutoHSPA = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
                dedEspecHSPA = this.calcDeducEspecificaH(this.modelo3.getContribuinte(i));
                double d = rendLiqHSPA = rendBrutoHSPA < dedEspecHSPA ? 0.0 : rendBrutoHSPA - dedEspecHSPA;
            }
            if (this.modelo3.getContribuinte(i).getIdCont().equals("SPB")) {
                rendBrutoASPB = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
                rendLiqASPB = rendBrutoASPB < (dedEspecASPB = this.calcDeducEspecificaLiq(this.modelo3.getContribuinte(i))) ? 0.0 : rendBrutoASPB - dedEspecASPB;
                rendBrutoHSPB = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
                dedEspecHSPB = this.calcDeducEspecificaH(this.modelo3.getContribuinte(i));
                double d = rendLiqHSPB = rendBrutoHSPB < dedEspecHSPB ? 0.0 : rendBrutoHSPB - dedEspecHSPB;
            }
            if (this.modelo3.getContribuinte(i).getIdCont().equals("D1")) {
                rendBrutoAD1 = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
                rendLiqAD1 = rendBrutoAD1 < (dedEspecAD1 = this.calcDeducEspecificaLiq(this.modelo3.getContribuinte(i))) ? 0.0 : rendBrutoAD1 - dedEspecAD1;
                rendBrutoHD1 = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
                dedEspecHD1 = this.calcDeducEspecificaH(this.modelo3.getContribuinte(i));
                double d = rendLiqHD1 = rendBrutoHD1 < dedEspecHD1 ? 0.0 : rendBrutoHD1 - dedEspecHD1;
            }
            if (this.modelo3.getContribuinte(i).getIdCont().equals("D2")) {
                rendBrutoAD2 = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
                rendLiqAD2 = rendBrutoAD2 < (dedEspecAD2 = this.calcDeducEspecificaLiq(this.modelo3.getContribuinte(i))) ? 0.0 : rendBrutoAD2 - dedEspecAD2;
                rendBrutoHD2 = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
                dedEspecHD2 = this.calcDeducEspecificaH(this.modelo3.getContribuinte(i));
                double d = rendLiqHD2 = rendBrutoHD2 < dedEspecHD2 ? 0.0 : rendBrutoHD2 - dedEspecHD2;
            }
            if (this.modelo3.getContribuinte(i).getIdCont().equals("D3")) {
                rendBrutoAD3 = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
                rendLiqAD3 = rendBrutoAD3 < (dedEspecAD3 = this.calcDeducEspecificaLiq(this.modelo3.getContribuinte(i))) ? 0.0 : rendBrutoAD3 - dedEspecAD3;
                rendBrutoHD3 = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
                dedEspecHD3 = this.calcDeducEspecificaH(this.modelo3.getContribuinte(i));
                double d = rendLiqHD3 = rendBrutoHD3 < dedEspecHD3 ? 0.0 : rendBrutoHD3 - dedEspecHD3;
            }
            if (!this.modelo3.getContribuinte(i).getIdCont().equals("SPF")) continue;
            rendBrutoASPF = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            rendLiqASPF = rendBrutoASPF < (dedEspecASPF = this.calcDeducEspecificaLiq(this.modelo3.getContribuinte(i))) ? 0.0 : rendBrutoASPF - dedEspecASPF;
            rendBrutoHSPF = this.calcRendBrutoCatH(this.modelo3.getContribuinte(i));
            dedEspecHSPF = this.calcDeducEspecificaH(this.modelo3.getContribuinte(i));
            rendLiqHSPF = rendBrutoHSPF < dedEspecHSPF ? 0.0 : rendBrutoHSPF - dedEspecHSPF;
        }
        rendLiqF = this.calcRendLiqCatF();
        rendBrutoF = this.calcRendBrutoCatF();
        if (rendAntASPA > 0.0 && anosAntASPA > 0 && rendBrutoASPA > 0.0 && rendLiqASPA > 0.0) {
            tempQuo+=rendAntASPA / rendBrutoASPA * rendLiqASPA * (double)(anosAntASPA - 1 + 1) / (double)(anosAntASPA + 1);
        }
        if (rendAntASPB > 0.0 && anosAntASPB > 0 && rendBrutoASPB > 0.0 && rendLiqASPB > 0.0) {
            tempQuo+=rendAntASPB / rendBrutoASPB * rendLiqASPB * (double)(anosAntASPB - 1 + 1) / (double)(anosAntASPB + 1);
        }
        if (rendAntAD1 > 0.0 && anosAntAD1 > 0 && rendBrutoAD1 > 0.0 && rendLiqAD1 > 0.0) {
            tempQuo+=rendAntAD1 / rendBrutoAD1 * rendLiqAD1 * (double)(anosAntAD1 - 1 + 1) / (double)(anosAntAD1 + 1);
        }
        if (rendAntAD2 > 0.0 && anosAntAD2 > 0 && rendBrutoAD2 > 0.0 && rendLiqAD2 > 0.0) {
            tempQuo+=rendAntAD2 / rendBrutoAD2 * rendLiqAD2 * (double)(anosAntAD2 - 1 + 1) / (double)(anosAntAD2 + 1);
        }
        if (rendAntAD3 > 0.0 && anosAntAD3 > 0 && rendBrutoAD3 > 0.0 && rendLiqAD3 > 0.0) {
            tempQuo+=rendAntAD3 / rendBrutoAD3 * rendLiqAD3 * (double)(anosAntAD3 - 1 + 1) / (double)(anosAntAD3 + 1);
        }
        if (rendAntASPF > 0.0 && anosAntASPF > 0 && rendBrutoASPF > 0.0 && rendLiqASPF > 0.0) {
            tempQuo+=rendAntASPF / rendBrutoASPF * rendLiqASPF * (double)(anosAntASPF - 1 + 1) / (double)(anosAntASPF + 1);
        }
        if (rendAntHSPA > 0.0 && anosAntHSPA > 0 && rendBrutoHSPA > 0.0 && rendLiqHSPA > 0.0) {
            tempQuo+=rendAntHSPA / rendBrutoHSPA * rendLiqHSPA * (double)(anosAntHSPA - 1 + 1) / (double)(anosAntHSPA + 1);
        }
        if (rendAntHSPB > 0.0 && anosAntHSPB > 0 && rendBrutoHSPB > 0.0 && rendLiqHSPB > 0.0) {
            tempQuo+=rendAntHSPB / rendBrutoHSPB * rendLiqHSPB * (double)(anosAntHSPB - 1 + 1) / (double)(anosAntHSPB + 1);
        }
        if (rendAntHD1 > 0.0 && anosAntHD1 > 0 && rendBrutoHD1 > 0.0 && rendLiqHD1 > 0.0) {
            tempQuo+=rendAntHD1 / rendBrutoHD1 * rendLiqHD1 * (double)(anosAntHD1 - 1 + 1) / (double)(anosAntHD1 + 1);
        }
        if (rendAntHD2 > 0.0 && anosAntHD2 > 0 && rendBrutoHD2 > 0.0 && rendLiqHD2 > 0.0) {
            tempQuo+=rendAntHD2 / rendBrutoHD2 * rendLiqHD2 * (double)(anosAntHD2 - 1 + 1) / (double)(anosAntHD2 + 1);
        }
        if (rendAntHD3 > 0.0 && anosAntHD3 > 0 && rendBrutoHD3 > 0.0 && rendLiqHD3 > 0.0) {
            tempQuo+=rendAntHD3 / rendBrutoHD3 * rendLiqHD3 * (double)(anosAntHD3 - 1 + 1) / (double)(anosAntHD3 + 1);
        }
        if (rendAntHSPF > 0.0 && anosAntHSPF > 0 && rendBrutoHSPF > 0.0 && rendLiqHSPF > 0.0) {
            tempQuo+=rendAntHSPF / rendBrutoHSPF * rendLiqHSPF * (double)(anosAntHSPF - 1 + 1) / (double)(anosAntHSPF + 1);
        }
        if (rendAntF > 0.0 && anosAntF > 0 && rendBrutoF > 0.0 && rendLiqF > 0.0) {
            tempQuo+=rendAntF / rendBrutoF * rendLiqF * (double)(anosAntF - 1 + 1) / (double)(anosAntF + 1);
        }
        return tempQuo;
    }

    @Override
    public int getAnoInt() {
        switch (((SPA)this.modelo3.getContribuinte("SPA")).getAnoSim()) {
            case 49: {
                return 2007;
            }
            case 50: {
                return 2006;
            }
            case 51: {
                return 2005;
            }
            case 52: {
                return 2004;
            }
            case 53: {
                return 2003;
            }
            case 54: {
                return 2002;
            }
            case 55: {
                return 2001;
            }
            case 56: {
                return 2008;
            }
            case 57: {
                return 2009;
            }
        }
        return 2009;
    }

    @Override
    public double calcRendBrutoCatH(Contribuinte cont) {
        double rbh = 0.0;
        double rtvh = this.modelo3.getVal("Pensoes", "valrendTempVit", cont.getIdCont());
        double palh = this.modelo3.getVal("Pensoes", "valrendPensAlim", cont.getIdCont());
        double psbv = this.modelo3.getVal("Pensoes", "PSobrev", cont.getIdCont());
        double rh30 = 0.0;
        double rhaux1 = 0.0;
        double rhtot = 0.0;
        rbh = cont.getIdCont().equals("SPA") ? this.modelo3.getVal("Pensoes", "valrendBrutoCatH", cont.getIdCont()) : this.modelo3.getVal("Pensoes", "valrendBrutoCatH", cont.getIdCont());
        if (!this.prereformaIsCatA(cont)) {
            if (cont.getIdCont().equals("SPA")) {
                rbh+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
            }
            if (cont.getIdCont().equals("SPB")) {
                rbh+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
            }
        }
        rh30 = this.CatH_RB_NDefFA_60_80_Taxa * (rbh + this.modelo3.getVal("Pensoes", "valrendTempVit", cont.getIdCont()) + this.modelo3.getVal("Pensoes", "valrendPensAlim", cont.getIdCont()) + this.modelo3.getVal("Pensoes", "PSobrev", cont.getIdCont()));
        if (!cont.isDeficiente()) {
            rhtot = rbh + rtvh;
            rhtot+=palh;
            return rhtot+=psbv;
        }
        if (cont.getIdCont().equals("D1") || cont.getIdCont().equals("D2") || cont.getIdCont().equals("D3")) {
            rhtot = rbh + rtvh;
            rhtot+=palh;
            return rhtot+=psbv;
        }
        rhaux1 = rh30 > this.CatH_RB_NDefFA_60_80 ? this.CatH_RB_NDefFA_60_80 : rh30;
        rhtot = rbh + rtvh + palh + psbv - rhaux1;
        return rhtot;
    }

    @Override
    public double calcDeducao1EspecificaH(Contribuinte cont) {
        double ded1EspecificaH = 0.0;
        double dedrbh = 0.0;
        double dedHaux1 = 0.0;
        double dedHaux2 = 0.0;
        double valContPreRef = 0.0;
        double valRendPreRef = 0.0;
        if (!this.prereformaIsCatA(cont)) {
            if (cont.getIdCont().equals("SPA")) {
                valContPreRef = this.modelo3.getVal("PreReforma", "valcontObrigPreSPA");
                valRendPreRef = this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
            }
            if (cont.getIdCont().equals("SPB")) {
                valContPreRef = this.modelo3.getVal("PreReforma", "valcontObrigPreSPB");
                valRendPreRef = this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
            }
            if (valContPreRef > ded1EspecificaH) {
                ded1EspecificaH = valRendPreRef - valContPreRef > 0.0 ? valContPreRef : valRendPreRef;
            }
        }
        return ded1EspecificaH;
    }

    @Override
    public double calcDedSujeitosPassivos() {
        double resultSujPass = 0.0;
        double resultSujPassA = 0.0;
        double resultSujPassB = 0.0;
        double resultSujPassF = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        Contribuinte spb = this.modelo3.getContribuinte("SPB");
        Contribuinte spf = this.modelo3.getContribuinte("SPF");
        if (spa.isFamiliaCasado() || spa.getEstadoCivil() == '3') {
            if (spa.getEstadoCivil() == '3') {
                resultSujPassA = spa.isDeficiente() ? this.DC_Familia_Separado_Def : this.DC_Familia_Separado_Ndef;
            } else {
                resultSujPassA = spa.isDeficiente() ? this.DC_Familia_Casado_Def : this.DC_Familia_Casado_Ndef;
                resultSujPassB = spb.isDeficiente() ? this.DC_Familia_Casado_Def : this.DC_Familia_Casado_Ndef;
                if (spa.getObito() == '1') {
                    resultSujPassF = spf.isDeficiente() ? this.DC_Familia_Casado_Def : this.DC_Familia_Casado_Ndef;
                }
            }
        } else if (spa.getEstadoCivil() == '2' && spa.getObito() == '1') {
            resultSujPassA = spa.isDeficiente() ? this.DC_Familia_Casado_Def : this.DC_Familia_Casado_Ndef;
            resultSujPassF = spf.isDeficiente() ? this.DC_Familia_Casado_Def : this.DC_Familia_Casado_Ndef;
        } else {
            resultSujPassA = spa.isFamiliaMonoParental() ? (spa.isDeficiente() ? this.DC_Familia_Mono_Def : this.DC_Familia_Mono_Ndef) : (spa.isDeficiente() ? this.DC_Familia_Solteiro_Def : this.DC_Familia_Solteiro_Ndef);
        }
        resultSujPass = resultSujPassA + resultSujPassB + resultSujPassF;
        return resultSujPass;
    }

    @Override
    public double recolhaSF(double colecta) {
        double valor = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        String servFin = spa.getServFin();
        double taxa = 0.05;
        for (int i = 0; i < this.servFinArray.length; ++i) {
            if (servFin == null || !servFin.equals(this.servFinArray[i][0])) continue;
            taxa = Double.parseDouble(this.servFinArray[i][1]);
            break;
        }
        valor = (0.05 - taxa) * colecta;
        return valor;
    }

    public double calcReducaoDeducao1EspecificaH(Contribuinte cont, double ded1EspecificaH) {
        double dedrbh = 0.0;
        double dedHaux1 = 0.0;
        double dedHaux2 = 0.0;
        double quotCatH = this.calcDeducaoQuotizaH(cont);
        if (cont.getIdCont().equals("SPA")) {
            dedrbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", cont.getIdCont());
            dedrbh+=this.modelo3.getVal("Pensoes", "valrendPensAlim", cont.getIdCont());
            dedrbh+=this.modelo3.getVal("Pensoes", "PSobrev", cont.getIdCont());
        } else {
            dedrbh = this.modelo3.getVal("Pensoes", "valrendBrutoCatH", cont.getIdCont());
            dedrbh+=this.modelo3.getVal("Pensoes", "valrendPensAlim", cont.getIdCont());
            dedrbh+=this.modelo3.getVal("Pensoes", "PSobrev", cont.getIdCont());
        }
        if (!this.prereformaIsCatA(cont)) {
            if (cont.getIdCont().equals("SPA")) {
                dedrbh+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPA");
            }
            if (cont.getIdCont().equals("SPB")) {
                dedrbh+=this.modelo3.getVal("PreReforma", "valrendBrutoPreSPB");
            }
        }
        dedHaux1 = dedrbh - this.SPM;
        dedHaux2 = this.SPM_Taxa * dedHaux1;
        ded1EspecificaH = !cont.isDeficiente() ? (dedrbh > this.SPM ? (this.CatH_Ded1_Ndef + quotCatH - dedHaux2 > 0.0 ? this.CatH_Ded1_Ndef + quotCatH - dedHaux2 : 0.0) : (dedrbh > this.CatH_Ded1_Ndef + quotCatH ? this.CatH_Ded1_Ndef + quotCatH : dedrbh)) : (dedrbh > this.SPM ? (this.CatH_Ded1_Def + quotCatH - dedHaux2 > 0.0 ? this.CatH_Ded1_Def + quotCatH - dedHaux2 : 0.0) : (dedrbh > this.CatH_Ded1_Def + quotCatH ? this.CatH_Ded1_Def + quotCatH : dedrbh));
        return ded1EspecificaH;
    }

    @Override
    public double calcRendLiqCatH() {
        double resH = 0.0;
        double resparcH = 0.0;
        double resparcH1 = 0.0;
        double resparcH2 = 0.0;
        for (int j = 0; j < this.modelo3.getNumContribs(); ++j) {
            resparcH1 = this.calcRendBrutoCatH(this.modelo3.getContribuinte(j));
            resparcH = resparcH1 < (resparcH2 = this.calcDeducEspecificaH(this.modelo3.getContribuinte(j))) ? 0.0 : resparcH1 - resparcH2;
            resH+=resparcH;
        }
        return resH;
    }

    @Override
    public double calcDeducEspecificaH(Contribuinte cont) {
        double dedparcH = 0.0;
        double dedparcHFinal = 0.0;
        double dedparcH2 = 0.0;
        dedparcH2 = this.modelo3.getVal("Pensoes", "ContribH", cont.getIdCont());
        if (dedparcH2 > (dedparcHFinal = this.calcReducaoDeducao1EspecificaH(cont, dedparcH))) {
            return dedparcH2;
        }
        return dedparcHFinal;
    }

    @Override
    public double calcDedEncargosAmbientais() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        AbatDedColecta abat = (AbatDedColecta)this.modelo3.getRubricas("AbatDedColecta");
        double valLimDespAmbiTot = 0.0;
        double valER = this.modelo3.getVal("AbatDedColecta", "valdespAquEnergRenov");
        double valCT = this.modelo3.getVal("BenefFiscais", "valObrasMelhTermico");
        double valVNP = this.modelo3.getVal("BenefFiscais", "valVeiculosNaoPoluentes");
        double valER30 = this.Ded_Encargos_Ambientais_Taxa * (valER + valCT + valVNP);
        double valDedDespAmbiTot = 0.0;
        valLimDespAmbiTot = spa.getEstadoCivil() == '3' ? this.Ded_Encargos_Ambientais_Sep : this.Ded_Encargos_Ambientais_Outros;
        valDedDespAmbiTot = valER30 > valLimDespAmbiTot ? valLimDespAmbiTot : valER30;
        return valDedDespAmbiTot;
    }

    @Override
    public double calcRendLiqCatA() {
        double res = 0.0;
        double resparc = 0.0;
        double resparc1 = 0.0;
        double resparc2 = 0.0;
        double resparc3 = 0.0;
        double resparc4 = 0.0;
        double resparc5 = 0.0;
        double dedEspTemp = 0.0;
        double valorRendIsentos = 0.0;
        for (int i = 0; i < this.modelo3.getNumContribs(); ++i) {
            resparc1 = this.calcRendBrutoCatA(this.modelo3.getContribuinte(i));
            dedEspTemp = ((SPA)this.modelo3.getContribuinte("SPA")).getReside() == '4' ? 0.0 : this.calcDeducEspecificaLiq(this.modelo3.getContribuinte(i));
            resparc2 = resparc1 - dedEspTemp;
            res+=resparc2;
        }
        return res;
    }

    @Override
    public double calcDeducEspecificaLiq(Contribuinte cont) {
        double res = 0.0;
        double resparc = 0.0;
        double resparc1 = 0.0;
        double resparc2 = 0.0;
        double resparc3 = 0.0;
        double resparc4 = 0.0;
        double resparc5 = 0.0;
        double quotasOrdem = 0.0;
        double limite = 0.0;
        double valorRendIsentos = 0.0;
        int i = 0;
        resparc1 = this.calcRendBrutoCatA(cont);
        resparc2 = this.calcDeducEspecifica(cont);
        if (!cont.getIdCont().equals("SPF")) {
            valorRendIsentos = valorRendIsentos + this.modelo3.getVal("RendIsentos", "cod401", this.modelo3.getContribuinte(i).getIdCont()) + this.modelo3.getVal("RendIsentos", "cod402", this.modelo3.getContribuinte(i).getIdCont()) + this.modelo3.getVal("RendIsentos", "cod404", this.modelo3.getContribuinte(i).getIdCont()) + this.modelo3.getVal("RendIsentos", "cod405", this.modelo3.getContribuinte(i).getIdCont()) + this.modelo3.getVal("RendIsentos", "cod406", this.modelo3.getContribuinte(i).getIdCont()) + this.modelo3.getVal("RendIsentos", "cod407", this.modelo3.getContribuinte(i).getIdCont()) + this.modelo3.getVal("RendIsentos", "cod409", this.modelo3.getContribuinte(i).getIdCont());
        }
        resparc4 = resparc2 * resparc1;
        resparc5 = resparc1 - this.getDifIsento404(this.modelo3.getContribuinte(i)) + valorRendIsentos;
        if (resparc4 > 0.0) {
            resparc3 = resparc4 / resparc5;
        }
        quotasOrdem = this.calcQDFP(cont);
        if (resparc3 < this.CatA_Ded1_Sem_QOPeDFP_Valor_Def && quotasOrdem == 0.0) {
            resparc3 = this.CatA_Ded1_Sem_QOPeDFP_Valor_Def;
        } else if (quotasOrdem > 0.0) {
            limite = this.CatA_Ded1_Sem_QOPeDFP_Valor_Def + quotasOrdem;
            if (limite > this.CatA_Ded1_Com_QOPouDFP_Valor_NDef) {
                limite = this.CatA_Ded1_Com_QOPouDFP_Valor_NDef;
            }
            if (resparc3 < limite) {
                resparc3 = limite;
            }
        }
        resparc = resparc1 < resparc3 ? resparc1 : resparc3;
        return res+=resparc;
    }

    @Override
    public double calcRendBrutoCatF() {
        double res = 0.0;
        double resparc = 0.0;
        double resAnosAntF = this.modelo3.getVal("Prediais", "valperdasAnosAntF");
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        res = spa.getEnglobF() == '1' ? this.modelo3.getVal("Prediais", "valtotalRendas") + this.modelo3.getVal("Prediais", "valtotalRendasImv") + this.calcSublocacao() : this.modelo3.getVal("Prediais", "valtotalRendas") + this.calcSublocacao();
        if (res > 0.0) {
            resparc = res - resAnosAntF;
            if (resparc < 0.0) {
                return 0.0;
            }
            return resparc;
        }
        return 0.0;
    }

    @Override
    public double calcTaxaImposto(double valorRCC) {
        double taxaImposto = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (valorRCC <= this.TaxasGerais_Esc2_Min || this.TaxasGerais_Esc1_Min == this.TaxasGerais_EscMax_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc1_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc1_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc1_Madeira_Taxa;
            }
            this.escalaoContribuinte = 1;
        } else if (valorRCC <= this.TaxasGerais_Esc3_Min || this.TaxasGerais_Esc2_Min == this.TaxasGerais_EscMax_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc2_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc2_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc2_Madeira_Taxa;
            }
            this.escalaoContribuinte = 2;
        } else if (valorRCC <= this.TaxasGerais_Esc4_Min || this.TaxasGerais_Esc3_Min == this.TaxasGerais_EscMax_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc3_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc3_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc3_Madeira_Taxa;
            }
            this.escalaoContribuinte = 3;
        } else if (valorRCC <= this.TaxasGerais_Esc5_Min || this.TaxasGerais_Esc4_Min == this.TaxasGerais_EscMax_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc4_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc4_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc4_Madeira_Taxa;
            }
            this.escalaoContribuinte = 4;
        } else if (valorRCC <= this.TaxasGerais_Esc6_Min || this.TaxasGerais_Esc5_Min == this.TaxasGerais_EscMax_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc5_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc5_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc5_Madeira_Taxa;
            }
            this.escalaoContribuinte = 5;
        } else if (valorRCC <= this.TaxasGerais_Esc7_Min || this.TaxasGerais_Esc6_Min == this.TaxasGerais_EscMax_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc6_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc6_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc6_Madeira_Taxa;
            }
            this.escalaoContribuinte = 6;
        } else if (valorRCC <= this.TaxasGerais_Esc8_Min || this.TaxasGerais_Esc7_Min == this.TaxasGerais_EscMax_Min) {
            if (spa.getReside() == '1') {
                taxaImposto = this.TaxasGerais_Esc7_Cont_Taxa;
            } else if (spa.getReside() == '2') {
                taxaImposto = this.TaxasGerais_Esc7_Acores_Taxa;
            } else if (spa.getReside() == '3') {
                taxaImposto = this.TaxasGerais_Esc7_Madeira_Taxa;
            }
            this.escalaoContribuinte = 7;
        } else {
            if (valorRCC > this.TaxasGerais_Esc8_Min) {
                if (spa.getReside() == '1') {
                    taxaImposto = this.TaxasGerais_Esc8_Cont_Taxa;
                } else if (spa.getReside() == '2') {
                    taxaImposto = this.TaxasGerais_Esc8_Acores_Taxa;
                } else if (spa.getReside() == '3') {
                    taxaImposto = this.TaxasGerais_Esc8_Madeira_Taxa;
                }
            }
            this.escalaoContribuinte = 8;
        }
        return taxaImposto;
    }

    @Override
    public double calcParcelaAbat(double valorRCC) {
        double parcelaAbater = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        if (valorRCC <= this.TaxasGerais_Esc2_Min || this.TaxasGerais_Esc1_Min == this.TaxasGerais_EscMax_Min) {
            if (spa.getReside() == '1') {
                parcelaAbater = this.TaxasGerais_Esc1_Cont_Parc_Abater;
            } else if (spa.getReside() == '2') {
                parcelaAbater = this.TaxasGerais_Esc1_Acores_Parc_Abater;
            } else if (spa.getReside() == '3') {
                parcelaAbater = this.TaxasGerais_Esc1_Madeira_Parc_Abater;
            }
        } else if (valorRCC <= this.TaxasGerais_Esc3_Min || this.TaxasGerais_Esc2_Min == this.TaxasGerais_EscMax_Min) {
            if (spa.getReside() == '1') {
                parcelaAbater = this.TaxasGerais_Esc2_Cont_Parc_Abater;
            } else if (spa.getReside() == '2') {
                parcelaAbater = this.TaxasGerais_Esc2_Acores_Parc_Abater;
            } else if (spa.getReside() == '3') {
                parcelaAbater = this.TaxasGerais_Esc2_Madeira_Parc_Abater;
            }
        } else if (valorRCC <= this.TaxasGerais_Esc4_Min || this.TaxasGerais_Esc3_Min == this.TaxasGerais_EscMax_Min) {
            if (spa.getReside() == '1') {
                parcelaAbater = this.TaxasGerais_Esc3_Cont_Parc_Abater;
            } else if (spa.getReside() == '2') {
                parcelaAbater = this.TaxasGerais_Esc3_Acores_Parc_Abater;
            } else if (spa.getReside() == '3') {
                parcelaAbater = this.TaxasGerais_Esc3_Madeira_Parc_Abater;
            }
        } else if (valorRCC <= this.TaxasGerais_Esc5_Min || this.TaxasGerais_Esc4_Min == this.TaxasGerais_EscMax_Min) {
            if (spa.getReside() == '1') {
                parcelaAbater = this.TaxasGerais_Esc4_Cont_Parc_Abater;
            } else if (spa.getReside() == '2') {
                parcelaAbater = this.TaxasGerais_Esc4_Acores_Parc_Abater;
            } else if (spa.getReside() == '3') {
                parcelaAbater = this.TaxasGerais_Esc4_Madeira_Parc_Abater;
            }
        } else if (valorRCC <= this.TaxasGerais_Esc6_Min || this.TaxasGerais_Esc5_Min == this.TaxasGerais_EscMax_Min) {
            if (spa.getReside() == '1') {
                parcelaAbater = this.TaxasGerais_Esc5_Cont_Parc_Abater;
            } else if (spa.getReside() == '2') {
                parcelaAbater = this.TaxasGerais_Esc5_Acores_Parc_Abater;
            } else if (spa.getReside() == '3') {
                parcelaAbater = this.TaxasGerais_Esc5_Madeira_Parc_Abater;
            }
        } else if (valorRCC <= this.TaxasGerais_Esc7_Min || this.TaxasGerais_Esc6_Min == this.TaxasGerais_EscMax_Min) {
            if (spa.getReside() == '1') {
                parcelaAbater = this.TaxasGerais_Esc6_Cont_Parc_Abater;
            } else if (spa.getReside() == '2') {
                parcelaAbater = this.TaxasGerais_Esc6_Acores_Parc_Abater;
            } else if (spa.getReside() == '3') {
                parcelaAbater = this.TaxasGerais_Esc6_Madeira_Parc_Abater;
            }
        } else if (valorRCC <= this.TaxasGerais_Esc8_Min || this.TaxasGerais_Esc7_Min == this.TaxasGerais_EscMax_Min) {
            if (spa.getReside() == '1') {
                parcelaAbater = this.TaxasGerais_Esc7_Cont_Parc_Abater;
            } else if (spa.getReside() == '2') {
                parcelaAbater = this.TaxasGerais_Esc7_Acores_Parc_Abater;
            } else if (spa.getReside() == '3') {
                parcelaAbater = this.TaxasGerais_Esc7_Madeira_Parc_Abater;
            }
        } else if (valorRCC > this.TaxasGerais_Esc8_Min) {
            if (spa.getReside() == '1') {
                parcelaAbater = this.TaxasGerais_Esc8_Cont_Parc_Abater;
            } else if (spa.getReside() == '2') {
                parcelaAbater = this.TaxasGerais_Esc8_Acores_Parc_Abater;
            } else if (spa.getReside() == '3') {
                parcelaAbater = this.TaxasGerais_Esc8_Madeira_Parc_Abater;
            }
        }
        return parcelaAbater;
    }

    @Override
    public double calcDedDependentes() {
        double resultDepend = 0.0;
        double resultDepe = 0.0;
        double resultAfilhados = 0.0;
        double resultDepe03 = 0.0;
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        resultDepe = this.DC_Familia_Dependente_Ndef * (double)(spa.getNumDepNDef() + spa.getNumDepDef() - spa.getNumDep03());
        resultDepe03 = this.DC_Familia_Dependente_3Anos * (double)spa.getNumDep03();
        resultAfilhados = this.DC_Familia_Dependente_Ndef * (double)spa.getNumAfilhados();
        resultDepend = resultDepe + resultDepe03 + resultAfilhados;
        return resultDepend;
    }

    public double calcLimDeducoesColecta() {
        double valorLimDedColecta = 0.0;
        double dedDespesasSaude = this.calcDespSaude5();
        double dedDespesasSaudeOutros = this.calcDespSaudeOutras();
        double dedDespesasEducacao = this.calcDedEducFormProf();
        double dedEncargosLares = this.calcDedEncLares();
        double dedEncImov = this.calcDedEncImoveis();
        valorLimDedColecta = dedDespesasSaude + dedDespesasSaudeOutros + dedDespesasEducacao + dedEncargosLares + dedEncImov;
        if (this.escalaoContribuinte == 7) {
            if (valorLimDedColecta > this.valorRCC * this.Saude_Educac_Lares_Imoveis_ESC7_taxa) {
                valorLimDedColecta = this.valorRCC * this.Saude_Educac_Lares_Imoveis_ESC7_taxa;
            }
            if (valorLimDedColecta > this.Saude_Educac_Lares_Imoveis_ESC7) {
                valorLimDedColecta = this.Saude_Educac_Lares_Imoveis_ESC7;
            }
        }
        if (this.escalaoContribuinte == 8 && valorLimDedColecta > this.Saude_Educac_Lares_Imoveis_ESC8) {
            valorLimDedColecta = this.Saude_Educac_Lares_Imoveis_ESC8;
        }
        return valorLimDedColecta;
    }

    public double calcLimBeneFiscais(double colectaRendimSujeitos, double rendimIsentSE, double acrescimosRendLiq, double colectaGratificacoes, double colectaFuturoOpcoes) {
        double valorLimBeneFiscais = 0.0;
        double dedSegurosSaude = this.calcDedSeguroSaude();
        double dedPPRPPE = this.calcDedPPRPPE(rendimIsentSE, acrescimosRendLiq, colectaGratificacoes, colectaFuturoOpcoes);
        double dedImovReab = this.calcDedImovReab();
        double dedDonativosOutrasEnt = this.calcDedDonativOutr(colectaRendimSujeitos);
        double dedRPC = this.calculaRPC();
        double dedEnergRenov = this.calcDedEncargosAmbientais();
        valorLimBeneFiscais = dedSegurosSaude + dedRPC + dedPPRPPE + dedDonativosOutrasEnt + dedImovReab + dedEnergRenov;
        if (this.escalaoContribuinte == 3) {
            if (valorLimBeneFiscais > this.Bendeficios_ESC3) {
                valorLimBeneFiscais = this.Bendeficios_ESC3;
            }
        } else if (this.escalaoContribuinte == 4) {
            if (valorLimBeneFiscais > this.Bendeficios_ESC4) {
                valorLimBeneFiscais = this.Bendeficios_ESC4;
            }
        } else if (this.escalaoContribuinte == 5) {
            if (valorLimBeneFiscais > this.Bendeficios_ESC5) {
                valorLimBeneFiscais = this.Bendeficios_ESC5;
            }
        } else if (this.escalaoContribuinte == 6) {
            if (valorLimBeneFiscais > this.Bendeficios_ESC6) {
                valorLimBeneFiscais = this.Bendeficios_ESC6;
            }
        } else if (this.escalaoContribuinte == 7) {
            if (valorLimBeneFiscais > this.Bendeficios_ESC7) {
                valorLimBeneFiscais = this.Bendeficios_ESC7;
            }
        } else if (this.escalaoContribuinte == 8 && valorLimBeneFiscais > this.Bendeficios_ESC8) {
            valorLimBeneFiscais = this.Bendeficios_ESC8;
        }
        return valorLimBeneFiscais;
    }

    public double calcDedContribVelhiceDef() {
        Contribuinte spa = this.modelo3.getContribuinte("SPA");
        Contribuinte spb = this.modelo3.getContribuinte("SPB");
        Contribuinte spf = this.modelo3.getContribuinte("SPF");
        SPA spx = (SPA)this.modelo3.getContribuinte("SPA");
        double valContribVelhiceDef = this.modelo3.getVal("AbatDedColecta", "valContribReformaDef");
        double valContribVelhiceDef25 = this.DC_ContribReformaDef_Taxa * valContribVelhiceDef;
        double valContribVelhiceDefTot = 0.0;
        if (spa.isDeficiente() || spb.isDeficiente() || spf.isDeficiente()) {
            valContribVelhiceDefTot = valContribVelhiceDef25;
            if (spx.getEstadoCivil() == '1' || spx.getEstadoCivil() == '4') {
                if (valContribVelhiceDefTot > this.DC_ContribReformaDef_Lim_Cas) {
                    valContribVelhiceDefTot = this.DC_ContribReformaDef_Lim_Cas;
                }
            } else if ((spx.getEstadoCivil() == '2' || spx.getEstadoCivil() == '3') && valContribVelhiceDefTot > this.DC_ContribReformaDef_Lim_NCas) {
                valContribVelhiceDefTot = this.DC_ContribReformaDef_Lim_NCas;
            }
        }
        return valContribVelhiceDefTot;
    }

    @Override
    public double calcDedPremiosSegurDef(double colectaRendimSujeitos) {
        double valPremSegDef = this.modelo3.getVal("BenefFiscais", "valpSegurosSPDdef");
        double valPremSegDef25 = this.DC_SegDef * valPremSegDef;
        double valPremSegDefTot = 0.0;
        Contribuinte spa = this.modelo3.getContribuinte("SPA");
        Contribuinte spb = this.modelo3.getContribuinte("SPB");
        Contribuinte spf = this.modelo3.getContribuinte("SPF");
        Contribuinte d1 = this.modelo3.getContribuinte("D1");
        Contribuinte d2 = this.modelo3.getContribuinte("D2");
        Contribuinte d3 = this.modelo3.getContribuinte("D3");
        if ((spa.getGrauDef() == '1' || spa.getGrauDef() == '2' || spb.getGrauDef() == '1' || spb.getGrauDef() == '2' || spf.getGrauDef() == '1' || spf.getGrauDef() == '2' || d1.getGrauDef() == '1' || d1.getGrauDef() == '2' || d2.getGrauDef() == '1' || d2.getGrauDef() == '2' || d3.getGrauDef() == '1' || d3.getGrauDef() == '2') && (valPremSegDefTot = valPremSegDef25) > this.Taxa_Lim_Prem_Seg_Def * colectaRendimSujeitos) {
            valPremSegDefTot = this.Taxa_Lim_Prem_Seg_Def * colectaRendimSujeitos;
        }
        return valPremSegDefTot;
    }

    public double calcSobretaxaRendimento() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double valRendimento = 0.0;
        double sobreTaxaEstadoCivil = 1.0;
        double gratifAux1 = this.modelo3.getVal("RenTrabDep", "valgratific", "SPA") + this.modelo3.getVal("RenTrabDep", "valgratific", "SPB") + this.modelo3.getVal("RenTrabDep", "valgratific", "D1") + this.modelo3.getVal("RenTrabDep", "valgratific", "D2") + this.modelo3.getVal("RenTrabDep", "valgratific", "D3") + this.modelo3.getVal("RenTrabDep", "valgratific", "SPF");
        double partSoc = this.modelo3.getVal("MaisValias", "valtotRealiPartSoc") - this.modelo3.getVal("MaisValias", "valtotAquisPartSoc") - this.modelo3.getVal("MaisValias", "valtotDespePartSoc");
        double maisValiasPS = this.modelo3.getVal("OpFinancei", "vali4") + this.modelo3.getVal("OpFinancei", "valoutros") + this.modelo3.getVal("OpFinancei", "valcertif") + (partSoc-=500.0);
        maisValiasPS = maisValiasPS < 0.0 ? 0.0 : maisValiasPS;
        valRendimento = this.rendimColectavel + gratifAux1;
        if (spa.isFamiliaCasado() || spa.isObito()) {
            sobreTaxaEstadoCivil = 2.0;
        }
        if (spa.getEnglobG() == '0') {
            valRendimento+=maisValiasPS;
        }
        if ((valRendimento-=this.SobretaxaDeducoes * sobreTaxaEstadoCivil) < 0.0) {
            valRendimento = 0.0;
        }
        return valRendimento;
    }

    public double calcSobretaxaDeducoes() {
        SPA spa = (SPA)this.modelo3.getContribuinte("SPA");
        double valDeducoes = 0.0;
        int numDep = 0;
        numDep = spa.getNumDepNDef() + spa.getNumDepDef() + spa.getNumAfilhados();
        valDeducoes = (double)numDep * this.SobretaxaDeducoes_Taxa * this.SobretaxaDeducoes / 14.0;
        return valDeducoes;
    }

    public double calcSobretaxaColecta() {
        double valColecta = 0.0;
        valColecta = this.calcSobretaxaRendimento() * 0.035 - this.calcSobretaxaDeducoes();
        if (valColecta < 0.0) {
            valColecta = 0.0;
        }
        return valColecta-=this.calcSobretaxaRetencoes();
    }

    public double calcSobretaxaRetencoes() {
        double valRetencoes = 0.0;
        valRetencoes = this.modelo3.getVal("RenTrabDep", "valSobretaxaCatA", "SPA") + this.modelo3.getVal("RenTrabDep", "valSobretaxaCatA", "SPB") + this.modelo3.getVal("RenTrabDep", "valSobretaxaCatA", "D1") + this.modelo3.getVal("RenTrabDep", "valSobretaxaCatA", "D2") + this.modelo3.getVal("RenTrabDep", "valSobretaxaCatA", "D3") + this.modelo3.getVal("RenTrabDep", "valSobretaxaCatA", "SPF") + this.modelo3.getVal("Pensoes", "valSobretaxaCatH", "SPA") + this.modelo3.getVal("Pensoes", "valSobretaxaCatH", "SPB") + this.modelo3.getVal("Pensoes", "valSobretaxaCatH", "D1") + this.modelo3.getVal("Pensoes", "valSobretaxaCatH", "D2") + this.modelo3.getVal("Pensoes", "valSobretaxaCatH", "D3") + this.modelo3.getVal("Pensoes", "valSobretaxaCatH", "SPF");
        return valRetencoes;
    }

    public double calcSobretaxaRendimentoXTaxa() {
        return this.calcSobretaxaRendimento() * 0.035;
    }
}

