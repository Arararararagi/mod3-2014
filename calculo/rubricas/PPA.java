/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;

public class PPA
extends Rubricas {
    private final int nPPA = 2;

    public PPA(String id, Contribuinte[] Contrib) {
        this.inicializa(2);
        this.setID(id);
        this.addRubrica(new Rubrica("valPPA", Contrib[0]));
        this.addRubrica(new Rubrica("valPPA", Contrib[1]));
    }
}

