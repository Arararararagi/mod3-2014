/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Rubrica;

public class InfGenerica
extends Rubricas {
    private final int nInfGenerica = 27;

    public InfGenerica(String id) {
        this.inicializa(27);
        this.setID(id);
        this.addRubrica(new Rubrica("anosim"));
        this.addRubrica(new Rubrica("residencia"));
        this.addRubrica(new Rubrica("estadoCivil"));
        this.addRubrica(new Rubrica("numDepNDef"));
        this.addRubrica(new Rubrica("numDepDef"));
        this.addRubrica(new Rubrica("numAscNDef"));
        this.addRubrica(new Rubrica("numAscDef"));
        this.addRubrica(new Rubrica("obitoConjuge"));
        this.addRubrica(new Rubrica("SPA"));
        this.addRubrica(new Rubrica("idadSPA"));
        this.addRubrica(new Rubrica("grauDefSPA"));
        this.addRubrica(new Rubrica("defFArmSPA"));
        this.addRubrica(new Rubrica("SPB"));
        this.addRubrica(new Rubrica("idadSPB"));
        this.addRubrica(new Rubrica("grauDefSPB"));
        this.addRubrica(new Rubrica("defFArmSPB"));
        this.addRubrica(new Rubrica("D1"));
        this.addRubrica(new Rubrica("idadeD1"));
        this.addRubrica(new Rubrica("grauDefD1"));
        this.addRubrica(new Rubrica("defFArmD1"));
        this.addRubrica(new Rubrica("D2"));
        this.addRubrica(new Rubrica("idadeD2"));
        this.addRubrica(new Rubrica("grauDefD2"));
        this.addRubrica(new Rubrica("defFArmD2"));
        this.addRubrica(new Rubrica("D3"));
        this.addRubrica(new Rubrica("idadeD3"));
        this.addRubrica(new Rubrica("grauDefD3"));
        this.addRubrica(new Rubrica("defFArmD3"));
        this.addRubrica(new Rubrica("SPF"));
        this.addRubrica(new Rubrica("grauDefSPF"));
        this.addRubrica(new Rubrica("defFArmSPF"));
        this.addRubrica(new Rubrica("numAfilhados"));
    }
}

