/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Rubrica;

public class RendAnosAnter
extends Rubricas {
    private final int nRendAnosAnter = 24;

    public RendAnosAnter(String id) {
        this.inicializa(24);
        this.setID(id);
        this.addRubrica(new Rubrica("valrendAntASPA", "Rendimento sujeito Passivo A - Categoria A"));
        this.addRubrica(new Rubrica("valrendAntASPB", "Rendimento sujeito Passivo B - Categoria A"));
        this.addRubrica(new Rubrica("valrendAntAD1", "Rendimento Dependente 1 - Categoria A"));
        this.addRubrica(new Rubrica("valrendAntAD2", "Rendimento Dependente 2 - Categoria A"));
        this.addRubrica(new Rubrica("valrendAntAD3", "Rendimento Dependente 3 - Categoria A"));
        this.addRubrica(new Rubrica("valrendAntASPF", "Rendimento Falecido - Categoria A"));
        this.addRubrica(new Rubrica("anosrendAntASPA", "Anos sujeito Passivo A - Categoria A"));
        this.addRubrica(new Rubrica("anosrendAntASPB", "Anos sujeito Passivo A - Categoria A"));
        this.addRubrica(new Rubrica("anosrendAntAD1", "Anos Dependente 1 - Categoria A"));
        this.addRubrica(new Rubrica("anosrendAntAD2", "Anos Dependente 2 - Categoria A"));
        this.addRubrica(new Rubrica("anosrendAntAD3", "Anos Dependente 3 - Categoria A"));
        this.addRubrica(new Rubrica("anosrendAntASPF", "Anos Falecido - Categoria A"));
        this.addRubrica(new Rubrica("valrendAntHSPA", "Rendimento sujeito Passivo A - Categoria B"));
        this.addRubrica(new Rubrica("valrendAntHSPB", "Rendimento sujeito Passivo B - Categoria B"));
        this.addRubrica(new Rubrica("valrendAntHD1", "Rendimento Dependente 1 - Categoria B"));
        this.addRubrica(new Rubrica("valrendAntHD2", "Rendimento Dependente 2 - Categoria B"));
        this.addRubrica(new Rubrica("valrendAntHD3", "Rendimento Dependente 3 - Categoria B"));
        this.addRubrica(new Rubrica("valrendAntHSPF", "Rendimento Falecido - Categoria B"));
        this.addRubrica(new Rubrica("anosrendAntHSPA", "Anos sujeito Passivo A - Categoria B"));
        this.addRubrica(new Rubrica("anosrendAntHSPB", "Anos sujeito Passivo B - Categoria B"));
        this.addRubrica(new Rubrica("anosrendAntHD1", "Anos Dependente 1 - Categoria B"));
        this.addRubrica(new Rubrica("anosrendAntHD2", "Anos Dependente 2 - Categoria B"));
        this.addRubrica(new Rubrica("anosrendAntHD3", "Anos Dependente 3 - Categoria B"));
        this.addRubrica(new Rubrica("anosrendAntHSPF", "Anos Falecido - Categoria B"));
    }
}

