/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Rubrica;

public class Capitais
extends Rubricas {
    private final int nCapitais = 10;

    public Capitais(String id) {
        this.inicializa(10);
        this.setID(id);
        this.addRubrica(new Rubrica("valJurosSuprimento", "Juros de suprimento"));
        this.addRubrica(new Rubrica("valoutrosCapitais1", "Outros rendimentos com englobamento"));
        this.addRubrica(new Rubrica("valLucrosAdianta", "Lucros e adiantamentos"));
        this.addRubrica(new Rubrica("valJurosDepositos", "Juros derivados de dep&oacute;sitos"));
        this.addRubrica(new Rubrica("valJurosPremios", "Rendimentos de t&iacute;tulos"));
        this.addRubrica(new Rubrica("valoutrosCapitais2", "Outros rendimentos com op&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valretFonteE", "Total de Reten&ccedil;&otilde;es"));
        this.addRubrica(new Rubrica("valDifPosN7", "Resgate PPA"));
        this.addRubrica(new Rubrica("valResgateFPR", "Resgate certificados FPR"));
        this.addRubrica(new Rubrica("valRendFundCapRisco", "Rendimentos de fundos de capital de risco"));
    }
}

