/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;

public class RPC
extends Rubricas {
    private final int nRPC = 3;

    public RPC(String id, Contribuinte[] Contrib) {
        this.inicializa(3);
        this.setID(id);
        this.addRubrica(new Rubrica("valRPC", Contrib[0]));
        this.addRubrica(new Rubrica("valRPC", Contrib[1]));
        this.addRubrica(new Rubrica("valRPCSPF"));
    }
}

