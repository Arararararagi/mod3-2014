/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;

public class AnexoC
extends Rubricas {
    private final int nAnexoC = 35;

    public AnexoC(String id, Contribuinte[] Contrib) {
        this.inicializa(35);
        this.setID(id);
        this.addRubrica(new Rubrica("chkAgriVSProfC", Contrib[0], "Agr&iacute;colas &#47; Profissionais"));
        this.addRubrica(new Rubrica("valLucroPrejProC", Contrib[0], "Lucro &#47; Preju&Iacute;zo &#45; Profiss."));
        this.addRubrica(new Rubrica("valLucroPrejAgrC", Contrib[0], "Lucro &#47; Preju&Iacute;zo &#45; Agr&iacutecola"));
        this.addRubrica(new Rubrica("valLucroPrejActFinC", Contrib[0], "Lucro&#47;Preju&iacute;zo Actividades financeiras"));
        this.addRubrica(new Rubrica("valRetFonteC", Contrib[0], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valPagamenContaC", Contrib[0], "Pagamentos por conta"));
        this.addRubrica(new Rubrica("valRendAufDefC", Contrib[0], "Rendimentos Auferidos por Deficientes"));
        this.addRubrica(new Rubrica("chkAgriVSProfC", Contrib[1], "Agr&iacute;colas &#47; Profissionais"));
        this.addRubrica(new Rubrica("valLucroPrejProC", Contrib[1], "Lucro &#47; Preju&Iacute;zo &#45; Profiss."));
        this.addRubrica(new Rubrica("valLucroPrejAgrC", Contrib[1], "Lucro &#47; Preju&Iacute;zo &#45; Agr&iacutecola"));
        this.addRubrica(new Rubrica("valLucroPrejActFinC", Contrib[1], "Lucro&#47;Preju&iacute;zo Actividades financeiras"));
        this.addRubrica(new Rubrica("valRetFonteC", Contrib[1], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valPagamenContaC", Contrib[1], "Pagamentos por conta"));
        this.addRubrica(new Rubrica("valRendAufDefC", Contrib[1], "Rendimentos Auferidos por Deficientes"));
        this.addRubrica(new Rubrica("chkAgriVSProfC", Contrib[2], "Agr&iacute;colas &#47; Profissionais"));
        this.addRubrica(new Rubrica("valLucroPrejProC", Contrib[2], "Lucro &#47; Preju&Iacute;zo &#45; Profiss."));
        this.addRubrica(new Rubrica("valLucroPrejAgrC", Contrib[2], "Lucro &#47; Preju&Iacute;zo &#45; Agr&iacutecola"));
        this.addRubrica(new Rubrica("valLucroPrejActFinC", Contrib[2], "Lucro&#47;Preju&iacute;zo Actividades financeiras"));
        this.addRubrica(new Rubrica("valRetFonteC", Contrib[2], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valPagamenContaC", Contrib[2], "Pagamentos por conta"));
        this.addRubrica(new Rubrica("valRendAufDefC", Contrib[2], "Rendimentos Auferidos por Deficientes"));
        this.addRubrica(new Rubrica("chkAgriVSProfC", Contrib[3], "Agr&iacute;colas &#47; Profissionais"));
        this.addRubrica(new Rubrica("valLucroPrejProC", Contrib[3], "Lucro &#47; Preju&Iacute;zo &#45; Profiss."));
        this.addRubrica(new Rubrica("valLucroPrejAgrC", Contrib[3], "Lucro &#47; Preju&Iacute;zo &#45; Agr&iacutecola"));
        this.addRubrica(new Rubrica("valLucroPrejActFinC", Contrib[3], "Lucro&#47;Preju&iacute;zo Actividades financeiras"));
        this.addRubrica(new Rubrica("valRetFonteC", Contrib[3], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valPagamenContaC", Contrib[3], "Pagamentos por conta"));
        this.addRubrica(new Rubrica("valRendAufDefC", Contrib[3], "Rendimentos Auferidos por Deficientes"));
        this.addRubrica(new Rubrica("chkAgriVSProfC", Contrib[4], "Agr&iacute;colas &#47; Profissionais"));
        this.addRubrica(new Rubrica("valLucroPrejProC", Contrib[4], "Lucro &#47; Preju&Iacute;zo &#45; Profiss."));
        this.addRubrica(new Rubrica("valLucroPrejAgrC", Contrib[4], "Lucro &#47; Preju&Iacute;zo &#45; Agr&iacutecola"));
        this.addRubrica(new Rubrica("valLucroPrejActFinC", Contrib[4], "Lucro&#47;Preju&iacute;zo Actividades financeiras"));
        this.addRubrica(new Rubrica("valRetFonteC", Contrib[4], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valPagamenContaC", Contrib[4], "Pagamentos por conta"));
        this.addRubrica(new Rubrica("valRendAufDefC", Contrib[4], "Rendimentos Auferidos por Deficientes"));
    }
}

