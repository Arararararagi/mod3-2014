/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Rubrica;

public class PreReforma
extends Rubricas {
    private final int nPreReform = 10;

    public PreReforma(String id) {
        this.inicializa(10);
        this.setID(id);
        this.addRubrica(new Rubrica("valrendBrutoPreSPA", "Rendimento Sujeito Passivo A"));
        this.addRubrica(new Rubrica("valrendBrutoPreSPB", "Rendimento Sujeito Passivo B"));
        this.addRubrica(new Rubrica("valcontObrigPreSPA", "Contribui\u00e7\u00f5es Sujeito Passivo A"));
        this.addRubrica(new Rubrica("valcontObrigPreSPB", "Contribui\u00e7\u00f5es Sujeito Passivo B"));
        this.addRubrica(new Rubrica("valretFontePreSPA", "Reten\u00e7\u00f5es Sujeito Passivo A"));
        this.addRubrica(new Rubrica("valretFontePreSPB", "Reten\u00e7\u00f5es Sujeito Passivo B"));
        this.addRubrica(new Rubrica("valdataContPreSPA", "Data Sujeito Passivo A"));
        this.addRubrica(new Rubrica("valdataContPreSPB", "Data Sujeito Passivo B"));
        this.addRubrica(new Rubrica("valdataPriPagPreSPA", "Primeiro Pagamento Sujeito Passivo A"));
        this.addRubrica(new Rubrica("valdataPriPagPreSPB", "Primeiro Pagamento Sujeito Passivo B"));
    }
}

