/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;

public class RendIsentos
extends Rubricas {
    private final int nRendIsentos = 100;

    public RendIsentos(String id, Contribuinte[] Contrib) {
        this.inicializa(100);
        this.setID(id);
        this.addRubrica(new Rubrica("cod401", Contrib[0], "Remunera\u00e7\u00f5es do pessoal das miss\u00f5es diplom\u00e1ticas e consulares"));
        this.addRubrica(new Rubrica("cod402", Contrib[0], "Remunera\u00e7\u00f5es do pessoal ao servi\u00e7o de organiza\u00e7\u00f5es estrangeiras ou internacionais"));
        this.addRubrica(new Rubrica("cod403", Contrib[0], "Lucros derivados de obras ou trabalhos das infra-estruturas comuns NATO."));
        this.addRubrica(new Rubrica("cod404", Contrib[0], "Recebimentos em capital de import\u00e2ncias despendidas pelas entidades patronais para regimes de seguran\u00e7a social"));
        this.addRubrica(new Rubrica("cod405", Contrib[0], "Remunera\u00e7\u00f5es auferidas na qualidade de tripulante de navios registados no Registo Internacional de Navios"));
        this.addRubrica(new Rubrica("cod406", Contrib[0], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o n\u00e3o dependente de reconhecimento pr\u00e9vio"));
        this.addRubrica(new Rubrica("cod407", Contrib[0], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - trabalho dependente"));
        this.addRubrica(new Rubrica("cod408", Contrib[0], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - rendimentos profissionais"));
        this.addRubrica(new Rubrica("cod409", Contrib[0], "Remunera\u00e7\u00f5es auferidas no desempenho de fun\u00e7\u00f5es integradas em miss\u00f5es de car\u00e1cter militar, no estrangeiro"));
        this.addRubrica(new Rubrica("cod410", Contrib[0], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o"));
        this.addRubrica(new Rubrica("cod401", Contrib[1], "Remunera\u00e7\u00f5es do pessoal das miss\u00f5es diplom\u00e1ticas e consulares"));
        this.addRubrica(new Rubrica("cod402", Contrib[1], "Remunera\u00e7\u00f5es do pessoal ao servi\u00e7o de organiza\u00e7\u00f5es estrangeiras ou internacionais"));
        this.addRubrica(new Rubrica("cod403", Contrib[1], "Lucros derivados de obras ou trabalhos das infra-estruturas comuns NATO."));
        this.addRubrica(new Rubrica("cod404", Contrib[1], "Recebimentos em capital de import\u00e2ncias despendidas pelas entidades patronais para regimes de seguran\u00e7a social"));
        this.addRubrica(new Rubrica("cod405", Contrib[1], "Remunera\u00e7\u00f5es auferidas na qualidade de tripulante de navios registados no Registo Internacional de Navios"));
        this.addRubrica(new Rubrica("cod406", Contrib[1], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o n\u00e3o dependente de reconhecimento pr\u00e9vio"));
        this.addRubrica(new Rubrica("cod407", Contrib[1], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - trabalho dependente"));
        this.addRubrica(new Rubrica("cod408", Contrib[1], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - rendimentos profissionais"));
        this.addRubrica(new Rubrica("cod409", Contrib[1], "Remunera\u00e7\u00f5es auferidas no desempenho de fun\u00e7\u00f5es integradas em miss\u00f5es de car\u00e1cter militar, no estrangeiro"));
        this.addRubrica(new Rubrica("cod410", Contrib[1], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o"));
        this.addRubrica(new Rubrica("cod401", Contrib[2], "Remunera\u00e7\u00f5es do pessoal das miss\u00f5es diplom\u00e1ticas e consulares"));
        this.addRubrica(new Rubrica("cod402", Contrib[2], "Remunera\u00e7\u00f5es do pessoal ao servi\u00e7o de organiza\u00e7\u00f5es estrangeiras ou internacionais"));
        this.addRubrica(new Rubrica("cod403", Contrib[2], "Lucros derivados de obras ou trabalhos das infra-estruturas comuns NATO."));
        this.addRubrica(new Rubrica("cod404", Contrib[2], "Recebimentos em capital de import\u00e2ncias despendidas pelas entidades patronais para regimes de seguran\u00e7a social"));
        this.addRubrica(new Rubrica("cod405", Contrib[2], "Remunera\u00e7\u00f5es auferidas na qualidade de tripulante de navios registados no Registo Internacional de Navios"));
        this.addRubrica(new Rubrica("cod406", Contrib[2], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o n\u00e3o dependente de reconhecimento pr\u00e9vio"));
        this.addRubrica(new Rubrica("cod407", Contrib[2], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - trabalho dependente"));
        this.addRubrica(new Rubrica("cod408", Contrib[2], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - rendimentos profissionais"));
        this.addRubrica(new Rubrica("cod409", Contrib[2], "Remunera\u00e7\u00f5es auferidas no desempenho de fun\u00e7\u00f5es integradas em miss\u00f5es de car\u00e1cter militar, no estrangeiro"));
        this.addRubrica(new Rubrica("cod410", Contrib[2], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o"));
        this.addRubrica(new Rubrica("cod401", Contrib[3], "Remunera\u00e7\u00f5es do pessoal das miss\u00f5es diplom\u00e1ticas e consulares"));
        this.addRubrica(new Rubrica("cod402", Contrib[3], "Remunera\u00e7\u00f5es do pessoal ao servi\u00e7o de organiza\u00e7\u00f5es estrangeiras ou internacionais"));
        this.addRubrica(new Rubrica("cod403", Contrib[3], "Lucros derivados de obras ou trabalhos das infra-estruturas comuns NATO."));
        this.addRubrica(new Rubrica("cod404", Contrib[3], "Recebimentos em capital de import\u00e2ncias despendidas pelas entidades patronais para regimes de seguran\u00e7a social"));
        this.addRubrica(new Rubrica("cod405", Contrib[3], "Remunera\u00e7\u00f5es auferidas na qualidade de tripulante de navios registados no Registo Internacional de Navios"));
        this.addRubrica(new Rubrica("cod406", Contrib[3], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o n\u00e3o dependente de reconhecimento pr\u00e9vio"));
        this.addRubrica(new Rubrica("cod407", Contrib[3], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - trabalho dependente"));
        this.addRubrica(new Rubrica("cod408", Contrib[3], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - rendimentos profissionais"));
        this.addRubrica(new Rubrica("cod409", Contrib[3], "Remunera\u00e7\u00f5es auferidas no desempenho de fun\u00e7\u00f5es integradas em miss\u00f5es de car\u00e1cter militar, no estrangeiro"));
        this.addRubrica(new Rubrica("cod410", Contrib[3], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o"));
        this.addRubrica(new Rubrica("cod401", Contrib[4], "Remunera\u00e7\u00f5es do pessoal das miss\u00f5es diplom\u00e1ticas e consulares"));
        this.addRubrica(new Rubrica("cod402", Contrib[4], "Remunera\u00e7\u00f5es do pessoal ao servi\u00e7o de organiza\u00e7\u00f5es estrangeiras ou internacionais"));
        this.addRubrica(new Rubrica("cod403", Contrib[4], "Lucros derivados de obras ou trabalhos das infra-estruturas comuns NATO."));
        this.addRubrica(new Rubrica("cod404", Contrib[4], "Recebimentos em capital de import\u00e2ncias despendidas pelas entidades patronais para regimes de seguran\u00e7a social"));
        this.addRubrica(new Rubrica("cod405", Contrib[4], "Remunera\u00e7\u00f5es auferidas na qualidade de tripulante de navios registados no Registo Internacional de Navios"));
        this.addRubrica(new Rubrica("cod406", Contrib[4], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o n\u00e3o dependente de reconhecimento pr\u00e9vio"));
        this.addRubrica(new Rubrica("cod407", Contrib[4], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - trabalho dependente"));
        this.addRubrica(new Rubrica("cod408", Contrib[4], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - rendimentos profissionais"));
        this.addRubrica(new Rubrica("cod409", Contrib[4], "Remunera\u00e7\u00f5es auferidas no desempenho de fun\u00e7\u00f5es integradas em miss\u00f5es de car\u00e1cter militar, no estrangeiro"));
        this.addRubrica(new Rubrica("cod410", Contrib[4], "Remunera\u00e7\u00f5es auferidas ao abrigo de acordos de coopera\u00e7\u00e3o"));
        this.addRubrica(new Rubrica("ret401", Contrib[0], "Reten\u00e7\u00f5es de IRS do pessoal das miss\u00f5es diplom\u00e1ticas e consulares"));
        this.addRubrica(new Rubrica("ret402", Contrib[0], "Reten\u00e7\u00f5es de IRS do pessoal ao servi\u00e7o de organiza\u00e7\u00f5es estrangeiras ou internacionais"));
        this.addRubrica(new Rubrica("ret403", Contrib[0], "Reten\u00e7\u00f5es de IRS de lucros derivados de obras ou trabalhos das infra-estruturas comuns NATO."));
        this.addRubrica(new Rubrica("ret404", Contrib[0], "Reten\u00e7\u00f5es de recebimentos em capital de import\u00e2ncias despendidas pelas entidades patronais para regimes de seguran\u00e7a social"));
        this.addRubrica(new Rubrica("ret405", Contrib[0], "Reten\u00e7\u00f5es de IRS auferidas na qualidade de tripulante de navios registados no Registo Internacional de Navios"));
        this.addRubrica(new Rubrica("ret406", Contrib[0], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o n\u00e3o dependente de reconhecimento pr\u00e9vio"));
        this.addRubrica(new Rubrica("ret407", Contrib[0], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - trabalho dependente"));
        this.addRubrica(new Rubrica("ret408", Contrib[0], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - rendimentos profissionais"));
        this.addRubrica(new Rubrica("ret409", Contrib[0], "Reten\u00e7\u00f5es de IRS auferidas no desempenho de fun\u00e7\u00f5es integradas em miss\u00f5es de car\u00e1cter militar, no estrangeiro"));
        this.addRubrica(new Rubrica("ret410", Contrib[0], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o"));
        this.addRubrica(new Rubrica("ret401", Contrib[1], "Reten\u00e7\u00f5es de IRS do pessoal das miss\u00f5es diplom\u00e1ticas e consulares"));
        this.addRubrica(new Rubrica("ret402", Contrib[1], "Reten\u00e7\u00f5es de IRS do pessoal ao servi\u00e7o de organiza\u00e7\u00f5es estrangeiras ou internacionais"));
        this.addRubrica(new Rubrica("ret403", Contrib[1], "Reten\u00e7\u00f5es de IRS de lucros derivados de obras ou trabalhos das infra-estruturas comuns NATO."));
        this.addRubrica(new Rubrica("ret404", Contrib[1], "Reten\u00e7\u00f5es de recebimentos em capital de import\u00e2ncias despendidas pelas entidades patronais para regimes de seguran\u00e7a social"));
        this.addRubrica(new Rubrica("ret405", Contrib[1], "Reten\u00e7\u00f5es de IRS auferidas na qualidade de tripulante de navios registados no Registo Internacional de Navios"));
        this.addRubrica(new Rubrica("ret406", Contrib[1], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o n\u00e3o dependente de reconhecimento pr\u00e9vio"));
        this.addRubrica(new Rubrica("ret407", Contrib[1], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - trabalho dependente"));
        this.addRubrica(new Rubrica("ret408", Contrib[1], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - rendimentos profissionais"));
        this.addRubrica(new Rubrica("ret409", Contrib[1], "Reten\u00e7\u00f5es de IRS auferidas no desempenho de fun\u00e7\u00f5es integradas em miss\u00f5es de car\u00e1cter militar, no estrangeiro"));
        this.addRubrica(new Rubrica("ret410", Contrib[1], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o"));
        this.addRubrica(new Rubrica("ret401", Contrib[2], "Reten\u00e7\u00f5es de IRS do pessoal das miss\u00f5es diplom\u00e1ticas e consulares"));
        this.addRubrica(new Rubrica("ret402", Contrib[2], "Reten\u00e7\u00f5es de IRS do pessoal ao servi\u00e7o de organiza\u00e7\u00f5es estrangeiras ou internacionais"));
        this.addRubrica(new Rubrica("ret403", Contrib[2], "Reten\u00e7\u00f5es de IRS de lucros derivados de obras ou trabalhos das infra-estruturas comuns NATO."));
        this.addRubrica(new Rubrica("ret404", Contrib[2], "Reten\u00e7\u00f5es de recebimentos em capital de import\u00e2ncias despendidas pelas entidades patronais para regimes de seguran\u00e7a social"));
        this.addRubrica(new Rubrica("ret405", Contrib[2], "Reten\u00e7\u00f5es de IRS auferidas na qualidade de tripulante de navios registados no Registo Internacional de Navios"));
        this.addRubrica(new Rubrica("ret406", Contrib[2], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o n\u00e3o dependente de reconhecimento pr\u00e9vio"));
        this.addRubrica(new Rubrica("ret407", Contrib[2], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - trabalho dependente"));
        this.addRubrica(new Rubrica("ret408", Contrib[2], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - rendimentos profissionais"));
        this.addRubrica(new Rubrica("ret409", Contrib[2], "Reten\u00e7\u00f5es de IRS auferidas no desempenho de fun\u00e7\u00f5es integradas em miss\u00f5es de car\u00e1cter militar, no estrangeiro"));
        this.addRubrica(new Rubrica("ret410", Contrib[2], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o"));
        this.addRubrica(new Rubrica("ret401", Contrib[3], "Reten\u00e7\u00f5es de IRS do pessoal das miss\u00f5es diplom\u00e1ticas e consulares"));
        this.addRubrica(new Rubrica("ret402", Contrib[3], "Reten\u00e7\u00f5es de IRS do pessoal ao servi\u00e7o de organiza\u00e7\u00f5es estrangeiras ou internacionais"));
        this.addRubrica(new Rubrica("ret403", Contrib[3], "Reten\u00e7\u00f5es de IRS de lucros derivados de obras ou trabalhos das infra-estruturas comuns NATO."));
        this.addRubrica(new Rubrica("ret404", Contrib[3], "Reten\u00e7\u00f5es de recebimentos em capital de import\u00e2ncias despendidas pelas entidades patronais para regimes de seguran\u00e7a social"));
        this.addRubrica(new Rubrica("ret405", Contrib[3], "Reten\u00e7\u00f5es de IRS auferidas na qualidade de tripulante de navios registados no Registo Internacional de Navios"));
        this.addRubrica(new Rubrica("ret406", Contrib[3], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o n\u00e3o dependente de reconhecimento pr\u00e9vio"));
        this.addRubrica(new Rubrica("ret407", Contrib[3], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - trabalho dependente"));
        this.addRubrica(new Rubrica("ret408", Contrib[3], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - rendimentos profissionais"));
        this.addRubrica(new Rubrica("ret409", Contrib[3], "Reten\u00e7\u00f5es de IRS auferidas no desempenho de fun\u00e7\u00f5es integradas em miss\u00f5es de car\u00e1cter militar, no estrangeiro"));
        this.addRubrica(new Rubrica("ret410", Contrib[3], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o"));
        this.addRubrica(new Rubrica("ret401", Contrib[4], "Reten\u00e7\u00f5es de IRS do pessoal das miss\u00f5es diplom\u00e1ticas e consulares"));
        this.addRubrica(new Rubrica("ret402", Contrib[4], "Reten\u00e7\u00f5es de IRS do pessoal ao servi\u00e7o de organiza\u00e7\u00f5es estrangeiras ou internacionais"));
        this.addRubrica(new Rubrica("ret403", Contrib[4], "Reten\u00e7\u00f5es de IRS de lucros derivados de obras ou trabalhos das infra-estruturas comuns NATO."));
        this.addRubrica(new Rubrica("ret404", Contrib[4], "Reten\u00e7\u00f5es de recebimentos em capital de import\u00e2ncias despendidas pelas entidades patronais para regimes de seguran\u00e7a social"));
        this.addRubrica(new Rubrica("ret405", Contrib[4], "Reten\u00e7\u00f5es de IRS auferidas na qualidade de tripulante de navios registados no Registo Internacional de Navios"));
        this.addRubrica(new Rubrica("ret406", Contrib[4], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o n\u00e3o dependente de reconhecimento pr\u00e9vio"));
        this.addRubrica(new Rubrica("ret407", Contrib[4], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - trabalho dependente"));
        this.addRubrica(new Rubrica("ret408", Contrib[4], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o - isen\u00e7\u00e3o dependente de reconhecimento pr\u00e9vio - rendimentos profissionais"));
        this.addRubrica(new Rubrica("ret409", Contrib[4], "Reten\u00e7\u00f5es de IRS auferidas no desempenho de fun\u00e7\u00f5es integradas em miss\u00f5es de car\u00e1cter militar, no estrangeiro"));
        this.addRubrica(new Rubrica("ret410", Contrib[4], "Reten\u00e7\u00f5es de IRS auferidas ao abrigo de acordos de coopera\u00e7\u00e3o"));
    }
}

