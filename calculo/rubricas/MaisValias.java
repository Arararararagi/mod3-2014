/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Rubrica;
import calculo.common.util.Utils;

public class MaisValias
extends Rubricas {
    private final int nMaisValias = 17;
    private String bemAlienado;

    public MaisValias(String id) {
        this.inicializa(17);
        this.setID(id);
        this.addRubrica(new Rubrica("valtotRealiBensImo", ""));
        this.addRubrica(new Rubrica("valtotAquisBensImo", ""));
        this.addRubrica(new Rubrica("valtotDespeBensImo", ""));
        this.addRubrica(new Rubrica("valtotRealiPropInt", ""));
        this.addRubrica(new Rubrica("valtotAquisPropInt", ""));
        this.addRubrica(new Rubrica("valtotDespePropInt", ""));
        this.addRubrica(new Rubrica("valtotRealiPosCont", ""));
        this.addRubrica(new Rubrica("valtotAquisPosCont", ""));
        this.addRubrica(new Rubrica("valtotRealiPartSoc", ""));
        this.addRubrica(new Rubrica("valtotAquisPartSoc", ""));
        this.addRubrica(new Rubrica("valtotDespePartSoc", ""));
        this.addRubrica(new Rubrica("valtotInstFinanDer", ""));
        this.addRubrica(new Rubrica("valtotIncrePatrim", ""));
        this.addRubrica(new Rubrica("valtotIncrePatrimRF", ""));
        this.addRubrica(new Rubrica("valperdasAnosAntG", "Perdas de anos anteriores"));
        this.addRubrica(new Rubrica("valdividEmp", "Valor em d\u00edvida do empr\u00e9stimo"));
        this.addRubrica(new Rubrica("valreinvParc", "Valor do reinvestimento"));
        this.bemAlienado = "0";
    }

    public String getBemAlienado() {
        if (this.bemAlienado == null) {
            return "0";
        }
        return this.bemAlienado;
    }

    public void setBemAlienado(String bemAlienado) {
        this.bemAlienado = bemAlienado;
    }

    public String getImpressao(String htmlRMVBensImo, String htmlRMVMaisValias, String htmlVals) {
        String html = "";
        if (htmlRMVBensImo.equals("") && htmlRMVMaisValias.equals("") && htmlVals.equals("")) {
            return html;
        }
        if (!(htmlRMVBensImo.equals("") && htmlRMVMaisValias.equals(""))) {
            html = "<table border=1 width=100% align=center class=tabDetalhe>";
            html = String.valueOf(html) + htmlRMVBensImo + htmlRMVMaisValias;
            html = String.valueOf(html) + "</table>";
        }
        if (!htmlVals.equals("")) {
            html = String.valueOf(html) + "<br><table border=1 width=100% align=center class=tabDetalhe>";
            html = String.valueOf(html) + htmlVals;
            html = String.valueOf(html) + "</table>";
        }
        return html;
    }

    public String getImpressaoMV(boolean header) {
        String linha = "";
        String html = "";
        if (header) {
            html = "<tr><td>&nbsp</td><td align=center nowrap>D.Realiza&ccedil;&atilde;o</td><td align=center nowrap>V.Realiza&ccedil;&atilde;o</td><td align=center nowrap>D.Aquisi&ccedil;&atilde;o</td><td align=center nowrap>V.Aquisi&ccedil;&atilde;o</td><td align=center nowrap>V.Despesas</td></tr>";
        }
        if (this.getRubrica("valtotRealiPropInt").getValor() != 0.0 || this.getRubrica("valtotAquisPropInt").getValor() != 0.0 || this.getRubrica("valtotDespePropInt").getValor() != 0.0) {
            linha = "<tr><td>&nbsp;Aliena&ccedil;&atilde;o de Propriedade Intelectual</td><td align=right width=12%>&nbsp;</td><td align=right width=12%>" + Utils.mascaraValor(this.getRubrica("valtotRealiPropInt").getValor()) + "</td>" + "<td align=right width=12%>&nbsp;</td>" + "<td align=right width=12%>" + Utils.mascaraValor(this.getRubrica("valtotAquisPropInt").getValor()) + "</td>" + "<td align=right width=12%>" + Utils.mascaraValor(this.getRubrica("valtotDespePropInt").getValor()) + "</td></tr>";
            html = String.valueOf(html) + linha;
        }
        if (this.getRubrica("valtotRealiPosCont").getValor() != 0.0 || this.getRubrica("valtotAquisPosCont").getValor() != 0.0) {
            linha = "<tr><td>&nbsp;Cessa&ccedil;&atilde;o de posi&ccedil;&otilde;es contratuais</td><td align=right width=12%>&nbsp;</td><td align=right width=12%>" + Utils.mascaraValor(this.getRubrica("valtotRealiPosCont").getValor()) + "</td>" + "<td align=right width=12%>&nbsp;</td>" + "<td align=right width=12%>" + Utils.mascaraValor(this.getRubrica("valtotAquisPosCont").getValor()) + "</td>" + "<td align=right width=12%>&nbsp;</td></tr>";
            html = String.valueOf(html) + linha;
        }
        if (this.getRubrica("valtotRealiPartSoc").getValor() != 0.0 || this.getRubrica("valtotAquisPartSoc").getValor() != 0.0 || this.getRubrica("valtotDespePartSoc").getValor() != 0.0) {
            linha = "<tr><td>&nbsp;Aliena&ccedil;&atilde;o de partes sociais (*)</td><td align=right width=12%>&nbsp;</td><td align=right width=12%>" + Utils.mascaraValor(this.getRubrica("valtotRealiPartSoc").getValor()) + "</td>" + "<td align=right width=12%>&nbsp;</td>" + "<td align=right width=12%>" + Utils.mascaraValor(this.getRubrica("valtotAquisPartSoc").getValor()) + "</td>" + "<td align=right width=12%>" + Utils.mascaraValor(this.getRubrica("valtotDespePartSoc").getValor()) + "</td></tr>";
            html = String.valueOf(html) + linha;
        }
        return html;
    }

    public boolean comValorMV() {
        if (this.getRubrica("valtotRealiPropInt").getValor() != 0.0 || this.getRubrica("valtotAquisPropInt").getValor() != 0.0 || this.getRubrica("valtotDespePropInt").getValor() != 0.0 || this.getRubrica("valtotRealiPosCont").getValor() != 0.0 || this.getRubrica("valtotAquisPosCont").getValor() != 0.0 || this.getRubrica("valtotRealiPartSoc").getValor() != 0.0 || this.getRubrica("valtotAquisPartSoc").getValor() != 0.0 || this.getRubrica("valtotDespePartSoc").getValor() != 0.0) {
            return true;
        }
        return false;
    }
}

