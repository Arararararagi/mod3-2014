/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;

public class Pensoes
extends Rubricas {
    private final int nPensoes = 48;

    public Pensoes(String id, Contribuinte[] Contrib) {
        this.inicializa(48);
        this.setID(id);
        this.addRubrica(new Rubrica("valrendBrutoCatH", Contrib[0], "Rendimento bruto"));
        this.addRubrica(new Rubrica("PSobrev", Contrib[0], "Pens&otilde;es de Sobreviv&ecirc;ncia"));
        this.addRubrica(new Rubrica("valrendPensAlim", Contrib[0], "Pens&otilde;es de Alimentos"));
        this.addRubrica(new Rubrica("valrendTempVit", Contrib[0], "Rendas"));
        this.addRubrica(new Rubrica("valretencoesFCatH", Contrib[0], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valSobretaxaCatH", Contrib[0], "Sobretaxa extraordin&aacute;ria"));
        this.addRubrica(new Rubrica("valquotizSindCatH", Contrib[0], "Quotiza&ccedil;&otilde;es sindicais"));
        this.addRubrica(new Rubrica("ContribH", Contrib[0], "Contribui&ccedil;&otilde;es Protec&ccedil;&atilde;o Social"));
        this.addRubrica(new Rubrica("valrendBrutoCatH", Contrib[1], "Rendimento bruto"));
        this.addRubrica(new Rubrica("PSobrev", Contrib[1], "Pens&otilde;es de Sobreviv&ecirc;ncia"));
        this.addRubrica(new Rubrica("valrendPensAlim", Contrib[1], "Pens&otilde;es de Alimentos"));
        this.addRubrica(new Rubrica("valrendTempVit", Contrib[1], "Rendas"));
        this.addRubrica(new Rubrica("valretencoesFCatH", Contrib[1], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valSobretaxaCatH", Contrib[1], "Sobretaxa extraordin&aacute;ria"));
        this.addRubrica(new Rubrica("valquotizSindCatH", Contrib[1], "Quotiza&ccedil;&otilde;es sindicais"));
        this.addRubrica(new Rubrica("ContribH", Contrib[1], "Contribui&ccedil;&otilde;es Protec&ccedil;&atilde;o Social"));
        this.addRubrica(new Rubrica("valrendBrutoCatH", Contrib[2], "Rendimento bruto"));
        this.addRubrica(new Rubrica("PSobrev", Contrib[2], "Pens&otilde;es de Sobreviv&ecirc;ncia"));
        this.addRubrica(new Rubrica("valrendPensAlim", Contrib[2], "Pens&otilde;es de Alimentos"));
        this.addRubrica(new Rubrica("valrendTempVit", Contrib[2], "Rendas"));
        this.addRubrica(new Rubrica("valretencoesFCatH", Contrib[2], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valSobretaxaCatH", Contrib[2], "Sobretaxa extraordin&aacute;ria"));
        this.addRubrica(new Rubrica("valquotizSindCatH", Contrib[2], "Quotiza&ccedil;&otilde;es sindicais"));
        this.addRubrica(new Rubrica("ContribH", Contrib[2], "Contribui&ccedil;&otilde;es Protec&ccedil;&atilde;o Social"));
        this.addRubrica(new Rubrica("valrendBrutoCatH", Contrib[3], "Rendimento bruto"));
        this.addRubrica(new Rubrica("PSobrev", Contrib[3], "Pens&otilde;es de Sobreviv&ecirc;ncia"));
        this.addRubrica(new Rubrica("valrendPensAlim", Contrib[3], "Pens&otilde;es de Alimentos"));
        this.addRubrica(new Rubrica("valrendTempVit", Contrib[3], "Rendas"));
        this.addRubrica(new Rubrica("valretencoesFCatH", Contrib[3], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valSobretaxaCatH", Contrib[3], "Sobretaxa extraordin&aacute;ria"));
        this.addRubrica(new Rubrica("valquotizSindCatH", Contrib[3], "Quotiza&ccedil;&otilde;es sindicais"));
        this.addRubrica(new Rubrica("ContribH", Contrib[3], "Contribui&ccedil;&otilde;es Protec&ccedil;&atilde;o Social"));
        this.addRubrica(new Rubrica("valrendBrutoCatH", Contrib[4], "Rendimento bruto"));
        this.addRubrica(new Rubrica("PSobrev", Contrib[4], "Pens&otilde;es de Sobreviv&ecirc;ncia"));
        this.addRubrica(new Rubrica("valrendPensAlim", Contrib[4], "Pens&otilde;es de Alimentos"));
        this.addRubrica(new Rubrica("valrendTempVit", Contrib[4], "Rendas"));
        this.addRubrica(new Rubrica("valretencoesFCatH", Contrib[4], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valSobretaxaCatH", Contrib[4], "Sobretaxa extraordin&aacute;ria"));
        this.addRubrica(new Rubrica("valquotizSindCatH", Contrib[4], "Quotiza&ccedil;&otilde;es sindicais"));
        this.addRubrica(new Rubrica("ContribH", Contrib[4], "Contribui&ccedil;&otilde;es Protec&ccedil;&atilde;o Social"));
        this.addRubrica(new Rubrica("valrendBrutoCatH", Contrib[5], "Rendimento bruto"));
        this.addRubrica(new Rubrica("PSobrev", Contrib[5], "Pens&otilde;es de Sobreviv&ecirc;ncia"));
        this.addRubrica(new Rubrica("valrendPensAlim", Contrib[5], "Pens&otilde;es de Alimentos"));
        this.addRubrica(new Rubrica("valrendTempVit", Contrib[5], "Rendas"));
        this.addRubrica(new Rubrica("valretencoesFCatH", Contrib[5], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valSobretaxaCatH", Contrib[5], "Sobretaxa extraordin&aacute;ria"));
        this.addRubrica(new Rubrica("valquotizSindCatH", Contrib[5], "Quotiza&ccedil;&otilde;es sindicais"));
        this.addRubrica(new Rubrica("ContribH", Contrib[5], "Contribui&ccedil;&otilde;es Protec&ccedil;&atilde;o Social"));
    }
}

