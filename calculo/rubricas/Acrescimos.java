/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Rubrica;
import calculo.common.util.Utils;

public class Acrescimos
extends Rubricas {
    private final int nAcrescimos = 16;

    public Acrescimos(String id) {
        this.inicializa(16);
        this.setID(id);
        this.addRubrica(new Rubrica("valsegurosRend", "Pelo pagamento de seguros"));
        this.addRubrica(new Rubrica("valsegurosCol", "Pelo pagamento de seguros"));
        this.addRubrica(new Rubrica("valPPRRend", "Por atribui&ccedil;&atilde;o de rendimentos de PPR&#47;E"));
        this.addRubrica(new Rubrica("valPPRCol", "Por atribui&ccedil;&atilde;o de rendimentos de PPR&#47;E"));
        this.addRubrica(new Rubrica("valPPALevaRend", "Por levantamento antecipado de PPA"));
        this.addRubrica(new Rubrica("valPPALevaCol", "Por levantamento antecipado de PPA"));
        this.addRubrica(new Rubrica("valPPAIncuRend", "Por incumprimento de condi&ccedil;&otilde;es PPA"));
        this.addRubrica(new Rubrica("valPPAIncuCol", "Por incumprimento de condi&ccedil;&otilde;es PPA"));
        this.addRubrica(new Rubrica("valPPHRend", "Por utiliza&ccedil;&atilde;o de saldos CPH"));
        this.addRubrica(new Rubrica("valPPHCol", "Por utiliza&ccedil;&atilde;o de saldos CPH"));
        this.addRubrica(new Rubrica("valCoopRend", "Por import&acirc;ncias entregues a cooperativas"));
        this.addRubrica(new Rubrica("valCoopCol", "Por import&acirc;ncias entregues a cooperativas"));
        this.addRubrica(new Rubrica("valInobsRend", "Por inobserv&acirc;ncia"));
        this.addRubrica(new Rubrica("valInobsCol", "Por inobserv&acirc;ncia"));
        this.addRubrica(new Rubrica("valCondomRend", "Por utiliza&ccedil;&atilde;o de contas poupan&ccedil;a-condom&iacute;nio"));
        this.addRubrica(new Rubrica("valCondomCol", "Por utiliza&ccedil;&atilde;o de contas poupan&ccedil;a-condom&iacute;nio"));
    }

    public String getImpAcrescimos() {
        String html = "";
        for (int i = 0; i < this.getNumRubricas(); i+=2) {
            if (this.rubricas[i].getValor() == 0.0 && this.rubricas[i + 1].getValor() == 0.0) continue;
            html = String.valueOf(html) + "<tr><td>&nbsp;" + this.rubricas[i + 1].getDesc() + "</td><td align=right>" + Utils.mascValImp(this.rubricas[i + 1].getValor()) + "</td><td align=right>" + Utils.mascValImp(this.rubricas[i].getValor()) + "</td></tr>";
        }
        if (this.comValor() != 0) {
            html = "<tr><td>&nbsp;</td><td width=12% align=center>Colecta</td><td width=12% align=center>Rendimento</td></tr>" + html;
        }
        return html;
    }
}

