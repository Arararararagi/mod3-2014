/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;

public class RendTrabDep
extends Rubricas {
    private final int nRendTrabDep = 60;

    public RendTrabDep(String id, Contribuinte[] Contrib) {
        this.inicializa(60);
        this.setID(id);
        this.addRubrica(new Rubrica("valrendBruto", Contrib[0], "Rendimento bruto"));
        this.addRubrica(new Rubrica("valgratific", Contrib[0], "Gratifica&ccedil;&otilde;es"));
        this.addRubrica(new Rubrica("valretFonteCatA", Contrib[0], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valSobretaxaCatA", Contrib[0], "Sobretaxa extraordin&aacute;ria"));
        this.addRubrica(new Rubrica("valrendDesp", Contrib[0], "Rendimentos de desportistas"));
        this.addRubrica(new Rubrica("valcontObrigSS", Contrib[0], "Contribui&ccedil;&otilde;es obrigat&oacute;rias"));
        this.addRubrica(new Rubrica("valindmnRescUni", Contrib[0], "Indemniza&ccedil;&otilde;es pagas"));
        this.addRubrica(new Rubrica("valquotizSind", Contrib[0], "Quotiza&ccedil;&otilde;es sindicais"));
        this.addRubrica(new Rubrica("valquotizOP", Contrib[0], "Quotiza&ccedil;&otilde;es forma&ccedil;&atilde;o profissional"));
        this.addRubrica(new Rubrica("valseguDesg", Contrib[0], "Rendimentos de Pr\u00e9mios de seguro de desgaste r\u00e1pido"));
        this.addRubrica(new Rubrica("valrendBruto", Contrib[1], "Rendimento bruto"));
        this.addRubrica(new Rubrica("valgratific", Contrib[1], "Gratifica&ccedil;&otilde;es"));
        this.addRubrica(new Rubrica("valretFonteCatA", Contrib[1], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valSobretaxaCatA", Contrib[1], "Sobretaxa extraordin&aacute;ria"));
        this.addRubrica(new Rubrica("valrendDesp", Contrib[1], "Rendimentos de desportistas"));
        this.addRubrica(new Rubrica("valcontObrigSS", Contrib[1], "Contribui&ccedil;&otilde;es obrigat&oacute;rias"));
        this.addRubrica(new Rubrica("valindmnRescUni", Contrib[1], "Indemniza&ccedil;&otilde;es pagas"));
        this.addRubrica(new Rubrica("valquotizSind", Contrib[1], "Quotiza&ccedil;&otilde;es sindicais"));
        this.addRubrica(new Rubrica("valquotizOP", Contrib[1], "Quotiza&ccedil;&otilde;es forma&ccedil;&atilde;o profissional"));
        this.addRubrica(new Rubrica("valseguDesg", Contrib[1], "Rendimentos de Pr\u00e9mios de seguro de desgaste r\u00e1pido"));
        this.addRubrica(new Rubrica("valrendBruto", Contrib[2], "Rendimento bruto"));
        this.addRubrica(new Rubrica("valgratific", Contrib[2], "Gratifica&ccedil;&otilde;es"));
        this.addRubrica(new Rubrica("valretFonteCatA", Contrib[2], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valSobretaxaCatA", Contrib[2], "Sobretaxa extraordin&aacute;ria"));
        this.addRubrica(new Rubrica("valrendDesp", Contrib[2], "Rendimentos de desportistas"));
        this.addRubrica(new Rubrica("valcontObrigSS", Contrib[2], "Contribui&ccedil;&otilde;es obrigat&oacute;rias"));
        this.addRubrica(new Rubrica("valindmnRescUni", Contrib[2], "Indemniza&ccedil;&otilde;es pagas"));
        this.addRubrica(new Rubrica("valquotizSind", Contrib[2], "Quotiza&ccedil;&otilde;es sindicais"));
        this.addRubrica(new Rubrica("valquotizOP", Contrib[2], "Quotiza&ccedil;&otilde;es forma&ccedil;&atilde;o profissional"));
        this.addRubrica(new Rubrica("valseguDesg", Contrib[2], "Rendimentos de Pr\u00e9mios de seguro de desgaste r\u00e1pido"));
        this.addRubrica(new Rubrica("valrendBruto", Contrib[3], "Rendimento bruto"));
        this.addRubrica(new Rubrica("valgratific", Contrib[3], "Gratifica&ccedil;&otilde;es"));
        this.addRubrica(new Rubrica("valretFonteCatA", Contrib[3], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valSobretaxaCatA", Contrib[3], "Sobretaxa extraordin&aacute;ria"));
        this.addRubrica(new Rubrica("valrendDesp", Contrib[3], "Rendimentos de desportistas"));
        this.addRubrica(new Rubrica("valcontObrigSS", Contrib[3], "Contribui&ccedil;&otilde;es obrigat&oacute;rias"));
        this.addRubrica(new Rubrica("valindmnRescUni", Contrib[3], "Indemniza&ccedil;&otilde;es pagas"));
        this.addRubrica(new Rubrica("valquotizSind", Contrib[3], "Quotiza&ccedil;&otilde;es sindicais"));
        this.addRubrica(new Rubrica("valquotizOP", Contrib[3], "Quotiza&ccedil;&otilde;es forma&ccedil;&atilde;o profissional"));
        this.addRubrica(new Rubrica("valseguDesg", Contrib[3], "Rendimentos de Pr\u00e9mios de seguro de desgaste r\u00e1pido"));
        this.addRubrica(new Rubrica("valrendBruto", Contrib[4], "Rendimento bruto"));
        this.addRubrica(new Rubrica("valgratific", Contrib[4], "Gratifica&ccedil;&otilde;es"));
        this.addRubrica(new Rubrica("valretFonteCatA", Contrib[4], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valSobretaxaCatA", Contrib[4], "Sobretaxa extraordin&aacute;ria"));
        this.addRubrica(new Rubrica("valrendDesp", Contrib[4], "Rendimentos de desportistas"));
        this.addRubrica(new Rubrica("valcontObrigSS", Contrib[4], "Contribui&ccedil;&otilde;es obrigat&oacute;rias"));
        this.addRubrica(new Rubrica("valindmnRescUni", Contrib[4], "Indemniza&ccedil;&otilde;es pagas"));
        this.addRubrica(new Rubrica("valquotizSind", Contrib[4], "Quotiza&ccedil;&otilde;es sindicais"));
        this.addRubrica(new Rubrica("valquotizOP", Contrib[4], "Quotiza&ccedil;&otilde;es forma&ccedil;&atilde;o profissional"));
        this.addRubrica(new Rubrica("valseguDesg", Contrib[4], "Rendimentos de Pr\u00e9mios de seguro de desgaste r\u00e1pido"));
        this.addRubrica(new Rubrica("valrendBruto", Contrib[5], "Rendimento bruto"));
        this.addRubrica(new Rubrica("valgratific", Contrib[5], "Gratifica&ccedil;&otilde;es"));
        this.addRubrica(new Rubrica("valretFonteCatA", Contrib[5], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valSobretaxaCatA", Contrib[5], "Sobretaxa extraordin&aacute;ria"));
        this.addRubrica(new Rubrica("valrendDesp", Contrib[5], "Rendimentos de desportistas"));
        this.addRubrica(new Rubrica("valcontObrigSS", Contrib[5], "Contribui&ccedil;&otilde;es obrigat&oacute;rias"));
        this.addRubrica(new Rubrica("valindmnRescUni", Contrib[5], "Indemniza&ccedil;&otilde;es pagas"));
        this.addRubrica(new Rubrica("valquotizSind", Contrib[5], "Quotiza&ccedil;&otilde;es sindicais"));
        this.addRubrica(new Rubrica("valquotizOP", Contrib[5], "Quotiza&ccedil;&otilde;es forma&ccedil;&atilde;o profissional"));
        this.addRubrica(new Rubrica("valseguDesg", Contrib[5], "Rendimentos de Pr\u00e9mios de seguro de desgaste r\u00e1pido"));
    }
}

