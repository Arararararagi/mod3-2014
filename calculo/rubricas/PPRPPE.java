/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;

public class PPRPPE
extends Rubricas {
    private final int nPPRPPE = 7;

    public PPRPPE(String id, Contribuinte[] Contrib) {
        this.inicializa(7);
        this.setID(id);
        this.addRubrica(new Rubrica("valPPR", Contrib[0]));
        this.addRubrica(new Rubrica("valPPR", Contrib[1]));
        this.addRubrica(new Rubrica("valPPE", Contrib[0]));
        this.addRubrica(new Rubrica("valPPE", Contrib[1]));
        this.addRubrica(new Rubrica("valPPRSPF"));
        this.addRubrica(new Rubrica("valPPESPF"));
        this.addRubrica(new Rubrica("valPPRPPEidadeSPF"));
    }
}

