/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Rubrica;
import calculo.business.pojos.RubricaMaisValia;

public class OpFinancei
extends Rubricas {
    private final int nOpFinancei = 3;

    public OpFinancei(String id) {
        this.inicializa(3);
        this.setID(id);
        this.addRubrica(new RubricaMaisValia("valoutros", "Opera&ccedil;&otilde;es de instrumentos financeiros"));
        this.addRubrica(new RubricaMaisValia("vali4", "Warrants aut&oacute;nomos"));
        this.addRubrica(new RubricaMaisValia("valcertif", "Opera&ccedil;&otilde;es relativas a certificados"));
    }
}

