/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Rubrica;
import calculo.business.pojos.RubricaMaisValia;

public class CessaoPos
extends Rubricas {
    private final int nCessaoPos = 4;

    public CessaoPos(String id) {
        this.inicializa(4);
        this.setID(id);
        this.addRubrica(new RubricaMaisValia("CessaoPos1", "Cessao Posi&ccedil;oes 1"));
        this.addRubrica(new RubricaMaisValia("CessaoPos2", "Cessao Posi&ccedil;oes 2"));
        this.addRubrica(new RubricaMaisValia("CessaoPos3", "Cessao Posi&ccedil;oes 3"));
        this.addRubrica(new RubricaMaisValia("CessaoPos4", "Cessao Posi&ccedil;oes 4"));
    }
}

