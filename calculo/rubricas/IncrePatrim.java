/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Rubrica;

public class IncrePatrim
extends Rubricas {
    private final int nIncrePatrim = 8;

    public IncrePatrim(String id) {
        this.inicializa(8);
        this.setID(id);
        this.addRubrica(new Rubrica("valIndemDanos", "Indemniza&ccedil;&otilde;es de danos e lucros"));
        this.addRubrica(new Rubrica("valImpNConc", "Import&acirc;ncias de n&atilde;o concorr&ecirc;ncia"));
        this.addRubrica(new Rubrica("valIndemDanosRF", "Reten&ccedil;&otilde;es de Indemniza&ccedil;&otilde;es de danos e lucros"));
        this.addRubrica(new Rubrica("valImpNConcRF", "Reten&ccedil;&otilde;es de Import&acirc;ncias de n&atilde;o concorr&ecirc;ncia"));
        this.addRubrica(new Rubrica("valAcrPatNaoJust1Rend", "Rendimentos de Acr&eacute;scimos patrimoniais n&atilde;o justificadas - d) do n.\u00ba 1 do art.\u00ba 9.\u00ba do CIRS"));
        this.addRubrica(new Rubrica("valAcrPatNaoJust1Ret", "Reten&ccedil;&otilde;es de Acr&eacute;scimos patrimoniais n&atilde;o justificadas - n.\u00ba 3 do art.\u00ba 9.\u00ba do CIRS"));
        this.addRubrica(new Rubrica("valAcrPatNaoJust3Rend", "Rendimentos de Acr&eacute;scimos patrimoniais n&atilde;o justificadas - d) do n.\u00ba 1 do art.\u00ba 9.\u00ba do CIRS"));
        this.addRubrica(new Rubrica("valAcrPatNaoJust3Ret", "Reten&ccedil;&otilde;es de Acr&eacute;scimos patrimoniais n&atilde;o justificadas - n.\u00ba 3 do art.\u00ba 9.\u00ba do CIRS"));
    }
}

