/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;

public class DespesaProB
extends Rubricas {
    private final int nDespesaProB = 45;

    public DespesaProB(String id, Contribuinte[] Contrib) {
        this.inicializa(45);
        this.setID(id);
        this.addRubrica(new Rubrica("valCustosExistPB", Contrib[0], "Custo das exist&ecirc;ncias"));
        this.addRubrica(new Rubrica("valEncViaturaPB", Contrib[0], "Encargos com viaturas"));
        this.addRubrica(new Rubrica("valValProfissionalPB", Contrib[0], "Valoriza&ccedil;&atilde;o profissional"));
        this.addRubrica(new Rubrica("valDespRepresentacaoPB", Contrib[0], "Despesas de representa&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valDeslocacoesPB", Contrib[0], "Desloca&ccedil;&otilde;es, viagens e estadas"));
        this.addRubrica(new Rubrica("valOutrosEncgPB", Contrib[0], "Outros encargos dedut&iacute;veis"));
        this.addRubrica(new Rubrica("valContObrSegSocP", Contrib[0], "Contribui&ccedil;&otilde;es obrigat&oacute;rias para a seguran&ccedil;a social"));
        this.addRubrica(new Rubrica("valQuotSindP", Contrib[0], "Quotiza&ccedil;&otilde;es Sindicais"));
        this.addRubrica(new Rubrica("valQuotOrdProfP", Contrib[0], "Quotiza&ccedil;&otilde;es para ordens profissionais"));
        this.addRubrica(new Rubrica("valCustosExistPB", Contrib[1], "Custo das exist&ecirc;ncias"));
        this.addRubrica(new Rubrica("valEncViaturaPB", Contrib[1], "Encargos com viaturas"));
        this.addRubrica(new Rubrica("valValProfissionalPB", Contrib[1], "Valoriza&ccedil;&atilde;o profissional"));
        this.addRubrica(new Rubrica("valDespRepresentacaoPB", Contrib[1], "Despesas de representa&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valDeslocacoesPB", Contrib[1], "Desloca&ccedil;&otilde;es, viagens e estadas"));
        this.addRubrica(new Rubrica("valOutrosEncgPB", Contrib[1], "Outros encargos dedut&iacute;veis"));
        this.addRubrica(new Rubrica("valContObrSegSocP", Contrib[1], "Contribui&ccedil;&otilde;es obrigat&oacute;rias para a seguran&ccedil;a social"));
        this.addRubrica(new Rubrica("valQuotSindP", Contrib[1], "Quotiza&ccedil;&otilde;es Sindicais"));
        this.addRubrica(new Rubrica("valQuotOrdProfP", Contrib[1], "Quotiza&ccedil;&otilde;es para ordens profissionais"));
        this.addRubrica(new Rubrica("valCustosExistPB", Contrib[2], "Custo das exist&ecirc;ncias"));
        this.addRubrica(new Rubrica("valEncViaturaPB", Contrib[2], "Encargos com viaturas"));
        this.addRubrica(new Rubrica("valValProfissionalPB", Contrib[2], "Valoriza&ccedil;&atilde;o profissional"));
        this.addRubrica(new Rubrica("valDespRepresentacaoPB", Contrib[2], "Despesas de representa&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valDeslocacoesPB", Contrib[2], "Desloca&ccedil;&otilde;es, viagens e estadas"));
        this.addRubrica(new Rubrica("valOutrosEncgPB", Contrib[2], "Outros encargos dedut&iacute;veis"));
        this.addRubrica(new Rubrica("valContObrSegSocP", Contrib[2], "Contribui&ccedil;&otilde;es obrigat&oacute;rias para a seguran&ccedil;a social"));
        this.addRubrica(new Rubrica("valQuotSindP", Contrib[2], "Quotiza&ccedil;&otilde;es Sindicais"));
        this.addRubrica(new Rubrica("valQuotOrdProfP", Contrib[2], "Quotiza&ccedil;&otilde;es para ordens profissionais"));
        this.addRubrica(new Rubrica("valCustosExistPB", Contrib[3], "Custo das exist&ecirc;ncias"));
        this.addRubrica(new Rubrica("valEncViaturaPB", Contrib[3], "Encargos com viaturas"));
        this.addRubrica(new Rubrica("valValProfissionalPB", Contrib[3], "Valoriza&ccedil;&atilde;o profissional"));
        this.addRubrica(new Rubrica("valDespRepresentacaoPB", Contrib[3], "Despesas de representa&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valDeslocacoesPB", Contrib[3], "Desloca&ccedil;&otilde;es, viagens e estadas"));
        this.addRubrica(new Rubrica("valOutrosEncgPB", Contrib[3], "Outros encargos dedut&iacute;veis"));
        this.addRubrica(new Rubrica("valContObrSegSocP", Contrib[3], "Contribui&ccedil;&otilde;es obrigat&oacute;rias para a seguran&ccedil;a social"));
        this.addRubrica(new Rubrica("valQuotSindP", Contrib[3], "Quotiza&ccedil;&otilde;es Sindicais"));
        this.addRubrica(new Rubrica("valQuotOrdProfP", Contrib[3], "Quotiza&ccedil;&otilde;es para ordens profissionais"));
        this.addRubrica(new Rubrica("valCustosExistPB", Contrib[4], "Custo das exist&ecirc;ncias"));
        this.addRubrica(new Rubrica("valEncViaturaPB", Contrib[4], "Encargos com viaturas"));
        this.addRubrica(new Rubrica("valValProfissionalPB", Contrib[4], "Valoriza&ccedil;&atilde;o profissional"));
        this.addRubrica(new Rubrica("valDespRepresentacaoPB", Contrib[4], "Despesas de representa&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valDeslocacoesPB", Contrib[4], "Desloca&ccedil;&otilde;es, viagens e estadas"));
        this.addRubrica(new Rubrica("valOutrosEncgPB", Contrib[4], "Outros encargos dedut&iacute;veis"));
        this.addRubrica(new Rubrica("valContObrSegSocP", Contrib[4], "Contribui&ccedil;&otilde;es obrigat&oacute;rias para a seguran&ccedil;a social"));
        this.addRubrica(new Rubrica("valQuotSindP", Contrib[4], "Quotiza&ccedil;&otilde;es Sindicais"));
        this.addRubrica(new Rubrica("valQuotOrdProfP", Contrib[4], "Quotiza&ccedil;&otilde;es para ordens profissionais"));
    }
}

