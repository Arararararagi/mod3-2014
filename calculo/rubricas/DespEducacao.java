/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Rubrica;

public class DespEducacao
extends Rubricas {
    private final int nDespEducacao = 2;
    private int numDepDespFPr;

    public DespEducacao(String id) {
        this.inicializa(2);
        this.setID(id);
        this.addRubrica(new Rubrica("valdiscrimDEducSP"));
        this.addRubrica(new Rubrica("valdiscrimDEducD"));
        this.numDepDespFPr = 0;
    }

    public int getNumDepDespFPr() {
        return this.numDepDespFPr;
    }

    public void setNumDepDespFPr(int numDep) {
        this.numDepDespFPr = numDep;
    }

    public void setNumDepDespFPrHt(Object numDep) {
        Integer depsDesp = (Integer)numDep;
        this.numDepDespFPr = depsDesp;
    }

    public void setNumDepDespFPr(String numDep) {
        this.numDepDespFPr = Integer.parseInt(numDep);
    }

    public void inicialDependentes() {
        this.rubricas[1].setValor(0.0);
    }
}

