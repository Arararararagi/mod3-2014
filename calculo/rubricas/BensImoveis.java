/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.RubricasMaisValia;
import calculo.business.pojos.Rubrica;
import calculo.business.pojos.RubricaMaisValia;

public class BensImoveis
extends RubricasMaisValia {
    private final int nBensImoveis = 8;

    public BensImoveis(String id) {
        this.inicializa(8);
        this.setID(id);
        this.addRubrica(new RubricaMaisValia("BensImoveis1", "Aliena&ccedil;&atilde;o de bens im&oacute;veis"));
        this.addRubrica(new RubricaMaisValia("BensImoveis2", "Aliena&ccedil;&atilde;o de bens im&oacute;veis"));
        this.addRubrica(new RubricaMaisValia("BensImoveis3", "Aliena&ccedil;&atilde;o de bens im&oacute;veis"));
        this.addRubrica(new RubricaMaisValia("BensImoveis4", "Aliena&ccedil;&atilde;o de bens im&oacute;veis"));
        this.addRubrica(new RubricaMaisValia("BensImoveisRec1", "Aliena&ccedil;&atilde;o de bens im&oacute;veis"));
        this.addRubrica(new RubricaMaisValia("BensImoveisRec2", "Aliena&ccedil;&atilde;o de bens im&oacute;veis"));
        this.addRubrica(new RubricaMaisValia("BensImoveisRec3", "Aliena&ccedil;&atilde;o de bens im&oacute;veis"));
        this.addRubrica(new RubricaMaisValia("BensImoveisRec4", "Aliena&ccedil;&atilde;o de bens im&oacute;veis"));
    }
}

