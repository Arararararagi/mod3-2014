/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;

public class AnexoD
extends Rubricas {
    private final int nAnexoD = 55;

    public AnexoD(String id, Contribuinte[] Contrib) {
        this.inicializa(55);
        this.setID(id);
        this.addRubrica(new Rubrica("chkAgriVSProfD", Contrib[0], "Agr&iacute;colas &#47; Profissionais"));
        this.addRubrica(new Rubrica("valLucroPrejProD", Contrib[0], "Lucro &#47; Preju&Iacute;zo &#45; Profiss."));
        this.addRubrica(new Rubrica("valLucroPrejAgrD", Contrib[0], "Lucro &#47; Preju&Iacute;zo &#45; Agr&iacutecola"));
        this.addRubrica(new Rubrica("valRetFonteD", Contrib[0], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valPagamenContaD", Contrib[0], "Pagamentos por conta"));
        this.addRubrica(new Rubrica("valRendLiqImpP", Contrib[0], "Rendimentos l&iacute;quidos imputados Profissionais"));
        this.addRubrica(new Rubrica("valAdContLucrosP", Contrib[0], "Adiamentos por conta de lucros Profissionais"));
        this.addRubrica(new Rubrica("valAjustamentosP", Contrib[0], "Ajustamentos Profissionais"));
        this.addRubrica(new Rubrica("valRendLiqImpA", Contrib[0], "Rendimentos l&iacute;quidos imputados Agr&iacutecola"));
        this.addRubrica(new Rubrica("valAdContLucrosA", Contrib[0], "Adiamentos por conta de lucros Agr&iacutecola"));
        this.addRubrica(new Rubrica("valAjustamentosA", Contrib[0], "Ajustamentos Agr&iacutecola"));
        this.addRubrica(new Rubrica("chkAgriVSProfD", Contrib[1], "Agr&iacute;colas &#47; Profissionais"));
        this.addRubrica(new Rubrica("valLucroPrejProD", Contrib[1], "Lucro &#47; Preju&Iacute;zo &#45; Profiss."));
        this.addRubrica(new Rubrica("valLucroPrejAgrD", Contrib[1], "Lucro &#47; Preju&Iacute;zo &#45; Agr&iacutecola"));
        this.addRubrica(new Rubrica("valRetFonteD", Contrib[1], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valPagamenContaD", Contrib[1], "Pagamentos por conta"));
        this.addRubrica(new Rubrica("valRendLiqImpP", Contrib[1], "Rendimentos l&iacute;quidos imputados Profissionais"));
        this.addRubrica(new Rubrica("valAdContLucrosP", Contrib[1], "Adiamentos por conta de lucros Profissionais"));
        this.addRubrica(new Rubrica("valAjustamentosP", Contrib[1], "Ajustamentos Profissionais"));
        this.addRubrica(new Rubrica("valRendLiqImpA", Contrib[1], "Rendimentos l&iacute;quidos imputados Agr&iacutecola"));
        this.addRubrica(new Rubrica("valAdContLucrosA", Contrib[1], "Adiamentos por conta de lucros Agr&iacutecola"));
        this.addRubrica(new Rubrica("valAjustamentosA", Contrib[1], "Ajustamentos Agr&iacutecola"));
        this.addRubrica(new Rubrica("chkAgriVSProfD", Contrib[2], "Agr&iacute;colas &#47; Profissionais"));
        this.addRubrica(new Rubrica("valLucroPrejProD", Contrib[2], "Lucro &#47; Preju&Iacute;zo &#45; Profiss."));
        this.addRubrica(new Rubrica("valLucroPrejAgrD", Contrib[2], "Lucro &#47; Preju&Iacute;zo &#45; Agr&iacutecola"));
        this.addRubrica(new Rubrica("valRetFonteD", Contrib[2], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valPagamenContaD", Contrib[2], "Pagamentos por conta"));
        this.addRubrica(new Rubrica("valRendLiqImpP", Contrib[2], "Rendimentos l&iacute;quidos imputados Profissionais"));
        this.addRubrica(new Rubrica("valAdContLucrosP", Contrib[2], "Adiamentos por conta de lucros Profissionais"));
        this.addRubrica(new Rubrica("valAjustamentosP", Contrib[2], "Ajustamentos Profissionais"));
        this.addRubrica(new Rubrica("valRendLiqImpA", Contrib[2], "Rendimentos l&iacute;quidos imputados Agr&iacutecola"));
        this.addRubrica(new Rubrica("valAdContLucrosA", Contrib[2], "Adiamentos por conta de lucros Agr&iacutecola"));
        this.addRubrica(new Rubrica("valAjustamentosA", Contrib[2], "Ajustamentos Agr&iacutecola"));
        this.addRubrica(new Rubrica("chkAgriVSProfD", Contrib[3], "Agr&iacute;colas &#47; Profissionais"));
        this.addRubrica(new Rubrica("valLucroPrejProD", Contrib[3], "Lucro &#47; Preju&Iacute;zo &#45; Profiss."));
        this.addRubrica(new Rubrica("valLucroPrejAgrD", Contrib[3], "Lucro &#47; Preju&Iacute;zo &#45; Agr&iacutecola"));
        this.addRubrica(new Rubrica("valRetFonteD", Contrib[3], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valPagamenContaD", Contrib[3], "Pagamentos por conta"));
        this.addRubrica(new Rubrica("valRendLiqImpP", Contrib[3], "Rendimentos l&iacute;quidos imputados Profissionais"));
        this.addRubrica(new Rubrica("valAdContLucrosP", Contrib[3], "Adiamentos por conta de lucros Profissionais"));
        this.addRubrica(new Rubrica("valAjustamentosP", Contrib[3], "Ajustamentos Profissionais"));
        this.addRubrica(new Rubrica("valRendLiqImpA", Contrib[3], "Rendimentos l&iacute;quidos imputados Agr&iacutecola"));
        this.addRubrica(new Rubrica("valAdContLucrosA", Contrib[3], "Adiamentos por conta de lucros Agr&iacutecola"));
        this.addRubrica(new Rubrica("valAjustamentosA", Contrib[3], "Ajustamentos Agr&iacutecola"));
        this.addRubrica(new Rubrica("chkAgriVSProfD", Contrib[4], "Agr&iacute;colas &#47; Profissionais"));
        this.addRubrica(new Rubrica("valLucroPrejProD", Contrib[4], "Lucro &#47; Preju&Iacute;zo &#45; Profiss."));
        this.addRubrica(new Rubrica("valLucroPrejAgrD", Contrib[4], "Lucro &#47; Preju&Iacute;zo &#45; Agr&iacutecola"));
        this.addRubrica(new Rubrica("valRetFonteD", Contrib[4], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valPagamenContaD", Contrib[4], "Pagamentos por conta"));
        this.addRubrica(new Rubrica("valRendLiqImpP", Contrib[4], "Rendimentos l&iacute;quidos imputados Profissionais"));
        this.addRubrica(new Rubrica("valAdContLucrosP", Contrib[4], "Adiamentos por conta de lucros Profissionais"));
        this.addRubrica(new Rubrica("valAjustamentosP", Contrib[4], "Ajustamentos Profissionais"));
        this.addRubrica(new Rubrica("valRendLiqImpA", Contrib[4], "Rendimentos l&iacute;quidos imputados Agr&iacutecola"));
        this.addRubrica(new Rubrica("valAdContLucrosA", Contrib[4], "Adiamentos por conta de lucros Agr&iacutecola"));
        this.addRubrica(new Rubrica("valAjustamentosA", Contrib[4], "Ajustamentos Agr&iacutecola"));
    }
}

