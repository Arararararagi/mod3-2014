/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;

public class DespesaAgrB
extends Rubricas {
    private final int nDespesaAgrB = 45;

    public DespesaAgrB(String id, Contribuinte[] Contrib) {
        this.inicializa(45);
        this.setID(id);
        this.addRubrica(new Rubrica("valCustosExistAB", Contrib[0], "Custo das exist&ecirc;ncias"));
        this.addRubrica(new Rubrica("valEncViaturaAB", Contrib[0], "Encargos com viaturas"));
        this.addRubrica(new Rubrica("valValProfissionalAB", Contrib[0], "Valoriza&ccedil;&atilde;o profissional"));
        this.addRubrica(new Rubrica("valDespRepresentacaoAB", Contrib[0], "Despesas de representa&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valDeslocacoesAB", Contrib[0], "Desloca&ccedil;&otilde;es, viagens e estadas"));
        this.addRubrica(new Rubrica("valOutrosEncgAB", Contrib[0], "Outros encargos dedut&iacute;veis"));
        this.addRubrica(new Rubrica("valContObrSegSocA", Contrib[0], "Contribui&ccedil;&otilde;es obrigat&oacute;rias para a seguran&ccedil;a social"));
        this.addRubrica(new Rubrica("valQuotSindA", Contrib[0], "Quotiza&ccedil;&otilde;es Sindicais"));
        this.addRubrica(new Rubrica("valQuotOrdProfA", Contrib[0], "Quotiza&ccedil;&otilde;es para ordens profissionais"));
        this.addRubrica(new Rubrica("valCustosExistAB", Contrib[1], "Custo das exist&ecirc;ncias"));
        this.addRubrica(new Rubrica("valEncViaturaAB", Contrib[1], "Encargos com viaturas"));
        this.addRubrica(new Rubrica("valValProfissionalAB", Contrib[1], "Valoriza&ccedil;&atilde;o profissional"));
        this.addRubrica(new Rubrica("valDespRepresentacaoAB", Contrib[1], "Despesas de representa&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valDeslocacoesAB", Contrib[1], "Desloca&ccedil;&otilde;es, viagens e estadas"));
        this.addRubrica(new Rubrica("valOutrosEncgAB", Contrib[1], "Outros encargos dedut&iacute;veis"));
        this.addRubrica(new Rubrica("valContObrSegSocA", Contrib[1], "Contribui&ccedil;&otilde;es obrigat&oacute;rias para a seguran&ccedil;a social"));
        this.addRubrica(new Rubrica("valQuotSindA", Contrib[1], "Quotiza&ccedil;&otilde;es Sindicais"));
        this.addRubrica(new Rubrica("valQuotOrdProfA", Contrib[1], "Quotiza&ccedil;&otilde;es para ordens profissionais"));
        this.addRubrica(new Rubrica("valCustosExistAB", Contrib[2], "Custo das exist&ecirc;ncias"));
        this.addRubrica(new Rubrica("valEncViaturaAB", Contrib[2], "Encargos com viaturas"));
        this.addRubrica(new Rubrica("valValProfissionalAB", Contrib[2], "Valoriza&ccedil;&atilde;o profissional"));
        this.addRubrica(new Rubrica("valDespRepresentacaoAB", Contrib[2], "Despesas de representa&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valDeslocacoesAB", Contrib[2], "Desloca&ccedil;&otilde;es, viagens e estadas"));
        this.addRubrica(new Rubrica("valOutrosEncgAB", Contrib[2], "Outros encargos dedut&iacute;veis"));
        this.addRubrica(new Rubrica("valContObrSegSocA", Contrib[2], "Contribui&ccedil;&otilde;es obrigat&oacute;rias para a seguran&ccedil;a social"));
        this.addRubrica(new Rubrica("valQuotSindA", Contrib[2], "Quotiza&ccedil;&otilde;es Sindicais"));
        this.addRubrica(new Rubrica("valQuotOrdProfA", Contrib[2], "Quotiza&ccedil;&otilde;es para ordens profissionais"));
        this.addRubrica(new Rubrica("valCustosExistAB", Contrib[3], "Custo das exist&ecirc;ncias"));
        this.addRubrica(new Rubrica("valEncViaturaAB", Contrib[3], "Encargos com viaturas"));
        this.addRubrica(new Rubrica("valValProfissionalAB", Contrib[3], "Valoriza&ccedil;&atilde;o profissional"));
        this.addRubrica(new Rubrica("valDespRepresentacaoAB", Contrib[3], "Despesas de representa&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valDeslocacoesAB", Contrib[3], "Desloca&ccedil;&otilde;es, viagens e estadas"));
        this.addRubrica(new Rubrica("valOutrosEncgAB", Contrib[3], "Outros encargos dedut&iacute;veis"));
        this.addRubrica(new Rubrica("valContObrSegSocA", Contrib[3], "Contribui&ccedil;&otilde;es obrigat&oacute;rias para a seguran&ccedil;a social"));
        this.addRubrica(new Rubrica("valQuotSindA", Contrib[3], "Quotiza&ccedil;&otilde;es Sindicais"));
        this.addRubrica(new Rubrica("valQuotOrdProfA", Contrib[3], "Quotiza&ccedil;&otilde;es para ordens profissionais"));
        this.addRubrica(new Rubrica("valCustosExistAB", Contrib[4], "Custo das exist&ecirc;ncias"));
        this.addRubrica(new Rubrica("valEncViaturaAB", Contrib[4], "Encargos com viaturas"));
        this.addRubrica(new Rubrica("valValProfissionalAB", Contrib[4], "Valoriza&ccedil;&atilde;o profissional"));
        this.addRubrica(new Rubrica("valDespRepresentacaoAB", Contrib[4], "Despesas de representa&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valDeslocacoesAB", Contrib[4], "Desloca&ccedil;&otilde;es, viagens e estadas"));
        this.addRubrica(new Rubrica("valOutrosEncgAB", Contrib[4], "Outros encargos dedut&iacute;veis"));
        this.addRubrica(new Rubrica("valContObrSegSocA", Contrib[4], "Contribui&ccedil;&otilde;es obrigat&oacute;rias para a seguran&ccedil;a social"));
        this.addRubrica(new Rubrica("valQuotSindA", Contrib[4], "Quotiza&ccedil;&otilde;es Sindicais"));
        this.addRubrica(new Rubrica("valQuotOrdProfA", Contrib[4], "Quotiza&ccedil;&otilde;es para ordens profissionais"));
    }
}

