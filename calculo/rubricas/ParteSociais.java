/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.RubricasMaisValia;
import calculo.business.pojos.Rubrica;
import calculo.business.pojos.RubricaMaisValia;

public class ParteSociais
extends RubricasMaisValia {
    private final int nParteSociais = 5;

    public ParteSociais(String id) {
        this.inicializa(5);
        this.setID(id);
        this.addRubrica(new RubricaMaisValia("ParteSociais1", "Partes Sociais"));
        this.addRubrica(new RubricaMaisValia("ParteSociais2", "Partes Sociais"));
        this.addRubrica(new RubricaMaisValia("ParteSociais3", "Partes Sociais"));
        this.addRubrica(new RubricaMaisValia("ParteSociais4", "Partes Sociais"));
        this.addRubrica(new RubricaMaisValia("ParteSociais5", "Partes Sociais"));
    }
}

