/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;

public class CategoriaB
extends Rubricas {
    private final int nCategoriaB = 45;

    public CategoriaB(String id, Contribuinte[] Contrib) {
        this.inicializa(45);
        this.setID(id);
        this.addRubrica(new Rubrica("chkAgriVSProfB", Contrib[0], "Agr&iacute;colas &#47; Profissionais"));
        this.addRubrica(new Rubrica("chkSimpVSIsolB", Contrib[0], "R. simplificado &#47; A. isolado"));
        this.addRubrica(new Rubrica("RendimProB", Contrib[0], ""));
        this.addRubrica(new Rubrica("RendimAgrB", Contrib[0], ""));
        this.addRubrica(new Rubrica("DespesaProB", Contrib[0], ""));
        this.addRubrica(new Rubrica("DespesaAgrB", Contrib[0], ""));
        this.addRubrica(new Rubrica("valRetFonteB", Contrib[0], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valPagamenContaB", Contrib[0], "Pagamentos por conta"));
        this.addRubrica(new Rubrica("valTributacaoCatA", Contrib[0], "Tributa\u00e7\u00e3o com regras da Cat. A"));
        this.addRubrica(new Rubrica("chkAgriVSProfB", Contrib[1], "Agr&iacute;colas &#47; Profissionais"));
        this.addRubrica(new Rubrica("chkSimpVSIsolB", Contrib[1], "R. simplificado &#47; A. isolado"));
        this.addRubrica(new Rubrica("RendimProB", Contrib[1], ""));
        this.addRubrica(new Rubrica("RendimAgrB", Contrib[1], ""));
        this.addRubrica(new Rubrica("DespesaProB", Contrib[1], ""));
        this.addRubrica(new Rubrica("DespesaAgrB", Contrib[1], ""));
        this.addRubrica(new Rubrica("valRetFonteB", Contrib[1], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valPagamenContaB", Contrib[1], "Pagamentos por conta"));
        this.addRubrica(new Rubrica("valTributacaoCatA", Contrib[1], "Tributa\u00e7\u00e3o com regras da Cat. A"));
        this.addRubrica(new Rubrica("chkAgriVSProfB", Contrib[2], "Agr&iacute;colas &#47; Profissionais"));
        this.addRubrica(new Rubrica("chkSimpVSIsolB", Contrib[2], "R. simplificado &#47; A. isolado"));
        this.addRubrica(new Rubrica("RendimProB", Contrib[2], ""));
        this.addRubrica(new Rubrica("RendimAgrB", Contrib[2], ""));
        this.addRubrica(new Rubrica("DespesaProB", Contrib[2], ""));
        this.addRubrica(new Rubrica("DespesaAgrB", Contrib[2], ""));
        this.addRubrica(new Rubrica("valRetFonteB", Contrib[2], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valPagamenContaB", Contrib[2], "Pagamentos por conta"));
        this.addRubrica(new Rubrica("valTributacaoCatA", Contrib[2], "Tributa\u00e7\u00e3o com regras da Cat. A"));
        this.addRubrica(new Rubrica("chkAgriVSProfB", Contrib[3], "Agr&iacute;colas &#47; Profissionais"));
        this.addRubrica(new Rubrica("chkSimpVSIsolB", Contrib[3], "R. simplificado &#47; A. isolado"));
        this.addRubrica(new Rubrica("RendimProB", Contrib[3], ""));
        this.addRubrica(new Rubrica("RendimAgrB", Contrib[3], ""));
        this.addRubrica(new Rubrica("DespesaProB", Contrib[3], ""));
        this.addRubrica(new Rubrica("DespesaAgrB", Contrib[3], ""));
        this.addRubrica(new Rubrica("valRetFonteB", Contrib[3], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valPagamenContaB", Contrib[3], "Pagamentos por conta"));
        this.addRubrica(new Rubrica("valTributacaoCatA", Contrib[3], "Tributa\u00e7\u00e3o com regras da Cat. A"));
        this.addRubrica(new Rubrica("chkAgriVSProfB", Contrib[4], "Agr&iacute;colas &#47; Profissionais"));
        this.addRubrica(new Rubrica("chkSimpVSIsolB", Contrib[4], "R. simplificado &#47; A. isolado"));
        this.addRubrica(new Rubrica("RendimProB", Contrib[4], ""));
        this.addRubrica(new Rubrica("RendimAgrB", Contrib[4], ""));
        this.addRubrica(new Rubrica("DespesaProB", Contrib[4], ""));
        this.addRubrica(new Rubrica("DespesaAgrB", Contrib[4], ""));
        this.addRubrica(new Rubrica("valRetFonteB", Contrib[4], "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valPagamenContaB", Contrib[4], "Pagamentos por conta"));
        this.addRubrica(new Rubrica("valTributacaoCatA", Contrib[4], "Tributa\u00e7\u00e3o com regras da Cat. A"));
    }
}

