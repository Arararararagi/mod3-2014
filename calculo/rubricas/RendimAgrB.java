/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;

public class RendimAgrB
extends Rubricas {
    private final int nRendimAgrB = 30;

    public RendimAgrB(String id, Contribuinte[] Contrib) {
        this.inicializa(30);
        this.setID(id);
        this.addRubrica(new Rubrica("valVendasMercadoA", Contrib[0], "Vendas de mercadorias e produtos"));
        this.addRubrica(new Rubrica("valOutrosServicosA", Contrib[0], "Presta&ccedil;&otilde;es de servi&ccedil;os"));
        this.addRubrica(new Rubrica("valSubsidiosExplA", Contrib[0], "Subs&iacute;dios &agrave; explora&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valOutrosSubsidiosA", Contrib[0], "Outros Subs&iacute;dios"));
        this.addRubrica(new Rubrica("valRendimentoDiversosBA", Contrib[0], "Rendimentos de capitais e prediais, propriedade intelectual, industrial ou prest. de informa&ccedil;&otilde;es e restantes incrementos"));
        this.addRubrica(new Rubrica("valRendimentoNaoIncluidoA", Contrib[0], "Rendimentos n&atilde;o inclu&iacute;dos nos campos anteriores"));
        this.addRubrica(new Rubrica("valVendasMercadoA", Contrib[1], "Vendas de mercadorias e produtos"));
        this.addRubrica(new Rubrica("valOutrosServicosA", Contrib[1], "Presta&ccedil;&otilde;es de servi&ccedil;os"));
        this.addRubrica(new Rubrica("valSubsidiosExplA", Contrib[1], "Subs&iacute;dios &agrave; explora&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valOutrosSubsidiosA", Contrib[1], "Outros Subs&iacute;dios"));
        this.addRubrica(new Rubrica("valRendimentoDiversosBA", Contrib[1], "Rendimentos de capitais e prediais, propriedade intelectual, industrial ou prest. de informa&ccedil;&otilde;es e restantes incrementos"));
        this.addRubrica(new Rubrica("valRendimentoNaoIncluidoA", Contrib[1], "Rendimentos n&atilde;o inclu&iacute;dos nos campos anteriores"));
        this.addRubrica(new Rubrica("valVendasMercadoA", Contrib[2], "Vendas de mercadorias e produtos"));
        this.addRubrica(new Rubrica("valOutrosServicosA", Contrib[2], "Presta&ccedil;&otilde;es de servi&ccedil;os"));
        this.addRubrica(new Rubrica("valSubsidiosExplA", Contrib[2], "Subs&iacute;dios &agrave; explora&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valOutrosSubsidiosA", Contrib[2], "Outros Subs&iacute;dios"));
        this.addRubrica(new Rubrica("valRendimentoDiversosBA", Contrib[2], "Rendimentos de capitais e prediais, propriedade intelectual, industrial ou prest. de informa&ccedil;&otilde;es e restantes incrementos"));
        this.addRubrica(new Rubrica("valRendimentoNaoIncluidoA", Contrib[2], "Rendimentos n&atilde;o inclu&iacute;dos nos campos anteriores"));
        this.addRubrica(new Rubrica("valVendasMercadoA", Contrib[3], "Vendas de mercadorias e produtos"));
        this.addRubrica(new Rubrica("valOutrosServicosA", Contrib[3], "Presta&ccedil;&otilde;es de servi&ccedil;os"));
        this.addRubrica(new Rubrica("valSubsidiosExplA", Contrib[3], "Subs&iacute;dios &agrave; explora&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valOutrosSubsidiosA", Contrib[3], "Outros Subs&iacute;dios"));
        this.addRubrica(new Rubrica("valRendimentoDiversosBA", Contrib[3], "Rendimentos de capitais e prediais, propriedade intelectual, industrial ou prest. de informa&ccedil;&otilde;es e restantes incrementos"));
        this.addRubrica(new Rubrica("valRendimentoNaoIncluidoA", Contrib[3], "Rendimentos n&atilde;o inclu&iacute;dos nos campos anteriores"));
        this.addRubrica(new Rubrica("valVendasMercadoA", Contrib[4], "Vendas de mercadorias e produtos"));
        this.addRubrica(new Rubrica("valOutrosServicosA", Contrib[4], "Presta&ccedil;&otilde;es de servi&ccedil;os"));
        this.addRubrica(new Rubrica("valSubsidiosExplA", Contrib[4], "Subs&iacute;dios &agrave; explora&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valOutrosSubsidiosA", Contrib[4], "Outros Subs&iacute;dios"));
        this.addRubrica(new Rubrica("valRendimentoDiversosBA", Contrib[4], "Rendimentos de capitais e prediais, propriedade intelectual, industrial ou prest. de informa&ccedil;&otilde;es e restantes incrementos"));
        this.addRubrica(new Rubrica("valRendimentoNaoIncluidoA", Contrib[4], "Rendimentos n&atilde;o inclu&iacute;dos nos campos anteriores"));
    }
}

