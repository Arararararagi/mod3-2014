/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Rubrica;

public class BenefFiscais
extends Rubricas {
    private final int nBenefFiscais = 32;

    public BenefFiscais(String id) {
        this.inicializa(32);
        this.setID(id);
        this.addRubrica(new Rubrica("valPPRPPE", "PPR e Contribui&ccedil;&otilde;es fundos pens&otilde;es"));
        this.addRubrica(new Rubrica("valPPH", "CPH"));
        this.addRubrica(new Rubrica("valPPA", "PPA"));
        this.addRubrica(new Rubrica("valdespEducSPDdef", "Despesas de educa&ccedil;&atilde;o - deficientes"));
        this.addRubrica(new Rubrica("valpSegurosSPDdef", "Pr&eacute;mios de seguros - deficientes"));
        this.addRubrica(new Rubrica("valaquisicaoComput", "Aquisi&ccedil;&atilde;o de computadores"));
        this.addRubrica(new Rubrica("valentregaCoopHabi", "Entregas a cooperativas de habita&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valentregaCooperad", "Entregas pelos cooperadores"));
        this.addRubrica(new Rubrica("valIvaAquiServicos", "IVA suportado com a aquisi&ccedil;&atilde;o de servi&ccedil;os"));
        this.addRubrica(new Rubrica("valmecenatoCientifico", "Donativos - Mecenato cient&iacute;fico"));
        this.addRubrica(new Rubrica("valmecenatoCultural", "Donativos - Mecenato cultural"));
        this.addRubrica(new Rubrica("valcontratosPluriA", "Donativos - Contratos plurianuais"));
        this.addRubrica(new Rubrica("valmecenatoSocial", "Donativos - Mecenato social"));
        this.addRubrica(new Rubrica("valmecenatoSocEsp", "Donativos - Mecenato social de apoio especial"));
        this.addRubrica(new Rubrica("valmecenatoSocInf", "Donativos - Mecenato sociedade de informa&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valmecenatoFamiliar", "Donativos - Mecenato familiar"));
        this.addRubrica(new Rubrica("valestadoMecCientifico", "Donativos - Estado, Mecenato cient&iacute;fico"));
        this.addRubrica(new Rubrica("valestadoMecCultural", "Donativos - Estado, Mecenato cultural"));
        this.addRubrica(new Rubrica("valestadoContPluri", "Donativos - Estado, Contratos plurianuais"));
        this.addRubrica(new Rubrica("valestadoMecSocial", "Donativos - Estado, Mecenato social"));
        this.addRubrica(new Rubrica("valestadoMecFamiliar", "Donativos - Estado, Mecenato familiar"));
        this.addRubrica(new Rubrica("valCentRepublica", "Donativos - Comemora&ccedil;&otilde;es do Centen&aacute;rio da Rep&uacute;blica"));
        this.addRubrica(new Rubrica("valCentRepubPluri", "Donativos - Comemora&ccedil;&otilde;es do Centen&aacute;rio da Rep&uacute;blica - Contratos Plurianuais"));
        this.addRubrica(new Rubrica("valRPC", "Regime p&uacute;blico de capitaliza&ccedil;&atilde;o"));
        Rubrica rub = new Rubrica("classEnergetica", "");
        rub.setValor(0.0);
        this.addRubrica(rub);
        Rubrica rub2 = new Rubrica("classEnergeticaR", "");
        rub2.setValor(0.0);
        this.addRubrica(rub2);
        this.addRubrica(new Rubrica("valObrasMelhTermico", "Obras de melhoria de comportamento t&eacute;rmico"));
        this.addRubrica(new Rubrica("valVeiculosNaoPoluentes", "Ve&iacute;culos n&atilde;o poluentes"));
        this.addRubrica(new Rubrica("valpSegurosSPDdefGC", "Pr&eacute;mios de seguros - deficientes - guarda conjunta"));
        this.addRubrica(new Rubrica("valpSegurosSPDdefOut", "Pr&eacute;mios de seguros - deficientes - outros membros do agregado"));
        this.addRubrica(new Rubrica("valdespEducSPDdefGC", "Despesas de educa&ccedil;&atilde;o - deficientes - guarda conjunta"));
        this.addRubrica(new Rubrica("valdespEducSPDdefOut", "Despesas de educa&ccedil;&atilde;o - deficientes - outros"));
    }

    public void actualizaPPA(Rubricas rbr) {
        double ppaA = rbr.getRubrica("valPPA", "SPA").getValor();
        double ppaB = rbr.getRubrica("valPPA", "SPB").getValor();
        this.getRubrica("valPPA").setValor(ppaA + ppaB);
    }

    public void actualizaPPR(Rubricas rbr) {
        double pprA = rbr.getRubrica("valPPR", "SPA").getValor();
        double pprB = rbr.getRubrica("valPPR", "SPB").getValor();
        double ppeA = rbr.getRubrica("valPPE", "SPA").getValor();
        double ppeB = rbr.getRubrica("valPPE", "SPB").getValor();
        double pprF = rbr.getRubrica("valPPRSPF").getValor();
        double ppeF = rbr.getRubrica("valPPESPF").getValor();
        this.getRubrica("valPPRPPE").setValor(pprA + pprB + ppeA + ppeB + pprF + ppeF);
    }
}

