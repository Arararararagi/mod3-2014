/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Rubrica;

public class AbatDedColecta
extends Rubricas {
    private final int nAbatDedColecta = 42;

    public AbatDedColecta(String id) {
        this.inicializa(42);
        this.setID(id);
        this.addRubrica(new Rubrica("valpensoesObrigJudic", "Pens&otilde;es obrigat&oacute;rias"));
        this.addRubrica(new Rubrica("valimportRendasHabiP", "Rendas pagas habit. perm. NRAU"));
        this.addRubrica(new Rubrica("valpremiosSegVidaCSS", "Pr&eacute;mios de seguros de vida"));
        this.addRubrica(new Rubrica("valpremiosSegRiSaude", "Pr&eacute;mios de seguros de sa&uacute;de"));
        this.addRubrica(new Rubrica("valdespesasSaude5", "Despesas de sa&uacute;de"));
        this.addRubrica(new Rubrica("valdespAquEnergRenov", "Energias renov&aacute;veis"));
        this.addRubrica(new Rubrica("valdespesasSaudeOut", "Despesas de sa&uacute;de - outros"));
        this.addRubrica(new Rubrica("valaquisEquipCompUlt", "Equipamentos complementares"));
        this.addRubrica(new Rubrica("valdespesasEducSPDnD", "Despesas de educa&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valdonativosEntPubli", "Donativos a entidades p&uacute;blicas"));
        this.addRubrica(new Rubrica("valencargosLares", "Encargos com lares"));
        this.addRubrica(new Rubrica("valdonativosOutEntid", "Donativos a outras entidades"));
        this.addRubrica(new Rubrica("valjurosDiviImovHabi", "Juros e amortiza&ccedil;&otilde;es por aquisi&ccedil;&atilde;o de im&oacute;veis"));
        this.addRubrica(new Rubrica("valaconselhaJuridico", "Aconselhamento jur&iacute;dico"));
        this.addRubrica(new Rubrica("valdonativosLibRelig", "Donativos Lei liberdade religiosa"));
        this.addRubrica(new Rubrica("valdonativosIgrCatolica", "Donativos &agrave; Igreja Cat&oacutelica"));
        this.addRubrica(new Rubrica("valReabImvArr", "Encargos com reabilita&ccedil;&atilde;o urbana"));
        this.addRubrica(new Rubrica("valRendasLocFinan", "Rendas por contrato de loca&ccedil;&atilde;o financeira"));
        this.addRubrica(new Rubrica("valContribReformaDef", "Contribui&ccedil;&otilde;es pagas para reforma por velhice - deficientes"));
        this.addRubrica(new Rubrica("valdespesasEducSP", "Despesas de educa&ccedil;&atilde;o do(s) Sujeito(s) Passivo(s)"));
        this.addRubrica(new Rubrica("NumDepDespEduc", "N&uacute;mero de dependentes com despesas de educa&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valdespesasEducDep", "Despesas de educa&ccedil;&atilde;o dos dependentes"));
        this.addRubrica(new Rubrica("NumDepGCDespEduc", "N&uacute;mero de dependentes em guarda conjunta com despedas de educa&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valdespesasEducDepGC", "Despesas de educa&ccedil;&atilde;o dos dependentes em guarda conjunta"));
        this.addRubrica(new Rubrica("valdespesasSaude5SP", "Despesas de sa&uacute;de do(s) Sujeito(s) Passivo(s)"));
        this.addRubrica(new Rubrica("NumDepDespSaude5", "N&uacute;mero de dependentes com despesas de sa&uacute;de"));
        this.addRubrica(new Rubrica("valdespesasSaude5Dep", "Despesas de sa&uacute;de dos dependentes"));
        this.addRubrica(new Rubrica("NumDepGCDespSaude5", "N&uacute;mero de dependentes em guarda conjunta com despedas de sa&uacute;de"));
        this.addRubrica(new Rubrica("valdespesasSaude5GC", "Despesas de sa&uacute;de dos dependentes em guarda conjunta"));
        this.addRubrica(new Rubrica("valdespesasSaudeOutSP", "Despesas de sa&uacute;de - outros - do(s) Sujeito(s) Passivo(s)"));
        this.addRubrica(new Rubrica("NumDepDespSaudeOut", "PN&uacute;mero de dependentes com despesas de sa&uacute;de - outros"));
        this.addRubrica(new Rubrica("valdespesasSaudeOutDep", "Despesas de sa&uacute;de - outros - dos dependentes"));
        this.addRubrica(new Rubrica("NumDepGCDespSaudeOut", "N&uacute;mero de dependentes em guarda conjunta com de sa&uacute;de - outros"));
        this.addRubrica(new Rubrica("valdespesasSaudeOutDepGC", "Despesas de sa&uacute;de - outros - dos dependentes em guarda conjunta"));
        this.addRubrica(new Rubrica("valencargosLaresGC", "Encargos com lares - dependentes em guarda conjunta"));
        this.addRubrica(new Rubrica("valencargosLaresOut", "Encargos com lares - resto do agregado familiar"));
        this.addRubrica(new Rubrica("valReabImvArrGC", "Encargos com reabilita&ccedil;&atilde;o urbana - dependentes guarda conjunta"));
        this.addRubrica(new Rubrica("valReabImvArrOut", "Encargos com reabilita&ccedil;&atilde;o urbana - resto do agregado familiar"));
        this.addRubrica(new Rubrica("valpremiosSegRiSaudeGC", "Pr&eacute;mios de seguros de sa&uacute;de - dependentes guarda conjunta"));
        this.addRubrica(new Rubrica("valpremiosSegRiSaudeOut", "Pr&eacute;mios de seguros de sa&uacute;de - resto do agregado familiar"));
        this.addRubrica(new Rubrica("numDepSegRiSaudeGC", "Pr&eacute;mios de seguros de sa&uacute;de - n\u00ba dependentes guarda conjunta"));
        this.addRubrica(new Rubrica("numDepSegRiSaudeOut", "Pr&eacute;mios de seguros de sa&uacute;de - n\u00ba outros dependentes"));
    }
}

