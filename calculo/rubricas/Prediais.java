/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Rubrica;

public class Prediais
extends Rubricas {
    private final int nPrediais = 19;

    public Prediais(String id) {
        this.inicializa(19);
        this.setID(id);
        this.addRubrica(new Rubrica("valtotalRendas", "Total de rendas"));
        this.addRubrica(new Rubrica("valdespManutencao", "Manuten&ccedil;&atilde;o pr&eacute;dios urbanos"));
        this.addRubrica(new Rubrica("valdespConservacao", "Conserva&ccedil;&atilde;o pr&eacute;dios urbanos"));
        this.addRubrica(new Rubrica("valtaxasAutarq", "Taxas aut&aacute;rquicas"));
        this.addRubrica(new Rubrica("valcontribAutarq", "Contribui&ccedil;&atilde;o aut&aacute;rquica"));
        this.addRubrica(new Rubrica("valdespCondominio", "Despesas de condom&iacute;nio"));
        this.addRubrica(new Rubrica("valretFonteF", "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valperdasAnosAntF", "Perdas de anos anteriores"));
        this.addRubrica(new Rubrica("valrendRecebSub", "Renda recebida pelo sublocador"));
        this.addRubrica(new Rubrica("valrendPagaSenhor", "Renda paga ao senhorio da parte sublocada"));
        this.addRubrica(new Rubrica("valtotalRendasImv", "Total de rendas"));
        this.addRubrica(new Rubrica("valdespManutencaoImv", "Manuten&ccedil;&atilde;o pr&eacute;dios urbanos"));
        this.addRubrica(new Rubrica("valdespConservacaoImv", "Conserva&ccedil;&atilde;o pr&eacute;dios urbanos"));
        this.addRubrica(new Rubrica("valtaxasAutarqImv", "Taxas aut&aacute;rquicas"));
        this.addRubrica(new Rubrica("valcontribAutarqImv", "Contribui&ccedil;&atilde;o aut&aacute;rquica"));
        this.addRubrica(new Rubrica("valdespCondominioImv", "Despesas de condom&iacute;nio"));
        this.addRubrica(new Rubrica("valretFonteFImv", "Reten&ccedil;&otilde;es na fonte"));
        this.addRubrica(new Rubrica("valrendAnosAntcatF", "Rendimentos de anos anteriores"));
        this.addRubrica(new Rubrica("valnAnoscatF", "N&uacute;mero de anos"));
    }
}

