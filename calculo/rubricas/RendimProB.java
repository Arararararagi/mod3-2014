/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;

public class RendimProB
extends Rubricas {
    private final int nRendimProB = 75;

    public RendimProB(String id, Contribuinte[] Contrib) {
        this.inicializa(75);
        this.setID(id);
        this.addRubrica(new Rubrica("valVendasMercadoP", Contrib[0], "Vendas de mercadorias e produtos"));
        this.addRubrica(new Rubrica("valServicosHotP", Contrib[0], "Presta&ccedil;&otilde;es servi&ccedil;os de act. hoteleiras"));
        this.addRubrica(new Rubrica("valOutrosServicosP", Contrib[0], "Outras presta&ccedil;&otilde;es de servi&ccedil;os"));
        this.addRubrica(new Rubrica("valRendimCIRSP", Contrib[0], "Rendimento de actividades profissionais"));
        this.addRubrica(new Rubrica("valPropIndustrialP", Contrib[0], "Propriedade intelectual, industrial ou de presta&ccedil;&atilde;o de informa&ccedil;&otilde;es"));
        this.addRubrica(new Rubrica("valSaldoPositivoP", Contrib[0], "Saldo positivo das mais e menos-valias e restantes incrementos patrimoniais"));
        this.addRubrica(new Rubrica("valRendimActFinP", Contrib[0], "Rendimentos de actividades financeiras"));
        this.addRubrica(new Rubrica("valServPrestSocP", Contrib[0], "Servi&ccedil;os prestados por s\u00f3cios a sociedades de transpar\u00eancia fiscal"));
        this.addRubrica(new Rubrica("valPropriedadeIntP", Contrib[0], "Propriedade intelectual"));
        this.addRubrica(new Rubrica("valRendPredImp", Contrib[0], "Rendimentos prediais imput&aacute;veis a actividade geradora de rendimentos da categoria B"));
        this.addRubrica(new Rubrica("valRendCapImp", Contrib[0], "Rendimentos de capitais imput&aacute;veis aactividade geradora de rendimentos da  categoria B"));
        this.addRubrica(new Rubrica("valMicroEletr", Contrib[0], "Microprodu&ccedil;&atilde;o de eletricidade"));
        this.addRubrica(new Rubrica("valSubsidiosExplP", Contrib[0], "Subs&iacute;dios &agrave; explora&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valOutrosSubsidiosP", Contrib[0], "Outros Subs&iacute;dios"));
        this.addRubrica(new Rubrica("valOutrosCatBP", Contrib[0], "Rendimentos da Categoria B n&atilde;o inclu&iacute;dos nos campos anteriores"));
        this.addRubrica(new Rubrica("valVendasMercadoP", Contrib[1], "Vendas de mercadorias e produtos"));
        this.addRubrica(new Rubrica("valServicosHotP", Contrib[1], "Presta&ccedil;&otilde;es servi&ccedil;os de act. hoteleiras"));
        this.addRubrica(new Rubrica("valOutrosServicosP", Contrib[1], "Outras presta&ccedil;&otilde;es de servi&ccedil;os"));
        this.addRubrica(new Rubrica("valRendimCIRSP", Contrib[1], "Rendimento de actividades profissionais"));
        this.addRubrica(new Rubrica("valPropIndustrialP", Contrib[1], "Propriedade intelectual, industrial ou de presta&ccedil;&atilde;o de informa&ccedil;&otilde;es"));
        this.addRubrica(new Rubrica("valSaldoPositivoP", Contrib[1], "Saldo positivo das mais e menos-valias e restantes incrementos patrimoniais"));
        this.addRubrica(new Rubrica("valRendimActFinP", Contrib[1], "Rendimentos de actividades financeiras"));
        this.addRubrica(new Rubrica("valServPrestSocP", Contrib[1], "Servi&ccedil;os prestados por s\u00f3cios a sociedades de transpar\u00eancia fiscal"));
        this.addRubrica(new Rubrica("valPropriedadeIntP", Contrib[1], "Propriedade intelectual"));
        this.addRubrica(new Rubrica("valRendPredImp", Contrib[1], "Rendimentos prediais imput&aacute;veis a actividade geradora de rendimentos da categoria B"));
        this.addRubrica(new Rubrica("valRendCapImp", Contrib[1], "Rendimentos de capitais imput&aacute;veis aactividade geradora de rendimentos da  categoria B"));
        this.addRubrica(new Rubrica("valMicroEletr", Contrib[1], "Microprodu&ccedil;&atilde;o de eletricidade"));
        this.addRubrica(new Rubrica("valSubsidiosExplP", Contrib[1], "Subs&iacute;dios &agrave; explora&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valOutrosSubsidiosP", Contrib[1], "Outros Subs&iacute;dios"));
        this.addRubrica(new Rubrica("valOutrosCatBP", Contrib[1], "Rendimentos da Categoria B n&atilde;o inclu&iacute;dos nos campos anteriores"));
        this.addRubrica(new Rubrica("valVendasMercadoP", Contrib[2], "Vendas de mercadorias e produtos"));
        this.addRubrica(new Rubrica("valServicosHotP", Contrib[2], "Presta&ccedil;&otilde;es servi&ccedil;os de act. hoteleiras"));
        this.addRubrica(new Rubrica("valOutrosServicosP", Contrib[2], "Outras presta&ccedil;&otilde;es de servi&ccedil;os"));
        this.addRubrica(new Rubrica("valRendimCIRSP", Contrib[2], "Rendimento de actividades profissionais"));
        this.addRubrica(new Rubrica("valPropIndustrialP", Contrib[2], "Propriedade intelectual, industrial ou de presta&ccedil;&atilde;o de informa&ccedil;&otilde;es"));
        this.addRubrica(new Rubrica("valSaldoPositivoP", Contrib[2], "Saldo positivo das mais e menos-valias e restantes incrementos patrimoniais"));
        this.addRubrica(new Rubrica("valRendimActFinP", Contrib[2], "Rendimentos de actividades financeiras"));
        this.addRubrica(new Rubrica("valServPrestSocP", Contrib[2], "Servi&ccedil;os prestados por s\u00f3cios a sociedades de transpar\u00eancia fiscal"));
        this.addRubrica(new Rubrica("valPropriedadeIntP", Contrib[2], "Propriedade intelectual"));
        this.addRubrica(new Rubrica("valRendPredImp", Contrib[2], "Rendimentos prediais imput&aacute;veis a actividade geradora de rendimentos da categoria B"));
        this.addRubrica(new Rubrica("valRendCapImp", Contrib[2], "Rendimentos de capitais imput&aacute;veis aactividade geradora de rendimentos da  categoria B"));
        this.addRubrica(new Rubrica("valMicroEletr", Contrib[2], "Microprodu&ccedil;&atilde;o de eletricidade"));
        this.addRubrica(new Rubrica("valSubsidiosExplP", Contrib[2], "Subs&iacute;dios &agrave; explora&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valOutrosSubsidiosP", Contrib[2], "Outros Subs&iacute;dios"));
        this.addRubrica(new Rubrica("valOutrosCatBP", Contrib[2], "Rendimentos da Categoria B n&atilde;o inclu&iacute;dos nos campos anteriores"));
        this.addRubrica(new Rubrica("valVendasMercadoP", Contrib[3], "Vendas de mercadorias e produtos"));
        this.addRubrica(new Rubrica("valServicosHotP", Contrib[3], "Presta&ccedil;&otilde;es servi&ccedil;os de act. hoteleiras"));
        this.addRubrica(new Rubrica("valOutrosServicosP", Contrib[3], "Outras presta&ccedil;&otilde;es de servi&ccedil;os"));
        this.addRubrica(new Rubrica("valRendimCIRSP", Contrib[3], "Rendimento de actividades profissionais"));
        this.addRubrica(new Rubrica("valPropIndustrialP", Contrib[3], "Propriedade intelectual, industrial ou de presta&ccedil;&atilde;o de informa&ccedil;&otilde;es"));
        this.addRubrica(new Rubrica("valSaldoPositivoP", Contrib[3], "Saldo positivo das mais e menos-valias e restantes incrementos patrimoniais"));
        this.addRubrica(new Rubrica("valRendimActFinP", Contrib[3], "Rendimentos de actividades financeiras"));
        this.addRubrica(new Rubrica("valServPrestSocP", Contrib[3], "Servi&ccedil;os prestados por s\u00f3cios a sociedades de transpar\u00eancia fiscal"));
        this.addRubrica(new Rubrica("valPropriedadeIntP", Contrib[3], "Propriedade intelectual"));
        this.addRubrica(new Rubrica("valRendPredImp", Contrib[3], "Rendimentos prediais imput&aacute;veis a actividade geradora de rendimentos da categoria B"));
        this.addRubrica(new Rubrica("valRendCapImp", Contrib[3], "Rendimentos de capitais imput&aacute;veis aactividade geradora de rendimentos da  categoria B"));
        this.addRubrica(new Rubrica("valMicroEletr", Contrib[3], "Microprodu&ccedil;&atilde;o de eletricidade"));
        this.addRubrica(new Rubrica("valSubsidiosExplP", Contrib[3], "Subs&iacute;dios &agrave; explora&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valOutrosSubsidiosP", Contrib[3], "Outros Subs&iacute;dios"));
        this.addRubrica(new Rubrica("valOutrosCatBP", Contrib[3], "Rendimentos da Categoria B n&atilde;o inclu&iacute;dos nos campos anteriores"));
        this.addRubrica(new Rubrica("valVendasMercadoP", Contrib[4], "Vendas de mercadorias e produtos"));
        this.addRubrica(new Rubrica("valServicosHotP", Contrib[4], "Presta&ccedil;&otilde;es servi&ccedil;os de act. hoteleiras"));
        this.addRubrica(new Rubrica("valOutrosServicosP", Contrib[4], "Outras presta&ccedil;&otilde;es de servi&ccedil;os"));
        this.addRubrica(new Rubrica("valRendimCIRSP", Contrib[4], "Rendimento de actividades profissionais"));
        this.addRubrica(new Rubrica("valPropIndustrialP", Contrib[4], "Propriedade intelectual, industrial ou de presta&ccedil;&atilde;o de informa&ccedil;&otilde;es"));
        this.addRubrica(new Rubrica("valSaldoPositivoP", Contrib[4], "Saldo positivo das mais e menos-valias e restantes incrementos patrimoniais"));
        this.addRubrica(new Rubrica("valRendimActFinP", Contrib[4], "Rendimentos de actividades financeiras"));
        this.addRubrica(new Rubrica("valServPrestSocP", Contrib[4], "Servi&ccedil;os prestados por s\u00f3cios a sociedades de transpar\u00eancia fiscal"));
        this.addRubrica(new Rubrica("valPropriedadeIntP", Contrib[4], "Propriedade intelectual"));
        this.addRubrica(new Rubrica("valRendPredImp", Contrib[4], "Rendimentos prediais imput&aacute;veis a actividade geradora de rendimentos da categoria B"));
        this.addRubrica(new Rubrica("valRendCapImp", Contrib[4], "Rendimentos de capitais imput&aacute;veis aactividade geradora de rendimentos da  categoria B"));
        this.addRubrica(new Rubrica("valMicroEletr", Contrib[4], "Microprodu&ccedil;&atilde;o de eletricidade"));
        this.addRubrica(new Rubrica("valSubsidiosExplP", Contrib[4], "Subs&iacute;dios &agrave; explora&ccedil;&atilde;o"));
        this.addRubrica(new Rubrica("valOutrosSubsidiosP", Contrib[4], "Outros Subs&iacute;dios"));
        this.addRubrica(new Rubrica("valOutrosCatBP", Contrib[4], "Rendimentos da Categoria B n&atilde;o inclu&iacute;dos nos campos anteriores"));
    }
}

