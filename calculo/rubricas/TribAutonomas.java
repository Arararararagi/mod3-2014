/*
 * Decompiled with CFR 0_102.
 */
package calculo.rubricas;

import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.Rubrica;

public class TribAutonomas
extends Rubricas {
    private final int nTribAutonomas = 35;

    public TribAutonomas(String id, Contribuinte[] Contrib) {
        this.inicializa(35);
        this.setID(id);
        this.addRubrica(new Rubrica("valDespesaTA1", Contrib[0], "Despesas confidenciais ou n\u00e3o documentadas"));
        this.addRubrica(new Rubrica("valDespesaTA2", Contrib[0], "Despesas de representa\u00e7\u00e3o"));
        this.addRubrica(new Rubrica("valDespesaTA3", Contrib[0], "Encargos com viaturas ligeiras de passageiros ou mistas"));
        this.addRubrica(new Rubrica("valEncargAquisMenorTA", Contrib[0], "Despesas de representa&ccedil;&atilde;o e encargos com viaturas ligeiras com custo de aquisi&ccedil;&atilde;o inferior a 20.000&#8364;"));
        this.addRubrica(new Rubrica("valEncargAquisMaiorTA", Contrib[0], "Encargos com viaturas ligeiras com custo de aquisi&ccedil;&atilde;o igual ou superior a 20.000&#8364;"));
        this.addRubrica(new Rubrica("valDespesaTA4", Contrib[0], "Import\u00e2ncias pagas ou devidas, a n\u00e3o residentes"));
        this.addRubrica(new Rubrica("valDespesaTA5", Contrib[0], "Ajudas de custo pela desloca\u00e7\u00e3o em viatura pr\u00f3pria"));
        this.addRubrica(new Rubrica("valDespesaTA1", Contrib[1], "Despesas confidenciais ou n\u00e3o documentadas"));
        this.addRubrica(new Rubrica("valDespesaTA2", Contrib[1], "Despesas de representa\u00e7\u00e3o"));
        this.addRubrica(new Rubrica("valDespesaTA3", Contrib[1], "Encargos com viaturas ligeiras de passageiros ou mistas"));
        this.addRubrica(new Rubrica("valEncargAquisMenorTA", Contrib[1], "Despesas de representa&ccedil;&atilde;o e encargos com viaturas ligeiras com custo de aquisi&ccedil;&atilde;o inferior a 20.000&#8364;"));
        this.addRubrica(new Rubrica("valEncargAquisMaiorTA", Contrib[1], "Encargos com viaturas ligeiras com custo de aquisi&ccedil;&atilde;o igual ou superior a 20.000&#8364;"));
        this.addRubrica(new Rubrica("valDespesaTA4", Contrib[1], "Import\u00e2ncias pagas ou devidas, a n\u00e3o residentes"));
        this.addRubrica(new Rubrica("valDespesaTA5", Contrib[1], "Ajudas de custo pela desloca\u00e7\u00e3o em viatura pr\u00f3pria"));
        this.addRubrica(new Rubrica("valDespesaTA1", Contrib[2], "Despesas confidenciais ou n\u00e3o documentadas"));
        this.addRubrica(new Rubrica("valDespesaTA2", Contrib[2], "Despesas de representa\u00e7\u00e3o"));
        this.addRubrica(new Rubrica("valDespesaTA3", Contrib[2], "Encargos com viaturas ligeiras de passageiros ou mistas"));
        this.addRubrica(new Rubrica("valEncargAquisMenorTA", Contrib[2], "Despesas de representa&ccedil;&atilde;o e encargos com viaturas ligeiras com custo de aquisi&ccedil;&atilde;o inferior a 20.000&#8364;"));
        this.addRubrica(new Rubrica("valEncargAquisMaiorTA", Contrib[2], "Encargos com viaturas ligeiras com custo de aquisi&ccedil;&atilde;o igual ou superior a 20.000&#8364;"));
        this.addRubrica(new Rubrica("valDespesaTA4", Contrib[2], "Import\u00e2ncias pagas ou devidas, a n\u00e3o residentes"));
        this.addRubrica(new Rubrica("valDespesaTA5", Contrib[2], "Ajudas de custo pela desloca\u00e7\u00e3o em viatura pr\u00f3pria"));
        this.addRubrica(new Rubrica("valDespesaTA1", Contrib[3], "Despesas confidenciais ou n\u00e3o documentadas"));
        this.addRubrica(new Rubrica("valDespesaTA2", Contrib[3], "Despesas de representa\u00e7\u00e3o"));
        this.addRubrica(new Rubrica("valDespesaTA3", Contrib[3], "Encargos com viaturas ligeiras de passageiros ou mistas"));
        this.addRubrica(new Rubrica("valEncargAquisMenorTA", Contrib[3], "Despesas de representa&ccedil;&atilde;o e encargos com viaturas ligeiras com custo de aquisi&ccedil;&atilde;o inferior a 20.000&#8364;"));
        this.addRubrica(new Rubrica("valEncargAquisMaiorTA", Contrib[3], "Encargos com viaturas ligeiras com custo de aquisi&ccedil;&atilde;o igual ou superior a 20.000&#8364;"));
        this.addRubrica(new Rubrica("valDespesaTA4", Contrib[3], "Import\u00e2ncias pagas ou devidas, a n\u00e3o residentes"));
        this.addRubrica(new Rubrica("valDespesaTA5", Contrib[3], "Ajudas de custo pela desloca\u00e7\u00e3o em viatura pr\u00f3pria"));
        this.addRubrica(new Rubrica("valDespesaTA1", Contrib[4], "Despesas confidenciais ou n\u00e3o documentadas"));
        this.addRubrica(new Rubrica("valDespesaTA2", Contrib[4], "Despesas de representa\u00e7\u00e3o"));
        this.addRubrica(new Rubrica("valDespesaTA3", Contrib[4], "Encargos com viaturas ligeiras de passageiros ou mistas"));
        this.addRubrica(new Rubrica("valEncargAquisMenorTA", Contrib[4], "Despesas de representa&ccedil;&atilde;o e encargos com viaturas ligeiras com custo de aquisi&ccedil;&atilde;o inferior a 20.000&#8364;"));
        this.addRubrica(new Rubrica("valEncargAquisMaiorTA", Contrib[4], "Encargos com viaturas ligeiras com custo de aquisi&ccedil;&atilde;o igual ou superior a 20.000&#8364;"));
        this.addRubrica(new Rubrica("valDespesaTA4", Contrib[4], "Import\u00e2ncias pagas ou devidas, a n\u00e3o residentes"));
        this.addRubrica(new Rubrica("valDespesaTA5", Contrib[4], "Ajudas de custo pela desloca\u00e7\u00e3o em viatura pr\u00f3pria"));
    }
}

