/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.TransformedList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventAssembler;
import ca.odell.glazedlists.event.ListEventListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class FunctionList<S, E>
extends TransformedList<S, E>
implements RandomAccess {
    private final List<S> sourceElements;
    private final List<E> mappedElements;
    private AdvancedFunction<S, E> forward;
    private Function<E, S> reverse;

    public FunctionList(EventList<S> source, Function<S, E> forward) {
        this(source, forward, null);
    }

    public FunctionList(EventList<S> source, Function<S, E> forward, Function<E, S> reverse) {
        super(source);
        this.updateForwardFunction(forward);
        this.setReverseFunction(reverse);
        this.sourceElements = new ArrayList<S>(source);
        this.mappedElements = new ArrayList(source.size());
        int n = source.size();
        for (int i = 0; i < n; ++i) {
            this.mappedElements.add(this.forward(source.get(i)));
        }
        source.addListEventListener(this);
    }

    private E forward(S s) {
        return this.forward.evaluate(s);
    }

    private E forward(E e, S s) {
        return this.forward.reevaluate(s, e);
    }

    private S reverse(E e) {
        if (this.reverse == null) {
            throw new IllegalStateException("A reverse mapping function must be specified to support this List operation");
        }
        return this.reverse.evaluate(e);
    }

    public void setForwardFunction(Function<S, E> forward) {
        this.updateForwardFunction(forward);
        this.updates.beginEvent(true);
        int n = this.source.size();
        for (int i = 0; i < n; ++i) {
            E oldValue = this.mappedElements.set(i, this.forward(this.source.get(i)));
            this.updates.elementUpdated(i, oldValue);
        }
        this.updates.commitEvent();
    }

    private void updateForwardFunction(Function<S, E> forward) {
        if (forward == null) {
            throw new IllegalArgumentException("forward Function may not be null");
        }
        this.forward = forward instanceof AdvancedFunction ? (AdvancedFunction)forward : new AdvancedFunctionAdapter<S, E>(forward);
    }

    public Function<S, E> getForwardFunction() {
        if (this.forward instanceof AdvancedFunctionAdapter) {
            return ((AdvancedFunctionAdapter)this.forward).getDelegate();
        }
        return this.forward;
    }

    public void setReverseFunction(Function<E, S> reverse) {
        this.reverse = reverse;
    }

    public Function<E, S> getReverseFunction() {
        return this.reverse;
    }

    @Override
    protected boolean isWritable() {
        return true;
    }

    @Override
    public void listChanged(ListEvent<S> listChanges) {
        this.updates.beginEvent(true);
        if (listChanges.isReordering()) {
            int[] reorderMap = listChanges.getReorderMap();
            ArrayList<E> originalMappedElements = new ArrayList<E>(this.mappedElements);
            for (int i = 0; i < reorderMap.length; ++i) {
                int sourceIndex = reorderMap[i];
                this.mappedElements.set(i, originalMappedElements.get(sourceIndex));
            }
            this.updates.reorder(reorderMap);
        } else {
            while (listChanges.next()) {
                int changeIndex = listChanges.getIndex();
                int changeType = listChanges.getType();
                if (changeType == 2) {
                    Object newValue = this.source.get(changeIndex);
                    E newValueTransformed = this.forward(newValue);
                    this.sourceElements.add(changeIndex, newValue);
                    this.mappedElements.add(changeIndex, newValueTransformed);
                    this.updates.elementInserted(changeIndex, newValueTransformed);
                    continue;
                }
                if (changeType == 1) {
                    E oldValueTransformed = this.get(changeIndex);
                    Object newValue = this.source.get(changeIndex);
                    E newValueTransformed = this.forward(oldValueTransformed, newValue);
                    this.sourceElements.set(changeIndex, newValue);
                    this.mappedElements.set(changeIndex, newValueTransformed);
                    this.updates.elementUpdated(changeIndex, oldValueTransformed, newValueTransformed);
                    continue;
                }
                if (changeType != 0) continue;
                S oldValue = this.sourceElements.remove(changeIndex);
                E oldValueTransformed = this.mappedElements.remove(changeIndex);
                this.forward.dispose(oldValue, oldValueTransformed);
                this.updates.elementDeleted(changeIndex, oldValueTransformed);
            }
        }
        this.updates.commitEvent();
    }

    @Override
    public E get(int index) {
        return this.mappedElements.get(index);
    }

    @Override
    public E remove(int index) {
        E removed = this.get(index);
        this.source.remove(index);
        return removed;
    }

    @Override
    public E set(int index, E value) {
        E updated = this.get(index);
        this.source.set(index, this.reverse(value));
        return updated;
    }

    @Override
    public void add(int index, E value) {
        this.source.add(index, this.reverse(value));
    }

    public static interface AdvancedFunction<A, B>
    extends Function<A, B> {
        public B reevaluate(A var1, B var2);

        public void dispose(A var1, B var2);
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private static final class AdvancedFunctionAdapter<A, B>
    implements AdvancedFunction<A, B> {
        private final Function<A, B> delegate;

        public AdvancedFunctionAdapter(Function<A, B> delegate) {
            this.delegate = delegate;
        }

        @Override
        public B evaluate(A sourceValue) {
            return this.delegate.evaluate(sourceValue);
        }

        @Override
        public B reevaluate(A sourceValue, B transformedValue) {
            return this.evaluate(sourceValue);
        }

        @Override
        public void dispose(A sourceValue, B transformedValue) {
        }

        public Function getDelegate() {
            return this.delegate;
        }
    }

    public static interface Function<A, B> {
        public B evaluate(A var1);
    }

}

