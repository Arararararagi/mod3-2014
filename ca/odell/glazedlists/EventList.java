/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists;

import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.event.ListEventPublisher;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;
import java.util.List;

public interface EventList<E>
extends List<E> {
    public void addListEventListener(ListEventListener<? super E> var1);

    public void removeListEventListener(ListEventListener<? super E> var1);

    public ReadWriteLock getReadWriteLock();

    public ListEventPublisher getPublisher();

    public void dispose();
}

