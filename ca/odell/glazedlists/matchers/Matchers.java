/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.matchers;

import ca.odell.glazedlists.Filterator;
import ca.odell.glazedlists.impl.matchers.AndMatcher;
import ca.odell.glazedlists.impl.matchers.BeanPropertyMatcher;
import ca.odell.glazedlists.impl.matchers.FalseMatcher;
import ca.odell.glazedlists.impl.matchers.NonNullAndNonEmptyStringMatcher;
import ca.odell.glazedlists.impl.matchers.NotMatcher;
import ca.odell.glazedlists.impl.matchers.NotNullMatcher;
import ca.odell.glazedlists.impl.matchers.NullMatcher;
import ca.odell.glazedlists.impl.matchers.OrMatcher;
import ca.odell.glazedlists.impl.matchers.PropertyEventNameMatcher;
import ca.odell.glazedlists.impl.matchers.RangeMatcher;
import ca.odell.glazedlists.impl.matchers.TrueMatcher;
import ca.odell.glazedlists.impl.matchers.TypeMatcher;
import ca.odell.glazedlists.impl.matchers.WeakReferenceMatcherEditor;
import ca.odell.glazedlists.matchers.Matcher;
import ca.odell.glazedlists.matchers.MatcherEditor;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class Matchers {
    private Matchers() {
        throw new UnsupportedOperationException();
    }

    public static <E> MatcherEditor<E> weakReferenceProxy(MatcherEditor<E> matcherEditor) {
        return new WeakReferenceMatcherEditor<E>(matcherEditor);
    }

    public static <E> Matcher<E> trueMatcher() {
        return TrueMatcher.getInstance();
    }

    public static <E> Matcher<E> falseMatcher() {
        return FalseMatcher.getInstance();
    }

    public static <E> Matcher<E> invert(Matcher<E> original) {
        return new NotMatcher<E>(original);
    }

    public static <E> Matcher<E> isNull() {
        return NullMatcher.getInstance();
    }

    public static <E> Matcher<E> isNotNull() {
        return NotNullMatcher.getInstance();
    }

    public static Matcher<String> nonNullAndNonEmptyString() {
        return NonNullAndNonEmptyStringMatcher.getInstance();
    }

    public static <E> Matcher<E> beanPropertyMatcher(Class<E> beanClass, String propertyName, Object expectedValue) {
        return new BeanPropertyMatcher<E>(beanClass, propertyName, expectedValue);
    }

    public static <D extends Comparable, E> Matcher<E> rangeMatcher(D start, D end) {
        return new RangeMatcher(start, end);
    }

    public static <D extends Comparable, E> Matcher<E> rangeMatcher(D start, D end, Filterator<D, E> filterator) {
        return new RangeMatcher<D, E>(start, end, filterator);
    }

    public static /* varargs */ Matcher<PropertyChangeEvent> propertyEventNameMatcher(boolean matchPropertyNames, String ... propertyNames) {
        return new PropertyEventNameMatcher(matchPropertyNames, propertyNames);
    }

    public static <E> int count(Collection<E> collection, Matcher<? super E> matcher) {
        int count = 0;
        Iterator<E> i = collection.iterator();
        while (i.hasNext()) {
            if (!matcher.matches(i.next())) continue;
            ++count;
        }
        return count;
    }

    public static <E> boolean filter(Collection<E> collection, Matcher<? super E> matcher) {
        boolean changed = false;
        Iterator<E> i = collection.iterator();
        while (i.hasNext()) {
            if (matcher.matches(i.next())) continue;
            i.remove();
            changed = true;
        }
        return changed;
    }

    public static <E> E[] select(E[] items, Matcher<? super E> matcher) {
        Collection<E> selections = Matchers.select(Arrays.asList(items), matcher);
        return selections.toArray((Object[])Array.newInstance(items.getClass().getComponentType(), selections.size()));
    }

    public static <E> Collection<? super E> select(Collection<E> collection, Matcher<? super E> matcher) {
        return Matchers.select(collection, matcher, new ArrayList());
    }

    public static <E> Collection<? super E> select(Collection<E> collection, Matcher<? super E> matcher, Collection<? super E> results) {
        for (E element : collection) {
            if (!matcher.matches(element)) continue;
            results.add(element);
        }
        return results;
    }

    public static <E> boolean contains(Collection<E> collection, Matcher<? super E> matcher) {
        Iterator<E> i = collection.iterator();
        while (i.hasNext()) {
            if (!matcher.matches(i.next())) continue;
            return true;
        }
        return false;
    }

    public static <E> int indexOf(List<E> list, Matcher<? super E> matcher) {
        int n = list.size();
        for (int i = 0; i < n; ++i) {
            if (!matcher.matches(list.get(i))) continue;
            return i;
        }
        return -1;
    }

    public static /* varargs */ <E> Matcher<E> or(Matcher<? super E> ... matchers) {
        return new OrMatcher<E>(matchers);
    }

    public static /* varargs */ <E> Matcher<E> and(Matcher<? super E> ... matchers) {
        return new AndMatcher<E>(matchers);
    }

    public static /* varargs */ <E> Matcher<E> types(Class ... classes) {
        return new TypeMatcher(classes);
    }
}

