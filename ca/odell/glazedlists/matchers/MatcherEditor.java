/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.matchers;

import ca.odell.glazedlists.FilterList;
import ca.odell.glazedlists.matchers.Matcher;
import java.util.EventListener;
import java.util.EventObject;

public interface MatcherEditor<E> {
    public void addMatcherEditorListener(Listener<E> var1);

    public void removeMatcherEditorListener(Listener<E> var1);

    public Matcher<E> getMatcher();

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    public static class Event<E>
    extends EventObject {
        public static final int MATCH_ALL = 0;
        public static final int MATCH_NONE = 1;
        public static final int CONSTRAINED = 2;
        public static final int RELAXED = 3;
        public static final int CHANGED = 4;
        private MatcherEditor<E> matcherEditor;
        private final Matcher<E> matcher;
        private final int type;

        public Event(MatcherEditor<E> matcherEditor, int changeType, Matcher<E> matcher) {
            super(matcherEditor);
            this.matcherEditor = matcherEditor;
            this.type = changeType;
            this.matcher = matcher;
        }

        public Event(FilterList eventSource, int changeType, Matcher<E> matcher) {
            super(eventSource);
            this.type = changeType;
            this.matcher = matcher;
        }

        public MatcherEditor<E> getMatcherEditor() {
            return this.matcherEditor;
        }

        public Matcher<E> getMatcher() {
            return this.matcher;
        }

        public int getType() {
            return this.type;
        }
    }

    public static interface Listener<E>
    extends EventListener {
        public void changedMatcher(Event<E> var1);
    }

}

