/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.matchers;

public interface Matcher<E> {
    public boolean matches(E var1);
}

