/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.matchers;

import ca.odell.glazedlists.matchers.Matcher;
import ca.odell.glazedlists.matchers.MatcherEditor;
import ca.odell.glazedlists.matchers.Matchers;
import java.util.EventListener;
import javax.swing.event.EventListenerList;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public abstract class AbstractMatcherEditor<E>
implements MatcherEditor<E> {
    private EventListenerList listenerList = new EventListenerList();
    protected Matcher<E> currentMatcher = Matchers.trueMatcher();

    @Override
    public Matcher<E> getMatcher() {
        return this.currentMatcher;
    }

    @Override
    public final void addMatcherEditorListener(MatcherEditor.Listener<E> listener) {
        this.listenerList.add(MatcherEditor.Listener.class, listener);
    }

    @Override
    public final void removeMatcherEditorListener(MatcherEditor.Listener<E> listener) {
        this.listenerList.remove(MatcherEditor.Listener.class, listener);
    }

    protected final void fireMatchAll() {
        this.currentMatcher = Matchers.trueMatcher();
        this.fireChangedMatcher(new MatcherEditor.Event<E>(this, 0, this.currentMatcher));
    }

    protected final void fireChanged(Matcher<E> matcher) {
        if (matcher == null) {
            throw new NullPointerException();
        }
        this.currentMatcher = matcher;
        this.fireChangedMatcher(new MatcherEditor.Event<E>(this, 4, this.currentMatcher));
    }

    protected final void fireConstrained(Matcher<E> matcher) {
        if (matcher == null) {
            throw new NullPointerException();
        }
        this.currentMatcher = matcher;
        this.fireChangedMatcher(new MatcherEditor.Event<E>(this, 2, this.currentMatcher));
    }

    protected final void fireRelaxed(Matcher<E> matcher) {
        if (matcher == null) {
            throw new NullPointerException();
        }
        this.currentMatcher = matcher;
        this.fireChangedMatcher(new MatcherEditor.Event<E>(this, 3, this.currentMatcher));
    }

    protected final void fireMatchNone() {
        this.currentMatcher = Matchers.falseMatcher();
        this.fireChangedMatcher(new MatcherEditor.Event<E>(this, 1, this.currentMatcher));
    }

    protected final boolean isCurrentlyMatchingAll() {
        return this.currentMatcher == Matchers.trueMatcher();
    }

    protected final boolean isCurrentlyMatchingNone() {
        return this.currentMatcher == Matchers.falseMatcher();
    }

    protected final void fireChangedMatcher(MatcherEditor.Event<E> event) {
        Object[] listeners = this.listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i-=2) {
            ((MatcherEditor.Listener)listeners[i + 1]).changedMatcher(event);
        }
    }
}

