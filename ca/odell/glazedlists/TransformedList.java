/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists;

import ca.odell.glazedlists.AbstractEventList;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventAssembler;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.event.ListEventPublisher;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;
import java.util.Collection;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public abstract class TransformedList<S, E>
extends AbstractEventList<E>
implements ListEventListener<S> {
    protected EventList<S> source;

    protected TransformedList(EventList<S> source) {
        super(source.getPublisher());
        this.source = source;
        this.readWriteLock = source.getReadWriteLock();
    }

    protected int getSourceIndex(int mutationIndex) {
        return mutationIndex;
    }

    protected abstract boolean isWritable();

    @Override
    public abstract void listChanged(ListEvent<S> var1);

    @Override
    public void add(int index, E value) {
        if (!this.isWritable()) {
            throw new IllegalStateException("Non-writable List cannot be modified");
        }
        if (index < 0 || index > this.size()) {
            throw new IndexOutOfBoundsException("Cannot add at " + index + " on list of size " + this.size());
        }
        int sourceIndex = index < this.size() ? this.getSourceIndex(index) : this.source.size();
        this.source.add(sourceIndex, value);
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> values) {
        this.updates.beginEvent(true);
        try {
            boolean bl = super.addAll(index, values);
            return bl;
        }
        finally {
            this.updates.commitEvent();
        }
    }

    @Override
    public void clear() {
        this.updates.beginEvent(true);
        try {
            super.clear();
        }
        finally {
            this.updates.commitEvent();
        }
    }

    @Override
    public E get(int index) {
        if (index < 0 || index >= this.size()) {
            throw new IndexOutOfBoundsException("Cannot get at " + index + " on list of size " + this.size());
        }
        return (E)this.source.get(this.getSourceIndex(index));
    }

    @Override
    public E remove(int index) {
        if (!this.isWritable()) {
            throw new IllegalStateException("Non-writable List cannot be modified");
        }
        if (index < 0 || index >= this.size()) {
            throw new IndexOutOfBoundsException("Cannot remove at " + index + " on list of size " + this.size());
        }
        return (E)this.source.remove(this.getSourceIndex(index));
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        this.updates.beginEvent(true);
        try {
            boolean bl = super.removeAll(collection);
            return bl;
        }
        finally {
            this.updates.commitEvent();
        }
    }

    @Override
    public boolean retainAll(Collection<?> values) {
        this.updates.beginEvent(true);
        try {
            boolean bl = super.retainAll(values);
            return bl;
        }
        finally {
            this.updates.commitEvent();
        }
    }

    @Override
    public E set(int index, E value) {
        if (!this.isWritable()) {
            throw new IllegalStateException("List " + this.getClass().getName() + " cannot be modified in the current state");
        }
        if (index < 0 || index >= this.size()) {
            throw new IndexOutOfBoundsException("Cannot set at " + index + " on list of size " + this.size());
        }
        return (E)this.source.set(this.getSourceIndex(index), value);
    }

    @Override
    public int size() {
        return this.source.size();
    }

    @Override
    public void dispose() {
        this.source.removeListEventListener(this);
    }
}

