/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.swing;

import javax.swing.event.TableModelEvent;
import javax.swing.table.TableModel;

final class MutableTableModelEvent
extends TableModelEvent {
    public MutableTableModelEvent(TableModel source) {
        super(source);
    }

    public void setRange(int firstRow, int lastRow) {
        this.firstRow = firstRow;
        this.lastRow = lastRow;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setStructureChanged() {
        this.firstRow = -1;
        this.lastRow = -1;
        this.column = -1;
        this.type = 0;
    }

    public void setAllDataChanged() {
        this.firstRow = 0;
        this.lastRow = Integer.MAX_VALUE;
        this.column = -1;
        this.type = 0;
    }

    public void setValues(int startIndex, int endIndex, int listChangeType) {
        this.firstRow = startIndex;
        this.lastRow = endIndex;
        if (listChangeType == 2) {
            this.type = 1;
        } else if (listChangeType == 0) {
            this.type = -1;
        } else if (listChangeType == 1) {
            this.type = 0;
        }
        this.column = -1;
    }
}

