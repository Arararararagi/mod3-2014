/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.swing;

import javax.swing.event.ListDataEvent;

final class MutableListDataEvent
extends ListDataEvent {
    private int index0;
    private int index1;
    private int type;

    public MutableListDataEvent(Object source) {
        super(source, 0, 0, 0);
    }

    public void setRange(int index0, int index1) {
        this.index0 = index0;
        this.index1 = index1;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getIndex0() {
        return this.index0;
    }

    public int getIndex1() {
        return this.index1;
    }

    public int getType() {
        return this.type;
    }

    public String toString() {
        return "" + this.type + "[" + this.index0 + "," + this.index1 + "]";
    }
}

