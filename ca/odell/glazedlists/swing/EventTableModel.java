/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.swing;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.TransformedList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import ca.odell.glazedlists.swing.GlazedListsSwing;
import ca.odell.glazedlists.swing.MutableTableModelEvent;
import ca.odell.glazedlists.util.concurrent.Lock;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class EventTableModel<E>
extends AbstractTableModel
implements ListEventListener<E> {
    protected TransformedList<E, E> swingThreadSource;
    protected EventList<E> source;
    private TableFormat<? super E> tableFormat;
    private final MutableTableModelEvent tableModelEvent;

    public EventTableModel(EventList<E> source, TableFormat<? super E> tableFormat) {
        this.tableModelEvent = new MutableTableModelEvent(this);
        source.getReadWriteLock().readLock().lock();
        try {
            TransformedList<E, E> decorated = this.createSwingThreadProxyList(source);
            this.source = decorated != null && decorated != source ? (this.swingThreadSource = decorated) : source;
            this.tableFormat = tableFormat;
            this.source.addListEventListener(this);
        }
        finally {
            source.getReadWriteLock().readLock().unlock();
        }
    }

    public EventTableModel(EventList<E> source, String[] propertyNames, String[] columnLabels, boolean[] writable) {
        this(source, GlazedLists.tableFormat(propertyNames, columnLabels, writable));
    }

    protected TransformedList<E, E> createSwingThreadProxyList(EventList<E> source) {
        return GlazedListsSwing.isSwingThreadProxyList(source) ? null : GlazedListsSwing.swingThreadProxyList(source);
    }

    public TableFormat<? super E> getTableFormat() {
        return this.tableFormat;
    }

    public void setTableFormat(TableFormat<E> tableFormat) {
        this.tableFormat = tableFormat;
        this.tableModelEvent.setStructureChanged();
        this.fireTableChanged(this.tableModelEvent);
    }

    public E getElementAt(int index) {
        this.source.getReadWriteLock().readLock().lock();
        try {
            E e = this.source.get(index);
            return e;
        }
        finally {
            this.source.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public void listChanged(ListEvent<E> listChanges) {
        this.handleListChange(listChanges);
    }

    protected void handleListChange(ListEvent<E> listChanges) {
        while (listChanges.nextBlock()) {
            int startIndex = listChanges.getBlockStartIndex();
            int endIndex = listChanges.getBlockEndIndex();
            int changeType = listChanges.getType();
            this.tableModelEvent.setValues(startIndex, endIndex, changeType);
            this.fireTableChanged(this.tableModelEvent);
        }
    }

    protected final MutableTableModelEvent getMutableTableModelEvent() {
        return this.tableModelEvent;
    }

    @Override
    public String getColumnName(int column) {
        return this.tableFormat.getColumnName(column);
    }

    @Override
    public int getRowCount() {
        this.source.getReadWriteLock().readLock().lock();
        try {
            int n = this.source.size();
            return n;
        }
        finally {
            this.source.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public int getColumnCount() {
        return this.tableFormat.getColumnCount();
    }

    public Class getColumnClass(int columnIndex) {
        if (this.tableFormat instanceof AdvancedTableFormat) {
            return ((AdvancedTableFormat)this.tableFormat).getColumnClass(columnIndex);
        }
        return super.getColumnClass(columnIndex);
    }

    @Override
    public Object getValueAt(int row, int column) {
        this.source.getReadWriteLock().readLock().lock();
        try {
            Object object = this.tableFormat.getColumnValue(this.source.get(row), column);
            return object;
        }
        finally {
            this.source.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        if (!(this.tableFormat instanceof WritableTableFormat)) {
            return false;
        }
        this.source.getReadWriteLock().readLock().lock();
        try {
            E toEdit = this.source.get(row);
            boolean bl = ((WritableTableFormat)this.tableFormat).isEditable(toEdit, column);
            return bl;
        }
        finally {
            this.source.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public void setValueAt(Object editedValue, int row, int column) {
        if (!(this.tableFormat instanceof WritableTableFormat)) {
            throw new UnsupportedOperationException("Unexpected setValueAt() on read-only table");
        }
        this.source.getReadWriteLock().writeLock().lock();
        try {
            E baseObject = this.source.get(row);
            WritableTableFormat writableTableFormat = (WritableTableFormat)this.tableFormat;
            E updatedObject = writableTableFormat.setColumnValue(baseObject, editedValue, column);
            if (updatedObject != null) {
                boolean baseObjectHasNotMoved;
                boolean bl = baseObjectHasNotMoved = row < this.getRowCount() && this.source.get(row) == baseObject;
                if (baseObjectHasNotMoved) {
                    this.source.set(row, updatedObject);
                }
            }
        }
        finally {
            this.source.getReadWriteLock().writeLock().unlock();
        }
    }

    public void dispose() {
        this.source.removeListEventListener(this);
        if (this.swingThreadSource != null) {
            this.swingThreadSource.dispose();
        }
        this.swingThreadSource = null;
        this.source = null;
    }
}

