/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.swing;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.swing.EventListModel;
import ca.odell.glazedlists.swing.MutableListDataEvent;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataEvent;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class EventComboBoxModel<E>
extends EventListModel<E>
implements ComboBoxModel {
    private Object selected;

    public EventComboBoxModel(EventList<E> source) {
        super(source);
    }

    @Override
    public Object getSelectedItem() {
        return this.selected;
    }

    @Override
    public void setSelectedItem(Object selected) {
        if (this.selected == selected) {
            return;
        }
        this.selected = selected;
        this.listDataEvent.setRange(-1, -1);
        this.listDataEvent.setType(0);
        this.fireListDataEvent(this.listDataEvent);
    }
}

