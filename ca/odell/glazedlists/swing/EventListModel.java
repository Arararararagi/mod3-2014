/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.swing;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.TransformedList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.swing.GlazedListsSwing;
import ca.odell.glazedlists.swing.MutableListDataEvent;
import ca.odell.glazedlists.util.concurrent.Lock;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class EventListModel<E>
implements ListEventListener<E>,
ListModel {
    private TransformedList<E, E> swingSource;
    private final List<ListDataListener> listeners = new ArrayList<ListDataListener>();
    protected final MutableListDataEvent listDataEvent;

    public EventListModel(EventList<E> source) {
        this.listDataEvent = new MutableListDataEvent(this);
        source.getReadWriteLock().readLock().lock();
        try {
            this.swingSource = GlazedListsSwing.swingThreadProxyList(source);
            this.swingSource.addListEventListener(this);
        }
        finally {
            source.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public void listChanged(ListEvent<E> listChanges) {
        listChanges.nextBlock();
        int startIndex = listChanges.getBlockStartIndex();
        int endIndex = listChanges.getBlockEndIndex();
        this.listDataEvent.setRange(startIndex, endIndex);
        int changeType = listChanges.getType();
        switch (changeType) {
            case 2: {
                this.listDataEvent.setType(1);
                break;
            }
            case 0: {
                this.listDataEvent.setType(2);
                break;
            }
            case 1: {
                this.listDataEvent.setType(0);
            }
        }
        if (listChanges.nextBlock()) {
            this.listDataEvent.setRange(0, Integer.MAX_VALUE);
            this.listDataEvent.setType(0);
        }
        this.fireListDataEvent(this.listDataEvent);
    }

    public Object getElementAt(int index) {
        this.swingSource.getReadWriteLock().readLock().lock();
        try {
            E e = this.swingSource.get(index);
            return e;
        }
        finally {
            this.swingSource.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public int getSize() {
        this.swingSource.getReadWriteLock().readLock().lock();
        try {
            int n = this.swingSource.size();
            return n;
        }
        finally {
            this.swingSource.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public void addListDataListener(ListDataListener listDataListener) {
        this.listeners.add(listDataListener);
    }

    @Override
    public void removeListDataListener(ListDataListener listDataListener) {
        this.listeners.remove(listDataListener);
    }

    protected void fireListDataEvent(ListDataEvent listDataEvent) {
        int n = this.listeners.size();
        block5 : for (int i = 0; i < n; ++i) {
            ListDataListener listDataListener = this.listeners.get(i);
            switch (listDataEvent.getType()) {
                case 0: {
                    listDataListener.contentsChanged(listDataEvent);
                    continue block5;
                }
                case 1: {
                    listDataListener.intervalAdded(listDataEvent);
                    continue block5;
                }
                case 2: {
                    listDataListener.intervalRemoved(listDataEvent);
                }
            }
        }
    }

    public void dispose() {
        this.swingSource.removeListEventListener(this);
        this.swingSource.dispose();
    }
}

