/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.RangeList;
import ca.odell.glazedlists.SortedList;
import java.util.Comparator;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class ThresholdList<E>
extends RangeList<E> {
    private int lowerThreshold = Integer.MIN_VALUE;
    private int upperThreshold = Integer.MAX_VALUE;
    private Evaluator<E> evaluator = null;
    private final SortedList<E> sortedSource;

    public ThresholdList(EventList<E> source, String propertyName) {
        this(source, GlazedLists.thresholdEvaluator(propertyName));
    }

    public ThresholdList(EventList<E> source, Evaluator<E> evaluator) {
        this(new SortedList<E>(source, new ThresholdComparator<E>(evaluator)), evaluator);
    }

    private ThresholdList(SortedList<E> sortedSource, Evaluator<E> evaluator) {
        super(sortedSource);
        this.sortedSource = sortedSource;
        this.evaluator = evaluator;
    }

    public void setLowerThreshold(E object) {
        this.setLowerThreshold(this.evaluator.evaluate(object));
    }

    public void setLowerThreshold(int lowerThreshold) {
        this.lowerThreshold = lowerThreshold;
        this.adjustRange();
    }

    public int getLowerThreshold() {
        return this.lowerThreshold;
    }

    public void setUpperThreshold(E object) {
        this.setUpperThreshold(this.evaluator.evaluate(object));
    }

    public void setUpperThreshold(int upperThreshold) {
        this.upperThreshold = upperThreshold;
        this.adjustRange();
    }

    public int getUpperThreshold() {
        return this.upperThreshold;
    }

    public Evaluator<E> getEvaluator() {
        return this.evaluator;
    }

    @Override
    public boolean contains(Object object) {
        if (!this.withinRange(object)) {
            return false;
        }
        return this.source.contains(object);
    }

    @Override
    public int indexOf(Object object) {
        if (!this.withinRange(object)) {
            return -1;
        }
        return this.source.indexOf(object);
    }

    @Override
    public int lastIndexOf(Object object) {
        if (!this.withinRange(object)) {
            return -1;
        }
        return this.source.lastIndexOf(object);
    }

    private boolean withinRange(E object) {
        int objectEvaluation = this.evaluator.evaluate(object);
        return objectEvaluation >= this.lowerThreshold && objectEvaluation <= this.upperThreshold;
    }

    @Override
    public void setRange(int startIndex, int endIndex) {
        this.lowerThreshold = this.sourceIndexToThreshold(startIndex);
        this.upperThreshold = this.sourceIndexToThreshold(endIndex);
        this.adjustRange();
    }

    @Override
    public void setTailRange(int startIndex, int endIndex) {
        this.lowerThreshold = this.sourceIndexToThreshold(this.source.size() - startIndex);
        this.upperThreshold = this.sourceIndexToThreshold(this.source.size() - endIndex);
        this.adjustRange();
    }

    private int sourceIndexToThreshold(int sourceIndex) {
        if (sourceIndex < 0) {
            return Integer.MIN_VALUE;
        }
        if (sourceIndex < this.source.size()) {
            return this.evaluator.evaluate(this.source.get(sourceIndex));
        }
        return Integer.MIN_VALUE;
    }

    @Override
    public int getStartIndex() {
        return this.sortedSource.sortIndex(new Integer(this.lowerThreshold));
    }

    @Override
    public int getEndIndex() {
        int index = this.sortedSource.lastSortIndex(new Integer(this.upperThreshold));
        if (index < this.sortedSource.size() && this.evaluator.evaluate(this.sortedSource.get(index)) == this.upperThreshold) {
            ++index;
        }
        return index;
    }

    @Override
    public void dispose() {
        this.sortedSource.dispose();
        super.dispose();
    }

    public static interface Evaluator<E> {
        public int evaluate(E var1);
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    static final class ThresholdComparator<E>
    implements Comparator<E> {
        private Evaluator<E> evaluator = null;

        ThresholdComparator(Evaluator<E> evaluator) {
            this.evaluator = evaluator;
        }

        @Override
        public int compare(E alpha, E beta) {
            int alphaValue = alpha instanceof Integer ? ((Integer)alpha).intValue() : this.evaluator.evaluate(alpha);
            int betaValue = beta instanceof Integer ? ((Integer)beta).intValue() : this.evaluator.evaluate(beta);
            if (alphaValue > betaValue) {
                return 1;
            }
            if (alphaValue < betaValue) {
                return -1;
            }
            return 0;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || this.getClass() != o.getClass()) {
                return false;
            }
            ThresholdComparator that = (ThresholdComparator)o;
            if (!this.evaluator.equals(that.evaluator)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return this.evaluator.hashCode();
        }
    }

}

