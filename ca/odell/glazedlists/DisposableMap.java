/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists;

import java.util.Map;

public interface DisposableMap<K, V>
extends Map<K, V> {
    public void dispose();
}

