/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.TransformedList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventAssembler;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.impl.adt.Barcode;
import ca.odell.glazedlists.impl.adt.BarcodeIterator;
import ca.odell.glazedlists.util.concurrent.Lock;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class ObservableElementList<E>
extends TransformedList<E, E> {
    private List<E> observedElements;
    private Connector<? super E> elementConnector = null;
    private boolean singleListenerMode = true;
    private List<EventListener> multiEventListenerRegistry = null;
    private EventListener singleEventListener = null;
    private Barcode singleEventListenerRegistry = null;

    public ObservableElementList(EventList<E> source, Connector<? super E> elementConnector) {
        super(source);
        this.elementConnector = elementConnector;
        this.elementConnector.setObservableElementList(this);
        this.observedElements = new ArrayList<E>(source);
        this.singleEventListenerRegistry = new Barcode();
        this.singleEventListenerRegistry.addWhite(0, source.size());
        int n = this.size();
        for (int i = 0; i < n; ++i) {
            EventListener listener = this.connectElement(this.get(i));
            this.registerListener(i, listener, false);
        }
        source.addListEventListener(this);
    }

    @Override
    public void listChanged(ListEvent<E> listChanges) {
        if (this.observedElements == null) {
            throw new IllegalStateException("This list has been disposed and can no longer be used.");
        }
        while (listChanges.next()) {
            EventListener listener;
            Object newValue;
            int changeIndex = listChanges.getIndex();
            int changeType = listChanges.getType();
            if (changeType == 2) {
                Object inserted = this.get(changeIndex);
                this.observedElements.add(changeIndex, inserted);
                EventListener listener2 = this.connectElement(inserted);
                this.registerListener(changeIndex, listener2, false);
                continue;
            }
            if (changeType == 0) {
                E deleted = listChanges.getOldValue();
                E deletedElementFromPrivateCopy = this.observedElements.remove(changeIndex);
                if (deleted == ListEvent.UNKNOWN_VALUE) {
                    deleted = deletedElementFromPrivateCopy;
                }
                listener = this.unregisterListener(changeIndex);
                this.disconnectElement(deleted, listener);
                continue;
            }
            if (changeType != 1) continue;
            E previousValue = listChanges.getOldValue();
            if (previousValue == ListEvent.UNKNOWN_VALUE) {
                previousValue = this.observedElements.get(changeIndex);
            }
            if ((newValue = this.get(changeIndex)) == previousValue) continue;
            this.observedElements.set(changeIndex, newValue);
            this.disconnectElement(previousValue, this.getListener(changeIndex));
            listener = this.connectElement(newValue);
            this.registerListener(changeIndex, listener, true);
        }
        listChanges.reset();
        this.updates.forwardEvent(listChanges);
    }

    private void registerListener(int index, EventListener listener, boolean replace) {
        if (replace) {
            if (this.singleListenerMode) {
                this.singleEventListenerRegistry.set(index, listener == null ? Barcode.WHITE : Barcode.BLACK, 1);
            } else {
                this.multiEventListenerRegistry.set(index, listener);
            }
        } else if (this.singleListenerMode) {
            this.singleEventListenerRegistry.add(index, listener == null ? Barcode.WHITE : Barcode.BLACK, 1);
        } else {
            this.multiEventListenerRegistry.add(index, listener);
        }
    }

    private EventListener getListener(int index) {
        EventListener listener = null;
        if (this.singleListenerMode) {
            if (this.singleEventListenerRegistry.get(index) == Barcode.BLACK) {
                listener = this.singleEventListener;
            }
        } else {
            listener = this.multiEventListenerRegistry.get(index);
        }
        return listener;
    }

    private EventListener unregisterListener(int index) {
        EventListener listener = null;
        if (this.singleListenerMode) {
            if (this.singleEventListenerRegistry.get(index) == Barcode.BLACK) {
                listener = this.singleEventListener;
            }
            this.singleEventListenerRegistry.remove(index, 1);
        } else {
            listener = this.multiEventListenerRegistry.remove(index);
        }
        return listener;
    }

    private EventListener connectElement(E listElement) {
        if (listElement == null) {
            return null;
        }
        EventListener listener = this.elementConnector.installListener(listElement);
        if (this.singleListenerMode && listener != null) {
            if (this.singleEventListener == null) {
                this.singleEventListener = listener;
            } else if (listener != this.singleEventListener) {
                this.switchToMultiListenerMode();
            }
        }
        return listener;
    }

    private void disconnectElement(E listElement, EventListener listener) {
        if (listElement != null && listener != null) {
            this.elementConnector.uninstallListener(listElement, listener);
        }
    }

    private void switchToMultiListenerMode() {
        if (!this.singleListenerMode) {
            throw new IllegalStateException();
        }
        this.multiEventListenerRegistry = new ArrayList<EventListener>(this.source.size());
        for (int i = 0; i < this.source.size(); ++i) {
            this.multiEventListenerRegistry.add(null);
        }
        BarcodeIterator iter = this.singleEventListenerRegistry.iterator();
        while (iter.hasNextBlack()) {
            iter.nextBlack();
            this.multiEventListenerRegistry.set(iter.getIndex(), this.singleEventListener);
        }
        this.singleEventListener = null;
        this.singleEventListenerRegistry = null;
        this.singleListenerMode = false;
    }

    @Override
    protected boolean isWritable() {
        return true;
    }

    @Override
    public void dispose() {
        int n = this.observedElements.size();
        for (int i = 0; i < n; ++i) {
            E element = this.observedElements.get(i);
            EventListener listener = this.getListener(i);
            this.disconnectElement(element, listener);
        }
        this.elementConnector.setObservableElementList(null);
        this.observedElements = null;
        this.multiEventListenerRegistry = null;
        this.singleEventListener = null;
        this.singleEventListenerRegistry = null;
        this.elementConnector = null;
        super.dispose();
    }

    public void elementChanged(E listElement) {
        if (this.observedElements == null) {
            throw new IllegalStateException("This list has been disposed and can no longer be used.");
        }
        this.getReadWriteLock().writeLock().lock();
        try {
            this.updates.beginEvent();
            int n = this.size();
            for (int i = 0; i < n; ++i) {
                if (listElement != this.get(i)) continue;
                this.updates.elementUpdated(i, listElement);
            }
            this.updates.commitEvent();
        }
        finally {
            this.getReadWriteLock().writeLock().unlock();
        }
    }

    public static interface Connector<E> {
        public EventListener installListener(E var1);

        public void uninstallListener(E var1, EventListener var2);

        public void setObservableElementList(ObservableElementList<? extends E> var1);
    }

}

