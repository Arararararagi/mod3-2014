/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEventAssembler;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.event.ListEventPublisher;
import ca.odell.glazedlists.impl.EventListIterator;
import ca.odell.glazedlists.impl.GlazedListsImpl;
import ca.odell.glazedlists.impl.SimpleIterator;
import ca.odell.glazedlists.impl.SubEventList;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public abstract class AbstractEventList<E>
implements EventList<E> {
    protected ListEventAssembler<E> updates = null;
    protected ReadWriteLock readWriteLock = null;
    protected ListEventPublisher publisher = null;

    protected AbstractEventList(ListEventPublisher publisher) {
        if (publisher == null) {
            publisher = ListEventAssembler.createListEventPublisher();
        }
        this.publisher = publisher;
        this.updates = new ListEventAssembler(this, publisher);
    }

    protected AbstractEventList() {
        this(null);
    }

    @Override
    public ListEventPublisher getPublisher() {
        return this.publisher;
    }

    @Override
    public ReadWriteLock getReadWriteLock() {
        return this.readWriteLock;
    }

    @Override
    public void addListEventListener(ListEventListener<? super E> listChangeListener) {
        this.updates.addListEventListener(listChangeListener);
    }

    @Override
    public void removeListEventListener(ListEventListener<? super E> listChangeListener) {
        this.updates.removeListEventListener(listChangeListener);
    }

    @Override
    public abstract int size();

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    public boolean contains(Object object) {
        Iterator<E> i = this.iterator();
        while (i.hasNext()) {
            if (!GlazedListsImpl.equal(object, i.next())) continue;
            return true;
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return new SimpleIterator<E>(this);
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[this.size()];
        int index = 0;
        for (Object array[index] : this) {
            ++index;
        }
        return array;
    }

    @Override
    public <T> T[] toArray(T[] array) {
        if (array.length < this.size()) {
            array = (Object[])Array.newInstance(array.getClass().getComponentType(), this.size());
        } else if (array.length > this.size()) {
            array[this.size()] = null;
        }
        int index = 0;
        for (Object array[index] : this) {
            ++index;
        }
        return array;
    }

    @Override
    public boolean add(E value) {
        int initialSize = this.size();
        this.add(this.size(), value);
        return this.size() != initialSize;
    }

    @Override
    public boolean remove(Object toRemove) {
        int index = this.indexOf(toRemove);
        if (index == -1) {
            return false;
        }
        this.remove(index);
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> values) {
        for (? a : values) {
            if (this.contains(a)) continue;
            return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> values) {
        return this.addAll(this.size(), values);
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> values) {
        if (index < 0 || index > this.size()) {
            throw new IndexOutOfBoundsException("Cannot add at " + index + " on list of size " + this.size());
        }
        if (values.size() == 0) {
            return false;
        }
        int initializeSize = this.size();
        Iterator<E> iter = values.iterator();
        while (iter.hasNext()) {
            this.add(index, iter.next());
            if (index >= this.size()) continue;
            ++index;
        }
        return this.size() != initializeSize;
    }

    @Override
    public boolean removeAll(Collection<?> values) {
        boolean changed = false;
        Iterator<E> i = this.iterator();
        while (i.hasNext()) {
            if (!values.contains(i.next())) continue;
            i.remove();
            changed = true;
        }
        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> values) {
        boolean changed = false;
        Iterator<E> i = this.iterator();
        while (i.hasNext()) {
            if (values.contains(i.next())) continue;
            i.remove();
            changed = true;
        }
        return changed;
    }

    @Override
    public void clear() {
        Iterator<E> i = this.iterator();
        while (i.hasNext()) {
            i.next();
            i.remove();
        }
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (!(object instanceof List)) {
            return false;
        }
        List otherList = (List)object;
        if (otherList.size() != this.size()) {
            return false;
        }
        Iterator<E> iterA = this.iterator();
        Iterator<E> iterB = otherList.iterator();
        while (iterA.hasNext() && iterB.hasNext()) {
            if (GlazedListsImpl.equal(iterA.next(), iterB.next())) continue;
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hashCode = 1;
        for (E a : this) {
            hashCode = 31 * hashCode + (a == null ? 0 : a.hashCode());
        }
        return hashCode;
    }

    @Override
    public abstract E get(int var1);

    @Override
    public E set(int index, E value) {
        throw new UnsupportedOperationException("this list does not support set()");
    }

    @Override
    public void add(int index, E value) {
        throw new UnsupportedOperationException("this list does not support add()");
    }

    @Override
    public E remove(int index) {
        throw new UnsupportedOperationException("this list does not support remove()");
    }

    @Override
    public int indexOf(Object object) {
        int index = 0;
        Iterator<E> i = this.iterator();
        while (i.hasNext()) {
            if (GlazedListsImpl.equal(object, i.next())) {
                return index;
            }
            ++index;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object object) {
        for (int i = this.size() - 1; i >= 0; --i) {
            if (!GlazedListsImpl.equal(object, this.get(i))) continue;
            return i;
        }
        return -1;
    }

    @Override
    public ListIterator<E> listIterator() {
        return this.listIterator(0);
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return new EventListIterator<E>(this, index);
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return new SubEventList<E>(this, fromIndex, toIndex, true);
    }

    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append("[");
        Iterator<E> i = this.iterator();
        while (i.hasNext()) {
            result.append(String.valueOf(i.next()));
            if (!i.hasNext()) continue;
            result.append(", ");
        }
        result.append("]");
        return result.toString();
    }
}

