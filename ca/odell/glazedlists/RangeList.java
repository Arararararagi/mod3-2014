/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.TransformedList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventAssembler;
import ca.odell.glazedlists.event.ListEventListener;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class RangeList<E>
extends TransformedList<E, E> {
    private int desiredStart = 0;
    private int desiredEnd = -1;
    private int currentStartIndex = 0;
    private int currentEndIndex;

    public RangeList(EventList<E> source) {
        super(source);
        this.currentEndIndex = source.size();
        source.addListEventListener(this);
    }

    @Override
    public final void listChanged(ListEvent<E> listChanges) {
        this.updates.beginEvent(true);
        while (listChanges.next()) {
            int changeType = listChanges.getType();
            int changeIndex = listChanges.getIndex();
            E oldValue = listChanges.getOldValue();
            E newValue = listChanges.getNewValue();
            if (changeType == 0) {
                if (changeIndex < this.currentStartIndex) {
                    --this.currentStartIndex;
                    --this.currentEndIndex;
                    continue;
                }
                if (changeIndex >= this.currentEndIndex) continue;
                --this.currentEndIndex;
                this.updates.elementDeleted(changeIndex - this.currentStartIndex, oldValue);
                continue;
            }
            if (changeType == 2) {
                if (changeIndex < this.currentStartIndex) {
                    ++this.currentStartIndex;
                    ++this.currentEndIndex;
                    continue;
                }
                if (changeIndex >= this.currentEndIndex) continue;
                ++this.currentEndIndex;
                this.updates.elementInserted(changeIndex - this.currentStartIndex, newValue);
                continue;
            }
            if (changeType != 1 || changeIndex < this.currentStartIndex || changeIndex >= this.currentEndIndex) continue;
            this.updates.elementUpdated(changeIndex - this.currentStartIndex, oldValue, newValue);
        }
        this.adjustRange();
        this.updates.commitEvent();
    }

    public void setRange(int startIndex, int endIndex) {
        this.setHeadRange(startIndex, endIndex);
    }

    public void setHeadRange(int startIndex, int endIndex) {
        this.desiredStart = startIndex;
        this.desiredEnd = endIndex;
        this.adjustRange();
    }

    public void setMiddleRange(int startIndex, int endIndex) {
        this.desiredStart = startIndex;
        this.desiredEnd = - endIndex - 1;
        this.adjustRange();
    }

    public void setTailRange(int startIndex, int endIndex) {
        this.desiredStart = - startIndex - 1;
        this.desiredEnd = - endIndex - 1;
        this.adjustRange();
    }

    protected final void adjustRange() {
        this.updates.beginEvent(true);
        int desiredStartIndex = this.getStartIndex();
        int desiredEndIndex = this.getEndIndex();
        if (desiredEndIndex < desiredStartIndex) {
            int temp = desiredEndIndex;
            desiredEndIndex = desiredStartIndex;
            desiredStartIndex = temp;
        }
        if (desiredStartIndex < this.currentStartIndex) {
            this.updates.addInsert(0, this.currentStartIndex - desiredStartIndex - 1);
        } else if (this.currentStartIndex < desiredStartIndex && this.currentStartIndex < this.currentEndIndex) {
            int deleteThru = Math.min(desiredStartIndex, this.currentEndIndex);
            for (int i = this.currentStartIndex; i < deleteThru; ++i) {
                this.updates.elementDeleted(0, this.source.get(i));
            }
        }
        this.currentStartIndex = desiredStartIndex;
        if (desiredEndIndex < this.currentEndIndex) {
            for (int i = desiredEndIndex; i < this.currentEndIndex; ++i) {
                this.updates.elementDeleted(desiredEndIndex - this.currentStartIndex, this.source.get(i));
            }
        } else if (this.currentEndIndex < desiredEndIndex && desiredStartIndex < desiredEndIndex) {
            int insertFrom = Math.max(this.currentEndIndex, this.currentStartIndex);
            this.updates.addInsert(insertFrom - this.currentStartIndex, desiredEndIndex - this.currentStartIndex - 1);
        }
        this.currentEndIndex = desiredEndIndex;
        this.updates.commitEvent();
    }

    @Override
    public final int size() {
        return this.currentEndIndex - this.currentStartIndex;
    }

    @Override
    protected final int getSourceIndex(int mutationIndex) {
        return mutationIndex + this.currentStartIndex;
    }

    @Override
    protected final boolean isWritable() {
        return true;
    }

    public int getStartIndex() {
        int desiredStartIndex;
        int n = desiredStartIndex = this.desiredStart >= 0 ? this.desiredStart : this.source.size() + this.desiredStart + 1;
        if (desiredStartIndex < 0) {
            return 0;
        }
        if (desiredStartIndex > this.source.size()) {
            return this.source.size();
        }
        return desiredStartIndex;
    }

    public int getEndIndex() {
        int desiredStartIndex;
        int desiredEndIndex = this.desiredEnd >= 0 ? this.desiredEnd : this.source.size() + this.desiredEnd + 1;
        if (desiredEndIndex < (desiredStartIndex = this.getStartIndex())) {
            return desiredStartIndex;
        }
        if (desiredEndIndex > this.source.size()) {
            return this.source.size();
        }
        return desiredEndIndex;
    }
}

