/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.TransformedList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventAssembler;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.impl.WeakReferenceProxy;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class SubEventList<E>
extends TransformedList<E, E> {
    private int startIndex;
    private int endIndex;

    public SubEventList(EventList<E> source, int startIndex, int endIndex, boolean automaticallyRemove) {
        super(source);
        if (startIndex < 0 || endIndex < startIndex || endIndex > source.size()) {
            throw new IllegalArgumentException("The range " + startIndex + "-" + endIndex + " is not valid over a list of size " + source.size());
        }
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        if (automaticallyRemove) {
            source.addListEventListener(new WeakReferenceProxy<E>(source, this));
        } else {
            source.addListEventListener(this);
        }
    }

    @Override
    public int size() {
        return this.endIndex - this.startIndex;
    }

    @Override
    protected int getSourceIndex(int mutationIndex) {
        return mutationIndex + this.startIndex;
    }

    @Override
    protected boolean isWritable() {
        return true;
    }

    @Override
    public void listChanged(ListEvent<E> listChanges) {
        this.updates.beginEvent();
        if (listChanges.isReordering() && this.size() == 1) {
            int[] reorderMap = listChanges.getReorderMap();
            for (int r = 0; r < reorderMap.length; ++r) {
                if (reorderMap[r] != this.startIndex) continue;
                this.startIndex = r;
                this.endIndex = this.startIndex + 1;
                break;
            }
        } else {
            while (listChanges.next()) {
                int changeIndex = listChanges.getIndex();
                int changeType = listChanges.getType();
                if (changeIndex < this.startIndex || changeType == 2 && changeIndex == this.startIndex) {
                    if (changeType == 2) {
                        ++this.startIndex;
                        ++this.endIndex;
                        continue;
                    }
                    if (changeType != 0) continue;
                    --this.startIndex;
                    --this.endIndex;
                    continue;
                }
                if (changeIndex >= this.endIndex) continue;
                if (changeType == 2) {
                    ++this.endIndex;
                    this.updates.elementInserted(changeIndex - this.startIndex, listChanges.getNewValue());
                    continue;
                }
                if (changeType == 1) {
                    this.updates.elementUpdated(changeIndex - this.startIndex, listChanges.getOldValue(), listChanges.getNewValue());
                    continue;
                }
                if (changeType != 0) continue;
                --this.endIndex;
                this.updates.elementDeleted(changeIndex - this.startIndex, listChanges.getOldValue());
            }
        }
        if (this.startIndex > this.endIndex) {
            throw new IllegalStateException();
        }
        this.updates.commitEvent();
    }
}

