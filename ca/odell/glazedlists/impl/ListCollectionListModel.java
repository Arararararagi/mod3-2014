/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl;

import ca.odell.glazedlists.CollectionList;
import java.util.Collections;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class ListCollectionListModel<E>
implements CollectionList.Model<List<E>, E> {
    @Override
    public List<E> getChildren(List<E> parent) {
        if (parent == null) {
            return Collections.EMPTY_LIST;
        }
        return parent;
    }
}

