/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.adt;

import ca.odell.glazedlists.impl.adt.Barcode;
import ca.odell.glazedlists.impl.adt.BarcodeNode;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class BarcodeIterator
implements Iterator {
    private Barcode barcode = null;
    private BarcodeNode currentNode = null;
    private int localIndex = -1;
    private int blackSoFar = 0;
    private int whiteSoFar = 0;

    BarcodeIterator(Barcode barcode) {
        BarcodeNode root = barcode.getRootNode();
        if (root != null) {
            this.currentNode = root;
            while (this.currentNode.left != null) {
                this.currentNode = this.currentNode.left;
            }
        }
        this.barcode = barcode;
    }

    public boolean hasNext() {
        if (this.getIndex() == this.barcode.size() - 1) {
            return false;
        }
        return true;
    }

    public boolean hasNextBlack() {
        if (this.getIndex() >= this.barcode.treeSize() - 1) {
            return false;
        }
        if (this.currentNode == null) {
            return false;
        }
        return true;
    }

    public boolean hasNextWhite() {
        if (this.barcode.size() != this.barcode.treeSize()) {
            return this.hasNext();
        }
        if (this.currentNode == null) {
            return false;
        }
        if (this.localIndex < this.currentNode.whiteSpace - 1 || this.whiteSoFar + this.currentNode.whiteSpace < this.barcode.whiteSize()) {
            return true;
        }
        return false;
    }

    public boolean hasNextColour(Object colour) {
        if (colour == Barcode.BLACK) {
            return this.hasNextBlack();
        }
        return this.hasNextWhite();
    }

    public Object next() {
        ++this.localIndex;
        if (this.currentNode == null) {
            if (this.getIndex() < this.barcode.size()) {
                return Barcode.WHITE;
            }
            throw new NoSuchElementException();
        }
        if (this.localIndex >= this.currentNode.whiteSpace + this.currentNode.rootSize) {
            if (this.getIndex() < this.barcode.treeSize()) {
                this.blackSoFar+=this.currentNode.rootSize;
                this.whiteSoFar+=this.currentNode.whiteSpace;
                this.findNextNode();
                this.localIndex = 0;
            } else {
                if (this.getIndex() < this.barcode.size()) {
                    return Barcode.WHITE;
                }
                throw new NoSuchElementException();
            }
        }
        if (this.localIndex < this.currentNode.whiteSpace) {
            return Barcode.WHITE;
        }
        return Barcode.BLACK;
    }

    public Object nextBlack() {
        ++this.localIndex;
        if (this.currentNode == null) {
            throw new NoSuchElementException();
        }
        if (this.localIndex < this.currentNode.whiteSpace) {
            this.localIndex = this.currentNode.whiteSpace;
        } else if (this.localIndex >= this.currentNode.whiteSpace + this.currentNode.rootSize) {
            if (this.getIndex() < this.barcode.treeSize()) {
                this.whiteSoFar+=this.currentNode.whiteSpace;
                this.blackSoFar+=this.currentNode.rootSize;
                this.findNextNode();
                this.localIndex = this.currentNode.whiteSpace;
            } else {
                throw new NoSuchElementException();
            }
        }
        if (this.localIndex < this.currentNode.whiteSpace) {
            throw new IllegalStateException();
        }
        return Barcode.BLACK;
    }

    public Object nextWhite() {
        ++this.localIndex;
        if (this.currentNode == null) {
            if (this.getIndex() < this.barcode.size()) {
                return Barcode.WHITE;
            }
            throw new NoSuchElementException();
        }
        if (this.localIndex >= this.currentNode.whiteSpace) {
            if (this.getIndex() < this.barcode.treeSize() && this.getIndex() + this.currentNode.rootSize >= this.barcode.treeSize()) {
                this.localIndex = this.currentNode.whiteSpace;
                this.localIndex+=this.currentNode.rootSize;
            }
            if (this.getIndex() < this.barcode.treeSize()) {
                this.blackSoFar+=this.currentNode.rootSize;
                this.whiteSoFar+=this.currentNode.whiteSpace;
                this.findNextNode();
                this.localIndex = 0;
            } else {
                if (this.getIndex() < this.barcode.size()) {
                    return Barcode.WHITE;
                }
                throw new NoSuchElementException();
            }
        }
        if (this.localIndex >= this.currentNode.whiteSpace) {
            throw new IllegalStateException();
        }
        return Barcode.WHITE;
    }

    public Object nextColour(Object colour) {
        if (colour == Barcode.BLACK) {
            return this.nextBlack();
        }
        return this.nextWhite();
    }

    public void remove() {
        if (this.localIndex == -1) {
            throw new NoSuchElementException("Cannot call remove() before next() is called.");
        }
        if (this.currentNode == null || this.getIndex() >= this.barcode.treeSize()) {
            this.barcode.remove(this.getIndex(), 1);
            --this.localIndex;
        } else {
            BarcodeNode affectedNode = this.currentNode;
            if (this.localIndex == 0 && this.currentNode.whiteSpace == 1 && this.getIndex() != 0) {
                this.findPreviousNode();
                this.blackSoFar-=this.currentNode.rootSize;
                this.whiteSoFar-=this.currentNode.whiteSpace;
                this.localIndex+=this.currentNode.whiteSpace + this.currentNode.rootSize;
            } else if (this.localIndex == this.currentNode.whiteSpace && this.currentNode.rootSize == 1) {
                if (this.localIndex == this.barcode.treeSize() - 1) {
                    this.currentNode = null;
                } else if (this.getIndex() == this.barcode.treeSize() - 1) {
                    this.findPreviousNode();
                    this.blackSoFar-=this.currentNode.rootSize;
                    this.whiteSoFar-=this.currentNode.whiteSpace;
                    this.localIndex+=this.currentNode.whiteSpace + this.currentNode.rootSize;
                } else {
                    this.findNextNode();
                }
            }
            affectedNode.removeBaseCase(this.getIndex(), this.localIndex);
            --this.localIndex;
        }
    }

    public int setWhite() {
        if (this.localIndex == -1) {
            throw new NoSuchElementException("Cannot call setWhite() before next() is called.");
        }
        if (this.currentNode == null || this.getIndex() >= this.barcode.treeSize() || this.localIndex < this.currentNode.whiteSpace) {
            return this.getWhiteIndex();
        }
        if (this.getIndex() != this.barcode.treeSize() - 1) {
            if (this.currentNode.rootSize == 1) {
                BarcodeNode affectedNode = this.currentNode;
                this.findNextNode();
                affectedNode.setWhite(this.getIndex(), this.localIndex, 1);
                if (this.barcode.getRootNode() != null) {
                    this.barcode.treeSizeChanged();
                }
                if (this.currentNode.whiteSpace == 0 && this.currentNode.rootSize == 0) {
                    this.currentNode = affectedNode;
                }
                return this.whiteSoFar + this.localIndex;
            }
            if (this.localIndex == this.currentNode.whiteSpace) {
                this.currentNode.setWhite(this.getIndex(), this.localIndex, 1);
                if (this.barcode.getRootNode() != null) {
                    this.barcode.treeSizeChanged();
                }
                return this.whiteSoFar + this.localIndex;
            }
            this.currentNode.setWhite(this.getIndex(), this.localIndex, 1);
            if (this.barcode.getRootNode() != null) {
                this.barcode.treeSizeChanged();
            }
            this.blackSoFar+=this.currentNode.rootSize;
            this.whiteSoFar+=this.currentNode.whiteSpace;
            this.findNextNode();
            this.localIndex = 0;
            return this.whiteSoFar;
        }
        if (this.currentNode.rootSize == 1) {
            BarcodeNode affectedNode = this.currentNode;
            if (this.currentNode.whiteSpace + 1 == this.barcode.treeSize()) {
                this.currentNode = null;
                affectedNode.setWhite(this.getIndex(), this.localIndex, 1);
                if (this.barcode.getRootNode() != null) {
                    this.barcode.treeSizeChanged();
                }
                return this.localIndex;
            }
            this.findPreviousNode();
            int currentLocalIndex = this.localIndex;
            this.blackSoFar-=this.currentNode.rootSize;
            this.whiteSoFar-=this.currentNode.whiteSpace;
            this.localIndex+=this.currentNode.whiteSpace + this.currentNode.rootSize;
            affectedNode.setWhite(this.getIndex(), currentLocalIndex, 1);
            if (this.barcode.getRootNode() != null) {
                this.barcode.treeSizeChanged();
            }
            if (this.currentNode.whiteSpace == 0 && this.currentNode.rootSize == 0) {
                this.currentNode = affectedNode;
            }
            return this.whiteSoFar + this.localIndex - this.currentNode.rootSize;
        }
        this.currentNode.setWhite(this.getIndex(), this.localIndex, 1);
        if (this.barcode.getRootNode() != null) {
            this.barcode.treeSizeChanged();
        }
        return this.whiteSoFar + this.localIndex - this.currentNode.rootSize;
    }

    public int setBlack() {
        if (this.localIndex == -1) {
            throw new NoSuchElementException("Cannot call setBlack() before next() is called.");
        }
        if (this.currentNode == null) {
            this.barcode.setBlack(this.getIndex(), 1);
            this.currentNode = this.barcode.getRootNode();
            return 0;
        }
        if (this.getIndex() == this.barcode.treeSize()) {
            this.barcode.setBlack(this.getIndex(), 1);
            if (this.barcode.getRootNode() != null) {
                this.barcode.treeSizeChanged();
            }
        } else if (this.getIndex() > this.barcode.treeSize()) {
            this.barcode.setBlack(this.getIndex(), 1);
            if (this.barcode.getRootNode() != null) {
                this.barcode.treeSizeChanged();
            }
            this.whiteSoFar+=this.currentNode.whiteSpace;
            this.blackSoFar+=this.currentNode.rootSize;
            this.localIndex-=this.currentNode.whiteSpace + this.currentNode.rootSize;
            this.findNextNode();
        } else if (this.localIndex < this.currentNode.whiteSpace && this.currentNode.whiteSpace == 1) {
            if (this.getIndex() == 0) {
                this.currentNode.setBlack(this.getIndex(), this.localIndex, 1);
                if (this.barcode.getRootNode() != null) {
                    this.barcode.treeSizeChanged();
                }
            } else {
                BarcodeNode affectedNode = this.currentNode;
                this.findPreviousNode();
                int currentLocalIndex = this.localIndex;
                this.blackSoFar-=this.currentNode.rootSize;
                this.whiteSoFar-=this.currentNode.whiteSpace;
                this.localIndex+=this.currentNode.whiteSpace + this.currentNode.rootSize;
                affectedNode.setBlack(this.getIndex(), currentLocalIndex, 1);
                if (this.barcode.getRootNode() != null) {
                    this.barcode.treeSizeChanged();
                }
                if (this.currentNode.whiteSpace == 0 && this.currentNode.rootSize == 0) {
                    this.currentNode = affectedNode;
                }
            }
        } else if (this.localIndex < this.currentNode.whiteSpace) {
            if (this.localIndex == 0) {
                this.currentNode.setBlack(this.getIndex(), this.localIndex, 1);
                if (this.barcode.getRootNode() != null) {
                    this.barcode.treeSizeChanged();
                }
                ++this.blackSoFar;
                --this.localIndex;
                return this.blackSoFar - 1;
            }
            if (this.localIndex == this.currentNode.whiteSpace - 1) {
                this.currentNode.setBlack(this.getIndex(), this.localIndex, 1);
                if (this.barcode.getRootNode() != null) {
                    this.barcode.treeSizeChanged();
                }
                return this.blackSoFar;
            }
            this.currentNode.setBlack(this.getIndex(), this.localIndex, 1);
            if (this.barcode.getRootNode() != null) {
                this.barcode.treeSizeChanged();
            }
            this.whiteSoFar+=this.localIndex;
            ++this.blackSoFar;
            this.localIndex = -1;
            return this.blackSoFar - 1;
        }
        return this.getBlackIndex();
    }

    public int set(Object colour) {
        if (colour == Barcode.BLACK) {
            return this.setBlack();
        }
        return this.setWhite();
    }

    public int getIndex() {
        return this.blackSoFar + this.whiteSoFar + this.localIndex;
    }

    public int getBlackIndex() {
        if (this.localIndex == -1) {
            return this.blackSoFar - 1;
        }
        if (this.currentNode == null || this.localIndex < this.currentNode.whiteSpace || this.localIndex >= this.currentNode.whiteSpace + this.currentNode.rootSize) {
            return -1;
        }
        return this.blackSoFar + this.localIndex - this.currentNode.whiteSpace;
    }

    public int getWhiteIndex() {
        if (this.currentNode == null) {
            if (this.localIndex == -1 && this.whiteSoFar != 0) {
                return this.whiteSoFar - 1;
            }
            return this.localIndex;
        }
        if (this.localIndex >= this.currentNode.whiteSpace && this.localIndex < this.currentNode.whiteSpace + this.currentNode.rootSize) {
            return -1;
        }
        if (this.localIndex >= this.currentNode.whiteSpace + this.currentNode.rootSize) {
            return this.whiteSoFar + this.localIndex - this.currentNode.rootSize;
        }
        return this.whiteSoFar + this.localIndex;
    }

    public int getColourIndex(Object colour) {
        if (colour == Barcode.WHITE) {
            return this.getWhiteIndex();
        }
        return this.getBlackIndex();
    }

    private void findNextNode() {
        if (this.currentNode.right != null) {
            this.currentNode = this.currentNode.right;
            while (this.currentNode.left != null) {
                this.currentNode = this.currentNode.left;
            }
        } else if (this.currentNode.parent.left == this.currentNode) {
            this.currentNode = this.currentNode.parent;
        } else if (this.currentNode.parent.right == this.currentNode) {
            while (this.currentNode.parent.right == this.currentNode) {
                this.currentNode = this.currentNode.parent;
            }
            this.currentNode = this.currentNode.parent;
        } else {
            throw new IllegalStateException();
        }
    }

    private void findPreviousNode() {
        if (this.currentNode.left != null) {
            this.currentNode = this.currentNode.left;
            while (this.currentNode.right != null) {
                this.currentNode = this.currentNode.right;
            }
        } else if (this.currentNode.parent.right == this.currentNode) {
            this.currentNode = this.currentNode.parent;
        } else if (this.currentNode.parent.left == this.currentNode) {
            while (this.currentNode.parent.left == this.currentNode) {
                this.currentNode = this.currentNode.parent;
            }
            this.currentNode = this.currentNode.parent;
        } else {
            throw new IllegalStateException();
        }
    }
}

