/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.adt.barcode2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class ListToByteCoder<C> {
    private final List<C> allColors;
    private final int colorCount;

    public ListToByteCoder(List<C> allColors) {
        if (allColors.size() > 7) {
            throw new IllegalArgumentException("Max 7 colors!");
        }
        this.allColors = Collections.unmodifiableList(new ArrayList<C>(allColors));
        this.colorCount = this.allColors.size();
    }

    public List<C> getColors() {
        return this.allColors;
    }

    public byte colorsToByte(List<C> colors) {
        int result = 0;
        for (int i = 0; i < colors.size(); ++i) {
            C color = colors.get(i);
            int index = this.allColors.indexOf(color);
            result|=1 << index;
        }
        return result;
    }

    public byte colorToByte(C color) {
        int index = this.allColors.indexOf(color);
        int result = 1 << index;
        return (byte)result;
    }

    public C byteToColor(byte encoded) {
        for (int i = 0; i < this.colorCount; ++i) {
            if ((1 << i & encoded) <= 0) continue;
            return this.allColors.get(i);
        }
        throw new IllegalStateException();
    }

    public List<C> byteToColors(byte encoded) {
        ArrayList<C> result = new ArrayList<C>(this.colorCount);
        for (int i = 0; i < this.colorCount; ++i) {
            if ((1 << i & encoded) <= 0) continue;
            result.add(this.allColors.get(i));
        }
        return result;
    }
}

