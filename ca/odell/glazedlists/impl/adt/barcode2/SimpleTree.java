/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.adt.barcode2;

import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.impl.adt.barcode2.Element;
import ca.odell.glazedlists.impl.adt.barcode2.SimpleNode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class SimpleTree<T0> {
    private SimpleNode<T0> root = null;
    private final List<SimpleNode<T0>> zeroQueue = new ArrayList<SimpleNode<T0>>();
    private final Comparator<? super T0> comparator;

    public SimpleTree(Comparator<? super T0> comparator) {
        if (comparator == null) {
            throw new NullPointerException("Comparator cannot be null.");
        }
        this.comparator = comparator;
    }

    public SimpleTree() {
        this(GlazedLists.comparableComparator());
    }

    public Comparator<? super T0> getComparator() {
        return this.comparator;
    }

    public Element<T0> get(int index) {
        if (this.root == null) {
            throw new IndexOutOfBoundsException();
        }
        SimpleNode<T0> node = this.root;
        do {
            int leftSize;
            assert (node != null);
            if (!($assertionsDisabled || index >= 0)) {
                throw new AssertionError();
            }
            SimpleNode nodeLeft = node.left;
            int n = leftSize = nodeLeft != null ? nodeLeft.count1 : 0;
            if (index < leftSize) {
                node = nodeLeft;
                continue;
            }
            int size = 1;
            if ((index-=leftSize) < size) {
                return node;
            }
            index-=size;
            node = node.right;
        } while (true);
    }

    public Element<T0> add(int index, T0 value, int size) {
        assert (index >= 0);
        if (!($assertionsDisabled || index <= this.size())) {
            throw new AssertionError();
        }
        assert (size >= 0);
        if (this.root == null) {
            if (index != 0) {
                throw new IndexOutOfBoundsException();
            }
            this.root = new SimpleNode<T0>(size, value, null);
            assert (this.valid());
            return this.root;
        }
        SimpleNode<T0> inserted = this.insertIntoSubtree(this.root, index, value, size);
        assert (this.valid());
        return inserted;
    }

    private SimpleNode<T0> insertIntoSubtree(SimpleNode<T0> parent, int index, T0 value, int size) {
        do {
            assert (parent != null);
            if (!($assertionsDisabled || index >= 0)) {
                throw new AssertionError();
            }
            SimpleNode parentLeft = parent.left;
            int parentLeftSize = parentLeft != null ? parentLeft.count1 : 0;
            int parentRightStartIndex = parentLeftSize + 1;
            if (index <= parentLeftSize) {
                if (parentLeft == null) {
                    SimpleNode<T0> inserted;
                    parent.left = inserted = new SimpleNode<T0>(size, value, parent);
                    this.fixCountsThruRoot(parent, size);
                    this.fixHeightPostChange(parent, false);
                    return inserted;
                }
                parent = parentLeft;
                continue;
            }
            if (index < parentRightStartIndex) {
                int parentRightHalfSize = parentRightStartIndex - index;
                this.fixCountsThruRoot(parent, - parentRightHalfSize);
                SimpleNode<Object> inserted = this.insertIntoSubtree(parent, index, null, parentRightHalfSize);
                inserted.set(parent.t0);
                parentRightStartIndex = parentLeftSize + 1;
            }
            int parentSize = parent.count1;
            assert (index <= parentSize);
            SimpleNode parentRight = parent.right;
            if (parentRight == null) {
                SimpleNode<T0> inserted;
                parent.right = inserted = new SimpleNode<T0>(size, value, parent);
                this.fixCountsThruRoot(parent, size);
                this.fixHeightPostChange(parent, false);
                return inserted;
            }
            parent = parentRight;
            index-=parentRightStartIndex;
        } while (true);
    }

    public Element<T0> addInSortedOrder(byte color, T0 value, int size) {
        assert (size >= 0);
        if (this.root == null) {
            this.root = new SimpleNode<T0>(size, value, null);
            assert (this.valid());
            return this.root;
        }
        SimpleNode<T0> inserted = this.insertIntoSubtreeInSortedOrder(this.root, value, size);
        assert (this.valid());
        return inserted;
    }

    private SimpleNode<T0> insertIntoSubtreeInSortedOrder(SimpleNode<T0> parent, T0 value, int size) {
        do {
            int sortSide;
            assert (parent != null);
            SimpleNode<T0> currentFollower = parent;
            do {
                if (currentFollower == null) {
                    sortSide = -1;
                    break;
                }
                if (currentFollower.sorted == 0) {
                    sortSide = this.comparator.compare(value, currentFollower.t0);
                    break;
                }
                currentFollower = SimpleTree.next(currentFollower);
            } while (true);
            boolean insertOnLeft = false;
            insertOnLeft = insertOnLeft || sortSide < 0;
            insertOnLeft = insertOnLeft || sortSide == 0 && parent.left == null;
            boolean bl = insertOnLeft = insertOnLeft || sortSide == 0 && parent.right != null && parent.left.height < parent.right.height;
            if (insertOnLeft) {
                SimpleNode parentLeft = parent.left;
                if (parentLeft == null) {
                    SimpleNode<T0> inserted;
                    parent.left = inserted = new SimpleNode<T0>(size, value, parent);
                    this.fixCountsThruRoot(parent, size);
                    this.fixHeightPostChange(parent, false);
                    return inserted;
                }
                parent = parentLeft;
                continue;
            }
            SimpleNode parentRight = parent.right;
            if (parentRight == null) {
                SimpleNode<T0> inserted;
                parent.right = inserted = new SimpleNode<T0>(size, value, parent);
                this.fixCountsThruRoot(parent, size);
                this.fixHeightPostChange(parent, false);
                return inserted;
            }
            parent = parentRight;
        } while (true);
    }

    private final void fixCountsThruRoot(SimpleNode<T0> node, int delta) {
        while (node != null) {
            node.count1+=delta;
            node = node.parent;
        }
    }

    private final void fixHeightPostChange(SimpleNode<T0> node, boolean allTheWayToRoot) {
        while (node != null) {
            byte rightHeight;
            byte leftHeight = node.left != null ? node.left.height : 0;
            byte by = rightHeight = node.right != null ? node.right.height : 0;
            if (leftHeight > rightHeight && leftHeight - rightHeight == 2) {
                byte leftRightHeight;
                byte leftLeftHeight = node.left.left != null ? node.left.left.height : 0;
                byte by2 = leftRightHeight = node.left.right != null ? node.left.right.height : 0;
                if (leftRightHeight > leftLeftHeight) {
                    this.rotateRight(node.left);
                }
                node = this.rotateLeft(node);
            } else if (rightHeight > leftHeight && rightHeight - leftHeight == 2) {
                byte rightRightHeight;
                byte rightLeftHeight = node.right.left != null ? node.right.left.height : 0;
                byte by3 = rightRightHeight = node.right.right != null ? node.right.right.height : 0;
                if (rightLeftHeight > rightRightHeight) {
                    this.rotateLeft(node.right);
                }
                node = this.rotateRight(node);
            }
            leftHeight = node.left != null ? node.left.height : 0;
            rightHeight = node.right != null ? node.right.height : 0;
            byte newNodeHeight = (byte)(Math.max(leftHeight, rightHeight) + 1);
            if (!(allTheWayToRoot || node.height != newNodeHeight)) {
                return;
            }
            node.height = newNodeHeight;
            node = node.parent;
        }
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private final SimpleNode<T0> rotateLeft(SimpleNode<T0> subtreeRoot) {
        assert (subtreeRoot.left != null);
        SimpleNode newSubtreeRoot = subtreeRoot.left;
        subtreeRoot.left = newSubtreeRoot.right;
        if (newSubtreeRoot.right != null) {
            newSubtreeRoot.right.parent = subtreeRoot;
        }
        newSubtreeRoot.parent = subtreeRoot.parent;
        if (newSubtreeRoot.parent != null) {
            if (newSubtreeRoot.parent.left == subtreeRoot) {
                newSubtreeRoot.parent.left = newSubtreeRoot;
            } else {
                if (newSubtreeRoot.parent.right != subtreeRoot) throw new IllegalStateException();
                newSubtreeRoot.parent.right = newSubtreeRoot;
            }
        } else {
            this.root = newSubtreeRoot;
        }
        newSubtreeRoot.right = subtreeRoot;
        subtreeRoot.parent = newSubtreeRoot;
        byte subtreeRootLeftHeight = subtreeRoot.left != null ? subtreeRoot.left.height : 0;
        byte subtreeRootRightHeight = subtreeRoot.right != null ? subtreeRoot.right.height : 0;
        subtreeRoot.height = (byte)(Math.max(subtreeRootLeftHeight, subtreeRootRightHeight) + 1);
        subtreeRoot.refreshCounts(!this.zeroQueue.contains(subtreeRoot));
        byte newSubtreeRootLeftHeight = newSubtreeRoot.left != null ? newSubtreeRoot.left.height : 0;
        byte newSubtreeRootRightHeight = newSubtreeRoot.right != null ? newSubtreeRoot.right.height : 0;
        newSubtreeRoot.height = (byte)(Math.max(newSubtreeRootLeftHeight, newSubtreeRootRightHeight) + 1);
        newSubtreeRoot.refreshCounts(!this.zeroQueue.contains(newSubtreeRoot));
        return newSubtreeRoot;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private final SimpleNode<T0> rotateRight(SimpleNode<T0> subtreeRoot) {
        assert (subtreeRoot.right != null);
        SimpleNode newSubtreeRoot = subtreeRoot.right;
        subtreeRoot.right = newSubtreeRoot.left;
        if (newSubtreeRoot.left != null) {
            newSubtreeRoot.left.parent = subtreeRoot;
        }
        newSubtreeRoot.parent = subtreeRoot.parent;
        if (newSubtreeRoot.parent != null) {
            if (newSubtreeRoot.parent.left == subtreeRoot) {
                newSubtreeRoot.parent.left = newSubtreeRoot;
            } else {
                if (newSubtreeRoot.parent.right != subtreeRoot) throw new IllegalStateException();
                newSubtreeRoot.parent.right = newSubtreeRoot;
            }
        } else {
            this.root = newSubtreeRoot;
        }
        newSubtreeRoot.left = subtreeRoot;
        subtreeRoot.parent = newSubtreeRoot;
        byte subtreeRootLeftHeight = subtreeRoot.left != null ? subtreeRoot.left.height : 0;
        byte subtreeRootRightHeight = subtreeRoot.right != null ? subtreeRoot.right.height : 0;
        subtreeRoot.height = (byte)(Math.max(subtreeRootLeftHeight, subtreeRootRightHeight) + 1);
        subtreeRoot.refreshCounts(!this.zeroQueue.contains(subtreeRoot));
        byte newSubtreeRootLeftHeight = newSubtreeRoot.left != null ? newSubtreeRoot.left.height : 0;
        byte newSubtreeRootRightHeight = newSubtreeRoot.right != null ? newSubtreeRoot.right.height : 0;
        newSubtreeRoot.height = (byte)(Math.max(newSubtreeRootLeftHeight, newSubtreeRootRightHeight) + 1);
        newSubtreeRoot.refreshCounts(!this.zeroQueue.contains(newSubtreeRoot));
        return newSubtreeRoot;
    }

    public void remove(Element<T0> element) {
        SimpleNode node = (SimpleNode)element;
        assert (this.root != null);
        this.fixCountsThruRoot(node, -1);
        this.zeroQueue.add(node);
        this.drainZeroQueue();
        assert (this.valid());
    }

    public void remove(int index, int size) {
        if (size == 0) {
            return;
        }
        assert (index >= 0);
        if (!($assertionsDisabled || index + size <= this.size())) {
            throw new AssertionError();
        }
        assert (this.root != null);
        this.removeFromSubtree(this.root, index, size);
        this.drainZeroQueue();
        assert (this.valid());
    }

    private void drainZeroQueue() {
        int size = this.zeroQueue.size();
        for (int i = 0; i < size; ++i) {
            SimpleNode<T0> node = this.zeroQueue.get(i);
            if (node.right == null) {
                this.replaceChild(node, node.left);
                continue;
            }
            if (node.left == null) {
                this.replaceChild(node, node.right);
                continue;
            }
            node = this.replaceEmptyNodeWithChild(node);
        }
        this.zeroQueue.clear();
    }

    private void removeFromSubtree(SimpleNode<T0> node, int index, int size) {
        while (size > 0) {
            int leftSize;
            assert (node != null);
            if (!($assertionsDisabled || index >= 0)) {
                throw new AssertionError();
            }
            SimpleNode nodeLeft = node.left;
            int n = leftSize = nodeLeft != null ? nodeLeft.count1 : 0;
            if (index < leftSize) {
                if (index + size > leftSize) {
                    int toRemove = leftSize - index;
                    this.removeFromSubtree(nodeLeft, index, toRemove);
                    size-=toRemove;
                    leftSize-=toRemove;
                } else {
                    node = nodeLeft;
                    continue;
                }
            }
            assert (index >= leftSize);
            int rightStartIndex = leftSize + 1;
            if (index < rightStartIndex) {
                int toRemove = Math.min(rightStartIndex - index, size);
                rightStartIndex-=toRemove;
                this.fixCountsThruRoot(node, - toRemove);
                this.zeroQueue.add(node);
                if ((size-=toRemove) == 0) {
                    return;
                }
            }
            assert (index >= rightStartIndex);
            index-=rightStartIndex;
            node = node.right;
        }
    }

    private void replaceChild(SimpleNode<T0> node, SimpleNode<T0> replacement) {
        SimpleNode nodeParent = node.parent;
        if (nodeParent == null) {
            assert (node == this.root);
            this.root = replacement;
        } else if (nodeParent.left == node) {
            nodeParent.left = replacement;
        } else if (nodeParent.right == node) {
            nodeParent.right = replacement;
        }
        if (replacement != null) {
            replacement.parent = nodeParent;
        }
        this.fixHeightPostChange(nodeParent, true);
    }

    private SimpleNode<T0> replaceEmptyNodeWithChild(SimpleNode<T0> toReplace) {
        assert (toReplace.left != null);
        if (!($assertionsDisabled || toReplace.right != null)) {
            throw new AssertionError();
        }
        SimpleNode replacement = toReplace.left;
        while (replacement.right != null) {
            replacement = replacement.right;
        }
        assert (replacement.right == null);
        this.fixCountsThruRoot(replacement, -1);
        this.replaceChild(replacement, replacement.left);
        replacement.left = toReplace.left;
        if (replacement.left != null) {
            replacement.left.parent = replacement;
        }
        replacement.right = toReplace.right;
        if (replacement.right != null) {
            replacement.right.parent = replacement;
        }
        replacement.height = toReplace.height;
        replacement.refreshCounts(!this.zeroQueue.contains(replacement));
        this.replaceChild(toReplace, replacement);
        this.fixCountsThruRoot(replacement.parent, 1);
        return replacement;
    }

    public Element<T0> set(int index, T0 value, int size) {
        this.remove(index, size);
        return this.add(index, value, size);
    }

    public void clear() {
        this.root = null;
    }

    public int indexOfNode(Element<T0> element, byte colorsOut) {
        int index;
        SimpleNode node = (SimpleNode)element;
        int n = index = node.left != null ? node.left.count1 : 0;
        while (node.parent != null) {
            if (node.parent.right == node) {
                index+=node.parent.left != null ? node.parent.left.count1 : 0;
                ++index;
            }
            node = node.parent;
        }
        return index;
    }

    public int indexOfValue(T0 element, boolean firstIndex, boolean simulated, byte colorsOut) {
        int result = 0;
        boolean found = false;
        SimpleNode<T0> node = this.root;
        do {
            if (node == null) {
                if (found && !firstIndex) {
                    --result;
                }
                if (found || simulated) {
                    return result;
                }
                return -1;
            }
            int comparison = this.comparator.compare(element, node.get());
            if (comparison < 0) {
                node = node.left;
                continue;
            }
            SimpleNode nodeLeft = node.left;
            if (comparison == 0) {
                found = true;
                if (firstIndex) {
                    node = nodeLeft;
                    continue;
                }
            }
            result+=nodeLeft != null ? nodeLeft.count1 : 0;
            ++result;
            node = node.right;
        } while (true);
    }

    public int convertIndexColor(int index, byte indexColors, byte colorsOut) {
        if (this.root == null) {
            if (index == 0) {
                return 0;
            }
            throw new IndexOutOfBoundsException();
        }
        int result = 0;
        SimpleNode<T0> node = this.root;
        do {
            int size;
            int leftSize;
            assert (node != null);
            if (!($assertionsDisabled || index >= 0)) {
                throw new AssertionError();
            }
            SimpleNode nodeLeft = node.left;
            int n = leftSize = nodeLeft != null ? nodeLeft.count1 : 0;
            if (index < leftSize) {
                node = nodeLeft;
                continue;
            }
            if (nodeLeft != null) {
                result+=nodeLeft.count1;
            }
            if ((index-=leftSize) < (size = 1)) {
                return result+=index;
            }
            ++result;
            index-=size;
            node = node.right;
        } while (true);
    }

    public int size() {
        if (this.root == null) {
            return 0;
        }
        return this.root.count1;
    }

    public String toString() {
        if (this.root == null) {
            return "";
        }
        return this.root.toString();
    }

    public static <T0> SimpleNode<T0> next(SimpleNode<T0> node) {
        if (node.right != null) {
            SimpleNode child = node.right;
            while (child.left != null) {
                child = child.left;
            }
            return child;
        }
        SimpleNode<T0> ancestor = node;
        while (ancestor.parent != null && ancestor.parent.right == ancestor) {
            ancestor = ancestor.parent;
        }
        return ancestor.parent;
    }

    public static <T0> SimpleNode<T0> previous(SimpleNode<T0> node) {
        if (node.left != null) {
            SimpleNode child = node.left;
            while (child.right != null) {
                child = child.right;
            }
            return child;
        }
        SimpleNode<T0> ancestor = node;
        while (ancestor.parent != null && ancestor.parent.left == ancestor) {
            ancestor = ancestor.parent;
        }
        return ancestor.parent;
    }

    SimpleNode<T0> firstNode() {
        if (this.root == null) {
            return null;
        }
        SimpleNode<T0> result = this.root;
        while (result.left != null) {
            result = result.left;
        }
        return result;
    }

    private boolean valid() {
        SimpleNode<T0> node = this.firstNode();
        while (node != null) {
            byte rightHeight;
            int originalCount1 = node.count1;
            node.refreshCounts(!this.zeroQueue.contains(node));
            assert (originalCount1 == node.count1);
            byte leftHeight = node.left != null ? node.left.height : 0;
            byte by = rightHeight = node.right != null ? node.right.height : 0;
            assert (Math.max(leftHeight, rightHeight) + 1 == node.height);
            if (!($assertionsDisabled || node.left == null || node.left.parent == node)) {
                throw new AssertionError();
            }
            assert (node.right == null || node.right.parent == node);
            if (!($assertionsDisabled || Math.abs(leftHeight - rightHeight) < 2)) {
                throw new AssertionError((Object)("Subtree is not AVL: \n" + node));
            }
            node = SimpleTree.next(node);
        }
        return true;
    }

    static final int colorAsIndex(byte color) {
        switch (color) {
            case 1: {
                return 0;
            }
            case 2: {
                return 1;
            }
            case 4: {
                return 2;
            }
            case 8: {
                return 3;
            }
            case 16: {
                return 4;
            }
            case 32: {
                return 5;
            }
            case 64: {
                return 6;
            }
        }
        throw new IllegalArgumentException();
    }
}

