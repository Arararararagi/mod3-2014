/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.adt.barcode2;

public interface Element<V> {
    public static final int SORTED = 0;
    public static final int UNSORTED = 1;
    public static final int PENDING = 2;

    public V get();

    public void set(V var1);

    public byte getColor();

    public void setSorted(int var1);

    public int getSorted();

    public Element<V> next();

    public Element<V> previous();
}

