/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.adt.barcode2;

import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.impl.adt.barcode2.Element;
import ca.odell.glazedlists.impl.adt.barcode2.FourColorNode;
import ca.odell.glazedlists.impl.adt.barcode2.ListToByteCoder;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class FourColorTree<T0> {
    private final ListToByteCoder coder;
    private FourColorNode<T0> root = null;
    private final List<FourColorNode<T0>> zeroQueue = new ArrayList<FourColorNode<T0>>();
    private final Comparator<? super T0> comparator;

    public FourColorTree(ListToByteCoder coder, Comparator<? super T0> comparator) {
        if (coder == null) {
            throw new NullPointerException("Coder cannot be null.");
        }
        if (comparator == null) {
            throw new NullPointerException("Comparator cannot be null.");
        }
        this.coder = coder;
        this.comparator = comparator;
    }

    public FourColorTree(ListToByteCoder coder) {
        this(coder, GlazedLists.comparableComparator());
    }

    public ListToByteCoder getCoder() {
        return this.coder;
    }

    public Comparator<? super T0> getComparator() {
        return this.comparator;
    }

    public Element<T0> get(int index, byte indexColors) {
        if (this.root == null) {
            throw new IndexOutOfBoundsException();
        }
        FourColorNode<T0> node = this.root;
        do {
            int leftSize;
            assert (node != null);
            if (!($assertionsDisabled || index >= 0)) {
                throw new AssertionError();
            }
            FourColorNode nodeLeft = node.left;
            int n = leftSize = nodeLeft != null ? nodeLeft.size(indexColors) : 0;
            if (index < leftSize) {
                node = nodeLeft;
                continue;
            }
            int size = node.nodeSize(indexColors);
            if ((index-=leftSize) < size) {
                return node;
            }
            index-=size;
            node = node.right;
        } while (true);
    }

    public Element<T0> add(int index, byte indexColors, byte color, T0 value, int size) {
        assert (index >= 0);
        if (!($assertionsDisabled || index <= this.size(indexColors))) {
            throw new AssertionError();
        }
        assert (size >= 0);
        if (this.root == null) {
            if (index != 0) {
                throw new IndexOutOfBoundsException();
            }
            this.root = new FourColorNode<T0>(color, size, value, null);
            assert (this.valid());
            return this.root;
        }
        FourColorNode<T0> inserted = this.insertIntoSubtree(this.root, index, indexColors, color, value, size);
        assert (this.valid());
        return inserted;
    }

    private FourColorNode<T0> insertIntoSubtree(FourColorNode<T0> parent, int index, byte indexColors, byte color, T0 value, int size) {
        do {
            assert (parent != null);
            if (!($assertionsDisabled || index >= 0)) {
                throw new AssertionError();
            }
            FourColorNode parentLeft = parent.left;
            int parentLeftSize = parentLeft != null ? parentLeft.size(indexColors) : 0;
            int parentRightStartIndex = parentLeftSize + parent.nodeSize(indexColors);
            if (color == parent.color && value == parent.t0 && value != null && index >= parentLeftSize && index <= parentRightStartIndex) {
                parent.size+=size;
                this.fixCountsThruRoot(parent, color, size);
                return parent;
            }
            if (index <= parentLeftSize) {
                if (parentLeft == null) {
                    FourColorNode<T0> inserted;
                    parent.left = inserted = new FourColorNode<T0>(color, size, value, parent);
                    this.fixCountsThruRoot(parent, color, size);
                    this.fixHeightPostChange(parent, false);
                    return inserted;
                }
                parent = parentLeft;
                continue;
            }
            if (index < parentRightStartIndex) {
                int parentRightHalfSize = parentRightStartIndex - index;
                parent.size-=parentRightHalfSize;
                this.fixCountsThruRoot(parent, parent.color, - parentRightHalfSize);
                FourColorNode<Object> inserted = this.insertIntoSubtree(parent, index, indexColors, parent.color, null, parentRightHalfSize);
                inserted.set(parent.t0);
                parentRightStartIndex = parentLeftSize + parent.nodeSize(indexColors);
            }
            int parentSize = parent.size(indexColors);
            assert (index <= parentSize);
            FourColorNode parentRight = parent.right;
            if (parentRight == null) {
                FourColorNode<T0> inserted;
                parent.right = inserted = new FourColorNode<T0>(color, size, value, parent);
                this.fixCountsThruRoot(parent, color, size);
                this.fixHeightPostChange(parent, false);
                return inserted;
            }
            parent = parentRight;
            index-=parentRightStartIndex;
        } while (true);
    }

    public Element<T0> addInSortedOrder(byte color, T0 value, int size) {
        assert (size >= 0);
        if (this.root == null) {
            this.root = new FourColorNode<T0>(color, size, value, null);
            assert (this.valid());
            return this.root;
        }
        FourColorNode<T0> inserted = this.insertIntoSubtreeInSortedOrder(this.root, color, value, size);
        assert (this.valid());
        return inserted;
    }

    private FourColorNode<T0> insertIntoSubtreeInSortedOrder(FourColorNode<T0> parent, byte color, T0 value, int size) {
        do {
            int sortSide;
            assert (parent != null);
            FourColorNode<T0> currentFollower = parent;
            do {
                if (currentFollower == null) {
                    sortSide = -1;
                    break;
                }
                if (currentFollower.sorted == 0) {
                    sortSide = this.comparator.compare(value, currentFollower.t0);
                    break;
                }
                currentFollower = FourColorTree.next(currentFollower);
            } while (true);
            if (sortSide == 0 && color == parent.color && value == parent.t0 && value != null) {
                parent.size+=size;
                this.fixCountsThruRoot(parent, color, size);
                return parent;
            }
            boolean insertOnLeft = false;
            insertOnLeft = insertOnLeft || sortSide < 0;
            insertOnLeft = insertOnLeft || sortSide == 0 && parent.left == null;
            boolean bl = insertOnLeft = insertOnLeft || sortSide == 0 && parent.right != null && parent.left.height < parent.right.height;
            if (insertOnLeft) {
                FourColorNode parentLeft = parent.left;
                if (parentLeft == null) {
                    FourColorNode<T0> inserted;
                    parent.left = inserted = new FourColorNode<T0>(color, size, value, parent);
                    this.fixCountsThruRoot(parent, color, size);
                    this.fixHeightPostChange(parent, false);
                    return inserted;
                }
                parent = parentLeft;
                continue;
            }
            FourColorNode parentRight = parent.right;
            if (parentRight == null) {
                FourColorNode<T0> inserted;
                parent.right = inserted = new FourColorNode<T0>(color, size, value, parent);
                this.fixCountsThruRoot(parent, color, size);
                this.fixHeightPostChange(parent, false);
                return inserted;
            }
            parent = parentRight;
        } while (true);
    }

    private final void fixCountsThruRoot(FourColorNode<T0> node, byte color, int delta) {
        if (color == 1) {
            while (node != null) {
                node.count1+=delta;
                node = node.parent;
            }
        }
        if (color == 2) {
            while (node != null) {
                node.count2+=delta;
                node = node.parent;
            }
        }
        if (color == 4) {
            while (node != null) {
                node.count4+=delta;
                node = node.parent;
            }
        }
        if (color == 8) {
            while (node != null) {
                node.count8+=delta;
                node = node.parent;
            }
        }
    }

    public final void setColor(Element<T0> element, byte color) {
        FourColorNode node = (FourColorNode)element;
        byte oldColor = node.getColor();
        if (oldColor == color) {
            return;
        }
        this.fixCountsThruRoot(node, oldColor, - node.size);
        node.color = color;
        this.fixCountsThruRoot(node, color, node.size);
    }

    private final void fixHeightPostChange(FourColorNode<T0> node, boolean allTheWayToRoot) {
        while (node != null) {
            byte rightHeight;
            byte leftHeight = node.left != null ? node.left.height : 0;
            byte by = rightHeight = node.right != null ? node.right.height : 0;
            if (leftHeight > rightHeight && leftHeight - rightHeight == 2) {
                byte leftRightHeight;
                byte leftLeftHeight = node.left.left != null ? node.left.left.height : 0;
                byte by2 = leftRightHeight = node.left.right != null ? node.left.right.height : 0;
                if (leftRightHeight > leftLeftHeight) {
                    this.rotateRight(node.left);
                }
                node = this.rotateLeft(node);
            } else if (rightHeight > leftHeight && rightHeight - leftHeight == 2) {
                byte rightRightHeight;
                byte rightLeftHeight = node.right.left != null ? node.right.left.height : 0;
                byte by3 = rightRightHeight = node.right.right != null ? node.right.right.height : 0;
                if (rightLeftHeight > rightRightHeight) {
                    this.rotateLeft(node.right);
                }
                node = this.rotateRight(node);
            }
            leftHeight = node.left != null ? node.left.height : 0;
            rightHeight = node.right != null ? node.right.height : 0;
            byte newNodeHeight = (byte)(Math.max(leftHeight, rightHeight) + 1);
            if (!(allTheWayToRoot || node.height != newNodeHeight)) {
                return;
            }
            node.height = newNodeHeight;
            node = node.parent;
        }
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private final FourColorNode<T0> rotateLeft(FourColorNode<T0> subtreeRoot) {
        assert (subtreeRoot.left != null);
        FourColorNode newSubtreeRoot = subtreeRoot.left;
        subtreeRoot.left = newSubtreeRoot.right;
        if (newSubtreeRoot.right != null) {
            newSubtreeRoot.right.parent = subtreeRoot;
        }
        newSubtreeRoot.parent = subtreeRoot.parent;
        if (newSubtreeRoot.parent != null) {
            if (newSubtreeRoot.parent.left == subtreeRoot) {
                newSubtreeRoot.parent.left = newSubtreeRoot;
            } else {
                if (newSubtreeRoot.parent.right != subtreeRoot) throw new IllegalStateException();
                newSubtreeRoot.parent.right = newSubtreeRoot;
            }
        } else {
            this.root = newSubtreeRoot;
        }
        newSubtreeRoot.right = subtreeRoot;
        subtreeRoot.parent = newSubtreeRoot;
        byte subtreeRootLeftHeight = subtreeRoot.left != null ? subtreeRoot.left.height : 0;
        byte subtreeRootRightHeight = subtreeRoot.right != null ? subtreeRoot.right.height : 0;
        subtreeRoot.height = (byte)(Math.max(subtreeRootLeftHeight, subtreeRootRightHeight) + 1);
        subtreeRoot.refreshCounts();
        byte newSubtreeRootLeftHeight = newSubtreeRoot.left != null ? newSubtreeRoot.left.height : 0;
        byte newSubtreeRootRightHeight = newSubtreeRoot.right != null ? newSubtreeRoot.right.height : 0;
        newSubtreeRoot.height = (byte)(Math.max(newSubtreeRootLeftHeight, newSubtreeRootRightHeight) + 1);
        newSubtreeRoot.refreshCounts();
        return newSubtreeRoot;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private final FourColorNode<T0> rotateRight(FourColorNode<T0> subtreeRoot) {
        assert (subtreeRoot.right != null);
        FourColorNode newSubtreeRoot = subtreeRoot.right;
        subtreeRoot.right = newSubtreeRoot.left;
        if (newSubtreeRoot.left != null) {
            newSubtreeRoot.left.parent = subtreeRoot;
        }
        newSubtreeRoot.parent = subtreeRoot.parent;
        if (newSubtreeRoot.parent != null) {
            if (newSubtreeRoot.parent.left == subtreeRoot) {
                newSubtreeRoot.parent.left = newSubtreeRoot;
            } else {
                if (newSubtreeRoot.parent.right != subtreeRoot) throw new IllegalStateException();
                newSubtreeRoot.parent.right = newSubtreeRoot;
            }
        } else {
            this.root = newSubtreeRoot;
        }
        newSubtreeRoot.left = subtreeRoot;
        subtreeRoot.parent = newSubtreeRoot;
        byte subtreeRootLeftHeight = subtreeRoot.left != null ? subtreeRoot.left.height : 0;
        byte subtreeRootRightHeight = subtreeRoot.right != null ? subtreeRoot.right.height : 0;
        subtreeRoot.height = (byte)(Math.max(subtreeRootLeftHeight, subtreeRootRightHeight) + 1);
        subtreeRoot.refreshCounts();
        byte newSubtreeRootLeftHeight = newSubtreeRoot.left != null ? newSubtreeRoot.left.height : 0;
        byte newSubtreeRootRightHeight = newSubtreeRoot.right != null ? newSubtreeRoot.right.height : 0;
        newSubtreeRoot.height = (byte)(Math.max(newSubtreeRootLeftHeight, newSubtreeRootRightHeight) + 1);
        newSubtreeRoot.refreshCounts();
        return newSubtreeRoot;
    }

    public void remove(Element<T0> element) {
        FourColorNode node = (FourColorNode)element;
        assert (node.size > 0);
        if (!($assertionsDisabled || this.root != null)) {
            throw new AssertionError();
        }
        this.fixCountsThruRoot(node, node.color, - node.size);
        node.size = 0;
        this.zeroQueue.add(node);
        this.drainZeroQueue();
        assert (this.valid());
    }

    public void remove(int index, byte indexColors, int size) {
        if (size == 0) {
            return;
        }
        assert (index >= 0);
        if (!($assertionsDisabled || index + size <= this.size(indexColors))) {
            throw new AssertionError();
        }
        assert (this.root != null);
        this.removeFromSubtree(this.root, index, indexColors, size);
        this.drainZeroQueue();
        assert (this.valid());
    }

    private void drainZeroQueue() {
        int size = this.zeroQueue.size();
        for (int i = 0; i < size; ++i) {
            FourColorNode<T0> node = this.zeroQueue.get(i);
            assert (node.size == 0);
            if (node.right == null) {
                this.replaceChild(node, node.left);
                continue;
            }
            if (node.left == null) {
                this.replaceChild(node, node.right);
                continue;
            }
            node = this.replaceEmptyNodeWithChild(node);
        }
        this.zeroQueue.clear();
    }

    private void removeFromSubtree(FourColorNode<T0> node, int index, byte indexColors, int size) {
        while (size > 0) {
            int leftSize;
            assert (node != null);
            if (!($assertionsDisabled || index >= 0)) {
                throw new AssertionError();
            }
            FourColorNode nodeLeft = node.left;
            int n = leftSize = nodeLeft != null ? nodeLeft.size(indexColors) : 0;
            if (index < leftSize) {
                if (index + size > leftSize) {
                    int toRemove = leftSize - index;
                    this.removeFromSubtree(nodeLeft, index, indexColors, toRemove);
                    size-=toRemove;
                    leftSize-=toRemove;
                } else {
                    node = nodeLeft;
                    continue;
                }
            }
            assert (index >= leftSize);
            int rightStartIndex = leftSize + node.nodeSize(indexColors);
            if (index < rightStartIndex) {
                int toRemove = Math.min(rightStartIndex - index, size);
                node.size-=toRemove;
                size-=toRemove;
                rightStartIndex-=toRemove;
                this.fixCountsThruRoot(node, node.color, - toRemove);
                if (node.size == 0) {
                    this.zeroQueue.add(node);
                }
                if (size == 0) {
                    return;
                }
            }
            assert (index >= rightStartIndex);
            index-=rightStartIndex;
            node = node.right;
        }
    }

    private void replaceChild(FourColorNode<T0> node, FourColorNode<T0> replacement) {
        FourColorNode nodeParent = node.parent;
        if (nodeParent == null) {
            assert (node == this.root);
            this.root = replacement;
        } else if (nodeParent.left == node) {
            nodeParent.left = replacement;
        } else if (nodeParent.right == node) {
            nodeParent.right = replacement;
        }
        if (replacement != null) {
            replacement.parent = nodeParent;
        }
        this.fixHeightPostChange(nodeParent, true);
    }

    private FourColorNode<T0> replaceEmptyNodeWithChild(FourColorNode<T0> toReplace) {
        assert (toReplace.size == 0);
        if (!($assertionsDisabled || toReplace.left != null)) {
            throw new AssertionError();
        }
        assert (toReplace.right != null);
        FourColorNode replacement = toReplace.left;
        while (replacement.right != null) {
            replacement = replacement.right;
        }
        assert (replacement.right == null);
        this.fixCountsThruRoot(replacement, replacement.color, - replacement.size);
        this.replaceChild(replacement, replacement.left);
        replacement.left = toReplace.left;
        if (replacement.left != null) {
            replacement.left.parent = replacement;
        }
        replacement.right = toReplace.right;
        if (replacement.right != null) {
            replacement.right.parent = replacement;
        }
        replacement.height = toReplace.height;
        replacement.refreshCounts();
        this.replaceChild(toReplace, replacement);
        this.fixCountsThruRoot(replacement.parent, replacement.color, replacement.size);
        return replacement;
    }

    public Element<T0> set(int index, byte indexColors, byte color, T0 value, int size) {
        this.remove(index, indexColors, size);
        return this.add(index, indexColors, color, value, size);
    }

    public void clear() {
        this.root = null;
    }

    public int indexOfNode(Element<T0> element, byte colorsOut) {
        int index;
        FourColorNode node = (FourColorNode)element;
        int n = index = node.left != null ? node.left.size(colorsOut) : 0;
        while (node.parent != null) {
            if (node.parent.right == node) {
                index+=node.parent.left != null ? node.parent.left.size(colorsOut) : 0;
                index+=node.parent.nodeSize(colorsOut);
            }
            node = node.parent;
        }
        return index;
    }

    public int indexOfValue(T0 element, boolean firstIndex, boolean simulated, byte colorsOut) {
        int result = 0;
        boolean found = false;
        FourColorNode<T0> node = this.root;
        do {
            if (node == null) {
                if (found && !firstIndex) {
                    --result;
                }
                if (found || simulated) {
                    return result;
                }
                return -1;
            }
            int comparison = this.comparator.compare(element, node.get());
            if (comparison < 0) {
                node = node.left;
                continue;
            }
            FourColorNode nodeLeft = node.left;
            if (comparison == 0) {
                found = true;
                if (firstIndex) {
                    node = nodeLeft;
                    continue;
                }
            }
            result+=nodeLeft != null ? nodeLeft.size(colorsOut) : 0;
            result+=node.nodeSize(colorsOut);
            node = node.right;
        } while (true);
    }

    public int convertIndexColor(int index, byte indexColors, byte colorsOut) {
        if (this.root == null) {
            if (index == 0) {
                return 0;
            }
            throw new IndexOutOfBoundsException();
        }
        int result = 0;
        FourColorNode<T0> node = this.root;
        do {
            int size;
            int leftSize;
            assert (node != null);
            if (!($assertionsDisabled || index >= 0)) {
                throw new AssertionError();
            }
            FourColorNode nodeLeft = node.left;
            int n = leftSize = nodeLeft != null ? nodeLeft.size(indexColors) : 0;
            if (index < leftSize) {
                node = nodeLeft;
                continue;
            }
            if (nodeLeft != null) {
                result+=nodeLeft.size(colorsOut);
            }
            if ((index-=leftSize) < (size = node.nodeSize(indexColors))) {
                result = (colorsOut & node.color) > 0 ? (result+=index) : --result;
                return result;
            }
            result+=node.nodeSize(colorsOut);
            index-=size;
            node = node.right;
        } while (true);
    }

    public int size(byte colors) {
        if (this.root == null) {
            return 0;
        }
        return this.root.size(colors);
    }

    public String toString() {
        if (this.root == null) {
            return "";
        }
        return this.root.toString(this.coder.getColors());
    }

    public String asSequenceOfColors() {
        if (this.root == null) {
            return "";
        }
        StringBuffer result = new StringBuffer();
        FourColorNode<T0> n = this.firstNode();
        while (n != null) {
            Object color = this.coder.getColors().get(FourColorTree.colorAsIndex(n.color));
            for (int i = 0; i < n.size; ++i) {
                result.append(color);
            }
            n = FourColorTree.next(n);
        }
        return result.toString();
    }

    public static <T0> FourColorNode<T0> next(FourColorNode<T0> node) {
        if (node.right != null) {
            FourColorNode child = node.right;
            while (child.left != null) {
                child = child.left;
            }
            return child;
        }
        FourColorNode<T0> ancestor = node;
        while (ancestor.parent != null && ancestor.parent.right == ancestor) {
            ancestor = ancestor.parent;
        }
        return ancestor.parent;
    }

    public static <T0> FourColorNode<T0> previous(FourColorNode<T0> node) {
        if (node.left != null) {
            FourColorNode child = node.left;
            while (child.right != null) {
                child = child.right;
            }
            return child;
        }
        FourColorNode<T0> ancestor = node;
        while (ancestor.parent != null && ancestor.parent.left == ancestor) {
            ancestor = ancestor.parent;
        }
        return ancestor.parent;
    }

    FourColorNode<T0> firstNode() {
        if (this.root == null) {
            return null;
        }
        FourColorNode<T0> result = this.root;
        while (result.left != null) {
            result = result.left;
        }
        return result;
    }

    private boolean valid() {
        FourColorNode<T0> node = this.firstNode();
        while (node != null) {
            byte rightHeight;
            int originalCount1 = node.count1;
            int originalCount2 = node.count2;
            int originalCount4 = node.count4;
            int originalCount8 = node.count8;
            node.refreshCounts();
            assert (originalCount1 == node.count1);
            if (!($assertionsDisabled || originalCount2 == node.count2)) {
                throw new AssertionError((Object)("Incorrect count 1 on node: \n" + node + "\n Expected " + node.count2 + " but was " + originalCount2));
            }
            assert (originalCount4 == node.count4);
            if (!($assertionsDisabled || originalCount8 == node.count8)) {
                throw new AssertionError((Object)("Incorrect count 3 on node: \n" + node + "\n Expected " + node.count8 + " but was " + originalCount8));
            }
            byte leftHeight = node.left != null ? node.left.height : 0;
            byte by = rightHeight = node.right != null ? node.right.height : 0;
            assert (Math.max(leftHeight, rightHeight) + 1 == node.height);
            if (!($assertionsDisabled || node.left == null || node.left.parent == node)) {
                throw new AssertionError();
            }
            assert (node.right == null || node.right.parent == node);
            if (!($assertionsDisabled || Math.abs(leftHeight - rightHeight) < 2)) {
                throw new AssertionError((Object)("Subtree is not AVL: \n" + node));
            }
            node = FourColorTree.next(node);
        }
        return true;
    }

    static final int colorAsIndex(byte color) {
        switch (color) {
            case 1: {
                return 0;
            }
            case 2: {
                return 1;
            }
            case 4: {
                return 2;
            }
            case 8: {
                return 3;
            }
            case 16: {
                return 4;
            }
            case 32: {
                return 5;
            }
            case 64: {
                return 6;
            }
        }
        throw new IllegalArgumentException();
    }
}

