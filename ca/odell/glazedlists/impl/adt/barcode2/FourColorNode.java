/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.adt.barcode2;

import ca.odell.glazedlists.impl.adt.barcode2.Element;
import ca.odell.glazedlists.impl.adt.barcode2.FourColorTree;
import java.util.Arrays;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
class FourColorNode<T0>
implements Element<T0> {
    int count1;
    int count2;
    int count4;
    int count8;
    byte color;
    T0 t0;
    int size;
    byte height;
    FourColorNode<T0> left;
    FourColorNode<T0> right;
    FourColorNode<T0> parent;
    int sorted = 0;

    public FourColorNode(byte color, int size, T0 value, FourColorNode<T0> parent) {
        assert (FourColorTree.colorAsIndex(color) >= 0 && FourColorTree.colorAsIndex(color) < 7);
        this.color = color;
        this.size = size;
        this.t0 = value;
        this.height = 1;
        this.parent = parent;
        if (color == 1) {
            this.count1+=size;
        }
        if (color == 2) {
            this.count2+=size;
        }
        if (color == 4) {
            this.count4+=size;
        }
        if (color == 8) {
            this.count8+=size;
        }
    }

    @Override
    public T0 get() {
        return this.t0;
    }

    @Override
    public void set(T0 value) {
        this.t0 = value;
    }

    public T0 get0() {
        return this.t0;
    }

    public void set0(T0 value) {
        this.t0 = value;
    }

    @Override
    public byte getColor() {
        return this.color;
    }

    final int size(byte colors) {
        int result = 0;
        if ((colors & 1) != 0) {
            result+=this.count1;
        }
        if ((colors & 2) != 0) {
            result+=this.count2;
        }
        if ((colors & 4) != 0) {
            result+=this.count4;
        }
        if ((colors & 8) != 0) {
            result+=this.count8;
        }
        return result;
    }

    final int nodeSize(byte colors) {
        return (colors & this.color) > 0 ? this.size : 0;
    }

    final void refreshCounts() {
        this.count1 = 0;
        this.count2 = 0;
        this.count4 = 0;
        this.count8 = 0;
        if (this.left != null) {
            this.count1+=this.left.count1;
            this.count2+=this.left.count2;
            this.count4+=this.left.count4;
            this.count8+=this.left.count8;
        }
        if (this.right != null) {
            this.count1+=this.right.count1;
            this.count2+=this.right.count2;
            this.count4+=this.right.count4;
            this.count8+=this.right.count8;
        }
        if (this.color == 1) {
            this.count1+=this.size;
        }
        if (this.color == 2) {
            this.count2+=this.size;
        }
        if (this.color == 4) {
            this.count4+=this.size;
        }
        if (this.color == 8) {
            this.count8+=this.size;
        }
    }

    public String toString() {
        return this.toString(Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H"));
    }

    String toString(List colors) {
        StringBuffer result = new StringBuffer();
        this.asTree(0, result, colors);
        return result.toString();
    }

    void asTree(int indentation, StringBuffer out, List colors) {
        if (this.left != null) {
            this.left.asTree(indentation + 1, out, colors);
        }
        for (int i = 0; i < indentation; ++i) {
            out.append("   ");
        }
        out.append(colors.get(FourColorTree.colorAsIndex(this.color)));
        out.append(" [").append(this.size).append("]");
        if (this.t0 != null) {
            out.append(": ");
            if (this.t0 instanceof FourColorNode) {
                out.append("<Node>");
            } else {
                out.append(this.t0);
            }
        }
        out.append("\n");
        if (this.right != null) {
            this.right.asTree(indentation + 1, out, colors);
        }
    }

    @Override
    public void setSorted(int sorted) {
        this.sorted = sorted;
    }

    @Override
    public int getSorted() {
        return this.sorted;
    }

    @Override
    public Element<T0> next() {
        return FourColorTree.next(this);
    }

    @Override
    public Element<T0> previous() {
        return FourColorTree.previous(this);
    }
}

