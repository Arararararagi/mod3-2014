/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.adt.barcode2;

import ca.odell.glazedlists.impl.adt.barcode2.Element;
import ca.odell.glazedlists.impl.adt.barcode2.SimpleTree;
import java.util.Arrays;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
class SimpleNode<T0>
implements Element<T0> {
    int count1;
    T0 t0;
    byte height;
    SimpleNode<T0> left;
    SimpleNode<T0> right;
    SimpleNode<T0> parent;
    int sorted = 0;

    public SimpleNode(int size, T0 value, SimpleNode<T0> parent) {
        assert (size == 1);
        this.t0 = value;
        this.height = 1;
        this.parent = parent;
        this.count1+=size;
    }

    @Override
    public T0 get() {
        return this.t0;
    }

    @Override
    public void set(T0 value) {
        this.t0 = value;
    }

    public T0 get0() {
        return this.t0;
    }

    public void set0(T0 value) {
        this.t0 = value;
    }

    @Override
    public byte getColor() {
        return 1;
    }

    final int size(byte colors) {
        int result = 0;
        if ((colors & 1) != 0) {
            result+=this.count1;
        }
        return result;
    }

    final void refreshCounts(boolean countSelf) {
        this.count1 = 0;
        if (this.left != null) {
            this.count1+=this.left.count1;
        }
        if (this.right != null) {
            this.count1+=this.right.count1;
        }
        this.count1+=countSelf ? 1 : 0;
    }

    public String toString() {
        return this.toString(Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H"));
    }

    String toString(List colors) {
        StringBuffer result = new StringBuffer();
        this.asTree(0, result, colors);
        return result.toString();
    }

    void asTree(int indentation, StringBuffer out, List colors) {
        if (this.left != null) {
            this.left.asTree(indentation + 1, out, colors);
        }
        for (int i = 0; i < indentation; ++i) {
            out.append("   ");
        }
        if (this.t0 != null) {
            out.append(": ");
            if (this.t0 instanceof SimpleNode) {
                out.append("<Node>");
            } else {
                out.append(this.t0);
            }
        }
        out.append("\n");
        if (this.right != null) {
            this.right.asTree(indentation + 1, out, colors);
        }
    }

    @Override
    public void setSorted(int sorted) {
        this.sorted = sorted;
    }

    @Override
    public int getSorted() {
        return this.sorted;
    }

    @Override
    public Element<T0> next() {
        return SimpleTree.next(this);
    }

    @Override
    public Element<T0> previous() {
        return SimpleTree.previous(this);
    }
}

