/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.adt.barcode2;

import ca.odell.glazedlists.impl.adt.barcode2.Element;
import ca.odell.glazedlists.impl.adt.barcode2.FourColorNode;
import ca.odell.glazedlists.impl.adt.barcode2.FourColorTree;
import java.util.NoSuchElementException;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class FourColorTreeIterator<T0> {
    int count1;
    int count2;
    int count4;
    int count8;
    private FourColorTree<T0> tree;
    private FourColorNode<T0> node;
    private int index;

    public FourColorTreeIterator(FourColorTree<T0> tree) {
        this(tree, 0, 0);
    }

    public FourColorTreeIterator(FourColorTree<T0> tree, int nextIndex, byte nextIndexColors) {
        this.tree = tree;
        if (nextIndex != 0) {
            int currentIndex = nextIndex - 1;
            this.node = (FourColorNode)tree.get(currentIndex, nextIndexColors);
            this.count1 = tree.convertIndexColor(currentIndex, nextIndexColors, 1) + (this.node.color == 1 ? 0 : 1);
            this.count2 = tree.convertIndexColor(currentIndex, nextIndexColors, 2) + (this.node.color == 2 ? 0 : 1);
            this.count4 = tree.convertIndexColor(currentIndex, nextIndexColors, 4) + (this.node.color == 4 ? 0 : 1);
            this.count8 = tree.convertIndexColor(currentIndex, nextIndexColors, 8) + (this.node.color == 8 ? 0 : 1);
            if (this.node.color == 1) {
                this.index = this.count1 - tree.indexOfNode(this.node, 1);
            }
            if (this.node.color == 2) {
                this.index = this.count2 - tree.indexOfNode(this.node, 2);
            }
            if (this.node.color == 4) {
                this.index = this.count4 - tree.indexOfNode(this.node, 4);
            }
            if (this.node.color == 8) {
                this.index = this.count8 - tree.indexOfNode(this.node, 8);
            }
        } else {
            this.node = null;
            this.index = 0;
        }
    }

    public FourColorTreeIterator<T0> copy() {
        FourColorTreeIterator<T0> result = new FourColorTreeIterator<T0>(this.tree);
        result.count1 = this.count1;
        result.count2 = this.count2;
        result.count4 = this.count4;
        result.count8 = this.count8;
        result.node = this.node;
        result.index = this.index;
        return result;
    }

    public boolean hasNext(byte colors) {
        if (this.node == null) {
            return this.tree.size(colors) > 0;
        }
        if ((colors & this.node.color) != 0) {
            return this.index(colors) < this.tree.size(colors) - 1;
        }
        return this.index(colors) < this.tree.size(colors);
    }

    public boolean hasNextNode(byte colors) {
        if (this.node == null) {
            return this.tree.size(colors) > 0;
        }
        return this.nodeEndIndex(colors) < this.tree.size(colors);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public void next(byte colors) {
        if (!this.hasNext(colors)) {
            throw new NoSuchElementException();
        }
        if (this.node == null) {
            this.node = this.tree.firstNode();
            this.index = 0;
            if ((this.node.color & colors) != 0) {
                return;
            }
        } else if ((this.node.color & colors) != 0 && this.index < this.node.size - 1) {
            if (this.node.color == 1) {
                ++this.count1;
            }
            if (this.node.color == 2) {
                ++this.count2;
            }
            if (this.node.color == 4) {
                ++this.count4;
            }
            if (this.node.color == 8) {
                ++this.count8;
            }
            ++this.index;
            return;
        }
        do {
            if (this.node.color == 1) {
                this.count1+=this.node.size - this.index;
            }
            if (this.node.color == 2) {
                this.count2+=this.node.size - this.index;
            }
            if (this.node.color == 4) {
                this.count4+=this.node.size - this.index;
            }
            if (this.node.color == 8) {
                this.count8+=this.node.size - this.index;
            }
            this.node = FourColorTree.next(this.node);
            this.index = 0;
        } while ((this.node.color & colors) == 0);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public void nextNode(byte colors) {
        if (!this.hasNextNode(colors)) {
            throw new NoSuchElementException();
        }
        if (this.node == null) {
            this.node = this.tree.firstNode();
            this.index = 0;
            if ((this.node.color & colors) != 0) {
                return;
            }
        }
        do {
            if (this.node.color == 1) {
                this.count1+=this.node.size - this.index;
            }
            if (this.node.color == 2) {
                this.count2+=this.node.size - this.index;
            }
            if (this.node.color == 4) {
                this.count4+=this.node.size - this.index;
            }
            if (this.node.color == 8) {
                this.count8+=this.node.size - this.index;
            }
            this.node = FourColorTree.next(this.node);
            this.index = 0;
        } while ((this.node.color & colors) == 0);
    }

    public int nodeSize(byte colors) {
        if ((this.node.color & colors) != 0) {
            return this.node.size;
        }
        return 0;
    }

    public byte color() {
        if (this.node == null) {
            throw new IllegalStateException();
        }
        return this.node.color;
    }

    public int index(byte colors) {
        if (this.node == null) {
            throw new NoSuchElementException();
        }
        int result = 0;
        if ((colors & 1) != 0) {
            result+=this.count1;
        }
        if ((colors & 2) != 0) {
            result+=this.count2;
        }
        if ((colors & 4) != 0) {
            result+=this.count4;
        }
        if ((colors & 8) != 0) {
            result+=this.count8;
        }
        return result;
    }

    public int nodeStartIndex(byte colors) {
        if (this.node == null) {
            throw new NoSuchElementException();
        }
        int result = 0;
        if ((colors & 1) != 0) {
            result+=this.count1;
        }
        if ((colors & 2) != 0) {
            result+=this.count2;
        }
        if ((colors & 4) != 0) {
            result+=this.count4;
        }
        if ((colors & 8) != 0) {
            result+=this.count8;
        }
        if ((this.node.color & colors) != 0) {
            result-=this.index;
        }
        return result;
    }

    public int nodeEndIndex(byte colors) {
        if (this.node == null) {
            throw new NoSuchElementException();
        }
        return this.nodeStartIndex(colors) + this.nodeSize(colors);
    }

    public T0 value() {
        if (this.node == null) {
            throw new IllegalStateException();
        }
        return this.node.get();
    }

    public Element<T0> node() {
        if (this.node == null) {
            throw new IllegalStateException();
        }
        return this.node;
    }
}

