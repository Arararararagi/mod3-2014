/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.adt.barcode2;

import ca.odell.glazedlists.impl.adt.barcode2.Element;
import ca.odell.glazedlists.impl.adt.barcode2.SimpleNode;
import ca.odell.glazedlists.impl.adt.barcode2.SimpleTree;
import java.util.NoSuchElementException;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class SimpleTreeIterator<T0> {
    int count1;
    private SimpleTree<T0> tree;
    private SimpleNode<T0> node;
    private int index;

    public SimpleTreeIterator(SimpleTree<T0> tree) {
        this(tree, 0, 0);
    }

    public SimpleTreeIterator(SimpleTree<T0> tree, int nextIndex, byte nextIndexColors) {
        this.tree = tree;
        if (nextIndex != 0) {
            int currentIndex = nextIndex - 1;
            this.node = (SimpleNode)tree.get(currentIndex);
            this.count1 = currentIndex;
            this.index = this.count1 - tree.indexOfNode(this.node, 1);
        } else {
            this.node = null;
            this.index = 0;
        }
    }

    public SimpleTreeIterator<T0> copy() {
        SimpleTreeIterator<T0> result = new SimpleTreeIterator<T0>(this.tree);
        result.count1 = this.count1;
        result.node = this.node;
        result.index = this.index;
        return result;
    }

    public boolean hasNext() {
        if (this.node == null) {
            return this.tree.size() > 0;
        }
        return this.index() < this.tree.size() - 1;
    }

    public boolean hasNextNode() {
        if (this.node == null) {
            return this.tree.size() > 0;
        }
        return this.nodeEndIndex() < this.tree.size();
    }

    public void next() {
        if (!this.hasNext()) {
            throw new NoSuchElementException();
        }
        if (this.node == null) {
            this.node = this.tree.firstNode();
            this.index = 0;
            return;
        }
        if (this.index < 0) {
            ++this.count1;
            ++this.index;
            return;
        }
        this.count1+=1 - this.index;
        this.node = SimpleTree.next(this.node);
        this.index = 0;
    }

    public void nextNode() {
        if (!this.hasNextNode()) {
            throw new NoSuchElementException();
        }
        if (this.node == null) {
            this.node = this.tree.firstNode();
            this.index = 0;
            return;
        }
        this.count1+=1 - this.index;
        this.node = SimpleTree.next(this.node);
        this.index = 0;
    }

    public int nodeSize() {
        return 1;
    }

    public int index() {
        if (this.node == null) {
            throw new NoSuchElementException();
        }
        int result = 0;
        return result+=this.count1;
    }

    public int nodeStartIndex() {
        if (this.node == null) {
            throw new NoSuchElementException();
        }
        int result = 0;
        result+=this.count1;
        return result-=this.index;
    }

    public int nodeEndIndex() {
        if (this.node == null) {
            throw new NoSuchElementException();
        }
        return this.nodeStartIndex() + this.nodeSize();
    }

    public T0 value() {
        if (this.node == null) {
            throw new IllegalStateException();
        }
        return this.node.get();
    }

    public Element<T0> node() {
        if (this.node == null) {
            throw new IllegalStateException();
        }
        return this.node;
    }
}

