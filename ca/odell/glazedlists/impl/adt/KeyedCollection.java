/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.adt;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class KeyedCollection<P, V> {
    private final Map<V, Object> values;
    private final Comparator<P> positionComparator;
    private P first;
    private P last;

    public KeyedCollection(Comparator<P> positionComparator, Map<V, Object> mapWithValuesAsKeys) {
        if (!mapWithValuesAsKeys.isEmpty()) {
            throw new IllegalArgumentException("mapWithValuesAsKeys must be empty");
        }
        this.positionComparator = positionComparator;
        this.values = mapWithValuesAsKeys;
    }

    public void insert(P position, V value) {
        Object previousPositions = this.values.get(value);
        if (previousPositions == null) {
            this.values.put(value, position);
        } else if (previousPositions instanceof SortedSet) {
            SortedSet allPositions = (SortedSet)previousPositions;
            allPositions.add(position);
        } else {
            TreeSet<P> allPositions = new TreeSet<P>(this.positionComparator);
            allPositions.add((Object)previousPositions);
            allPositions.add(position);
            this.values.put((TreeSet<P>)value, allPositions);
        }
        if (this.first == null || this.lessThan(position, this.first)) {
            this.first = position;
        }
        if (this.last == null || this.greaterThan(position, this.last)) {
            this.last = position;
        }
    }

    public P find(P min, P max, V value) {
        if (this.positionComparator.compare(min, max) > 0) {
            throw new IllegalArgumentException("min " + min + " > max " + max);
        }
        Object positionsAsSingleOrSet = this.values.get(value);
        if (positionsAsSingleOrSet == null) {
            return null;
        }
        if (positionsAsSingleOrSet instanceof SortedSet) {
            SortedSet positions = (SortedSet)positionsAsSingleOrSet;
            SortedSet<P> positionsInRange = positions.subSet(min, max);
            return positionsInRange.isEmpty() ? null : (P)positionsInRange.iterator().next();
        }
        Object position = positionsAsSingleOrSet;
        if (!this.lessThan(position, min) && this.lessThan(position, max)) {
            return (P)position;
        }
        return null;
    }

    public P last() {
        return this.last;
    }

    public P first() {
        return this.first;
    }

    private boolean lessThan(P a, P b) {
        return this.positionComparator.compare(a, b) < 0;
    }

    private boolean greaterThan(P a, P b) {
        return this.positionComparator.compare(a, b) > 0;
    }
}

