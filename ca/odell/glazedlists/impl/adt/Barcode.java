/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.adt;

import ca.odell.glazedlists.impl.adt.BarcodeIterator;
import ca.odell.glazedlists.impl.adt.BarcodeNode;
import java.io.PrintStream;

public final class Barcode {
    public static final Object WHITE = Boolean.FALSE;
    public static final Object BLACK = Boolean.TRUE;
    private BarcodeNode root = null;
    private int whiteSpace = 0;
    private int treeSize = 0;

    public void printDebug() {
        System.out.println("\nTotal Size: " + this.size());
        System.out.println("Trailing Whitespace : " + this.whiteSpace);
        System.out.println("Tree Size: " + this.treeSize);
        System.out.println("Tree Structure:\n" + this.root);
    }

    public void validate() {
        if (this.root != null) {
            this.root.validate();
        }
    }

    public int size() {
        return this.treeSize + this.whiteSpace;
    }

    public boolean isEmpty() {
        return this.size() == 0;
    }

    public int whiteSize() {
        return this.root == null ? this.whiteSpace : this.root.whiteSize() + this.whiteSpace;
    }

    public int blackSize() {
        return this.root == null ? 0 : this.root.blackSize();
    }

    public int colourSize(Object colour) {
        if (colour == WHITE) {
            return this.whiteSize();
        }
        return this.blackSize();
    }

    public void add(int index, Object colour, int length) {
        if (colour == WHITE) {
            this.addWhite(index, length);
        } else {
            this.addBlack(index, length);
        }
    }

    public void addWhite(int index, int length) {
        if (length < 0) {
            throw new IllegalStateException();
        }
        if (length == 0) {
            return;
        }
        if (this.root == null || index >= this.treeSize) {
            this.whiteSpace+=length;
        } else {
            this.root.insertWhite(index, length);
            this.treeSizeChanged();
        }
    }

    public void addBlack(int index, int length) {
        if (length < 0) {
            throw new IllegalArgumentException();
        }
        if (length == 0) {
            return;
        }
        if (this.root == null) {
            this.root = new BarcodeNode(this, null, length, index);
            this.treeSize = index + length;
            this.whiteSpace-=index;
        } else if (index >= this.treeSize) {
            int movingWhitespace = index - this.treeSize;
            this.whiteSpace-=movingWhitespace;
            this.root.insertBlackAtEnd(length, movingWhitespace);
            this.treeSizeChanged();
        } else {
            this.root.insertBlack(index, length);
            this.treeSizeChanged();
        }
    }

    public Object get(int index) {
        if (this.getBlackIndex(index) == -1) {
            return WHITE;
        }
        return BLACK;
    }

    public void set(int index, Object colour, int length) {
        int trailingChange;
        if (length < 1) {
            throw new IllegalArgumentException();
        }
        int n = trailingChange = index > this.treeSize - 1 ? length : index + length - this.treeSize;
        if (trailingChange > 0) {
            if (colour == BLACK) {
                this.whiteSpace-=trailingChange;
                this.addBlack(index, trailingChange);
            }
            if ((length-=trailingChange) == 0) {
                return;
            }
        }
        if (this.root != null) {
            this.root.set(index, colour, length);
            if (this.root != null) {
                this.treeSizeChanged();
            }
        }
    }

    public void setWhite(int index, int length) {
        this.set(index, WHITE, length);
    }

    public void setBlack(int index, int length) {
        this.set(index, BLACK, length);
    }

    public void remove(int index, int length) {
        int trailingChange;
        if (length < 1) {
            throw new IllegalArgumentException();
        }
        int n = trailingChange = index > this.treeSize ? length : index + length - this.treeSize;
        if (trailingChange > 0) {
            this.whiteSpace-=trailingChange;
            length-=trailingChange;
        }
        if (this.root != null && index < this.treeSize) {
            int oldTreeSize = -1;
            while (length > 0) {
                oldTreeSize = this.treeSize;
                this.root.remove(index, length);
                if (this.root != null) {
                    this.treeSizeChanged();
                }
                length-=oldTreeSize - this.treeSize;
            }
            if (this.root != null) {
                this.treeSizeChanged();
            }
        }
    }

    public void clear() {
        this.treeSize = 0;
        this.whiteSpace = 0;
        this.root = null;
    }

    BarcodeNode getRootNode() {
        return this.root;
    }

    void setRootNode(BarcodeNode root) {
        this.root = root;
        if (root == null) {
            this.treeSize = 0;
        }
    }

    int treeSize() {
        return this.treeSize;
    }

    void treeSizeChanged() {
        this.treeSize = this.root.size();
    }

    public int getIndex(int colourIndex, Object colour) {
        if (colour == WHITE) {
            if (this.root == null) {
                return colourIndex;
            }
            if (colourIndex >= this.root.whiteSize()) {
                return colourIndex - this.root.whiteSize() + this.treeSize;
            }
            return this.root.getIndexByWhiteIndex(colourIndex);
        }
        return this.root.getIndexByBlackIndex(colourIndex);
    }

    public int getColourIndex(int index, Object colour) {
        if (colour == WHITE) {
            return this.getWhiteIndex(index);
        }
        return this.getBlackIndex(index);
    }

    public int getWhiteIndex(int index) {
        if (this.root != null && index < this.treeSize) {
            return this.root.getWhiteIndex(index);
        }
        if (this.root != null) {
            return index - this.treeSize + this.root.whiteSize();
        }
        return index;
    }

    public int getBlackIndex(int index) {
        if (this.root != null && index < this.treeSize) {
            return this.root.getBlackIndex(index);
        }
        return -1;
    }

    public int getColourIndex(int index, boolean left, Object colour) {
        if (colour == WHITE) {
            return this.getWhiteIndex(index, left);
        }
        return this.getBlackIndex(index, left);
    }

    public int getWhiteIndex(int index, boolean left) {
        if (this.root == null) {
            return index;
        }
        if (index >= this.treeSize) {
            return index - this.treeSize + this.root.whiteSize();
        }
        return this.root.getWhiteIndex(index, left);
    }

    public int getBlackIndex(int index, boolean left) {
        if (this.root == null) {
            if (left) {
                return -1;
            }
            return 0;
        }
        if (index >= this.treeSize) {
            if (left) {
                return this.root.blackSize() - 1;
            }
            return this.root.blackSize();
        }
        return this.root.getBlackIndex(index, left);
    }

    public int getWhiteSequenceIndex(int whiteIndex) {
        if (this.root == null) {
            return whiteIndex;
        }
        if (whiteIndex >= this.root.whiteSize()) {
            return whiteIndex - this.root.whiteSize();
        }
        return this.root.getWhiteSequenceIndex(whiteIndex);
    }

    public int getBlackBeforeWhite(int whiteIndex) {
        if (this.root == null) {
            return -1;
        }
        if (whiteIndex >= this.root.whiteSize()) {
            return this.root.blackSize() - 1;
        }
        return this.root.getBlackBeforeWhite(whiteIndex);
    }

    public int findSequenceOfMinimumSize(int size, Object colour) {
        if (this.root == null) {
            if (colour == BLACK) {
                return -1;
            }
            if (this.whiteSpace >= size) {
                return 0;
            }
            return -1;
        }
        if (colour == BLACK) {
            return this.root.findSequenceOfMinimumSize(size, colour);
        }
        int result = this.root.findSequenceOfMinimumSize(size, colour);
        if (result == -1 && this.whiteSpace >= size) {
            result = this.treeSize;
        }
        return result;
    }

    public BarcodeIterator iterator() {
        return new BarcodeIterator(this);
    }

    public String toString() {
        StringBuffer result = new StringBuffer();
        BarcodeIterator bi = this.iterator();
        while (bi.hasNext()) {
            result.append(bi.next() == BLACK ? "X" : "_");
        }
        return result.toString();
    }
}

