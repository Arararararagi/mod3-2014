/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.adt.gnutrove;

import java.util.Arrays;
import java.util.Random;

public class TIntArrayList
implements Cloneable {
    protected transient int[] _data = new int[capacity];
    protected transient int _pos = 0;
    protected static final int DEFAULT_CAPACITY = 10;

    public TIntArrayList() {
        this(10);
    }

    public TIntArrayList(int capacity) {
    }

    public TIntArrayList(int[] values) {
        this(Math.max(values.length, 10));
        this.add(values);
    }

    public void ensureCapacity(int capacity) {
        if (capacity > this._data.length) {
            int newCap = Math.max(this._data.length << 1, capacity);
            int[] tmp = new int[newCap];
            System.arraycopy(this._data, 0, tmp, 0, this._data.length);
            this._data = tmp;
        }
    }

    public int size() {
        return this._pos;
    }

    public boolean isEmpty() {
        return this._pos == 0;
    }

    public void trimToSize() {
        if (this._data.length > this.size()) {
            int[] tmp = new int[this.size()];
            this.toNativeArray(tmp, 0, tmp.length);
            this._data = tmp;
        }
    }

    public void add(int val) {
        this.ensureCapacity(this._pos + 1);
        this._data[this._pos++] = val;
    }

    public void add(int[] vals) {
        this.add(vals, 0, vals.length);
    }

    public void add(int[] vals, int offset, int length) {
        this.ensureCapacity(this._pos + length);
        System.arraycopy(vals, offset, this._data, this._pos, length);
        this._pos+=length;
    }

    public void insert(int offset, int value) {
        if (offset == this._pos) {
            this.add(value);
            return;
        }
        this.ensureCapacity(this._pos + 1);
        System.arraycopy(this._data, offset, this._data, offset + 1, this._pos - offset);
        this._data[offset] = value;
        ++this._pos;
    }

    public void insert(int offset, int[] values) {
        this.insert(offset, values, 0, values.length);
    }

    public void insert(int offset, int[] values, int valOffset, int len) {
        if (offset == this._pos) {
            this.add(values, valOffset, len);
            return;
        }
        this.ensureCapacity(this._pos + len);
        System.arraycopy(this._data, offset, this._data, offset + len, this._pos - offset);
        System.arraycopy(values, valOffset, this._data, offset, len);
        this._pos+=len;
    }

    public int get(int offset) {
        if (offset >= this._pos) {
            throw new ArrayIndexOutOfBoundsException(offset);
        }
        return this._data[offset];
    }

    public int getQuick(int offset) {
        return this._data[offset];
    }

    public void set(int offset, int val) {
        if (offset >= this._pos) {
            throw new ArrayIndexOutOfBoundsException(offset);
        }
        this._data[offset] = val;
    }

    public int getSet(int offset, int val) {
        if (offset >= this._pos) {
            throw new ArrayIndexOutOfBoundsException(offset);
        }
        int old = this._data[offset];
        this._data[offset] = val;
        return old;
    }

    public void set(int offset, int[] values) {
        this.set(offset, values, 0, values.length);
    }

    public void set(int offset, int[] values, int valOffset, int length) {
        if (offset < 0 || offset + length > this._pos) {
            throw new ArrayIndexOutOfBoundsException(offset);
        }
        System.arraycopy(values, valOffset, this._data, offset, length);
    }

    public void setQuick(int offset, int val) {
        this._data[offset] = val;
    }

    public void clear() {
        this.clear(10);
    }

    public void clear(int capacity) {
        this._data = new int[capacity];
        this._pos = 0;
    }

    public void reset() {
        this._pos = 0;
        this.fill(0);
    }

    public void resetQuick() {
        this._pos = 0;
    }

    public int remove(int offset) {
        int old = this.get(offset);
        this.remove(offset, 1);
        return old;
    }

    public void remove(int offset, int length) {
        if (offset < 0 || offset >= this._pos) {
            throw new ArrayIndexOutOfBoundsException(offset);
        }
        if (offset == 0) {
            System.arraycopy(this._data, length, this._data, 0, this._pos - length);
        } else if (this._pos - length != offset) {
            System.arraycopy(this._data, offset + length, this._data, offset, this._pos - (offset + length));
        }
        this._pos-=length;
    }

    public void reverse() {
        this.reverse(0, this._pos);
    }

    public void reverse(int from, int to) {
        if (from == to) {
            return;
        }
        if (from > to) {
            throw new IllegalArgumentException("from cannot be greater than to");
        }
        int i = from;
        for (int j = to - 1; i < j; ++i, --j) {
            this.swap(i, j);
        }
    }

    public void shuffle(Random rand) {
        int i = this._pos;
        while (i-- > 1) {
            this.swap(i, rand.nextInt(i));
        }
    }

    private final void swap(int i, int j) {
        int tmp = this._data[i];
        this._data[i] = this._data[j];
        this._data[j] = tmp;
    }

    public Object clone() {
        TIntArrayList clone = null;
        try {
            clone = (TIntArrayList)super.clone();
            clone._data = (int[])this._data.clone();
        }
        catch (CloneNotSupportedException e) {
            // empty catch block
        }
        return clone;
    }

    public int[] toNativeArray() {
        return this.toNativeArray(0, this._pos);
    }

    public int[] toNativeArray(int offset, int len) {
        int[] rv = new int[len];
        this.toNativeArray(rv, offset, len);
        return rv;
    }

    public void toNativeArray(int[] dest, int offset, int len) {
        if (len == 0) {
            return;
        }
        if (offset < 0 || offset >= this._pos) {
            throw new ArrayIndexOutOfBoundsException(offset);
        }
        System.arraycopy(this._data, offset, dest, 0, len);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof TIntArrayList) {
            TIntArrayList that = (TIntArrayList)other;
            if (that.size() != this.size()) {
                return false;
            }
            int i = this._pos;
            while (i-- > 0) {
                if (this._data[i] == that._data[i]) continue;
                return false;
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        int h = 0;
        int i = this._pos;
        while (i-- > 0) {
            h+=this._data[i];
        }
        return h;
    }

    public void sort() {
        Arrays.sort(this._data, 0, this._pos);
    }

    public void sort(int fromIndex, int toIndex) {
        Arrays.sort(this._data, fromIndex, toIndex);
    }

    public void fill(int val) {
        Arrays.fill(this._data, 0, this._pos, val);
    }

    public void fill(int fromIndex, int toIndex, int val) {
        if (toIndex > this._pos) {
            this.ensureCapacity(toIndex);
            this._pos = toIndex;
        }
        Arrays.fill(this._data, fromIndex, toIndex, val);
    }

    public int binarySearch(int value) {
        return this.binarySearch(value, 0, this._pos);
    }

    public int binarySearch(int value, int fromIndex, int toIndex) {
        if (fromIndex < 0) {
            throw new ArrayIndexOutOfBoundsException(fromIndex);
        }
        if (toIndex > this._pos) {
            throw new ArrayIndexOutOfBoundsException(toIndex);
        }
        int low = fromIndex;
        int high = toIndex - 1;
        while (low <= high) {
            int mid = low + high >> 1;
            int midVal = this._data[mid];
            if (midVal < value) {
                low = mid + 1;
                continue;
            }
            if (midVal > value) {
                high = mid - 1;
                continue;
            }
            return mid;
        }
        return - low + 1;
    }

    public int indexOf(int value) {
        return this.indexOf(0, value);
    }

    public int indexOf(int offset, int value) {
        for (int i = offset; i < this._pos; ++i) {
            if (this._data[i] != value) continue;
            return i;
        }
        return -1;
    }

    public int lastIndexOf(int value) {
        return this.lastIndexOf(this._pos, value);
    }

    public int lastIndexOf(int offset, int value) {
        int i = offset;
        while (i-- > 0) {
            if (this._data[i] != value) continue;
            return i;
        }
        return -1;
    }

    public boolean contains(int value) {
        return this.lastIndexOf(value) >= 0;
    }

    public int max() {
        if (this.size() == 0) {
            throw new IllegalStateException("cannot find maximum of an empty list");
        }
        int max = this._data[this._pos - 1];
        int i = this._pos - 1;
        while (i-- > 0) {
            if (this._data[i] <= max) continue;
            max = this._data[i];
        }
        return max;
    }

    public int min() {
        if (this.size() == 0) {
            throw new IllegalStateException("cannot find minimum of an empty list");
        }
        int min = this._data[this._pos - 1];
        int i = this._pos - 1;
        while (i-- > 0) {
            if (this._data[i] >= min) continue;
            min = this._data[i];
        }
        return min;
    }

    public String toString() {
        StringBuffer buf = new StringBuffer("{");
        int end = this._pos - 1;
        for (int i = 0; i < end; ++i) {
            buf.append(this._data[i]);
            buf.append(", ");
        }
        if (this.size() > 0) {
            buf.append(this._data[this._pos - 1]);
        }
        buf.append("}");
        return buf.toString();
    }
}

