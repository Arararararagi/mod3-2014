/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.adt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class IdentityMultimap<K, V>
extends IdentityHashMap<K, List<V>> {
    public void addValue(K key, V value) {
        ArrayList<V> values = (ArrayList<V>)super.get(key);
        if (values == null) {
            values = new ArrayList<V>(2);
            this.put(key, values);
        }
        values.add(value);
    }

    @Override
    public List<V> get(Object key) {
        List values = (List)super.get(key);
        return values == null ? Collections.EMPTY_LIST : values;
    }

    public int count(Object key) {
        List values = (List)super.get(key);
        return values == null ? 0 : values.size();
    }
}

