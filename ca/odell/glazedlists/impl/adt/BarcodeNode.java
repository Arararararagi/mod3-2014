/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.adt;

import ca.odell.glazedlists.impl.adt.Barcode;

final class BarcodeNode {
    BarcodeNode parent;
    private Barcode host;
    BarcodeNode left = null;
    BarcodeNode right = null;
    private int blackLeftSize = 0;
    private int blackRightSize = 0;
    private int treeLeftSize = 0;
    private int treeRightSize = 0;
    int whiteSpace = 0;
    int rootSize = 1;
    private int height = 1;

    private BarcodeNode(Barcode host, BarcodeNode parent) {
        this.host = host;
        this.parent = parent;
    }

    BarcodeNode(Barcode host, BarcodeNode parent, int values, int whiteSpace) {
        this(host, parent);
        this.whiteSpace = whiteSpace;
        this.rootSize = values;
    }

    int size() {
        return this.treeLeftSize + this.whiteSpace + this.rootSize + this.treeRightSize;
    }

    int blackSize() {
        return this.blackLeftSize + this.rootSize + this.blackRightSize;
    }

    int whiteSize() {
        return this.treeLeftSize - this.blackLeftSize + this.whiteSpace + (this.treeRightSize - this.blackRightSize);
    }

    void insertBlack(int index, int length) {
        int localIndex = index - this.treeLeftSize;
        if (localIndex < 0) {
            this.blackLeftSize+=length;
            this.treeLeftSize+=length;
            this.left.insertBlack(index, length);
        } else if (localIndex > this.whiteSpace + this.rootSize) {
            this.blackRightSize+=length;
            this.treeRightSize+=length;
            this.right.insertBlack(localIndex - this.whiteSpace - this.rootSize, length);
        } else if (localIndex == this.whiteSpace + this.rootSize) {
            this.rootSize+=length;
        } else if (localIndex < this.whiteSpace) {
            this.whiteSpace-=localIndex;
            this.blackLeftSize+=length;
            this.treeLeftSize+=localIndex + length;
            if (this.left == null) {
                this.left = new BarcodeNode(this.host, this, length, localIndex);
                this.ensureAVL();
            } else {
                this.left.insertBlackAtEnd(length, localIndex);
            }
        } else {
            this.rootSize+=length;
        }
    }

    void insertBlackAtEnd(int values, int leadingWhite) {
        if (this.right != null) {
            this.blackRightSize+=values;
            this.treeRightSize+=values + leadingWhite;
            this.right.insertBlackAtEnd(values, leadingWhite);
        } else if (leadingWhite == 0) {
            this.rootSize+=values;
        } else {
            this.blackRightSize+=values;
            this.treeRightSize+=values + leadingWhite;
            this.right = new BarcodeNode(this.host, this, values, leadingWhite);
            this.ensureAVL();
        }
    }

    void insertWhite(int index, int length) {
        int localIndex = index - this.treeLeftSize;
        if (localIndex < 0) {
            this.treeLeftSize+=length;
            this.left.insertWhite(index, length);
        } else if (localIndex > this.whiteSpace + this.rootSize - 1) {
            this.treeRightSize+=length;
            this.right.insertWhite(localIndex - this.whiteSpace - this.rootSize, length);
        } else if (localIndex <= this.whiteSpace) {
            this.whiteSpace+=length;
        } else {
            int movingRoot = this.rootSize - (localIndex-=this.whiteSpace);
            this.rootSize = localIndex;
            this.blackRightSize+=movingRoot;
            this.treeRightSize+=movingRoot + length;
            if (this.right == null) {
                this.right = new BarcodeNode(this.host, this, movingRoot, length);
                this.ensureAVL();
            } else {
                BarcodeNode node = new BarcodeNode(this.host, null, movingRoot, length);
                this.right.moveToSmallest(node);
            }
        }
    }

    private void moveToSmallest(BarcodeNode movingNode) {
        if (this.left != null) {
            this.blackLeftSize+=movingNode.rootSize;
            this.treeLeftSize+=movingNode.whiteSpace + movingNode.rootSize;
            this.left.moveToSmallest(movingNode);
        } else if (this.whiteSpace == 0) {
            this.rootSize+=movingNode.rootSize;
            this.whiteSpace+=movingNode.whiteSpace;
            movingNode.clear();
        } else {
            this.blackLeftSize+=movingNode.rootSize;
            this.treeLeftSize+=movingNode.whiteSpace + movingNode.rootSize;
            movingNode.parent = this;
            this.left = movingNode;
            this.ensureAVL();
        }
    }

    int getWhiteIndex(int index) {
        return this.getWhiteIndex(index, 0);
    }

    private int getWhiteIndex(int index, int accumulation) {
        int localIndex = index - this.treeLeftSize;
        if (localIndex < 0) {
            return this.left.getWhiteIndex(index, accumulation);
        }
        if (localIndex > this.whiteSpace + this.rootSize - 1) {
            return this.right.getWhiteIndex(localIndex - this.whiteSpace - this.rootSize, accumulation+=this.treeLeftSize - this.blackLeftSize + this.whiteSpace);
        }
        if (localIndex < this.whiteSpace) {
            return accumulation + (this.treeLeftSize - this.blackLeftSize) + localIndex;
        }
        return -1;
    }

    int getBlackIndex(int index) {
        return this.getBlackIndex(index, 0);
    }

    private int getBlackIndex(int index, int accumulation) {
        int localIndex = index - this.treeLeftSize;
        if (localIndex < 0) {
            return this.left.getBlackIndex(index, accumulation);
        }
        if (localIndex > this.whiteSpace + this.rootSize - 1) {
            return this.right.getBlackIndex(localIndex - this.whiteSpace - this.rootSize, accumulation + this.blackLeftSize + this.rootSize);
        }
        if (localIndex < this.whiteSpace) {
            return -1;
        }
        return accumulation + this.blackLeftSize + localIndex - this.whiteSpace;
    }

    public int getWhiteIndex(int index, boolean lead) {
        int localIndex = index - this.treeLeftSize;
        if (localIndex < 0) {
            return this.left.getWhiteIndex(index, lead);
        }
        if (localIndex > this.whiteSpace + this.rootSize - 1) {
            return this.right.getWhiteIndex(localIndex - this.whiteSpace - this.rootSize, lead) + this.treeLeftSize - this.blackLeftSize + this.whiteSpace;
        }
        if (localIndex < this.whiteSpace) {
            return this.treeLeftSize - this.blackLeftSize + localIndex;
        }
        if (lead) {
            return this.treeLeftSize - this.blackLeftSize + this.whiteSpace - 1;
        }
        return this.treeLeftSize - this.blackLeftSize + this.whiteSpace;
    }

    public int getBlackIndex(int index, boolean lead) {
        int localIndex = index - this.treeLeftSize;
        if (localIndex < 0) {
            return this.left.getBlackIndex(index, lead);
        }
        if (localIndex > this.whiteSpace + this.rootSize - 1) {
            return this.right.getBlackIndex(localIndex - this.whiteSpace - this.rootSize, lead) + this.blackLeftSize + this.rootSize;
        }
        if (localIndex < this.whiteSpace) {
            if (lead) {
                return this.blackLeftSize - 1;
            }
            return this.blackLeftSize;
        }
        return this.blackLeftSize + localIndex - this.whiteSpace;
    }

    public int getIndexByWhiteIndex(int whiteIndex) {
        int localIndex = whiteIndex - (this.treeLeftSize - this.blackLeftSize);
        if (localIndex < 0) {
            return this.left.getIndexByWhiteIndex(whiteIndex);
        }
        if (localIndex >= this.whiteSpace) {
            return this.right.getIndexByWhiteIndex(localIndex - this.whiteSpace) + this.treeLeftSize + this.whiteSpace + this.rootSize;
        }
        return this.treeLeftSize + localIndex;
    }

    public int getIndexByBlackIndex(int blackIndex) {
        int localIndex = blackIndex - this.blackLeftSize;
        if (localIndex < 0) {
            return this.left.getIndexByBlackIndex(blackIndex);
        }
        if (localIndex >= this.rootSize) {
            return this.right.getIndexByBlackIndex(localIndex - this.rootSize) + this.treeLeftSize + this.whiteSpace + this.rootSize;
        }
        return this.treeLeftSize + this.whiteSpace + localIndex;
    }

    public int getWhiteSequenceIndex(int whiteIndex) {
        int localIndex = whiteIndex - (this.treeLeftSize - this.blackLeftSize);
        if (localIndex < 0) {
            return this.left.getWhiteSequenceIndex(whiteIndex);
        }
        if (localIndex >= this.whiteSpace) {
            return this.right.getWhiteSequenceIndex(localIndex - this.whiteSpace);
        }
        return localIndex;
    }

    public int getBlackBeforeWhite(int whiteIndex) {
        int localIndex = whiteIndex - (this.treeLeftSize - this.blackLeftSize);
        if (localIndex < 0) {
            return this.left.getBlackBeforeWhite(whiteIndex);
        }
        if (localIndex >= this.whiteSpace) {
            return this.right.getBlackBeforeWhite(localIndex - this.whiteSpace) + this.blackLeftSize + this.rootSize;
        }
        return this.blackLeftSize - 1;
    }

    public int findSequenceOfMinimumSize(int size, Object colour) {
        return this.findFirstFitSequence(size, colour, 0);
    }

    private int findFirstFitSequence(int size, Object colour, int accumulation) {
        int result = -1;
        if (this.left != null) {
            result = this.left.findFirstFitSequence(size, colour, accumulation);
        }
        if (result == -1) {
            if (colour == Barcode.WHITE && size <= this.whiteSpace) {
                return accumulation + this.treeLeftSize;
            }
            if (colour == Barcode.BLACK && size <= this.rootSize) {
                return accumulation + this.treeLeftSize + this.whiteSpace;
            }
        }
        if (result == -1 && this.right != null) {
            result = this.right.findFirstFitSequence(size, colour, accumulation + this.treeLeftSize + this.whiteSpace + this.rootSize);
        }
        return result;
    }

    void set(int index, Object value, int length) {
        if (length == 1) {
            this.setBaseCase(index, index, value);
        } else {
            this.set(index, index, value, length);
        }
    }

    private void set(int absoluteIndex, int localIndex, Object value, int length) {
        int localizedIndex = localIndex - this.treeLeftSize;
        if (localizedIndex < 0) {
            this.left.set(absoluteIndex, localIndex, value, length);
        } else if (localizedIndex > this.whiteSpace + this.rootSize - 1) {
            this.right.set(absoluteIndex, localizedIndex - this.whiteSpace - this.rootSize, value, length);
        } else if (value == Barcode.WHITE) {
            this.setWhite(absoluteIndex, localizedIndex, length);
        } else {
            this.setBlack(absoluteIndex, localizedIndex, length);
        }
    }

    void setWhite(int absoluteIndex, int localIndex, int length) {
        int endIndex = localIndex + length - 1;
        if (endIndex >= this.whiteSpace) {
            if (localIndex > this.whiteSpace - 1) {
                int rootChange = Math.min(length, this.whiteSpace + this.rootSize - localIndex);
                if (this.rootSize == rootChange) {
                    this.whiteSpace+=rootChange;
                    this.rootSize = 0;
                    this.correctSizes(- rootChange, 0);
                    this.unlink(absoluteIndex - localIndex);
                } else {
                    this.rootSize-=rootChange;
                    if (localIndex < this.whiteSpace + this.rootSize) {
                        this.correctSizes(- rootChange, 0);
                        this.insertWhite(localIndex + this.treeLeftSize, rootChange);
                    } else {
                        this.correctSizes(- rootChange, - rootChange);
                        this.host.addWhite(absoluteIndex, rootChange);
                    }
                }
                if (rootChange != length) {
                    this.host.remove(absoluteIndex + rootChange, length - rootChange);
                    this.host.addWhite(absoluteIndex + rootChange, length - rootChange);
                }
            } else if (localIndex < this.whiteSpace + 1 && endIndex < this.whiteSpace + this.rootSize) {
                int rootChange = Math.min(length, this.whiteSpace + this.rootSize - localIndex) + (localIndex - this.whiteSpace);
                this.rootSize-=rootChange;
                this.whiteSpace+=rootChange;
                this.correctSizes(- rootChange, 0);
            } else {
                this.whiteSpace+=this.rootSize;
                int localLength = this.whiteSpace - localIndex;
                this.unlink(absoluteIndex - localIndex);
                if (localLength != length) {
                    this.host.remove(absoluteIndex + localLength, length - localLength);
                    this.host.addWhite(absoluteIndex + localLength, length - localLength);
                }
            }
        }
    }

    void setBlack(int absoluteIndex, int localIndex, int length) {
        int endIndex = localIndex + length - 1;
        int localLength = Math.min(length, this.whiteSpace + this.rootSize - localIndex);
        if (localIndex <= this.whiteSpace - 1) {
            if (endIndex > this.whiteSpace - 1) {
                int whiteChange = this.whiteSpace - localIndex;
                this.rootSize+=whiteChange;
                this.whiteSpace-=whiteChange;
                this.correctSizes(whiteChange, 0);
                this.compressNode(absoluteIndex - localIndex);
            } else {
                this.whiteSpace-=length;
                this.correctSizes(0, - length);
                this.host.addBlack(absoluteIndex, length);
                this.compressNode(absoluteIndex - localIndex);
            }
        }
        if (localLength != length) {
            this.host.remove(absoluteIndex + localLength, length - localLength);
            this.host.addBlack(absoluteIndex + localLength, length - localLength);
        }
    }

    private void setBaseCase(int absoluteIndex, int index, Object value) {
        int localIndex = index - this.treeLeftSize;
        if (localIndex < 0) {
            this.left.setBaseCase(absoluteIndex, index, value);
        } else if (localIndex > this.whiteSpace + this.rootSize) {
            this.right.setBaseCase(absoluteIndex, localIndex - this.whiteSpace - this.rootSize, value);
        } else if (localIndex == this.whiteSpace + this.rootSize) {
            if (value != Barcode.WHITE) {
                ++this.rootSize;
                --this.treeRightSize;
                this.correctSizes(1, 0);
                this.right.setFirstNullToTrue(absoluteIndex, localIndex - this.whiteSpace - this.rootSize + 1);
            }
        } else if (localIndex < this.whiteSpace) {
            if (value == Barcode.WHITE) {
                return;
            }
            --this.whiteSpace;
            this.correctSizes(1, 0);
            this.insertBlack(index, 1);
            this.compressNode(absoluteIndex);
        } else if (localIndex == this.whiteSpace) {
            if (value == Barcode.WHITE) {
                ++this.whiteSpace;
                --this.rootSize;
                this.correctSizes(-1, 0);
                if (this.rootSize == 0) {
                    this.unlink(absoluteIndex - localIndex);
                }
            }
        } else if (localIndex == this.whiteSpace + this.rootSize - 1) {
            if (value == Barcode.WHITE) {
                --this.rootSize;
                if (this.right != null) {
                    ++this.treeRightSize;
                    this.right.insertWhite(localIndex - this.whiteSpace - this.rootSize, 1);
                    this.correctSizes(-1, 0);
                } else if (this.parent != null && this.parent.left == this) {
                    ++this.parent.whiteSpace;
                    --this.parent.treeLeftSize;
                    this.parent.correctSizes(true, -1, 0);
                } else {
                    this.correctSizes(-1, -1);
                    this.host.addWhite(absoluteIndex, 1);
                }
            }
        } else if (value == Barcode.WHITE) {
            --this.rootSize;
            this.correctSizes(-1, 0);
            this.insertWhite(index, 1);
        }
    }

    private void setFirstNullToTrue(int absoluteIndex, int index) {
        int localIndex = index - this.treeLeftSize;
        if (localIndex < 0) {
            --this.treeLeftSize;
            this.left.setFirstNullToTrue(absoluteIndex, index);
        } else if (localIndex > this.whiteSpace + this.rootSize - 1) {
            --this.treeRightSize;
            this.right.setFirstNullToTrue(absoluteIndex, localIndex - this.whiteSpace - this.rootSize);
        } else {
            --this.whiteSpace;
            this.compressNode(absoluteIndex);
        }
    }

    void remove(int index, int length) {
        if (length == 1) {
            this.removeBaseCase(index, index);
        } else {
            this.remove(index, index, length);
        }
    }

    private void remove(int absoluteIndex, int index, int length) {
        int localIndex = index - this.treeLeftSize;
        if (localIndex < 0) {
            this.left.remove(absoluteIndex, index, length);
        } else if (localIndex > this.whiteSpace + this.rootSize - 1) {
            this.right.remove(absoluteIndex, localIndex - this.whiteSpace - this.rootSize, length);
        } else {
            length = Math.min(localIndex + length, this.whiteSpace + this.rootSize) - localIndex;
            int endIndex = localIndex + length - 1;
            if (localIndex < this.whiteSpace && endIndex < this.whiteSpace + this.rootSize) {
                int whiteChange = Math.min(this.whiteSpace - localIndex, length);
                int blackChange = Math.max(endIndex - this.whiteSpace + 1, 0);
                this.whiteSpace-=whiteChange;
                this.rootSize-=blackChange;
                this.correctSizes(- blackChange, - whiteChange + blackChange);
                this.compressNode(absoluteIndex - localIndex);
            } else if (localIndex > this.whiteSpace - 1) {
                if (length == this.rootSize) {
                    this.unlink(absoluteIndex - localIndex);
                } else {
                    this.rootSize-=length;
                    this.correctSizes(- length, - length);
                }
            } else {
                int whiteChange = this.whiteSpace;
                int blackChange = this.rootSize;
                this.whiteSpace = 0;
                this.rootSize = 0;
                this.correctSizes(- blackChange, - whiteChange + blackChange);
                this.unlink(absoluteIndex - localIndex);
            }
        }
    }

    void removeBaseCase(int absoluteIndex, int index) {
        int localIndex = index - this.treeLeftSize;
        if (localIndex < 0) {
            --this.treeLeftSize;
            this.left.removeBaseCase(absoluteIndex, index);
        } else if (localIndex > this.whiteSpace + this.rootSize - 1) {
            --this.treeRightSize;
            this.right.removeBaseCase(absoluteIndex, localIndex - this.whiteSpace - this.rootSize);
        } else if (localIndex < this.whiteSpace) {
            --this.whiteSpace;
            this.compressNode(absoluteIndex);
        } else {
            --this.rootSize;
            if (this.rootSize == 0) {
                this.rootSize = 1;
                this.unlink(absoluteIndex - localIndex, false);
            } else {
                this.correctSizes(-1, 0);
            }
        }
    }

    private void unlink(int absoluteIndex) {
        this.unlink(absoluteIndex, true);
    }

    private void unlink(int absoluteIndex, boolean consistent) {
        if (this.right != null && this.left != null) {
            if (this.rootSize != 0) {
                this.correctSizes(- this.rootSize, - this.rootSize, consistent);
            }
            this.unlinkWithTwoChildren();
        } else if (this.right != null) {
            this.unlinkWithRightChild(consistent);
        } else {
            BarcodeNode replacement = null;
            if (this.left != null) {
                replacement = this.left;
                replacement.parent = this.parent;
            } else {
                replacement = null;
            }
            if (this.parent == null) {
                this.host.setRootNode(replacement);
                if (this.whiteSpace != 0) {
                    this.host.addWhite(this.host.size() + 1, this.whiteSpace);
                }
            } else if (this.parent.left == this) {
                this.parent.whiteSpace+=this.whiteSpace;
                this.parent.treeLeftSize-=this.whiteSpace;
                this.parent.left = replacement;
                this.parent.ensureAVL();
                if (this.rootSize != 0) {
                    this.parent.correctSizes(true, - this.rootSize, - this.rootSize, consistent);
                }
                this.clear();
            } else {
                this.parent.right = replacement;
                this.parent.ensureAVL();
                if (this.whiteSpace != 0) {
                    this.parent.correctSizes(false, - this.rootSize, - this.whiteSpace + this.rootSize, consistent);
                    this.host.addWhite(absoluteIndex, this.whiteSpace);
                } else if (this.rootSize != 0) {
                    this.parent.correctSizes(false, - this.rootSize, - this.rootSize, consistent);
                }
                this.clear();
            }
        }
    }

    private void unlinkWithTwoChildren() {
        BarcodeNode replacement = this.right.pruneSmallestChild();
        BarcodeNode repParent = replacement.parent;
        this.whiteSpace+=replacement.whiteSpace;
        this.rootSize = replacement.rootSize;
        this.treeRightSize-=replacement.whiteSpace + replacement.rootSize;
        this.blackRightSize-=replacement.rootSize;
        if (repParent == this) {
            this.right = replacement.right;
            if (this.right != null) {
                this.right.parent = this;
            }
            this.ensureAVL();
        } else {
            repParent.left = replacement.right;
            if (repParent.left != null) {
                repParent.left.parent = repParent;
            }
            repParent.ensureAVL();
        }
        replacement.clear();
    }

    private void unlinkWithRightChild(boolean consistent) {
        this.whiteSpace+=this.right.whiteSpace;
        int oldSize = this.rootSize;
        this.rootSize = this.right.rootSize;
        this.right.clear();
        this.right = null;
        this.blackRightSize = 0;
        this.treeRightSize = 0;
        this.height = 1;
        if (this.parent != null) {
            if (oldSize != 0) {
                this.parent.correctSizes(this.parent.left == this, - oldSize, - oldSize, consistent);
            }
            this.parent.ensureAVL();
        }
    }

    private BarcodeNode pruneSmallestChild() {
        if (this.left != null) {
            BarcodeNode prunedNode = this.left.pruneSmallestChild();
            this.blackLeftSize-=prunedNode.rootSize;
            this.treeLeftSize-=prunedNode.whiteSpace + prunedNode.rootSize;
            return prunedNode;
        }
        return this;
    }

    private void correctSizes(int blackOffset, int totalOffset, boolean consistent) {
        if (consistent) {
            this.correctSizes(blackOffset, totalOffset);
        } else {
            this.correctSizes(-1, totalOffset - blackOffset);
        }
    }

    private void correctSizes(boolean leftChild, int blackOffset, int totalOffset, boolean consistent) {
        if (consistent) {
            this.correctSizes(leftChild, blackOffset, totalOffset);
        } else {
            this.correctSizes(leftChild, -1, totalOffset - blackOffset);
        }
    }

    private void correctSizes(int blackOffset, int totalOffset) {
        if (this.parent != null) {
            this.parent.correctSizes(this.parent.left == this, blackOffset, totalOffset);
        } else {
            this.host.treeSizeChanged();
        }
    }

    private void correctSizes(boolean leftChild, int blackOffset, int totalOffset) {
        if (leftChild) {
            this.blackLeftSize+=blackOffset;
            this.treeLeftSize+=totalOffset;
        } else {
            this.blackRightSize+=blackOffset;
            this.treeRightSize+=totalOffset;
        }
        if (this.parent != null) {
            this.parent.correctSizes(this.parent.left == this, blackOffset, totalOffset);
        } else {
            this.host.treeSizeChanged();
        }
    }

    private void clear() {
        this.left = null;
        this.blackLeftSize = 0;
        this.treeLeftSize = 0;
        this.right = null;
        this.blackRightSize = 0;
        this.treeRightSize = 0;
        this.host = null;
        this.parent = null;
        this.whiteSpace = 0;
        this.rootSize = 0;
        this.height = -1;
    }

    private void replace(BarcodeNode child, BarcodeNode replacement) {
        if (child == this.left) {
            this.left = replacement;
        } else {
            this.right = replacement;
        }
    }

    private void compressNode(int absoluteIndex) {
        if (this.whiteSpace != 0) {
            return;
        }
        if (this.parent == null) {
            this.compressRoot(absoluteIndex);
        } else if (this.parent.left == this) {
            this.compressLeftChild(absoluteIndex);
        } else {
            this.compressRightChild(absoluteIndex);
        }
    }

    private void compressRoot(int absoluteIndex) {
        if (this.left != null) {
            if (this.right == null) {
                this.left.rootSize+=this.rootSize;
                this.left.parent = null;
                this.host.setRootNode(this.left);
                this.clear();
            } else {
                this.left.compressToTheRight(this.rootSize);
                this.blackLeftSize+=this.rootSize;
                this.treeLeftSize+=this.rootSize;
                this.rootSize = 0;
                this.unlink(absoluteIndex);
            }
        }
    }

    private void compressLeftChild(int absoluteIndex) {
        if (this.left != null) {
            this.left.compressToTheRight(this.rootSize);
            this.blackLeftSize+=this.rootSize;
            this.treeLeftSize+=this.rootSize;
            this.rootSize = 0;
            this.unlink(absoluteIndex);
        } else {
            if (absoluteIndex == 0) {
                return;
            }
            this.parent.left = this.right;
            if (this.right != null) {
                this.parent.left.parent = this.parent;
            }
            this.parent.correctSizes(true, - this.rootSize, - this.rootSize);
            this.parent.ensureAVL();
            this.host.addBlack(absoluteIndex - 1, this.rootSize);
            this.clear();
        }
    }

    private void compressRightChild(int absoluteIndex) {
        if (this.left == null) {
            this.parent.blackRightSize-=this.rootSize;
            this.parent.treeRightSize-=this.rootSize;
            this.parent.rootSize+=this.rootSize;
            this.rootSize = 0;
            this.unlink(absoluteIndex);
        } else {
            this.left.compressToTheRight(this.rootSize);
            this.blackLeftSize+=this.rootSize;
            this.treeLeftSize+=this.rootSize;
            this.rootSize = 0;
            this.unlink(absoluteIndex);
        }
    }

    private void compressToTheRight(int values) {
        if (this.right != null) {
            this.blackRightSize+=values;
            this.treeRightSize+=values;
            this.right.compressToTheRight(values);
        } else {
            this.rootSize+=values;
        }
    }

    private void ensureAVL() {
        int oldHeight = this.height;
        this.recalculateHeight();
        this.avlRotate();
        if (this.height != oldHeight && this.parent != null) {
            this.parent.ensureAVL();
        }
    }

    private void recalculateHeight() {
        int leftHeight = this.left == null ? 0 : this.left.height;
        int rightHeight = this.right == null ? 0 : this.right.height;
        this.height = 1 + Math.max(leftHeight, rightHeight);
    }

    private void avlRotate() {
        int rightHeight;
        int leftHeight = this.left != null ? this.left.height : 0;
        int n = rightHeight = this.right != null ? this.right.height : 0;
        if (leftHeight - rightHeight >= 2) {
            int leftRightHeight;
            int leftLeftHeight = this.left.left != null ? this.left.left.height : 0;
            int n2 = leftRightHeight = this.left.right != null ? this.left.right.height : 0;
            if (leftRightHeight > leftLeftHeight) {
                this.left.rotateRight();
            }
            this.rotateLeft();
        } else if (rightHeight - leftHeight >= 2) {
            int rightRightHeight;
            int rightLeftHeight = this.right.left != null ? this.right.left.height : 0;
            int n3 = rightRightHeight = this.right.right != null ? this.right.right.height : 0;
            if (rightLeftHeight > rightRightHeight) {
                this.right.rotateLeft();
            }
            this.rotateRight();
        }
    }

    private void rotateLeft() {
        BarcodeNode replacement = this.left;
        this.left = replacement.right;
        this.blackLeftSize = replacement.blackRightSize;
        this.treeLeftSize = replacement.treeRightSize;
        if (replacement.right != null) {
            replacement.right.parent = this;
        }
        replacement.right = this;
        replacement.blackRightSize = this.blackSize();
        replacement.treeRightSize = this.size();
        if (this.parent != null) {
            this.parent.replace(this, replacement);
        } else {
            this.host.setRootNode(replacement);
        }
        replacement.parent = this.parent;
        this.parent = replacement;
        this.recalculateHeight();
        replacement.height = 0;
    }

    private void rotateRight() {
        BarcodeNode replacement = this.right;
        this.right = replacement.left;
        this.blackRightSize = replacement.blackLeftSize;
        this.treeRightSize = replacement.treeLeftSize;
        if (replacement.left != null) {
            replacement.left.parent = this;
        }
        replacement.left = this;
        replacement.blackLeftSize = this.blackSize();
        replacement.treeLeftSize = this.size();
        if (this.parent != null) {
            this.parent.replace(this, replacement);
        } else {
            this.host.setRootNode(replacement);
        }
        replacement.parent = this.parent;
        this.parent = replacement;
        this.recalculateHeight();
        replacement.height = 0;
    }

    public String toString() {
        return "[ " + this.left + " (" + this.blackLeftSize + ", " + this.treeLeftSize + ")" + " <" + this.whiteSpace + "> " + this.rootSize + " <" + this.height + "> " + "(" + this.blackRightSize + ", " + this.treeRightSize + ") " + this.right + " ]";
    }

    public void validate() {
        this.validateLineage();
        this.validateHeight();
        this.validateTreeSize();
        this.validateBlackSize();
        this.validateCompression();
        this.validateRootSize();
    }

    private int validateBlackSize() {
        int rightTreeSize;
        int leftTreeSize = this.left == null ? 0 : this.left.validateBlackSize();
        int n = rightTreeSize = this.right == null ? 0 : this.right.validateBlackSize();
        if (leftTreeSize != this.blackLeftSize) {
            throw new IllegalStateException("Black Size Validation Failure in Left Subtree\nExpected: " + leftTreeSize + "\nActual: " + this.blackLeftSize + "\n" + this);
        }
        if (rightTreeSize != this.blackRightSize) {
            throw new IllegalStateException("Black Size Validation Failure in Right Subtree\nExpected: " + rightTreeSize + "\nActual: " + this.blackRightSize + "\n" + this);
        }
        return leftTreeSize + rightTreeSize + this.rootSize;
    }

    private int validateHeight() {
        int rightHeight;
        int leftHeight = this.left == null ? 0 : this.left.validateHeight();
        int n = rightHeight = this.right == null ? 0 : this.right.validateHeight();
        if (this.height != 1 + Math.max(leftHeight, rightHeight)) {
            throw new IllegalStateException("Height Validation Failure\nExpected: " + (1 + Math.max(leftHeight, rightHeight)) + "\nActual: " + this.height + "\n" + this);
        }
        if (Math.abs(leftHeight - rightHeight) > 1) {
            throw new IllegalStateException("AVL Property Validation Failure\n" + this);
        }
        return 1 + Math.max(leftHeight, rightHeight);
    }

    private void validateLineage() {
        if (this.left != null) {
            if (this.left.parent != this) {
                throw new IllegalStateException("Lineage Validation Failure\nLeft child is orphaned :\n" + this.left);
            }
            this.left.validateLineage();
        }
        if (this.right != null) {
            if (this.right.parent != this) {
                throw new IllegalStateException("Lineage Validation Failure\nRight child is orphaned :\n" + this.right);
            }
            this.right.validateLineage();
        }
    }

    private void validateCompression() {
        if (this.left != null) {
            this.left.validateCompression();
        }
        if (this.right != null) {
            this.right.validateCompression();
        }
        if (this.whiteSpace == 0 && this.getIndexForValidation() != 0) {
            throw new IllegalStateException("Compression Validation Failure\nThe following node was found that could be compressed: \n" + this);
        }
    }

    private int validateTreeSize() {
        int rightTreeSize;
        int leftTreeSize = this.left == null ? 0 : this.left.validateTreeSize();
        int n = rightTreeSize = this.right == null ? 0 : this.right.validateTreeSize();
        if (this.treeLeftSize != leftTreeSize) {
            throw new IllegalStateException("Tree Size Validation Failure\nThe following node was found that had a tree size failure on the left subtree: \n" + this);
        }
        if (this.treeRightSize != rightTreeSize) {
            throw new IllegalStateException("Tree Size Validation Failure\nThe following node was found that had a tree size failure on the right subtree: \n" + this);
        }
        return this.treeLeftSize + this.whiteSpace + this.rootSize + this.treeRightSize;
    }

    private int getIndexForValidation() {
        if (this.parent != null) {
            return this.parent.getIndexForValidation(this) + this.treeLeftSize;
        }
        return this.treeLeftSize;
    }

    private int getIndexForValidation(BarcodeNode child) {
        if (child == this.left) {
            if (this.parent != null) {
                return this.parent.getIndexForValidation(this);
            }
            return 0;
        }
        if (this.parent != null) {
            return this.parent.getIndexForValidation(this) + this.treeLeftSize + this.whiteSpace + this.rootSize;
        }
        return this.treeLeftSize + this.whiteSpace + this.rootSize;
    }

    private void validateRootSize() {
        if (this.left != null) {
            this.left.validateRootSize();
        }
        if (this.right != null) {
            this.right.validateRootSize();
        }
        if (this.rootSize == 0) {
            throw new IllegalStateException("Root Size Validation Failure\nA node was found with a root size of zero.");
        }
    }
}

