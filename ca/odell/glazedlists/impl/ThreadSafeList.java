/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.TransformedList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventAssembler;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.util.concurrent.Lock;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;
import java.util.Collection;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class ThreadSafeList<E>
extends TransformedList<E, E> {
    public ThreadSafeList(EventList<E> source) {
        super(source);
        source.addListEventListener(this);
    }

    @Override
    public void listChanged(ListEvent<E> listChanges) {
        this.updates.forwardEvent(listChanges);
    }

    @Override
    public E get(int index) {
        this.getReadWriteLock().readLock().lock();
        try {
            Object e = this.source.get(index);
            return e;
        }
        finally {
            this.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public int size() {
        this.getReadWriteLock().readLock().lock();
        try {
            int n = this.source.size();
            return n;
        }
        finally {
            this.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    protected boolean isWritable() {
        return true;
    }

    @Override
    public boolean contains(Object object) {
        this.getReadWriteLock().readLock().lock();
        try {
            boolean bl = this.source.contains(object);
            return bl;
        }
        finally {
            this.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        this.getReadWriteLock().readLock().lock();
        try {
            boolean bl = this.source.containsAll(collection);
            return bl;
        }
        finally {
            this.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public boolean equals(Object object) {
        this.getReadWriteLock().readLock().lock();
        try {
            boolean bl = this.source.equals(object);
            return bl;
        }
        finally {
            this.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public int hashCode() {
        this.getReadWriteLock().readLock().lock();
        try {
            int n = this.source.hashCode();
            return n;
        }
        finally {
            this.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public int indexOf(Object object) {
        this.getReadWriteLock().readLock().lock();
        try {
            int n = this.source.indexOf(object);
            return n;
        }
        finally {
            this.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public int lastIndexOf(Object object) {
        this.getReadWriteLock().readLock().lock();
        try {
            int n = this.source.lastIndexOf(object);
            return n;
        }
        finally {
            this.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public boolean isEmpty() {
        this.getReadWriteLock().readLock().lock();
        try {
            boolean bl = this.source.isEmpty();
            return bl;
        }
        finally {
            this.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public Object[] toArray() {
        this.getReadWriteLock().readLock().lock();
        try {
            Object[] arrobject = this.source.toArray();
            return arrobject;
        }
        finally {
            this.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public <T> T[] toArray(T[] array) {
        this.getReadWriteLock().readLock().lock();
        try {
            T[] arrT = this.source.toArray(array);
            return arrT;
        }
        finally {
            this.getReadWriteLock().readLock().unlock();
        }
    }

    @Override
    public boolean add(E value) {
        this.getReadWriteLock().writeLock().lock();
        try {
            boolean bl = this.source.add(value);
            return bl;
        }
        finally {
            this.getReadWriteLock().writeLock().unlock();
        }
    }

    @Override
    public boolean remove(Object toRemove) {
        this.getReadWriteLock().writeLock().lock();
        try {
            boolean bl = this.source.remove(toRemove);
            return bl;
        }
        finally {
            this.getReadWriteLock().writeLock().unlock();
        }
    }

    @Override
    public boolean addAll(Collection<? extends E> values) {
        this.getReadWriteLock().writeLock().lock();
        try {
            boolean bl = this.source.addAll(values);
            return bl;
        }
        finally {
            this.getReadWriteLock().writeLock().unlock();
        }
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> values) {
        this.getReadWriteLock().writeLock().lock();
        try {
            boolean bl = this.source.addAll(index, values);
            return bl;
        }
        finally {
            this.getReadWriteLock().writeLock().unlock();
        }
    }

    @Override
    public boolean removeAll(Collection<?> values) {
        this.getReadWriteLock().writeLock().lock();
        try {
            boolean bl = this.source.removeAll(values);
            return bl;
        }
        finally {
            this.getReadWriteLock().writeLock().unlock();
        }
    }

    @Override
    public boolean retainAll(Collection<?> values) {
        this.getReadWriteLock().writeLock().lock();
        try {
            boolean bl = this.source.retainAll(values);
            return bl;
        }
        finally {
            this.getReadWriteLock().writeLock().unlock();
        }
    }

    @Override
    public void clear() {
        this.getReadWriteLock().writeLock().lock();
        try {
            this.source.clear();
        }
        finally {
            this.getReadWriteLock().writeLock().unlock();
        }
    }

    @Override
    public E set(int index, E value) {
        this.getReadWriteLock().writeLock().lock();
        try {
            E e = this.source.set(index, value);
            return e;
        }
        finally {
            this.getReadWriteLock().writeLock().unlock();
        }
    }

    @Override
    public void add(int index, E value) {
        this.getReadWriteLock().writeLock().lock();
        try {
            this.source.add(index, value);
        }
        finally {
            this.getReadWriteLock().writeLock().unlock();
        }
    }

    @Override
    public E remove(int index) {
        this.getReadWriteLock().writeLock().lock();
        try {
            Object e = this.source.remove(index);
            return e;
        }
        finally {
            this.getReadWriteLock().writeLock().unlock();
        }
    }

    @Override
    public String toString() {
        this.getReadWriteLock().readLock().lock();
        try {
            String string = this.source.toString();
            return string;
        }
        finally {
            this.getReadWriteLock().readLock().unlock();
        }
    }
}

