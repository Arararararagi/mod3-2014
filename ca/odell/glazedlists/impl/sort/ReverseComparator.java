/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.sort;

import java.util.Comparator;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class ReverseComparator<T>
implements Comparator<T> {
    private Comparator<T> source;

    public ReverseComparator(Comparator<T> source) {
        this.source = source;
    }

    @Override
    public int compare(T alpha, T beta) {
        return this.source.compare(beta, alpha);
    }

    public Comparator<T> getSourceComparator() {
        return this.source;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        ReverseComparator that = (ReverseComparator)o;
        if (!this.source.equals(that.source)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.source.hashCode();
    }
}

