/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.sort;

import java.util.Comparator;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class ComparableComparator
implements Comparator<Comparable> {
    @Override
    public int compare(Comparable alpha, Comparable beta) {
        if (alpha != null && beta != null) {
            return alpha.compareTo(beta);
        }
        if (alpha == null) {
            if (beta == null) {
                return 0;
            }
            return -1;
        }
        return 1;
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof ComparableComparator;
    }
}

