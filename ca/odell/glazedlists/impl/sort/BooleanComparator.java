/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.sort;

import java.util.Comparator;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class BooleanComparator
implements Comparator<Boolean> {
    @Override
    public int compare(Boolean alpha, Boolean beta) {
        int alphaOrdinal;
        int n = alpha == null ? 0 : (alphaOrdinal = alpha == false ? 1 : 2);
        int betaOrdinal = beta == null ? 0 : (beta == false ? 1 : 2);
        return alphaOrdinal - betaOrdinal;
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof BooleanComparator;
    }
}

