/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.sort;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class ComparatorChain<T>
implements Comparator<T> {
    private final Comparator<T>[] comparators;

    public ComparatorChain(List<Comparator<T>> comparators) {
        this.comparators = comparators.toArray(new Comparator[comparators.size()]);
    }

    @Override
    public int compare(T alpha, T beta) {
        for (int i = 0; i < this.comparators.length; ++i) {
            int compareResult = this.comparators[i].compare(alpha, beta);
            if (compareResult == 0) continue;
            return compareResult;
        }
        return 0;
    }

    public Comparator<T>[] getComparators() {
        return this.comparators;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        ComparatorChain that = (ComparatorChain)o;
        if (!Arrays.equals(this.comparators, that.comparators)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return 0;
    }
}

