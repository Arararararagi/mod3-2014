/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.sort;

import ca.odell.glazedlists.impl.beans.BeanProperty;
import java.util.Comparator;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class BeanPropertyComparator<T>
implements Comparator<T> {
    private Comparator propertyComparator;
    private BeanProperty beanProperty = null;

    public BeanPropertyComparator(Class<T> className, String property, Comparator propertyComparator) {
        this.beanProperty = new BeanProperty<T>(className, property, true, false);
        this.propertyComparator = propertyComparator;
    }

    @Override
    public int compare(T alpha, T beta) {
        Object alphaProperty = null;
        if (alpha != null) {
            alphaProperty = this.beanProperty.get(alpha);
        }
        Object betaProperty = null;
        if (beta != null) {
            betaProperty = this.beanProperty.get(beta);
        }
        return this.propertyComparator.compare(alphaProperty, betaProperty);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        BeanPropertyComparator that = (BeanPropertyComparator)o;
        if (!this.beanProperty.equals(that.beanProperty)) {
            return false;
        }
        if (!this.propertyComparator.equals(that.propertyComparator)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result = this.propertyComparator.hashCode();
        result = 29 * result + this.beanProperty.hashCode();
        return result;
    }
}

