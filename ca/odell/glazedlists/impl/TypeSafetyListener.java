/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import java.util.Set;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class TypeSafetyListener<E>
implements ListEventListener<E> {
    private final Class[] types;

    public TypeSafetyListener(EventList<E> source, Set<Class> types) {
        this.types = types.toArray(new Class[types.size()]);
        source.addListEventListener(this);
    }

    @Override
    public void listChanged(ListEvent<E> listChanges) {
        EventList<E> source = listChanges.getSourceList();
        while (listChanges.next()) {
            int type = listChanges.getType();
            if (type == 0) continue;
            int index = listChanges.getIndex();
            E e = source.get(index);
            if (!(type != 2 || this.checkType(e))) {
                Class badType = e == null ? null : e.getClass();
                throw new IllegalArgumentException("Element with illegal type " + badType + " inserted at index " + index + ": " + e);
            }
            if (type != 1 || this.checkType(e)) continue;
            Class badType = e == null ? null : e.getClass();
            throw new IllegalArgumentException("Element with illegal type " + badType + " updated at index " + index + ": " + e);
        }
    }

    private boolean checkType(E e) {
        for (int i = 0; i < this.types.length; ++i) {
            if (e == null && this.types[i] != null || !(this.types[i] == null ? e == null : this.types[i].isAssignableFrom(e.getClass()))) continue;
            return true;
        }
        return false;
    }
}

