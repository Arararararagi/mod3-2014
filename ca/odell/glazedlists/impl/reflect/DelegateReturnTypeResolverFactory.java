/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.reflect;

import ca.odell.glazedlists.impl.reflect.J2SE14ReturnTypeResolver;
import ca.odell.glazedlists.impl.reflect.ReturnTypeResolver;
import ca.odell.glazedlists.impl.reflect.ReturnTypeResolverFactory;

class DelegateReturnTypeResolverFactory
implements ReturnTypeResolverFactory {
    private ReturnTypeResolver returnTypeResolver;

    DelegateReturnTypeResolverFactory() {
        try {
            Class.forName("java.lang.reflect.Type");
            this.returnTypeResolver = (ReturnTypeResolver)Class.forName("ca.odell.glazedlists.impl.java15.J2SE50ReturnTypeResolver").newInstance();
        }
        catch (Throwable t) {
            this.returnTypeResolver = new J2SE14ReturnTypeResolver();
        }
    }

    public ReturnTypeResolver createReturnTypeResolver() {
        return this.returnTypeResolver;
    }
}

