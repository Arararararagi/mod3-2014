/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.reflect;

import ca.odell.glazedlists.impl.reflect.DelegateReturnTypeResolverFactory;
import ca.odell.glazedlists.impl.reflect.ReturnTypeResolver;

public interface ReturnTypeResolverFactory {
    public static final ReturnTypeResolverFactory DEFAULT = new DelegateReturnTypeResolverFactory();

    public ReturnTypeResolver createReturnTypeResolver();
}

