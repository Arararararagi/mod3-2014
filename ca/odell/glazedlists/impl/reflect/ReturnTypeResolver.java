/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.reflect;

public interface ReturnTypeResolver {
    public Class<?> getReturnType(Class<?> var1, Method var2);

    public Class<?> getFirstParameterType(Class<?> var1, Method var2);
}

