/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.reflect;

import ca.odell.glazedlists.impl.reflect.ReturnTypeResolver;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
final class J2SE14ReturnTypeResolver
implements ReturnTypeResolver {
    J2SE14ReturnTypeResolver() {
    }

    @Override
    public Class<?> getReturnType(Class<?> clazz, Method method) {
        return method.getReturnType();
    }

    @Override
    public Class<?> getFirstParameterType(Class<?> clazz, Method method) {
        return method.getParameterTypes()[0];
    }
}

