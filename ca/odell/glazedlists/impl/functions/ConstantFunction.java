/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.functions;

import ca.odell.glazedlists.FunctionList;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class ConstantFunction<E, V>
implements FunctionList.Function<E, V> {
    private final V value;

    public ConstantFunction(V value) {
        this.value = value;
    }

    @Override
    public V evaluate(E sourceValue) {
        return this.value;
    }
}

