/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.impl.GlazedListsImpl;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class Diff {
    public static <E> void replaceAll(EventList<E> target, List<E> source, boolean updates) {
        Diff.replaceAll(target, source, updates, GlazedListsImpl.equalsComparator());
    }

    public static <E> void replaceAll(EventList<E> target, List<E> source, boolean updates, Comparator<E> comparator) {
        ListDiffMatcher<E> listDiffMatcher = new ListDiffMatcher<E>(target, source, comparator);
        List<Point> editScript = Diff.shortestEditScript(listDiffMatcher);
        int targetIndex = 0;
        int sourceIndex = 0;
        Point previousPoint = null;
        for (Point currentPoint : editScript) {
            int deltaY;
            if (previousPoint == null) {
                previousPoint = currentPoint;
                continue;
            }
            int deltaX = currentPoint.getX() - previousPoint.getX();
            if (deltaX == (deltaY = currentPoint.getY() - previousPoint.getY())) {
                if (updates) {
                    for (int u = 0; u < deltaX; ++u) {
                        target.set(targetIndex + u, source.get(sourceIndex + u));
                    }
                }
                targetIndex+=deltaX;
                sourceIndex+=deltaY;
            } else if (deltaX == 1 && deltaY == 0) {
                target.remove(targetIndex);
            } else if (deltaX == 0 && deltaY == 1) {
                target.add(targetIndex, source.get(sourceIndex));
                ++sourceIndex;
                ++targetIndex;
            } else {
                throw new IllegalStateException();
            }
            previousPoint = currentPoint;
        }
    }

    private static List<Point> shortestEditScript(DiffMatcher input) {
        int N = input.getAlphaLength();
        int M = input.getBetaLength();
        Point maxPoint = new Point(N, M);
        int maxSteps = N + M;
        HashMap<Integer, Point> furthestReachingPoints = new HashMap<Integer, Point>();
        for (int D = 0; D <= maxSteps; ++D) {
            for (int k = - D; k <= D; k+=2) {
                Point belowLeft = (Point)furthestReachingPoints.get(new Integer(k - 1));
                Point aboveRight = (Point)furthestReachingPoints.get(new Integer(k + 1));
                Point point = furthestReachingPoints.isEmpty() ? new Point(0, 0) : (k == - D || k != D && belowLeft.getX() < aboveRight.getX() ? aboveRight.createDeltaPoint(0, 1) : belowLeft.createDeltaPoint(1, 0));
                while (point.isLessThan(maxPoint) && input.matchPair(point.getX(), point.getY())) {
                    point = point.incrementDiagonally();
                }
                furthestReachingPoints.put(new Integer(k), point);
                if (!point.isEqualToOrGreaterThan(maxPoint)) continue;
                return point.trail();
            }
        }
        throw new IllegalStateException();
    }

    static interface DiffMatcher {
        public int getAlphaLength();

        public int getBetaLength();

        public boolean matchPair(int var1, int var2);

        public char alphaAt(int var1);

        public char betaAt(int var1);
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    static class ListDiffMatcher<E>
    implements DiffMatcher {
        private List<E> alpha;
        private List<E> beta;
        private Comparator<E> comparator;

        public ListDiffMatcher(List<E> alpha, List<E> beta, Comparator<E> comparator) {
            this.alpha = alpha;
            this.beta = beta;
            this.comparator = comparator;
        }

        @Override
        public int getAlphaLength() {
            return this.alpha.size();
        }

        @Override
        public char alphaAt(int index) {
            return this.alpha.get(index).toString().charAt(0);
        }

        @Override
        public char betaAt(int index) {
            return this.beta.get(index).toString().charAt(0);
        }

        @Override
        public int getBetaLength() {
            return this.beta.size();
        }

        @Override
        public boolean matchPair(int alphaIndex, int betaIndex) {
            return this.comparator.compare(this.alpha.get(alphaIndex), this.beta.get(betaIndex)) == 0;
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private static class Point {
        private int x = 0;
        private int y = 0;
        private Point predecessor = null;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public Point createDeltaPoint(int deltaX, int deltaY) {
            Point result = new Point(this.x + deltaX, this.y + deltaY);
            result.predecessor = this;
            return result;
        }

        public Point incrementDiagonally() {
            int deltaY;
            int deltaX;
            Point result = this.createDeltaPoint(1, 1);
            if (this.predecessor != null && (deltaX = result.x - this.predecessor.x) == (deltaY = result.y - this.predecessor.y)) {
                result.predecessor = this.predecessor;
            }
            return result;
        }

        public int getX() {
            return this.x;
        }

        public int getY() {
            return this.y;
        }

        public boolean isLessThan(Point other) {
            return this.x < other.x && this.y < other.y;
        }

        public boolean isEqualToOrGreaterThan(Point other) {
            return this.x >= other.x && this.y >= other.y;
        }

        public String toString() {
            return "(" + this.x + "," + this.y + ")";
        }

        public List<Point> trail() {
            ArrayList<Point> reverse = new ArrayList<Point>();
            Point current = this;
            while (current != null) {
                reverse.add(current);
                current = current.predecessor;
            }
            Collections.reverse(reverse);
            return reverse;
        }
    }

}

