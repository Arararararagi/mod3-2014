/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.filter;

import ca.odell.glazedlists.TextFilterator;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class StringTextFilterator<E>
implements TextFilterator<E> {
    @Override
    public void getFilterStrings(List<String> baseList, E element) {
        if (element != null) {
            baseList.add(element.toString());
        }
    }
}

