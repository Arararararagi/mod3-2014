/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.TransformedList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventAssembler;
import ca.odell.glazedlists.event.ListEventListener;
import java.util.Collection;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class ReadOnlyList<E>
extends TransformedList<E, E> {
    public ReadOnlyList(EventList<E> source) {
        super(source);
        source.addListEventListener(this);
    }

    @Override
    protected boolean isWritable() {
        return false;
    }

    @Override
    public void listChanged(ListEvent<E> listChanges) {
        this.updates.forwardEvent(listChanges);
    }

    @Override
    public boolean contains(Object object) {
        return this.source.contains(object);
    }

    @Override
    public Object[] toArray() {
        return this.source.toArray();
    }

    @Override
    public <T> T[] toArray(T[] array) {
        return this.source.toArray(array);
    }

    @Override
    public boolean containsAll(Collection<?> values) {
        return this.source.containsAll(values);
    }

    @Override
    public int indexOf(Object object) {
        return this.source.indexOf(object);
    }

    @Override
    public int lastIndexOf(Object object) {
        return this.source.lastIndexOf(object);
    }

    @Override
    public boolean equals(Object object) {
        return this.source.equals(object);
    }

    @Override
    public int hashCode() {
        return this.source.hashCode();
    }

    @Override
    public boolean add(E value) {
        throw new UnsupportedOperationException("ReadOnlyList cannot be modified");
    }

    @Override
    public void add(int index, E value) {
        throw new UnsupportedOperationException("ReadOnlyList cannot be modified");
    }

    @Override
    public boolean addAll(Collection<? extends E> values) {
        throw new UnsupportedOperationException("ReadOnlyList cannot be modified");
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> values) {
        throw new UnsupportedOperationException("ReadOnlyList cannot be modified");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("ReadOnlyList cannot be modified");
    }

    @Override
    public boolean remove(Object toRemove) {
        throw new UnsupportedOperationException("ReadOnlyList cannot be modified");
    }

    @Override
    public E remove(int index) {
        throw new UnsupportedOperationException("ReadOnlyList cannot be modified");
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException("ReadOnlyList cannot be modified");
    }

    @Override
    public boolean retainAll(Collection<?> values) {
        throw new UnsupportedOperationException("ReadOnlyList cannot be modified");
    }

    @Override
    public E set(int index, E value) {
        throw new UnsupportedOperationException("ReadOnlyList cannot be modified");
    }
}

