/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.beans;

import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import ca.odell.glazedlists.impl.beans.BeanProperty;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class BeanTableFormat<E>
implements WritableTableFormat<E>,
AdvancedTableFormat<E> {
    protected BeanProperty<E>[] beanProperties = null;
    protected String[] propertyNames;
    protected String[] columnLabels;
    private boolean[] editable;
    protected Comparator[] comparators;
    protected Class[] classes;
    protected static final Map<Class, Class> primitiveToObjectMap;

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public BeanTableFormat(Class<E> beanClass, String[] propertyNames, String[] columnLabels, boolean[] editable) {
        this.propertyNames = propertyNames;
        this.columnLabels = columnLabels;
        this.editable = editable;
        this.comparators = new Comparator[propertyNames.length];
        this.classes = new Class[propertyNames.length];
        if (beanClass == null) {
            for (int c = 0; c < this.classes.length; ++c) {
                this.classes[c] = Object.class;
                this.comparators[c] = GlazedLists.comparableComparator();
            }
            return;
        }
        this.loadPropertyDescriptors(beanClass);
        for (int c = 0; c < this.classes.length; ++c) {
            Class rawClass = this.beanProperties[c].getValueClass();
            this.classes[c] = primitiveToObjectMap.containsKey(rawClass) ? primitiveToObjectMap.get(rawClass) : rawClass;
            this.comparators[c] = Comparable.class.isAssignableFrom(this.classes[c]) ? GlazedLists.comparableComparator() : null;
        }
    }

    public BeanTableFormat(Class<E> beanClass, String[] propertyNames, String[] columnLabels) {
        this(beanClass, propertyNames, columnLabels, new boolean[propertyNames.length]);
    }

    protected void loadPropertyDescriptors(Class<E> beanClass) {
        this.beanProperties = new BeanProperty[this.propertyNames.length];
        for (int p = 0; p < this.propertyNames.length; ++p) {
            this.beanProperties[p] = new BeanProperty<E>(beanClass, this.propertyNames[p], true, this.editable[p]);
        }
    }

    @Override
    public int getColumnCount() {
        return this.columnLabels.length;
    }

    @Override
    public String getColumnName(int column) {
        return this.columnLabels[column];
    }

    @Override
    public Object getColumnValue(E baseObject, int column) {
        if (baseObject == null) {
            return null;
        }
        if (this.beanProperties == null) {
            this.loadPropertyDescriptors(baseObject.getClass());
        }
        return this.beanProperties[column].get(baseObject);
    }

    @Override
    public boolean isEditable(E baseObject, int column) {
        return this.editable[column];
    }

    @Override
    public E setColumnValue(E baseObject, Object editedValue, int column) {
        if (baseObject == null) {
            return null;
        }
        if (this.beanProperties == null) {
            this.loadPropertyDescriptors(baseObject.getClass());
        }
        this.beanProperties[column].set(baseObject, editedValue);
        return baseObject;
    }

    @Override
    public Class getColumnClass(int column) {
        return this.classes[column];
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return this.comparators[column];
    }

    static {
        HashMap<Class, reference> primitiveToObjectMapWritable = new HashMap<Class, reference>();
        primitiveToObjectMapWritable.put(Boolean.TYPE, Boolean.class);
        primitiveToObjectMapWritable.put(Character.TYPE, Character.class);
        primitiveToObjectMapWritable.put(Byte.TYPE, Byte.class);
        primitiveToObjectMapWritable.put(Short.TYPE, Short.class);
        primitiveToObjectMapWritable.put(Integer.TYPE, Integer.class);
        primitiveToObjectMapWritable.put(Long.TYPE, Long.class);
        primitiveToObjectMapWritable.put(Float.TYPE, Float.class);
        primitiveToObjectMapWritable.put(Double.TYPE, Double.class);
        primitiveToObjectMap = Collections.unmodifiableMap(primitiveToObjectMapWritable);
    }
}

