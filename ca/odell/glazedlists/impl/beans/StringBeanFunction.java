/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.beans;

import ca.odell.glazedlists.FunctionList;
import ca.odell.glazedlists.impl.beans.BeanProperty;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class StringBeanFunction<E>
implements FunctionList.Function<E, String> {
    private final BeanProperty<E> property;

    public StringBeanFunction(Class<E> beanClass, String propertyName) {
        this.property = new BeanProperty<E>(beanClass, propertyName, true, false);
    }

    @Override
    public String evaluate(E sourceValue) {
        Object rawValue = this.property.get(sourceValue);
        return rawValue == null ? null : String.valueOf(rawValue);
    }
}

