/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.beans;

import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.matchers.Matcher;
import ca.odell.glazedlists.matchers.Matchers;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.EventListener;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class BeanConnector<E>
implements ObservableElementList.Connector<E> {
    private Method addListenerMethod;
    private Method removeListenerMethod;
    private ObservableElementList<? extends E> list;
    protected PropertyChangeListener propertyChangeListener;
    private Matcher<PropertyChangeEvent> eventMatcher;
    private final Object[] reflectionParameters;
    private static final Class[] REFLECTION_TYPES = new Class[]{PropertyChangeListener.class};

    public BeanConnector(Class<E> beanClass) {
        this.propertyChangeListener = this.createPropertyChangeListener();
        this.eventMatcher = Matchers.trueMatcher();
        this.reflectionParameters = new Object[]{this.propertyChangeListener};
        Method[] methods = beanClass.getMethods();
        for (int m = 0; m < methods.length; ++m) {
            if (methods[m].getParameterTypes().length != 1 || methods[m].getParameterTypes()[0] != PropertyChangeListener.class) continue;
            if (methods[m].getName().startsWith("add")) {
                this.addListenerMethod = methods[m];
            }
            if (!methods[m].getName().startsWith("remove")) continue;
            this.removeListenerMethod = methods[m];
        }
        if (this.addListenerMethod == null || this.removeListenerMethod == null) {
            throw new IllegalArgumentException("Couldn't find listener methods for " + beanClass.getName());
        }
    }

    public BeanConnector(Class<E> beanClass, Matcher<PropertyChangeEvent> eventMatcher) {
        this(beanClass);
        super.setEventMatcher(eventMatcher);
    }

    public BeanConnector(Class<E> beanClass, String addListenerMethodName, String removeListenerMethodName) {
        this.propertyChangeListener = this.createPropertyChangeListener();
        this.eventMatcher = Matchers.trueMatcher();
        this.reflectionParameters = new Object[]{this.propertyChangeListener};
        try {
            this.addListenerMethod = beanClass.getMethod(addListenerMethodName, REFLECTION_TYPES);
            this.removeListenerMethod = beanClass.getMethod(removeListenerMethodName, REFLECTION_TYPES);
        }
        catch (NoSuchMethodException e) {
            throw new IllegalArgumentException("Failed to find method " + e.getMessage() + " in " + beanClass);
        }
    }

    public BeanConnector(Class<E> beanClass, String addListenerMethodName, String removeListenerMethodName, Matcher<PropertyChangeEvent> eventMatcher) {
        this(beanClass, addListenerMethodName, removeListenerMethodName);
        super.setEventMatcher(eventMatcher);
    }

    @Override
    public EventListener installListener(E element) {
        try {
            this.addListenerMethod.invoke(element, this.reflectionParameters);
            return this.propertyChangeListener;
        }
        catch (IllegalAccessException iae) {
            throw new RuntimeException(iae);
        }
        catch (InvocationTargetException ite) {
            throw new RuntimeException(ite.getCause());
        }
    }

    @Override
    public void uninstallListener(E element, EventListener listener) {
        try {
            this.removeListenerMethod.invoke(element, this.reflectionParameters);
        }
        catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        catch (InvocationTargetException e) {
            throw new RuntimeException(e.getCause());
        }
    }

    @Override
    public void setObservableElementList(ObservableElementList<? extends E> list) {
        this.list = list;
    }

    public final Matcher<PropertyChangeEvent> getEventMatcher() {
        return this.eventMatcher;
    }

    private void setEventMatcher(Matcher<PropertyChangeEvent> eventMatcher) {
        if (eventMatcher == null) {
            throw new IllegalArgumentException("Event matcher may not be null.");
        }
        this.eventMatcher = eventMatcher;
    }

    protected PropertyChangeListener createPropertyChangeListener() {
        return new PropertyChangeHandler();
    }

    public class PropertyChangeHandler
    implements PropertyChangeListener {
        public void propertyChange(PropertyChangeEvent event) {
            if (BeanConnector.this.getEventMatcher().matches(event)) {
                BeanConnector.this.list.elementChanged(event.getSource());
            }
        }
    }

}

