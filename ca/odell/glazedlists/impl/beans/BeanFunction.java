/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.beans;

import ca.odell.glazedlists.FunctionList;
import ca.odell.glazedlists.impl.beans.BeanProperty;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class BeanFunction<E, V>
implements FunctionList.Function<E, V> {
    private final BeanProperty<E> property;

    public BeanFunction(Class<E> beanClass, String propertyName) {
        this.property = new BeanProperty<E>(beanClass, propertyName, true, false);
    }

    @Override
    public V evaluate(E sourceValue) {
        return (V)this.property.get(sourceValue);
    }
}

