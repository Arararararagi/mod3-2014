/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.beans;

import ca.odell.glazedlists.ThresholdList;
import ca.odell.glazedlists.impl.beans.BeanProperty;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class BeanThresholdEvaluator<E>
implements ThresholdList.Evaluator<E> {
    private String propertyName = null;
    private BeanProperty<E> beanProperty = null;

    public BeanThresholdEvaluator(String propertyName) {
        this.propertyName = propertyName;
    }

    @Override
    public int evaluate(E object) {
        if (this.beanProperty == null) {
            this.loadPropertyDescriptors(object);
        }
        Object property = this.beanProperty.get(object);
        return (Integer)property;
    }

    private void loadPropertyDescriptors(Object beanObject) {
        Class beanClass = beanObject.getClass();
        this.beanProperty = new BeanProperty(beanClass, this.propertyName, true, false);
    }
}

