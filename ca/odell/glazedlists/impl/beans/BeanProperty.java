/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.beans;

import ca.odell.glazedlists.impl.reflect.ReturnTypeResolver;
import ca.odell.glazedlists.impl.reflect.ReturnTypeResolverFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class BeanProperty<T> {
    private static final ReturnTypeResolver TYPE_RESOLVER = ReturnTypeResolverFactory.DEFAULT.createReturnTypeResolver();
    private final Class<T> beanClass;
    private final String propertyName;
    private final boolean identityProperty;
    private Class valueClass = null;
    private List<Method> getterChain = null;
    private List<Method> setterChain = null;
    private static final Object[] EMPTY_ARGUMENTS = new Object[0];
    private static final Class[] EMPTY_PARAMETER_TYPES = new Class[0];

    public BeanProperty(Class<T> beanClass, String propertyName, boolean readable, boolean writable) {
        if (beanClass == null) {
            throw new IllegalArgumentException("beanClass may not be null");
        }
        if (propertyName == null) {
            throw new IllegalArgumentException("propertyName may not be null");
        }
        if (propertyName.length() == 0) {
            throw new IllegalArgumentException("propertyName may not be empty");
        }
        this.beanClass = beanClass;
        this.propertyName = propertyName;
        this.identityProperty = "this".equals(propertyName);
        if (this.identityProperty && writable) {
            throw new IllegalArgumentException("The identity property name (this) cannot be writable");
        }
        String[] propertyParts = propertyName.split("\\.");
        ArrayList<Method> commonChain = new ArrayList<Method>(propertyParts.length);
        Class currentClass = beanClass;
        for (int p = 0; p < propertyParts.length - 1; ++p) {
            Method partGetter = this.findGetterMethod(currentClass, propertyParts[p]);
            commonChain.add(partGetter);
            currentClass = TYPE_RESOLVER.getReturnType(currentClass, partGetter);
        }
        if (readable) {
            if (this.identityProperty) {
                this.valueClass = beanClass;
            } else {
                this.getterChain = new ArrayList<Method>();
                this.getterChain.addAll(commonChain);
                Method lastGetter = this.findGetterMethod(currentClass, propertyParts[propertyParts.length - 1]);
                this.getterChain.add(lastGetter);
                this.valueClass = TYPE_RESOLVER.getReturnType(currentClass, lastGetter);
            }
        }
        if (writable) {
            this.setterChain = new ArrayList<Method>();
            this.setterChain.addAll(commonChain);
            Method lastSetter = this.findSetterMethod(currentClass, propertyParts[propertyParts.length - 1]);
            this.setterChain.add(lastSetter);
            if (this.valueClass == null) {
                this.valueClass = TYPE_RESOLVER.getFirstParameterType(currentClass, lastSetter);
            }
        }
    }

    private Method findGetterMethod(Class targetClass, String property) {
        for (Class currentClass = targetClass; currentClass != null; currentClass = currentClass.getSuperclass()) {
            String getProperty = "get" + this.capitalize(property);
            Method result = this.getMethod(currentClass, getProperty, EMPTY_PARAMETER_TYPES);
            if (result != null) {
                this.validateGetter(result);
                return result;
            }
            String isProperty = "is" + this.capitalize(property);
            result = this.getMethod(currentClass, isProperty, EMPTY_PARAMETER_TYPES);
            if (result == null) continue;
            this.validateGetter(result);
            return result;
        }
        throw new IllegalArgumentException("Failed to find getter for property \"" + property + "\" of " + targetClass);
    }

    private Method findSetterMethod(Class targetClass, String property) {
        String setProperty = "set" + this.capitalize(property);
        for (Class currentClass = targetClass; currentClass != null; currentClass = currentClass.getSuperclass()) {
            Method[] classMethods = currentClass.getMethods();
            for (int m = 0; m < classMethods.length; ++m) {
                if (!classMethods[m].getName().equals(setProperty)) continue;
                if (classMethods[m].getParameterTypes().length != 1) continue;
                this.validateSetter(classMethods[m]);
                return classMethods[m];
            }
        }
        throw new IllegalArgumentException("Failed to find setter for property \"" + property + "\" of " + targetClass);
    }

    private void validateGetter(Method method) {
        if (!Modifier.isPublic(method.getModifiers())) {
            throw new IllegalArgumentException("Getter \"" + method + "\" is not public");
        }
        if (Void.TYPE.equals(method.getReturnType())) {
            throw new IllegalArgumentException("Getter \"" + method + "\" returns void");
        }
        if (method.getParameterTypes().length != 0) {
            throw new IllegalArgumentException("Getter \"" + method + "\" has too many parameters; expected 0 but found " + method.getParameterTypes().length);
        }
    }

    private void validateSetter(Method method) {
        if (!Modifier.isPublic(method.getModifiers())) {
            throw new IllegalArgumentException("Setter \"" + method + "\" is not public");
        }
        if (method.getParameterTypes().length != 1) {
            throw new IllegalArgumentException("Setter \"" + method + "\" takes the wrong number of parameters; expected 1 but found " + method.getParameterTypes().length);
        }
    }

    private String capitalize(String property) {
        StringBuffer result = new StringBuffer();
        result.append(Character.toUpperCase(property.charAt(0)));
        result.append(property.substring(1));
        return result.toString();
    }

    private Method getMethod(Class targetClass, String methodName, Class[] parameterTypes) {
        try {
            return targetClass.getMethod(methodName, parameterTypes);
        }
        catch (NoSuchMethodException e) {
            return null;
        }
    }

    public Class<T> getBeanClass() {
        return this.beanClass;
    }

    public String getPropertyName() {
        return this.propertyName;
    }

    public Class getValueClass() {
        return this.valueClass;
    }

    public boolean isReadable() {
        return this.getterChain != null || this.identityProperty;
    }

    public boolean isWritable() {
        return this.setterChain != null;
    }

    public Object get(T member) {
        if (!this.isReadable()) {
            throw new IllegalStateException("Property " + this.propertyName + " of " + this.beanClass + " not readable");
        }
        if (this.identityProperty) {
            return member;
        }
        try {
            Object currentMember = member;
            int n = this.getterChain.size();
            for (int i = 0; i < n; ++i) {
                Method currentMethod = this.getterChain.get(i);
                currentMember = currentMethod.invoke(currentMember, EMPTY_ARGUMENTS);
                if (currentMember != null) continue;
                return null;
            }
            return currentMember;
        }
        catch (IllegalAccessException e) {
            SecurityException se = new SecurityException();
            se.initCause(e);
            throw se;
        }
        catch (InvocationTargetException e) {
            throw new UndeclaredThrowableException(e.getCause());
        }
    }

    public Object set(T member, Object newValue) {
        if (!this.isWritable()) {
            throw new IllegalStateException("Property " + this.propertyName + " of " + this.beanClass + " not writable");
        }
        Method setterMethod = null;
        try {
            Object currentMember = member;
            int n = this.setterChain.size() - 1;
            for (int i = 0; i < n; ++i) {
                Method currentMethod = this.setterChain.get(i);
                currentMember = currentMethod.invoke(currentMember, EMPTY_ARGUMENTS);
                if (currentMember != null) continue;
                return null;
            }
            setterMethod = this.setterChain.get(this.setterChain.size() - 1);
            return setterMethod.invoke(currentMember, newValue);
        }
        catch (IllegalArgumentException e) {
            String message = e.getMessage();
            if ("argument type mismatch".equals(message) && setterMethod != null) {
                message = BeanProperty.getSimpleName(setterMethod.getDeclaringClass()) + "." + setterMethod.getName() + "(" + BeanProperty.getSimpleName(setterMethod.getParameterTypes()[0]) + ") cannot be called with an instance of " + BeanProperty.getSimpleName(newValue.getClass());
            }
            throw new IllegalArgumentException(message);
        }
        catch (IllegalAccessException e) {
            SecurityException se = new SecurityException();
            se.initCause(e);
            throw se;
        }
        catch (InvocationTargetException e) {
            throw new UndeclaredThrowableException(e.getCause());
        }
        catch (RuntimeException e) {
            throw new RuntimeException("Failed to set property \"" + this.propertyName + "\" of " + this.beanClass + " to " + (newValue == null ? "null" : new StringBuilder().append("instance of ").append(newValue.getClass()).toString()), e);
        }
    }

    private static String getSimpleName(Class clazz) {
        String simpleName;
        int index;
        Class declaringClass = clazz.getDeclaringClass();
        String string = simpleName = declaringClass == null ? null : declaringClass.getName();
        if (simpleName == null) {
            simpleName = clazz.getName();
            return simpleName.substring(simpleName.lastIndexOf(".") + 1);
        }
        int length = simpleName.length();
        if (length < 1 || simpleName.charAt(0) != '$') {
            throw new InternalError("Malformed class name");
        }
        for (index = 1; index < length && BeanProperty.isAsciiDigit(simpleName.charAt(index)); ++index) {
        }
        return simpleName.substring(index);
    }

    private static boolean isAsciiDigit(char c) {
        return '0' <= c && c <= '9';
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        BeanProperty that = (BeanProperty)o;
        if (!this.beanClass.equals(that.beanClass)) {
            return false;
        }
        if (!this.propertyName.equals(that.propertyName)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result = this.beanClass.hashCode();
        result = 29 * result + this.propertyName.hashCode();
        return result;
    }
}

