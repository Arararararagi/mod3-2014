/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.beans;

import ca.odell.glazedlists.Filterator;
import ca.odell.glazedlists.TextFilterator;
import ca.odell.glazedlists.impl.beans.BeanProperty;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class BeanTextFilterator<D, E>
implements TextFilterator<E>,
Filterator<D, E> {
    private String[] propertyNames;
    private BeanProperty[] beanProperties = null;

    public /* varargs */ BeanTextFilterator(String ... propertyNames) {
        this.propertyNames = propertyNames;
    }

    public /* varargs */ BeanTextFilterator(Class<E> beanClass, String ... propertyNames) {
        this.propertyNames = propertyNames;
        this.loadPropertyDescriptors(beanClass);
    }

    @Override
    public void getFilterStrings(List<String> baseList, E element) {
        if (element == null) {
            return;
        }
        if (this.beanProperties == null) {
            this.loadPropertyDescriptors(element.getClass());
        }
        for (int p = 0; p < this.beanProperties.length; ++p) {
            Object propertyValue = this.beanProperties[p].get(element);
            if (propertyValue == null) continue;
            baseList.add(propertyValue.toString());
        }
    }

    @Override
    public void getFilterValues(List<D> baseList, E element) {
        if (element == null) {
            return;
        }
        if (this.beanProperties == null) {
            this.loadPropertyDescriptors(element.getClass());
        }
        for (int p = 0; p < this.beanProperties.length; ++p) {
            Object propertyValue = this.beanProperties[p].get(element);
            if (propertyValue == null) continue;
            baseList.add((Object)propertyValue);
        }
    }

    private void loadPropertyDescriptors(Class beanClass) {
        this.beanProperties = new BeanProperty[this.propertyNames.length];
        for (int p = 0; p < this.propertyNames.length; ++p) {
            this.beanProperties[p] = new BeanProperty(beanClass, this.propertyNames[p], true, false);
        }
    }
}

