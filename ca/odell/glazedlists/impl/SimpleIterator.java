/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class SimpleIterator<E>
implements Iterator<E> {
    private final List<E> source;
    private int nextIndex = 0;

    public SimpleIterator(List<E> source) {
        this.source = source;
    }

    @Override
    public boolean hasNext() {
        return this.nextIndex < this.source.size();
    }

    @Override
    public E next() {
        if (this.nextIndex == this.source.size()) {
            throw new NoSuchElementException("Cannot retrieve element " + this.nextIndex + " on a list of size " + this.source.size());
        }
        return this.source.get(this.nextIndex++);
    }

    @Override
    public void remove() {
        if (this.nextIndex == 0) {
            throw new IllegalStateException("Cannot remove() without a prior call to next() or previous()");
        }
        this.source.remove(--this.nextIndex);
    }
}

