/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl;

import ca.odell.glazedlists.SortedList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.impl.adt.Barcode;
import java.util.Comparator;
import java.util.LinkedList;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class Grouper<E> {
    public static final Object UNIQUE = Barcode.BLACK;
    public static final Object DUPLICATE = Barcode.WHITE;
    private static final Object UNIQUE_WITH_DUPLICATE = null;
    private static final Object TODO = Barcode.BLACK;
    private static final Object DONE = Barcode.WHITE;
    private static final int LEFT_GROUP = -1;
    private static final int NO_GROUP = 0;
    private static final int RIGHT_GROUP = 1;
    private SortedList<E> sortedList;
    private Comparator<? super E> comparator;
    private Client<E> client;
    private Barcode barcode;

    public Grouper(SortedList<E> sortedList, Client client) {
        this.sortedList = sortedList;
        this.client = client;
        this.setComparator(sortedList.getComparator());
    }

    public void setComparator(Comparator<? super E> comparator) {
        if (this.comparator == comparator) {
            return;
        }
        this.comparator = comparator;
        this.barcode = new Barcode();
        int n = this.sortedList.size();
        for (int i = 0; i < n; ++i) {
            this.barcode.add(i, this.groupTogether(i, i - 1) ? DUPLICATE : UNIQUE, 1);
        }
    }

    public Comparator<? super E> getComparator() {
        return this.comparator;
    }

    public Client getClient() {
        return this.client;
    }

    public Barcode getBarcode() {
        return this.barcode;
    }

    public void listChanged(ListEvent<E> listChanges) {
        Barcode toDoList = new Barcode();
        toDoList.addWhite(0, this.barcode.size());
        LinkedList<Object> removedValues = new LinkedList<Object>();
        while (listChanges.next()) {
            int changeIndex = listChanges.getIndex();
            int changeType = listChanges.getType();
            if (changeType == 2) {
                this.barcode.add(changeIndex, UNIQUE, 1);
                toDoList.add(changeIndex, TODO, 1);
                continue;
            }
            if (changeType == 1) {
                if (this.barcode.get(changeIndex) != UNIQUE || changeIndex + 1 >= this.barcode.size() || this.barcode.get(changeIndex + 1) != DUPLICATE) continue;
                this.barcode.set(changeIndex, UNIQUE, 2);
                toDoList.set(changeIndex, TODO, 1);
                continue;
            }
            if (changeType != 0) continue;
            Object deleted = this.barcode.get(changeIndex);
            this.barcode.remove(changeIndex, 1);
            toDoList.remove(changeIndex, 1);
            if (deleted == UNIQUE && changeIndex < this.barcode.size() && this.barcode.get(changeIndex) == DUPLICATE) {
                this.barcode.set(changeIndex, UNIQUE, 1);
                deleted = UNIQUE_WITH_DUPLICATE;
            }
            removedValues.addLast(deleted);
        }
        TryJoinResult tryJoinResult = new TryJoinResult();
        listChanges.reset();
        while (listChanges.next()) {
            Object newValueInGroup;
            int groupDeletedIndex;
            int changeIndex = listChanges.getIndex();
            int changeType = listChanges.getType();
            E newValue = listChanges.getNewValue();
            E oldValue = listChanges.getOldValue();
            if (changeType == 2) {
                this.tryJoinExistingGroup(changeIndex, toDoList, tryJoinResult);
                if (tryJoinResult.group == 0) {
                    this.client.groupChanged(changeIndex, tryJoinResult.groupIndex, 2, true, changeType, ListEvent.UNKNOWN_VALUE, tryJoinResult.newFirstInGroup);
                    continue;
                }
                this.client.groupChanged(changeIndex, tryJoinResult.groupIndex, 1, true, changeType, tryJoinResult.oldFirstInGroup, tryJoinResult.newFirstInGroup);
                continue;
            }
            if (changeType == 1) {
                Object firstFromPreviousGroup;
                Object firstFromNextGroup;
                int oldGroup = 0;
                if (toDoList.get(changeIndex) == TODO) {
                    oldGroup = 1;
                } else if (this.barcode.get(changeIndex) == DUPLICATE) {
                    oldGroup = -1;
                } else if (this.barcode.get(changeIndex) == UNIQUE) {
                    oldGroup = 0;
                }
                this.tryJoinExistingGroup(changeIndex, toDoList, tryJoinResult);
                int groupIndex = tryJoinResult.groupIndex;
                if (tryJoinResult.group == 0) {
                    if (oldGroup == 0) {
                        this.client.groupChanged(changeIndex, groupIndex, 1, true, changeType, oldValue, tryJoinResult.newFirstInGroup);
                        continue;
                    }
                    if (oldGroup == -1) {
                        firstFromPreviousGroup = this.sortedList.get(this.barcode.getIndex(groupIndex - 1, UNIQUE));
                        this.client.groupChanged(changeIndex, groupIndex - 1, 1, false, changeType, firstFromPreviousGroup, firstFromPreviousGroup);
                        this.client.groupChanged(changeIndex, groupIndex, 2, true, changeType, ListEvent.UNKNOWN_VALUE, tryJoinResult.newFirstInGroup);
                        continue;
                    }
                    if (oldGroup != 1) continue;
                    firstFromNextGroup = this.sortedList.get(this.barcode.getIndex(groupIndex + 1, UNIQUE));
                    this.client.groupChanged(changeIndex, groupIndex, 2, true, changeType, ListEvent.UNKNOWN_VALUE, tryJoinResult.newFirstInGroup);
                    this.client.groupChanged(changeIndex, groupIndex + 1, 1, false, changeType, oldValue, firstFromNextGroup);
                    continue;
                }
                if (tryJoinResult.group == -1) {
                    if (oldGroup == 0) {
                        this.client.groupChanged(changeIndex, groupIndex, 1, true, changeType, tryJoinResult.oldFirstInGroup, tryJoinResult.newFirstInGroup);
                        this.client.groupChanged(changeIndex, groupIndex + 1, 0, false, changeType, oldValue, ListEvent.UNKNOWN_VALUE);
                        continue;
                    }
                    if (oldGroup == -1) {
                        this.client.groupChanged(changeIndex, groupIndex, 1, true, changeType, tryJoinResult.oldFirstInGroup, tryJoinResult.newFirstInGroup);
                        continue;
                    }
                    if (oldGroup != 1) continue;
                    this.client.groupChanged(changeIndex, groupIndex, 1, true, changeType, tryJoinResult.oldFirstInGroup, tryJoinResult.newFirstInGroup);
                    if (groupIndex + 1 >= this.barcode.blackSize()) continue;
                    firstFromNextGroup = this.sortedList.get(this.barcode.getIndex(groupIndex + 1, UNIQUE));
                    this.client.groupChanged(changeIndex, groupIndex + 1, 1, false, changeType, oldValue, firstFromNextGroup);
                    continue;
                }
                if (tryJoinResult.group != 1) continue;
                if (oldGroup == 0) {
                    this.client.groupChanged(changeIndex, groupIndex, 0, false, changeType, oldValue, ListEvent.UNKNOWN_VALUE);
                    this.client.groupChanged(changeIndex, groupIndex, 1, true, changeType, tryJoinResult.oldFirstInGroup, tryJoinResult.newFirstInGroup);
                    continue;
                }
                if (oldGroup == -1) {
                    if (groupIndex - 1 >= 0) {
                        firstFromPreviousGroup = this.sortedList.get(this.barcode.getIndex(groupIndex - 1, UNIQUE));
                        this.client.groupChanged(changeIndex, groupIndex - 1, 1, false, changeType, firstFromPreviousGroup, firstFromPreviousGroup);
                    }
                    this.client.groupChanged(changeIndex, groupIndex, 1, true, changeType, tryJoinResult.oldFirstInGroup, tryJoinResult.newFirstInGroup);
                    continue;
                }
                if (oldGroup != 1) continue;
                this.client.groupChanged(changeIndex, groupIndex, 1, true, changeType, tryJoinResult.oldFirstInGroup, tryJoinResult.newFirstInGroup);
                continue;
            }
            if (changeType != 0) continue;
            Object deleted = removedValues.removeFirst();
            int sourceDeletedIndex = deleted == DUPLICATE ? changeIndex - 1 : changeIndex;
            int n = groupDeletedIndex = sourceDeletedIndex < this.barcode.size() ? this.barcode.getBlackIndex(sourceDeletedIndex, true) : this.barcode.blackSize();
            if (deleted == UNIQUE) {
                this.client.groupChanged(changeIndex, groupDeletedIndex, 0, true, changeType, oldValue, ListEvent.UNKNOWN_VALUE);
                continue;
            }
            if (groupDeletedIndex < this.barcode.blackSize()) {
                int firstInGroupIndex = this.barcode.getIndex(groupDeletedIndex, UNIQUE);
                newValueInGroup = this.sortedList.get(firstInGroupIndex);
            } else {
                newValueInGroup = ListEvent.UNKNOWN_VALUE;
            }
            Object oldValueInGroup = deleted == UNIQUE_WITH_DUPLICATE ? oldValue : newValueInGroup;
            this.client.groupChanged(changeIndex, groupDeletedIndex, 1, true, changeType, oldValueInGroup, newValueInGroup);
        }
    }

    private boolean groupTogether(int sourceIndex0, int sourceIndex1) {
        if (sourceIndex0 < 0 || sourceIndex0 >= this.sortedList.size()) {
            return false;
        }
        if (sourceIndex1 < 0 || sourceIndex1 >= this.sortedList.size()) {
            return false;
        }
        return this.comparator.compare(this.sortedList.get(sourceIndex0), this.sortedList.get(sourceIndex1)) == 0;
    }

    private TryJoinResult tryJoinExistingGroup(int changeIndex, Barcode toDoList, TryJoinResult<E> result) {
        int predecessorIndex = changeIndex - 1;
        if (this.groupTogether(predecessorIndex, changeIndex)) {
            this.barcode.set(changeIndex, DUPLICATE, 1);
            int groupIndex = this.barcode.getColourIndex(changeIndex, true, UNIQUE);
            int indexOfFirstInGroup = this.barcode.getIndex(groupIndex, UNIQUE);
            Object firstElementInGroup = this.sortedList.get(indexOfFirstInGroup);
            return result.set(-1, groupIndex, firstElementInGroup, firstElementInGroup);
        }
        int successorIndex = changeIndex + 1;
        while (this.groupTogether(changeIndex, successorIndex)) {
            if (toDoList.get(successorIndex) == DONE) {
                this.barcode.set(changeIndex, UNIQUE, 1);
                this.barcode.set(successorIndex, DUPLICATE, 1);
                int groupIndex = this.barcode.getColourIndex(changeIndex, UNIQUE);
                Object oldFirstElementInGroup = this.sortedList.get(successorIndex);
                Object newFirstElementInGroup = this.sortedList.get(changeIndex);
                return result.set(1, groupIndex, oldFirstElementInGroup, newFirstElementInGroup);
            }
            ++successorIndex;
        }
        this.barcode.set(changeIndex, UNIQUE, 1);
        int groupIndex = this.barcode.getColourIndex(changeIndex, UNIQUE);
        Object onlyElementInGroup = this.sortedList.get(changeIndex);
        return result.set(0, groupIndex, ListEvent.UNKNOWN_VALUE, onlyElementInGroup);
    }

    public static interface Client<E> {
        public void groupChanged(int var1, int var2, int var3, boolean var4, int var5, E var6, E var7);
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private static class TryJoinResult<E> {
        int group;
        int groupIndex;
        E oldFirstInGroup;
        E newFirstInGroup;

        private TryJoinResult() {
        }

        public TryJoinResult set(int group, int groupIndex, E oldFirstElementInGroup, E newFirstElementInGroup) {
            this.group = group;
            this.groupIndex = groupIndex;
            this.oldFirstInGroup = oldFirstElementInGroup;
            this.newFirstInGroup = newFirstElementInGroup;
            return this;
        }
    }

}

