/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.gui;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.TransformedList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventAssembler;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.event.ListEventPublisher;
import ca.odell.glazedlists.util.concurrent.Lock;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public abstract class ThreadProxyEventList<E>
extends TransformedList<E, E>
implements RandomAccess {
    private List<E> localCache = new ArrayList();
    private ThreadProxyEventList<E> updateRunner;
    private final ListEventAssembler<E> cacheUpdates;
    private boolean scheduled;

    public ThreadProxyEventList(EventList<E> source) {
        super(source);
        this.updateRunner = new UpdateRunner();
        this.cacheUpdates = new ListEventAssembler(this, ListEventAssembler.createListEventPublisher());
        this.scheduled = false;
        this.localCache.addAll(source);
        this.cacheUpdates.addListEventListener(this.updateRunner);
        source.addListEventListener(this);
    }

    @Override
    public final void listChanged(ListEvent<E> listChanges) {
        if (!this.scheduled) {
            this.updates.beginEvent(true);
            this.cacheUpdates.beginEvent(true);
        }
        this.updates.forwardEvent(listChanges);
        this.cacheUpdates.forwardEvent(listChanges);
        if (!this.scheduled) {
            this.scheduled = true;
            this.schedule((Runnable)this.updateRunner);
        }
    }

    protected abstract void schedule(Runnable var1);

    @Override
    public final int size() {
        return this.localCache.size();
    }

    @Override
    public final E get(int index) {
        return this.localCache.get(index);
    }

    @Override
    protected final boolean isWritable() {
        return true;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private List<E> applyChangeToCache(EventList<E> source, ListEvent<E> listChanges, List<E> localCache) {
        int changeIndex;
        int changeType;
        ArrayList<E> result = new ArrayList<E>(source.size());
        int resultIndex = 0;
        int cacheOffset = 0;
        if (listChanges.next()) {
            changeIndex = listChanges.getIndex();
            changeType = listChanges.getType();
        } else {
            changeIndex = source.size();
            changeType = -1;
        }
        while (resultIndex < changeIndex) {
            result.add(resultIndex, localCache.get(resultIndex + cacheOffset));
            ++resultIndex;
        }
        return result;
    }

    @Override
    public void dispose() {
        super.dispose();
        this.cacheUpdates.removeListEventListener(this.updateRunner);
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class UpdateRunner
    implements Runnable,
    ListEventListener<E> {
        private UpdateRunner() {
        }

        @Override
        public void run() {
            ThreadProxyEventList.this.getReadWriteLock().writeLock().lock();
            try {
                ThreadProxyEventList.this.cacheUpdates.commitEvent();
                ThreadProxyEventList.this.updates.commitEvent();
            }
            finally {
                ThreadProxyEventList.this.scheduled = false;
                ThreadProxyEventList.this.getReadWriteLock().writeLock().unlock();
            }
        }

        @Override
        public void listChanged(ListEvent<E> listChanges) {
            ThreadProxyEventList.this.localCache = ThreadProxyEventList.this.applyChangeToCache(ThreadProxyEventList.this.source, listChanges, ThreadProxyEventList.this.localCache);
        }
    }

}

