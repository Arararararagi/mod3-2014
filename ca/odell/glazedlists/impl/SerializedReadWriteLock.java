/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl;

import ca.odell.glazedlists.util.concurrent.Lock;
import ca.odell.glazedlists.util.concurrent.LockFactory;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;
import java.io.ObjectStreamException;
import java.io.Serializable;

public final class SerializedReadWriteLock
implements ReadWriteLock,
Serializable {
    private static final long serialVersionUID = -8627867501684280198L;

    public Lock readLock() {
        throw new UnsupportedOperationException("SerializedReadWriteLock is only used for serialization");
    }

    public Lock writeLock() {
        throw new UnsupportedOperationException("SerializedReadWriteLock is only used for serialization");
    }

    private Object readResolve() throws ObjectStreamException {
        return LockFactory.DEFAULT.createReadWriteLock();
    }
}

