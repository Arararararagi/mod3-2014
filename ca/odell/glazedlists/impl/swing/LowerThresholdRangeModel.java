/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.swing;

import ca.odell.glazedlists.ThresholdList;
import ca.odell.glazedlists.util.concurrent.Lock;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;
import javax.swing.BoundedRangeModel;
import javax.swing.DefaultBoundedRangeModel;

public class LowerThresholdRangeModel
extends DefaultBoundedRangeModel
implements BoundedRangeModel {
    private ThresholdList target = null;

    public LowerThresholdRangeModel(ThresholdList target) {
        this.target = target;
    }

    public int getMaximum() {
        this.target.getReadWriteLock().readLock().lock();
        try {
            int n = this.target.getUpperThreshold();
            return n;
        }
        finally {
            this.target.getReadWriteLock().readLock().unlock();
        }
    }

    public int getValue() {
        this.target.getReadWriteLock().readLock().lock();
        try {
            int n = this.target.getLowerThreshold();
            return n;
        }
        finally {
            this.target.getReadWriteLock().readLock().unlock();
        }
    }

    public void setRangeProperties(int newValue, int newExtent, int newMin, int newMax, boolean adjusting) {
        this.target.getReadWriteLock().writeLock().lock();
        try {
            boolean changed;
            if (newMin > newMax) {
                newMin = newMax;
            }
            if (newValue > newMax) {
                newMax = newValue;
            }
            if (newValue < newMin) {
                newMin = newValue;
            }
            boolean bl = changed = newExtent != this.getExtent() || newMin != this.getMinimum() || adjusting != this.getValueIsAdjusting();
            if (newValue != this.getValue()) {
                this.target.setLowerThreshold(newValue);
                changed = true;
            }
            if (newMax != this.getMaximum()) {
                this.target.setUpperThreshold(newMax);
                changed = true;
            }
            if (changed) {
                super.setRangeProperties(newValue, newExtent, newMin, newMax, adjusting);
            }
        }
        finally {
            this.target.getReadWriteLock().writeLock().unlock();
        }
    }
}

