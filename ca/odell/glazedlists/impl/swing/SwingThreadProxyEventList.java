/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.swing;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.impl.gui.ThreadProxyEventList;
import javax.swing.SwingUtilities;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class SwingThreadProxyEventList<E>
extends ThreadProxyEventList<E> {
    public SwingThreadProxyEventList(EventList<E> source) {
        super(source);
    }

    @Override
    protected void schedule(Runnable runnable) {
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }
}

