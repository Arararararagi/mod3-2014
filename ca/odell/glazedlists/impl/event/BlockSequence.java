/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.event;

import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.impl.adt.gnutrove.TIntArrayList;
import java.util.ArrayList;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class BlockSequence<E> {
    private TIntArrayList starts = new TIntArrayList();
    private TIntArrayList ends = new TIntArrayList();
    private TIntArrayList types = new TIntArrayList();
    private List<E> oldValues = new ArrayList();
    private List<E> newValues = new ArrayList();

    public boolean update(int startIndex, int endIndex) {
        return this.addChange(1, startIndex, endIndex, ListEvent.UNKNOWN_VALUE, ListEvent.UNKNOWN_VALUE);
    }

    public boolean insert(int startIndex, int endIndex) {
        return this.addChange(2, startIndex, endIndex, ListEvent.UNKNOWN_VALUE, ListEvent.UNKNOWN_VALUE);
    }

    public boolean delete(int startIndex, int endIndex) {
        return this.addChange(0, startIndex, endIndex, ListEvent.UNKNOWN_VALUE, ListEvent.UNKNOWN_VALUE);
    }

    public boolean addChange(int type, int startIndex, int endIndex, E oldValue, E newValue) {
        Object lastNewValue;
        Object lastOldValue;
        int lastChangedIndex;
        int lastEndIndex;
        int lastStartIndex;
        int lastType;
        int size = this.types.size();
        if (size == 0) {
            lastType = -1;
            lastStartIndex = -1;
            lastEndIndex = 0;
            lastChangedIndex = 0;
            lastOldValue = ListEvent.UNKNOWN_VALUE;
            lastNewValue = ListEvent.UNKNOWN_VALUE;
        } else {
            lastType = this.types.get(size - 1);
            lastStartIndex = this.starts.get(size - 1);
            lastEndIndex = this.ends.get(size - 1);
            lastChangedIndex = lastType == 0 ? lastStartIndex : lastEndIndex;
            lastOldValue = lastType == 0 ? this.oldValues.get(size - 1) : ListEvent.UNKNOWN_VALUE;
            lastNewValue = this.newValues.get(size - 1);
        }
        if (startIndex < lastChangedIndex) {
            return false;
        }
        if (lastChangedIndex == startIndex && lastType == type && oldValue == lastOldValue && newValue == lastNewValue) {
            int newLength = lastEndIndex - lastStartIndex + (endIndex - startIndex);
            this.ends.set(size - 1, lastStartIndex + newLength);
            return true;
        }
        this.starts.add(startIndex);
        this.ends.add(endIndex);
        this.types.add(type);
        this.oldValues.add(oldValue);
        this.newValues.add(newValue);
        return true;
    }

    public boolean isEmpty() {
        return this.types.isEmpty();
    }

    public void reset() {
        this.starts.clear();
        this.ends.clear();
        this.types.clear();
        this.oldValues.clear();
        this.newValues.clear();
    }

    public BlockSequence<E> iterator() {
        return new Iterator();
    }

    public String toString() {
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < this.types.size(); ++i) {
            int type;
            if (i != 0) {
                result.append(", ");
            }
            if ((type = this.types.get(i)) == 2) {
                result.append("+");
            } else if (type == 1) {
                result.append("U");
            } else if (type == 0) {
                result.append("X");
            }
            int start = this.starts.get(i);
            int end = this.ends.get(i);
            result.append(start);
            if (end == start) continue;
            result.append("-");
            result.append(end);
        }
        return result.toString();
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    public class Iterator {
        private int blockIndex;
        private int offset;
        private int startIndex;
        private int endIndex;
        private int type;

        public Iterator() {
            this.blockIndex = -1;
            this.offset = 0;
            this.startIndex = -1;
            this.endIndex = -1;
            this.type = -1;
        }

        public BlockSequence<E> copy() {
            Iterator result = new Iterator();
            result.blockIndex = this.blockIndex;
            result.offset = this.offset;
            result.startIndex = this.startIndex;
            result.endIndex = this.endIndex;
            result.type = this.type;
            return result;
        }

        public int getIndex() {
            if (this.type == 2 || this.type == 1) {
                return this.startIndex + this.offset;
            }
            if (this.type == 0) {
                return this.startIndex;
            }
            throw new IllegalStateException();
        }

        public int getBlockStart() {
            if (this.startIndex == -1) {
                throw new IllegalStateException("The ListEvent is not currently in a state to return a block start index");
            }
            return this.startIndex;
        }

        public int getBlockEnd() {
            if (this.endIndex == -1) {
                throw new IllegalStateException("The ListEvent is not currently in a state to return a block end index");
            }
            return this.endIndex;
        }

        public int getType() {
            if (this.type == -1) {
                throw new IllegalStateException("The ListEvent is not currently in a state to return a type");
            }
            return this.type;
        }

        public E getOldValue() {
            return BlockSequence.this.oldValues.get(this.blockIndex);
        }

        public E getNewValue() {
            return BlockSequence.this.newValues.get(this.blockIndex);
        }

        public boolean next() {
            if (this.offset + 1 < this.endIndex - this.startIndex) {
                ++this.offset;
                return true;
            }
            if (this.blockIndex + 1 < BlockSequence.this.types.size()) {
                ++this.blockIndex;
                this.offset = 0;
                this.startIndex = BlockSequence.this.starts.get(this.blockIndex);
                this.endIndex = BlockSequence.this.ends.get(this.blockIndex);
                this.type = BlockSequence.this.types.get(this.blockIndex);
                return true;
            }
            return false;
        }

        public boolean nextBlock() {
            if (this.blockIndex + 1 < BlockSequence.this.types.size()) {
                ++this.blockIndex;
                this.offset = 0;
                this.startIndex = BlockSequence.this.starts.get(this.blockIndex);
                this.endIndex = BlockSequence.this.ends.get(this.blockIndex);
                this.type = BlockSequence.this.types.get(this.blockIndex);
                return true;
            }
            return false;
        }

        public boolean hasNext() {
            if (this.offset + 1 < this.endIndex - this.startIndex) {
                return true;
            }
            if (this.blockIndex + 1 < BlockSequence.this.types.size()) {
                return true;
            }
            return false;
        }

        public boolean hasNextBlock() {
            if (this.blockIndex + 1 < BlockSequence.this.types.size()) {
                return true;
            }
            return false;
        }
    }

}

