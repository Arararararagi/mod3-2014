/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.event;

import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.impl.adt.barcode2.Element;
import ca.odell.glazedlists.impl.adt.barcode2.FourColorTree;
import ca.odell.glazedlists.impl.adt.barcode2.FourColorTreeIterator;
import ca.odell.glazedlists.impl.adt.barcode2.ListToByteCoder;
import ca.odell.glazedlists.impl.event.BlockSequence;
import java.util.Arrays;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class Tree4Deltas<E> {
    private static final ListToByteCoder<String> BYTE_CODER = new ListToByteCoder<String>(Arrays.asList("+", "U", "X", "_"));
    public static final byte INSERT = BYTE_CODER.colorToByte("+");
    public static final byte UPDATE = BYTE_CODER.colorToByte("U");
    public static final byte DELETE = BYTE_CODER.colorToByte("X");
    public static final byte NO_CHANGE = BYTE_CODER.colorToByte("_");
    private static final byte SOURCE_INDICES = BYTE_CODER.colorsToByte(Arrays.asList("U", "X", "_"));
    private static final byte TARGET_INDICES = BYTE_CODER.colorsToByte(Arrays.asList("U", "+", "_"));
    private static final byte ALL_INDICES = BYTE_CODER.colorsToByte(Arrays.asList("U", "X", "+", "_"));
    private static final byte CHANGE_INDICES = BYTE_CODER.colorsToByte(Arrays.asList("U", "X", "+"));
    private FourColorTree<E> tree = new FourColorTree(BYTE_CODER);
    private boolean allowContradictingEvents = false;
    private boolean initialCapacityKnown = false;
    public boolean horribleHackPreferMostRecentValue = false;

    public boolean getAllowContradictingEvents() {
        return this.allowContradictingEvents;
    }

    public void setAllowContradictingEvents(boolean allowContradictingEvents) {
        this.allowContradictingEvents = allowContradictingEvents;
    }

    public int targetToSource(int targetIndex) {
        if (!this.initialCapacityKnown) {
            this.ensureCapacity(targetIndex + 1);
        }
        return this.tree.convertIndexColor(targetIndex, TARGET_INDICES, SOURCE_INDICES);
    }

    public int sourceToTarget(int sourceIndex) {
        if (!this.initialCapacityKnown) {
            this.ensureCapacity(sourceIndex + 1);
        }
        return this.tree.convertIndexColor(sourceIndex, SOURCE_INDICES, TARGET_INDICES);
    }

    public void targetUpdate(int startIndex, int endIndex, E oldValue, E newValue) {
        if (!this.initialCapacityKnown) {
            this.ensureCapacity(endIndex);
        }
        for (int i = startIndex; i < endIndex; ++i) {
            int overallIndex = this.tree.convertIndexColor(i, TARGET_INDICES, ALL_INDICES);
            Element<E> standingChangeToIndex = this.tree.get(overallIndex, ALL_INDICES);
            if (this.horribleHackPreferMostRecentValue) {
                byte newColor = standingChangeToIndex.getColor() == INSERT ? INSERT : UPDATE;
                this.tree.set(overallIndex, ALL_INDICES, newColor, oldValue, 1);
                continue;
            }
            if (standingChangeToIndex.getColor() == INSERT) continue;
            if (standingChangeToIndex.getColor() == UPDATE) {
                oldValue = standingChangeToIndex.get();
            }
            this.tree.set(overallIndex, ALL_INDICES, UPDATE, oldValue, 1);
        }
    }

    public void targetInsert(int startIndex, int endIndex, E newValue) {
        if (!this.initialCapacityKnown) {
            this.ensureCapacity(endIndex);
        }
        this.tree.add(startIndex, TARGET_INDICES, INSERT, newValue, endIndex - startIndex);
    }

    public void targetDelete(int startIndex, int endIndex, E value) {
        if (!this.initialCapacityKnown) {
            this.ensureCapacity(endIndex);
        }
        for (int i = startIndex; i < endIndex; ++i) {
            if (startIndex > 0 && startIndex > this.tree.size(TARGET_INDICES)) {
                throw new IllegalArgumentException();
            }
            int overallIndex = this.tree.convertIndexColor(startIndex, TARGET_INDICES, ALL_INDICES);
            Element<E> standingChangeToIndex = this.tree.get(overallIndex, ALL_INDICES);
            if (standingChangeToIndex.getColor() == INSERT) {
                if (!this.allowContradictingEvents) {
                    throw new IllegalStateException("Remove " + i + " undoes prior insert at the same index! Consider enabling contradicting events.");
                }
                this.tree.remove(overallIndex, ALL_INDICES, 1);
                continue;
            }
            if (standingChangeToIndex.getColor() == UPDATE) {
                value = standingChangeToIndex.get();
            }
            this.tree.set(overallIndex, ALL_INDICES, DELETE, value, 1);
        }
    }

    public void sourceInsert(int sourceIndex) {
        this.tree.add(sourceIndex, SOURCE_INDICES, NO_CHANGE, (Object)ListEvent.UNKNOWN_VALUE, 1);
    }

    public void sourceDelete(int sourceIndex) {
        this.tree.remove(sourceIndex, SOURCE_INDICES, 1);
    }

    public void sourceRevert(int sourceIndex) {
        this.tree.set(sourceIndex, SOURCE_INDICES, NO_CHANGE, (Object)ListEvent.UNKNOWN_VALUE, 1);
    }

    public int targetSize() {
        return this.tree.size(TARGET_INDICES);
    }

    public int sourceSize() {
        return this.tree.size(SOURCE_INDICES);
    }

    public byte getChangeType(int sourceIndex) {
        return this.tree.get(sourceIndex, SOURCE_INDICES).getColor();
    }

    public E getTargetValue(int targetIndex) {
        return this.tree.get(targetIndex, TARGET_INDICES).get();
    }

    public E getSourceValue(int sourceIndex) {
        return this.tree.get(sourceIndex, SOURCE_INDICES).get();
    }

    public void reset(int size) {
        this.tree.clear();
        this.initialCapacityKnown = true;
        this.ensureCapacity(size);
    }

    private void ensureCapacity(int size) {
        int currentSize = this.tree.size(TARGET_INDICES);
        int delta = size - currentSize;
        if (delta > 0) {
            int endOfTree = this.tree.size(ALL_INDICES);
            this.tree.add(endOfTree, ALL_INDICES, NO_CHANGE, (Object)ListEvent.UNKNOWN_VALUE, delta);
        }
    }

    public void addAll(BlockSequence<E> blocks) {
        BlockSequence.Iterator i = blocks.iterator();
        while (i.nextBlock()) {
            int blockStart = i.getBlockStart();
            int blockEnd = i.getBlockEnd();
            int type = i.getType();
            Object oldValue = i.getOldValue();
            Object newValue = i.getNewValue();
            if (type == 2) {
                this.targetInsert(blockStart, blockEnd, newValue);
                continue;
            }
            if (type == 1) {
                this.targetUpdate(blockStart, blockEnd, oldValue, newValue);
                continue;
            }
            if (type == 0) {
                this.targetDelete(blockStart, blockEnd, oldValue);
                continue;
            }
            throw new IllegalStateException();
        }
    }

    public boolean isEmpty() {
        return this.tree.size(CHANGE_INDICES) == 0;
    }

    public Iterator<E> iterator() {
        return new Iterator((FourColorTree)this.tree);
    }

    public String toString() {
        return this.tree.asSequenceOfColors();
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    public static class Iterator<E> {
        private final FourColorTree<E> tree;
        private final FourColorTreeIterator<E> treeIterator;

        private Iterator(FourColorTree<E> tree) {
            this.tree = tree;
            this.treeIterator = new FourColorTreeIterator<E>(tree);
        }

        private Iterator(FourColorTree<E> tree, FourColorTreeIterator<E> treeIterator) {
            this.tree = tree;
            this.treeIterator = treeIterator;
        }

        public Iterator<E> copy() {
            return new Iterator<E>(this.tree, this.treeIterator.copy());
        }

        public int getIndex() {
            return this.treeIterator.index(TARGET_INDICES);
        }

        public int getEndIndex() {
            return this.treeIterator.nodeStartIndex(TARGET_INDICES) + this.treeIterator.nodeSize(ALL_INDICES);
        }

        public int getType() {
            byte color = this.treeIterator.color();
            if (color == Tree4Deltas.INSERT) {
                return 2;
            }
            if (color == Tree4Deltas.UPDATE) {
                return 1;
            }
            if (color == Tree4Deltas.DELETE) {
                return 0;
            }
            throw new IllegalStateException();
        }

        public boolean next() {
            if (!this.hasNext()) {
                return false;
            }
            this.treeIterator.next(CHANGE_INDICES);
            return true;
        }

        public boolean nextNode() {
            if (!this.hasNextNode()) {
                return false;
            }
            this.treeIterator.nextNode(CHANGE_INDICES);
            return true;
        }

        public boolean hasNext() {
            return this.treeIterator.hasNext(CHANGE_INDICES);
        }

        public boolean hasNextNode() {
            return this.treeIterator.hasNextNode(CHANGE_INDICES);
        }

        public E getOldValue() {
            return this.treeIterator.node().get();
        }
    }

}

