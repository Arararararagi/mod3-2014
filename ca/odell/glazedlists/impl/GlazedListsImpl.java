/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.FunctionList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.impl.adt.KeyedCollection;
import ca.odell.glazedlists.impl.text.LatinDiacriticsStripper;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class GlazedListsImpl {
    private GlazedListsImpl() {
        throw new UnsupportedOperationException();
    }

    public static boolean equal(Object a, Object b) {
        if (a == b) {
            return true;
        }
        if (a == null || b == null) {
            return false;
        }
        return a.equals(b);
    }

    public static <E> List<E> concatenate(List<E> a, List<E> b) {
        ArrayList<E> aAndB = new ArrayList<E>(a.size() + b.size());
        aAndB.addAll(a);
        aAndB.addAll(b);
        return aAndB;
    }

    public static <E> void replaceAll(EventList<E> target, Collection<E> source, boolean updates, Comparator<E> comparator) {
        reference NEW_VALUE_NEEDED;
        if (comparator == null) {
            comparator = GlazedLists.comparableComparator();
        }
        int targetIndex = -1;
        Iterator<E> sourceIterator = source.iterator();
        reference targetObject = NEW_VALUE_NEEDED = Void.class;
        reference sourceObject = NEW_VALUE_NEEDED;
        do {
            if (targetObject == NEW_VALUE_NEEDED) {
                if (targetIndex < target.size()) {
                    ++targetIndex;
                }
                if (targetIndex < target.size()) {
                    targetObject = (reference)target.get(targetIndex);
                }
            }
            if (sourceObject == NEW_VALUE_NEEDED && sourceIterator.hasNext()) {
                sourceObject = (reference)sourceIterator.next();
            }
            if (targetObject == NEW_VALUE_NEEDED && sourceObject == NEW_VALUE_NEEDED) break;
            int compareResult = targetObject == NEW_VALUE_NEEDED ? 1 : (sourceObject == NEW_VALUE_NEEDED ? -1 : comparator.compare(targetObject, sourceObject));
            if (compareResult < 0) {
                target.remove(targetIndex);
                --targetIndex;
                targetObject = NEW_VALUE_NEEDED;
                continue;
            }
            if (compareResult == 0) {
                if (updates) {
                    target.set(targetIndex, sourceObject);
                }
                targetObject = NEW_VALUE_NEEDED;
                sourceObject = NEW_VALUE_NEEDED;
                continue;
            }
            if (compareResult <= 0) continue;
            target.add(targetIndex, sourceObject);
            ++targetIndex;
            sourceObject = NEW_VALUE_NEEDED;
        } while (true);
    }

    public static char[] getLatinDiacriticsStripper() {
        return LatinDiacriticsStripper.getMapper();
    }

    public static Date getMonthBegin(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return GlazedListsImpl.getMonthStart(cal);
    }

    public static Date getMonthStart(Calendar calendar) {
        calendar.set(5, 1);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        return calendar.getTime();
    }

    public static boolean isMonthStart(Calendar calendar) {
        return calendar.get(14) == 0 && calendar.get(13) == 0 && calendar.get(12) == 0 && calendar.get(11) == 0 && calendar.get(5) == 1;
    }

    public static Comparator<? extends Object> equalsComparator() {
        return new EqualsComparator();
    }

    public static <E> FunctionList.Function<E, E> identityFunction() {
        return new IdentityFunction();
    }

    public static <P, V> KeyedCollection<P, V> keyedCollection(Comparator<P> positionComparator, Comparator<V> valueComparator) {
        return new KeyedCollection<P, V>(positionComparator, new TreeMap(valueComparator));
    }

    public static <P, V> KeyedCollection<P, V> keyedCollection(Comparator<P> positionComparator) {
        return new KeyedCollection(positionComparator, new HashMap());
    }

    public static <V> boolean identityRemove(Collection<V> c, V value) {
        Iterator<V> i = c.iterator();
        while (i.hasNext()) {
            if (i.next() != value) continue;
            i.remove();
            return true;
        }
        return false;
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private static class EqualsComparator<T>
    implements Comparator<T> {
        private EqualsComparator() {
        }

        @Override
        public int compare(T alpha, T beta) {
            boolean equal = alpha == null ? beta == null : alpha.equals(beta);
            return equal ? 0 : 1;
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private static class IdentityFunction<E>
    implements FunctionList.Function<E, E> {
        private IdentityFunction() {
        }

        @Override
        public E evaluate(E sourceValue) {
            return sourceValue;
        }
    }

}

