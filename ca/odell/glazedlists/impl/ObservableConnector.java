/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl;

import ca.odell.glazedlists.ObservableElementList;
import java.util.EventListener;
import java.util.Observable;
import java.util.Observer;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class ObservableConnector<E extends Observable>
implements ObservableElementList.Connector<E>,
Observer,
EventListener {
    private ObservableElementList<? extends E> list;

    @Override
    public void update(Observable o, Object arg) {
        this.list.elementChanged(o);
    }

    @Override
    public EventListener installListener(E element) {
        element.addObserver(this);
        return this;
    }

    @Override
    public void uninstallListener(E element, EventListener listener) {
        element.deleteObserver(this);
    }

    @Override
    public void setObservableElementList(ObservableElementList<? extends E> list) {
        this.list = list;
    }
}

