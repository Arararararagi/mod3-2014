/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import java.util.Collection;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class SyncListener<E>
implements ListEventListener<E> {
    private List<E> target;
    private int targetSize;

    public SyncListener(EventList<E> source, List<E> target) {
        this.target = target;
        target.clear();
        target.addAll(source);
        this.targetSize = target.size();
        source.addListEventListener(this);
    }

    @Override
    public void listChanged(ListEvent<E> listChanges) {
        EventList<E> source = listChanges.getSourceList();
        if (this.target.size() != this.targetSize) {
            throw new IllegalStateException("Synchronize EventList target has been modified");
        }
        while (listChanges.next()) {
            int index = listChanges.getIndex();
            int type = listChanges.getType();
            if (type == 2) {
                this.target.add(index, source.get(index));
                ++this.targetSize;
                continue;
            }
            if (type == 1) {
                this.target.set(index, source.get(index));
                continue;
            }
            if (type != 0) continue;
            this.target.remove(index);
            --this.targetSize;
        }
    }
}

