/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.DisposableMap;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.FunctionList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.impl.GlazedListsImpl;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class FunctionListMap<K, V>
implements DisposableMap<K, V>,
ListEventListener<V> {
    private List<K> keyList;
    private Set<K> keySet;
    private final EventList<V> valueList;
    private Set<Map.Entry<K, V>> entrySet;
    private final FunctionList.Function<V, K> keyFunction;
    private final Map<K, V> delegate;

    public FunctionListMap(EventList<V> source, FunctionList.Function<V, K> keyFunction) {
        if (keyFunction == null) {
            throw new IllegalArgumentException("keyFunction may not be null");
        }
        this.valueList = source;
        this.valueList.addListEventListener(this);
        this.keyFunction = keyFunction;
        this.keyList = new BasicEventList<K>(source.size());
        this.delegate = new HashMap(source.size());
        int n = source.size();
        for (int i = 0; i < n; ++i) {
            this.elementAdded(i);
        }
    }

    @Override
    public void dispose() {
        this.valueList.removeListEventListener(this);
        this.keySet = null;
        this.entrySet = null;
        this.keyList.clear();
        this.delegate.clear();
    }

    @Override
    public int size() {
        return this.delegate.size();
    }

    @Override
    public boolean isEmpty() {
        return this.delegate.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return this.delegate.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return this.delegate.containsValue(value);
    }

    @Override
    public V get(Object key) {
        return this.delegate.get(key);
    }

    @Override
    public V put(K key, V value) {
        this.checkKeyValueAgreement(key, value);
        if (!this.containsKey(key)) {
            this.valueList.add(value);
            return null;
        }
        V toReplace = this.get(key);
        ListIterator<V> i = this.valueList.listIterator();
        while (i.hasNext()) {
            if (i.next() != toReplace) continue;
            i.set(value);
            return toReplace;
        }
        throw new IllegalStateException("Found key: " + key + " in delegate map but could not find corresponding value in valueList: " + toReplace);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        for (Map.Entry<K, V> entry2 : m.entrySet()) {
            this.checkKeyValueAgreement(entry2.getKey(), entry2.getValue());
        }
        for (Map.Entry<K, V> entry2 : m.entrySet()) {
            this.put(entry2.getKey(), entry2.getValue());
        }
    }

    private void checkKeyValueAgreement(K key, V value) {
        K k = this.key(value);
        if (!GlazedListsImpl.equal(key, k)) {
            throw new IllegalArgumentException("The calculated key for the given value (" + k + ") does not match the given key (" + key + ")");
        }
    }

    @Override
    public void clear() {
        this.valueList.clear();
    }

    @Override
    public V remove(Object key) {
        if (!this.containsKey(key)) {
            return null;
        }
        V value = this.get(key);
        GlazedListsImpl.identityRemove(this.valueList, value);
        return value;
    }

    @Override
    public Collection<V> values() {
        return this.valueList;
    }

    @Override
    public Set<K> keySet() {
        if (this.keySet == null) {
            this.keySet = new KeySet();
        }
        return this.keySet;
    }

    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        if (this.entrySet == null) {
            this.entrySet = new EntrySet();
        }
        return this.entrySet;
    }

    @Override
    public boolean equals(Object o) {
        return this.delegate.equals(o);
    }

    @Override
    public int hashCode() {
        return this.delegate.hashCode();
    }

    @Override
    public void listChanged(ListEvent<V> listChanges) {
        int offset = 0;
        while (listChanges.next()) {
            switch (listChanges.getType()) {
                case 0: {
                    this.elementRemoved(listChanges.getIndex() + offset);
                    break;
                }
                case 1: {
                    this.elementRemoved(listChanges.getIndex() + offset);
                    --offset;
                    break;
                }
                case 2: {
                    --offset;
                }
            }
        }
        listChanges.reset();
        while (listChanges.next()) {
            switch (listChanges.getType()) {
                case 1: {
                    this.elementAdded(listChanges.getIndex());
                    break;
                }
                case 2: {
                    this.elementAdded(listChanges.getIndex());
                }
            }
        }
    }

    private void elementAdded(int index) {
        V value = this.valueList.get(index);
        K key = this.key(value);
        this.keyList.add(index, key);
        this.putInDelegate(key, value);
    }

    private void elementRemoved(int index) {
        K key = this.keyList.remove(index);
        this.delegate.remove(key);
    }

    private void putInDelegate(K key, V value) {
        if (this.delegate.containsKey(key)) {
            throw new IllegalStateException("Detected duplicate key->value mapping: attempted to put '" + key + "' -> '" + value + "' in the map, but found '" + key + "' -> '" + this.delegate.get(key) + "' already existed.");
        }
        this.delegate.put(key, value);
    }

    private K key(V value) {
        return this.keyFunction.evaluate(value);
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class EntrySet
    extends AbstractSet<Map.Entry<K, V>> {
        private EntrySet() {
        }

        @Override
        public int size() {
            return FunctionListMap.this.keyList.size();
        }

        @Override
        public Iterator<Map.Entry<K, V>> iterator() {
            return new EntrySetIterator(FunctionListMap.this.keyList.listIterator());
        }

        @Override
        public boolean contains(Object o) {
            if (!(o instanceof Map.Entry)) {
                return false;
            }
            Map.Entry e = (Map.Entry)o;
            Object key = e.getKey();
            Object value = e.getValue();
            Object mapValue = FunctionListMap.this.get(key);
            return GlazedListsImpl.equal(value, mapValue);
        }

        @Override
        public boolean remove(Object o) {
            if (!this.contains(o)) {
                return false;
            }
            FunctionListMap.this.remove(((Map.Entry)o).getKey());
            return true;
        }

        @Override
        public void clear() {
            FunctionListMap.this.clear();
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class EntrySetIterator
    implements Iterator<Map.Entry<K, V>> {
        private final ListIterator<K> keyIter;

        EntrySetIterator(ListIterator<K> keyIter) {
            this.keyIter = keyIter;
        }

        @Override
        public boolean hasNext() {
            return this.keyIter.hasNext();
        }

        @Override
        public Map.Entry<K, V> next() {
            K key = this.keyIter.next();
            return new MapEntry(key, FunctionListMap.this.get(key));
        }

        @Override
        public void remove() {
            int index = this.keyIter.previousIndex();
            if (index == -1) {
                throw new IllegalStateException("Cannot remove() without a prior call to next()");
            }
            FunctionListMap.this.valueList.remove(index);
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class KeySet
    extends AbstractSet<K> {
        private KeySet() {
        }

        @Override
        public int size() {
            return FunctionListMap.this.keyList.size();
        }

        @Override
        public Iterator<K> iterator() {
            return new KeySetIterator(FunctionListMap.this.keyList.listIterator());
        }

        @Override
        public boolean contains(Object o) {
            return FunctionListMap.this.containsKey(o);
        }

        @Override
        public boolean remove(Object o) {
            return FunctionListMap.this.remove(o) != null;
        }

        @Override
        public void clear() {
            FunctionListMap.this.clear();
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class KeySetIterator
    implements Iterator<K> {
        private final ListIterator<K> keyIter;

        KeySetIterator(ListIterator<K> keyIter) {
            this.keyIter = keyIter;
        }

        @Override
        public boolean hasNext() {
            return this.keyIter.hasNext();
        }

        @Override
        public K next() {
            return this.keyIter.next();
        }

        @Override
        public void remove() {
            int index = this.keyIter.previousIndex();
            if (index == -1) {
                throw new IllegalStateException("Cannot remove() without a prior call to next()");
            }
            FunctionListMap.this.valueList.remove(index);
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class MapEntry
    implements Map.Entry<K, V> {
        private final K key;
        private V value;

        MapEntry(K key, V value) {
            if (value == null) {
                throw new IllegalArgumentException("value cannot be null");
            }
            this.value = value;
            this.key = key;
        }

        @Override
        public K getKey() {
            return this.key;
        }

        @Override
        public V getValue() {
            return this.value;
        }

        @Override
        public V setValue(V newValue) {
            FunctionListMap.this.checkKeyValueAgreement(this.key, newValue);
            this.value = newValue;
            return FunctionListMap.this.put(this.key, newValue);
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Map.Entry)) {
                return false;
            }
            Map.Entry e = (Map.Entry)o;
            boolean keysEqual = GlazedListsImpl.equal(this.getKey(), e.getKey());
            return keysEqual && GlazedListsImpl.equal(this.getValue(), e.getValue());
        }

        @Override
        public int hashCode() {
            return (this.key == null ? 0 : this.key.hashCode()) ^ this.value.hashCode();
        }

        public String toString() {
            return this.getKey() + "=" + this.getValue();
        }
    }

}

