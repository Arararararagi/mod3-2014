/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.DisposableMap;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.FunctionList;
import ca.odell.glazedlists.GroupingList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.impl.GlazedListsImpl;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class GroupingListMultiMap<K, V>
implements DisposableMap<K, List<V>>,
ListEventListener<List<V>> {
    private final GroupingList<V> groupingList;
    private final FunctionList<List<V>, List<V>> valueList;
    private final List<K> keyList;
    private Set<K> keySet;
    private final FunctionList.Function<V, ? extends K> keyFunction;
    private final Map<K, List<V>> delegate;
    private Set<Map.Entry<K, List<V>>> entrySet;

    public GroupingListMultiMap(EventList<V> source, FunctionList.Function<V, ? extends K> keyFunction, Comparator<? super K> keyGrouper) {
        if (keyFunction == null) {
            throw new IllegalArgumentException("keyFunction may not be null");
        }
        if (keyGrouper == null) {
            throw new IllegalArgumentException("keyGrouper may not be null");
        }
        this.keyFunction = keyFunction;
        this.groupingList = new GroupingList<V>(source, new FunctionComparator(keyFunction, keyGrouper));
        this.valueList = new FunctionList(this.groupingList, new ValueListFunction());
        this.valueList.addListEventListener(this);
        this.keyList = new BasicEventList<K>(this.groupingList.size());
        this.delegate = new HashMap<K, List<V>>(this.groupingList.size());
        for (List value : this.valueList) {
            K key = this.key((V)value);
            this.keyList.add(key);
            this.delegate.put(key, value);
        }
    }

    @Override
    public void dispose() {
        this.valueList.removeListEventListener(this);
        this.valueList.dispose();
        this.groupingList.dispose();
        this.keySet = null;
        this.entrySet = null;
        this.keyList.clear();
        this.delegate.clear();
    }

    @Override
    public int size() {
        return this.delegate.size();
    }

    @Override
    public boolean isEmpty() {
        return this.delegate.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return this.delegate.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return this.delegate.containsValue(value);
    }

    @Override
    public List<V> get(Object key) {
        return this.delegate.get(key);
    }

    @Override
    public List<V> put(K key, List<V> value) {
        this.checkKeyValueAgreement(key, (V)value);
        Object removed = this.remove(key);
        this.groupingList.add(value);
        return removed;
    }

    @Override
    public void putAll(Map<? extends K, ? extends List<V>> m) {
        for (Map.Entry<K, List<V>> entry : m.entrySet()) {
            K key = entry.getKey();
            List<V> value = entry.getValue();
            this.checkKeyValueAgreement(key, (V)value);
        }
        Iterator<Map.Entry<K, List<V>>> i = m.keySet().iterator();
        while (i.hasNext()) {
            this.remove(i.next());
        }
        this.groupingList.addAll(m.values());
    }

    private void checkKeyValueAgreement(K key, Collection<? extends V> value) {
        Iterator<V> i = value.iterator();
        while (i.hasNext()) {
            this.checkKeyValueAgreement(key, i.next());
        }
    }

    private void checkKeyValueAgreement(K key, V value) {
        K k = this.key(value);
        if (!GlazedListsImpl.equal(key, k)) {
            throw new IllegalArgumentException("The calculated key for the given value (" + k + ") does not match the given key (" + key + ")");
        }
    }

    @Override
    public void clear() {
        this.groupingList.clear();
    }

    @Override
    public List<V> remove(Object key) {
        int index = this.keyList.indexOf(key);
        return index == -1 ? null : this.groupingList.remove(index);
    }

    @Override
    public Collection<List<V>> values() {
        return this.groupingList;
    }

    @Override
    public Set<K> keySet() {
        if (this.keySet == null) {
            this.keySet = new KeySet();
        }
        return this.keySet;
    }

    @Override
    public Set<Map.Entry<K, List<V>>> entrySet() {
        if (this.entrySet == null) {
            this.entrySet = new EntrySet();
        }
        return this.entrySet;
    }

    @Override
    public boolean equals(Object o) {
        return this.delegate.equals(o);
    }

    @Override
    public int hashCode() {
        return this.delegate.hashCode();
    }

    @Override
    public void listChanged(ListEvent<List<V>> listChanges) {
        while (listChanges.next()) {
            int changeIndex = listChanges.getIndex();
            int changeType = listChanges.getType();
            if (changeType == 2) {
                List<V> inserted = listChanges.getSourceList().get(changeIndex);
                K key = this.key((V)inserted);
                this.keyList.add(changeIndex, key);
                this.delegate.put(key, inserted);
                continue;
            }
            if (changeType != 0) continue;
            K deleted = this.keyList.remove(changeIndex);
            this.delegate.remove(deleted);
        }
    }

    private K key(List<V> values) {
        return this.key(values.get(0));
    }

    private K key(V value) {
        return this.keyFunction.evaluate(value);
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class EntrySet
    extends AbstractSet<Map.Entry<K, List<V>>> {
        private EntrySet() {
        }

        @Override
        public int size() {
            return GroupingListMultiMap.this.keyList.size();
        }

        @Override
        public Iterator<Map.Entry<K, List<V>>> iterator() {
            return new EntrySetIterator(GroupingListMultiMap.this.keyList.listIterator());
        }

        @Override
        public boolean contains(Object o) {
            if (!(o instanceof Map.Entry)) {
                return false;
            }
            Map.Entry e = (Map.Entry)o;
            Object key = e.getKey();
            List value = (List)e.getValue();
            Object mapValue = GroupingListMultiMap.this.get(key);
            return GlazedListsImpl.equal(value, mapValue);
        }

        @Override
        public boolean remove(Object o) {
            if (!this.contains(o)) {
                return false;
            }
            GroupingListMultiMap.this.remove(((Map.Entry)o).getKey());
            return true;
        }

        @Override
        public void clear() {
            GroupingListMultiMap.this.clear();
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class EntrySetIterator
    implements Iterator<Map.Entry<K, List<V>>> {
        private final ListIterator<K> keyIter;

        EntrySetIterator(ListIterator<K> keyIter) {
            this.keyIter = keyIter;
        }

        @Override
        public boolean hasNext() {
            return this.keyIter.hasNext();
        }

        @Override
        public Map.Entry<K, List<V>> next() {
            K key = this.keyIter.next();
            return new MultiMapEntry(key, GroupingListMultiMap.this.get(key));
        }

        @Override
        public void remove() {
            int index = this.keyIter.previousIndex();
            if (index == -1) {
                throw new IllegalStateException("Cannot remove() without a prior call to next()");
            }
            GroupingListMultiMap.this.groupingList.remove(index);
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private final class FunctionComparator
    implements Comparator<V> {
        private final Comparator<? super K> delegate;
        private final FunctionList.Function<V, ? extends K> function;

        FunctionComparator(FunctionList.Function<V, ? extends K> function, Comparator<? super K> delegate) {
            this.function = function;
            this.delegate = delegate;
        }

        @Override
        public int compare(V o1, V o2) {
            K k1 = this.function.evaluate(o1);
            K k2 = this.function.evaluate(o2);
            return this.delegate.compare(k1, k2);
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class KeySet
    extends AbstractSet<K> {
        private KeySet() {
        }

        @Override
        public int size() {
            return GroupingListMultiMap.this.keyList.size();
        }

        @Override
        public Iterator<K> iterator() {
            return new KeySetIterator(GroupingListMultiMap.this.keyList.listIterator());
        }

        @Override
        public boolean contains(Object o) {
            return GroupingListMultiMap.this.containsKey(o);
        }

        @Override
        public boolean remove(Object o) {
            return GroupingListMultiMap.this.remove(o) != null;
        }

        @Override
        public void clear() {
            GroupingListMultiMap.this.clear();
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class KeySetIterator
    implements Iterator<K> {
        private final ListIterator<K> keyIter;

        KeySetIterator(ListIterator<K> keyIter) {
            this.keyIter = keyIter;
        }

        @Override
        public boolean hasNext() {
            return this.keyIter.hasNext();
        }

        @Override
        public K next() {
            return this.keyIter.next();
        }

        @Override
        public void remove() {
            int index = this.keyIter.previousIndex();
            if (index == -1) {
                throw new IllegalStateException("Cannot remove() without a prior call to next()");
            }
            GroupingListMultiMap.this.groupingList.remove(index);
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class MultiMapEntry
    implements Map.Entry<K, List<V>> {
        private final K key;
        private List<V> value;

        MultiMapEntry(K key, List<V> value) {
            if (value == null) {
                throw new IllegalArgumentException("value cannot be null");
            }
            this.value = value;
            this.key = key;
        }

        @Override
        public K getKey() {
            return this.key;
        }

        @Override
        public List<V> getValue() {
            return this.value;
        }

        @Override
        public List<V> setValue(List<V> newValue) {
            GroupingListMultiMap.this.checkKeyValueAgreement((K)this.getKey(), (V)newValue);
            ArrayList<V> oldValue = new ArrayList<V>(this.value);
            this.value.addAll(newValue);
            this.value.removeAll(oldValue);
            return oldValue;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Map.Entry)) {
                return false;
            }
            Map.Entry e = (Map.Entry)o;
            boolean keysEqual = GlazedListsImpl.equal(this.getKey(), e.getKey());
            return keysEqual && GlazedListsImpl.equal(this.getValue(), e.getValue());
        }

        @Override
        public int hashCode() {
            return (this.key == null ? 0 : this.key.hashCode()) ^ this.value.hashCode();
        }

        public String toString() {
            return this.getKey() + "=" + this.getValue();
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private final class ValueList
    implements List<V> {
        private final List<V> delegate;
        private final K key;

        public ValueList(List<V> delegate) {
            this.delegate = delegate;
            this.key = GroupingListMultiMap.this.key(delegate.get(0));
        }

        @Override
        public int size() {
            return this.delegate.size();
        }

        @Override
        public boolean isEmpty() {
            return this.delegate.isEmpty();
        }

        @Override
        public boolean contains(Object o) {
            return this.delegate.contains(o);
        }

        @Override
        public Iterator<V> iterator() {
            return this.delegate.iterator();
        }

        @Override
        public Object[] toArray() {
            return this.delegate.toArray();
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return this.delegate.toArray(a);
        }

        @Override
        public boolean add(V o) {
            GroupingListMultiMap.this.checkKeyValueAgreement(this.key, o);
            return this.delegate.add(o);
        }

        @Override
        public boolean addAll(Collection<? extends V> c) {
            GroupingListMultiMap.this.checkKeyValueAgreement((K)this.key, (V)c);
            return this.delegate.addAll(c);
        }

        @Override
        public boolean addAll(int index, Collection<? extends V> c) {
            GroupingListMultiMap.this.checkKeyValueAgreement((K)this.key, (V)c);
            return this.delegate.addAll(index, c);
        }

        @Override
        public void add(int index, V element) {
            GroupingListMultiMap.this.checkKeyValueAgreement(this.key, element);
            this.delegate.add(index, element);
        }

        @Override
        public V set(int index, V element) {
            GroupingListMultiMap.this.checkKeyValueAgreement(this.key, element);
            return this.delegate.set(index, element);
        }

        @Override
        public List<V> subList(int fromIndex, int toIndex) {
            return new ValueList(this.delegate.subList(fromIndex, toIndex));
        }

        @Override
        public ListIterator<V> listIterator() {
            return new ValueListIterator(this.delegate.listIterator());
        }

        @Override
        public ListIterator<V> listIterator(int index) {
            return new ValueListIterator(this.delegate.listIterator(index));
        }

        @Override
        public boolean remove(Object o) {
            return this.delegate.remove(o);
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return this.delegate.containsAll(c);
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return this.delegate.removeAll(c);
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return this.delegate.retainAll(c);
        }

        @Override
        public void clear() {
            this.delegate.clear();
        }

        @Override
        public boolean equals(Object o) {
            return this.delegate.equals(o);
        }

        @Override
        public int hashCode() {
            return this.delegate.hashCode();
        }

        @Override
        public V get(int index) {
            return this.delegate.get(index);
        }

        @Override
        public V remove(int index) {
            return this.delegate.remove(index);
        }

        @Override
        public int indexOf(Object o) {
            return this.delegate.indexOf(o);
        }

        @Override
        public int lastIndexOf(Object o) {
            return this.delegate.lastIndexOf(o);
        }

        public String toString() {
            return this.delegate.toString();
        }

        /*
         * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
         */
        private final class ValueListIterator
        implements ListIterator<V> {
            private final ListIterator<V> delegate;

            public ValueListIterator(ListIterator<V> delegate) {
                this.delegate = delegate;
            }

            @Override
            public void set(V o) {
                GroupingListMultiMap.this.checkKeyValueAgreement(ValueList.this.key, o);
                this.delegate.set(o);
            }

            @Override
            public void add(V o) {
                GroupingListMultiMap.this.checkKeyValueAgreement(ValueList.this.key, o);
                this.delegate.add(o);
            }

            @Override
            public boolean hasNext() {
                return this.delegate.hasNext();
            }

            @Override
            public V next() {
                return this.delegate.next();
            }

            @Override
            public boolean hasPrevious() {
                return this.delegate.hasPrevious();
            }

            @Override
            public V previous() {
                return this.delegate.previous();
            }

            @Override
            public int nextIndex() {
                return this.delegate.nextIndex();
            }

            @Override
            public void remove() {
                this.delegate.remove();
            }

            @Override
            public int previousIndex() {
                return this.delegate.previousIndex();
            }
        }

    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private final class ValueListFunction
    implements FunctionList.Function<List<V>, List<V>> {
        private ValueListFunction() {
        }

        @Override
        public List<V> evaluate(List<V> sourceValue) {
            return new ValueList(sourceValue);
        }
    }

}

