/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.event.ListEventPublisher;
import ca.odell.glazedlists.impl.WeakReferenceProxy;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class EventListIterator<E>
implements ListIterator<E>,
ListEventListener<E> {
    private EventList<E> source;
    private int nextIndex;
    private int lastIndex = -1;

    public EventListIterator(EventList<E> source) {
        this(source, 0, true);
    }

    public EventListIterator(EventList<E> source, int nextIndex) {
        this(source, nextIndex, true);
    }

    public EventListIterator(EventList<E> source, int nextIndex, boolean automaticallyRemove) {
        this.source = source;
        this.nextIndex = nextIndex;
        if (automaticallyRemove) {
            WeakReferenceProxy<E> gcProxy = new WeakReferenceProxy<E>(source, this);
            source.addListEventListener(gcProxy);
            source.getPublisher().removeDependency(source, gcProxy);
        } else {
            source.addListEventListener(this);
            source.getPublisher().removeDependency(source, this);
        }
    }

    @Override
    public boolean hasNext() {
        return this.nextIndex < this.source.size();
    }

    @Override
    public E next() {
        if (this.nextIndex == this.source.size()) {
            throw new NoSuchElementException("Cannot retrieve element " + this.nextIndex + " on a list of size " + this.source.size());
        }
        this.lastIndex = this.nextIndex++;
        return this.source.get(this.lastIndex);
    }

    @Override
    public int nextIndex() {
        return this.nextIndex;
    }

    @Override
    public boolean hasPrevious() {
        return this.nextIndex > 0;
    }

    @Override
    public E previous() {
        if (this.nextIndex == 0) {
            throw new NoSuchElementException("Cannot retrieve element " + this.nextIndex + " on a list of size " + this.source.size());
        }
        --this.nextIndex;
        this.lastIndex = this.nextIndex;
        return this.source.get(this.nextIndex);
    }

    @Override
    public int previousIndex() {
        return this.nextIndex - 1;
    }

    @Override
    public void add(E o) {
        this.source.add(this.nextIndex, o);
    }

    @Override
    public void remove() {
        if (this.lastIndex == -1) {
            throw new IllegalStateException("Cannot remove() without a prior call to next() or previous()");
        }
        this.source.remove(this.lastIndex);
    }

    @Override
    public void set(E o) {
        if (this.lastIndex == -1) {
            throw new IllegalStateException("Cannot set() without a prior call to next() or previous()");
        }
        this.source.set(this.lastIndex, o);
    }

    @Override
    public void listChanged(ListEvent<E> listChanges) {
        while (listChanges.next()) {
            int changeIndex = listChanges.getIndex();
            int changeType = listChanges.getType();
            if (changeType == 2) {
                if (changeIndex <= this.nextIndex) {
                    ++this.nextIndex;
                }
                if (this.lastIndex == -1 || changeIndex > this.lastIndex) continue;
                ++this.lastIndex;
                continue;
            }
            if (changeType != 0) continue;
            if (changeIndex < this.nextIndex) {
                --this.nextIndex;
            }
            if (this.lastIndex != -1 && changeIndex < this.lastIndex) {
                --this.lastIndex;
                continue;
            }
            if (this.lastIndex == -1 || changeIndex != this.lastIndex) continue;
            this.lastIndex = -1;
        }
    }
}

