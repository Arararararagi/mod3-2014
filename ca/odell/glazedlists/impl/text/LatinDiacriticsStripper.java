/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.text;

public final class LatinDiacriticsStripper {
    private static final char[] MAPPER = new char[592];

    public static char[] getMapper() {
        return MAPPER;
    }

    static {
        LatinDiacriticsStripper.MAPPER[0] = '\u0000';
        LatinDiacriticsStripper.MAPPER[1] = '\u0001';
        LatinDiacriticsStripper.MAPPER[2] = 2;
        LatinDiacriticsStripper.MAPPER[3] = 3;
        LatinDiacriticsStripper.MAPPER[4] = 4;
        LatinDiacriticsStripper.MAPPER[5] = 5;
        LatinDiacriticsStripper.MAPPER[6] = 6;
        LatinDiacriticsStripper.MAPPER[7] = 7;
        LatinDiacriticsStripper.MAPPER[8] = 8;
        LatinDiacriticsStripper.MAPPER[9] = 9;
        LatinDiacriticsStripper.MAPPER[10] = 10;
        LatinDiacriticsStripper.MAPPER[11] = 11;
        LatinDiacriticsStripper.MAPPER[12] = 12;
        LatinDiacriticsStripper.MAPPER[13] = 13;
        LatinDiacriticsStripper.MAPPER[14] = 14;
        LatinDiacriticsStripper.MAPPER[15] = 15;
        LatinDiacriticsStripper.MAPPER[16] = 16;
        LatinDiacriticsStripper.MAPPER[17] = 17;
        LatinDiacriticsStripper.MAPPER[18] = 18;
        LatinDiacriticsStripper.MAPPER[19] = 19;
        LatinDiacriticsStripper.MAPPER[20] = 20;
        LatinDiacriticsStripper.MAPPER[21] = 21;
        LatinDiacriticsStripper.MAPPER[22] = 22;
        LatinDiacriticsStripper.MAPPER[23] = 23;
        LatinDiacriticsStripper.MAPPER[24] = 24;
        LatinDiacriticsStripper.MAPPER[25] = 25;
        LatinDiacriticsStripper.MAPPER[26] = 26;
        LatinDiacriticsStripper.MAPPER[27] = 27;
        LatinDiacriticsStripper.MAPPER[28] = 28;
        LatinDiacriticsStripper.MAPPER[29] = 29;
        LatinDiacriticsStripper.MAPPER[30] = 30;
        LatinDiacriticsStripper.MAPPER[31] = 31;
        LatinDiacriticsStripper.MAPPER[32] = 32;
        LatinDiacriticsStripper.MAPPER[33] = 33;
        LatinDiacriticsStripper.MAPPER[34] = 34;
        LatinDiacriticsStripper.MAPPER[35] = 35;
        LatinDiacriticsStripper.MAPPER[36] = 36;
        LatinDiacriticsStripper.MAPPER[37] = 37;
        LatinDiacriticsStripper.MAPPER[38] = 38;
        LatinDiacriticsStripper.MAPPER[39] = 39;
        LatinDiacriticsStripper.MAPPER[40] = 40;
        LatinDiacriticsStripper.MAPPER[41] = 41;
        LatinDiacriticsStripper.MAPPER[42] = 42;
        LatinDiacriticsStripper.MAPPER[43] = 43;
        LatinDiacriticsStripper.MAPPER[44] = 44;
        LatinDiacriticsStripper.MAPPER[45] = 45;
        LatinDiacriticsStripper.MAPPER[46] = 46;
        LatinDiacriticsStripper.MAPPER[47] = 47;
        LatinDiacriticsStripper.MAPPER[48] = 48;
        LatinDiacriticsStripper.MAPPER[49] = 49;
        LatinDiacriticsStripper.MAPPER[50] = 50;
        LatinDiacriticsStripper.MAPPER[51] = 51;
        LatinDiacriticsStripper.MAPPER[52] = 52;
        LatinDiacriticsStripper.MAPPER[53] = 53;
        LatinDiacriticsStripper.MAPPER[54] = 54;
        LatinDiacriticsStripper.MAPPER[55] = 55;
        LatinDiacriticsStripper.MAPPER[56] = 56;
        LatinDiacriticsStripper.MAPPER[57] = 57;
        LatinDiacriticsStripper.MAPPER[58] = 58;
        LatinDiacriticsStripper.MAPPER[59] = 59;
        LatinDiacriticsStripper.MAPPER[60] = 60;
        LatinDiacriticsStripper.MAPPER[61] = 61;
        LatinDiacriticsStripper.MAPPER[62] = 62;
        LatinDiacriticsStripper.MAPPER[63] = 63;
        LatinDiacriticsStripper.MAPPER[64] = 64;
        LatinDiacriticsStripper.MAPPER[65] = 65;
        LatinDiacriticsStripper.MAPPER[66] = 66;
        LatinDiacriticsStripper.MAPPER[67] = 67;
        LatinDiacriticsStripper.MAPPER[68] = 68;
        LatinDiacriticsStripper.MAPPER[69] = 69;
        LatinDiacriticsStripper.MAPPER[70] = 70;
        LatinDiacriticsStripper.MAPPER[71] = 71;
        LatinDiacriticsStripper.MAPPER[72] = 72;
        LatinDiacriticsStripper.MAPPER[73] = 73;
        LatinDiacriticsStripper.MAPPER[74] = 74;
        LatinDiacriticsStripper.MAPPER[75] = 75;
        LatinDiacriticsStripper.MAPPER[76] = 76;
        LatinDiacriticsStripper.MAPPER[77] = 77;
        LatinDiacriticsStripper.MAPPER[78] = 78;
        LatinDiacriticsStripper.MAPPER[79] = 79;
        LatinDiacriticsStripper.MAPPER[80] = 80;
        LatinDiacriticsStripper.MAPPER[81] = 81;
        LatinDiacriticsStripper.MAPPER[82] = 82;
        LatinDiacriticsStripper.MAPPER[83] = 83;
        LatinDiacriticsStripper.MAPPER[84] = 84;
        LatinDiacriticsStripper.MAPPER[85] = 85;
        LatinDiacriticsStripper.MAPPER[86] = 86;
        LatinDiacriticsStripper.MAPPER[87] = 87;
        LatinDiacriticsStripper.MAPPER[88] = 88;
        LatinDiacriticsStripper.MAPPER[89] = 89;
        LatinDiacriticsStripper.MAPPER[90] = 90;
        LatinDiacriticsStripper.MAPPER[91] = 91;
        LatinDiacriticsStripper.MAPPER[92] = 92;
        LatinDiacriticsStripper.MAPPER[93] = 93;
        LatinDiacriticsStripper.MAPPER[94] = 94;
        LatinDiacriticsStripper.MAPPER[95] = 95;
        LatinDiacriticsStripper.MAPPER[96] = 96;
        LatinDiacriticsStripper.MAPPER[97] = 97;
        LatinDiacriticsStripper.MAPPER[98] = 98;
        LatinDiacriticsStripper.MAPPER[99] = 99;
        LatinDiacriticsStripper.MAPPER[100] = 100;
        LatinDiacriticsStripper.MAPPER[101] = 101;
        LatinDiacriticsStripper.MAPPER[102] = 102;
        LatinDiacriticsStripper.MAPPER[103] = 103;
        LatinDiacriticsStripper.MAPPER[104] = 104;
        LatinDiacriticsStripper.MAPPER[105] = 105;
        LatinDiacriticsStripper.MAPPER[106] = 106;
        LatinDiacriticsStripper.MAPPER[107] = 107;
        LatinDiacriticsStripper.MAPPER[108] = 108;
        LatinDiacriticsStripper.MAPPER[109] = 109;
        LatinDiacriticsStripper.MAPPER[110] = 110;
        LatinDiacriticsStripper.MAPPER[111] = 111;
        LatinDiacriticsStripper.MAPPER[112] = 112;
        LatinDiacriticsStripper.MAPPER[113] = 113;
        LatinDiacriticsStripper.MAPPER[114] = 114;
        LatinDiacriticsStripper.MAPPER[115] = 115;
        LatinDiacriticsStripper.MAPPER[116] = 116;
        LatinDiacriticsStripper.MAPPER[117] = 117;
        LatinDiacriticsStripper.MAPPER[118] = 118;
        LatinDiacriticsStripper.MAPPER[119] = 119;
        LatinDiacriticsStripper.MAPPER[120] = 120;
        LatinDiacriticsStripper.MAPPER[121] = 121;
        LatinDiacriticsStripper.MAPPER[122] = 122;
        LatinDiacriticsStripper.MAPPER[123] = 123;
        LatinDiacriticsStripper.MAPPER[124] = 124;
        LatinDiacriticsStripper.MAPPER[125] = 125;
        LatinDiacriticsStripper.MAPPER[126] = 126;
        LatinDiacriticsStripper.MAPPER[127] = 127;
        LatinDiacriticsStripper.MAPPER[128] = 128;
        LatinDiacriticsStripper.MAPPER[129] = 129;
        LatinDiacriticsStripper.MAPPER[130] = 130;
        LatinDiacriticsStripper.MAPPER[131] = 131;
        LatinDiacriticsStripper.MAPPER[132] = 132;
        LatinDiacriticsStripper.MAPPER[133] = 133;
        LatinDiacriticsStripper.MAPPER[134] = 134;
        LatinDiacriticsStripper.MAPPER[135] = 135;
        LatinDiacriticsStripper.MAPPER[136] = 136;
        LatinDiacriticsStripper.MAPPER[137] = 137;
        LatinDiacriticsStripper.MAPPER[138] = 138;
        LatinDiacriticsStripper.MAPPER[139] = 139;
        LatinDiacriticsStripper.MAPPER[140] = 140;
        LatinDiacriticsStripper.MAPPER[141] = 141;
        LatinDiacriticsStripper.MAPPER[142] = 142;
        LatinDiacriticsStripper.MAPPER[143] = 143;
        LatinDiacriticsStripper.MAPPER[144] = 144;
        LatinDiacriticsStripper.MAPPER[145] = 145;
        LatinDiacriticsStripper.MAPPER[146] = 146;
        LatinDiacriticsStripper.MAPPER[147] = 147;
        LatinDiacriticsStripper.MAPPER[148] = 148;
        LatinDiacriticsStripper.MAPPER[149] = 149;
        LatinDiacriticsStripper.MAPPER[150] = 150;
        LatinDiacriticsStripper.MAPPER[151] = 151;
        LatinDiacriticsStripper.MAPPER[152] = 152;
        LatinDiacriticsStripper.MAPPER[153] = 153;
        LatinDiacriticsStripper.MAPPER[154] = 154;
        LatinDiacriticsStripper.MAPPER[155] = 155;
        LatinDiacriticsStripper.MAPPER[156] = 156;
        LatinDiacriticsStripper.MAPPER[157] = 157;
        LatinDiacriticsStripper.MAPPER[158] = 158;
        LatinDiacriticsStripper.MAPPER[159] = 159;
        LatinDiacriticsStripper.MAPPER[160] = 160;
        LatinDiacriticsStripper.MAPPER[161] = 161;
        LatinDiacriticsStripper.MAPPER[162] = 162;
        LatinDiacriticsStripper.MAPPER[163] = 163;
        LatinDiacriticsStripper.MAPPER[164] = 164;
        LatinDiacriticsStripper.MAPPER[165] = 165;
        LatinDiacriticsStripper.MAPPER[166] = 166;
        LatinDiacriticsStripper.MAPPER[167] = 167;
        LatinDiacriticsStripper.MAPPER[168] = 168;
        LatinDiacriticsStripper.MAPPER[169] = 169;
        LatinDiacriticsStripper.MAPPER[170] = 170;
        LatinDiacriticsStripper.MAPPER[171] = 171;
        LatinDiacriticsStripper.MAPPER[172] = 172;
        LatinDiacriticsStripper.MAPPER[173] = 173;
        LatinDiacriticsStripper.MAPPER[174] = 174;
        LatinDiacriticsStripper.MAPPER[175] = 175;
        LatinDiacriticsStripper.MAPPER[176] = 176;
        LatinDiacriticsStripper.MAPPER[177] = 177;
        LatinDiacriticsStripper.MAPPER[178] = 178;
        LatinDiacriticsStripper.MAPPER[179] = 179;
        LatinDiacriticsStripper.MAPPER[180] = 180;
        LatinDiacriticsStripper.MAPPER[181] = 181;
        LatinDiacriticsStripper.MAPPER[182] = 182;
        LatinDiacriticsStripper.MAPPER[183] = 183;
        LatinDiacriticsStripper.MAPPER[184] = 184;
        LatinDiacriticsStripper.MAPPER[185] = 185;
        LatinDiacriticsStripper.MAPPER[186] = 186;
        LatinDiacriticsStripper.MAPPER[187] = 187;
        LatinDiacriticsStripper.MAPPER[188] = 188;
        LatinDiacriticsStripper.MAPPER[189] = 189;
        LatinDiacriticsStripper.MAPPER[190] = 190;
        LatinDiacriticsStripper.MAPPER[191] = 191;
        LatinDiacriticsStripper.MAPPER[192] = 65;
        LatinDiacriticsStripper.MAPPER[193] = 65;
        LatinDiacriticsStripper.MAPPER[194] = 65;
        LatinDiacriticsStripper.MAPPER[195] = 65;
        LatinDiacriticsStripper.MAPPER[196] = 65;
        LatinDiacriticsStripper.MAPPER[197] = 65;
        LatinDiacriticsStripper.MAPPER[198] = 198;
        LatinDiacriticsStripper.MAPPER[199] = 67;
        LatinDiacriticsStripper.MAPPER[200] = 69;
        LatinDiacriticsStripper.MAPPER[201] = 69;
        LatinDiacriticsStripper.MAPPER[202] = 69;
        LatinDiacriticsStripper.MAPPER[203] = 69;
        LatinDiacriticsStripper.MAPPER[204] = 73;
        LatinDiacriticsStripper.MAPPER[205] = 73;
        LatinDiacriticsStripper.MAPPER[206] = 73;
        LatinDiacriticsStripper.MAPPER[207] = 73;
        LatinDiacriticsStripper.MAPPER[208] = 208;
        LatinDiacriticsStripper.MAPPER[209] = 78;
        LatinDiacriticsStripper.MAPPER[210] = 79;
        LatinDiacriticsStripper.MAPPER[211] = 79;
        LatinDiacriticsStripper.MAPPER[212] = 79;
        LatinDiacriticsStripper.MAPPER[213] = 79;
        LatinDiacriticsStripper.MAPPER[214] = 79;
        LatinDiacriticsStripper.MAPPER[215] = 215;
        LatinDiacriticsStripper.MAPPER[216] = 216;
        LatinDiacriticsStripper.MAPPER[217] = 85;
        LatinDiacriticsStripper.MAPPER[218] = 85;
        LatinDiacriticsStripper.MAPPER[219] = 85;
        LatinDiacriticsStripper.MAPPER[220] = 85;
        LatinDiacriticsStripper.MAPPER[221] = 89;
        LatinDiacriticsStripper.MAPPER[222] = 222;
        LatinDiacriticsStripper.MAPPER[223] = 223;
        LatinDiacriticsStripper.MAPPER[224] = 97;
        LatinDiacriticsStripper.MAPPER[225] = 97;
        LatinDiacriticsStripper.MAPPER[226] = 97;
        LatinDiacriticsStripper.MAPPER[227] = 97;
        LatinDiacriticsStripper.MAPPER[228] = 97;
        LatinDiacriticsStripper.MAPPER[229] = 97;
        LatinDiacriticsStripper.MAPPER[230] = 230;
        LatinDiacriticsStripper.MAPPER[231] = 99;
        LatinDiacriticsStripper.MAPPER[232] = 101;
        LatinDiacriticsStripper.MAPPER[233] = 101;
        LatinDiacriticsStripper.MAPPER[234] = 101;
        LatinDiacriticsStripper.MAPPER[235] = 101;
        LatinDiacriticsStripper.MAPPER[236] = 105;
        LatinDiacriticsStripper.MAPPER[237] = 105;
        LatinDiacriticsStripper.MAPPER[238] = 105;
        LatinDiacriticsStripper.MAPPER[239] = 105;
        LatinDiacriticsStripper.MAPPER[240] = 240;
        LatinDiacriticsStripper.MAPPER[241] = 110;
        LatinDiacriticsStripper.MAPPER[242] = 111;
        LatinDiacriticsStripper.MAPPER[243] = 111;
        LatinDiacriticsStripper.MAPPER[244] = 111;
        LatinDiacriticsStripper.MAPPER[245] = 111;
        LatinDiacriticsStripper.MAPPER[246] = 111;
        LatinDiacriticsStripper.MAPPER[247] = 247;
        LatinDiacriticsStripper.MAPPER[248] = 248;
        LatinDiacriticsStripper.MAPPER[249] = 117;
        LatinDiacriticsStripper.MAPPER[250] = 117;
        LatinDiacriticsStripper.MAPPER[251] = 117;
        LatinDiacriticsStripper.MAPPER[252] = 117;
        LatinDiacriticsStripper.MAPPER[253] = 121;
        LatinDiacriticsStripper.MAPPER[254] = 254;
        LatinDiacriticsStripper.MAPPER[255] = 121;
        LatinDiacriticsStripper.MAPPER[256] = 65;
        LatinDiacriticsStripper.MAPPER[257] = 97;
        LatinDiacriticsStripper.MAPPER[258] = 65;
        LatinDiacriticsStripper.MAPPER[259] = 97;
        LatinDiacriticsStripper.MAPPER[260] = 65;
        LatinDiacriticsStripper.MAPPER[261] = 97;
        LatinDiacriticsStripper.MAPPER[262] = 67;
        LatinDiacriticsStripper.MAPPER[263] = 99;
        LatinDiacriticsStripper.MAPPER[264] = 67;
        LatinDiacriticsStripper.MAPPER[265] = 99;
        LatinDiacriticsStripper.MAPPER[266] = 67;
        LatinDiacriticsStripper.MAPPER[267] = 99;
        LatinDiacriticsStripper.MAPPER[268] = 67;
        LatinDiacriticsStripper.MAPPER[269] = 99;
        LatinDiacriticsStripper.MAPPER[270] = 68;
        LatinDiacriticsStripper.MAPPER[271] = 100;
        LatinDiacriticsStripper.MAPPER[272] = 272;
        LatinDiacriticsStripper.MAPPER[273] = 273;
        LatinDiacriticsStripper.MAPPER[274] = 69;
        LatinDiacriticsStripper.MAPPER[275] = 101;
        LatinDiacriticsStripper.MAPPER[276] = 69;
        LatinDiacriticsStripper.MAPPER[277] = 101;
        LatinDiacriticsStripper.MAPPER[278] = 69;
        LatinDiacriticsStripper.MAPPER[279] = 101;
        LatinDiacriticsStripper.MAPPER[280] = 69;
        LatinDiacriticsStripper.MAPPER[281] = 101;
        LatinDiacriticsStripper.MAPPER[282] = 69;
        LatinDiacriticsStripper.MAPPER[283] = 101;
        LatinDiacriticsStripper.MAPPER[284] = 71;
        LatinDiacriticsStripper.MAPPER[285] = 103;
        LatinDiacriticsStripper.MAPPER[286] = 71;
        LatinDiacriticsStripper.MAPPER[287] = 103;
        LatinDiacriticsStripper.MAPPER[288] = 71;
        LatinDiacriticsStripper.MAPPER[289] = 103;
        LatinDiacriticsStripper.MAPPER[290] = 71;
        LatinDiacriticsStripper.MAPPER[291] = 103;
        LatinDiacriticsStripper.MAPPER[292] = 72;
        LatinDiacriticsStripper.MAPPER[293] = 104;
        LatinDiacriticsStripper.MAPPER[294] = 294;
        LatinDiacriticsStripper.MAPPER[295] = 295;
        LatinDiacriticsStripper.MAPPER[296] = 73;
        LatinDiacriticsStripper.MAPPER[297] = 105;
        LatinDiacriticsStripper.MAPPER[298] = 73;
        LatinDiacriticsStripper.MAPPER[299] = 105;
        LatinDiacriticsStripper.MAPPER[300] = 73;
        LatinDiacriticsStripper.MAPPER[301] = 105;
        LatinDiacriticsStripper.MAPPER[302] = 73;
        LatinDiacriticsStripper.MAPPER[303] = 105;
        LatinDiacriticsStripper.MAPPER[304] = 73;
        LatinDiacriticsStripper.MAPPER[305] = 305;
        LatinDiacriticsStripper.MAPPER[306] = 306;
        LatinDiacriticsStripper.MAPPER[307] = 307;
        LatinDiacriticsStripper.MAPPER[308] = 74;
        LatinDiacriticsStripper.MAPPER[309] = 106;
        LatinDiacriticsStripper.MAPPER[310] = 75;
        LatinDiacriticsStripper.MAPPER[311] = 107;
        LatinDiacriticsStripper.MAPPER[312] = 312;
        LatinDiacriticsStripper.MAPPER[313] = 76;
        LatinDiacriticsStripper.MAPPER[314] = 108;
        LatinDiacriticsStripper.MAPPER[315] = 76;
        LatinDiacriticsStripper.MAPPER[316] = 108;
        LatinDiacriticsStripper.MAPPER[317] = 76;
        LatinDiacriticsStripper.MAPPER[318] = 108;
        LatinDiacriticsStripper.MAPPER[319] = 319;
        LatinDiacriticsStripper.MAPPER[320] = 320;
        LatinDiacriticsStripper.MAPPER[321] = 321;
        LatinDiacriticsStripper.MAPPER[322] = 322;
        LatinDiacriticsStripper.MAPPER[323] = 78;
        LatinDiacriticsStripper.MAPPER[324] = 110;
        LatinDiacriticsStripper.MAPPER[325] = 78;
        LatinDiacriticsStripper.MAPPER[326] = 110;
        LatinDiacriticsStripper.MAPPER[327] = 78;
        LatinDiacriticsStripper.MAPPER[328] = 110;
        LatinDiacriticsStripper.MAPPER[329] = 329;
        LatinDiacriticsStripper.MAPPER[330] = 330;
        LatinDiacriticsStripper.MAPPER[331] = 331;
        LatinDiacriticsStripper.MAPPER[332] = 79;
        LatinDiacriticsStripper.MAPPER[333] = 111;
        LatinDiacriticsStripper.MAPPER[334] = 79;
        LatinDiacriticsStripper.MAPPER[335] = 111;
        LatinDiacriticsStripper.MAPPER[336] = 79;
        LatinDiacriticsStripper.MAPPER[337] = 111;
        LatinDiacriticsStripper.MAPPER[338] = 338;
        LatinDiacriticsStripper.MAPPER[339] = 339;
        LatinDiacriticsStripper.MAPPER[340] = 82;
        LatinDiacriticsStripper.MAPPER[341] = 114;
        LatinDiacriticsStripper.MAPPER[342] = 82;
        LatinDiacriticsStripper.MAPPER[343] = 114;
        LatinDiacriticsStripper.MAPPER[344] = 82;
        LatinDiacriticsStripper.MAPPER[345] = 114;
        LatinDiacriticsStripper.MAPPER[346] = 83;
        LatinDiacriticsStripper.MAPPER[347] = 115;
        LatinDiacriticsStripper.MAPPER[348] = 83;
        LatinDiacriticsStripper.MAPPER[349] = 115;
        LatinDiacriticsStripper.MAPPER[350] = 83;
        LatinDiacriticsStripper.MAPPER[351] = 115;
        LatinDiacriticsStripper.MAPPER[352] = 83;
        LatinDiacriticsStripper.MAPPER[353] = 115;
        LatinDiacriticsStripper.MAPPER[354] = 84;
        LatinDiacriticsStripper.MAPPER[355] = 116;
        LatinDiacriticsStripper.MAPPER[356] = 84;
        LatinDiacriticsStripper.MAPPER[357] = 116;
        LatinDiacriticsStripper.MAPPER[358] = 358;
        LatinDiacriticsStripper.MAPPER[359] = 359;
        LatinDiacriticsStripper.MAPPER[360] = 85;
        LatinDiacriticsStripper.MAPPER[361] = 117;
        LatinDiacriticsStripper.MAPPER[362] = 85;
        LatinDiacriticsStripper.MAPPER[363] = 117;
        LatinDiacriticsStripper.MAPPER[364] = 85;
        LatinDiacriticsStripper.MAPPER[365] = 117;
        LatinDiacriticsStripper.MAPPER[366] = 85;
        LatinDiacriticsStripper.MAPPER[367] = 117;
        LatinDiacriticsStripper.MAPPER[368] = 85;
        LatinDiacriticsStripper.MAPPER[369] = 117;
        LatinDiacriticsStripper.MAPPER[370] = 85;
        LatinDiacriticsStripper.MAPPER[371] = 117;
        LatinDiacriticsStripper.MAPPER[372] = 87;
        LatinDiacriticsStripper.MAPPER[373] = 119;
        LatinDiacriticsStripper.MAPPER[374] = 89;
        LatinDiacriticsStripper.MAPPER[375] = 121;
        LatinDiacriticsStripper.MAPPER[376] = 89;
        LatinDiacriticsStripper.MAPPER[377] = 90;
        LatinDiacriticsStripper.MAPPER[378] = 122;
        LatinDiacriticsStripper.MAPPER[379] = 90;
        LatinDiacriticsStripper.MAPPER[380] = 122;
        LatinDiacriticsStripper.MAPPER[381] = 90;
        LatinDiacriticsStripper.MAPPER[382] = 122;
        LatinDiacriticsStripper.MAPPER[383] = 383;
        LatinDiacriticsStripper.MAPPER[384] = 384;
        LatinDiacriticsStripper.MAPPER[385] = 385;
        LatinDiacriticsStripper.MAPPER[386] = 386;
        LatinDiacriticsStripper.MAPPER[387] = 387;
        LatinDiacriticsStripper.MAPPER[388] = 388;
        LatinDiacriticsStripper.MAPPER[389] = 389;
        LatinDiacriticsStripper.MAPPER[390] = 390;
        LatinDiacriticsStripper.MAPPER[391] = 391;
        LatinDiacriticsStripper.MAPPER[392] = 392;
        LatinDiacriticsStripper.MAPPER[393] = 393;
        LatinDiacriticsStripper.MAPPER[394] = 394;
        LatinDiacriticsStripper.MAPPER[395] = 395;
        LatinDiacriticsStripper.MAPPER[396] = 396;
        LatinDiacriticsStripper.MAPPER[397] = 397;
        LatinDiacriticsStripper.MAPPER[398] = 398;
        LatinDiacriticsStripper.MAPPER[399] = 399;
        LatinDiacriticsStripper.MAPPER[400] = 400;
        LatinDiacriticsStripper.MAPPER[401] = 401;
        LatinDiacriticsStripper.MAPPER[402] = 402;
        LatinDiacriticsStripper.MAPPER[403] = 403;
        LatinDiacriticsStripper.MAPPER[404] = 404;
        LatinDiacriticsStripper.MAPPER[405] = 405;
        LatinDiacriticsStripper.MAPPER[406] = 406;
        LatinDiacriticsStripper.MAPPER[407] = 407;
        LatinDiacriticsStripper.MAPPER[408] = 408;
        LatinDiacriticsStripper.MAPPER[409] = 409;
        LatinDiacriticsStripper.MAPPER[410] = 410;
        LatinDiacriticsStripper.MAPPER[411] = 411;
        LatinDiacriticsStripper.MAPPER[412] = 412;
        LatinDiacriticsStripper.MAPPER[413] = 413;
        LatinDiacriticsStripper.MAPPER[414] = 414;
        LatinDiacriticsStripper.MAPPER[415] = 415;
        LatinDiacriticsStripper.MAPPER[416] = 79;
        LatinDiacriticsStripper.MAPPER[417] = 111;
        LatinDiacriticsStripper.MAPPER[418] = 418;
        LatinDiacriticsStripper.MAPPER[419] = 419;
        LatinDiacriticsStripper.MAPPER[420] = 420;
        LatinDiacriticsStripper.MAPPER[421] = 421;
        LatinDiacriticsStripper.MAPPER[422] = 422;
        LatinDiacriticsStripper.MAPPER[423] = 423;
        LatinDiacriticsStripper.MAPPER[424] = 424;
        LatinDiacriticsStripper.MAPPER[425] = 425;
        LatinDiacriticsStripper.MAPPER[426] = 426;
        LatinDiacriticsStripper.MAPPER[427] = 427;
        LatinDiacriticsStripper.MAPPER[428] = 428;
        LatinDiacriticsStripper.MAPPER[429] = 429;
        LatinDiacriticsStripper.MAPPER[430] = 430;
        LatinDiacriticsStripper.MAPPER[431] = 85;
        LatinDiacriticsStripper.MAPPER[432] = 117;
        LatinDiacriticsStripper.MAPPER[433] = 433;
        LatinDiacriticsStripper.MAPPER[434] = 434;
        LatinDiacriticsStripper.MAPPER[435] = 435;
        LatinDiacriticsStripper.MAPPER[436] = 436;
        LatinDiacriticsStripper.MAPPER[437] = 437;
        LatinDiacriticsStripper.MAPPER[438] = 438;
        LatinDiacriticsStripper.MAPPER[439] = 439;
        LatinDiacriticsStripper.MAPPER[440] = 440;
        LatinDiacriticsStripper.MAPPER[441] = 441;
        LatinDiacriticsStripper.MAPPER[442] = 442;
        LatinDiacriticsStripper.MAPPER[443] = 443;
        LatinDiacriticsStripper.MAPPER[444] = 444;
        LatinDiacriticsStripper.MAPPER[445] = 445;
        LatinDiacriticsStripper.MAPPER[446] = 446;
        LatinDiacriticsStripper.MAPPER[447] = 447;
        LatinDiacriticsStripper.MAPPER[448] = 448;
        LatinDiacriticsStripper.MAPPER[449] = 449;
        LatinDiacriticsStripper.MAPPER[450] = 450;
        LatinDiacriticsStripper.MAPPER[451] = 451;
        LatinDiacriticsStripper.MAPPER[452] = 452;
        LatinDiacriticsStripper.MAPPER[453] = 453;
        LatinDiacriticsStripper.MAPPER[454] = 454;
        LatinDiacriticsStripper.MAPPER[455] = 455;
        LatinDiacriticsStripper.MAPPER[456] = 456;
        LatinDiacriticsStripper.MAPPER[457] = 457;
        LatinDiacriticsStripper.MAPPER[458] = 458;
        LatinDiacriticsStripper.MAPPER[459] = 459;
        LatinDiacriticsStripper.MAPPER[460] = 460;
        LatinDiacriticsStripper.MAPPER[461] = 65;
        LatinDiacriticsStripper.MAPPER[462] = 97;
        LatinDiacriticsStripper.MAPPER[463] = 73;
        LatinDiacriticsStripper.MAPPER[464] = 105;
        LatinDiacriticsStripper.MAPPER[465] = 79;
        LatinDiacriticsStripper.MAPPER[466] = 111;
        LatinDiacriticsStripper.MAPPER[467] = 85;
        LatinDiacriticsStripper.MAPPER[468] = 117;
        LatinDiacriticsStripper.MAPPER[469] = 85;
        LatinDiacriticsStripper.MAPPER[470] = 117;
        LatinDiacriticsStripper.MAPPER[471] = 85;
        LatinDiacriticsStripper.MAPPER[472] = 117;
        LatinDiacriticsStripper.MAPPER[473] = 85;
        LatinDiacriticsStripper.MAPPER[474] = 117;
        LatinDiacriticsStripper.MAPPER[475] = 85;
        LatinDiacriticsStripper.MAPPER[476] = 117;
        LatinDiacriticsStripper.MAPPER[477] = 477;
        LatinDiacriticsStripper.MAPPER[478] = 65;
        LatinDiacriticsStripper.MAPPER[479] = 97;
        LatinDiacriticsStripper.MAPPER[480] = 65;
        LatinDiacriticsStripper.MAPPER[481] = 97;
        LatinDiacriticsStripper.MAPPER[482] = 198;
        LatinDiacriticsStripper.MAPPER[483] = 230;
        LatinDiacriticsStripper.MAPPER[484] = 484;
        LatinDiacriticsStripper.MAPPER[485] = 485;
        LatinDiacriticsStripper.MAPPER[486] = 71;
        LatinDiacriticsStripper.MAPPER[487] = 103;
        LatinDiacriticsStripper.MAPPER[488] = 75;
        LatinDiacriticsStripper.MAPPER[489] = 107;
        LatinDiacriticsStripper.MAPPER[490] = 79;
        LatinDiacriticsStripper.MAPPER[491] = 111;
        LatinDiacriticsStripper.MAPPER[492] = 79;
        LatinDiacriticsStripper.MAPPER[493] = 111;
        LatinDiacriticsStripper.MAPPER[494] = 439;
        LatinDiacriticsStripper.MAPPER[495] = 658;
        LatinDiacriticsStripper.MAPPER[496] = 106;
        LatinDiacriticsStripper.MAPPER[497] = 497;
        LatinDiacriticsStripper.MAPPER[498] = 498;
        LatinDiacriticsStripper.MAPPER[499] = 499;
        LatinDiacriticsStripper.MAPPER[500] = 71;
        LatinDiacriticsStripper.MAPPER[501] = 103;
        LatinDiacriticsStripper.MAPPER[502] = 502;
        LatinDiacriticsStripper.MAPPER[503] = 503;
        LatinDiacriticsStripper.MAPPER[504] = 78;
        LatinDiacriticsStripper.MAPPER[505] = 110;
        LatinDiacriticsStripper.MAPPER[506] = 65;
        LatinDiacriticsStripper.MAPPER[507] = 97;
        LatinDiacriticsStripper.MAPPER[508] = 198;
        LatinDiacriticsStripper.MAPPER[509] = 230;
        LatinDiacriticsStripper.MAPPER[510] = 216;
        LatinDiacriticsStripper.MAPPER[511] = 248;
        LatinDiacriticsStripper.MAPPER[512] = 65;
        LatinDiacriticsStripper.MAPPER[513] = 97;
        LatinDiacriticsStripper.MAPPER[514] = 65;
        LatinDiacriticsStripper.MAPPER[515] = 97;
        LatinDiacriticsStripper.MAPPER[516] = 69;
        LatinDiacriticsStripper.MAPPER[517] = 101;
        LatinDiacriticsStripper.MAPPER[518] = 69;
        LatinDiacriticsStripper.MAPPER[519] = 101;
        LatinDiacriticsStripper.MAPPER[520] = 73;
        LatinDiacriticsStripper.MAPPER[521] = 105;
        LatinDiacriticsStripper.MAPPER[522] = 73;
        LatinDiacriticsStripper.MAPPER[523] = 105;
        LatinDiacriticsStripper.MAPPER[524] = 79;
        LatinDiacriticsStripper.MAPPER[525] = 111;
        LatinDiacriticsStripper.MAPPER[526] = 79;
        LatinDiacriticsStripper.MAPPER[527] = 111;
        LatinDiacriticsStripper.MAPPER[528] = 82;
        LatinDiacriticsStripper.MAPPER[529] = 114;
        LatinDiacriticsStripper.MAPPER[530] = 82;
        LatinDiacriticsStripper.MAPPER[531] = 114;
        LatinDiacriticsStripper.MAPPER[532] = 85;
        LatinDiacriticsStripper.MAPPER[533] = 117;
        LatinDiacriticsStripper.MAPPER[534] = 85;
        LatinDiacriticsStripper.MAPPER[535] = 117;
        LatinDiacriticsStripper.MAPPER[536] = 83;
        LatinDiacriticsStripper.MAPPER[537] = 115;
        LatinDiacriticsStripper.MAPPER[538] = 84;
        LatinDiacriticsStripper.MAPPER[539] = 116;
        LatinDiacriticsStripper.MAPPER[540] = 540;
        LatinDiacriticsStripper.MAPPER[541] = 541;
        LatinDiacriticsStripper.MAPPER[542] = 72;
        LatinDiacriticsStripper.MAPPER[543] = 104;
        LatinDiacriticsStripper.MAPPER[544] = 544;
        LatinDiacriticsStripper.MAPPER[545] = 545;
        LatinDiacriticsStripper.MAPPER[546] = 546;
        LatinDiacriticsStripper.MAPPER[547] = 547;
        LatinDiacriticsStripper.MAPPER[548] = 548;
        LatinDiacriticsStripper.MAPPER[549] = 549;
        LatinDiacriticsStripper.MAPPER[550] = 65;
        LatinDiacriticsStripper.MAPPER[551] = 97;
        LatinDiacriticsStripper.MAPPER[552] = 69;
        LatinDiacriticsStripper.MAPPER[553] = 101;
        LatinDiacriticsStripper.MAPPER[554] = 79;
        LatinDiacriticsStripper.MAPPER[555] = 111;
        LatinDiacriticsStripper.MAPPER[556] = 79;
        LatinDiacriticsStripper.MAPPER[557] = 111;
        LatinDiacriticsStripper.MAPPER[558] = 79;
        LatinDiacriticsStripper.MAPPER[559] = 111;
        LatinDiacriticsStripper.MAPPER[560] = 79;
        LatinDiacriticsStripper.MAPPER[561] = 111;
        LatinDiacriticsStripper.MAPPER[562] = 89;
        LatinDiacriticsStripper.MAPPER[563] = 121;
        LatinDiacriticsStripper.MAPPER[564] = 564;
        LatinDiacriticsStripper.MAPPER[565] = 565;
        LatinDiacriticsStripper.MAPPER[566] = 566;
        LatinDiacriticsStripper.MAPPER[567] = 567;
        LatinDiacriticsStripper.MAPPER[568] = 568;
        LatinDiacriticsStripper.MAPPER[569] = 569;
        LatinDiacriticsStripper.MAPPER[570] = 570;
        LatinDiacriticsStripper.MAPPER[571] = 571;
        LatinDiacriticsStripper.MAPPER[572] = 572;
        LatinDiacriticsStripper.MAPPER[573] = 573;
        LatinDiacriticsStripper.MAPPER[574] = 574;
        LatinDiacriticsStripper.MAPPER[575] = 575;
        LatinDiacriticsStripper.MAPPER[576] = 576;
        LatinDiacriticsStripper.MAPPER[577] = 577;
        LatinDiacriticsStripper.MAPPER[578] = 578;
        LatinDiacriticsStripper.MAPPER[579] = 579;
        LatinDiacriticsStripper.MAPPER[580] = 580;
        LatinDiacriticsStripper.MAPPER[581] = 581;
        LatinDiacriticsStripper.MAPPER[582] = 582;
        LatinDiacriticsStripper.MAPPER[583] = 583;
        LatinDiacriticsStripper.MAPPER[584] = 584;
        LatinDiacriticsStripper.MAPPER[585] = 585;
        LatinDiacriticsStripper.MAPPER[586] = 586;
        LatinDiacriticsStripper.MAPPER[587] = 587;
        LatinDiacriticsStripper.MAPPER[588] = 588;
        LatinDiacriticsStripper.MAPPER[589] = 589;
        LatinDiacriticsStripper.MAPPER[590] = 590;
        LatinDiacriticsStripper.MAPPER[591] = 591;
    }
}

