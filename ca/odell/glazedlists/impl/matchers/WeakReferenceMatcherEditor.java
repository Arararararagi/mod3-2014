/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.matchers;

import ca.odell.glazedlists.matchers.Matcher;
import ca.odell.glazedlists.matchers.MatcherEditor;
import java.util.EventListener;
import javax.swing.event.EventListenerList;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class WeakReferenceMatcherEditor<E>
implements MatcherEditor<E>,
MatcherEditor.Listener<E> {
    private final EventListenerList listenerList = new EventListenerList();
    private MatcherEditor<E> source;

    public WeakReferenceMatcherEditor(MatcherEditor<E> source) {
        this.source = source;
        source.addMatcherEditorListener(new WeakMatcherEditorListener<E>(source, this));
    }

    @Override
    public Matcher<E> getMatcher() {
        return this.source.getMatcher();
    }

    @Override
    public void addMatcherEditorListener(MatcherEditor.Listener<E> listener) {
        this.listenerList.add(MatcherEditor.Listener.class, new WeakMatcherEditorListener<E>(this, listener));
    }

    @Override
    public void removeMatcherEditorListener(MatcherEditor.Listener<E> listener) {
        Object[] listeners = this.listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i-=2) {
            WeakMatcherEditorListener weakMatcherEditorListener;
            MatcherEditor.Listener currentListener;
            Object currentObject = listeners[i + 1];
            if (currentObject == listener) {
                this.listenerList.remove(MatcherEditor.Listener.class, listener);
            }
            if (!(currentObject instanceof WeakMatcherEditorListener) || (currentListener = (weakMatcherEditorListener = (WeakMatcherEditorListener)currentObject).getDecoratedListener()) != listener) continue;
            this.listenerList.remove(MatcherEditor.Listener.class, weakMatcherEditorListener);
        }
    }

    @Override
    public void changedMatcher(MatcherEditor.Event<E> matcherEvent) {
        Object[] listeners = this.listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i-=2) {
            ((MatcherEditor.Listener)listeners[i + 1]).changedMatcher(matcherEvent);
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private static class WeakMatcherEditorListener<E>
    implements MatcherEditor.Listener<E> {
        private final WeakReference<MatcherEditor.Listener<E>> weakListener;
        private final MatcherEditor<E> editor;

        public WeakMatcherEditorListener(MatcherEditor<E> editor, MatcherEditor.Listener<E> listener) {
            this.weakListener = new WeakReference<MatcherEditor.Listener<E>>(listener);
            this.editor = editor;
        }

        public MatcherEditor.Listener<E> getDecoratedListener() {
            return this.weakListener.get();
        }

        @Override
        public void changedMatcher(MatcherEditor.Event<E> matcherEvent) {
            MatcherEditor.Listener<E> matcherEditorListener = this.weakListener.get();
            if (matcherEditorListener == null) {
                this.editor.removeMatcherEditorListener(this);
            } else {
                matcherEditorListener.changedMatcher(matcherEvent);
            }
        }
    }

}

