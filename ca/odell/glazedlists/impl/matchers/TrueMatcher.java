/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.matchers;

import ca.odell.glazedlists.matchers.Matcher;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class TrueMatcher<E>
implements Matcher<E> {
    private static final Matcher INSTANCE = new TrueMatcher<E>();

    private TrueMatcher() {
    }

    public static <E> Matcher<E> getInstance() {
        return INSTANCE;
    }

    @Override
    public boolean matches(E item) {
        return true;
    }
}

