/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.matchers;

import ca.odell.glazedlists.matchers.Matcher;
import java.beans.PropertyChangeEvent;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class PropertyEventNameMatcher
implements Matcher<PropertyChangeEvent> {
    private final Set<String> propertyNames = new HashSet<String>();
    private boolean matchPropertyNames;

    public /* varargs */ PropertyEventNameMatcher(boolean matchPropertyNames, String ... properties) {
        if (properties == null) {
            throw new IllegalArgumentException("Array of property names may not be null");
        }
        this.matchPropertyNames = matchPropertyNames;
        int n = properties.length;
        for (int i = 0; i < n; ++i) {
            this.propertyNames.add(properties[i]);
        }
    }

    public PropertyEventNameMatcher(boolean matchPropertyNames, Collection<String> properties) {
        if (properties == null) {
            throw new IllegalArgumentException("Collection of property names may not be null");
        }
        this.matchPropertyNames = matchPropertyNames;
        this.propertyNames.addAll(properties);
    }

    @Override
    public boolean matches(PropertyChangeEvent event) {
        boolean containsProperty = this.propertyNames.contains(event.getPropertyName());
        return this.matchPropertyNames ? containsProperty : !containsProperty;
    }

    public boolean isMatchPropertyNames() {
        return this.matchPropertyNames;
    }
}

