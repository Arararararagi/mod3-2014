/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.matchers;

import ca.odell.glazedlists.matchers.Matcher;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class NonNullAndNonEmptyStringMatcher
implements Matcher<String> {
    private static final Matcher<String> INSTANCE = new NonNullAndNonEmptyStringMatcher();

    public static Matcher<String> getInstance() {
        return INSTANCE;
    }

    @Override
    public boolean matches(String item) {
        return item != null && item.length() > 0;
    }
}

