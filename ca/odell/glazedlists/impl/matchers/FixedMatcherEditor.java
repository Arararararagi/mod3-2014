/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.matchers;

import ca.odell.glazedlists.matchers.AbstractMatcherEditor;
import ca.odell.glazedlists.matchers.Matcher;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class FixedMatcherEditor<E>
extends AbstractMatcherEditor<E> {
    public FixedMatcherEditor(Matcher<E> matcher) {
        super.fireChanged(matcher);
    }
}

