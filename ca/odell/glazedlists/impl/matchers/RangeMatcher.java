/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.matchers;

import ca.odell.glazedlists.Filterator;
import ca.odell.glazedlists.matchers.Matcher;
import java.util.ArrayList;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class RangeMatcher<D extends Comparable, E>
implements Matcher<E> {
    private final D start;
    private final D end;
    private final Filterator<D, E> filterator;
    private final List<D> filterComparables = new ArrayList<D>();

    public RangeMatcher(D start, D end) {
        this(start, end, null);
    }

    public RangeMatcher(D start, D end, Filterator<D, E> filterator) {
        this.start = start;
        this.end = end;
        this.filterator = filterator;
    }

    @Override
    public boolean matches(E item) {
        this.filterComparables.clear();
        if (this.filterator == null) {
            this.filterComparables.add((Comparable)((Comparable)item));
        } else {
            this.filterator.getFilterValues(this.filterComparables, item);
        }
        for (int c = 0; c < this.filterComparables.size(); ++c) {
            Comparable filterComparable = (Comparable)this.filterComparables.get(c);
            if (filterComparable != null) {
                if (this.start != null && this.start.compareTo((Comparable)filterComparable) > 0) continue;
                if (this.end != null && this.end.compareTo((Comparable)filterComparable) < 0) continue;
            }
            return true;
        }
        return false;
    }

    public String toString() {
        return "[RangeMatcher between " + this.start + " and " + this.end + "]";
    }
}

