/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.matchers;

import ca.odell.glazedlists.matchers.Matcher;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class NotMatcher<E>
implements Matcher<E> {
    private Matcher<E> parent;

    public NotMatcher(Matcher<E> parent) {
        if (parent == null) {
            throw new IllegalArgumentException("parent cannot be null");
        }
        this.parent = parent;
    }

    @Override
    public boolean matches(E item) {
        return !this.parent.matches(item);
    }

    public String toString() {
        return "[NotMatcher parent:" + this.parent + "]";
    }
}

