/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.matchers;

import ca.odell.glazedlists.impl.GlazedListsImpl;
import ca.odell.glazedlists.impl.beans.BeanProperty;
import ca.odell.glazedlists.matchers.Matcher;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class BeanPropertyMatcher<E>
implements Matcher<E> {
    private final BeanProperty<E> beanProperty;
    private final Object value;

    public BeanPropertyMatcher(Class<E> beanClass, String propertyName, Object value) {
        this.beanProperty = new BeanProperty<E>(beanClass, propertyName, true, false);
        this.value = value;
    }

    @Override
    public boolean matches(E item) {
        if (item == null) {
            return false;
        }
        return GlazedListsImpl.equal(this.beanProperty.get(item), this.value);
    }
}

