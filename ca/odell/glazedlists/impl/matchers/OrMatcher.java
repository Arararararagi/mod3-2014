/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.matchers;

import ca.odell.glazedlists.matchers.Matcher;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class OrMatcher<E>
implements Matcher<E> {
    private final Matcher<? super E>[] matchers;

    public /* varargs */ OrMatcher(Matcher<? super E> ... matchers) {
        this.matchers = matchers;
    }

    @Override
    public boolean matches(E item) {
        for (int i = 0; i < this.matchers.length; ++i) {
            if (!this.matchers[i].matches(item)) continue;
            return true;
        }
        return false;
    }
}

