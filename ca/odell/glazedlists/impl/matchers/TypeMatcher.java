/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl.matchers;

import ca.odell.glazedlists.matchers.Matcher;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class TypeMatcher<E>
implements Matcher<E> {
    private final Class[] classes;

    public /* varargs */ TypeMatcher(Class ... classes) {
        this.classes = classes;
    }

    @Override
    public boolean matches(E item) {
        if (item == null) {
            return false;
        }
        Class target = item.getClass();
        for (int i = 0; i < this.classes.length; ++i) {
            if (!this.classes[i].isAssignableFrom(target)) continue;
            return true;
        }
        return false;
    }
}

