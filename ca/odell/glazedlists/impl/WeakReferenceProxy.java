/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.impl;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class WeakReferenceProxy<E>
implements ListEventListener<E> {
    private final WeakReference<ListEventListener<E>> proxyTargetReference;
    private EventList<E> source;

    public WeakReferenceProxy(EventList<E> source, ListEventListener<E> proxyTarget) {
        if (source == null) {
            throw new IllegalArgumentException("source may not be null");
        }
        if (proxyTarget == null) {
            throw new IllegalArgumentException("proxyTarget may not be null");
        }
        this.source = source;
        this.proxyTargetReference = new WeakReference<ListEventListener<E>>(proxyTarget);
    }

    @Override
    public void listChanged(ListEvent<E> listChanges) {
        if (this.source == null) {
            return;
        }
        ListEventListener<E> proxyTarget = this.getReferent();
        if (proxyTarget == null) {
            this.source.removeListEventListener(this);
            this.dispose();
        } else {
            proxyTarget.listChanged(listChanges);
        }
    }

    public ListEventListener<E> getReferent() {
        return this.proxyTargetReference.get();
    }

    public void dispose() {
        this.source = null;
    }
}

