/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.CollectionList;
import ca.odell.glazedlists.DisposableMap;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.Filterator;
import ca.odell.glazedlists.FunctionList;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.TextFilterator;
import ca.odell.glazedlists.ThresholdList;
import ca.odell.glazedlists.TransformedList;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.impl.Diff;
import ca.odell.glazedlists.impl.FunctionListMap;
import ca.odell.glazedlists.impl.GlazedListsImpl;
import ca.odell.glazedlists.impl.GroupingListMultiMap;
import ca.odell.glazedlists.impl.ListCollectionListModel;
import ca.odell.glazedlists.impl.ObservableConnector;
import ca.odell.glazedlists.impl.ReadOnlyList;
import ca.odell.glazedlists.impl.SyncListener;
import ca.odell.glazedlists.impl.ThreadSafeList;
import ca.odell.glazedlists.impl.TypeSafetyListener;
import ca.odell.glazedlists.impl.WeakReferenceProxy;
import ca.odell.glazedlists.impl.beans.BeanConnector;
import ca.odell.glazedlists.impl.beans.BeanFunction;
import ca.odell.glazedlists.impl.beans.BeanTableFormat;
import ca.odell.glazedlists.impl.beans.BeanTextFilterator;
import ca.odell.glazedlists.impl.beans.BeanThresholdEvaluator;
import ca.odell.glazedlists.impl.beans.StringBeanFunction;
import ca.odell.glazedlists.impl.filter.StringTextFilterator;
import ca.odell.glazedlists.impl.functions.ConstantFunction;
import ca.odell.glazedlists.impl.matchers.FixedMatcherEditor;
import ca.odell.glazedlists.impl.sort.BeanPropertyComparator;
import ca.odell.glazedlists.impl.sort.BooleanComparator;
import ca.odell.glazedlists.impl.sort.ComparableComparator;
import ca.odell.glazedlists.impl.sort.ComparatorChain;
import ca.odell.glazedlists.impl.sort.ReverseComparator;
import ca.odell.glazedlists.matchers.Matcher;
import ca.odell.glazedlists.matchers.MatcherEditor;
import ca.odell.glazedlists.matchers.Matchers;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Observable;
import java.util.Set;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class GlazedLists {
    private static Comparator<Boolean> booleanComparator = null;
    private static Comparator<Comparable> comparableComparator = null;
    private static Comparator<Comparable> reversedComparable = null;
    private static TextFilterator<Object> stringTextFilterator = null;

    private GlazedLists() {
        throw new UnsupportedOperationException();
    }

    public static <E> void replaceAll(EventList<E> target, List<E> source, boolean updates) {
        Diff.replaceAll(target, source, updates);
    }

    public static <E> void replaceAll(EventList<E> target, List<E> source, boolean updates, Comparator<E> comparator) {
        Diff.replaceAll(target, source, updates, comparator);
    }

    public static <E> void replaceAllSorted(EventList<E> target, Collection<E> source, boolean updates, Comparator<E> comparator) {
        GlazedListsImpl.replaceAll(target, source, updates, comparator);
    }

    public static /* varargs */ <T> Comparator<T> beanPropertyComparator(Class<T> clazz, String property, String ... properties) {
        Comparator<T> firstComparator = GlazedLists.beanPropertyComparator(clazz, property, GlazedLists.<T>comparableComparator());
        if (properties.length == 0) {
            return firstComparator;
        }
        ArrayList<Comparator<T>> comparators = new ArrayList<Comparator<T>>(properties.length + 1);
        comparators.add(firstComparator);
        for (int i = 0; i < properties.length; ++i) {
            comparators.add(GlazedLists.beanPropertyComparator(clazz, properties[i], GlazedLists.<T>comparableComparator()));
        }
        return GlazedLists.chainComparators(comparators);
    }

    public static <T> Comparator<T> beanPropertyComparator(Class<T> className, String property, Comparator propertyComparator) {
        return new BeanPropertyComparator<T>(className, property, propertyComparator);
    }

    public static Comparator<Boolean> booleanComparator() {
        if (booleanComparator == null) {
            booleanComparator = new BooleanComparator();
        }
        return booleanComparator;
    }

    public static Comparator<String> caseInsensitiveComparator() {
        return String.CASE_INSENSITIVE_ORDER;
    }

    public static <T> Comparator<T> chainComparators(List<Comparator<T>> comparators) {
        return new ComparatorChain<T>(comparators);
    }

    public static /* varargs */ <T> Comparator<T> chainComparators(Comparator<T> ... comparators) {
        return GlazedLists.chainComparators(Arrays.asList(comparators));
    }

    public static <T extends Comparable> Comparator<T> comparableComparator() {
        if (comparableComparator == null) {
            comparableComparator = new ComparableComparator();
        }
        return comparableComparator;
    }

    public static <T extends Comparable> Comparator<T> reverseComparator() {
        if (reversedComparable == null) {
            reversedComparable = GlazedLists.reverseComparator(GlazedLists.<T>comparableComparator());
        }
        return reversedComparable;
    }

    public static <T> Comparator<T> reverseComparator(Comparator<T> forward) {
        return new ReverseComparator<T>(forward);
    }

    public static <T> TableFormat<T> tableFormat(String[] propertyNames, String[] columnLabels) {
        return new BeanTableFormat(null, propertyNames, columnLabels);
    }

    public static <T> TableFormat<T> tableFormat(Class<T> baseClass, String[] propertyNames, String[] columnLabels) {
        return new BeanTableFormat<T>(baseClass, propertyNames, columnLabels);
    }

    public static <T> TableFormat<T> tableFormat(String[] propertyNames, String[] columnLabels, boolean[] editable) {
        return new BeanTableFormat(null, propertyNames, columnLabels, editable);
    }

    public static <T> TableFormat<T> tableFormat(Class<T> baseClass, String[] propertyNames, String[] columnLabels, boolean[] editable) {
        return new BeanTableFormat<T>(baseClass, propertyNames, columnLabels, editable);
    }

    public static /* varargs */ <E> TextFilterator<E> textFilterator(String ... propertyNames) {
        return new BeanTextFilterator(propertyNames);
    }

    public static /* varargs */ <E> TextFilterator<E> textFilterator(Class<E> beanClass, String ... propertyNames) {
        return new BeanTextFilterator(beanClass, propertyNames);
    }

    public static /* varargs */ <D, E> Filterator<D, E> filterator(String ... propertyNames) {
        return new BeanTextFilterator(propertyNames);
    }

    public static /* varargs */ <D, E> Filterator<D, E> filterator(Class<E> beanClass, String ... propertyNames) {
        return new BeanTextFilterator(beanClass, propertyNames);
    }

    public static <E> TextFilterator<E> toStringTextFilterator() {
        if (stringTextFilterator == null) {
            stringTextFilterator = new StringTextFilterator<Object>();
        }
        return stringTextFilterator;
    }

    public static <E> ThresholdList.Evaluator<E> thresholdEvaluator(String propertyName) {
        return new BeanThresholdEvaluator(propertyName);
    }

    public static <E> CollectionList.Model<List<E>, E> listCollectionListModel() {
        return new ListCollectionListModel();
    }

    public static /* varargs */ <E> EventList<E> eventListOf(E ... contents) {
        return GlazedLists.eventList(contents == null ? Collections.EMPTY_LIST : Arrays.asList(contents));
    }

    public static <E> EventList<E> eventList(Collection<? extends E> contents) {
        BasicEventList<E> result = new BasicEventList<E>(contents == null ? 0 : contents.size());
        if (contents != null) {
            result.addAll(contents);
        }
        return result;
    }

    public static <E> TransformedList<E, E> readOnlyList(EventList<E> source) {
        return new ReadOnlyList<E>(source);
    }

    public static <E> TransformedList<E, E> threadSafeList(EventList<E> source) {
        return new ThreadSafeList<E>(source);
    }

    public static <E> ListEventListener<E> weakReferenceProxy(EventList<E> source, ListEventListener<E> target) {
        return new WeakReferenceProxy<E>(source, target);
    }

    public static <E> ObservableElementList.Connector<E> beanConnector(Class<E> beanClass) {
        return new BeanConnector<E>(beanClass);
    }

    public static /* varargs */ <E> ObservableElementList.Connector<E> beanConnector(Class<E> beanClass, boolean matchPropertyNames, String ... propertyNames) {
        Matcher<PropertyChangeEvent> byNameMatcher = Matchers.propertyEventNameMatcher(matchPropertyNames, propertyNames);
        return GlazedLists.beanConnector(beanClass, byNameMatcher);
    }

    public static <E> ObservableElementList.Connector<E> beanConnector(Class<E> beanClass, Matcher<PropertyChangeEvent> eventMatcher) {
        return new BeanConnector<E>(beanClass, eventMatcher);
    }

    public static <E> ObservableElementList.Connector<E> beanConnector(Class<E> beanClass, String addListener, String removeListener) {
        return new BeanConnector<E>(beanClass, addListener, removeListener);
    }

    public static <E> ObservableElementList.Connector<E> beanConnector(Class<E> beanClass, String addListener, String removeListener, Matcher<PropertyChangeEvent> eventMatcher) {
        return new BeanConnector<E>(beanClass, addListener, removeListener, eventMatcher);
    }

    public static <E extends Observable> ObservableElementList.Connector<E> observableConnector() {
        return new ObservableConnector();
    }

    public static <E> Matcher<E> beanPropertyMatcher(Class<E> beanClass, String propertyName, Object value) {
        return Matchers.beanPropertyMatcher(beanClass, propertyName, value);
    }

    public static <E> MatcherEditor<E> fixedMatcherEditor(Matcher<E> matcher) {
        return new FixedMatcherEditor<E>(matcher);
    }

    public static <E, V> FunctionList.Function<E, V> constantFunction(V value) {
        return new ConstantFunction(value);
    }

    public static <E> FunctionList.Function<E, String> toStringFunction(Class<E> beanClass, String propertyName) {
        return new StringBeanFunction<E>(beanClass, propertyName);
    }

    public static <E, V> FunctionList.Function<E, V> beanFunction(Class<E> beanClass, String propertyName) {
        return new BeanFunction(beanClass, propertyName);
    }

    public static <E> ListEventListener<E> syncEventListToList(EventList<E> source, List<E> target) {
        return new SyncListener<E>(source, target);
    }

    public static <E> ListEventListener<E> typeSafetyListener(EventList<E> source, Set<Class> types) {
        return new TypeSafetyListener<E>(source, types);
    }

    public static <K extends Comparable, V> DisposableMap<K, List<V>> syncEventListToMultiMap(EventList<V> source, FunctionList.Function<V, ? extends K> keyMaker) {
        return GlazedLists.syncEventListToMultiMap(source, keyMaker, GlazedLists.<T>comparableComparator());
    }

    public static <K, V> DisposableMap<K, List<V>> syncEventListToMultiMap(EventList<V> source, FunctionList.Function<V, ? extends K> keyMaker, Comparator<? super K> keyGrouper) {
        return new GroupingListMultiMap<K, V>(source, keyMaker, keyGrouper);
    }

    public static <K, V> DisposableMap<K, V> syncEventListToMap(EventList<V> source, FunctionList.Function<V, K> keyMaker) {
        return new FunctionListMap<K, V>(source, keyMaker);
    }
}

