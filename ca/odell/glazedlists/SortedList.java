/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.TransformedList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventAssembler;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.impl.GlazedListsImpl;
import ca.odell.glazedlists.impl.adt.barcode2.Element;
import ca.odell.glazedlists.impl.adt.barcode2.SimpleTree;
import ca.odell.glazedlists.impl.adt.barcode2.SimpleTreeIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class SortedList<E>
extends TransformedList<E, E> {
    private static final byte ALL_COLORS = 1;
    private static final Element EMPTY_ELEMENT = null;
    public static final int STRICT_SORT_ORDER = 0;
    public static final int AVOID_MOVING_ELEMENTS = 1;
    private SimpleTree<Element> unsorted = null;
    private SimpleTree<Element> sorted = null;
    private Comparator<? super E> comparator = null;
    private int mode = 0;

    public static <E extends Comparable<? super E>> SortedList<E> create(EventList<E> source) {
        return new SortedList<E>(source);
    }

    public SortedList(EventList<E> source) {
        this(source, GlazedLists.comparableComparator());
    }

    public SortedList(EventList<E> source, Comparator<? super E> comparator) {
        super(source);
        this.setComparator(comparator);
        source.addListEventListener(this);
    }

    public void setMode(int mode) {
        if (mode != 0 && mode != 1) {
            throw new IllegalArgumentException("Mode must be either SortedList.STRICT_SORT_ORDER or SortedList.AVOID_MOVING_ELEMENTS");
        }
        if (mode == this.mode) {
            return;
        }
        this.mode = mode;
        if (this.mode == 0) {
            this.setComparator(this.getComparator());
        }
    }

    public int getMode() {
        return this.mode;
    }

    @Override
    public void listChanged(ListEvent<E> listChanges) {
        int i;
        Element sortedNode;
        if (listChanges.isReordering()) {
            int[] sourceReorder = listChanges.getReorderMap();
            int[] previousIndexToSortedIndex = new int[this.sorted.size()];
            int index = 0;
            SimpleTreeIterator<Element> i2 = new SimpleTreeIterator<Element>(this.sorted);
            while (i2.hasNext()) {
                i2.next();
                Element unsortedNode = i2.value();
                int unsortedIndex = this.unsorted.indexOfNode(unsortedNode, 1);
                previousIndexToSortedIndex[unsortedIndex] = index++;
            }
            int[] newIndexToSortedIndex = new int[this.sorted.size()];
            for (int i3 = 0; i3 < previousIndexToSortedIndex.length; ++i3) {
                newIndexToSortedIndex[i3] = previousIndexToSortedIndex[sourceReorder[i3]];
            }
            Element[] unsortedNodes = new Element[this.unsorted.size()];
            index = 0;
            SimpleTreeIterator<Element> i4 = new SimpleTreeIterator<Element>(this.unsorted);
            while (i4.hasNext()) {
                Element<Element> unsortedNode;
                i4.next();
                unsortedNodes[index] = unsortedNode = i4.node();
                ++index;
            }
            Arrays.sort(unsortedNodes, this.sorted.getComparator());
            int[] reorderMap = new int[this.sorted.size()];
            boolean indexChanged = false;
            index = 0;
            SimpleTreeIterator<Element> i5 = new SimpleTreeIterator<Element>(this.sorted);
            while (i5.hasNext()) {
                i5.next();
                Element<Element> sortedNode2 = i5.node();
                Element unsortedNode = unsortedNodes[index];
                sortedNode2.set(unsortedNode);
                unsortedNode.set(sortedNode2);
                int unsortedIndex = this.unsorted.indexOfNode(unsortedNode, 1);
                reorderMap[index] = newIndexToSortedIndex[unsortedIndex];
                indexChanged = indexChanged || index != reorderMap[index];
                ++index;
            }
            if (indexChanged) {
                this.updates.beginEvent();
                this.updates.reorder(reorderMap);
                this.updates.commitEvent();
            }
            return;
        }
        this.updates.beginEvent();
        LinkedList<Element<Element>> insertNodes = new LinkedList<Element<Element>>();
        ArrayList<Element> updateNodes = new ArrayList<Element>();
        ArrayList<E> previousValues = new ArrayList<E>();
        while (listChanges.next()) {
            Element<Element> unsortedNode;
            int unsortedIndex = listChanges.getIndex();
            int changeType = listChanges.getType();
            if (changeType == 2) {
                unsortedNode = this.unsorted.add(unsortedIndex, EMPTY_ELEMENT, 1);
                insertNodes.addLast(unsortedNode);
                continue;
            }
            if (changeType == 1) {
                unsortedNode = this.unsorted.get(unsortedIndex);
                sortedNode = unsortedNode.get();
                sortedNode.setSorted(2);
                updateNodes.add(sortedNode);
                previousValues.add(listChanges.getOldValue());
                continue;
            }
            if (changeType != 0) continue;
            unsortedNode = this.unsorted.get(unsortedIndex);
            E deleted = listChanges.getOldValue();
            this.unsorted.remove(unsortedNode);
            int deleteSortedIndex = this.deleteByUnsortedNode(unsortedNode);
            this.updates.elementDeleted(deleteSortedIndex, deleted);
        }
        int size = updateNodes.size();
        for (i = 0; i < size; ++i) {
            Element sortedNode3 = (Element)updateNodes.get(i);
            if (sortedNode3.getSorted() != 2) continue;
            Element lowerBound = null;
            Element upperBound = null;
            Element firstUnsortedNode = sortedNode3;
            for (Element leftNeighbour = sortedNode3.previous(); leftNeighbour != null; leftNeighbour = leftNeighbour.previous()) {
                if (leftNeighbour.getSorted() == 0) {
                    lowerBound = leftNeighbour;
                    break;
                }
                firstUnsortedNode = leftNeighbour;
            }
            for (Element rightNeighbour = sortedNode3.next(); rightNeighbour != null; rightNeighbour = rightNeighbour.next()) {
                if (rightNeighbour.getSorted() != 0) continue;
                upperBound = rightNeighbour;
                break;
            }
            Comparator<Element> nodeComparator = this.sorted.getComparator();
            for (Element current = firstUnsortedNode; current != upperBound; current = current.next()) {
                if (upperBound != null && nodeComparator.compare((Element)current.get(), (Element)upperBound.get()) > 0) {
                    current.setSorted(1);
                    continue;
                }
                if (lowerBound != null && nodeComparator.compare((Element)current.get(), (Element)lowerBound.get()) < 0) {
                    current.setSorted(1);
                    continue;
                }
                current.setSorted(0);
                lowerBound = current;
            }
        }
        size = updateNodes.size();
        for (i = 0; i < size; ++i) {
            Object previous = previousValues.get(i);
            sortedNode = (Element)updateNodes.get(i);
            assert (sortedNode.getSorted() != 2);
            int originalIndex = this.sorted.indexOfNode(sortedNode, 1);
            if (sortedNode.getSorted() == 0) {
                this.updates.elementUpdated(originalIndex, previous);
                continue;
            }
            if (this.mode == 1) {
                this.updates.elementUpdated(originalIndex, previous);
                continue;
            }
            this.sorted.remove(sortedNode);
            this.updates.elementDeleted(originalIndex, previous);
            int insertedIndex = this.insertByUnsortedNode((Element)sortedNode.get());
            this.updates.addInsert(insertedIndex);
        }
        while (!insertNodes.isEmpty()) {
            Element insertNode = (Element)insertNodes.removeFirst();
            int insertedIndex = this.insertByUnsortedNode(insertNode);
            this.updates.addInsert(insertedIndex);
        }
        this.updates.commitEvent();
    }

    private int insertByUnsortedNode(Element unsortedNode) {
        Element<Element> sortedNode = this.sorted.addInSortedOrder(1, unsortedNode, 1);
        unsortedNode.set(sortedNode);
        return this.sorted.indexOfNode(sortedNode, 1);
    }

    private int deleteByUnsortedNode(Element unsortedNode) {
        Element sortedNode = (Element)unsortedNode.get();
        int sortedIndex = this.sorted.indexOfNode(sortedNode, 1);
        this.sorted.remove(sortedIndex, 1);
        return sortedIndex;
    }

    @Override
    protected int getSourceIndex(int mutationIndex) {
        Element<Element> sortedNode = this.sorted.get(mutationIndex);
        Element unsortedNode = sortedNode.get();
        return this.unsorted.indexOfNode(unsortedNode, 1);
    }

    @Override
    protected boolean isWritable() {
        return true;
    }

    public Comparator<? super E> getComparator() {
        return this.comparator;
    }

    public void setComparator(Comparator<? super E> comparator) {
        this.comparator = comparator;
        SimpleTree<Element> previousSorted = this.sorted;
        Comparator treeComparator = comparator != null ? new ElementComparator(comparator) : new ElementRawOrderComparator();
        this.sorted = new SimpleTree(treeComparator);
        if (previousSorted == null && this.unsorted == null) {
            this.unsorted = new SimpleTree();
            int n = this.source.size();
            for (int i = 0; i < n; ++i) {
                Element<Element> unsortedNode = this.unsorted.add(i, EMPTY_ELEMENT, 1);
                this.insertByUnsortedNode(unsortedNode);
            }
            return;
        }
        if (this.source.size() == 0) {
            return;
        }
        SimpleTreeIterator<Element> i = new SimpleTreeIterator<Element>(this.unsorted);
        while (i.hasNext()) {
            i.next();
            Element<Element> unsortedNode = i.node();
            this.insertByUnsortedNode(unsortedNode);
        }
        int[] reorderMap = new int[this.size()];
        int oldSortedIndex = 0;
        SimpleTreeIterator<Element> i2 = new SimpleTreeIterator<Element>(previousSorted);
        while (i2.hasNext()) {
            i2.next();
            Element<Element> oldSortedNode = i2.node();
            Element unsortedNode = oldSortedNode.get();
            Element newSortedNode = (Element)unsortedNode.get();
            int newSortedIndex = this.sorted.indexOfNode(newSortedNode, 1);
            reorderMap[newSortedIndex] = oldSortedIndex++;
        }
        this.updates.beginEvent();
        this.updates.reorder(reorderMap);
        this.updates.commitEvent();
    }

    @Override
    public int indexOf(Object object) {
        if (this.mode != 0 || this.comparator == null) {
            return this.source.indexOf(object);
        }
        int index = this.sorted.indexOfValue((Element)object, true, false, 1);
        if (index == -1) {
            return -1;
        }
        while (index < this.size()) {
            Object objectAtIndex = this.get(index);
            if (this.comparator.compare((Object)object, objectAtIndex) != 0) {
                return -1;
            }
            if (GlazedListsImpl.equal(object, objectAtIndex)) {
                return index;
            }
            ++index;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object object) {
        if (this.mode != 0 || this.comparator == null) {
            return this.source.lastIndexOf(object);
        }
        int index = this.sorted.indexOfValue((Element)object, false, false, 1);
        if (index == -1) {
            return -1;
        }
        while (index > -1) {
            Object objectAtIndex = this.get(index);
            if (this.comparator.compare((Object)object, objectAtIndex) != 0) {
                return -1;
            }
            if (GlazedListsImpl.equal(object, objectAtIndex)) {
                return index;
            }
            --index;
        }
        return -1;
    }

    public int sortIndex(Object object) {
        if (this.comparator == null) {
            throw new IllegalStateException("No Comparator exists to perform this operation");
        }
        return this.sorted.indexOfValue((Element)object, true, true, 1);
    }

    public int lastSortIndex(Object object) {
        if (this.comparator == null) {
            throw new IllegalStateException("No Comparator exists to perform this operation");
        }
        return this.sorted.indexOfValue((Element)object, false, true, 1);
    }

    public int indexOfSimulated(Object object) {
        return this.comparator != null ? this.sorted.indexOfValue((Element)object, true, true, 1) : this.size();
    }

    @Override
    public boolean contains(Object object) {
        return this.indexOf(object) != -1;
    }

    @Override
    public Iterator<E> iterator() {
        return new SortedListIterator();
    }

    private class ElementComparator
    implements Comparator {
        private Comparator comparator;

        public ElementComparator(Comparator comparator) {
            this.comparator = comparator;
        }

        public int compare(Object alpha, Object beta) {
            int result;
            Object alphaObject = alpha;
            Object betaObject = beta;
            int alphaIndex = -1;
            int betaIndex = -1;
            if (alpha instanceof Element) {
                Element alphaTreeNode = (Element)alpha;
                alphaIndex = SortedList.this.unsorted.indexOfNode(alphaTreeNode, 1);
                alphaObject = SortedList.this.source.get(alphaIndex);
            }
            if (beta instanceof Element) {
                Element betaTreeNode = (Element)beta;
                betaIndex = SortedList.this.unsorted.indexOfNode(betaTreeNode, 1);
                betaObject = SortedList.this.source.get(betaIndex);
            }
            if ((result = this.comparator.compare(alphaObject, betaObject)) != 0) {
                return result;
            }
            if (alphaIndex != -1 && betaIndex != -1) {
                return alphaIndex - betaIndex;
            }
            return 0;
        }
    }

    private class ElementRawOrderComparator
    implements Comparator {
        private ElementRawOrderComparator() {
        }

        public int compare(Object alpha, Object beta) {
            Element alphaTreeNode = (Element)alpha;
            Element betaTreeNode = (Element)beta;
            int alphaIndex = SortedList.this.unsorted.indexOfNode(alphaTreeNode, 1);
            int betaIndex = SortedList.this.unsorted.indexOfNode(betaTreeNode, 1);
            return alphaIndex - betaIndex;
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class SortedListIterator
    implements Iterator<E> {
        private SimpleTreeIterator<Element> treeIterator;

        private SortedListIterator() {
            this.treeIterator = new SimpleTreeIterator(SortedList.this.sorted);
        }

        @Override
        public boolean hasNext() {
            return this.treeIterator.hasNext();
        }

        @Override
        public E next() {
            this.treeIterator.next();
            Element unsortedNode = this.treeIterator.value();
            return SortedList.this.source.get(SortedList.this.unsorted.indexOfNode(unsortedNode, 1));
        }

        @Override
        public void remove() {
            int indexToRemove = this.treeIterator.index();
            SortedList.this.source.remove(SortedList.this.getSourceIndex(indexToRemove));
            this.treeIterator = new SimpleTreeIterator(SortedList.this.sorted, indexToRemove, 1);
        }
    }

}

