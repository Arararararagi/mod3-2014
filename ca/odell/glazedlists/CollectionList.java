/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.TransformedList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventAssembler;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.event.ListEventPublisher;
import ca.odell.glazedlists.impl.adt.Barcode;
import ca.odell.glazedlists.impl.adt.barcode2.Element;
import ca.odell.glazedlists.impl.adt.barcode2.SimpleTree;
import ca.odell.glazedlists.impl.adt.barcode2.SimpleTreeIterator;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;
import java.util.Collections;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class CollectionList<S, E>
extends TransformedList<S, E>
implements ListEventListener<S> {
    private final ChildElement<E> EMPTY_CHILD_ELEMENT;
    private final Model<S, E> model;
    private final Barcode barcode;
    private final SimpleTree<ChildElement<E>> childElements;

    public CollectionList(EventList<S> source, Model<S, E> model) {
        super(source);
        this.EMPTY_CHILD_ELEMENT = new SimpleChildElement(Collections.EMPTY_LIST, null);
        this.barcode = new Barcode();
        this.childElements = new SimpleTree();
        if (model == null) {
            throw new IllegalArgumentException("model cannot be null");
        }
        this.model = model;
        int n = source.size();
        for (int i = 0; i < n; ++i) {
            List<S> children = model.getChildren(source.get(i));
            Element<ChildElement<E>> node = this.childElements.add(i, this.EMPTY_CHILD_ELEMENT, 1);
            node.set(this.createChildElementForList(children, node));
            this.barcode.addBlack(this.barcode.size(), 1);
            if (children.isEmpty()) continue;
            this.barcode.addWhite(this.barcode.size(), children.size());
        }
        source.addListEventListener(this);
    }

    @Override
    protected boolean isWritable() {
        return false;
    }

    @Override
    public int size() {
        return this.barcode.whiteSize();
    }

    @Override
    public E get(int index) {
        ChildElement<E> childElement = this.getChildElement(index);
        int childIndexInParent = this.barcode.getWhiteSequenceIndex(index);
        return childElement.get(childIndexInParent);
    }

    @Override
    public E set(int index, E value) {
        ChildElement<E> childElement = this.getChildElement(index);
        int childIndexInParent = this.barcode.getWhiteSequenceIndex(index);
        return childElement.set(childIndexInParent, value);
    }

    @Override
    public E remove(int index) {
        ChildElement<E> childElement = this.getChildElement(index);
        int childIndexInParent = this.barcode.getWhiteSequenceIndex(index);
        return childElement.remove(childIndexInParent);
    }

    public int childStartingIndex(int parentIndex) {
        if (parentIndex < 0) {
            throw new IndexOutOfBoundsException("Invalid index: " + parentIndex);
        }
        if (parentIndex >= this.source.size()) {
            throw new IndexOutOfBoundsException("Invalid index: " + parentIndex);
        }
        int parentFullIndex = this.barcode.getIndex(parentIndex, Barcode.BLACK);
        int childFullIndex = parentFullIndex + 1;
        if (childFullIndex >= this.barcode.size()) {
            return -1;
        }
        if (this.barcode.get(childFullIndex) != Barcode.WHITE) {
            return -1;
        }
        int childIndex = childFullIndex - (parentIndex + 1);
        assert (this.barcode.getWhiteIndex(childFullIndex) == childIndex);
        return childIndex;
    }

    public int childEndingIndex(int parentIndex) {
        if (parentIndex < 0) {
            throw new IndexOutOfBoundsException("Invalid index: " + parentIndex);
        }
        if (parentIndex >= this.source.size()) {
            throw new IndexOutOfBoundsException("Invalid index: " + parentIndex);
        }
        int nextParentFullIndex = parentIndex == this.barcode.blackSize() - 1 ? this.barcode.size() : this.barcode.getIndex(parentIndex + 1, Barcode.BLACK);
        int lastWhiteBeforeNextParent = nextParentFullIndex - 1;
        if (this.barcode.get(lastWhiteBeforeNextParent) == Barcode.BLACK) {
            return -1;
        }
        int childIndex = lastWhiteBeforeNextParent - (parentIndex + 1);
        assert (this.barcode.getWhiteIndex(lastWhiteBeforeNextParent) == childIndex);
        return childIndex;
    }

    @Override
    public void listChanged(ListEvent<S> listChanges) {
        this.updates.beginEvent();
        while (listChanges.next()) {
            int index = listChanges.getIndex();
            int type = listChanges.getType();
            if (type == 2) {
                this.handleInsert(index);
                continue;
            }
            if (type == 0) {
                this.handleDelete(index);
                continue;
            }
            if (type != 1) continue;
            this.handleDelete(index);
            this.handleInsert(index);
        }
        this.updates.commitEvent();
    }

    @Override
    public void dispose() {
        super.dispose();
        SimpleTreeIterator<ChildElement<E>> treeIterator = new SimpleTreeIterator<ChildElement<E>>(this.childElements);
        while (treeIterator.hasNext()) {
            treeIterator.next();
            treeIterator.value().dispose();
        }
    }

    private void handleInsert(int parentIndex) {
        int absoluteIndex = this.getAbsoluteIndex(parentIndex);
        Object parent = this.source.get(parentIndex);
        List<E> children = this.model.getChildren(parent);
        Element<ChildElement<E>> node = this.childElements.add(parentIndex, this.EMPTY_CHILD_ELEMENT, 1);
        node.set(this.createChildElementForList(children, node));
        this.barcode.addBlack(absoluteIndex, 1);
        if (!children.isEmpty()) {
            this.barcode.addWhite(absoluteIndex + 1, children.size());
        }
        int childIndex = absoluteIndex - parentIndex;
        for (int i = 0; i < children.size(); ++i) {
            E element = children.get(i);
            this.updates.elementInserted(childIndex, element);
        }
    }

    private void handleDelete(int sourceIndex) {
        int parentIndex = this.getAbsoluteIndex(sourceIndex);
        int nextParentIndex = this.getAbsoluteIndex(sourceIndex + 1);
        int childCount = nextParentIndex - parentIndex - 1;
        if (childCount > 0) {
            int firstDeletedChildIndex = parentIndex - sourceIndex;
            int firstNotDeletedChildIndex = firstDeletedChildIndex + childCount;
            for (int i = firstDeletedChildIndex; i < firstNotDeletedChildIndex; ++i) {
                this.updates.elementDeleted(firstDeletedChildIndex, this.get(i));
            }
        }
        Element<ChildElement<E>> removedChildElement = this.childElements.get(sourceIndex);
        this.childElements.remove(removedChildElement);
        removedChildElement.get().dispose();
        this.barcode.remove(parentIndex, 1 + childCount);
    }

    private ChildElement<E> getChildElement(int childIndex) {
        if (childIndex < 0) {
            throw new IndexOutOfBoundsException("Invalid index: " + childIndex);
        }
        if (childIndex >= this.size()) {
            throw new IndexOutOfBoundsException("Index: " + childIndex + ", Size: " + this.size());
        }
        int parentIndex = this.barcode.getBlackBeforeWhite(childIndex);
        return this.childElements.get(parentIndex).get();
    }

    private ChildElement<E> createChildElementForList(List<E> children, Element<ChildElement<E>> node) {
        if (children instanceof EventList) {
            return new EventChildElement((EventList)children, node);
        }
        return new SimpleChildElement(children, node);
    }

    private int getAbsoluteIndex(int parentIndex) {
        if (parentIndex < this.barcode.blackSize()) {
            return this.barcode.getIndex(parentIndex, Barcode.BLACK);
        }
        if (parentIndex == this.barcode.blackSize()) {
            return this.barcode.size();
        }
        throw new IndexOutOfBoundsException();
    }

    private static interface ChildElement<E> {
        public E get(int var1);

        public E remove(int var1);

        public E set(int var1, E var2);

        public void dispose();
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class EventChildElement
    implements ChildElement<E>,
    ListEventListener<E> {
        private final EventList<E> children;
        private final Element<ChildElement<E>> node;

        public EventChildElement(EventList<E> children, Element<ChildElement<E>> node) {
            this.children = children;
            this.node = node;
            if (!CollectionList.this.getPublisher().equals(children.getPublisher())) {
                throw new IllegalArgumentException("If a CollectionList.Model returns EventLists, those EventLists must use the same ListEventPublisher as the CollectionList");
            }
            if (!CollectionList.this.getReadWriteLock().equals(children.getReadWriteLock())) {
                throw new IllegalArgumentException("If a CollectionList.Model returns EventLists, those EventLists must use the same ReadWriteLock as the CollectionList");
            }
            children.getPublisher().setRelatedSubject(this, CollectionList.this);
            children.addListEventListener(this);
        }

        @Override
        public E get(int index) {
            return this.children.get(index);
        }

        @Override
        public E remove(int index) {
            return this.children.remove(index);
        }

        @Override
        public E set(int index, E element) {
            return this.children.set(index, element);
        }

        @Override
        public void listChanged(ListEvent<E> listChanges) {
            int firstChildIndex;
            int parentIndex = CollectionList.this.childElements.indexOfNode(this.node, 1);
            int absoluteIndex = CollectionList.this.getAbsoluteIndex(parentIndex);
            int nextNodeIndex = CollectionList.this.getAbsoluteIndex(parentIndex + 1);
            int previousChildrenCount = nextNodeIndex - (firstChildIndex = absoluteIndex + 1);
            if (previousChildrenCount > 0) {
                CollectionList.this.barcode.remove(firstChildIndex, previousChildrenCount);
            }
            if (!this.children.isEmpty()) {
                CollectionList.this.barcode.addWhite(firstChildIndex, this.children.size());
            }
            int childOffset = absoluteIndex - parentIndex;
            CollectionList.this.updates.beginEvent();
            while (listChanges.next()) {
                int index = listChanges.getIndex();
                int type = listChanges.getType();
                int overallIndex = index + childOffset;
                switch (type) {
                    case 2: {
                        CollectionList.this.updates.elementInserted(overallIndex, listChanges.getNewValue());
                        break;
                    }
                    case 1: {
                        CollectionList.this.updates.elementUpdated(overallIndex, listChanges.getOldValue(), listChanges.getNewValue());
                        break;
                    }
                    case 0: {
                        CollectionList.this.updates.elementDeleted(overallIndex, listChanges.getOldValue());
                    }
                }
            }
            CollectionList.this.updates.commitEvent();
        }

        @Override
        public void dispose() {
            this.children.removeListEventListener(this);
            this.children.getPublisher().clearRelatedSubject(this);
        }

        public String toString() {
            return "[" + CollectionList.this.childElements.indexOfNode(this.node, 0) + ":" + this.children + "]";
        }
    }

    public static interface Model<E, S> {
        public List<S> getChildren(E var1);
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class SimpleChildElement
    implements ChildElement<E> {
        private final List<E> children;
        private final Element<ChildElement<E>> node;

        public SimpleChildElement(List<E> children, Element<ChildElement<E>> node) {
            this.children = children;
            this.node = node;
        }

        @Override
        public E get(int index) {
            return this.children.get(index);
        }

        @Override
        public E remove(int index) {
            E removed = this.children.remove(index);
            int parentIndex = CollectionList.this.childElements.indexOfNode(this.node, 0);
            int absoluteIndex = CollectionList.this.getAbsoluteIndex(parentIndex);
            int firstChildIndex = absoluteIndex + 1;
            CollectionList.this.barcode.remove(firstChildIndex + index, 1);
            int childOffset = absoluteIndex - parentIndex;
            CollectionList.this.updates.beginEvent();
            CollectionList.this.updates.elementDeleted(index + childOffset, removed);
            CollectionList.this.updates.commitEvent();
            return removed;
        }

        @Override
        public E set(int index, E element) {
            E replaced = this.children.set(index, element);
            int parentIndex = CollectionList.this.childElements.indexOfNode(this.node, 0);
            int absoluteIndex = CollectionList.this.getAbsoluteIndex(parentIndex);
            int childOffset = absoluteIndex - parentIndex;
            CollectionList.this.updates.beginEvent();
            CollectionList.this.updates.elementUpdated(index + childOffset, replaced);
            CollectionList.this.updates.commitEvent();
            return replaced;
        }

        @Override
        public void dispose() {
        }
    }

}

