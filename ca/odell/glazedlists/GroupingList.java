/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.SortedList;
import ca.odell.glazedlists.TransformedList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventAssembler;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.impl.Grouper;
import ca.odell.glazedlists.impl.adt.Barcode;
import ca.odell.glazedlists.impl.adt.barcode2.Element;
import ca.odell.glazedlists.impl.adt.barcode2.SimpleTree;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class GroupingList<E>
extends TransformedList<E, List<E>> {
    private SimpleTree<GroupingList<E>> groupLists = new SimpleTree();
    private final Grouper<E> grouper;

    public static <E extends Comparable<? super E>> GroupingList<E> create(EventList<E> source) {
        return new GroupingList<E>(source);
    }

    public GroupingList(EventList<E> source) {
        this(source, GlazedLists.comparableComparator());
    }

    public GroupingList(EventList<E> source, Comparator<? super E> comparator) {
        this(new SortedList<E>(source, comparator), comparator, null);
    }

    private GroupingList(SortedList<E> source, Comparator<? super E> comparator, Void dummyParameter) {
        super(source);
        this.grouper = new Grouper<E>(source, new GrouperClient());
        this.rebuildGroupListTreeFromBarcode();
        source.addListEventListener(this);
    }

    private void rebuildGroupListTreeFromBarcode() {
        this.groupLists.clear();
        GrouperClient grouperClient = (GrouperClient)this.grouper.getClient();
        int n = this.grouper.getBarcode().colourSize(Grouper.UNIQUE);
        for (int i = 0; i < n; ++i) {
            grouperClient.insertGroupList(i);
        }
    }

    public int indexOfGroup(E groupElement) {
        int sourceIndex = ((SortedList)this.source).sortIndex(groupElement);
        if (sourceIndex == this.source.size() || this.grouper.getComparator().compare(this.source.get(sourceIndex), groupElement) != 0) {
            return -1;
        }
        return this.grouper.getBarcode().getColourIndex(sourceIndex, Grouper.UNIQUE);
    }

    public void setComparator(Comparator<? super E> comparator) {
        if (comparator == null) {
            comparator = GlazedLists.comparableComparator();
        }
        ((SortedList)this.source).setComparator(comparator);
    }

    @Override
    protected int getSourceIndex(int index) {
        return this.grouper.getBarcode().getIndex(index, Grouper.UNIQUE);
    }

    @Override
    protected boolean isWritable() {
        return true;
    }

    @Override
    public void listChanged(ListEvent<E> listChanges) {
        this.updates.beginEvent(true);
        SortedList sortedSource = (SortedList)this.source;
        Comparator sourceComparator = sortedSource.getComparator();
        if (sourceComparator != this.grouper.getComparator()) {
            int n = this.size();
            for (int i = 0; i < n; ++i) {
                this.updates.elementDeleted(0, this.get(i));
            }
            this.grouper.setComparator(sourceComparator);
            this.rebuildGroupListTreeFromBarcode();
            this.updates.addInsert(0, this.size() - 1);
        } else {
            this.grouper.listChanged(listChanges);
        }
        this.updates.commitEvent();
    }

    @Override
    public List<E> get(int index) {
        return this.groupLists.get(index).get();
    }

    @Override
    public List<E> remove(int index) {
        if (index < 0 || index >= this.size()) {
            throw new IndexOutOfBoundsException("Cannot remove at " + index + " on list of size " + this.size());
        }
        Object removed = this.get(index);
        ArrayList result = new ArrayList(removed);
        removed.clear();
        return result;
    }

    @Override
    public List<E> set(int index, List<E> value) {
        if (index < 0 || index >= this.size()) {
            throw new IndexOutOfBoundsException("Cannot set at " + index + " on list of size " + this.size());
        }
        this.updates.beginEvent(true);
        Object result = this.remove(index);
        this.add(index, value);
        this.updates.commitEvent();
        return result;
    }

    @Override
    public void add(int index, List<E> value) {
        this.source.addAll(value);
    }

    @Override
    public int size() {
        return this.grouper.getBarcode().colourSize(Grouper.UNIQUE);
    }

    @Override
    public void dispose() {
        ((SortedList)this.source).dispose();
        super.dispose();
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class GroupList
    extends AbstractList<E> {
        private Element<GroupingList<E>> treeNode;

        private GroupList() {
        }

        private void setTreeNode(Element<GroupingList<E>> treeNode) {
            this.treeNode = treeNode;
        }

        private int getStartIndex() {
            if (this.treeNode == null) {
                return -1;
            }
            int groupIndex = GroupingList.this.groupLists.indexOfNode(this.treeNode, 1);
            return GroupingList.this.getSourceIndex(groupIndex);
        }

        private int getEndIndex() {
            if (this.treeNode == null) {
                return -1;
            }
            int groupIndex = GroupingList.this.groupLists.indexOfNode(this.treeNode, 1);
            if (groupIndex < GroupingList.this.grouper.getBarcode().blackSize() - 1) {
                return GroupingList.this.grouper.getBarcode().getIndex(groupIndex + 1, Grouper.UNIQUE);
            }
            return GroupingList.this.grouper.getBarcode().size();
        }

        private int getSourceIndex(int index) {
            return this.getStartIndex() + index;
        }

        @Override
        public E set(int index, E element) {
            return GroupingList.this.source.set(this.getSourceIndex(index), element);
        }

        @Override
        public E get(int index) {
            return GroupingList.this.source.get(this.getSourceIndex(index));
        }

        @Override
        public int size() {
            return this.getEndIndex() - this.getStartIndex();
        }

        @Override
        public void clear() {
            GroupingList.this.source.subList(this.getStartIndex(), this.getEndIndex()).clear();
        }

        @Override
        public E remove(int index) {
            return GroupingList.this.source.remove(this.getSourceIndex(index));
        }

        @Override
        public void add(int index, E element) {
            GroupingList.this.source.add(this.getSourceIndex(index), element);
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class GrouperClient
    implements Grouper.Client<E> {
        private GrouperClient() {
        }

        @Override
        public void groupChanged(int index, int groupIndex, int groupChangeType, boolean primary, int elementChangeType, E oldValue, E newValue) {
            if (groupChangeType == 2) {
                this.insertGroupList(groupIndex);
                GroupingList.this.updates.addInsert(groupIndex);
            } else if (groupChangeType == 0) {
                this.removeGroupList(groupIndex);
                GroupingList.this.updates.addDelete(groupIndex);
            } else if (groupChangeType == 1) {
                GroupingList.this.updates.addUpdate(groupIndex);
            } else {
                throw new IllegalStateException();
            }
        }

        private void insertGroupList(int index) {
            GroupList groupList = new GroupList();
            Element<GroupList> indexedTreeNode = GroupingList.this.groupLists.add(index, groupList, 1);
            groupList.setTreeNode(indexedTreeNode);
        }

        private void removeGroupList(int index) {
            Element indexedTreeNode = GroupingList.this.groupLists.get(index);
            GroupingList.this.groupLists.remove(indexedTreeNode);
            ((GroupList)indexedTreeNode.get()).setTreeNode(null);
        }
    }

}

