/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.gui;

public interface TableFormat<E> {
    public int getColumnCount();

    public String getColumnName(int var1);

    public Object getColumnValue(E var1, int var2);
}

