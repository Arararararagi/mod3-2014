/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.gui;

import ca.odell.glazedlists.gui.TableFormat;

public interface WritableTableFormat<E>
extends TableFormat<E> {
    public boolean isEditable(E var1, int var2);

    public E setColumnValue(E var1, Object var2, int var3);
}

