/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.gui;

import ca.odell.glazedlists.gui.TableFormat;
import java.util.Comparator;

public interface AdvancedTableFormat<E>
extends TableFormat<E> {
    public Class getColumnClass(int var1);

    public Comparator getColumnComparator(int var1);
}

