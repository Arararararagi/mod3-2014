/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.TransformedList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventAssembler;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.impl.adt.Barcode;
import ca.odell.glazedlists.impl.adt.BarcodeIterator;
import ca.odell.glazedlists.matchers.Matcher;
import ca.odell.glazedlists.matchers.MatcherEditor;
import ca.odell.glazedlists.matchers.Matchers;
import ca.odell.glazedlists.util.concurrent.Lock;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class FilterList<E>
extends TransformedList<E, E> {
    private Barcode flagList = new Barcode();
    private Matcher<? super E> currentMatcher = Matchers.trueMatcher();
    private MatcherEditor<? super E> currentEditor = null;
    private final MatcherEditor.Listener listener;

    public FilterList(EventList<E> source) {
        super(source);
        this.listener = new PrivateMatcherEditorListener();
        this.flagList.addBlack(0, source.size());
        source.addListEventListener(this);
    }

    public FilterList(EventList<E> source, Matcher<? super E> matcher) {
        this(source);
        if (matcher == null) {
            return;
        }
        this.currentMatcher = matcher;
    }

    public FilterList(EventList<E> source, MatcherEditor<? super E> matcherEditor) {
        this(source);
        if (matcherEditor == null) {
            return;
        }
        this.currentEditor = matcherEditor;
        this.currentEditor.addMatcherEditorListener(this.listener);
        this.currentMatcher = this.currentEditor.getMatcher();
    }

    public void setMatcher(Matcher<? super E> matcher) {
        if (this.currentEditor != null) {
            this.currentEditor.removeMatcherEditorListener(this.listener);
            this.currentEditor = null;
        }
        if (matcher != null) {
            this.changeMatcherWithLocks(this.currentEditor, matcher, 4);
        } else {
            this.changeMatcherWithLocks(this.currentEditor, null, 0);
        }
    }

    public void setMatcherEditor(MatcherEditor<? super E> editor) {
        if (this.currentEditor != null) {
            this.currentEditor.removeMatcherEditorListener(this.listener);
        }
        this.currentEditor = editor;
        if (this.currentEditor != null) {
            this.currentEditor.addMatcherEditorListener(this.listener);
            this.changeMatcherWithLocks(this.currentEditor, this.currentEditor.getMatcher(), 4);
        } else {
            this.changeMatcherWithLocks(this.currentEditor, null, 0);
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        if (this.currentEditor != null) {
            this.currentEditor.removeMatcherEditorListener(this.listener);
            this.currentEditor = null;
        }
        this.currentMatcher = null;
    }

    @Override
    public final void listChanged(ListEvent<E> listChanges) {
        this.updates.beginEvent();
        if (listChanges.isReordering()) {
            int[] sourceReorderMap = listChanges.getReorderMap();
            int[] filterReorderMap = new int[this.flagList.blackSize()];
            Barcode previousFlagList = this.flagList;
            this.flagList = new Barcode();
            for (int i = 0; i < sourceReorderMap.length; ++i) {
                Object flag = previousFlagList.get(sourceReorderMap[i]);
                this.flagList.add(i, flag, 1);
                if (flag == Barcode.WHITE) continue;
                filterReorderMap[this.flagList.getBlackIndex((int)i)] = previousFlagList.getBlackIndex(sourceReorderMap[i]);
            }
            this.updates.reorder(filterReorderMap);
        } else {
            while (listChanges.next()) {
                int filteredIndex;
                int sourceIndex = listChanges.getIndex();
                int changeType = listChanges.getType();
                if (changeType == 0) {
                    filteredIndex = this.flagList.getBlackIndex(sourceIndex);
                    if (filteredIndex != -1) {
                        E removed = listChanges.getOldValue();
                        this.updates.elementDeleted(filteredIndex, removed);
                    }
                    this.flagList.remove(sourceIndex, 1);
                    continue;
                }
                if (changeType == 2) {
                    Object element = this.source.get(sourceIndex);
                    boolean include = this.currentMatcher.matches(element);
                    if (include) {
                        this.flagList.addBlack(sourceIndex, 1);
                        int filteredIndex2 = this.flagList.getBlackIndex(sourceIndex);
                        this.updates.elementInserted(filteredIndex2, element);
                        continue;
                    }
                    this.flagList.addWhite(sourceIndex, 1);
                    continue;
                }
                if (changeType != 1) continue;
                filteredIndex = this.flagList.getBlackIndex(sourceIndex);
                boolean wasIncluded = filteredIndex != -1;
                Object updated = this.source.get(sourceIndex);
                boolean include = this.currentMatcher.matches(updated);
                if (wasIncluded && !include) {
                    this.flagList.setWhite(sourceIndex, 1);
                    this.updates.elementDeleted(filteredIndex, listChanges.getOldValue());
                    continue;
                }
                if (!wasIncluded && include) {
                    this.flagList.setBlack(sourceIndex, 1);
                    this.updates.elementInserted(this.flagList.getBlackIndex(sourceIndex), updated);
                    continue;
                }
                if (!wasIncluded || !include) continue;
                this.updates.elementUpdated(filteredIndex, listChanges.getOldValue(), updated);
            }
        }
        this.updates.commitEvent();
    }

    private void changeMatcherWithLocks(MatcherEditor<? super E> matcherEditor, Matcher<? super E> matcher, int changeType) {
        this.getReadWriteLock().writeLock().lock();
        try {
            this.changeMatcher(matcherEditor, matcher, changeType);
        }
        finally {
            this.getReadWriteLock().writeLock().unlock();
        }
    }

    private void changeMatcher(MatcherEditor<? super E> matcherEditor, Matcher<? super E> matcher, int changeType) {
        if (this.currentEditor != matcherEditor) {
            throw new IllegalStateException();
        }
        switch (changeType) {
            case 2: {
                this.currentMatcher = matcher;
                this.constrained();
                break;
            }
            case 3: {
                this.currentMatcher = matcher;
                this.relaxed();
                break;
            }
            case 4: {
                this.currentMatcher = matcher;
                this.changed();
                break;
            }
            case 0: {
                this.currentMatcher = Matchers.trueMatcher();
                this.matchAll();
                break;
            }
            case 1: {
                this.currentMatcher = Matchers.falseMatcher();
                this.matchNone();
            }
        }
    }

    private void matchNone() {
        this.updates.beginEvent();
        for (int i = 0; i < this.size(); ++i) {
            this.updates.elementDeleted(0, this.get(i));
        }
        this.flagList.clear();
        this.flagList.addWhite(0, this.source.size());
        this.updates.commitEvent();
    }

    private void matchAll() {
        this.updates.beginEvent();
        BarcodeIterator i = this.flagList.iterator();
        while (i.hasNextWhite()) {
            i.nextWhite();
            int index = i.getIndex();
            this.updates.elementInserted(index, this.source.get(index));
        }
        this.flagList.clear();
        this.flagList.addBlack(0, this.source.size());
        this.updates.commitEvent();
    }

    private void relaxed() {
        this.updates.beginEvent();
        BarcodeIterator i = this.flagList.iterator();
        while (i.hasNextWhite()) {
            i.nextWhite();
            Object element = this.source.get(i.getIndex());
            if (!this.currentMatcher.matches(element)) continue;
            this.updates.elementInserted(i.setBlack(), element);
        }
        this.updates.commitEvent();
    }

    private void constrained() {
        this.updates.beginEvent();
        BarcodeIterator i = this.flagList.iterator();
        while (i.hasNextBlack()) {
            i.nextBlack();
            Object value = this.source.get(i.getIndex());
            if (this.currentMatcher.matches(value)) continue;
            int blackIndex = i.getBlackIndex();
            i.setWhite();
            this.updates.elementDeleted(blackIndex, value);
        }
        this.updates.commitEvent();
    }

    private void changed() {
        this.updates.beginEvent();
        BarcodeIterator i = this.flagList.iterator();
        while (i.hasNext()) {
            i.next();
            int filteredIndex = i.getBlackIndex();
            boolean wasIncluded = filteredIndex != -1;
            Object value = this.source.get(i.getIndex());
            boolean include = this.currentMatcher.matches(value);
            if (wasIncluded && !include) {
                i.setWhite();
                this.updates.elementDeleted(filteredIndex, value);
                continue;
            }
            if (wasIncluded || !include) continue;
            this.updates.elementInserted(i.setBlack(), value);
        }
        this.updates.commitEvent();
    }

    @Override
    public final int size() {
        return this.flagList.blackSize();
    }

    @Override
    protected final int getSourceIndex(int mutationIndex) {
        return this.flagList.getIndex(mutationIndex, Barcode.BLACK);
    }

    @Override
    protected boolean isWritable() {
        return true;
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class PrivateMatcherEditorListener
    implements MatcherEditor.Listener<E> {
        private PrivateMatcherEditorListener() {
        }

        @Override
        public void changedMatcher(MatcherEditor.Event<E> matcherEvent) {
            MatcherEditor<E> matcherEditor = matcherEvent.getMatcherEditor();
            Matcher<E> matcher = matcherEvent.getMatcher();
            int changeType = matcherEvent.getType();
            FilterList.this.changeMatcherWithLocks(matcherEditor, matcher, changeType);
        }
    }

}

