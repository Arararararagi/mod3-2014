/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.util.concurrent;

import ca.odell.glazedlists.util.concurrent.J2SE14LockFactory;
import ca.odell.glazedlists.util.concurrent.Lock;
import ca.odell.glazedlists.util.concurrent.LockFactory;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;

class DelegateLockFactory
implements LockFactory {
    private LockFactory delegate;

    DelegateLockFactory() {
        try {
            Class.forName("java.util.concurrent.locks.ReadWriteLock");
            this.delegate = (LockFactory)Class.forName("ca.odell.glazedlists.impl.java15.J2SE50LockFactory").newInstance();
        }
        catch (Throwable t) {
            this.delegate = new J2SE14LockFactory();
        }
    }

    public ReadWriteLock createReadWriteLock() {
        return this.delegate.createReadWriteLock();
    }

    public Lock createLock() {
        return this.delegate.createLock();
    }
}

