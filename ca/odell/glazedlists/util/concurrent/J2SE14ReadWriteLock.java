/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.util.concurrent;

import ca.odell.glazedlists.impl.SerializedReadWriteLock;
import ca.odell.glazedlists.util.concurrent.Lock;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.HashMap;

public class J2SE14ReadWriteLock
implements ReadWriteLock,
Serializable {
    private static final long serialVersionUID = -3463448656717690166L;
    final ReadLock readerLock_;
    final WriteLock writerLock_;
    final Sync sync;

    public J2SE14ReadWriteLock() {
        this.readerLock_ = new ReadLock(this);
        this.writerLock_ = new WriteLock(this);
        this.sync = new NonfairSync();
    }

    public Lock writeLock() {
        return this.writerLock_;
    }

    public Lock readLock() {
        return this.readerLock_;
    }

    private Object writeReplace() throws ObjectStreamException {
        return new SerializedReadWriteLock();
    }

    public final boolean isFair() {
        return false;
    }

    protected Thread getOwner() {
        return this.sync.getOwner();
    }

    public int getReadLockCount() {
        return this.sync.getReadLockCount();
    }

    public boolean isWriteLocked() {
        return this.sync.isWriteLocked();
    }

    public boolean isWriteLockedByCurrentThread() {
        return this.sync.isWriteLockedByCurrentThread();
    }

    public int getWriteHoldCount() {
        return this.sync.getWriteHoldCount();
    }

    public int getReadHoldCount() {
        return this.sync.getReadHoldCount();
    }

    public final boolean hasQueuedThreads() {
        return this.sync.hasQueuedThreads();
    }

    public final int getQueueLength() {
        return this.sync.getQueueLength();
    }

    public String toString() {
        return super.toString() + "[Write locks = " + this.getWriteHoldCount() + ", Read locks = " + this.getReadLockCount() + "]";
    }

    private static class NonfairSync
    extends Sync {
        NonfairSync() {
        }
    }

    public static class ReadLock
    implements Lock,
    Serializable {
        private static final long serialVersionUID = -5992448646407690164L;
        final J2SE14ReadWriteLock lock;

        protected ReadLock(J2SE14ReadWriteLock lock) {
            if (lock == null) {
                throw new NullPointerException();
            }
            this.lock = lock;
        }

        public void lock() {
            ReadLock readLock = this;
            synchronized (readLock) {
                if (this.lock.sync.startReadFromNewReader()) {
                    return;
                }
                boolean wasInterrupted = Thread.interrupted();
                try {
                    do {
                        try {
                            this.wait();
                            continue;
                        }
                        catch (InterruptedException ex) {
                            wasInterrupted = true;
                        }
                    } while (!this.lock.sync.startReadFromWaitingReader());
                    return;
                }
                finally {
                    if (wasInterrupted) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        public void lockInterruptibly() throws InterruptedException {
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            InterruptedException ie = null;
            ReadLock readLock = this;
            synchronized (readLock) {
                if (!this.lock.sync.startReadFromNewReader()) {
                    try {
                        do {
                            this.wait();
                        } while (!this.lock.sync.startReadFromWaitingReader());
                        return;
                    }
                    catch (InterruptedException ex) {
                        this.lock.sync.cancelledWaitingReader();
                        ie = ex;
                    }
                }
            }
            if (ie != null) {
                this.lock.writerLock_.signalWaiters();
                throw ie;
            }
        }

        public boolean tryLock() {
            return this.lock.sync.startRead();
        }

        public void unlock() {
            switch (this.lock.sync.endRead()) {
                case 0: {
                    return;
                }
                case 1: {
                    this.lock.readerLock_.signalWaiters();
                    return;
                }
                case 2: {
                    this.lock.writerLock_.signalWaiters();
                    return;
                }
            }
        }

        synchronized void signalWaiters() {
            this.notifyAll();
        }

        public String toString() {
            int r = this.lock.getReadLockCount();
            return super.toString() + "[Read locks = " + r + "]";
        }
    }

    private static abstract class Sync
    implements Serializable {
        private static final int NONE = 0;
        private static final int READER = 1;
        private static final int WRITER = 2;
        transient int activeReaders_ = 0;
        transient Thread activeWriter_ = null;
        transient int waitingReaders_ = 0;
        transient int waitingWriters_ = 0;
        transient int writeHolds_ = 0;
        transient HashMap readers_ = new HashMap();
        static final Integer IONE = new Integer(1);

        Sync() {
        }

        synchronized boolean startReadFromNewReader() {
            boolean pass = this.startRead();
            if (!pass) {
                ++this.waitingReaders_;
            }
            return pass;
        }

        synchronized boolean startWriteFromNewWriter() {
            boolean pass = this.startWrite();
            if (!pass) {
                ++this.waitingWriters_;
            }
            return pass;
        }

        synchronized boolean startReadFromWaitingReader() {
            boolean pass = this.startRead();
            if (pass) {
                --this.waitingReaders_;
            }
            return pass;
        }

        synchronized boolean startWriteFromWaitingWriter() {
            boolean pass = this.startWrite();
            if (pass) {
                --this.waitingWriters_;
            }
            return pass;
        }

        synchronized void cancelledWaitingReader() {
            --this.waitingReaders_;
        }

        synchronized void cancelledWaitingWriter() {
            --this.waitingWriters_;
        }

        boolean allowReader() {
            return this.activeWriter_ == null && this.waitingWriters_ == 0 || this.activeWriter_ == Thread.currentThread();
        }

        synchronized boolean startRead() {
            Thread t = Thread.currentThread();
            Integer c = (Integer)this.readers_.get(t);
            if (c != null) {
                this.readers_.put(t, new Integer(c + 1));
                ++this.activeReaders_;
                return true;
            }
            if (this.allowReader()) {
                this.readers_.put(t, IONE);
                ++this.activeReaders_;
                return true;
            }
            return false;
        }

        synchronized boolean startWrite() {
            if (this.activeWriter_ == Thread.currentThread()) {
                ++this.writeHolds_;
                return true;
            }
            if (this.writeHolds_ == 0) {
                if (this.activeReaders_ == 0 || this.readers_.size() == 1 && this.readers_.get(Thread.currentThread()) != null) {
                    this.activeWriter_ = Thread.currentThread();
                    this.writeHolds_ = 1;
                    return true;
                }
                return false;
            }
            return false;
        }

        synchronized int endRead() {
            Thread t = Thread.currentThread();
            Integer c = (Integer)this.readers_.get(t);
            if (c == null) {
                throw new IllegalMonitorStateException();
            }
            --this.activeReaders_;
            if (c != IONE) {
                int h = c - 1;
                Integer ih = h == 1 ? IONE : new Integer(h);
                this.readers_.put(t, ih);
                return 0;
            }
            this.readers_.remove(t);
            if (this.writeHolds_ > 0) {
                return 0;
            }
            if (this.activeReaders_ == 0 && this.waitingWriters_ > 0) {
                return 2;
            }
            return 0;
        }

        synchronized int endWrite() {
            if (this.activeWriter_ != Thread.currentThread()) {
                throw new IllegalMonitorStateException();
            }
            --this.writeHolds_;
            if (this.writeHolds_ > 0) {
                return 0;
            }
            this.activeWriter_ = null;
            if (this.waitingReaders_ > 0 && this.allowReader()) {
                return 1;
            }
            if (this.waitingWriters_ > 0) {
                return 2;
            }
            return 0;
        }

        synchronized Thread getOwner() {
            return this.activeWriter_;
        }

        synchronized int getReadLockCount() {
            return this.activeReaders_;
        }

        synchronized boolean isWriteLocked() {
            return this.activeWriter_ != null;
        }

        synchronized boolean isWriteLockedByCurrentThread() {
            return this.activeWriter_ == Thread.currentThread();
        }

        synchronized int getWriteHoldCount() {
            return this.isWriteLockedByCurrentThread() ? this.writeHolds_ : 0;
        }

        synchronized int getReadHoldCount() {
            if (this.activeReaders_ == 0) {
                return 0;
            }
            Thread t = Thread.currentThread();
            Integer i = (Integer)this.readers_.get(t);
            return i == null ? 0 : i;
        }

        final synchronized boolean hasQueuedThreads() {
            return this.waitingWriters_ > 0 || this.waitingReaders_ > 0;
        }

        final synchronized int getQueueLength() {
            return this.waitingWriters_ + this.waitingReaders_;
        }

        private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
            in.defaultReadObject();
            Sync sync = this;
            synchronized (sync) {
                this.readers_ = new HashMap();
            }
        }
    }

    public static class WriteLock
    implements Lock,
    Serializable {
        private static final long serialVersionUID = -4992448646407690164L;
        final J2SE14ReadWriteLock lock;

        protected WriteLock(J2SE14ReadWriteLock lock) {
            if (lock == null) {
                throw new NullPointerException();
            }
            this.lock = lock;
        }

        public void lock() {
            WriteLock writeLock = this;
            synchronized (writeLock) {
                if (this.lock.sync.startWriteFromNewWriter()) {
                    return;
                }
                boolean wasInterrupted = Thread.interrupted();
                try {
                    do {
                        try {
                            this.wait();
                            continue;
                        }
                        catch (InterruptedException ex) {
                            wasInterrupted = true;
                        }
                    } while (!this.lock.sync.startWriteFromWaitingWriter());
                    return;
                }
                finally {
                    if (wasInterrupted) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        public void lockInterruptibly() throws InterruptedException {
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            InterruptedException ie = null;
            WriteLock writeLock = this;
            synchronized (writeLock) {
                if (!this.lock.sync.startWriteFromNewWriter()) {
                    try {
                        do {
                            this.wait();
                        } while (!this.lock.sync.startWriteFromWaitingWriter());
                        return;
                    }
                    catch (InterruptedException ex) {
                        this.lock.sync.cancelledWaitingWriter();
                        this.notify();
                        ie = ex;
                    }
                }
            }
            if (ie != null) {
                this.lock.readerLock_.signalWaiters();
                throw ie;
            }
        }

        public boolean tryLock() {
            return this.lock.sync.startWrite();
        }

        public void unlock() {
            switch (this.lock.sync.endWrite()) {
                case 0: {
                    return;
                }
                case 1: {
                    this.lock.readerLock_.signalWaiters();
                    return;
                }
                case 2: {
                    this.lock.writerLock_.signalWaiters();
                    return;
                }
            }
        }

        synchronized void signalWaiters() {
            this.notify();
        }

        public String toString() {
            Thread o;
            return super.toString() + ((o = this.lock.getOwner()) == null ? "[Unlocked]" : new StringBuilder().append("[Locked by thread ").append(o.getName()).append("]").toString());
        }

        public boolean isHeldByCurrentThread() {
            return this.lock.sync.isWriteLockedByCurrentThread();
        }

        public int getHoldCount() {
            return this.lock.sync.getWriteHoldCount();
        }
    }

}

