/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.util.concurrent;

import ca.odell.glazedlists.util.concurrent.DelegateLockFactory;
import ca.odell.glazedlists.util.concurrent.Lock;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;

public interface LockFactory {
    public static final LockFactory DEFAULT = new DelegateLockFactory();

    public ReadWriteLock createReadWriteLock();

    public Lock createLock();
}

