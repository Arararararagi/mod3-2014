/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.util.concurrent;

import ca.odell.glazedlists.util.concurrent.Lock;

public interface ReadWriteLock {
    public Lock readLock();

    public Lock writeLock();
}

