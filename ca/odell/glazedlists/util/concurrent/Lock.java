/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.util.concurrent;

public interface Lock {
    public void lock();

    public boolean tryLock();

    public void unlock();
}

