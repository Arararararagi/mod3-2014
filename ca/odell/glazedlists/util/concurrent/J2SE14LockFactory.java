/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.util.concurrent;

import ca.odell.glazedlists.util.concurrent.J2SE14ReadWriteLock;
import ca.odell.glazedlists.util.concurrent.Lock;
import ca.odell.glazedlists.util.concurrent.LockFactory;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;

final class J2SE14LockFactory
implements LockFactory {
    J2SE14LockFactory() {
    }

    public ReadWriteLock createReadWriteLock() {
        return new J2SE14ReadWriteLock();
    }

    public Lock createLock() {
        return new J2SE14ReadWriteLock().writeLock();
    }
}

