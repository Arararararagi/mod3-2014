/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists;

import java.util.List;

public interface Filterator<C, E> {
    public void getFilterValues(List<C> var1, E var2);
}

