/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists;

import ca.odell.glazedlists.AbstractEventList;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEventAssembler;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.event.ListEventPublisher;
import ca.odell.glazedlists.util.concurrent.LockFactory;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class BasicEventList<E>
extends AbstractEventList<E>
implements Serializable,
RandomAccess {
    private static final long serialVersionUID = 4883958173323072345L;
    private List<E> data;

    public BasicEventList() {
        this(LockFactory.DEFAULT.createReadWriteLock());
    }

    public BasicEventList(ReadWriteLock readWriteLock) {
        this(null, readWriteLock);
    }

    public BasicEventList(int initalCapacity) {
        this(initalCapacity, null, LockFactory.DEFAULT.createReadWriteLock());
    }

    public BasicEventList(ListEventPublisher publisher, ReadWriteLock readWriteLock) {
        this(10, publisher, readWriteLock);
    }

    public BasicEventList(int initialCapacity, ListEventPublisher publisher, ReadWriteLock readWriteLock) {
        super(publisher);
        this.data = new ArrayList(initialCapacity);
        this.readWriteLock = readWriteLock;
    }

    public BasicEventList(List<E> list) {
        super(null);
        this.data = list;
        this.readWriteLock = LockFactory.DEFAULT.createReadWriteLock();
    }

    @Override
    public void add(int index, E element) {
        this.updates.beginEvent();
        this.updates.elementInserted(index, element);
        this.data.add(index, element);
        this.updates.commitEvent();
    }

    @Override
    public boolean add(E element) {
        this.updates.beginEvent();
        this.updates.elementInserted(this.size(), element);
        boolean result = this.data.add(element);
        this.updates.commitEvent();
        return result;
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        return this.addAll(this.size(), collection);
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> collection) {
        if (collection.size() == 0) {
            return false;
        }
        this.updates.beginEvent();
        for (E value : collection) {
            this.updates.elementInserted(index, value);
            this.data.add(index, value);
            ++index;
        }
        this.updates.commitEvent();
        return !collection.isEmpty();
    }

    @Override
    public E remove(int index) {
        this.updates.beginEvent();
        E removed = this.data.remove(index);
        this.updates.elementDeleted(index, removed);
        this.updates.commitEvent();
        return removed;
    }

    @Override
    public boolean remove(Object element) {
        int index = this.data.indexOf(element);
        if (index == -1) {
            return false;
        }
        this.remove(index);
        return true;
    }

    @Override
    public void clear() {
        if (this.isEmpty()) {
            return;
        }
        this.updates.beginEvent();
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            this.updates.elementDeleted(0, this.get(i));
        }
        this.data.clear();
        this.updates.commitEvent();
    }

    @Override
    public E set(int index, E element) {
        this.updates.beginEvent();
        E previous = this.data.set(index, element);
        this.updates.elementUpdated(index, previous);
        this.updates.commitEvent();
        return previous;
    }

    @Override
    public E get(int index) {
        return this.data.get(index);
    }

    @Override
    public int size() {
        return this.data.size();
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        boolean changed = false;
        this.updates.beginEvent();
        for (Object value : collection) {
            int index = -1;
            while ((index = this.indexOf(value)) != -1) {
                E removed = this.data.remove(index);
                this.updates.elementDeleted(index, removed);
                changed = true;
            }
        }
        this.updates.commitEvent();
        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        boolean changed = false;
        this.updates.beginEvent();
        int index = 0;
        while (index < this.data.size()) {
            if (collection.contains(this.data.get(index))) {
                ++index;
                continue;
            }
            E removed = this.data.remove(index);
            this.updates.elementDeleted(index, removed);
            changed = true;
        }
        this.updates.commitEvent();
        return changed;
    }

    @Override
    public void dispose() {
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        Object[] elements = this.data.toArray(new Object[this.data.size()]);
        ArrayList serializableListeners = new ArrayList(1);
        for (ListEventListener listener : this.updates.getListEventListeners()) {
            if (!(listener instanceof Serializable)) continue;
            serializableListeners.add(listener);
        }
        ListEventListener[] listeners = serializableListeners.toArray(new ListEventListener[serializableListeners.size()]);
        out.writeObject(elements);
        out.writeObject(listeners);
        out.writeObject(this.getPublisher());
        out.writeObject(this.getReadWriteLock());
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        Object[] elements = (Object[])in.readObject();
        ListEventListener[] listeners = (ListEventListener[])in.readObject();
        try {
            this.publisher = (ListEventPublisher)in.readObject();
            this.updates = new ListEventAssembler(this, this.publisher);
            this.readWriteLock = (ReadWriteLock)in.readObject();
        }
        catch (OptionalDataException e) {
            if (e.eof) {
                this.readWriteLock = LockFactory.DEFAULT.createReadWriteLock();
            }
            throw e;
        }
        this.data = new ArrayList(elements.length);
        this.data.addAll(Arrays.asList(elements));
        for (int i = 0; i < listeners.length; ++i) {
            this.updates.addListEventListener(listeners[i]);
        }
    }
}

