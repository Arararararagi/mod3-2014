/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.event;

import ca.odell.glazedlists.EventList;
import java.util.EventObject;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public abstract class ListEvent<E>
extends EventObject {
    public static final int DELETE = 0;
    public static final int UPDATE = 1;
    public static final int INSERT = 2;
    public static final Object UNKNOWN_VALUE = new String("UNKNOWN VALUE");
    protected EventList<E> sourceList;

    ListEvent(EventList<E> sourceList) {
        super(sourceList);
        this.sourceList = sourceList;
    }

    public abstract ListEvent<E> copy();

    public abstract void reset();

    public abstract boolean next();

    public abstract boolean hasNext();

    public abstract boolean nextBlock();

    public abstract boolean isReordering();

    public abstract int[] getReorderMap();

    public abstract int getIndex();

    public abstract int getBlockStartIndex();

    public abstract int getBlockEndIndex();

    public abstract int getType();

    public abstract E getOldValue();

    public abstract E getNewValue();

    public abstract int getBlocksRemaining();

    public EventList<E> getSourceList() {
        return this.sourceList;
    }

    @Override
    public abstract String toString();
}

