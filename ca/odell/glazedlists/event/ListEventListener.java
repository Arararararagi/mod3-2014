/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.event;

import ca.odell.glazedlists.event.ListEvent;
import java.util.EventListener;

public interface ListEventListener<E>
extends EventListener {
    public void listChanged(ListEvent<E> var1);
}

