/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.event;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventAssembler;
import ca.odell.glazedlists.impl.event.BlockSequence;
import ca.odell.glazedlists.impl.event.Tree4Deltas;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
class Tree4DeltasListEvent<E>
extends ListEvent<E> {
    private Tree4Deltas.Iterator deltasIterator;
    private BlockSequence.Iterator linearIterator;
    private ListEventAssembler deltasAssembler;

    public Tree4DeltasListEvent(ListEventAssembler deltasAssembler, EventList<E> sourceList) {
        super(sourceList);
        this.deltasAssembler = deltasAssembler;
    }

    @Override
    public ListEvent<E> copy() {
        Tree4DeltasListEvent<E> result = new Tree4DeltasListEvent<E>(this.deltasAssembler, this.sourceList);
        result.deltasIterator = this.deltasIterator != null ? this.deltasIterator.copy() : null;
        result.linearIterator = this.linearIterator != null ? this.linearIterator.copy() : null;
        result.deltasAssembler = this.deltasAssembler;
        return result;
    }

    @Override
    public void reset() {
        if (this.deltasAssembler.getUseListBlocksLinear()) {
            this.linearIterator = this.deltasAssembler.getListBlocksLinear().iterator();
            this.deltasIterator = null;
        } else {
            this.deltasIterator = this.deltasAssembler.getListDeltas().iterator();
            this.linearIterator = null;
        }
    }

    @Override
    public boolean next() {
        if (this.linearIterator != null) {
            return this.linearIterator.next();
        }
        return this.deltasIterator.next();
    }

    @Override
    public boolean hasNext() {
        if (this.linearIterator != null) {
            return this.linearIterator.hasNext();
        }
        return this.deltasIterator.hasNext();
    }

    @Override
    public boolean nextBlock() {
        if (this.linearIterator != null) {
            return this.linearIterator.nextBlock();
        }
        return this.deltasIterator.nextNode();
    }

    @Override
    public boolean isReordering() {
        return this.deltasAssembler.getReorderMap() != null;
    }

    @Override
    public int[] getReorderMap() {
        int[] reorderMap = this.deltasAssembler.getReorderMap();
        if (reorderMap == null) {
            throw new IllegalStateException("Cannot get reorder map for a non-reordering change");
        }
        return reorderMap;
    }

    @Override
    public int getIndex() {
        if (this.linearIterator != null) {
            return this.linearIterator.getIndex();
        }
        return this.deltasIterator.getIndex();
    }

    @Override
    public int getBlockStartIndex() {
        if (this.linearIterator != null) {
            return this.linearIterator.getBlockStart();
        }
        return this.deltasIterator.getIndex();
    }

    @Override
    public int getBlockEndIndex() {
        if (this.linearIterator != null) {
            return this.linearIterator.getBlockEnd() - 1;
        }
        return this.deltasIterator.getEndIndex() - 1;
    }

    @Override
    public int getType() {
        if (this.linearIterator != null) {
            return this.linearIterator.getType();
        }
        return this.deltasIterator.getType();
    }

    @Override
    public E getOldValue() {
        if (this.linearIterator != null) {
            return this.linearIterator.getOldValue();
        }
        return this.deltasIterator.getOldValue();
    }

    @Override
    public E getNewValue() {
        return (E)ListEvent.UNKNOWN_VALUE;
    }

    @Override
    public int getBlocksRemaining() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        if (this.linearIterator != null) {
            return "ListEvent: " + this.deltasAssembler.getListBlocksLinear().toString();
        }
        return "ListEvent: " + this.deltasAssembler.getListDeltas().toString();
    }
}

