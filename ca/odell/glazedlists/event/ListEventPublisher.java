/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.event;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEventListener;

public interface ListEventPublisher {
    public void addDependency(EventList var1, ListEventListener var2);

    public void removeDependency(EventList var1, ListEventListener var2);

    public void setRelatedSubject(Object var1, Object var2);

    public void clearRelatedSubject(Object var1);

    public void setRelatedListener(Object var1, Object var2);

    public void clearRelatedListener(Object var1, Object var2);
}

