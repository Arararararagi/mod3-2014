/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.event;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.event.ListEventPublisher;
import ca.odell.glazedlists.event.SequenceDependenciesEventPublisher;
import ca.odell.glazedlists.event.Tree4DeltasListEvent;
import ca.odell.glazedlists.impl.WeakReferenceProxy;
import ca.odell.glazedlists.impl.event.BlockSequence;
import ca.odell.glazedlists.impl.event.Tree4Deltas;
import java.util.ConcurrentModificationException;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class ListEventAssembler<E> {
    protected EventList<E> sourceList;
    private Thread eventThread;
    protected int eventLevel = 0;
    protected boolean allowNestedEvents = true;
    protected int[] reorderMap = null;
    private BlockSequence<E> blockSequence = new BlockSequence();
    private boolean useListBlocksLinear = false;
    private Tree4Deltas<E> listDeltas = new Tree4Deltas();
    private final SequenceDependenciesEventPublisher publisher;
    private final ListEvent<E> listEvent;
    private final ListEventAssembler<E> eventFormat;
    private boolean eventIsBeingPublished;

    public static ListEventPublisher createListEventPublisher() {
        return new SequenceDependenciesEventPublisher();
    }

    public ListEventAssembler(EventList<E> sourceList, ListEventPublisher publisher) {
        this.eventFormat = new ListEventFormat();
        this.eventIsBeingPublished = false;
        this.sourceList = sourceList;
        this.publisher = (SequenceDependenciesEventPublisher)publisher;
        this.listEvent = new Tree4DeltasListEvent<E>(this, sourceList);
    }

    public void beginEvent() {
        this.beginEvent(false);
    }

    public synchronized void beginEvent(boolean allowNestedEvents) {
        if (!this.allowNestedEvents) {
            throw new ConcurrentModificationException("Cannot begin a new event while another event is in progress by thread, " + this.eventThread.getName());
        }
        this.allowNestedEvents = allowNestedEvents;
        if (allowNestedEvents || this.eventLevel == 0 && this.eventThread != null) {
            this.listDeltas.setAllowContradictingEvents(true);
        }
        if (this.eventThread == null) {
            this.eventThread = Thread.currentThread();
            this.useListBlocksLinear = true;
        }
        ++this.eventLevel;
    }

    public void elementInserted(int index, E newValue) {
        this.addChange(2, index, index, ListEvent.UNKNOWN_VALUE, newValue);
    }

    public void elementUpdated(int index, E oldValue, E newValue) {
        this.addChange(1, index, index, oldValue, newValue);
    }

    public void elementDeleted(int index, E oldValue) {
        this.addChange(0, index, index, oldValue, ListEvent.UNKNOWN_VALUE);
    }

    public void elementUpdated(int index, E oldValue) {
        this.elementUpdated(index, oldValue, ListEvent.UNKNOWN_VALUE);
    }

    public void addChange(int type, int startIndex, int endIndex) {
        this.addChange(type, startIndex, endIndex, ListEvent.UNKNOWN_VALUE, ListEvent.UNKNOWN_VALUE);
    }

    public void addChange(int type, int index) {
        this.addChange(type, index, index);
    }

    public void addInsert(int index) {
        this.addChange(2, index);
    }

    public void addDelete(int index) {
        this.addChange(0, index);
    }

    public void addUpdate(int index) {
        this.addChange(1, index);
    }

    public void addInsert(int startIndex, int endIndex) {
        this.addChange(2, startIndex, endIndex);
    }

    public void addDelete(int startIndex, int endIndex) {
        this.addChange(0, startIndex, endIndex);
    }

    public void addUpdate(int startIndex, int endIndex) {
        this.addChange(1, startIndex, endIndex);
    }

    private void addChange(int type, int startIndex, int endIndex, E oldValue, E newValue) {
        if (this.useListBlocksLinear) {
            boolean success = this.blockSequence.addChange(type, startIndex, endIndex + 1, oldValue, newValue);
            if (success) {
                return;
            }
            this.listDeltas.addAll(this.blockSequence);
            this.useListBlocksLinear = false;
        }
        switch (type) {
            case 2: {
                this.listDeltas.targetInsert(startIndex, endIndex + 1, newValue);
                break;
            }
            case 1: {
                this.listDeltas.targetUpdate(startIndex, endIndex + 1, oldValue, newValue);
                break;
            }
            case 0: {
                this.listDeltas.targetDelete(startIndex, endIndex + 1, oldValue);
            }
        }
    }

    public void reorder(int[] reorderMap) {
        if (!this.isEventEmpty()) {
            throw new IllegalStateException("Cannot combine reorder with other change events");
        }
        if (reorderMap.length == 0) {
            return;
        }
        this.addChange(0, 0, reorderMap.length - 1, ListEvent.UNKNOWN_VALUE, ListEvent.UNKNOWN_VALUE);
        this.addChange(2, 0, reorderMap.length - 1, ListEvent.UNKNOWN_VALUE, ListEvent.UNKNOWN_VALUE);
        this.reorderMap = reorderMap;
    }

    public void forwardEvent(ListEvent<?> listChanges) {
        this.beginEvent(false);
        this.reorderMap = null;
        if (this.isEventEmpty() && listChanges.isReordering()) {
            this.reorder(listChanges.getReorderMap());
        } else {
            while (listChanges.next()) {
                int type = listChanges.getType();
                int index = listChanges.getIndex();
                Object oldValue = listChanges.getOldValue();
                Object newValue = listChanges.getNewValue();
                this.addChange(type, index, index, oldValue, newValue);
            }
            listChanges.reset();
        }
        this.commitEvent();
    }

    public synchronized void commitEvent() {
        if (this.eventLevel == 0) {
            throw new IllegalStateException("Cannot commit without an event in progress");
        }
        --this.eventLevel;
        this.allowNestedEvents = true;
        if (this.eventLevel != 0) {
            return;
        }
        if (this.isEventEmpty()) {
            this.cleanup();
            return;
        }
        if (this.eventIsBeingPublished) {
            return;
        }
        this.eventIsBeingPublished = true;
        this.publisher.fireEvent(this.sourceList, this.listEvent, this.eventFormat);
    }

    public synchronized void discardEvent() {
        if (this.eventLevel == 0) {
            throw new IllegalStateException("Cannot discard without an event in progress");
        }
        --this.eventLevel;
        this.allowNestedEvents = true;
        if (this.eventLevel == 0) {
            this.cleanup();
        }
    }

    public boolean isEventEmpty() {
        return this.useListBlocksLinear ? this.blockSequence.isEmpty() : this.listDeltas.isEmpty();
    }

    public synchronized void addListEventListener(ListEventListener<? super E> listChangeListener) {
        this.publisher.addListener(this.sourceList, listChangeListener, this.eventFormat);
    }

    public synchronized void removeListEventListener(ListEventListener<? super E> listChangeListener) {
        this.publisher.removeListener(this.sourceList, listChangeListener);
    }

    public List<ListEventListener<E>> getListEventListeners() {
        return this.publisher.getListeners(this.sourceList);
    }

    boolean getUseListBlocksLinear() {
        return this.useListBlocksLinear;
    }

    Tree4Deltas getListDeltas() {
        return this.listDeltas;
    }

    BlockSequence getListBlocksLinear() {
        return this.blockSequence;
    }

    int[] getReorderMap() {
        return this.reorderMap;
    }

    private void cleanup() {
        this.eventThread = null;
        this.blockSequence.reset();
        this.listDeltas.reset(this.sourceList.size());
        this.reorderMap = null;
        this.listDeltas.setAllowContradictingEvents(false);
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private class ListEventFormat
    implements SequenceDependenciesEventPublisher.EventFormat<EventList<E>, ListEventListener<? super E>, ListEvent<E>> {
        private ListEventFormat() {
        }

        @Override
        public void fire(EventList<E> subject, ListEvent<E> event, ListEventListener<? super E> listener) {
            event.reset();
            listener.listChanged(event);
        }

        @Override
        public void postEvent(EventList<E> subject) {
            ListEventAssembler.this.cleanup();
            ListEventAssembler.this.eventIsBeingPublished = false;
        }

        @Override
        public boolean isStale(EventList<E> subject, ListEventListener<? super E> listener) {
            if (listener instanceof WeakReferenceProxy && ((WeakReferenceProxy)listener).getReferent() == null) {
                ((WeakReferenceProxy)listener).dispose();
                return true;
            }
            return false;
        }
    }

}

