/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists.event;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.event.ListEventPublisher;
import ca.odell.glazedlists.impl.adt.IdentityMultimap;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
final class SequenceDependenciesEventPublisher
implements ListEventPublisher,
Serializable {
    private static final long serialVersionUID = -8228256898169043019L;
    private transient int reentrantFireEventCount;
    private final transient Map<Object, EventFormat> subjectsToCleanUp = new IdentityHashMap<Object, EventFormat>();
    private final transient Map<Object, Object> listenersToRelatedSubjects = new IdentityHashMap<Object, Object>();
    private transient int nextToNotify;
    private transient List<SubjectAndListener> subjectAndListeners = Collections.EMPTY_LIST;
    private transient List<SubjectAndListener> subjectsAndListenersForCurrentEvent;

    SequenceDependenciesEventPublisher() {
    }

    private Object readResolve() throws ObjectStreamException {
        return new SequenceDependenciesEventPublisher();
    }

    private List<SubjectAndListener> orderSubjectsAndListeners(List<SubjectAndListener> subjectsAndListeners) {
        ArrayList<SubjectAndListener> result = new ArrayList<SubjectAndListener>();
        IdentityMultimap<Object, SubjectAndListener> sourceToPairs = new IdentityMultimap<Object, SubjectAndListener>();
        IdentityMultimap<Object, SubjectAndListener> targetToPairs = new IdentityMultimap<Object, SubjectAndListener>();
        IdentityHashMap<Object, Boolean> satisfied = new IdentityHashMap<Object, Boolean>();
        ArrayList<Object> satisfiedToDo = new ArrayList<Object>();
        int size = subjectsAndListeners.size();
        for (int i = 0; i < size; ++i) {
            SubjectAndListener subjectAndListener = subjectsAndListeners.get(i);
            Object source = subjectAndListener.subject;
            Object target = this.getRelatedSubject(subjectAndListener.listener);
            sourceToPairs.addValue(source, subjectAndListener);
            targetToPairs.addValue(target, subjectAndListener);
            satisfied.remove(target);
            if (targetToPairs.count(source) != 0) continue;
            satisfied.put(source, Boolean.TRUE);
        }
        satisfiedToDo.addAll(satisfied.keySet());
        while (!satisfiedToDo.isEmpty()) {
            Object subject = satisfiedToDo.remove(0);
            Object sourceTargets = sourceToPairs.get(subject);
            int targetsSize = sourceTargets.size();
            block2 : for (int t = 0; t < targetsSize; ++t) {
                Object sourceTarget = this.getRelatedSubject(((SubjectAndListener)sourceTargets.get(t)).listener);
                Object allSourcesForSourceTarget = targetToPairs.get(sourceTarget);
                if (allSourcesForSourceTarget.size() == 0) continue;
                int sourcesSize = allSourcesForSourceTarget.size();
                for (int s = 0; s < sourcesSize; ++s) {
                    SubjectAndListener sourceAndTarget = (SubjectAndListener)allSourcesForSourceTarget.get(s);
                    if (!satisfied.containsKey(sourceAndTarget.subject)) continue block2;
                }
                result.addAll((Collection<SubjectAndListener>)allSourcesForSourceTarget);
                targetToPairs.remove(sourceTarget);
                satisfiedToDo.add(sourceTarget);
                satisfied.put(sourceTarget, Boolean.TRUE);
            }
        }
        if (!targetToPairs.isEmpty()) {
            throw new IllegalStateException("Listener cycle detected, " + targetToPairs.values());
        }
        return result;
    }

    private Object getRelatedSubject(Object listener) {
        Object subject = this.listenersToRelatedSubjects.get(listener);
        if (subject == null) {
            return listener;
        }
        return subject;
    }

    public synchronized <Subject, Listener, Event> void addListener(Subject subject, Listener listener, EventFormat<Subject, Listener, Event> eventFormat) {
        List<SubjectAndListener> unordered = this.updateListEventListeners(subject, listener, null, eventFormat);
        this.subjectAndListeners = this.orderSubjectsAndListeners(unordered);
    }

    public synchronized void removeListener(Object subject, Object listener) {
        this.subjectAndListeners = this.updateListEventListeners(subject, null, listener, null);
    }

    private <Subject, Listener, Event> List<SubjectAndListener> updateListEventListeners(Subject subject, Listener listenerToAdd, Listener listenerToRemove, EventFormat<Subject, Listener, Event> eventFormat) {
        int anticipatedSize = this.subjectAndListeners.size() + (listenerToAdd == null ? -1 : 1);
        ArrayList<SubjectAndListener> result = new ArrayList<SubjectAndListener>(anticipatedSize);
        int n = this.subjectAndListeners.size();
        for (int i = 0; i < n; ++i) {
            SubjectAndListener originalSubjectAndListener = this.subjectAndListeners.get(i);
            if (originalSubjectAndListener.listener == listenerToRemove && originalSubjectAndListener.subject == subject) {
                listenerToRemove = null;
                continue;
            }
            if (originalSubjectAndListener.eventFormat.isStale(originalSubjectAndListener.subject, originalSubjectAndListener.listener)) continue;
            result.add(originalSubjectAndListener);
        }
        if (listenerToRemove != null) {
            throw new IllegalArgumentException("Cannot remove nonexistent listener " + listenerToRemove);
        }
        if (listenerToAdd != null) {
            result.add(new SubjectAndListener<Subject, Listener, Event>(subject, listenerToAdd, eventFormat));
        }
        return result;
    }

    @Override
    public void setRelatedListener(Object subject, Object relatedListener) {
        this.addListener(relatedListener, subject, NoOpEventFormat.INSTANCE);
    }

    @Override
    public void clearRelatedListener(Object subject, Object relatedListener) {
        this.removeListener(relatedListener, subject);
    }

    @Override
    public void addDependency(EventList dependency, ListEventListener listener) {
    }

    @Override
    public void removeDependency(EventList dependency, ListEventListener listener) {
    }

    @Override
    public void setRelatedSubject(Object listener, Object relatedSubject) {
        if (relatedSubject != null) {
            this.listenersToRelatedSubjects.put(listener, relatedSubject);
        } else {
            this.listenersToRelatedSubjects.remove(listener);
        }
    }

    @Override
    public void clearRelatedSubject(Object listener) {
        this.listenersToRelatedSubjects.remove(listener);
    }

    public synchronized <Listener> List<Listener> getListeners(Object subject) {
        ArrayList<Object> result = new ArrayList<Object>();
        int size = this.subjectAndListeners.size();
        for (int i = 0; i < size; ++i) {
            SubjectAndListener subjectAndListener = this.subjectAndListeners.get(i);
            if (subjectAndListener.subject != subject) continue;
            result.add(subjectAndListener.listener);
        }
        return result;
    }

    public <Subject, Listener, Event> void fireEvent(Subject subject, Event event, EventFormat<Subject, Listener, Event> eventFormat) {
        if (this.reentrantFireEventCount == 0) {
            this.subjectsAndListenersForCurrentEvent = this.subjectAndListeners;
            this.nextToNotify = Integer.MAX_VALUE;
        }
        ++this.reentrantFireEventCount;
        try {
            EventFormat<Subject, Listener, Event> previous = this.subjectsToCleanUp.put(subject, eventFormat);
            if (previous != null) {
                throw new IllegalStateException("Reentrant fireEvent() by \"" + subject + "\"");
            }
            int subjectAndListenersSize = this.subjectsAndListenersForCurrentEvent.size();
            for (int i = 0; i < subjectAndListenersSize; ++i) {
                SubjectAndListener subjectAndListener = this.subjectsAndListenersForCurrentEvent.get(i);
                if (subjectAndListener.subject != subject) continue;
                if (i < this.nextToNotify) {
                    this.nextToNotify = i;
                }
                subjectAndListener.addPendingEvent(event);
            }
            if (this.reentrantFireEventCount != 1) {
                return;
            }
            RuntimeException toRethrow = null;
            do {
                SubjectAndListener nextToFire = null;
                for (int i2 = this.nextToNotify; i2 < subjectAndListenersSize; ++i2) {
                    SubjectAndListener subjectAndListener = this.subjectsAndListenersForCurrentEvent.get(i2);
                    if (!subjectAndListener.hasPendingEvent()) continue;
                    nextToFire = subjectAndListener;
                    this.nextToNotify = i2 + 1;
                    break;
                }
                if (nextToFire == null) break;
                try {
                    nextToFire.firePendingEvent();
                }
                catch (RuntimeException e) {
                    if (toRethrow != null) continue;
                    toRethrow = e;
                }
            } while (true);
            for (Map.Entry<Object, EventFormat> subjectAndEventFormat : this.subjectsToCleanUp.entrySet()) {
                try {
                    subjectAndEventFormat.getValue().postEvent(subjectAndEventFormat.getKey());
                }
                catch (RuntimeException e) {
                    if (toRethrow != null) continue;
                    toRethrow = e;
                }
            }
            this.subjectsToCleanUp.clear();
            this.subjectsAndListenersForCurrentEvent = null;
            if (toRethrow != null) {
                throw toRethrow;
            }
        }
        finally {
            --this.reentrantFireEventCount;
        }
    }

    public static interface EventFormat<Subject, Listener, Event> {
        public void fire(Subject var1, Event var2, Listener var3);

        public void postEvent(Subject var1);

        public boolean isStale(Subject var1, Listener var2);
    }

    private static class NoOpEventFormat
    implements EventFormat {
        public static final EventFormat INSTANCE = new NoOpEventFormat();

        private NoOpEventFormat() {
        }

        public void fire(Object subject, Object event, Object listener) {
            throw new UnsupportedOperationException();
        }

        public void postEvent(Object subject) {
            throw new UnsupportedOperationException();
        }

        public boolean isStale(Object subject, Object listener) {
            return false;
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private static class SubjectAndListener<Subject, Listener, Event> {
        private final Subject subject;
        private final Listener listener;
        private final EventFormat<Subject, Listener, Event> eventFormat;
        private Event pendingEvent;

        public SubjectAndListener(Subject subject, Listener listener, EventFormat<Subject, Listener, Event> eventFormat) {
            this.subject = subject;
            this.listener = listener;
            this.eventFormat = eventFormat;
        }

        public boolean hasPendingEvent() {
            return this.pendingEvent != null;
        }

        public void addPendingEvent(Event pendingEvent) {
            if (this.pendingEvent != null) {
                throw new IllegalStateException();
            }
            if (pendingEvent == null) {
                throw new IllegalStateException();
            }
            this.pendingEvent = pendingEvent;
        }

        public void firePendingEvent() {
            assert (this.pendingEvent != null);
            try {
                this.eventFormat.fire(this.subject, this.pendingEvent, this.listener);
            }
            finally {
                this.pendingEvent = null;
            }
        }

        public String toString() {
            String separator = this.hasPendingEvent() ? ">>>" : "-->";
            return this.subject + separator + this.listener;
        }
    }

}

