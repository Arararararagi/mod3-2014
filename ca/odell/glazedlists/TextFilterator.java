/*
 * Decompiled with CFR 0_102.
 */
package ca.odell.glazedlists;

import java.util.List;

public interface TextFilterator<E> {
    public void getFilterStrings(List<String> var1, E var2);
}

