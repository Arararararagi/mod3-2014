/*
 * Decompiled with CFR 0_102.
 */
import calculo.business.Modelo3;
import calculo.business.Rubricas;
import calculo.business.pojos.Contribuinte;
import calculo.business.pojos.SPA;
import calculo.common.util.Mod3IO;
import calculo.common.util.Utils;
import calculo.rubricas.Acrescimos;
import calculo.rubricas.AnexoC;
import calculo.rubricas.AnexoD;
import calculo.rubricas.BenefFiscais;
import calculo.rubricas.BensImoveis;
import calculo.rubricas.CategoriaB;
import calculo.rubricas.DespEducacao;
import calculo.rubricas.DespesaAgrB;
import calculo.rubricas.DespesaProB;
import calculo.rubricas.MaisValias;
import calculo.rubricas.PPA;
import calculo.rubricas.PPRPPE;
import calculo.rubricas.Pensoes;
import calculo.rubricas.RendTrabDep;
import calculo.rubricas.RendimAgrB;
import calculo.rubricas.RendimProB;
import java.applet.Applet;

public class Mod3
extends Applet {
    private static final long serialVersionUID = 1;
    Modelo3 Modelo3;

    @Override
    public void init() {
        this.Modelo3 = new Modelo3();
    }

    @Override
    public String getAppletInfo() {
        return "Simulador de C\u00e1lculo IRS";
    }

    public void mod3Inicializa() {
        this.Modelo3.mod3Inicializa();
    }

    String removeCaracter(String str, char chr) {
        char[] newStr = new char[str.length()];
        int j = 0;
        for (int i = 0; i < str.length(); ++i) {
            if (str.charAt(i) == chr) continue;
            newStr[j] = str.charAt(i);
            ++j;
        }
        return new String(newStr);
    }

    String removeEspacos(String str) {
        return this.removeCaracter(str, ' ');
    }

    public void setVal3(String idRubricas, String idRubrica, String value) {
        boolean flag = false;
        value = this.removeEspacos(value);
        if (idRubricas.equals("DespEducacao") && idRubrica.equals("numDepDespFPr")) {
            ((DespEducacao)this.Modelo3.getRubricas("DespEducacao")).setNumDepDespFPr(value);
            flag = true;
        }
        if (!flag) {
            this.Modelo3.setVal(idRubricas, idRubrica, value);
        }
    }

    public void setVal4(String idRubricas, String idRubrica, String idCont, String value) {
        value = this.removeEspacos(value);
        this.Modelo3.setVal(idRubricas, idRubrica, idCont, value);
    }

    public void setIdade(String idCont, String nIdade) {
        if (nIdade == null) {
            nIdade = "0";
        }
        this.Modelo3.getContribuinte(idCont).setIdade(nIdade);
    }

    public void setGrauDef(String idCont, String graudef) {
        this.Modelo3.getContribuinte(idCont).setGrauDef(graudef);
    }

    public void setGrauDefaux(String idCont, String graudef) {
        this.Modelo3.getContribuinte(idCont).setGrauDefaux(graudef);
    }

    public void setDefArm(String idCont, String def) {
        this.Modelo3.getContribuinte(idCont).setDefFArm(def);
    }

    public void setGuardaConjunta(String idCont, String guardaConjunta) {
        this.Modelo3.getContribuinte(idCont).setGuardaConjunta(guardaConjunta);
    }

    public void setComRendim(String idCont, String rendim) {
        this.Modelo3.getContribuinte(idCont).setComRendim(rendim);
    }

    public void setAnoSim(int eAno) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setAnoSim(eAno);
    }

    public void setReside(String local) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setReside(local);
    }

    public void setEstadoCivil(String eCivil) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setEstadoCivil(eCivil);
    }

    public void setNumDepNDef(String numDepNDef) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setNumDepNDef(numDepNDef);
    }

    public void setNumDepDef(String numDepDef) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setNumDepDef(numDepDef);
    }

    public void setNumAfilhados(String numAfilhados) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setNumAfilhados(numAfilhados);
    }

    public void setNumDepGCNDef(String numDepGCNDef) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setNumDepGCNDef(numDepGCNDef);
    }

    public void setNumDepGCDef(String numDepGCDef) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setNumDepGCDef(numDepGCDef);
    }

    public void setNumDepGC03(String numDepGC03) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setNumDepGC03(numDepGC03);
    }

    public void setNumAscNDef(String numAscNDef) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setNumAscNDef(numAscNDef);
    }

    public void setNumAscDef(String numAscDef) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setNumAscDef(numAscDef);
    }

    public void setNumDep03(String numDep03) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setNumDep03(numDep03);
    }

    public void setServFin(String servFin) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setServFin(servFin);
    }

    public void setObito(String obitoConjug) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setObito(obitoConjug);
    }

    public void setEnglobE(String englobE) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setEnglobE(englobE);
    }

    public void setEnglobG(String englobG) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setEnglobG(englobG);
    }

    public void setEnglobF(String englobF) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setEnglobF(englobF);
    }

    public void setOpcaoEnglobCatF(boolean englobF2) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setOpcaoEnglobCatF(englobF2);
    }

    public void setDocsE(String juntDocs) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setDocsE(juntDocs);
    }

    public void setDataAquisicao(String idRubricas, String idRubrica, String value) {
        if (value == null) {
            value = "";
        }
        this.Modelo3.setDataAquisicao(idRubricas, idRubrica, value);
    }

    public void setDataRealizacao(String idRubricas, String idRubrica, String value) {
        if (value == null) {
            value = "";
        }
        this.Modelo3.setDataRealizacao(idRubricas, idRubrica, value);
    }

    public void setValAquisicao(String idRubricas, String idRubrica, String value) {
        value = this.removeEspacos(value);
        this.Modelo3.setValAquisicao(idRubricas, idRubrica, value);
    }

    public void setValRealizacao(String idRubricas, String idRubrica, String value) {
        value = this.removeEspacos(value);
        this.Modelo3.setValRealizacao(idRubricas, idRubrica, value);
    }

    public void setValEncargos(String idRubricas, String idRubrica, String value) {
        value = this.removeEspacos(value);
        this.Modelo3.setValEncargos(idRubricas, idRubrica, value);
    }

    public void setnaturRendB(String idCont, String nRendB) {
        this.Modelo3.getContribuinte(idCont).setnaturRendB(nRendB);
    }

    public void settipoRendB(String idCont, String tRendB) {
        this.Modelo3.getContribuinte(idCont).settipoRendB(tRendB);
    }

    public void settipoRendC(String idCont, String tRendC) {
        this.Modelo3.getContribuinte(idCont).settipoRendC(tRendC);
    }

    public void settipoRendD(String idCont, String tRendD) {
        this.Modelo3.getContribuinte(idCont).settipoRendD(tRendD);
    }

    public void inicialSolteiro() {
        ((PPA)this.Modelo3.getRubricas("PPA")).inicialSujeito("SPB");
        ((BenefFiscais)this.Modelo3.getRubricas("BenefFiscais")).actualizaPPA(this.Modelo3.getRubricas("PPA"));
        ((PPRPPE)this.Modelo3.getRubricas("PPRPPE")).inicialSujeito("SPB");
        ((BenefFiscais)this.Modelo3.getRubricas("BenefFiscais")).actualizaPPR(this.Modelo3.getRubricas("PPRPPE"));
        this.Modelo3.getContribuinte("SPB").setIdade("0");
    }

    public void inicialSeparadoFacto() {
        ((PPA)this.Modelo3.getRubricas("PPA")).inicialSujeito("SPB");
        ((BenefFiscais)this.Modelo3.getRubricas("BenefFiscais")).actualizaPPA(this.Modelo3.getRubricas("PPA"));
        ((PPRPPE)this.Modelo3.getRubricas("PPRPPE")).inicialSujeito("SPB");
        ((BenefFiscais)this.Modelo3.getRubricas("BenefFiscais")).actualizaPPR(this.Modelo3.getRubricas("PPRPPE"));
        this.Modelo3.getContribuinte("SPB").setIdade("");
    }

    public void initRendimentos() {
        this.Modelo3.getContribuinte("SPA").initRendimentos();
        this.Modelo3.getContribuinte("SPB").initRendimentos();
        this.Modelo3.getContribuinte("D1").initRendimentos();
        this.Modelo3.getContribuinte("D2").initRendimentos();
        this.Modelo3.getContribuinte("D3").initRendimentos();
        this.Modelo3.getContribuinte("SPF").initRendimentos();
    }

    void initRendimentosSujeitoCatB(String idCont) {
    }

    public void initRendimentosCatB() {
        this.initRendimentosSujeitoCatB("SPA");
        this.initRendimentosSujeitoCatB("SPB");
        this.initRendimentosSujeitoCatB("D1");
        this.initRendimentosSujeitoCatB("D2");
        this.initRendimentosSujeitoCatB("D3");
    }

    public double getVal2(String idRubricas, String idRubrica) {
        if (idRubricas.equals("DespEducacao") && idRubrica.equals("numDepDespFPr")) {
            return ((DespEducacao)this.Modelo3.getRubricas("DespEducacao")).getNumDepDespFPr();
        }
        return this.Modelo3.getVal(idRubricas, idRubrica);
    }

    public double getVal3(String idRubricas, String idRubrica, String numFisc) {
        return this.Modelo3.getVal(idRubricas, idRubrica, numFisc);
    }

    public double efectuaSimulacao() {
        return this.Modelo3.efectuaSimulacao();
    }

    public double getValRendimGlobal() {
        return this.Modelo3.getValRendimGlobal();
    }

    public double getValDedEspecificas() {
        return this.Modelo3.getValDedEspecificas();
    }

    public double getValAbatimentos() {
        return this.Modelo3.getValAbatimentos();
    }

    public double getValRendimDetTaxas() {
        return this.Modelo3.getValRendimDetTaxas();
    }

    public double getValRendimIsentos() {
        return this.Modelo3.getValRendimIsentos();
    }

    public double getValRendimColectavel() {
        return this.Modelo3.getValRendimColectavel();
    }

    public double getValQuoRendAnter() {
        return this.Modelo3.getValQuoRendAnter();
    }

    public double getValImpostAnosAnt() {
        return this.Modelo3.getValImpostAnosAnt();
    }

    public double getValCoefConjugal() {
        return this.Modelo3.getValCoefConjugal();
    }

    public double getValTaxaImposto() {
        return this.Modelo3.getValTaxaImposto();
    }

    public double getImpostoPago() {
        return this.Modelo3.getImpostoPago();
    }

    public double getImpostoRecebido() {
        return this.Modelo3.getImpostoRecebido();
    }

    public double getValImportApurada() {
        return this.Modelo3.getValImportApurada();
    }

    public double getValParcelaAbater() {
        return this.Modelo3.getValParcelaAbater();
    }

    public double getValorApurado() {
        return this.Modelo3.getValorApurado();
    }

    public double getValImpTribAutonoma() {
        return this.Modelo3.getValImpTribAutonoma();
    }

    public double getValColectaTotal() {
        return this.Modelo3.getValColectaTotal();
    }

    public double getValDeduColecta() {
        return this.Modelo3.getValDeduColecta();
    }

    public double getValColectaLiquida() {
        return this.Modelo3.getValColectaLiquida();
    }

    public double getValRetencoesFonte() {
        return this.Modelo3.getValRetencoesFonte();
    }

    public double getValImpostoApurado() {
        return this.Modelo3.getValImpostoApurado();
    }

    public boolean getMinExistencia() {
        return this.Modelo3.getMinExistencia();
    }

    public double getValPerdasRecup() {
        return this.Modelo3.getValPerdasRecup();
    }

    public double getValAcrescColecta() {
        return this.Modelo3.getValAcrescColecta();
    }

    public double getValPagamentoConta() {
        return this.Modelo3.getValPagamentoConta();
    }

    public double getValorColRendIsent() {
        return this.Modelo3.getValorColRendIsent();
    }

    public static String mascaraValor(double val) {
        return Utils.mascaraValor(val);
    }

    public void setValoresReiniciar() {
        this.Modelo3.mod3Inicializa();
    }

    public String getAnoSim() {
        return String.valueOf(((SPA)this.Modelo3.getContribuinte("SPA")).getAnoSim());
    }

    public String getReside() {
        return String.valueOf(((SPA)this.Modelo3.getContribuinte("SPA")).getReside());
    }

    public String getEstadoCivil() {
        return String.valueOf(((SPA)this.Modelo3.getContribuinte("SPA")).getEstadoCivil());
    }

    public int getNumDepNDef() {
        return ((SPA)this.Modelo3.getContribuinte("SPA")).getNumDepNDef();
    }

    public int getNumDepDef() {
        return ((SPA)this.Modelo3.getContribuinte("SPA")).getNumDepDef();
    }

    public int getNumAfilhados() {
        return ((SPA)this.Modelo3.getContribuinte("SPA")).getNumAfilhados();
    }

    public int getNumDepGCNDef() {
        return ((SPA)this.Modelo3.getContribuinte("SPA")).getNumDepGCNDef();
    }

    public int getNumDepGCDef() {
        return ((SPA)this.Modelo3.getContribuinte("SPA")).getNumDepGCDef();
    }

    public int getNumDepGC03() {
        return ((SPA)this.Modelo3.getContribuinte("SPA")).getNumDepGC03();
    }

    public int getNumAscNDef() {
        return ((SPA)this.Modelo3.getContribuinte("SPA")).getNumAscNDef();
    }

    public int getNumAscDef() {
        return ((SPA)this.Modelo3.getContribuinte("SPA")).getNumAscDef();
    }

    public int getNumDep03() {
        return ((SPA)this.Modelo3.getContribuinte("SPA")).getNumDep03();
    }

    public String getServFin() {
        return ((SPA)this.Modelo3.getContribuinte("SPA")).getServFin();
    }

    public String getObito() {
        return String.valueOf(((SPA)this.Modelo3.getContribuinte("SPA")).getObito());
    }

    public String getIDCont(int index) {
        return this.Modelo3.getContribuinte(index).getIdCont();
    }

    public String getGrauDef(int index) {
        return String.valueOf(this.Modelo3.getContribuinte(index).getGrauDef());
    }

    public String getGrauDefaux(int index) {
        return this.Modelo3.getContribuinte(index).getGrauDefaux();
    }

    public boolean isDefArm(int index) {
        return this.Modelo3.getContribuinte(index).isDefFArm();
    }

    public boolean isGuardaConjunta(int index) {
        return this.Modelo3.getContribuinte(index).isGuardaConjunta();
    }

    public int getIdade(String idCont) {
        return this.Modelo3.getContribuinte(idCont).getIdade();
    }

    public String getnaturRendB(String idCont) {
        return String.valueOf(this.Modelo3.getContribuinte(idCont).getnaturRendB());
    }

    public String getDescEnqB(String idCont) {
        return String.valueOf(this.Modelo3.getContribuinte(idCont).getDescEnqB());
    }

    public String gettipoRendB(String idCont) {
        return String.valueOf(this.Modelo3.getContribuinte(idCont).gettipoRendB());
    }

    public String gettipoRendC(String idCont) {
        return String.valueOf(this.Modelo3.getContribuinte(idCont).gettipoRendC());
    }

    public String gettipoRendD(String idCont) {
        return String.valueOf(this.Modelo3.getContribuinte(idCont).gettipoRendD());
    }

    public boolean isComRendim(String idCont) {
        return this.Modelo3.getContribuinte(idCont).isComRendim();
    }

    public boolean isDeficiente(String idCont) {
        return this.Modelo3.getContribuinte(idCont).isDeficiente();
    }

    public boolean isFamiliaCasado() {
        return ((SPA)this.Modelo3.getContribuinte("SPA")).isFamiliaCasado();
    }

    public void limpaRendimentos(String idCont) {
        ((RendTrabDep)this.Modelo3.getRubricas("RenTrabDep")).inicialSujeito(idCont);
        ((Pensoes)this.Modelo3.getRubricas("Pensoes")).inicialSujeito(idCont);
        if (!idCont.equals("SPF")) {
            ((CategoriaB)this.Modelo3.getRubricas("CategoriaB")).inicialSujeito(idCont);
            ((AnexoC)this.Modelo3.getRubricas("AnexoC")).inicialSujeito(idCont);
            ((AnexoD)this.Modelo3.getRubricas("AnexoD")).inicialSujeito(idCont);
            ((RendimProB)this.Modelo3.getRubricas("RendimProB")).inicialSujeito(idCont);
            ((RendimAgrB)this.Modelo3.getRubricas("RendimAgrB")).inicialSujeito(idCont);
            ((DespesaProB)this.Modelo3.getRubricas("DespesaProB")).inicialSujeito(idCont);
            ((DespesaAgrB)this.Modelo3.getRubricas("DespesaAgrB")).inicialSujeito(idCont);
        }
    }

    public String getEnglobE() {
        return String.valueOf(((SPA)this.Modelo3.getContribuinte("SPA")).getEnglobE());
    }

    public String getEnglobG() {
        return String.valueOf(((SPA)this.Modelo3.getContribuinte("SPA")).getEnglobG());
    }

    public String getEnglobF() {
        return String.valueOf(((SPA)this.Modelo3.getContribuinte("SPA")).getEnglobF());
    }

    public boolean getOpcaoEnglobCatF() {
        return ((SPA)this.Modelo3.getContribuinte("SPA")).getOpcaoEnglobCatF();
    }

    public String getDocsE() {
        return String.valueOf(((SPA)this.Modelo3.getContribuinte("SPA")).getDocsE());
    }

    public String getDataAquisicao(String idRubricas, String idRubrica) {
        return this.Modelo3.getDataAquisicao(idRubricas, idRubrica);
    }

    public String getDataRealizacao(String idRubricas, String idRubrica) {
        return this.Modelo3.getDataRealizacao(idRubricas, idRubrica);
    }

    public double getValAquisicao(String idRubricas, String idRubrica) {
        return this.Modelo3.getValAquisicao(idRubricas, idRubrica);
    }

    public double getValEncargos(String idRubricas, String idRubrica) {
        return this.Modelo3.getValEncargos(idRubricas, idRubrica);
    }

    public double getValRealizacao(String idRubricas, String idRubrica) {
        return this.Modelo3.getValRealizacao(idRubricas, idRubrica);
    }

    public String getImpSPA() {
        return ((SPA)this.Modelo3.getContribuinte("SPA")).getImpSPA();
    }

    public String getImpContribuinte(String idCont) {
        return this.Modelo3.getContribuinte(idCont).getImpContribuinte();
    }

    public String getImpRubricasPorCont(String idRubricas, boolean header, String AgrProf, String Enquadra) {
        return this.Modelo3.getRubricas(idRubricas).getImpRubricasPorCont(header, AgrProf, Enquadra);
    }

    public int comValor(String idRubricas) {
        return this.Modelo3.getRubricas(idRubricas).comValor();
    }

    public String getDescNaturB(String idCont) {
        return this.Modelo3.getContribuinte(idCont).getDescNaturB();
    }

    public String getDescRendB(String idCont) {
        return this.Modelo3.getContribuinte(idCont).getDescRendB();
    }

    public String getDescRendC(String idCont) {
        return this.Modelo3.getContribuinte(idCont).getDescRendC();
    }

    public String getDescRendD(String idCont) {
        return this.Modelo3.getContribuinte(idCont).getDescRendD();
    }

    public String getImpRubricas(String idRubricas) {
        return this.Modelo3.getRubricas(idRubricas).getImpRubricas();
    }

    public String getImpOpcoesCatE() {
        return ((SPA)this.Modelo3.getContribuinte("SPA")).getImpOpcoesCatE();
    }

    public String getImpOpcoesCatG() {
        return ((SPA)this.Modelo3.getContribuinte("SPA")).getImpOpcoesCatG();
    }

    public String getImpressaoMaisValias() {
        BensImoveis rBI = null;
        rBI = (BensImoveis)this.Modelo3.getRubricas("BensImoveis");
        int nBI = rBI.comValor();
        String htmlBI = "";
        if (nBI > 0) {
            htmlBI = rBI.getImpressao(true);
        }
        MaisValias rMV = null;
        rMV = (MaisValias)this.Modelo3.getRubricas("MaisValias");
        boolean nMV = rMV.comValorMV();
        String htmlMV = "";
        if (nMV) {
            htmlMV = nBI > 0 ? rMV.getImpressaoMV(false) : rMV.getImpressaoMV(true);
        }
        int nVals = this.Modelo3.getRubricas("MaisValias").comValor();
        String htmlValsMV = "";
        if (nVals > 0) {
            htmlValsMV = this.Modelo3.getRubricas("MaisValias").getImpRubricas();
        }
        nVals = this.Modelo3.getRubricas("OpFinancei").comValor();
        String htmlValsOF = "";
        if (nVals > 0) {
            htmlValsOF = this.Modelo3.getRubricas("OpFinancei").getImpRubricas();
        }
        String htmlValsIP = "";
        nVals = this.Modelo3.getRubricas("IncrePatrim").comValor();
        if (nVals > 0) {
            htmlValsIP = this.Modelo3.getRubricas("IncrePatrim").getImpRubricas();
        }
        String htmlG = rMV.getImpressao(htmlBI, htmlMV, String.valueOf(htmlValsOF) + htmlValsIP + htmlValsMV);
        return htmlG;
    }

    public String getImpAcrescimos() {
        return ((Acrescimos)this.Modelo3.getRubricas("Acrescimos")).getImpAcrescimos();
    }

    String lerFicheiro(String ficheiro) {
        Mod3IO mod3io = new Mod3IO();
        String retorno = mod3io.readFich(ficheiro);
        if (retorno.equals("")) {
            this.Modelo3 = mod3io.hashToMod3();
            return "";
        }
        return retorno;
    }

    String escreverFicheiro(String ficheiro) {
        Mod3IO mod3io = new Mod3IO();
        mod3io.mod3ToHash(this.Modelo3);
        return mod3io.saveFich(ficheiro);
    }

    public String getDataAquisicaoRec(String idRubricas, String idRubrica) {
        return this.Modelo3.getDataAquisicaoRec(idRubricas, idRubrica);
    }

    public String getDataRealizacaoRec(String idRubricas, String idRubrica) {
        return this.Modelo3.getDataRealizacaoRec(idRubricas, idRubrica);
    }

    public double getValAquisicaoRec(String idRubricas, String idRubrica) {
        return this.Modelo3.getValAquisicaoRec(idRubricas, idRubrica);
    }

    public double getValEncargosRec(String idRubricas, String idRubrica) {
        return this.Modelo3.getValEncargosRec(idRubricas, idRubrica);
    }

    public double getValRealizacaoRec(String idRubricas, String idRubrica) {
        return this.Modelo3.getValRealizacaoRec(idRubricas, idRubrica);
    }

    public void setDataAquisicaoRec(String idRubricas, String idRubrica, String value) {
        if (value == null) {
            value = "";
        }
        this.Modelo3.setDataAquisicaoRec(idRubricas, idRubrica, value);
    }

    public void setDataRealizacaoRec(String idRubricas, String idRubrica, String value) {
        if (value == null) {
            value = "";
        }
        this.Modelo3.setDataRealizacaoRec(idRubricas, idRubrica, value);
    }

    public void setValAquisicaoRec(String idRubricas, String idRubrica, String value) {
        value = this.removeEspacos(value);
        this.Modelo3.setValAquisicaoRec(idRubricas, idRubrica, value);
    }

    public void setValRealizacaoRec(String idRubricas, String idRubrica, String value) {
        value = this.removeEspacos(value);
        this.Modelo3.setValRealizacaoRec(idRubricas, idRubrica, value);
    }

    public void setValEncargosRec(String idRubricas, String idRubrica, String value) {
        value = this.removeEspacos(value);
        this.Modelo3.setValEncargosRec(idRubricas, idRubrica, value);
    }

    public String getEnglobImvG() {
        return String.valueOf(((SPA)this.Modelo3.getContribuinte("SPA")).getEnglobImvG());
    }

    public void setEnglobImvG(String englobG) {
        ((SPA)this.Modelo3.getContribuinte("SPA")).setEnglobImvG(englobG);
    }

    public void setImovelAlienado(String idRubricas, String value) {
        this.Modelo3.setImovelAlienado(idRubricas, value);
    }

    public String getImovelAlienado(String idRubricas) {
        return this.Modelo3.getImovelAlienado(idRubricas);
    }

    public double getValBenFiscalSF() {
        return this.Modelo3.getValBenFiscalSF();
    }

    public double getValStRendimento() {
        return this.Modelo3.getValStRendimento();
    }

    public double getValStDeducoes() {
        return this.Modelo3.getValStDeducoes();
    }

    public double getValStColecta() {
        return this.Modelo3.getValStColecta();
    }

    public double getValStRetencoes() {
        return this.Modelo3.getValStRetencoes();
    }

    public double getValStRendimentoXTaxa() {
        return this.Modelo3.getValStRendimentoXTaxa();
    }

    public double getValTaxaAdicional() {
        return this.Modelo3.getValTaxaAdicional();
    }
}

