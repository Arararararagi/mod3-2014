/*
 * Decompiled with CFR 0_102.
 */
import Mod3;
import java.awt.FileDialog;
import java.awt.Frame;
import java.security.AccessController;
import java.security.PrivilegedAction;

public class Mod3VMSU
extends Mod3 {
    public String m3IO(int operacao, String titulo, String directoria, int posx, int posy) {
        final int foperacao = operacao;
        final String ftitulo = titulo;
        final String fdirectoria = directoria;
        final int fposx = posx;
        final int fposy = posy;
        String s2 = "";
        s2 = (String)AccessController.doPrivileged(new PrivilegedAction(){

            public Object run() {
                String s3 = "";
                FileOperation fileoperation = new FileOperation(foperacao, ftitulo, fdirectoria.replace('/', '\\'), fposx, fposy);
                s3 = fileoperation.getRetorno();
                fileoperation.dispose();
                return s3;
            }
        });
        return s2;
    }

    class FileOperation
    extends Frame {
        String retorno;

        FileOperation(int i, String s, String directoria, int posx, int posy) {
            FileDialog filedialog = new FileDialog(this, s, i);
            filedialog.setDirectory(directoria);
            filedialog.setLocation(posx, posy);
            filedialog.setVisible(true);
            String s1 = filedialog.getFile();
            String s2 = filedialog.getDirectory();
            filedialog.dispose();
            if (!(s1 == null || s1.equals(""))) {
                if (i == 0) {
                    this.retorno = Mod3VMSU.this.lerFicheiro(String.valueOf(s2) + s1);
                    return;
                }
                this.retorno = Mod3VMSU.this.escreverFicheiro(String.valueOf(s2) + s1);
                return;
            }
            this.retorno = "";
        }

        public String getRetorno() {
            return this.retorno;
        }
    }

}

